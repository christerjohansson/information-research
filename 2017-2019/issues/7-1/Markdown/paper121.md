#### Information Research, Vol. 7 No. 1, October 2001,

# Factors influencing environmental scanning in the organizational context

#### [Zita Correia](mailto:Zita.Correia@ineti.pt)  
Centre for Technical Information for Industry  
National Institute for Engineering and Industrial Technology, Lisbon, Portugal  

#### [T.D. Wilson](mailto:t.d.wilson@shef.ac.uk)  
Professor Emeritus  
University of Sheffield, UK

#### **Abstract**

> This paper identifies and analyses the factors internal to the organization, which affect the activity of environmental scanning, understood here as the information seeking activity of managers, directed at the company's external environment. These factors include individual factors, such as information consciousness and exposure to information, and organizational factors such as information climate and "outwardness". The main relationships among them are also analysed. These factors were identified in the course of research aiming to provide a comprehensive understanding of the environmental scanning process ([Correia & Wilson, 1996](#cor96b)). The methodology used - a case-study approach coupled with the grounded theory method of qualitative analysis - was of major importance in obtaining information that is grounded largely on the personal experience of managers.

## Introduction

Information seeking is an individual activity undertaken to identify and select information to satisfy a previously detected information need, the satisfaction of which will enable the individual to solve a problem or make a decision. [Rouse & Rouse](#rou84) (1984) reviewed the contributions to the body of knowledge on human information seeking, of disciplines such as psychology, library science, management, computer science and systems engineering. They described information seeking as "the process of identifying and choosing among alternative information sources" (1984: 131) and concluded:

> "The overall perspective of human information seeking includes a recognition that information seeking is seldom an end in itself, but instead is part of the processes of decision making and problem solving. Further, information seeking is itself a process which usually is dynamic in the sense that information needs as well as information vary in time."([Rouse & Rouse, 1984](#rou84): 135)

On the other hand, Aguilar defined environmental scanning as:

> "...the activity of acquiring information... about events and relationships in a company's outside environment, the knowledge of which would assist top management in its task of charting the company's future course of action." ([Aguilar, 1967](#agu67): 1).

However, an examination of the scanning modes identified by Aguilar elucidates the real nature of scanning: it refers not only to the search for information, but also to the exposure to information, without a specific purpose. [Wilson](#wil96) (1996) has also noted, in his multi-disciplinary review of the literature on human information behaviour, the existence of a passive reception mode of information acquisition. Aguilar stressed that the importance of scanning derived from the importance of the decisions involved and [Mintzberg](#min89) (1989) remarked that the manager is in essence an information processing system.

Studies within the field of organizational research have looked at managers as decision makers who use information to make decisions. Little attention has been paid to the problematic of information acquisition. As noted by [Roberts & Clarke](#rob89) (1989), it has been assumed that "the acquisition of information is a largely non-problematic procedure", and that "no special information skills are required to operate effectively in the external environment" (1989: 33). On the other hand, studies in the field of information needs research, have looked at managers as special information users, who have specific information needs and operate in a specific information-use context. Important inputs have been provided by this branch of research to the study of the information needs of managers and to an understanding of the context of their information use. These studies usually focus on the information needed and the information sources used, within specific contexts.

Research on individual scanning has focused mainly on the individual scanning activities of managers, namely:

1.  on the identification of the information sources used ([Aguilar 1967](#agu67); [Keegan 1974](#kee74); [Smeltzer _et al._ 1988](#sme88); [Culnan, 1983](#cul83); [Ghoshal, 1985](#gho85); and [Choo, 1993](#cho93));
2.  on the environmental sectors scanned ([Aguilar 1967](#agu67); [Kefalas & Schoderbeck, 1973](#kef73); [Hambrick, 1979](#ham79); [Ghoshal 1985](#gho85); [Lester & Walters, 1989](#les89); [Maier, 1992](#mai92); & [Choo, 1993](#cho93));
3.  on the scanning mode and methods ([Aguilar, 1967](#agu67); [Keegan, 1974](#kee74); [Fahey & King](#fah77), 1977; & [Thomas, 1980](#tho80));
4.  on the influence of the role played and the tasks performed upon the scanning activity ([Aguilar, 1967](#agu67); [Kefalas & Schoderbeck, 1973](#kef73); [Keegan, 1974](#kee74); [Hambrick, 1979](#ham79); & [Choo 1993)](#cho93).

Research directed at organizational scanning has attempted to identify varying degrees of formalisation of scanning activities, and the evolutionary pattern of organizational scanning ([Stubbart, 1982](#stu82); [Jain, 1984](#jai84); [Engledow & Lenz, 1986](#eng86); & [Preble _et al._, 1988](#pre88)). But little attention has been paid to aspects such as:

1.  how the information that flows into the organization is internally organized;
2.  whether there is any integration of that information with the internally generated information;
3.  the identification of internal conditions - of an organizational as well as of an individual nature - that may influence access to and use of information in corporations. This paper addresses specifically this particular issue, and is based on research with a broader scope ([Correia, 1996](#cor96a)).

## Methodology and research design

In the context of this research, the case study was considered appropriate for providing a holistic approach to the study of environmental scanning in industrial organizations operating in the chemicals sector. To make up the case study, several types of data and information were collected, including general data about the industry and specific information about the companies that agreed to participate in the study.

The information about the companies included formal data, such as the official designation, address, telephone and fax number, name of the managing director, SIC code of the main industry, sales volume, number of workers, social capital and distribution, type of ownership, which were obtained through two business databases consulted. Other information was used, such as publicly available data provided by the annual reports of the companies, promotional material and newsletters; and historical and cultural information provided by the managers interviewed. The historical and cultural information was provided either orally - as an introductory part of the interview - or in printed form, when available, including organizational charts. All this information was of great importance to contextualise and illuminate the core information regarding the environmental scanning process in the companies analysed. Not all cases, however, provided equally rich frameworks.

[Strauss](#str87) (1987) emphasises the usefulness of the case-study approach when articulated with grounded theory. Grounded theory seeks to generate theoretical statements and, ultimately, theories based on empirical evidence, although it can be used in different ways and reach various degrees of complexity. In this case, grounded theory was used to uncover the motives, events and processes associated with the environmental scanning phenomenon. The concepts and categories that emerged in the course of the analysis led to the identification of four major factors, of an organizational and of an individual nature, which influence the environmental scanning activity of managers.

The research design was that of a multiple case study, composed according to the theory building structure, as described by [Yin](#yin89) (1989: 139), and using the grounded theory method of qualitative analysis.

The sample used in this study did not obey to the principles of statistical sampling, but the principles of maximum variation sampling, as defined by [Patton](#pat90) (1990) and those of theoretical sampling, as defined by [Strauss & Corbin](#str90) (1990). This means sampling on the basis of concepts that have proven theoretical relevance to the evolving theory.

The resulting sample incorporated nineteen companies: five small companies (with more than 10 and fewer than 99 workers), ten medium-size companies (with up to 399 workers) and four large companies (with more than 400 workers). The values used for this classification reflects the characteristics of the Portuguese industrial fabric, where very small (up to 10 workers), small and medium-size companies make up more than 90% of the total number of companies.

The main tool used for collecting the core information for this research was the semi-structured interview, a tool flexible enough to favour adaptation to each context, organization and individual, and also to pursue unexpected paths and cues suggested by the theoretical sensitivity ([Glaser, 1978](#gla78)) developed by the researcher throughout the research process.

Forty seven interviews were carried out. Table 1 shows the distribution according to job title.

Observation played a minor, but non-negligible role, as it contributed to the consolidation of impressions or confirmed information based on documentary evidence or in the interviews.

## Factors influencing environmental scanning

### Individual factors

The individual factors identified as influencing the scanning activity were **information consciousness** and **exposure to information**.

**Information consciousness** means the attitude towards information-related activities, denoting the value attributed to information. This category emerged in the course of the analysis process, as a result of grouping together events, and the concepts describing them, which were judged as pertaining to the same category of events. Those concepts included the personal sense of responsibility for environmental scanning and the communication pattern developed by the individual. All the interviewees agreed about the vital role of information in business. Top managers of large and medium-size companies operating in different sub-sectors described their role, as far as environmental scanning is concerned, as a mix of personal monitoring and dissemination of information among direct collaborators.

Mintzberg identified these aspects of the managerial job as informational roles and described them as follows:

> "As monitor, the manager perpetually scans his or her environment, interrogates liaison contacts and subordinates, and receives unsolicited information, much of it as a result of the network of personal contacts he or she has developed.... Managers must share and distribute much of this information. Information they glean from outside personal contacts may be needed within their organizations. In their disseminator role, managers pass some of their privileged information directly to their subordinates, who would otherwise have no access to it. Moreover, when their subordinates lack easy contact with one another, managers will sometimes pass information from one another." ([Mintzberg, 1989](#min89): 18)

This activity is illustrated by the following quotations from the interviews carried out:

> "The company has its structure and several people collect information... In our marketing department, the sales people, the advertisers of medical information, the supervisors and the marketing director bring in a lot of information. Of course that, when I am at the industrial association or when I am received by a member of government I try to pick up as much information as I can, but my contribution is small." (2,3,11,2)

> "I'm responsible in so far as I co-ordinate all the information. I personally try to collect as much information as I can, but the information that is brought in by the marketing and the R&D directors is brought together by myself and it is used as the basis for decisions that have to be made." (1,1,2,2)

> "I consider to be responsible not only to keep myself informed but also to transmit that information to those who work with me." (19,38,13,4)

A significant difference was detected between managers of larger companies and those of smaller companies. In larger companies, managers tended to minimise their role as monitors and emphasise their role as disseminators; dissemination of information becomes an important issue in larger organizations, where more complex structures and functional diversification are dominant features. On the other hand, managers of smaller companies assume environmental scanning as a personal responsibility and attribute great importance to that activity, while the dissemination factor is irrelevant, because in many cases there is no-one else to pass the information to.

Communication was found to be generally intense between the top manager and the functional directors, and also among functional directors. Functional directors communicated with the chief executive in the first place whenever information of strategic importance was at stake. But communication among directors of the several functional areas was intense, particularly between the marketing or commercial area and the R&D, technical or production areas; the financial and administrative area also provided a lot of information to those two areas, but there was no similar influx in the opposite direction. The quotations that follow, from marketing and commercial directors and R&D directors from large and medium-size companies of different sub-sectors illustrate this assertion:

> "Marketing is one of the areas to which we pass more information, because they depend on that information to adopt a marketing strategy. The product information that we provide may modify drastically the ongoing strategy. We pass that information in every possible way. It depends on the urgency or on the importance. Some things have to be written down and others can be transmitted orally. This may be done directly, or over the telephone, or during a meeting." (3,7,13,3)

> "I pass it to the sales department of the North, the research, the production, and always to the chief executive. I pass it immediately and orally. Sometimes I write a report, depending on the nature of the information." (14,28,19,3)

> "When I obtain important information, I write a report addressed to the administration and to the colleagues to whom that information may interest.... I prefer written information to oral information.... I interact more with the commercial director and with the technical director. I pass information to those colleagues using written notes, or we meet and discuss and record what is said." (19,40,13,2-3)

> "... If [the information] demands immediate action, I act accordingly. If it's someone else's field of action, I pass it on immediately. If it's a trend, I transmit the information to those who can work on it.... [I pass it first] to the administration... Formally or informally, depending on the urgency and the kind of information." (18,37,12,4-13,1)

Communication among managers was made up of a mix of oral information and written information; the nature of this mix and the reasons that determine the choice of either of the forms of communication was not entirely clarified. However, some interviewees associated the choice of oral communication with the generic scope of the information or its potential for starting action.

Chief executives tended to use oral communication more than functional directors, while these apparently used both oral and written forms, without favouring clearly one or the other a priori. Sometimes both forms were used to convey the same information, the oral form being used for the first approach, followed by a memo or a report.

**Exposure to information** means the frequency of opportunities of contact with well- informed people and information-rich contexts. The emergence of this category was based on the analysis of the information networks developed by managers. All the interviewees played a very similar role, since they were all managers, either chief executives or entrepreneurs, or functional directors, mainly marketing and commercial directors. They were therefore in a privileged position concerning the degree of exposure to information.

The performance of the liaison role by chief executives and other managers consisted largely in the setting up of their external information system, as described by [Mintzberg](#min89) (1989), who comments that the contacts cultivated by managers provide them with precious information, even if it is mostly "informal, private and oral".

It was found that access to important sources of information was reached through the appointment to key places within the sectoral and industrial associations and sometimes in international associations, where managers' exposure to information is taken to the full. This fact is testified by the following quotation:

> "...I have other sources of information which are a consequence of my activity in the entrepreneurial world. I was the president of the sectoral association for some years and now I'm the president of the general assembly [in the same association]. I'm also the director of CIP (Confederation of the Portuguese Industry) where I meet monthly representatives of all sectors of the national industry..." (19,38,13,5-14,1)

The formal networks built as a natural consequence of performing this role also pave the way, in turn, for the development of informal networks that are more difficult to detect. Managers tend to be very discreet about these networks. Exposure to information emerged in this study as the individual face of the broader phenomenon of organizational 'outwardness', which was difficult to detach and analyse on its own. This was determined, to a great extent, by the decision to target the organization as the unit of analysis. For this reason, it was considered that the analysis of the training opportunities provided by the organization would elucidate the level of exposure to information of individuals in other ranks within each organization, thus providing a broader picture.

Exposure to information is strongly influenced by the 'outwardness' of the organization: some organizations provide more opportunities of contact with well-informed people or information-rich contexts. It is also influenced by the role played by the individual within the organization: marketing and commercial directors have a much greater exposure to information than financial or administrative directors. But exposure to information is also influenced by the information climate of the organizations, which differ in the range of conditions of access to and use of information by their staff.

### Organizational factors

The organizational factors identified as influencing the environmental scanning activity were **information climate** and **outwardness**.

The **information climate** means the setting of conditions that determine access to and use of information in an organization. It was assessed through the information infrastructure implemented, i.e., the processes, technologies and people used in information acquisition and handling (collecting, organizing and making information available, and disseminating it). But it was the emergence of evidence relating to the role of organizational culture in shaping the information infrastructure that led to the creation of the construct "information climate".

All the pharmaceutical companies had rich, centralised collections of scientific and technical information, managed by information professionals with different backgrounds, and offered access to international on-line systems, and provided selective dissemination of information and loan services. The other companies provided a consistent picture, characterised by loose, small collections made up mainly of specialised journals, market reports and product literature, lack of skilled staff to manage information. The main service provided was the circulation of journals. This was the picture in all the remaining companies, independently of sub-sector or size.

The acquisition, collection or gathering of information was understood in a very broad way, encompassing every situation where information was collected, regardless of the precise kind of information, the media used for that purpose and the degree of intentionality (that is, whether incidental to other activities or deliberate information-related behaviour). Information handling comprises the notions of storage, processing and dissemination of information.

The pervasiveness of information was pointed out as one of the reasons why it is so difficult to account for the costs involved in environmental scanning. In fact, it is always associated with the performance of specific roles which are labelled and acknowledged as product manager, sales agent, marketing director, R&D director, market researcher or planning officer. The italics in the text of the quotations below illustrate this assertion:

> "There's no one exclusively responsible for collecting information. Some people are exposed to information in the course of their duties and they collect it.... It's difficult to separate things, because when they go to the field, fortunately for the company, their purpose is not exclusively to collect information, that would be too expensive. Their activity consists in developing the market. Let's admit that 30% of their time is used for collecting information." (19,40,12,3-4)

> "In this company we consider that the acquisition of information is an obligation of all.... but there is not a formal structure to do that. It's part of their duties." (14,26,25,4)

> "In principle, all the staff are involved in collecting information. Everyone must transmit information to the hierarchy. It's the staff of the commercial and marketing areas who have the mission of collecting information. That information is treated by the marketing department and, when an economic analysis is needed, that is done by the office of economic studies. There are about ten people in the commercial area who have the obligation to collect information about the business. We can't say how much it costs. It's an important function, but it's a secondary function." (18,36,17,3-4)

The data collected indicate an average of 50% of staff involved in information acquisition and handling in the medium to large pharmaceutical companies. Smaller companies have few resources to invest and different needs; an average of 9% of staff was found to be involved in information acquisition and handling in smaller companies of other sectors.

**Outwardness** means the openness of the organizations to the external environment, their permeability to external influences, and capacity to influence the environment. It was assessed through their links with R&D organizations, collaboration with regulatory agencies and participation in development programmes. Apart from two multinational pharmaceutical companies that developed fundamental research, the remaining companies either developed applied research, independently or in association with research organizations, or did not develop research at all. This was the case of large companies operating in declining industries and small companies in the plastics sub-sector, which used external laboratories for quality control only.

Large and medium size companies enjoying relative economic health engage in collaborative actions with a view to influencing legislative and other regulatory initiatives, usually through their sectoral associations, sometimes regional or international bodies. This collaboration, however, is generally passive, i.e., companies tend to act only under request. Companies of the same size going through a crisis tend to turn inwards for reorganization. Smaller companies, on the other hand, usually lack the resources needed to be able to provide collaboration to external bodies.

Most of the successful applications submitted to development programmes, with relevance for PEDIP (Strategic Programme for the Development of the Portuguese Industry), were directed to productive investments and training, this last item being funded either through PEDIP or through the European Social Fund.

Organizational culture emerged as an important factor in the analysis of information issues within these organizations. The type of information culture that prevails in the pharmaceutical companies analysed is a formal information culture, meaning cultures "which exhibit extensive formalisation and systematisation of information channels", as defined by [Brown](#bro90) (1990).

Two other companies in two different sub-sectors were identified as having, respectively, an oral culture, meaning cultures "which support a high degree of word-of-mouth communication", according to Brown (op. cit.) and an information conscious culture (cultures "which demonstrate considerable awareness of the value of information and sophistication in their information behaviour and systems", still according to Brown, 1990). No kind of information culture had developed in the remaining companies, where there was little sensitiveness to information matters, and little investment was made in the information infrastructure.

## Relationships among factors

### Relationship between information consciousness and information climate

The construct **information consciousness** emerges within the individual sphere but at the intersection of individual behaviour and the organizational way-of-doing-things. In fact, the environmental scanning activity, or the information seeking behaviour of managers as far as environmental information is concerned, is affected by organizational culture.

The communication pattern established among managers provides a good ground to verify that influence. Where a strong oral culture dominates, managers, even those who joined the organization recently, tend to conform to the established culture and use predominantly oral means to acquire and transmit information. Where an information conscious culture exists, managers are encouraged to make use of information technologies, including using e-mail for exchanging current messages. On the other hand, where a formal information culture dominates, managers tend to conform to the established formal procedures of acquisition and transmission of information.

The construct **information climate** emerged within the organizational sphere as a set of conditions that determine access to and use of information, including the establishment of an information infrastructure. Again, the organizational culture influences not only the investments made in the infrastructure, but also the type of infrastructure implemented (more or less formalised procedures, type of utilisation made of information technologies, number of people involved in information acquisition and handling). Organizations with an oral culture may underestimate the value of establishing a formal information infrastructure, while an organization with an information-conscious culture will tend to set up advanced information systems. Organizations with formal information cultures tend to stick to traditional information infrastructures. Where no information culture exists, little investment is made in information infrastructures.

The **information-consciousness to information climate** relationship translates the influence detected between the top managers' information consciousness - or the attitude towards information-related activities, including the personal sense of responsibility for environmental scanning and a keen sense of communication - and the type of information climate established in each company.

The role of the top manager proved to be significant, by supporting or neglecting information-related activities and, in particular, in establishing or improving existing information infrastructures. In extreme cases, the top manager may foster the development of an information culture, by favouring the implementation of adequate infrastructures, encouraging the adoption of information technologies, rewarding the use of the facilities available and making heavy use of them, personally.

### Relationship between outwardness and exposure to information

The construct **outwardness** emerged within the organizational sphere as the capacity of the organization to be open to the external environment, thus becoming permeable to external influences and capable of influencing the environment itself.

The openness to the external environment was manifested through the links established with R&D organizations and the participation in development programmes, while the collaboration with regulatory agents reflects the capacity to influence the environment, by intervening in the regulatory process. In fact, outward-oriented companies are very much engaged in lobbying. This activity is pursued through the sectoral associations or international agencies, or independently.

The construct **exposure to information** emerged within the individual sphere as the condition of individual access to well-informed people and information-rich contexts. It depends largely on the organizational role played by the individual, and is related to what is known about the establishment of external networks of contacts by managers, with competitors, clients, suppliers, government agencies and sectoral lobbies.

Some of these networks are of a more formal type, such as those resulting from the appointment to key places in associations or agencies of all kinds, while others are more informal, of the "old-boy network" type, and thus more difficult to detect. Because the focus of analysis was the organization and not the individual, exposure to information emerged as the individual face of the organizational phenomenon of outwardness.

The **outwardness-exposure to information** relationship reflects the impact of the organization's capacity to relate to its environment, on the degree of exposure to information of organizational staff.

The four core categories described above _(information consciousness, exposure to information, information climate and outwardness)_ emerged in the course of the data analysis process. Grounded theory principles and techniques were used to uncover the motives, events and processes associated with the environmental scanning activity of managers in an organizational context. Those motives, events and processes were then labelled and conceptualised, and grouped into categories. Finally, relationships were identified between the emerging categories.

Those categories represent factors influencing the environmental scanning activity. Emergent as they are, their conceptual roots may, however, be traced back to established concepts in two different academic perspectives: Organization Theory and Information Science. Table 2 roots the emergent categories, and the resulting relationships, in the above mentioned academic perspectives.

Information seeking is the individual activity undertaken with the purpose to identify and select information to satisfy a previously detected information need, the satisfaction of which will enable the individual to solve a problem or make a decision. Environmental scanning is the "activity of acquiring information about events and relationships in a company's outside environment, the knowledge of which would assist top management in its task of charting the company's future course of action." ([Aguilar, 1967](#agu67): 1). Environmental scanning refers to information seeking and use in a specific context, that of an organization whose survival and expansion depends on adequate exchanges with the environment surrounding it.

On the other hand, organizational culture "is a pattern of beliefs and expectations shared by the organization's members. These beliefs and expectations produce norms that powerfully shape the behaviour of individuals and groups in the organization." ([Schwartz & Davies, 1981](#sch81): 33). [Brown](#bro90) (1990) developed a bridging concept, "information conscious culture", meaning cultures "which demonstrate considerable awareness of the value of information and sophistication in their information behaviour and systems". The category _information consciousness_ emerged in representation of an attitude that simultaneously expresses the individual's belief in the value of information, and the individual's behaviour in accordance to that belief, either engaging in a purposive information seeking activity, or actively encouraging and supporting information-related activity.

Even though _information climate_ is an emergent category in this context, the term "organizational climate" has a long history in the field of organizational behaviour. An overview article on organizational climate ([Moran & Volkwein, 1992](#mor92)) identifies four conceptual perspectives on this topic:

1.  A structural perspective: climate is an objective manifestation of the organizational structure, ergo climate is formed because members of an organization are exposed to common structural characteristics of an organization.
2.  A perceptual perspective: individuals respond to situational variables in a way that is psychologically meaningful to them. Climate is a psychologically processed description of organizational conditions.
3.  An interactive perspective: the interaction of individuals in responding to the same organizational setting brings forth the shared agreement where the organizational climate is grounded.
4.  A cultural perspective: the interaction of individuals who share a common frame of reference - the organizational culture - creates the organizational climate as they come to terms with situational contingencies.

If we look at _information climate_ as a subset of the organizational climate, we realise that two of the perspectives described above interacted in the formation of the concept _information climate_ . In fact, the structural component (the information infrastructure) and the cultural component (the common frame of reference that makes up the organizational culture) were the ground from which that category emerged.

The category _outwardness_ is rooted in the perspective of organizations as "open systems". The open system approach, when applied to organization studies, represents the adaptation of work carried out in biology and in physics. Two major implications result from regarding organizations as open systems:

1.  Organizations are complex systems, made up of several interdependent subsystems;
2.  Organizations interact with their environment.

[Katz & Kahn](#kat78) (1978) proposed the concepts of "system openness", "system coding", "system boundaries" and "boundary roles" which are central to the paradigm of organizations as open systems. The concept of "system openness" refers to the degree to which the system is receptive to all types of inputs. "Boundary roles" are described as boundary positions held by some members of the organization, in order to help in the export of services, ideas, and products of the system and in the import of the needed inputs. _Outwardness_ incorporates the idea of openness, but also the capacity to influence the environment.

_Exposure to information_ is not alien to the concept of "boundary roles", as people establishing the connection between their organizations and the environment. Similarly, the concept of gatekeeping, is also part of the background against which this category may be assessed. [Allen](#all66) (1966) developed the concept of "technological gatekeeper", understood as a member of a team who acts as the channel for communication of ideas between the organization and the outside and vice-versa. The concept was adopted by researchers in other fields and came to describe gatekeepers as individuals who influence opinions, disseminate information or facilitate cultural adaptation in different settings ([Metoyer-Duran, 1993](#met93)). This concept incorporates, _a priori_, the idea of an individual with outstanding communication skills, playing a prominent role within an informal network. While some of the managers more exposed to information are natural gatekeepers, the concept _exposure to information_ has a broader scope, to include situations of access to information-rich people and contexts, independently of the centrality of the role played.

The relationships described above are represented in Figure 1.

<figure>

![Figure 1](../p121fig1.gif)

<figcaption>

**Figure 1: Relationships among factors affecting environmental scanning**</figcaption>

</figure>

Besides those two main relationships or connections, represented in the figure above by thick arrows, there are indications (represented by broken arrows) that managers with a strong information consciousness tend to foster their organizations' outwardness; on the other hand, where a stimulating information climate is created, the individual exposure to information increases.

## Concluding remarks

As a result of the qualitative analysis of evidence from companies in the chemical industries sector in Portugal, we have evolved four theoretical concepts and have proposed relationships among them. The concepts relate to both organizational and individual behaviour. We suggest that, in terms of information-seeking to support organizational functions, both sets of factors must be taken into account. Further, we propose that the more open the organization is to its environment, particularly in terms of openness to information flows, the more likely it is that individuals in the organization will experience exposure to relevant information and develop an information consciousness. Similarly, to the extent that this occurs, the organization is more likely to develop an information climate that supports the individual.

We have also shown that the marriage of ideas from both organization theory and information science provides a fertile inter-disciplinary perspective from which to view the phenomenon of environmental scanning. We suggest, in fact, that any analysis of environmental scanning _requires_ the use of concepts from these two fields of research if a full understanding of the processes and their effectiveness, or lack of it, in specific situations is to be reached.

## Notes

1\. The numbers between brackets refer, respectively, to the code number attributed to each company, the interviewee, the page number of the transcribed text of the interview, and the number of the paragraph quoted.

## References

*   <a id="agu67"></a>Aguilar, F. J. (1967). _Scanning the business environment._ New York, NY: McMillan.

<a id="all66"></a>
*   Allen, T. J. (1966). _Managing the flow of scientific and technological information_. Cambridge, MA: Massachusetts Institute of Technology. .(Ph.D. thesis)

<a id="bro90"></a>
*   Brown, Andrew D. (1990). _Information, communication and organizational culture: a grounded theory approach._ Sheffield: University of Sheffield. (Ph.D. thesis)

<a id="cho93"></a>
*   Choo, Chun Wei & Auster, Ethel (1993). "Environmental scanning: acquisition and use of information by managers". _Annual Review of Information Science and Technology_, **28**, 279-314\.

<a id="cor96a"></a>
*   Correia, Zita (1996). Scanning the business environment for information: a grounded theory approach. Sheffield: University of Sheffield. (Ph.D. thesis)

<a id="cor96b"></a>
*   Correia, Z. and Wilson, T. D. (1996). "[Scanning the business environment for information](http://informationr.net/ir/2-4/paper21.html)". _Information Research_ **2** (4) Available at http://informationr.net/ir/2-4/paper21.html [Accessed 1 October 2001]

<a id="cul83"></a>
*   Culnan, Mary (1983). "Environmental scanning: the effects of task complexity and source accessibility on information gathering behaviour". _Decision Sciences_, **14** (2), 194-206.

<a id="eng85"></a>
*   Engledow, Jack L. & Lenz, R. T. (1985). "Whatever happened to environmental analysis?" _Long Range Planning_, **14** (2) 32-39.

<a id="fah77"></a>
*   Fahey, Liam & King, William R. (1977). "Environmental scanning for corporate planning" _Business Horizons_, August, 61-71.

<a id="gho85"></a>
*   Ghoshal, Sumantra (1985). _Environmental scanning: an individual and organizational level analysis_. Cambridge, MA: Massachussets Institute of Technology. (Ph. D. thesis)

<a id="gla78"></a>
*   Glaser, B. (1978). _Theoretical sensitivity_. Mill Valley, CA: Sociology Press.

<a id="gla67"></a>
*   Glaser, B. & Strauss, A. (1967). _The discovery of grounded theory: strategies of qualitative research._ London: Weidenfeld & Nicolson.

<a id="ham79"></a>
*   Hambrick, Donald Carol (1979). Environmental scanning, organizational strategy and executive roles: a study in threeindustries. College Station, PA: Pennsylvania State University. (Ph. D. thesis)

<a id="jai84"></a>
*   Jain, Subhash C. (1984). "Environmental scanning in US corporations". _Long Range Planning_, **17** (2), 117-128.

<a id="kat78"></a>
*   Katz, Daniel & Kahn, Robert L. (1978). _The social psychology of organizations_, 2nd ed.. New York, NY: Wiley.

<a id="kee74"></a>
*   Keegan, Warren J. (1974). "Multinational scanning: a study of the information sources utilized by headquarters executives in multinational companies". _Administrative Science Quarterly_, **19**, 411-421.

<a id="kef73"></a>
*   Kefalas, A. G. & Shoderbek, P. P. (1973). "Scanning the business environment: some empirical results." _Decision Sciences_, **4** (1), 63-74.

<a id="les89"></a>
*   Lester, Ray & Waters, Judith (1989). _Environmental scanning and business strategy._ London: British Library Board. (Library and Information Research Report; 75).

<a id="mai92"></a>
*   Maier, Jerry Lee (1992). Environmental scanning for information technology: an investigation of how firms assess the information technology component of the external business environment. Auburn, AL: Auburn University. (Ph. D. thesis)

<a id="met93"></a>
*   Metoyer-Duran, Cheryl (1993). "Information gatekeepers". _Annual Review of Information Science and Technology_, **28**, 111-150.

<a id="mil87"></a>
*   Miles, Robert H. (1987). _Managing the corporate social environment: a grounded theory._ Englewood Cliffs, NJ: Prentice-Hall.

<a id="min89"></a>
*   Mintzberg, Henry (1989). _Mintzberg on management: inside our strange world of organizations_. New York, NY: McMillan.

<a id="mor92"></a>
*   Moran, E. T. & Kwein, J. F. (1992). "The cultural approach to the formation of organizational climate ". _Human Relations_, **45**, 19-47.

<a id="pat90"></a>
*   Patton, Michael Quinn (1990). _Qualitative evaluation and research methods_, 2nd ed.. London: Sage.

<a id="pre88"></a>
*   Preble, J.F., .Rau, P.A. & Reichel, A. (1988). "The environmental scanning practices of US multinationals in the late 1980s". _Management International Review_, **28** (4), 4-14\.

<a id="rob89"></a>
*   Roberts, N. & Clarke, D. (1989). "Organizational information concepts and information management". _International Journal of Information Management_, **9** (1), 25-34.

<a id="rou84"></a>
*   Rouse, William B. & Rouse, Sandra H. (1984). "Human information seeking and design of information systems". _Information Processing and Management_, **20** (1-2), 129-138.

<a id="sch81"></a>
*   Schwartz, H. & Davies, S. M. (1981). "Matching corporate culture and business strategy". _Organizational Dynamics_, **10**, 30-40\.

<a id="sme88"></a>
*   Smeltzer, Larry R., Fann, Gail & Nokolaisen, V. Neal (1988). "Environmental scanning practices in small firms". _Journal of Small Business Management_, July, 55-62.

<a id="str87"></a>
*   Strauss, A. (1987). _Qualitative analysis for social scientists_. Cambridge: Cambridge University Press.

<a id="str90"></a>
*   Strauss, A. & Corbin, Juliet (1990). _Basics of qualitative research: grounded theory procedures and techniques._ London: Sage.

<a id="stu82"></a>
*   Stubbart, Charles (1982). "Are environment scanning units effective?" _Long Range Planning,_ **15** (3), 139-145.

<a id="tho80"></a>
*   Thomas, Philip S. (1980). "Environmental scanning: the state of the art". _Long Range Planning_, **13** (2), 20-25.

<a id="wil96"></a>
*   Wilson, T.D. & Walsh, Christina (1996). _[Information behaviour: an inter-disciplinary perspective](http://informationr.net/tdw/publ/infbehav/prelims.html)_. London: British Library Research and Innovation Centre. Available at http://informationr.net/tdw/publ/infbehav/prelims.html [Accessed 1 October 2001]

<a id="yin89"></a>
*   Yin, Robert (1989). _Case study research: design and methods_. Rev. ed., London: Sage. (Applied Social Research Methods Series; 5).