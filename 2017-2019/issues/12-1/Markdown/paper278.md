#### Vol. 12 No. 1, October 2006

* * *

# The presentation of the information user in reports of information behaviour research

#### [Lynne (E.F.) McKechnie](mailto:mckechnie@uwo.ca),
Faculty of Information and Media Studies,
The University of Western Ontario,
London, Ontario, Canada  
#### [Heidi Julien](mailto:Heidi.Julien@ualberta.ca)  
School of Library and Information Studies,
University of Alberta,
Edmonton, Alberta, Canada  
#### [Jennifer L. Pecoskie](mailto:jpecoski@uwo.ca) and [Christopher M. Dixon](mailto:cdixon9@uwo.ca)  
Faculty of Information and Media Studies,
The University of Western Ontario,
London, Ontario, Canada

#### Abstract

> **Introduction.** This study examined how human information behaviour researchers present research study participants through the following research questions: How do human information behaviour researchers describe participants in reports of empirical research?; and, What strategies do human information behaviour researchers use in their presentation of participants?  
> **Method and Analysis.** A content analysis was conducted of 96 research reports published in the 1996-2004 ISIC proceedings. Articles were coded for data collection and data analysis methods, the use of theory, and parts of the article (title, abstract, literature search, method, findings, discussion and conclusion) where participants were included. Also noted were terms used to label research participants, data collection practices and strategies used by authors to present users.  
> **Results.** Authors use a variety of terms and data collection practices that construct the information user or research participant as both peripheral and central to research.  
> **Conclusion.** Researchers must bring greater consciousness to the conduct and reporting of their research. Improvements are needed in doctoral training, research methods texts and publishers' instructions to authors.

## Introduction

Human information behaviour research, like much scholarly endeavour, would be impossible without research participants. The labels we use to identify these people who are critical elements in academic study (e.g., users, subjects, respondents, persons, and interviewees), directly reflect the relationships created between the researchers and the researched. Despite the undeniably good intentions of human information behaviour researchers there appears to be some dissonance between the value of research participants to the research enterprise and the ways in which participants are constructed in the presentation of results to the human information behaviour community of scholars. The labels we choose, consciously or not, construct research participants in particular ways and reflect deeply held, and perhaps not thoroughly examined, theoretical positions on the part of researchers. This concern has been raised previously in the human information behaviour literature ([Tuominen 1997](#tuo97); [Julien 1999b](#jul99b); [Julien 1999a](#jul99a)).

The centrality of the information user (hereafter simply 'user') in human information behaviour research is reflected in influential works such as Wilson's ([1981](#wil81)) information-seeking model which clearly positions the user at its centre and Dervin and Nilan's ([1986](#der86)) landmark literature review which advocated shifting from a systems to a user oriented perspective. Since the publication of these works a number of human information behaviour researchers have worked from a user-centred focus. Examples include: Chatman and her ethnographic study of janitors ([1987](#cha87)) in which the voices of participants were directly heard; Kuhlthau ([1991](#kuh91)) and her Information Search Process which is inclusive of the thoughts and feelings of information seekers; and Savolainen's ([1993](#sav93)) urging of researchers to use Dervin's ([1992](#der92)) sense-making theory to inform and support a user-centered perspective. A recent study ([Julien _et al._ 2005](#jul05)), however, suggests much research has continued to examine people's information seeking and use from the standpoint of information retrieval from formal information systems.

In order to gauge the extent to which we, the human information behaviour research community, represent the _user_ as truly central in the relationship between scholar and participant, we conducted a content analysis of the reports of human information behaviour research published in the proceedings of the Information Seeking in Context (ISIC) conferences, from their inception in 1996 to the fifth conference in 2004\. The specific research questions addressed were: How do human information behaviour researchers describe participants in reports of empirical research?; and, What strategies do human information behaviour researchers use in their presentation of research participants?

## Literature review

While we were unable to find any relevant literature in library and information science, scholars in sociology and education and others who have written about the process of doing research have addressed the relationship between the scholar and participant, between the researcher and the researched. Most of this writing is situated within the qualitative paradigm and much of the discourse is dominated by the writings of Lincoln and Guba whose foundational work _Naturalistic Inquiry_ ([1985](#lin85)) is cited by scholars in many disciplines including library and information science.

Lincoln and Guba note that one of the questions which is central to accounts of research is "[W]hose reality gets presented?" ([2003: 233](#lin03)). Written reports can reflect the agendas of the researcher, the funding agency, the researched, or some combination of these. As the researcher writes up the work and ultimately has responsibility for the text, the researcher's agenda may influence the tone of the final published work ([Lincoln 1997](#lin97)). This stance is presented as problematic. Feminist researchers advocate the creation of positive, trusting relationships between the researcher and the researched to help ensure researchers do not treat human participants as objects, but instead focus on collaborative relationships in both the doing of the research and the writing of research reports ([Olesen 2005](#ole05)).

The literature suggests some specific strategies that work to empower research participants. Tierney ([1993](#tie93)) suggests that giving voice to human participants ensures they are not silenced or marginalised through the research process. Lincoln and Guba ([2003](#lin03)) maintain that by attributing voice and speech to the researched, we allow participants to retain their dignity through the ownership of their words, thoughts, and ideas. Lincoln and Guba ([1985](#lin85)) also reflect on the words we use when referring to the researched. They suggest that the term _respondents_ is more appropriate than the term _subjects_ which they believe carries the sense of people having things done to them rather than with them. The idea that language communicates on more than one level is also expounded by Flinders who notes that words convey information explicitly, but also carry implicit meanings and information about "relationships and where these relationships stand in terms of power, status, and privilege" ([1992: 112](#fli92)). Flinders suggests we must be mindful, therefore, of the terms we use in describing people.

The literature then argues for open, honest and respectful relationships with the researched and for research reporting that reflects these values. This study set out to determine if this is the case for current human information behaviour research as reflected in recent reports of empirical human information behaviour research involving users.

## Method

To answer our research questions we conducted a content analysis ([Krippendorff 2004](#kri04)) of reports of empirical research involving human participants as published in the proceedings of the biennial Information Seeking in Context (ISIC) conferences held from 1996 to 2004\. While human information behaviour researchers communicate their work through a variety of publications and conferences, as ISIC is one of the premier international venues for dissemination of human information behaviour research, this sample is likely to be highly representative of recent work being completed in the area.

In total, 96 articles were analyzed. These were more or less evenly distributed across the five conferences (1996, n=17; 1998, n=19; 2000, n=20; 2002, n=18; 2004, n=24). The articles were coded for data collection and data analysis method(s), use of theory and parts of the article (title, abstract, introduction and literature search, method, findings, discussion and conclusion) where participants were included. In addition we noted all terms that were used to label research participants, data collection practices and strategies used by authors when presenting the research participants.

A random sample of ten articles was selected and re-coded by an independent coder. This test of inter-coder reliability resulted in an agreement rate of 81%, suggesting that the coding scheme was robust and reliable ([Miles and Huberman 1994](#mil94)).

## Findings

A theoretical framework was reported in 60 (62.5%) of the articles. This rate of theory use is consistent with that found by McKechnie _et al._ ([2001](#mck01)) who reported that 58.9% of the human information behaviour literature employed theory. Theoretical approaches used by authors include Kuhlthau's information search process, Dervin's sense making, Belkin's anomalous states of knowledge, Wilson's model of information-seeking behaviour, Taylor's levels of information need, Savolainen's everyday life information seeking as well as theories developed in disciplines other than but frequently used within library and information science such as actor network theory and role theory. As found in an earlier study ([McKechnie, _et al._ 2002](#mck02)), data collection methods were diverse and included interviews, surveys, observation, focus group interviews, content and document analysis, transaction log analysis, think-aloud protocols, discourse analysis, diary keeping and experiments. The use of more than one method was evident in 60 (62.5%) of the articles. Only 70 (72.9%) authors described their data analysis techniques, representative examples of which include thematic analysis, phenomenographic analysis, grounded theory and statistical (both descriptive and inferential) analysis. Terms to describe users/participants were included in titles (n=45; 46.9%), abstracts (n=60; 84.5% of the 71 articles with abstracts), introduction/literature search (n=80; 83.3%), method (n=91; 94.8%), results (n=92; 95.8%) and discussion (n=76; 79.2%) sections of the papers.

### Terms for users or research participants

As presented in Table 1, four major categories of terms were used by authors when referring to individuals who participated in their studies.

<table><caption>

**Table 1: Categories of terms used by human information behaviour researchers**</caption>

<tbody>

<tr>

<th>Terms</th>

<th>1996  
(n=17)</th>

<th>1998  
(n=19)</th>

<th>2000  
(n=20)</th>

<th>2002  
(n=18)</th>

<th>2004  
(n=22)</th>

<th>Total  
(n=96)</th>

</tr>

<tr>

<td>Quasi socio-demographic</td>

<td>9  
(52.9%)</td>

<td>10  
(52.6%)</td>

<td>15  
(75.0%)</td>

<td>8  
(44.4%)</td>

<td>6  
(27.3%)</td>

<td>48  
(50.0%)</td>

</tr>

<tr>

<td>Information behaviour</td>

<td>5  
(29.4%)</td>

<td>9  
(47.4%)</td>

<td>9  
(45.0%)</td>

<td>4  
(22.2%)</td>

<td>8  
(36.4%)</td>

<td>35  
(36.5%)</td>

</tr>

<tr>

<td>Life roles</td>

<td>14  
(82.3%)</td>

<td>15  
(78.9%)</td>

<td>20  
(100%)</td>

<td>12  
(66.7%)</td>

<td>19  
(86.4%)</td>

<td>80  
(83.3%)</td>

</tr>

<tr>

<td>Research project roles</td>

<td>14  
(82.3%)</td>

<td>11  
(57.9%)</td>

<td>12  
(60.0%)</td>

<td>15  
(83.3%)</td>

<td>18  
(81.8%)</td>

<td>70  
(72.9%)</td>

</tr>

</tbody>

</table>

Researchers sometimes (n=48 articles; 50.0%) used words like individual, people, persons, females, males, children, older adults, families and ordinary people that described research participants in terms of general, quasi socio-demographic characteristics. A second approach was to use labels associated with information behaviour (n=35; 36.5%) such as end-user, inquirer, information seeker or information user. Thirdly, participants were frequently (n=80; 83.3%) identified in terms of their everyday life, work or educational roles, categories that are commonly of interest to human information behaviour researchers as contexts for information behaviour. A wide range of roles is represented in these studies including mothers, elementary school students, graduate students, professors, municipal administrators, journalists, engineers, and lawyers. Finally and most relevant for this project, individuals were also named in terms of their role within the research project (n=70; 72.9%).

As presented in Table 2, in the 70 papers using terms related to roles in research studies, the following research study role descriptors were used: experimental group, informant(s), interview partner, interviewee(s), participant(s), respondent(s), and subject(s).

<table><caption>

**Table 2\. Terms used by human information behaviour researchers to describe research project roles**</caption>

<tbody>

<tr>

<th>Project roles</th>

<th>1996  
(n=17)</th>

<th>1998  
(n=19)</th>

<th>2000  
(n=20)</th>

<th>2002  
(n=18)</th>

<th>2004  
(n=22)</th>

<th>Total  
(n=96)</th>

</tr>

<tr>

<td>Experimental group</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1  
(1.4%)</td>

</tr>

<tr>

<td>Informant(s)</td>

<td>1</td>

<td>1</td>

<td>3</td>

<td>1</td>

<td>2</td>

<td>8  
(11.4%)</td>

</tr>

<tr>

<td>Interview partner</td>

<td>1</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>0</td>

<td>1  
(1.4%)</td>

</tr>

<tr>

<td>Interviewee(s)</td>

<td>1</td>

<td>3</td>

<td>2</td>

<td>3</td>

<td>3</td>

<td>12  
(17.1%)</td>

</tr>

<tr>

<td>Participant(s)</td>

<td>7</td>

<td>0</td>

<td>8</td>

<td>9</td>

<td>11</td>

<td>35  
(50.0%)</td>

</tr>

<tr>

<td>Respondent(s)</td>

<td>6</td>

<td>6</td>

<td>4</td>

<td>4</td>

<td>6</td>

<td>26  
(37.1%)</td>

</tr>

<tr>

<td>Subject(s)</td>

<td>4</td>

<td>2</td>

<td>1</td>

<td>3</td>

<td>3</td>

<td>13  
(18.6%)</td>

</tr>

</tbody>

</table>

We note that these terms have entailments which represent the user as having variant degrees of autonomy and centrality in the research process. As noted in the literature review, Lincoln and Guba ([1985](#lin85)) suggest that _subject_ carries the connotation of manipulation and as such does not construct the user as central and autonomous. _Respondents_ and _interviewees_ may be construed as having more control in that, although the agenda (for example, an interview schedule) is presumably created by the researcher, _respondents_ and _interviewees_ (at least in the case, for example, of open-ended and to a lesser extent semi-structured interviews) are free to answer as they deem fit. The term _informant_ (for example, in the case of key informants in naturalistic observation and ethnography) carries with it the sense of the individual determining just what will be shared: it is the _informant_ who is in charge and who informs the researcher. Finally, the terms _partner_ and _participant_, may perhaps be regarded as the most inclusive of all as they construct the individual as a member of the research team and an active player in the research process.

Twenty-two authors (22.9%) used more than one research study role descriptor in their research reports. Some authors (n=26 / 27.1%) used no research role descriptors, but instead referred to people in terms of their everyday life, work or academic contexts.

### Data collection practices

The important role of the user or research participant was evident in the method section of some of the papers. Some data collection practices reported by authors were designed to bring, and were effective at bringing, researchers closer to users and capturing their perspectives. These included open-ended interviews, face-to-face interviews, close interaction over an extended period of time, audio-recording of interviews, full transcription of audio-recorded interviews and participant checking. Conversely, data collection practices such as transaction log analysis or the use of secondary survey data served to distance the researcher from the researched. While rare, we found that some authors actively reflected on the treatment and/or inclusion of human subjects.

### Other strategies used by authors to represent users or research participants

Authors used a variety of means to describe the roles and experiences of the people who took part in their studies. Some practices were more effective and others less effective in giving voice to participants. The following summarizes the strategies we discovered in our analysis of the research reports. Our intent is not to draw attention to _good_" or _bad_ authors but to deconstruct and to allow us to understand more clearly our discursive practices and their implications.

Strategies used by authors that did not position the research participant as central included:

*   Using summary narrative descriptions rather than participants' voices to present research results. When used without direct quotations, summary statements may serve to silence research participants through generalisation.
*   Using summary statistics alone rather than with participants' voices to present research results.
*   Using models, pie charts and graphs alone rather than with participants' voices to present research results.
*   Using participant statements with no identifiers.
*   Using letter and/or number identifiers when presenting participants' statements.

Strategies used by authors that positioned research participants as central included:

*   Using participants' voices in the presentation of research results. Some authors included verbatim quotations from their participants in their research reports. These varied in length from a few words interspersed in sentences to full paragraphs.
*   Using pseudonyms as identifiers when presenting participants' statements. This served to reinforce the idea that participants are real people with real names represented through pseudonyms.
*   Adding brief socio-demographic characteristics to identifiers which served to make participants more real to readers.
*   Using tables to present responses from individual participants.
*   Describing each participant through brief narrative statements, profiles or tables of socio-demographic characteristics.
*   Providing fuller, more explicit discussion of one or a few individual participants or cases which has the effect of making them more central to the research.
*   Explicitly thanking research participants for their contribution to the study. Of the 96 articles we analysed, 29 (30.2%) included a statement of acknowledgement. Authors thanked granting agencies, colleagues, supervisors, research assistants, referees, secondary data sources and, in one case, a journal editor - all components of our formal research infrastructure. Research participants were only acknowledged in 13 of the 29 papers.

It is important to note that strategies that constructed the user as central to the research were not exclusively associated with particular methods. Our data includes two experiments where the researcher used _participant_ to describe users and at least one study where _subjects_ took part in indepth, open-ended interviews.

## Discussion

Our findings suggest that while discursive practices of many human information behaviour researchers acknowledge the central role of participants, as a scholarly community we need to become more conscious of this responsibility. We often use labels for research participants without attending to the entailments associated with these terms. We do not believe that human information behaviour researchers intentionally denigrate users through the terms used in reporting our research. Our scholarly writing is, for example, influenced by a number of other variables such as our research training, instructions to authors from publishers, and literary concerns.

There are important implications of these findings for how we train doctoral students and other researchers, and how the writing process is discussed in our research methods texts. The centrality of the user to human information behaviour research is clear. We need to devote much greater attention to the value of deep reflection about the relationship between researchers and research participants, and about how those relationships become constructed and interpreted through the writing and presentation processes.

## Acknowledgements

A special thank you to all the individuals who participated in the studies and the authors of the articles that constituted the sample for this research project.

## References

*   <a id="cha87"></a>Chatman, E.A. (1987). The information world of low-skilled workers. _Library & Information Science Research_, **9**(4), 265-283.
*   <a id="der92"></a>Dervin, B. (1992). From the mind's eye of the user: the sense-making qualitative-quantitative methodology. In J.D. Glazer & R.R. Powell (Eds.), _Qualitative research in information management_ (pp. 61-84), Englewood, CO: Libraries Unlimited.
*   <a id="der86"></a>Dervin, B. & Nilan, M. (1986). Information needs and uses. _Annual Review of Information Science and Technology_, **21**, 1-25.
*   <a id="fli92"></a>Flinders, D. (1992). In search of ethical guidelines: constructing a basis for dialogue. _Qualitative Studies in Education_, **5**(2), 101-115.
*   <a id="jul99b"></a>Julien, H. (1999b). Constructing "user"s in library and information science. _Aslib Proceedings_, **51**(6), 206-209.
*   <a id="jul99a"></a>Julien, H. (1999a). Where to from here? Results of an empirical study and user-centred implications for system design. In T.D. Wilson & D.K. Allen, (Eds.), _Exploring the contexts of iInformation behaviour_ (pp. 586-596). London: Taylor Graham.
*   <a id="jul05"></a>Julien, H., McKechnie, L., & Hart, S. (2005). A content analysis of affective issues in library and information science systems work. _Library & Information Science Research_, **27**(4), 453-466.
*   <a id="kri04"></a>Krippendorff, K. (2004). _Content analysis: an introduction to its methodology_. (2nd ed.). Thousand Oaks, CA: Sage.
*   <a id="kuh91"></a>Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   <a id="lin97"></a>Lincoln, Y.S. (1997). Self, subject, audience and text: living at the edge, writing in the margins. In W.G. Tierney & Y.S. Lincoln (Eds.), _Representation and text: re-framing the narrative voice_ (pp. 37-55). Albany, NY: State University of New York Press.
*   <a id="lin03"></a>Lincoln, Y.S., & Guba, E.G. (2003). Ethics: the failure of positivist science. In Y.S. Lincoln & N.K. Denzin (Eds.), _Turning points in qualitative research: tying knots in a handkerchief_ (pp. 219-238). Walnut Creek, CA: AltaMira Press.
*   <a id="lin85"></a>Lincoln, Y.S., & Guba, E.G. (1985). _Naturalistic inquiry_. Beverly Hills, CA: Sage.
*   <a id="mck01"></a>McKechnie, L., Pettigrew, K. & Joyce, S. (2001). The origins and contextual use of theory in human information behaviour research. _New Review of Information Behaviour Research_, **2**, 47-63.
*   <a id="mck02"></a>McKechnie, L., Baker, L, Greenwood, M. & Julien, H. (2002). Research method trends in human information literature. _New Review of Information Behaviour Research_, **3**, 113-126.
*   <a id="mil94"></a>Miles, M.B. & Huberman, A.M. (1994). _Qualitative data analysis: an expanded sourcebook_. (2nd ed.). Thousand Oaks, CA: Sage.
*   <a id="ole05"></a>Olesen, V. (2005). Early millennial feminist qualitative research: challenges and contours. In N.K. Denzin & Y.S. Lincoln (Eds.), _The Sage handbook of qualitative research_ (pp.235-278). Thousand Oaks, CA: Sage Publications.
*   <a id="sav93"></a>Savolainen, R. (1993). The sense-making theory: reviewing the interests of a user-centered approach to information seeking and use. _Information Processing & Management_, **29**(1), 13-28.
*   <a id="tie93"></a>Tierney, W.G. (1993). Introduction: developing archives of resistance: speak, memory. In D. McLaughlin & W.G. Tierney (Eds.), _Naming silenced lives: personal narratives and processes of educational change_ (pp.1-8). New York, NY: Routledge.
*   <a id="tuo97"></a>Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. _The Library Quarterly_, **67**(4), 350-371.
*   <a id="wil81"></a>Wilson, T.D. (1981). On user studies and information needs. _Journal of Documentation_, **37**(1), 3-15.