#### Vol. 12 No. 1, October 2006

# The information activity of rail passenger information staff: a foundation for information system requirements

#### [Martin Rose](mailto:mnrose@yahoo.com)  
Information & Knowledge Management Program, University of Technology, Sydney, Australia

#### Abstract

> **Introduction**. This paper examines the goal-directed information activity of passenger information staff, working in the dynamic environment of rail network control. The explicit aim is to define a meaningful set of information system requirements. The report shows how dynamic situations may lead us to question a number of established theories of information science.  
> **Method**. Passenger information officers (PIOs) were observed on duty within the rail command and control headquarters. Observation sessions totally eight hours involved the manual recording of sequential information flows and the associated activity of PIOs. A semi-structured management interview was also conducted to provide further insight into the organizational context.  
> **Analysis**. A viewpoint-oriented analysis technique was used to analyse sequential data captured during observation sessions. Event sequences that represent and explain the viewpoints were identified and elaborated into detailed scenario descriptions.  
> **Results**. The analysis both supports and contests a number of established theories from information science. Additionally, a range of 'mandatory' and 'desirable' system requirements are derived from the scenario and viewpoint analyses.  
> **Conclusion**. Dynamic situations have a significant impact on information behaviour which is not always predicted by current theories of information science.

## Introduction

The information behaviour field is rich and expansive, but, despite free ranging possibilities ([Soergel 1997](#soe97)), researchers have failed to push the boundaries of information behaviour in some key areas. This paper attempts to address two of them. The first is the boundary between information science and information systems. With a common interest in information users, one might expect to find examples of collaboration between the information science and information systems communities. Instead we find a major disconnect ([Ellis _et al._ 1999](#el99); [Pettigrew _et al._ 2001](#pet01)). The second area is that of dynamic group work situations. Dynamic environments are particularly interesting because they cause us to challenge some of our established thinking about information behaviour. This is due in large part to the _multiple stringent constraints_, ([Sonnenwald and Pierce 2000:](#son00) 462) under which activities occur. However, very few empirical attempts have been made to study information behaviour in dynamic situations. In a rare foray, Sonnenwald and Pierce ([2000](#son00)) studied the military command and control context. The authors focus mainly on the information behaviour that underpins successful group functioning. They do not use their findings to make system design recommendations, even though military command and control is an area where support systems are increasingly deployed.

The research reported here addresses the work-related information behaviour of passenger information staff in an urban rail control context. I describe the dynamic activity of Passenger Information Officers (PIOs) as they respond to disruptions on the rail network. Their activity shifts from monitoring and scanning for emerging problems, to information seeking when service disruptions are found, and to information use in the process of communicating service status to commuters and staff. Through a focus on information behaviour, I identify a number of key requirements for an information system that might support the work of PIOs.

A key consideration in passenger information work is the dynamic environment in which it takes place. The research findings reveal how time pressure, information shortage and pervasive uncertainty faced by PIOs have a significant effect on information behaviour. This may cause us to modify some key theories, including Kuhlthau's ([1993](#kuh93)) uncertainty principle, and the findings of [Byström and Järvelin (1995)](#by95) concerning the relationships between task complexity and source usage.

It is researchers outside of information science who appear to have best grappled with the complexities of dynamic situations.Engeström and Middleton's ([1996](#eng96)) collection of microsociological studies, _Cognition and communication at work_, provides enormous insight into group functioning in a range of work settings, as do the more in-depth works of Hutchins ([1995](#hut95)) and Suchman ([1987](#suc87)). Largely drawing on participant observation, these reports examine conversation, artefacts, action, cognition, collaboration, expertise and learning, producing vivid ethnographic descriptions. While the focus is not information behaviour _per se_ (or information systems),it is clear from these writings that dynamic work activity is fertile ground for information behaviour research (and a rich source of system requirements).

The information systems community has used similar ethnographic methods to study work environments and to address one of the chief problems of systems design: the establishment of clear system requirements. In order to successfully define what a system should do, and how it should be operated, designers must arrive at a clear and comprehensive understanding of information behaviour within the problem domain. Specifically they must describe how communication is integrated with social action ([Sutcliffe 1997](#sut97); [Malmsjö 1997](#mal97)). This is a domain where information behaviour research has much to offer, and one in which such research is sorely lacking.

Wilson defines information behaviour as 'the totality of human behaviour in relation to sources and channels of information, including both active and passive information seeking, and information use', ([Wilson 2000](#wil00): 49). This provides a broadest possible view of the relationship between information and people. Within information behaviour, Wilson refers to information seeking behaviour, information search behaviour, and information use behaviour. However these terms, individually, are too narrow to describe the potential scope of behaviour that may be supported by an information system.

Information systems designers may be concerned with any or all aspects of active information behaviour. Systems are not merely limited to supporting information seeking behaviour, but may also support the identification of information needs, the use of information, information exchange and so on. Determining what behaviour a system _should_ support is very much dependent upon the activity of its intended users. And this requires us to describe that activity in a way that might directly inform design.

By viewing information behaviour through the lens of requirements engineering, researchers are compelled to narrow their focus on the situated human behaviour associated with a specific set of goal-directed activities. Hence, _information activity_ becomes the focal point of system analysis and design. I will define information activity as goal-directed human behaviour in relation to sources and channels of information.

The information activity of PIOs takes a number of forms:

*   _Monitoring network status_. Scanning the status of train services for new and changing situations, using a range of human and computer-based information sources.
*   _Investigating service disruptions_. Recognising an emerging service disruption situation, and investigating, using appropriate information sources to establish basic information about cause and impact.
*   _Communicating situation status_. Determining, first, whether communication is warranted, then formulating and delivering a situation-specific or general network status message to inform passengers or staff.
*   _Relaying information to operational staff_. Dealing with incoming communications from a variety of sources, and relaying information as necessary to those who need to know.

### Research context

The research in this paper was conducted in the central control room of a major Australian passenger rail service provider (Figure 1). The Rail Management Centre (RMC) controls the movement of trains across the metropolitan rail network, and is responsible for monitoring or coordinating around 2,500 train movements a day. Train services are continually impacted by unplanned events such as mechanical failures, crew problems, security incidents, and infrastructural issues. Train Control staff in the RMC intervene regularly in services to ensure that trains run as close to schedule as possible. Interventions may involve (among other things) running additional services, changing the stopping patterns of existing services, or cancelling trains altogether. Throughout the day, passengers and staff must be kept informed of service changes, and overall network status. Responsibility for this falls in part on the Passenger Information function.

Passenger Information Officers work in the RMC as part of a team of thirty operational control staff. The RMC is designed in a similar way to many other operational control environments, with a tiered floor, sloping towards a large monitor system, and control staff arranged in rows that face 'down-slope', towards the monitors. Two PIOs sit at the rear of the room, beside the RMC Shift Manager. From this vantage point they can see and hear the rest of the Centre. PIOs interact closely with each other, with the Shift Manager on duty, and with the various Train Controllers looking after different operational areas.

<figure>

![Figure 1: View of the RMC from the Shift Manager's desk (immediately to the right of PIO desks)](../p275fig1.jpg)

<figcaption>

**Figure 1: View of the RMC from the Shift Manager's desk (immediately to the right of PIO desks)**</figcaption>

</figure>

## Research method

Dynamic situations pose methodological challenges. Within the information systems field, researchers have looked to sociological approaches, exploring the use of ethnographic techniques as a means of overcoming the limitations of interviews, task analysis and cognitive modelling ([Crabtree _et al._ 1999](#cra99); [Fields and Wright, 2000](#fie00); [Suchman _et al._ 1999](#suc99)). A number of researchers have modified ethnographic methods to focus them more on the outcomes required by the systems design process. They have done so in two key ways:

1.  By making pragmatic changes and applying shortcuts to ethnographic approaches ([Ball and Ormerod, 2000](#bal00)).
2.  By adapting software modelling languages to account for social aspects of work practice ([Viller and Sommerville, 2000](#vil00)).

In this study two key research methods are used, non-participant observation and interviews, as part of an ethnographically-informed methodology to describe information activity in a dynamic and socially integrated work environment. I employ elements from two existing methodologies developed in the information systems field: Beyer and Holtzblatt's _Contextual Inquiry_, and Viller and Sommerville's _Coherence_ approach.

Contextual Inquiry forms the data gathering stage of a broader _Contextual Design_ methodology ([Beyer and Holtzblatt 1998](#bey98)). It is based on participant observation using a master and apprentice relationship model, where learning occurs through demonstration and doing, and observation is interspersed with discussion. The full methodology, which is not followed here, flows through into user interface design and prototyping. Also, as a non-participant observer of passenger information work, the master and apprentice relationship model is not fully adopted for this research.

Coherence ([Viller and Sommerville, 2000](#vil00)) is a method for analysing socially embedded information system requirements in dynamic work settings, using observation and interviews. Data are represented in standard software notation called the Unified Modelling Language or UML. Coherence applies the concept of _viewpoints_ which are explicitly defined, to prompt the observer about key aspects of the work activity. Viewpoints are a 'partial analysis of the workplace, as seen from a particular perspective or focus. The complete analysis of the workplace is obtained through integrating and reconciling the multiple viewpoint analyses', ([Viller and Sommerville 2000](#vil00): 173).

While both methods involve observation in the workplace and concern themselves with systems modelling, each was judged to have particular strengths that could be taken advantage of, and weaknesses that might be mitigated if they brought together. The strength of Contextual Inquiry is in the observation stages, where a particular relationship model is recommended and guidelines provided for structuring the data gathering process. However it offers no structured or standardised means of representing data about sequential information flows. Coherence makes up for this deficiency by utilising a modified UML notation. It also strengthens the data analysis stages through the use of _viewpoints_. The approach used here can be considered a hybrid of Contextual Inquiry (for data gathering) and Coherence (for analysis).

### Observation

I observed two PIOs on duty in the RMC as they performed their normal work activities. Observations were spread over four sessions of approximately two hours each, occurring over a period of about one month. I observed different informants, or different combinations of the same informants, in each sitting. The sessions took place during the afternoon peak travel period. This is usually the time during which service disruptions are the most significant.

Audio or video recording was not authorised, due to the sensitive nature of the work environment, so all data had to be gathered through the use of worksheets, developed after an introductory site visit ( [see Appendix 1](#app1) ).

During the observation sessions, I recorded the information activities of PIOs and the sequential flow of information into and out of the passenger information team. This involved capturing the sources and channels of individual information flows and representing the content of each flow, either as a verbatim or paraphrased quotation, or as a summary description of an action. I also captured the full text of all communication messages issued through pager, fax or Website. Where informants' actions were ambiguous, and where a suitable, non-interruptive opportunity was available, I questioned PIOs about their actions.

### Interview

Apart from observation sessions, I also interviewed the manager responsible for the passenger information function, to identify and elaborate the key organizational concerns. The interview used a semi-structured format ([see Appendix 2](#app2)). I began with questions about the purpose and value of the passenger information function to the organization, and its relationship to other functions that provide operational information. I moved on to questions about the problems faced by PIOs and the impact on their work. Questions then covered the mechanisms used for assessing information and communication quality, and the strategies planned by management to improve quality. The interview was approximately one hour in length. A combination of note taking and audio-recording was used. Tapes were later transcribed.

### Data analysis

Different viewpoints were defined and evolved using both the interview transcription and observation sessions as input. I also adopted three viewpoints recommended by Viller and Sommerville ([2000](#vil00)) to capture information about the social aspects of work. Observation supported the identification of additional viewpoints.

Using these viewpoints to provide structure and focus to the analysis process, I reviewed the sequential data captured during contextual inquiry sessions. Through this process I identified event sequences that had some potential to represent and explain the viewpoints. Key sequences were elaborated into greater levels of detail, and viewpoints used to help interpret actions and focus attention on the pertinent aspects of information behaviour.

The viewpoint and scenario analyses were then re-analysed in order to identify realistic opportunities for information system support. I derived a set of system requirement statements, describing the features of a hypothetical system for supporting passenger information work. I also compared the findings with a number of relevant information science theories to assess their applicability in a dynamic work situation.

## Scenario analysis

The sequence of events shown in Figure 2 illustrates the non-linear nature of PIO information activity, which is accentuated by the general uncertainty of operational control situations. In this scenario, incomplete or insufficient information heightens the uncertainty of the actors. In response, PIOs monitor, investigate and construct messages in close, iterative cycles, rather than linear steps, as they forage for explanations and piece together a satisfactory picture of events.

The scenario touches on some of the basic challenges of Passenger Information work. Information sources will often yield only fragments of the actual situation, and uncertainty is the norm. The job of the PIO is to piece together these fragments, answer key questions about the scale and cause of a service disruption, and communicate coherently with those affected.

<figure>

![Figure 2: Dealing with uncertainty](../p275fig2.jpg)

<figcaption>

**Figure 2: Dealing with uncertainty**</figcaption>

</figure>

Initiating this sequence of events, the attention of PIO-2 is drawn to a single late train on the Train Location System (TLS) monitor. Noting his concern out loud: 'The W566 is late...', he turns to asks the Shift Manager, who sits in the next seat, 'What's happening with the W566?' The Shift Manager is unable to provide a reason on the spot, but picks up the phone to make a call. The conversation happens out of earshot.

When Train Controllers or the Shift Manager are unable to provide the missing fragments, as in this scenario, PIOs are unable to construct a satisfactory message to communicate to the affected parties. As a consequence, PIOs hold back from communicating service disruptions, in the hope that key information will come to light. The benefits of speedy communication here may be outweighed by the risks of pre-empting events and broadcasting unreliable or incomplete information.

Meanwhile PIO-1 has heard (from an unknown source) that the 133 from Blacktown is delayed with door problems. He informs PIO-2\. Both PIOs discuss how they should communicate this information, using the act of constructing the message text itself to focus their analysis of the situation. PIO-1 is due to go on air, on public radio, to provide a status update in the next few minutes, and wants to be sure he has complete and up-to-date information. Some of the radio show hosts enjoy putting PIOs on the spot about service reliability.

Intense discussion over message content appears to reflect the uncertainty of the situation itself, and trepidation about the impending radio announcement. The debate acts as both a sense-making tool for PIOs themselves and a quality control mechanism. The message is collaboratively constructed, and through this process, PIOs are able to qualify their own tentative interpretations of events. The outcome is a common understanding of the situation.

A second delay on the same line prompts a broader question from PIO-1 to the Shift Manager: 'What's happening with the Western Line?'. Information sources indicate that the same situation may be affecting several services, but no clear answer is forthcoming. The lack of causal information appears to make the PIOs uneasy. PIO-1 reviews the overall status of the Western Line on the TLS monitors, but appears to find no explanations. Display systems like TLS may draw attention to delayed trains and patterns of disruption, but they do not offer any explanations. Human actors are often the PIO's only reliable and up-to-date source of such detail.

He discusses the message content further with his colleague. The uncertainty is not fully resolved, but the need for some form of status bulletin compels PIO-1 to record an update to the call centre Interactive Voice Response (IVR) system. Passengers can ring the call centre and access this recorded message to find out if any delays are likely to affect them.

While in this scenario, the need for timeliness prevails, there is a basic conflict in passenger information work between speed of communication and reliability of message content. Our next scenario, in Figure 3, demonstrates that sometimes, in a dynamic train control environment, incoming operational information may not be accurate at first, and normal validation mechanisms may not uncover problems. When this is passed on to customers, the result can be communications that do not reflect what is actually happening on the ground.

<figure>

![Figure 3: Speed versus reliability](../p275fig3.jpg)

<figcaption>

**Figure 3: Speed versus reliability**</figcaption>

</figure>

Triggering this sequence of events, a Train Controller alerts other staff with an out-loud statement: 'The 79 is cancelled: North Sydney to Richmond'. PIO-1 becomes suddenly animated, calling out for clarification: 'What's the reason?'. 'Faulty working: shunter' replies the train controller, using operation terminology. A shunting error has prevented the 79 from beginning its journey.

By virtue of their direct involvement in service interventions, train controllers provide a key source of real-time operational status information, and PIOs often use them to validate information they receive through other channels. Because this information comes from a train controller, it is accepted without question and acted upon.

On hearing of the cancellation, the usual flurry of activity ensues in the passenger information group. PIO-1 sends a pager message informing stations on the North Shore and Western lines about the cancellation, and calls the Transport Infoline to ensure call centre operators are informed.

But as this scenario illustrates, in dynamic operational situations, the first information, even from train controllers, may not be reliable. The 79 is initially cancelled. But the train controller subsequently makes arrangements to start the trip several stations into its scheduled journey. The PIOs are not informed directly of this intervention, and continue to act in the belief they are dealing with a cancellation.

PIO-2 posts a cancellation notice to the Web, but appears to doubt the original information provided by the Train Controller, possibly when another information source provides conflicting information. His validation tactic is to triangulate sources; i.e., to query another source, to either confirm or dispute the original. He speaks with an unknown person over the phone. Meanwhile the Shift Manager announces 'the 79 is running', but the full details about the trip starting at a different station are still not communicated. PIO-1 has already picked up the phone to record an IVR message, and proceeds with the update. It is not known whether his recording contains information about the 79 or not.

Eventually PIO-2 learns (from an unknown source) that the 79 is actually running out of Wynyard station, and not out of North Sydney as scheduled. He quickly announces this qualifying information. PIO-1 responds: 'So it's still cancelled out of North Sydney'. The statement salvages some truth from the original communication messages, sent out to inform passengers and staff of the cancellation. But by the time the full story has emerged, some three communication messages have been issued which do not fully reflect the situation on the ground, and corrective action is required.

## Discussion

The passenger information function is recognised as one that would benefit from better information system support and an increased level of automation. However, a system designed to support the information activity of PIOs would need to integrate well with the dynamic work practices described earlier, and should mitigate where possible the weaknesses inherent in the current environment. The scenario analysis points to a range of requirements that should be considered as mandatory or desirable for the successful deployment of a passenger information support system. Requirements are highlighted throughout this section, and marked M (mandatory) or D (desirable). While the requirements have not been subject to technical validation, they are considered by the author, based on experience, as realistic and achievable using current technology.

The requirements defined in this section are derived from a viewpoint analysis. For the sake of brevity, I use only three of five viewpoints identified during the research, including two out of three social viewpoints recommended as part of the Coherence methodology ([Viller and Sommerville 2000](#vil00)). The selected viewpoints are as follows:

*   _PIO information activity_. This is a domain-specific viewpoint focussing on the actions performed by PIOs in the RMC to meet the information needs of staff and passengers. This viewpoint highlights the actions and interactions involved in PIO work.
*   _Awareness of work_. This is a social viewpoint adopted from the Coherence methodology. It is concerned with how work is organized and executed in a way that provides others with the required awareness. It highlights the tactics employed consciously and otherwise to make work visible to other staff members and to gain visibility of the work of others.
*   _Distributed coordination_. Another social viewpoint adopted from Coherence considers the coordination of people, resources and tasks to sustain interrelated work processes. It calls attention to the way labour is divided and allocated among actors and how different roles and responsibilities are reconciled through work activity.

### PIO information activity viewpoint

The response of PIOs to observed service disruptions usually follows a similar pattern:

1.  One or both PIOs are alerted to a situation of interest by a human actor, either internal or external to the organization, or by observing one of the display systems in the RMC.
2.  The PIO who receives the information verbally alerts the other PIO on duty.
3.  If the information did not originate from a primary operational source—i.e., a train controller or the RMC shift manager—the recipient PIO verifies the information with one of these primary sources.
4.  The PIO investigates further, if necessary, to establish basic cause and impact, utilising a range of sources (including people, documents, display systems and databases). Human actors, and in particular train controllers and the shift manager, are used most often. New information is passed on immediately to the other PIO.
5.  Where a range of different communication activities are required to deal with a particular disruption, PIOs may divide tasks up among themselves.
6.  PIOs construct and publish messages to impacted parties using the available communication channels. Staff are targeted by location and role (e.g., all guards on the Illawarra Line). More general messages targeting passengers are published to public information channels.

Passenger information tasks are simple, and they require simple factual information in order to 'fill in the blanks' about service disruptions. The problem is well structured and PIOs know what information is needed. This is consistent with Vakkari's ([1999](#vak99)) theory of task complexity, prior knowledge and information actions. It is also consistent with Byström and Järvelin's ([1995](#by95)) findings that simple tasks can generally be completed using simple factual information. However it is inconsistent with their correlation between simple tasks and the use of specific factual sources such as documents and registers. In the passenger information context, source usage is clearly influenced by the dynamic nature of the work setting, where recorded information about emergent service disruptions is rarely available. PIOs, therefore rely on human sources; behaviour that is more consistent with complex tasks.

> **System requirements:**
> 
> *   Provide a mechanism for cumulatively capturing information about service disruptions, e.g., type, location, cause, impact (M).
> *   Allow distributed users to proactively alert PIOs to new disruption situations (D).
> *   Allow a number of different communication tasks, related to different audiences or different channels, to be created in the system for any one service disruption (M).
> *   For efficiency, automatically create message pro-formas, based on the captured information, and provide PIOs with the option of using, modifying or discarding these templates (D).
> *   Automatically determine which messages should be sent to which users based on location, role and message type (D).

Passenger information work rarely occurs as a discrete or linear sequence of events. Monitoring, investigation and message construction are often indistinct activities, occurring in iterative cycles and this is heightened when PIOs are highly uncertain about the causes and impacts of service disruptions. Passenger information tasks are frequently interrupted and PIOs often need to carry out more than one task in parallel.

Amid this turbulent activity, PIOs rarely emerge completely from a state of uncertainty. Instead of the linear process of uncertainty resolution described by Kuhlthau ([1993](#kuh93)), we find a _fluctuation in the level of uncertainty_ faced by PIOs, and this is reflected in their iterative information foraging. Pragmatic decisions to inform passengers or staff of disruption situations are made when the need for timely action outweighs the misgivings of PIOs over information quality. Furthermore, uncertainty is experienced during a post-focus stage of problem construction when, according to Kuhlthau's theory, uncertainty should be reduced and users should be feeling confident, thinking clearly and acting conclusively. There is no pre-focus stage in the passenger information context, as tasks are simple and well-understood. Again, the dynamic situation is likely to be the key factor. Limited availability of information, its low reliability, and time pressure combine to fuel uncertainty.

> **System Requirements:**
> 
> *   Provide support for work processes that do not follow a set sequence, ie the system should not enforce linearity (M).
> *   Provide specific support for iterative modification of service disruption information captured by the system (M).
> *   Allow users to commence and work on a number of separate tasks in parallel (M).
> *   Allow users to move between active tasks quickly, without losing any work already completed (M).

### Awareness of work viewpoint

PIOs interact closely during service disruptions, making considerable effort to keep each other informed of unfolding events. Most information is shared immediately and reflexively. The degree of interaction that occurs between PIOs appears to reflect their level of uncertainty about the situation at hand. By debating the content of communication messages, PIOs make better sense of the situation and achieve a common understanding before sending messages.

PIOs usually relay all new information out loud. Intended actions are usually pre-announced, and the content of any communications made by one PIO is often recounted to the other PIO on duty, either before or after it is sent.

Cross functional awareness of work is also important. RMC staff in general appear to freely volunteer new information, announcing it out loud on discovery, and this often triggers action among PIOs. This is consistent with the findings of Sonnenwald and Pierce ([2000](#son00)), who identify interwoven situation awareness and dense social networks as key success factors in military command and control. When this breaks down, problems occur.

In the rail control environment, situations may change without PIOs being informed. Incomplete visibility of train control interventions is a particular concern. PIOs spend a lot of time querying train controllers for information and even 'hang around' them to pick up key pieces of information. This is inherently unreliable, and when failures occur, this can have flow-on effects on the quality of communication from PIOs.

> **System Requirements:**
> 
> *   Provide a collaborative data capture environment whereby PIOs and distributed users may submit and update information about service disruptions as it is discovered. Alert PIOs to all new information as it comes in (D).
> *   Provide PIOs with full mutual visibility of each other's tasks (M).
> *   Provide options for PIOs to collaboratively develop message content (M).
> *   Permanently display the real-time service disruption information held in the system to train controllers and the shift manager. This will provide an opportunity for these parties to correct the data or inform PIOs about inaccuracies (D).
> *   Provide a simple mechanism for logging the service intervention decisions made by train controllers, e.g., using a touch screen (M).
> *   Automatically alert PIOs about specific intervention decisions made by train controllers (M).
> *   For efficiency, provide PIOs with the option to create new service disruption records in the system, based on the intervention data provided by train controllers (D).
> *   Allows users to easily retract or issue corrections to messages already sent about service disruptions (M).

### Distributed coordination viewpoint

Passenger Information coordination is apparent on two levels: within the Passenger Information function itself, and between PIOs and other functions that supply passengers and staff with operational information.

Reflecting the dynamic environment in which they operate, PIOs share responsibilities and tasks flexibly. Work is allocated between PIOs almost automatically, in response to criteria that may vary according to the particular situation, or to the particular PIOs on duty. Division of work appears to be aimed at achieving parallel information processing within the team, and to increase the speed and consistency with which communication channels are covered.

Part of the passenger communication function is fulfilled by the Transport Infoline call centre. As the key point of contact for the Infoline on the day of operation, PIOs are engaged in frequent conversation with call centre representatives. Each party provides the other with alerts about new incidents or service disruptions. The relationship can be viewed as mutually coordinating.

> **System requirements:**
> 
> *   Allow communication tasks related to different audiences or different channels to be separately allocated to different PIOs (M).
> *   Allow PIOs to freely allocate and reallocate communication tasks between each other as situations change, without losing any of the work completed (M).
> *   Provide PIOs with an indication of the level of workload allocated to team members, to support work optimisation (D).
> *   Provide external parties, such as the Transport Infoline, with the ability to submit and receive alerts (D).

## Conclusion

The dynamic environment of passenger information work is characterised by information scarcity. Information that is available is itself unreliable. This creates an environment of high uncertainty, despite the simplicity of passenger information tasks. As a result, PIOs rely heavily on human sources for situational information. Information systems may mitigate some of the problems, for instance, by enabling the harvesting of information from distributed actors. However, it appears that the rapid changeability of the work situation is a fundamental behavioural constraint.

These findings suggest that dynamic situations offer new opportunities for testing and refining theories of information behaviour. While Kuhlthau's uncertainty principle and Byström and Järvelin's theories concerning task complexity are largely supported, they require modification in order to adequately explain the events described in this paper. Further research is required in order to explore these relationships in full.

### Evaluating the methods

Passenger information work is a challenging research subject. Fortunately, it is largely explicit work that lends itself well to observation-based analysis. The approach adopted in the current project was, on the whole, successful in terms of the research objectives. However a number of limitations can be identified. Both the strengths and limitations are discussed below.

#### Strengths

The ethnographically informed method employed in the current project provided a number of advantages over other potential methods:

*   Findings reveal the social aspects of passenger information work which are not readily visible using techniques such as interviews and task analysis.
*   Work models are built directly from observations of real-world actions and interactions, removing the usual barriers between system analysts and users.
*   Allows the researcher to better understand how the system as a whole functions, and to identify weaknesses in certain information flows.
*   The method facilitates the definition of system requirements that clearly reflect the realities of passenger information work.

#### Limitations

The methods, as applied in the current research, also face certain limitations, some of which are outlined below, along with suggestions for improvement.

*   The perspective captured in this report is that of the PIO and, therefore, is partial. Analysis would need to be repeated from the perspective of other RMC roles in order to describe the situation in full.
*   The findings are based on selective information, limited by the capacity of a single researcher, using pen and paper, to capture rapid and dynamic information flows. Therefore, the approach may filter out events and actions that have significant explanatory potential.
*   The notations used for data capture limit the richness of scenario descriptions. The approach did not use formal methods for marking time, or for representing speech characteristics, preventing more detailed commentary, for instance on the temporal aspects of work, or on generalised versus private information sharing.
*   A broader range of scenarios is needed to properly represent passenger information work. While the current project certainly covers enough ground to draw a range of conclusions about the essential nature of passenger information work, key insights were derived, in the main, from non-standard situations.
*   Because of the focus on the explicit aspects of work; i.e., the actions and utterances of PIOs, and the observable events in the workplace, the method provides limited insight into the cognitive work of PIOs, such as relevance judgements.

## References

*   <a id="bal00"></a>Ball, L. & Ormerod, T. (2000). Putting ethnography to work: the case for a cognitive ethnography of design. _International Journal of Human Computer Studies_, **53**(1), 147-168.
*   <a id="bey98"></a>Beyer, H. & Holtzblatt, K. (1998). _Contextual design: defining customer-centred systems_. San Francisco, CA: Morgan Kaufmann.
*   <a id="by95"></a>Byström, K. & Järvelin, K. (1995). Task complexity affects information seeking and use. _Information Processing & Management_, **31**(2), 191-213.
*   <a id="cra99"></a>Crabtree, A., Rouncefield, M., O'Brien, J., Tolmie, P. & Hughes, J. (1999). [There's something else missing here: requirements specification in changing circumstances of work and design](http://www.lancs.ac.uk/fss/sociology/papers/crabtree-rouncefield-something-else-missing.pdf). Retrieved 20 August, 2006 from: http://www.lancs.ac.uk/fss/sociology/papers/crabtree-rouncefield-something-else-missing.pdf.
*   <a id="el99"></a>Ellis, D., Allen, D. & Wilson, T.D. (1999). Information science and information systems: conjunct subjects disjunct disciplines. _Journal of the American Society for Information Science_, **50**(12), 1095-1107\.
*   <a id="eng96"></a>Engestrom, Y. & Middleton, D. (Eds.) (1996). _Cognition and communication at work_. Cambridge: Cambridge University Press.
*   <a id="fie00"></a>Fields, R. & Wright, P. (2000). Editorial: understanding work and designing artefacts. _International Journal of Human Computer Studies_, **53**(1), 1-4.
*   <a id="hut95"></a>Hutchins, E. (1995). _Cognition in the wild_. Cambridge, MA: MIT Press.
*   <a id="kuh93"></a>Kuhlthau, C. (1993). _Seeking meaning: a process approach to library and information services_. Norwood, NJ: Ablex Publishing.
*   <a id="mal97"></a>Malmsjö, A. (1997). Information seeking behaviour and development of information systems: a contextual view. In Vakkari, P., Savolainen R. & Dervin, B. (Eds.). _Information seeking in context: Proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts_. 14-16 August, 1996, Tampere, Finland. (pp. 222-235). London: Taylor Graham.
*   <a id="nar96"></a>Nardi, B.M. (Ed) (1996). _Context and consciousness: activity theory and human-computer interaction_. Cambridge, MA: MIT Press.
*   <a id="pet01"></a>Pettigrew, K., Fidel, R. & Bruce, H. (2001) Conceptual frameworks in information behaviour. _Annual Review of Information Science and Technology_, **35**, 43-78\.
*   <a id="soe97"></a>Soergel, D. (1997). [An information science manifesto](http://www.asis.org/Bulletin/Dec-97/Soergel.htm). _Bulletin of the American Society for Information Science_, **24**(2). Retrieved 7 May 2006 from http://www.asis.org/Bulletin/Dec-97/Soergel.htm.
*   <a id="son00"></a>Sonnenwald, D. & Pierce, L. (2000). Information behaviour in dynamic group work contexts: interwoven situational awareness, dense social networks and contested collaboration in command and control. _Information Processing and Management_, **36**(3), 461-479.
*   <a id="suc87"></a>Suchman, L. (1987). _Plans and situated action_. Cambridge: Cambridge University Press.
*   <a id="suc99"></a>Suchman, L., Blomberg, J., Orr, J. & Trigg, R. (1999). Reconstructing technologies and social practice. _American Behavioural Scientist_, **43**(3), 392-408\.
*   <a id="sut97"></a>Sutcliffe, A. (1997). Task-related information analysis. _International Journal of Human-Computer Studies_, **47**(2), 223-257\.
*   <a id="vak99"></a>Vakkari, P. (1999). Task complexity, problem structure and information actions: integrating studies on information seeking and retrieval. _Information Processing and Management_, **35**(6), 819-837.
*   <a id="vil00"></a>Viller, S. & Sommerville, I. (2000). Ethnographically informed analysis for software engineers. _International Journal of Human Computer Studies_, **53**(1), 169-196.
*   <a id="wil99"></a>Wilson, T.D. (1999). Models in information behaviour research. _Journal of Documentation_, **55**(3), 249-270.
*   <a id="wil00"></a>Wilson, T.D. (2000). [Human information behavior](http://inform.nu/Articles/Vol3/v3n2p49-56.pdf). _Informing Science_, **3**(2), 49-55\. Retrieved 20 August, 2006 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf.

## <a id="app2"></a>Appendix 2

Interview guide: business context

1.  In a day-to-day operational setting, what do you consider to be the major objectives of the passenger information function?
2.  Who relies on passenger information, and what for?
3.  What are the other sources of operational status information?
4.  How do you see passenger information fitting in with or complementing these sources?
5.  What are the major barriers or challenges faced by passenger information when trying to meet daily operational objectives?
6.  In what areas is RailCorp seeking improvements to passenger information?
7.  How do you judge the effectiveness or performance of passenger information as a function?
8.  Are staff very vocal about what they want or what they don't like?
9.  In terms of specific communications issued by passenger information, what are the key qualities that define a good message?
10.  Given unlimited budget, technology and resources, how would you envisage an ideal passenger information service?