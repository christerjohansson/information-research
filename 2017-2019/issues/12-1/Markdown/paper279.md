#### Vol. 12 No. 1, October 2006

* * *

# Responses and influences: a model of online information use for learning

#### [Hilary Hughes](mailto:h.hughes@qut.edu.au)  
School of Cultural and Language Studies in Education  
Queensland University of Technology Kelvin Grove Q4059,
Brisbane, Australia

#### Abstract

> **Introduction.** Explores the complexity of online information use for learning in the culturally-diverse, information and communication technologies-intensive, higher education context. It presents a _Model of responses and influences in online information use for learning_, which aims to increase awareness of the complexity of online information use and support information literacy development.  
> **Background.** Despite increasing integration of information literacy into university curricula there are evident limitations in students' use of information associated with an information literacy imbalance between well-developed information technology skills and uncritical approaches, compounded by differences in cultural and linguistic experience.  
> **Influences.** This model draw insight from models of information behaviour and information seeking, information literacy, cross-cultural adaptation, and reflective online use.  
> **The model.** Incorporates behavioural, cognitive and affective responses with cultural and linguistic influences in an action research framework that represents online information use as holistic, dynamic and continuous, envisaged as the _experience of_ engaging with online information for learning.  
> **Conclusion.** The model represents the synergy between information use and learning. It supports the development of inclusive reflective approaches to information literacy that address identified learning challenges related to information literacy imbalance and cultural and linguistic diversity.

## Introduction

The connection between information literacy and learning is widely acknowledged. In higher education it supports a trend towards self-directed, research-based and online learning approaches which require a balance of digital capabilities, effective information seeking and the critical, ethical and creative use of information. However despite the increasing integration of information literacy education into university curricula around the world, research indicates significant limitations in study-related information use. An apparent information literacy imbalance is evident between many students' well developed information technology skills and less developed critical approaches to information use. Significantly also, in light of the internationalisation of higher education, learning and information use challenges can be further intensified by differences in students' the social, cultural, and linguistic experience.

In response to these challenges this paper offers a _Model of responses and influences in online information use for learning_ (hereafter called 'the Model') which has a two-fold purpose: to represent the complexity of online information use for learning in a culturally diverse, ICT-intensive environment; and to provide a framework for developing inclusive reflective information literacy strategies that addresses this complexity. Consequently this model reflects both a broad concern with the nature of information use and a specific concern with learners' information literacy needs. It embodies a developing conception of (online) information use as the _experience of engaging with (online) information for learning_; understood here to be a multi-faceted experience that incorporates users' context, needs, engagement and outcomes. The model draws together key elements of significant models in the fields of information behaviour/seeking ([Wilson 1997](#wil97); [Foster 2005](#fos05); [Kuhlthau 2004](#kuh04)), information literacy ([Bruce 1997](#bru97)) and cross-cultural adaptation ([Anderson 1994](#and94)). In particular it emphasises the role of information literacy in learning, and blends cultural and linguistic influences into the interplay of thoughts, feelings and actions in information use. It is intended to complement previous models of reflective internet searching ([Edwards & Bruce 2002](#edw02)) and reflective online information use ([Hughes _et al._ 2006](#hug06)).

The paper comprises three main parts. The first, Background, explains the purpose and context of the Model; the second, Conceptual influences, describes the influence of other models on the development of the Model; and the third, Overview, discusses the structure and intended application of the Model.

## Background to the Model

### Research base

This paper arises from the author's ongoing doctoral research: a study that investigates international students' experience of using of online information resources and related information literacy learning needs in a culturally diverse educational environment ([Hughes 2005](#hug05); [Hughes & Bruce 2006](#hug06a)). Whilst set in the Australian higher education context it draws insight from the wider literature of cultural and educational diversity, information behaviour, information use and information literacy. An exploration of conceptual models and their applications supports the data analysis, resulting in the Model which is introduced in Figure 2 and discussed below.

### Purpose of the Model

Wilson ([1999](#wil99): 250) states:'A model may be described as a framework for thinking about a problem and may evolve into a statement of relationships among theoretical propositions'. The problem addressed here concerns the complexity of, and inter-related influences on, online information use for learning in a culturally diverse information and communication technologies-intensive higher education environment. The Model shown in Figure 2 provides the framework for representing this complexity and developing an information literacy approach that responds to this complexity.

### Context of the model

Wilson's 1981 model of information-seeking behaviour represents the circumstances that give rise to information-seeking behaviour in terms of 'the person performing a role in an environment' ([Wilson 1997](#wil97): 552). Extrapolated to the present Model, the _persons_ can be said to be university students, the _role_ their use of online information for learning and the _environment_ the culturally-diverse, information and communication technologies-intensive higher education.

### Information use and information literacy

Definitions of information behaviour, information seeking, information use and information literacy overlap and vary in interpretation. In seeking a broad interpretation of _information use_ this paper refers to it as the experience of engaging with online information for learning. This is understood as a multifaceted experience involving users' context, needs, actions (behaviour, information seeking), responses and influences (cognitive, affective, cultural, linguistic) and outcomes (insight, knowledge construction). Information literacy (in the context of this paper) is considered to be intrinsic to the experience of engaging with online information use for learning; it constitutes the understanding, capabilities and critical approaches that collectively underpin effective online information use and that foster learning.

The model introduced in this paper reflects Bruce's ([1997](#bru97)) holistic representation of information literacy as a multifaceted experience that reaches beyond digital competencies and information seeking skills to the development of critical, creative and ethical approaches to information use that promote knowledge construction and wisdom. It also supports a close connection between information literacy and learning ([Breivik 1998](#bre98), [Bruce 1997](#bru97), [Lupton 2004](#lup04)).

### Students and their use of online infomation resources for learning

Online information resources include a wide array of materials available in electronic format from public, proprietary and institutional sources such as the World Wide Web, specialist databases, library catalogues and course Websites. The [QUT Library Website](http://www.library.qut.edu.au/) for example provides a representative snapshot of the educational material provided by Australian universities to their enrolled students (QUT 2006).

The variety and complexity of online information resources pose challenges for learners and educators in Australia and elsewhere. Despite increasing recognition of the role of information literacy in learning and its inclusion in university curricula, research shows that many students either disregard or are unfamiliar with academic information sources. Rather they tend to access material that is freely available on the Internet through search engines, Weblogs, wikis and bulletin boards. Significantly, these tendencies are often associated with over-confidence in their computer proficiency and/or limited critical awareness ([Armstrong _et al._ 2001](#arm01); [Brown _et al._ 2003](#bro03); [Edwards 2006](#edw06); [Jones 2002](#jon02); and [Logan 2004](#log04)). All this contributes to what may be identified as an apparent information literacy imbalance between the students' generally strong information technology skills and their frequently uncritical approaches to searching for, evaluating and using information.

These limitations may be compounded by differing cultural and linguistic experiences ([Hughes 2005](#hug05); [Hughes & Bruce 2006](#hug06a)). For example, international students in Australia are frequently unaccustomed to the self-directed, research-based learning approaches that predominate here ([Ballard & Clanchy 1997](#bal97); [Ramsay _et al._ 1999](#ram99); [Samuelowicz 1987](#sam87)). Moreover, they often have little or no previous experience of libraries and online information resources and possess limited information literacy ([Baron & Strout-Dapaz 2001](#bar01); [DiMartino & Zoe 2000](#dim00); [McSwiney 1995](#mcs95)).

It is important to note however that cultural and linguistic barriers to information use are not exclusively experienced by people from different backgrounds. For example, the inclination to save face evidenced by the reluctance to ask questions and seek help is a fairly common characteristic across cultures. Likewise, it is not only people from non-English speaking backgrounds who experience linguistic difficulties such as mistyped and misspelled search terms, or who struggle with discipline-specific jargon and academic styles of English. This suggests the value of an inclusive approach to information literacy ([Biggs 2003](#big03); [Hughes& Bruce 2006](#hug06a)).

## Conceptual influences on the Model

Conceptually the present Model draws insight from significant models in the fields of information behaviour, information literacy and cross-cultural adaptation and it complements models of reflective Internet and online information use. The essential elements of each of these models, as they contribute to the present Model, are highlighted below

### Wilson's _Revised general model of information behaviour_

The Model reflects key elements of Wilson's revised general model ([1997](#wil97): 569) with regard to its focus on information users, their information needs, their information-seeking behaviour and their context. In particular it emphasises the impact of personal and cultural influences on information use which relate to Wilson's _intervening variables_. However the present Model's underlying conception of _engaging with online information_ extends beyond information-seeking to encompass critical, ethical and creative use(s) of information for learning. While the present Model retains the inter-connectedness of Wilson's model, its cyclical form, with no fixed start and end points, envisages flexible approaches to information use and allows for jumping and reiterating between and within phases.

### Kuhlthau's _Information search process_

The present Model is also influenced by Kuhlthau's ([2004)](#kuh04) Information Search Process in relation to its user-centredness, focus on learning and emphasis on the close inter-relationship, or holistic'interplay', between behavioural, cognitive and affective responses in information seeking and their influence on all aspects of information use. The present Model expands Kuhlthau's conception by incorporating cultural and linguistic influences into the interplay and by widening the perspective beyond information seeking to all aspects of engaging with online information. However, its cyclical form contrasts with the linear structure of the Information Search Process. While no direct correlation is intended between the six stages of that Process and the four phases of the present model, it is anticipated that the Plan-Act-Record-Reflect elements could come into play during Kuhlthau's process stages.

### Foster's _Non-linear model of information seeking behaviour_

The impact of Foster's ([2005](#fos05)) model on the present Model is reflected in their shared conception of information use as dynamic, multilayered and non-linear. It is also felt in their focus on both the information users' experience and the contextual, affective and cognitive influences on information use. While no direct correlation is intended between Foster's three core processes and the four phases of the present model, elements of the Plan-Act-Record-Reflect cycle might be linked to 'activities' pertaining to the core processes. So, for example, within Foster's _Orientation_ core process the _Reviewing_ activity may be akin to _Reflect_ and _Picture building_ akin to _Plan_ and/or _Act_. Significantly, in view of the present model's intended information literacy application, Foster proposes that his model could be 'a holistic tool for viewing and creating an information literate person... a framework for educators and library professionals to teach information skills' (Foster 2005, para. 37).

### Bruce's _Seven faces of information literacy_

The multifaceted conception of information literacy and emphasis on people's experience of using information that characterise Bruce's ([1997](#bru97)) analysis are intrinsic to the present Model. Both reflect a holistic understanding of information use, envisaged here as _engaging with information_, that encompasses digital competencies, information seeking skills, critical understanding, knowledge construction and creative applications.

### Anderson's _Cross-cultural adaptation process model_

The significance of Anderson's ([1994](#and94)) model for the present Model relates to its depiction of cultural, linguistic, affective, cognitive and behavioural influences on aspects of human experience. Notably it has been used at an Australian university to identify similarities and differences among international and local students, with a view to supporting their learning and adjustment ([Ramsay_et al._ 1999](#ram99)). The cyclical, recursive structure, which signifies the continuous nature of adaptation, resonates with the present model's similar representation of information use for learning. Also, the six principles of cross-cultural adaptation listed by Anderson have close parallels with those of the present model: 'It involves adjustment; it implies learning; it implies a stranger-host relationship; it is cyclical, continuous and interactive; it is relative; and it implies personal development' ([Anderson 1994](#and94): 303).

### Hughes _et al._'s _Reflective online information use model_

The model ([Hughes _et al._ 2006](#hug06)) shown in Figure 1 highlights the synergy between information use and learning and offers a framework for developing a reflective approach to online information use. It builds on Edwards and Bruce's ([2002](#edw02)) action research model [<sup style="font-size: small">1</sup>](#note) for reflective Internet searching, which, in turn, draws on Bruce's earlier models for reviewing the literature ([1996](#bru96)) and reflective information searching ([1992](#bru92)). They all share two characteristics with the original model: the action research-influenced structure ([Kemmis & McTaggart 1988](#kem88)); and the underlying principle of reflective practice that promotes continuous, contextualised learning by encouraging participants to reflect both 'on-action' (after completing an activity) and 'in-action' (whilst engaged in the activity) ([Schön 1987](#sch87)).

The novel feature of the _Reflective online information use model_ is that it integrates Bruce's Seven faces of information literacy ([1997](#bru97)) with the characteristic action research and reflective practice elements. Consequently this model's emphasis is on information literacy as an holistic, user-focused experience. Here the model's four inter-connected phases (Plan, Act, Record and Reflect) represent online information use as continuous and relational rather than linear. Online information use is shown to be dynamic, both in terms of the searcher's active engagement and the changing online environment in which the engagement takes place. The four key phases of the model relate to Bruce's ([1997](#bru97)) first five conceptions of information literacy, as follows:

*   PLAN relates to the first two conceptions of information literacy, Information technology and Information sources, identified in the present Model respectively as scanning and sourcing information. At this initial phase the user is concerned with investigating online resources and planning strategies for using them effectively to find information.
*   ACT relates to the third conception, Information process, identified in the present Model as engaging with information processes. This phase involves applying the strategies determined previously for using the online resources to find needed information.
*   RECORD relates to the fourth conception, Knowledge control, identified in the present Model as controlling information. This phase involves activities such as saving, bookmarking, e-mailing and printing information found during the ACT phase.
*   REFLECT relates to the fifth conception, Knowledge construction, identified in the present Model as critiquing information and constructing new knowledge.

The purpose and outcomes of online information use are embedded in the centre of the model:

_Use information creatively and ethically_ relates to sixth and seventh conceptions of information literacy: Knowledge extension and Wisdom, identified in the present model as gaining novel insights and using information wisely.

The Plan-Act-Record-Reflect 'meta-cycle' can support any number of inter-related 'mini-cycles' within each phase. Learners are able to engage flexibly with information and they may pass through part or all of the meta-cycle once, several or many times depending on their information need(s). Although sequential progression through each phase is the suggested 'ideal' approach, information users may jump phases, backtrack or exit mid-way. Also they may complete one or several mini-cycles within a particular phase. Importantly, and in accordance with the concept of reflective practice ([Schön 1987](#sch87)), reflection occurs continuously 'in-action' within each mini-cycle, as well as 'on-action' at completion of the meta-cycle. In other words learners are encouraged to reflect on both their information needs, process(es), actions and their results whilst involved in any of the phases of the online information use cycle, as well as retrospectively on the whole experience and its outcomes.

<figure>

![Figure 1](../p279fig1.jpg)

<figcaption>

**Figure 1: Reflective online information use model. (Hughes _et al._ 2006)**</figcaption>

</figure>

### Summary of related models

As stated previously the Model draws on elements of significant models in the fields of information behaviour, information literacy and cross-cultural adaptation. These models, their key contributing elements and their significance to the present model are summarised in the following table.

<table><caption>

**Table 1: Summary of conceptual influences on _Model of responses and influences in online information use for learning_**</caption>

<tbody>

<tr>

<th>Model</th>

<th>

Contribution to the _Model of responses and influences_</th>

<th>

Significance to the _Model of responses and influences_</th>

</tr>

<tr>

<td>

Wilson's _Revised general model of information behaviour_</td>

<td>Conception of information use as a continuous process, importance of users' situation and needs, and intervening variables</td>

<td>Underpins understanding and representation of information behaviour and use</td>

</tr>

<tr>

<td>

Kuhlthau's _Information seeking process_</td>

<td>Conception of interplay of behavioural, cognitive & affective influences in information seeking</td>

<td>Holistic view of information seeking incorporating behavioural, cognitive & affective influences, application to learning</td>

</tr>

<tr>

<td>

Foster's _Non-linear model of information seeking_</td>

<td>Conception of information seeking as non-linear and multi-layered, focuses on information users' experience</td>

<td>Suggests an alternative approach to modelling information seeking - conceptual relationships rather than stages</td>

</tr>

<tr>

<td>

Bruce's _Seven faces of information literacy_</td>

<td>Conception of information literacy as a multi-faceted phenomenon, focus on the user's information experience</td>

<td>Extends beyond information behaviours and information seeking to a holistic view of information experience, promotes link between information use and learning</td>

</tr>

<tr>

<td>

Anderson's _Cross-cultural adaptation process_</td>

<td>Integration of behavioural, cognitive and affective influences, cyclical representation of cross-cultural adaptation</td>

<td>Extends awareness of behavioural, cognitive, affective and cultural influences beyond the information domain</td>

</tr>

<tr>

<td>

Hughes _et al._ _Reflective online information use model_ 2006)</td>

<td>

Action research-influenced structure, integration of principles of Bruce's _Seven faces of information literacy_ model</td>

<td>

Common conceptual ground with the _Model of responses and influences_; promotes holistic understanding of information use and information literacy</td>

</tr>

</tbody>

</table>

## Overview of the Model

### Introducing the Model

The Model which is introduced below in Figure 2, is learner-centred. It addresses evident limitations in students' use of information for learning that are associated with an imbalance between their digital competencies and less developed critical approaches, and also with differences in cultural and linguistic experience ([Armstrong _et al._ 2001](#arm01); [Brown _et al._ 2003](#bro03); [Hughes 2005](#hug05); [Jones 2002](#jon02)). It aims to represent the complex nature of information use for learning in a culturally diverse information and communications technologies-intensive, higher education environment and also to support the development of reflective information literacy approaches.

The Model is based on a broad conception of the _experience of engaging with online information for learning._ As explained above it reflects essential elements of significant models in the fields of information behaviour, information literacy and cross-cultural adaptation. In particular it highlights the inter-relationship, or interweaving, of the behavioural, cognitive, affective, cultural and linguistic aspects that colour the experience of online information use. Thus, it resonates with Kuhlthau's ([2004](#kuh04)) conception of the interplay of behavioural, cognitive & affective elements in information seeking.

In terms of structure and emphasis the present Model is intended to complement the model discussed above and shown in Figure 1\. The latter's purpose and form are embedded in the present Model and represented by the _Plan-Act-Record-Reflect_ labels. While the previous model provides a framework for developing reflective online information use, this Model draws attention to significant cultural and linguistic influences and the corresponding need for inclusive approaches to information literacy education.

The _Plan-Act-Record-Reflect_ framework of both these models resembles the action research cycle ([Kemmis and McTaggart 1988](#kem88)). Both represent information use as dynamic and continuous and support unlimited information use meta-cycles and mini-cycles. They are underpinned by the holistic understanding of information literacy represented by Bruce's ([1997](#bru97)) model. The key elements of the present Model, Plan, Act, Record, Reflect and engaging with online information for learning, again relate respectively to the seven conceptions of information literacy identified by Bruce as: Scanning and Sourcing information; Engaging with information processes; Controlling information; Critiquing information and Constructing new knowledge; Gaining novel insights and Using information wisely.

The conceptual centre of the Model, engaging with online information for learning, represents both the purpose and outcomes of online information use, which inter-relate with the phases of Plan-Act-Record-Reflect. The Model's rectangular border encapsulates this experience within a culturally-diverse information and communication technologies-intensive, higher education environment.

The cyclical structure of the Model implies the continuous, flexible and holistic nature of online information use. It also signals that each and every response and influence may come into play during any of the information cycle phases (both meta-cycles and mini-cycles). As an example of linguistic influence, a student's limited English language proficiency may inhibit their effectiveness in determining appropriate search terms at the PLAN phase, entering valid search terms at the ACT stage, evaluating and selecting resources at the REFLECT stage and, ultimately, understanding the material to gain insight and construct new knowledge.

<figure>

![Figure 2](../p279fig2.jpg)

<figcaption>

**Figure 2: Model of responses and influences in online information use for learning**</figcaption>

</figure>

The distinctive features of this Model are the segmented circles that indicate the inter-relationship, or interweaving, of the various elements that colour the experience of engaging with information for learning. These aspects are of two types, responses (shades of blue) and influences (shades of yellow). _Responses_ here correspond to the behavioural, cognitive and affective aspects of Kuhlthau's (2004) process model. _Influences_ are the cultural and linguistic aspects that are added to the interplay in this Model.

The five aspects are not mutually exclusive; like the segments of the circles they overlap and blend. The different influences and responses have varying impact on information use for learning depending on the situation. Face-saving, for example, can have strong cultural (cultural influence) and emotional (affective response) connotations, perhaps associated with educator-learner relationships and academic anxiety. Language limitations (linguistic influence) may also relate to face-saving responses, such as reticence in class and reluctance to seek help. Any of these experiences could affect information seeking behaviour (behavioural response), such as abandoning a search.

### Applying the Model

The present Model complements that of Hughes, _et al._ ([2006](#hug06)) in supporting an inclusive, reflective approach to information literacy education ([Hughes and Bruce 2006](#hug06a)). It draws attention to the impact of cultural and linguistic influences on information use, their inter-relationship with behavioural, cognitive and affective influences and especially the need to consider these elements in the development and implementation of information literacy education. On a practical level this model provides a framework for developing information literacy strategies that promote active enagement with information, critcal thinking and problem-solving.

### Summarising the Model

The Model introduced in Figure 2 seeks to illustrate the complexity of online information use for learning. In particular highlighting:

*   the multifaceted nature of information use that encompasses information skills (information searching and processing) and the critical, ethical and creative use of information for knowledge creation;
*   the inter-relatedness, or interplay, of behavioural, cognitive and affective responses and cultural and linguistic influences in online information use; and
*   the dynamic, culturally diverse environment of online information use

The key features of the Model are that it is:

*   cyclical and relational in form;
*   holistic: its broad conception of information use extends beyond information behaviour and information seeking and reflects the multifaceted conception of information literacy of Bruce's ([1997](#bru97)) model;
*   inclusive: it emphasises the impact of cultural and linguistic influences on information use, and the need to allow for them in addressing information literacy learning needs;
*   reflective: it encourages critical, holistic approaches to information use beyond digital competencies and information seeking skills; and
*   user-centred: it focuses on the experience rather than the process of information use.

Perhaps most significantly the Model fosters learning. It supports an inclusive, reflective approach to information literacy development as proposed by Hughes and Bruce ([2006](#hug06a)). While models of information use tend to look at the person as 'a finder, creator and user of information' ([Case 2002](#cas02): 6), this Model and the previous models of internet and online resource use ([Edwards and Bruce 2002](#edw02); [Hughes _et al._ 2006](#hug06)) view the information user primarily as **learner**.

## Conclusion

This paper responds to evident learning challenges encountered by university students, relating to their generally uncritical approaches to information use, expressed here as an information literacy imbalance, and their differing cultural, linguistic and educational experience. The Model presented here has a two-fold aim: to increase awareness of the complexity of online information use for learning and to support teaching and learning. On a conceptual level this model envisages online information use as _the experience of engaging with online information_, as a multifaceted experience which encapsulates learners' information needs and context, their information seeking and use of information and their learning outcomes. On a practical level the Model offers a framework for developing inclusive and reflective information literacy strategies. In this way the Model represents the synergy between information use and learning and underpins a holistic information literacy approach that fosters learning in the culturally-diverse, information and communications technology-intensive, higher education environment.

## Editor's note

1\. 'Action research' is here understood as associated with the concept of 'reflective learning'. It differs from the earlier concept of action research as a participative method of social research employed in organizations to effect change. See, for example, Masters, J. (1995) [The history of action research](http://www.webcitation.org/5JpPPaXs9). In I. Hughes (Ed.). _Action research electronic reader_. Sydney, Australia: The University of Sydney. Retrieved 11 September, 2006 from http://www.scu.edu.au/schools/gcm/ar/arr/arow/default.html

## Acknowledgements

The research reported here was conducted as part of a PhD programme in the Faculty of Information Technology, Queensland University of Technology, Brisbane, Queensland, Australia. The author acknowledges the valued support of supervisors Associate Professor Christine Bruce & Mr Michael Middleton.

## References

*   <a id="and94"></a>Anderson, L.E. (1994). A new look at an old construct: cross-cultural adaptation. _International Journal of Intercultural Relations_ , **18**(3), 293-328\.
*   <a id="arm01"></a>Armstrong, C., Fenton, R., Lonsdale, R., Stoker, D., Thomas, R. &Urquhart, C. (2001). A study of the use of electronic information systems by higher education students in the UK. _Program_, **35**(3), 241-262\.
*   <a id="bal97"></a>Ballard, B. & Clanchy, J. (1997). _Teaching international students: a brief guide for lecturers and supervisors._ Deakin, VIC, Australia: IDP Education Australia.
*   <a id="bar01"></a>Baron, S. & Strout-Dapaz, A. (2001). Communicating with and empowering international students with a library skills set. _Reference Services Review_ **29**(4), 314- 26.
*   <a id="big03"></a>Biggs, J. (2003). _Teaching for quality learning at university: what the student does._ (2nd. ed.). Maidenhead, UK: Open University Press.
*   <a id="bre98"></a>Breivik, P.S. (1998). _Student learning in the information age_ . Phoenix, AZ: American Council on Education and Oryx Press.
*   <a id="bro03"></a>Brown, C., Murphy, T.J. & Nanny, M. (2003). Turning techno-savvy into info-savvy: authentically integrating information literacy into the college curriculum. _Journal of Academic Librarianship_, **29**(6), 386-398.
*   <a id="bru92"></a>Bruce, C.S. (1992). Developing students' library research skills. Campbelltown, NSW, Australia: Higher Education Research Development Society of Australia. (HERDSA Green Guide No. 13).
*   <a id="bru96"></a>Bruce, C.S (1996). From neophyte to expert: counting on reflection to facilitate complex conceptions of the literature review. In O. Zuber-Skerrit (Ed.), _Frameworks for postgraduate education_. (pp. 239-253). Lismore, NSW, Australia: Southern Cross University Press.
*   <a id="bru97"></a>Bruce, C.S. (1997). _The seven faces of information literacy._ Adelaide, SA, Australia: Auslib Press.
*   <a id="cas02"></a>Case, D.O. (2002). _Looking for information: a survey of research on information seeking, needs and behaviour_ . Amsterdam: Academic press.
*   <a id="dim00"></a>Di Martino, D. & Zoe, L.R. (2000). International students and the library: new tools, new users, and new instruction. In T.E Jacobson &H.C. Williams, (Eds.), _Teaching the new library to today's users_. (pp. 17-43). New York, NY: Neal-Schuman.
*   <a id="edw06"></a>Edwards, S.L. (2006). _Panning for gold: information literacy and the Net Lenses model_ . Adelaide, VIC, Australia: Auslib Press.
*   <a id="edw02"></a>Edwards, S.L. & Bruce, C.S. (2002). Reflective internet searching: an action research model. _The Learning Organization_, **9**(4), 180-188\.
*   <a id="fos05"></a>Foster, A. (2005). [A non-linear model of information seeking behaviour](http://informationr.net/ir/10-2/paper222.html). _Information Research_, **10**(2), paper 222\. Retrieved 31 August, 2006 from http://informationr.net/ir/10-2/paper222.html http://informationr.net/ir/10-2/paper222.html
*   <a id="hug05"></a>Hughes H. (2005). Actions and reactions: exploring international students' use of online information resources. _Australian and Academic Research Libraries_, **36**(4), 169-179\.
*   <a id="hug06a"></a>Hughes, H. & Bruce, C.S. (2006). Cultural diversity and educational inclusivity: international students' use of online information. _International Journal of Learning_, **12**(9), 33-40
*   <a id="hug06"></a>Hughes, H., Bruce, C.S. & Edwards, S.L. (2006). [Fostering a reflective approach to online information use for learning.](http://eprints.qut.edu.au/archive/00004556/) In D. Orr, F. Nouwens, C. Macpherson, R.E. Harreveld & P.A Danaher, (Eds.). _Lifelong learning: partners, pathways, and pedagogies. Keynote and refereed papers from the 4th International Lifelong Learning Conference, Yeppoon, Queensland, 2006._ (pp. 143-150). Rockhampton, QLD, Australia: Central Queensland University Press. Retrieved 31 August, 2006 from http://eprints.qut.edu.au/archive/00004556/
*   <a id="jon02"></a>Jones, S. (2002). [_The Internet goes to college: how students are living in the future with today's technology_]( http://www.webcitation.org/5IYLnI6f4). Washington, DC: Pew Internet and American Life Project. Retrieved 26 November, 2005 from http://www.pewinternet.org/pdfs/PIP_College_Report.pdf
*   <a id="kem88"></a>Kemmis, S. & McTaggart, R. (1988). _The action research planner_. 3rd. ed. Geelong, SA, Australia: Deakin University Press.
*   <a id="kuh04"></a>Kuhlthau, C. C. (2004). _Seeking meaning: a process approach to library and information services_ . 2nd. ed. Westport, CT: Libraries Unlimited.
*   <a id="log04"></a>Logan, J. (2004). [_Using an online database searching tutorial to encourage reflection on the research process by undergraduate students_](https://olt.qut.edu.au/udf/OLT2004/index.cfm?fa=getFile&rNum=1587192&nc=1). Paper delivered at the Online Teaching and Learning Conference: exploring Integrated Learning Environment. Brisbane, 3 November 2004\. Retrieved 26 November, 2005 from https://olt.qut.edu.au/udf/OLT2004/index.cfm?fa=getFile&rNum=1587192&nc=1
*   <a id="lup04"></a>Lupton, M. (2004). _The learning connection: information literacy and the student experience_ . Adelaide, SA, Australia: Auslib Press.
*   <a id="mcs95"></a>McSwiney, C. (1995). _Essential understandings: international students, learning, libraries._ Adelaide, SA, Australia: Auslib Press.
*   <a id="ram99"></a>Ramsay, S., Barker, M. & Jones, E. (1999). Academic adjustment and learning processes: a comparison of international and local students in first-year university. _Higher Education Research and Development_, **18**(1), 129-144\.
*   <a id="sam87"></a>Samuelowicz, K. (1987). Learning problems of overseas students: two sides of a story. _Higher Education Research & Development_, **6**(2), 121-133\.
*   <a id="sch87"></a>Schön, D. (1987). _Educating the reflective practitioner_. San Francisco, CA: Jossey-Bass.
*   <a id="wil97"></a>Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing and Management_, **33**(4), 551-572\. [_Ed. The research report on which this paper was based can be found at [http://informationr.net/tdw/publ/infbehav/cont.html](http://www.webcitation.org/5IYMkztPW )_
*   <a id="wil99"></a>Wilson, T.D. (1999). [Models in information behaviour research](http://www.webcitation.org/5IYMXQGwQ). _Journal of Documentation_, **55**(3), 249-270\. Retrieved 31 August, 2006 from http://informationr.net/tdw/publ/papers/1999JDoc.html