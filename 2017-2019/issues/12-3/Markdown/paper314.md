#### Vol. 12 No. 3, April 2007

* * *

# Information technology adoption for service innovation practices and competitive advantage: the case of financial firms

#### [J.S. Chen](mailto:jchen@saturn.yzu.edu.tw) and H.T. Tsou  
Department of Business Administration, Yuan Ze University, Chung Li, Taiwan

#### Abstract

> **Background.** The importance of information technology to current business practices has long drawn the attention of practitioners and academicians.  
> **Aim.** This paper aims to broaden understanding about service innovation as a critical organizational capability through which information technology adoption influences the competitive advantage of a firm. In the context of financial firms, this study examines how information technology is adopted and managed to enhance service innovation practices and whether and how service innovation practices may influence the competitive advantage of firms.  
> **Method.** A research framework and the associated hypotheses are proposed. An empirical survey was conducted and questionnaires were mailed to 558 financial firms in Taiwan.  
> **Analysis.** A total of 124 valid observations was collected and analysed using the partial least squares technique.  
> **Conclusions.** The results suggest that adopting information technology has positive effects on service innovation practices, which increase the competitive advantage of firms.

## Introduction

Contemporary firms are making significant investments in information technology to align business strategies, enable innovative functional operations and provide extended enterprise networks. These firms have adopted information technology to foster changes in managing customer relationships, manufacturing, procurement, the supply chain and all other key activities ([Agarwal & Sambamurthy 2002](#a02); [Barua & Mukhopadhyay 2000](#a12)) and to enhance their competitive capabilities ([Sambamurthy _et al._ 2003](#a79)). A number of information systems researchers have posited information technology as an important ingredient of innovation development (e.g., [Corso & Paolucci 2001](#a29); [Dewett & Jones 2001](#a39); [Xu _et al._ 2005](#a96)). Firms implement information technology to enhance and/or enlarge the scope of their products and services. As many innovation activities involve adding new services, expanding existing ones and/or improving the service delivery process, the success of an organization hinges on how well it implements its service innovation ([Berry _et al._ 2006](#a14)) to create new markets.

Good innovation practices help enhance a firm's competitive advantage (e.g., [Afuah 1998](#a01); [Bharadwaj _et al._ 1993](#a16)). However, there is little theoretical work on the development of nomological relationships among information technology, service innovation and competitive advantage. Systematic empirical investigations of these relationships are also scarce and no dominant pattern has emerged ([Preissl 1999](#a71)). To address these gaps and advance understanding of information technology adoption and specific service innovation practices, we explored information technology adoption as a coordination mechanism ([Dedrick _et al._ 2003](#a37); [Galbraith 1973](#a46)), which has led to changes in innovation-related activities.

Our aim was to shed new and important light on these constructs in the financial services industry. The choice of financial services was influenced by the desire to investigate service firms in a highly competitive, dynamic and technology-driven industry. The rapidly changing business environment of the financial services sector has led to an upsurge in innovation-related activities ([Blazevic & Lievens 2004](#a18)). To this end, the objective of the present study was twofold: to assess how information technology adoption should be organized and managed to enhance the service innovation practices of the firm and evaluate how service innovation practices improve the competitive advantage of a firm. Our results may help managers to understand service innovation and resource allocation better, with a view to increasing the level of information technology adoption within a firm.

We devised a component-based structure equation model that links these constructs. Then, we conducted a survey-based study of financial firms to evaluate the validity of the linkages. The rest of the paper is organized as follows. In the next section, we assess the plausibility of information technology adoption as an antecedent to service innovation practices and introduce the associated hypotheses. Next, we examine how service innovation practices are related to competitive advantage in the context of relevant theoretical perspectives. In the methods section, the study sample of 124 firms is described and the construct measures are evaluated. Then, the relationships among these constructs are assessed and discussed. Finally, we close with a discussion of the theoretical and practical contributions of the paper.

## Literature and hypotheses

The main emphasis in the literature is on the discussion of information technology adoption and its influence on service innovation practices and competitive advantage. Based on [Rogers's (1983](#a75)) innovation diffusion theory, implementation represents the infusion stage in the process of innovation diffusion ([Cooper & Zmud 1990](#a28)). Therefore, once it has adopted and adapted a technology, a firm begins to use it in a comprehensive and integrated manner to support organizational work and innovative practices. Figure 1 presents the research framework of the present study. It shows the relationships that are hypothesized to exist among information technology adoption, service innovation practices and competitive advantage.

<figure>

![Figure 1](../p314fig1.jpg)

<figcaption>

**Figure 1\. Research framework**</figcaption>

</figure>

### Information technology adoption

Swanson ([1994](#a85)) suggested that information systems innovation among organizations can be categorized into three distinct types: innovations that occur within the information systems function (Type I), at the individual user or work group level (Type II) and at the organizational level (Type III). Consistent with the perspective of Type III innovations, we discuss and analyse information technology adoption at the organizational level and conceptualize information technology adoption based on four elements in Scott Morton's ([1995](#a81)) MIT90 model. The terms of the four elements are slightly modified as information technology infrastructure, strategic alignment, organizational structure and individual learning, without losing the original meanings. Another element in the MIT90 model, management process, is considered separately and discussed specifically with regard to service innovation, to investigate its relationship to the other elements. The four elements of information technology adoption defined in this study are discussed below.

_Information technology infrastructure_ Information technology infrastructure includes networks; management and provisioning of large-scale computing, electronic data interchange and shared databases, and research and development to identify emerging technologies ([Davenport _et al._ 1989](#a33)). Almost two-thirds of the information technology budget of an organization is spent on information technology infrastructure ([Weill & Broadbent 1998](#a94)), not only to enable the sharing of information across different departments but also to provide flexibility to respond to changes in business strategy ([Weill _et al._ 2002](#a95)). Adequate investment and management of information technology infrastructure are the foundation of information technology adoption.

_Strategic alignment_ Strategic alignment suggests that the effect of information technology on performance will depend on how well the information technology strategy and corporate strategy coincide ([Chan _et al._ 1997](#a22); [Palmer & Markus 2000](#a66)). Companies can be successful in aligning their information technology and business strategies by balancing internal and external factors as well as business and information technology domains ([Henderson & Venkatraman 1993](#a50)). A number of studies have shown that aligning information technology and business strategies is critical for successful information technology adoption and positively associated with effective organizational performance (e.g., [Chan _et al._ 1997](#a22); [Sabherwal & Kirs 1994](#a77)).

_Organizational structure_ Organizational structure specifies the formal line of communication; helps control, integrate and coordinate work activities; and defines the allocation of work roles ([Porrass & Robertson 1992](#a70)). While information technology is being adopted, organizational structure is often reexamined and adjusted to improve performance via pooled resources, innovation and collaboration across organizational boundaries ([Dewett & Jones 2001](#a39)).

_Individual learning_ For the organization to effectively take advantage of information technology, both end-users and information technology personnel must acquire new information technology-related skills and knowledge ([Grover _et al._ 1999](#a48); [Scott Morton 1995](#a81)). The acceptance of new information technology may hinge on the proper assessment and identification of organization divisions that would benefit the most, which subsequently may influence the adoption behavior of others ([Rogers 1983](#a75)). Moreover, the successful adoption of new information technology requires people in the entire organization to adapt and provide employee support and training, to reap greater benefits beyond the change in technology.

### Service innovation

Innovation is commonly defined as '_the initiation, adoption and implementation of ideas or activity that are new to the adopting organization_' (e.g., [Daft 1978](#a31); [Fichman 2001](#a42); [Pierce & Delbecq 1977](#a68)) and entails identifying and using opportunities to create new products, services, or work practices (e.g., [Tushman & Nadler 1986](#a88); [Van de Ven 1986](#a90)). When faced with keen competition, one of a firm's predominant problems is whether to pursue an aggressive growth strategy through service innovation practices. Early studies on service innovation suggested that service was in itself a product, or at least an integral part of a product and should be managed under new product development for service companies (e.g., [Easingwood 1986](#a41)). More recently, a number of studies have focused broadly on service itself, investigating issues in new service development processes, such as customer participation (e.g., [de Brentani 1989](#a36); [Magnusson _et al._ 2003](#a60); [Martin & Horne 1993](#a61), [1995](#a62)) and the importance of idea generation, screening and development (e.g., [Alam & Perry 2002](#a03); [Barczak 1995](#a09)). Others have suggested that project learning ([Blazevic & Lievens 2004](#a18); [Blazevic _et al._ 2003](#a19)) and communication ([Lievens _et al._ 1999](#a58)) are critical to service development. In sum, to create new markets, firms must implement specific service innovation practices to develop scalar business models, manage customer experience, monitor employee performance and provide managerial process innovation ([Atuahene-Gima, 1996](#a05); [Berry _et al._, 2006](#a14)).

Two commonly raised categories of service innovation are product innovation and process innovation (e.g., [Avlonitis _et al._ 2001](#a06); [Crawford & Benedetto 2002](#a30); [Gadrey _et al._ 1995](#a45); [Gallouj & Weinstein 1997](#a47); [Hertog 2000](#a51); [Hipp _et al._ 2000](#a52); [Lyytinen & Rose 2003](#a59); [Uchupalanan 2000](#a89)). For example, Gadrey _et al._ ([1995](#a45)) categorized four types of service innovation according to service context, namely innovations in service products, architectural innovations that bundle or un-bundle existing service products, innovations that result from the modification of an existing service product and innovations in processes and organization for an existing service product. Further, Lyytinen & Rose ([2003](#a59)) identified service process innovations as services that (1) support the administrative core (administrative process innovation), (2) support functional processes (technological processes innovation), (3) expand and support customer interfacing processes (technological service innovation) and (4) support inter-organizational processes and operations (technological integration innovation). Table 1 provides an overview of these concepts of service innovation practices. The present study divides service innovation practices into two categories: service process innovation, or changes in service delivery and/or development processes as defined by method, functionality, administration, or other features; and service product innovation, or changes in service products/offerings as defined by changes in general product features. These definitions of service innovation were selected to help us focus on examining the effect of information technology adoption on service innovation practices.

<table><caption>

**Table 1: Overview of concept matrix: service innovation practices**</caption>

<tbody>

<tr>

<th>Articles</th>

<th>Methodologies</th>

<th colspan="7">Service innovation practices</th>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Customer experience management</td>

<td>Investment in employee performance</td>

<td>Managerial process innovation</td>

<td>Brand differentiation</td>

<td>Product development/ innovation</td>

<td>Project learning and communication</td>

<td>Use of customer information</td>

</tr>

<tr>

<td>

Atuahene-Gima ([1996](#a05))</td>

<td>Survey</td>

<td>•</td>

<td>•</td>

<td>•</td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Avlonitis _et al._ ([2001](#a06))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Berry _et al._ ([2006](#a14))</td>

<td>Theoretical</td>

<td>•</td>

<td>•</td>

<td>•</td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Blazevic & Lievens ([2004](#a18))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

</tr>

<tr>

<td>

Blazevic _et al._ ([2003](#a19))</td>

<td>Case(s)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

</tr>

<tr>

<td>

Chan _et al._ ([1998](#a21))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Crawford & Di Benedetto ([2002](#a30))</td>

<td>Theoretical + Case</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

De Brentani ([1989](#a36))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

</tr>

<tr>

<td>

Drejer ([2004](#a40))</td>

<td>Theoretical</td>

<td> </td>

<td> </td>

<td>•</td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Gadrey _et al._ ([1995](#a45))</td>

<td>Theoretical</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Gallouj & Weinstein ([1997](#a47))</td>

<td>Theoretical</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Hertog ([2000](#a51))</td>

<td>Theoretical</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Hipp _et al._ ([2000](#a52))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Lievens _et al._ ([1999](#a58))</td>

<td>Case (s)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

</tr>

<tr>

<td>

Lyytinen & Rose ([2003](#a59))</td>

<td>Case (m)</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>

Magnusson _et al._ ([2003](#a60))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

</tr>

<tr>

<td>

Martin & Horne ([1993](#a61))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td>•</td>

<td> </td>

<td>•</td>

</tr>

<tr>

<td>

Martin & Horne ([1995](#a62))</td>

<td>Survey</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

</tr>

<tr>

<td>

Uchupalanan ([2000](#a89))</td>

<td>Case (m)</td>

<td> </td>

<td> </td>

<td>•</td>

<td> </td>

<td> </td>

<td> </td>

<td>•</td>

</tr>

<tr>

<td colspan="9">

_**Note**_: We use the label 'Case study (s)' to identify a single-site case study and the label 'Case Study (m)' to identify a multi-site case.</td>

</tr>

</tbody>

</table>

### Information technology adoption for service innovation practices

_Service process innovation_ Process innovation refers to the introduction of a new production method that includes a novel way of handling a commodity commercially ([Schumpeter 1934](#a80)) and can be applied to the entire value chain process, including manufacturing, data processing, distribution and service ([Zaltman _et al._ 1973](#a98)). Adopting information technology may have positive impacts on internal operational processes as well as external cross-enterprise processes that integrate other organizational and supply chain processes ([Joglekar & Yassine 2002](#a55)). The adoption of information technology enhances a company's response to customer demands with shorter delivery times ([Jackson 1990](#a54)) and enables customers to monitor their deliveries ([Tinnilä & Vepsäläinen 1995](#a87)). Externally, companies can not only improve delivery speed and progress visibility, but also take advantage of information technology in designing or modifying new service processes ([Avlonitis _et al._ 2001](#a06)), such as using Web or mobile services for customer information inquiry and consultation, enriching multi-channel purchasing features and enhancing after-sale services. Internally, information technology may enhance service development capabilities and administration efficiency to shorten product design time, reduce the number of prototypes that must be built, cut costs, improve quality ([Karagozoglu & Brown 1993](#a57)) and foster better collaboration, communication and coordination among project members ([Ozer 2000](#a65)). Therefore, we suggest that information technology adoption has positive and significant effects on service process innovation.

> H1: The higher the level of information technology adoption, the greater the level of service process innovation.

_Service product innovation_ Product innovation refers to the introduction of a new good or a new quality of an existing good ([Schumpeter, 1934](#a80)) and involves the development, production and dissemination of new consumer and capital goods and services ([Zaltman _et al._ 1973](#a98)). Compared to physical products, service products are easier to imitate and more difficult to protect under commercial patents. Even so, innovating service products is still an important task for service firms, to remain competitive. Adopting information technology provides a means for production and marketing staff to create numerous opportunities to innovate new services ([Vermeulen & Dankbaar 2002](#a93)). Using information technology applications, such as information management and business intelligence, enable employees to access past service innovation projects, thereby allowing them to learn from previous experiences and update their current market strategy. In doing so, firms are capable of developing new services that are better suited to market demand and offer better post-selling services to fit customer needs ([Demirhan _et al._ 2006](#a38); [Preissl 1999](#a71)). In particular, direct marketing and customization approaches have been widely applied in the financial sector and information technology can help firms quickly identify customer needs from customer profile analysis and frequent interactions with customers and provide customized products and/or services. Thus, we suggest that service firms that fully utilize information technology will do better in differentiating their products and providing superior services.

> H2: The higher the level of information technology adoption, the greater the level of service product innovation.

### Service innovation practices and competitive advantage

Competitive advantage can be gained when an organization produces its goods or services more cheaply than its competitors and resolves bargaining situations to its own advantage ([Bakos & Treacy 1986](#a07)). Recent discussions on competitive advantage have broadened the scope from _value chain_ and _value creation capabilities_ perspectives (e.g., [Barney 1991](#a10); [Piccoli & Ives 2005](#a67)) and suggest that competitive advantage is gained through outstanding organizational conditions and strong value creation capabilities in a firm. That is, competitive advantage is achieved by fully deploying and using idiosyncratic, valuable and inimitable resources and capabilities ([Bhatt & Grover 2005](#a17)) and can be viewed externally as outcome performance and internally as organizational capabilities. Thus, in this study, based on the above discussion, we categorize competitive advantage as external and internal, to examine the effect of service innovation on competitive advantage externally and internally.

External competitive advantage is attainable by providing high-quality products and services to meet customer desires and being constantly aware of market changes and quick to react to trends and competitors' strategies. Firms depend on the effective use of existing assets to enhance profitability and apply innovation practices to establish their values when faced with imitation by competitors ([Roberts & Amit 2003](#a74)). The effects of service process innovation on external competitive advantage can be examined by evaluating customer satisfaction with quality, delivery time and installation assistance ([Day 1994](#a34)). Process innovation may also increase the effectiveness and efficiency of operations (e.g., [Tushman & Nadler 1986](#a88)). It follows that firms that constantly innovate service processes would excel at utilizing new marketing techniques and enhancing customer satisfaction to fulfil the constantly changing needs of their customers. Furthermore, launching new products and improving existing products help firms grow their sales and become market leaders (e.g., [Iansiti 1995](#a53)). Offering new service products to fit customer needs would enable firms to keep pace with the shifting desires of customers and help improve brand image. In addition, taking good care of customer needs leads to sustainable success in business operations ([Henard & Szymanski 2001](#a49)). We postulate that both service process innovation and service product innovation would have positive and significant effects on external competitive advantage.

> H3: The higher the level of service process innovation, the greater the level of external competitive advantage.

> H4: The higher the level of service product innovation, the greater the level of external competitive advantage.

Internal competitive advantage depends on internal resources and capabilities. It emphasizes the importance of creating and encouraging a corporate environment in which employees plan, develop and launch new, innovative services ([de Brentani & Ragot 1996](#a35)). The effects of service innovation practices on internal competitive advantage can be examined by evaluating employee job satisfaction, domain knowledge and level of creativity after new services are launched ([Atuahene-Gima 1996](#a05); [Van Riel _et al._ 2004](#a91)). Firms that continuously provide innovative services are often characterized by a service-oriented working environment, better cross-function coordination and well-defined training and learning mechanisms. Employees in such environments may be more satisfied and motivated to learn and build new knowledge ([Brown & Duguid 1991](#a20)). Whenever a new service process is provided and/or a new service product is launched, employees will need to learn about the new processes or services. Consequently, employees will tend to become more creative and acquire new knowledge ([Rubery _et al._ 2002](#a76)). Similarly, if firms launch new services as a routine practice, employees will be more able to adapt to new roles and new practices in selling the new services ([Smith _et al._ 2005](#a83)) and will be more satisfied with their work, by assuming a challenging, ever-changing role, instead of selling the same services time after time.

> H5: The higher the level of service process innovation, the greater the level of internal competitive advantage.

> H6: The higher the level of service product innovation, the greater the level of internal competitive advantage.

## Research methodology

### Operationalization of constructs

#### Information technology adoption

Information technology adoption was measured using a Likert-type summated scale, including eighteen items rated on a five-point scale (1: strongly disagree, 5: strongly agree). As explained earlier, four different sub-constructs were used to capture dimensions of the information technology adoption. Information technology infrastructure was measured using four items that refer to the firm's investment in related hardware, software, staffing and sophisticated Internet applications ([Bharadwaj 2000](#a15); [Sircar _et al._ 2000](#a82)). Strategic alignment was measured using four items that refer to the firm's alignment of its information technology strategy with its corporate strategy to achieve greatest effectiveness ([Palmer & Markus 2000](#a66); [Reich & Benbasat 1996](#a73); [Venkatraman 1989](#a92)). Organizational structure was measured using five items including organizational structural change for new business practices and for increasing employee empowerment, enabling inter-department (cross-function) integration, enhancing operations mobility and improving timely response in managerial decision making ([Flippo 1966](#a43); [Mintzberg 1979](#a63); [Porrass & Robertson 1992](#a70); [Zaltman _et al._ 1973](#a98)). Individual learning was measured using five items including the learning skills and acquired knowledge that can effectively manipulate information technology applications ([Barrett 1995](#a11); [Chonko _et al._ 2003](#a26); [Grover _et al._ 1999](#a48); [Zahra & George 2002](#a97)).

Service innovation practices

The scale of process innovation was adopted and modified mainly from [Zaltman _et al._ (1973](#a98)) and [Davenport and Short (1990](#a32)), with seven question items to measure new service processes within a firm regarding customer service, information inquiry, promotion, trade, administration and new service development. The scale of service product innovation was adapted and modified mainly from [Avlonitis _et al._ (2001](#a06)), using four items, namely, service modifications, service line extensions, service repositioning and improvements in existing services.

#### Competitive advantage

The competitive advantage scale identified two categories, external and internal advantage. The measure of external competitive advantage was mainly modified from Avlonitis _et al._ ([2001](#a06)) and Atuahene-Gima ([1996](#a05)), using three items to assess how a firm uses new services to increase competitive advantage, enter new markets and provide better service quality than competitors. The measure of internal competitive advantage was mainly adopted from Van Riel _et al._ ([2004](#a91)) and Atuahene-Gima ([1996](#a05)), with three items including improvements to employee innovation, domain knowledge and job satisfaction. Table 2 summarizes the operational definitions, sources of the variables and scale items.

<table><caption>

**Table 2: Operational Definitions of Observed Variables**</caption>

<tbody>

<tr>

<th>Variables</th>

<th>Operational definition</th>

<th>Sources</th>

</tr>

<tr>

<td rowspan="4">Information technology adoption</td>

<td>

*   Information technology infrastructure
    *   Information technology hardware establishment
    *   Information technology software purchasing and maintenance
    *   Information technology staffing
    *   Implementing new information technology applications

</td>

<td>

Sircar _et al._ ([2000](#a82));  
Bharadwaj ([2000](#a15))</td>

</tr>

<tr>

<td>

*   Strategic alignment
    *   Aligning information technology strategies to business strategies
    *   Developing information technology projects to support business strategies
    *   Updating information technology applications for business strategic goals
    *   Deploying information technology strategies for business processes

</td>

<td>

Venkatraman ([1989](#a92));  
Palmer & Markus ([2000](#a66));  
Reich & Benbasat ([1996](#a73))</td>

</tr>

<tr>

<td>

*   Organizational structure
    *   Employee empowerment
    *   Business function integration
    *   Work activities coordination
    *   Departmental operations mobility
    *   Decision making quick response

</td>

<td>

Flippo ([1966](#a43));  
Mintzberg ([1979](#a63));  
Porrass & Robertson ([1992](#a70));  
Zaltman _et al._ ([1973](#a98))</td>

</tr>

<tr>

<td>

*   Individual learning
    *   Providing information technology related training
    *   Being familiar with information technology applications
    *   Adapting to use information technology applications
    *   Possessing information technology knowledge and skills
    *   Less resistance to information technology applications

</td>

<td>

Barrett ([1995](#a11));  
Scott Morton ([1995](#a81));  
Grover _et al._ ([1999](#a48));  
Zahra & George ([2002](#a97));  
Chonko _et al._ ([2003](#a26))</td>

</tr>

<tr>

<td rowspan="2">Service  
innovation  
practices</td>

<td>

*   Process innovation
    *   New external service processes
    *   New internal service development processes
    *   New internal administration processes

</td>

<td>

Zaltman _et al._ ([1973](#a98));  
Davenport & Short ([1990](#a32))</td>

</tr>

<tr>

<td>

*   Product innovation
    *   Service modifications
    *   Service line extensions
    *   Service repositioning
    *   New service launch

</td>

<td>

Avlonitis _et al._ ([2001](#a06))</td>

</tr>

<tr>

<td rowspan="2">Competitive  
advantage</td>

<td>

*   External advantage
    *   Entering a new market
    *   Obtaining higher competitive advantage
    *   Providing better services quality than competitors

</td>

<td>

Avlonitis _et al._ ([2001](#a06));  
Atuahene-Gima ([1996](#a05))</td>

</tr>

<tr>

<td>

*   Internal advantage
    *   Increasing staff job satisfaction
    *   Enhancing staff experience and domain knowledge
    *   Uplifting staff innovative capability

</td>

<td>

Van Riel _et al._ ([2004](#a91));  
Atuahene-Gima ([1996](#a05))</td>

</tr>

</tbody>

</table>

### Instrument design

The structured questionnaire was based on academic- and practitioner-oriented literature and interviews. The data were secured by means of a four-page, self-administered questionnaire as part of a wider examination of the information technology adoption, service innovation practices and competitive advantage in the financial industry. Following the suggestions of Churchill ([1979](#a27)), existing scales were adopted, modified and extended. Information was gathered employing five-point, Likert-type scales ([Appendix A](#app)).

### Data collection and sample

Data were collected from a sample of 558 financial firms in Taiwan, drawn from the list published by the Taiwan Joint Credit Information Center. Financial firms were chosen because the highly competitive nature of their markets and their need to attain sustainable competitive advantage make them prime candidates for information technology adoption. The questionnaire was mailed to one information technology manager in each firm, accompanied by a covering letter explaining the purpose of the research and assuring respondents that answers would remain confidential. We also indicated that we would provide the summary of the survey results and a gift certificate, after receiving their responses. Even though we provided incentives, three weeks after the initial mailing, only sixty-five responses had been received. Therefore, telephone calls and fax requests were subsequently made to information technology managers of firms that had not replied. These efforts increased the total responses to 124, corresponding to a valid return rate of 22%. To examine non-response bias, following Armstrong & Overton ([1977](#a04)), a comparison of early and late respondents was performed. The first mailing was classified as early (n = 65), while the follow-up contacts were considered late (n = 59). The independent-sample t tests revealed no statistically significant differences between the two groups in terms of firm capital (p = 0.76), years the firm has been established (p = 0.78), the percentage of information technology spending to annual revenue (p = 0.56) and number of employees (p = 0.8). Because our data were self-reported, we used Harmon's one-factor test ([Podsakoff & Organ 1986](#a69)) to examine whether a common-method bias was present. The items used to measure the dependent and independent variables were entered into a single exploratory factor analysis. The results did not suggest a common-method bias, because a single factor did not emerge or one factor did not account for most of the variance ([Barua _et al._ 2004](#a13)).

## Data analysis and results

Partial least squares regression was primarily used to evaluate the research hypotheses. Whereas the typical factor-based LISREL analysis using maximum likelihood estimation emphasizes the transition from exploratory to confirmatory analysis, the partial least squares method is primary intended for causal-predictive analysis and to explain complex relationships by following a component-based strategy ([Joreskog & Wold 1982](#a56); [Rai _et al._ 2006](#a72); [Stewart & Gosain 2006](#a84)), as is the case with this research. Hence, structural equation modelling procedures implemented in _PLS Graph 3.0_ ([Chin 2001](#a24)) were used to perform a simultaneous evaluation of both the quality of measurement (the measurement model) and construct interrelationships (the structural model). Moreover, _PLS Graph 3.0_ provides the ability to model latent constructs even under conditions of non-normality and small- to medium-size samples ([Chin _et al._ 1996](#a25)). The sample of 124 cases is adequate for such analysis, satisfying the heuristic that the sample size be at least ten times the largest number of structural paths directed at any one construct.

### Sample demographics

The demographics of the firms surveyed are shown in Table 3\. Among them, 41.1% had been established for more than twenty years at the time of the survey; 28.2% had earnings of more than US$310 million annually; 51.6% had an information technology spending-to-revenue ratio of less than 5%; 43.5% had a new products or services-to-revenue ratio of less than 5%; and 32.3% had between 100 and 500 employees. Moreover, 44.4% of the firms were securities and insurance companies and 41.1% were banks and credit cooperatives.

<table><caption>

**Table 3: Demographics of the Sample Firms**</caption>

<tbody>

<tr>

<th>Variable</th>

<th>Category</th>

<th>N</th>

<th>Rate (%)</th>

</tr>

<tr>

<td rowspan="7">Years Since Established</td>

<td>Less than 3 years</td>

<td>4</td>

<td>3.2</td>

</tr>

<tr>

<td>3 to 5 years</td>

<td>7</td>

<td>5.6</td>

</tr>

<tr>

<td>6 to10 years</td>

<td>27</td>

<td>21.8</td>

</tr>

<tr>

<td>11 to 15 years</td>

<td>21</td>

<td>16.9</td>

</tr>

<tr>

<td>16 to 20 years</td>

<td>14</td>

<td>11.3</td>

</tr>

<tr>

<td>Over 20 years</td>

<td>51</td>

<td>41.1</td>

</tr>

<tr>

<td>Aggregate</td>

<td>124</td>

<td>100</td>

</tr>

<tr>

<td rowspan="8">Firm Capital (1 US dollar  
= 31.9 NT dollars)</td>

</tr>

<tr>

<td>Less than USD 3.1 millions</td>

<td>5</td>

<td>4.0</td>

</tr>

<tr>

<td>USD 3.1 millions to 31 millions</td>

<td>34</td>

<td>27.4</td>

</tr>

<tr>

<td>USD 31 millions to 93 millions</td>

<td>24</td>

<td>19.4</td>

</tr>

<tr>

<td>USD 93 millions to 155 millions</td>

<td>12</td>

<td>9.7</td>

</tr>

<tr>

<td>USD 155 millions to 310 millions</td>

<td>14</td>

<td>11.3</td>

</tr>

<tr>

<td>Over USD 310 millions</td>

<td>35</td>

<td>28.2</td>

</tr>

<tr>

<td>Aggregate</td>

<td>124</td>

<td>100</td>

</tr>

<tr>

<td rowspan="7">% of information technology spending  
comparing to Annual  
Revenue</td>

</tr>

<tr>

<td>Less than 5 %</td>

<td>64</td>

<td>51.6</td>

</tr>

<tr>

<td>6 to 10 %</td>

<td>38</td>

<td>30.6</td>

</tr>

<tr>

<td>11 to 19 %</td>

<td>13</td>

<td>10.5</td>

</tr>

<tr>

<td>20 to 29 %</td>

<td>4</td>

<td>3.2</td>

</tr>

<tr>

<td>Over 30 %</td>

<td>5</td>

<td>4.0</td>

</tr>

<tr>

<td>Aggregate</td>

<td>124</td>

<td>100</td>

</tr>

<tr>

<td rowspan="7">% of new  
products or services  
Revenue to  
annual revenue</td>

</tr>

<tr>

<td>Less than 5 %</td>

<td>54</td>

<td>43.5</td>

</tr>

<tr>

<td>6 to 10 %</td>

<td>33</td>

<td>26.6</td>

</tr>

<tr>

<td>11 to 19 %</td>

<td>12</td>

<td>9.7</td>

</tr>

<tr>

<td>20 to 29 %</td>

<td>11</td>

<td>8.9</td>

</tr>

<tr>

<td>Over 30 %</td>

<td>14</td>

<td>11.3</td>

</tr>

<tr>

<td>Aggregate</td>

<td>124</td>

<td>100</td>

</tr>

<tr>

<td rowspan="8">No. of Employees</td>

</tr>

<tr>

<td>Less than 100</td>

<td>34</td>

<td>27.4</td>

</tr>

<tr>

<td>100 to 499</td>

<td>40</td>

<td>32.3</td>

</tr>

<tr>

<td>500 to 999</td>

<td>15</td>

<td>12.1</td>

</tr>

<tr>

<td>1000 to 1999</td>

<td>11</td>

<td>8.9</td>

</tr>

<tr>

<td>2000 to 2999</td>

<td>8</td>

<td>6.5</td>

</tr>

<tr>

<td>Over 3000</td>

<td>16</td>

<td>12.9</td>

</tr>

<tr>

<td>Aggregate</td>

<td>124</td>

<td>100</td>

</tr>

<tr>

<td rowspan="6">Industry</td>

</tr>

<tr>

<td>Banking and co-operative</td>

<td>51</td>

<td>41.1</td>

</tr>

<tr>

<td>Insurance and securities</td>

<td>55</td>

<td>44.4</td>

</tr>

<tr>

<td>Investment bank</td>

<td>7</td>

<td>5.6</td>

</tr>

<tr>

<td>Others</td>

<td>11</td>

<td>8.8</td>

</tr>

<tr>

<td>Aggregate</td>

<td>124</td>

<td>100</td>

</tr>

</tbody>

</table>

### Measurement properties

The expected factor structure was obtained in all eight constructs (see [Appendix B](#appb)). Scale reliability was tested and the Cronbach alpha values were in the range 0.85 to 0.93 for the eight constructs, indicating a high internal consistency of measure reliability ([Nunnally 1978](#a64)). Composite reliability was then assessed by examining the ?<sub>c</sub> values for the constructs, all of which were above the suggested threshold of 0.7\. The properties of the measurement model are summarized in Table 4 and the correlation matrix and the statistics of the observed variables are shown in Table 5\. The average variance extracted values (AVEs) were all above the recommended threshold of .50 ([Barclay _et al._ 1995](#a08)) and the square root of those values were all greater than the construct correlations (the off-diagonal entries in Table 5) ([Fornell & Larcker 1981](#a44)). The convergent and discriminant validity tests were both satisfied.

<table><caption>

**Table 4\. Summary of constructs**</caption>

<tbody>

<tr>

<th>Construct name and identifier</th>

<th>Items</th>

<th>Cronbach alpha</th>

<th>Composite reliability (?<sub>c</sub>)</th>

</tr>

<tr>

<td>Information technology infrastructure (ITI)</td>

<td>4</td>

<td>0.85</td>

<td>0.90</td>

</tr>

<tr>

<td>Strategic alignment (SA)</td>

<td>4</td>

<td>0.86</td>

<td>0.91</td>

</tr>

<tr>

<td>Organizational structure (OS)</td>

<td>5</td>

<td>0.86</td>

<td>0.90</td>

</tr>

<tr>

<td>Individual learning (IL)</td>

<td>5</td>

<td>0.87</td>

<td>0.91</td>

</tr>

<tr>

<td>Process innovation (PRI)</td>

<td>7</td>

<td>0.92</td>

<td>0.94</td>

</tr>

<tr>

<td>Product innovation (PDI)</td>

<td>4</td>

<td>0.93</td>

<td>0.95</td>

</tr>

<tr>

<td>External advantage (EA)</td>

<td>3</td>

<td>0.92</td>

<td>0.95</td>

</tr>

<tr>

<td>Internal advantage (IA)</td>

<td>3</td>

<td>0.87</td>

<td>0.92</td>

</tr>

</tbody>

</table>

<table><caption>

**Table 5\. Means, SD, correlations and average variance extracted (n = 124)**</caption>

<tbody>

<tr>

<th>Construct</th>

<th>Mean</th>

<th>SD</th>

<th>AVE</th>

<th>(1)</th>

<th>(2)</th>

<th>(3)</th>

<th>(4)</th>

<th>(5)</th>

<th>(6)</th>

<th>(7)</th>

<th>(8)</th>

</tr>

<tr>

<td>ITI (1)</td>

<td>3.84</td>

<td>0.71</td>

<td>0.69</td>

<td>

**0.83**</td>

</tr>

<tr>

<td>SA (2)</td>

<td>3.87</td>

<td>0.69</td>

<td>0.72</td>

<td>0.603**</td>

<td>

**0.84**</td>

</tr>

<tr>

<td>OS (3)</td>

<td>3.61</td>

<td>0.64</td>

<td>0.65</td>

<td>0.383**</td>

<td>0.691**</td>

<td>

**0.80**</td>

</tr>

<tr>

<td>IL (4)</td>

<td>3.50</td>

<td>0.67</td>

<td>0.68</td>

<td>0.448**</td>

<td>0.573**</td>

<td>0.665**</td>

<td>

**0.82**</td>

</tr>

<tr>

<td>PRI (5)</td>

<td>3.69</td>

<td>0.72</td>

<td>0.69</td>

<td>0.373**</td>

<td>0.657**</td>

<td>0.574**</td>

<td>0.480**</td>

<td>

**0.83**</td>

</tr>

<tr>

<td>PDI (6)</td>

<td>3.41</td>

<td>0.80</td>

<td>0.83</td>

<td>0.283**</td>

<td>0.505**</td>

<td>0.550**</td>

<td>0.450**</td>

<td>0.775**</td>

<td>

**0.91**</td>

</tr>

<tr>

<td>EA (7)</td>

<td>3.48</td>

<td>0.82</td>

<td>0.87</td>

<td>0.312**</td>

<td>0.509**</td>

<td>0.467**</td>

<td>0.440**</td>

<td>0.715**</td>

<td>.745**</td>

<td>

**0.93**</td>

</tr>

<tr>

<td>IA (8)</td>

<td>3.51</td>

<td>0.68</td>

<td>0.80</td>

<td>0.310**</td>

<td>0.545**</td>

<td>0.496**</td>

<td>0.440**</td>

<td>0.683**</td>

<td>0.613**</td>

<td>0.633**</td>

<td>

**0.89**</td>

</tr>

<tr>

<td>

_**Notes:**_</td>

<td colspan="11">

_a) Figures in shaded diagonal are values of the square root of the AVE  
b) * p < .05, ** p < .01_</td>

</tr>

</tbody>

</table>

Information technology adoption was modelled as a reflective second-order construct comprised of four first-order dimensions: information technology infrastructure, strategic alignment, organizational structure and individual learning. The first-order dimensions of information technology adoption are complementary, thus a reflective second-order construct is appropriate for capturing the complementarities ([Tanriverdi 2005](#a86)). Figure 2 shows the results of the partial least squares estimation, with loadings for information technology adoption, service innovation practices and competitive advantage. Of the thirty-five loadings, thirty were above 0.8, indicating that each measure was accounting for 50% or more of the variance of the underlying latent variable. The path coefficients for the research constructs are expressed in a standardized form. Five of the six path coefficients were above 0.3, with the lowest path coefficient being 0.23, indicating that they are meaningful and significant ([Chin 1998](#a23)). The significance levels of paths in the research model were determined using the partial least squares _jackknife_ resampling procedures ([Sambamurthy & Chin 1994](#a78)). Overall, the results suggest a satisfactory fit of the model to the data. As for R-square values, information technology adoption explains 42% of the variance in process innovation and 31% of the variance in product innovation; service innovation practices explains 60% of the variance in external advantage and 49% of the variance in internal advantage. They are all significant at p < .01.

<figure>

![Figure 2](../p314fig2.jpg)

<figcaption>

**Figure 2: Research framework linking information technology adoption, service Innovation practices and competitive advantage**  
**Notes:** 
a) * these values indicate loadings of the indicators for the reflective construct  
b) The paths represent standardized beta estimates. Numbers in the parentheses indicate the standard errors obtained via jackknife estimates. All R2 values are significant at p <.01</figcaption>

</figure>

### Hypotheses testing

Hypotheses were tested within the structural model shown in Figure 2\. The path coefficients and T-values of the structural links are listed in Table 6\. All of the six links were positive and significant in supporting the six proposed hypotheses.

> *   H1, that information technology adoption has a positive and significant effect on service innovation in process, is supported (path = 0.65, t = 9.28, p < .01).
> *   H2, that information technology adoption has a positive and significant effect on service innovation in product, is supported (path = 0.56, t = 7.56, p < .01).
> *   H3, that service innovation in process has a positive and significant effect on external competitive advantage, is supported (path = 0.34, t = 3.57, p < .01).
> *   H4, that service innovation in product has a positive and significant effect on external competitive advantage, is supported (path = 0.48, t = 4.80, p < .01).
> *   H5, that service innovation in process has a positive and significant effect on internal competitive advantage, is supported (path = 0.52, t = 4.06, p < .01).
> *   H6,that service innovation in product has a positive and significant effect on internal competitive advantage, is supported (path = 0.23, t = 1.98, p < .05).

## Discussion and conclusions

Implementing new information technology applications to enable a competitive edge has become a core and important strategy in most contemporary corporations. Prior studies have suggested that information technology plays a fundamental role in a firm's ability to enhance business performance through innovations in products, channels and customer segments (e.g., [Sambamurthy _et al._ 2003](#a79)). This study developed a research framework and empirically investigated the effect of information technology adoption on competitive advantage through service innovation practices. We highlighted two service innovation practices in the form of process innovation and product innovation. The implementation of these innovation activities requires the coordination of related and complementary resources across the firm's business units. Building on previous research, this study conceptualized information technology as a major coordination mechanism ([Tanriverdi 2005](#a86)). Based on the MIT90 model ([Scott Morton 1995](#a81)), we used the information technology adoption construct to conceptualize the relationship among information technology infrastructure, strategic alignment, management processes, organizational structure and individual learning and to explain how such mechanisms can sustain and enhance service innovation practices in financial firms. Our research model and the associated hypotheses add detail to the prevailing understanding of critical linkages between information technology adoption and competitive advantage. With all hypotheses supported, the empirical results provide strong overall validation and point to the important role of information technology adoption that coordinates four elements to improve the implementation of service innovation practices. Further, the R-square values of service process innovation (.42) and service product innovation (.31) indicated that information technology adoption was well chosen to interpret the causal relationship with service innovation practices. We showed that continuous investments in information technology resources is a desired approach in engaging service innovation practices and firms should follow-up by re-investigating other issues in strategy alignment, structure adjustment and individual learning. Moreover, our results showed that service innovation practices have positive and significant effects on competitive advantage. The R-square values of external competitive advantage (.60) and internal competitive advantage (.49) indicate that service process and product innovations interpret well the effects on obtaining and retaining competitive advantage. Managers must pay special attention to how service innovation, in conjunction with suitable processes and products, can enable all aspects of innovation interactions between the external and internal aspects of firms to obtain superior competitive performance. Hence, the nomological relationships among information technology adoption, service innovation practices and competitive advantage constructs were demonstrated in the context of financial firms and the results suggest that service innovation practices serve as a catalyst in the information technology:performance relationship.

### Implications for research

The goal of this paper was to develop a theoretical perspective for understanding the links among information technology adoption, service innovation practices and competitive advantage. Our results have three significant implications. First, we provide an organization-wide perspective about information technology adoption that is valid for the enterprise, business unit and process levels in a firm. We propose that the value-added role of information technology adoption lies in enabling a coordination mechanism that shapes a firm's capacity to launch frequent and varied innovation practices. Based on the MIT90 model, information technology adoption is coordinated and accessed by elements of information technology infrastructure, strategic alignment, organizational structure and employee learning. We discussed management processes with a specific focus on service innovation practices and investigated its relationship with others. This conceptualization has significant implications on how researchers should think about the valuation of information technology adoption for service innovation practices. Second, our research highlights an integrated perspective to link information technology adoption, service innovation practices and competitive advantage. In particular, we highlighted two service innovation practice dimensions, namely, process innovation and product innovation. We propose that service innovation practices are important because they visualize how firms continually develop their capabilities and focus on their process and product to shape their strategy. Furthermore, service innovation practices capture the interactions among information technology infrastructure, strategic alignment, organizational structure and employee learning in shaping competitive advantage. Attention to the information technology adoption and service innovation practices in our model will be important for researchers. However, further research is needed to understand the influence of information technology adoption and service innovation practices on overall business functions and how firms could direct such processes effectively. Finally, our conceptualizations about service innovation practices illustrate the complementarity between information technology adoption and competitive advantage. We argue that service process innovation and service product innovation are the key enablers to competitive advantage. Our research model suggests that gaining the competitive advantage will require attention to both service process innovation (in service development, service promotion and post-sales services) and service product innovation (in service modification, line extension, repositioning and improvements to existing services). In addition, researchers should examine the nature of organization designs, governance structures and managerial skills that will foster such innovation practices and facilitate the development of product and strategic processes innovation described in our model.

### Implications for practice

Given the critical role of information technology in service innovation practices, it is important to understand the implications of our findings for practice. First, an understanding of the key service innovation practices affecting competitive advantage will put practitioners in a better position to develop appropriate strategies for resource deployment and, consequently, enhance its advantage. Financial firms need to continue to emphasize service innovation to retain customers and employees. They should pull more resources into innovation programs and campaigns and foster closer relationships with customers to identify market opportunities and design new services accordingly. Second, information technology plays a critical role in the implementation of innovation practices. Given that dimensions, such as information technology infrastructure, strategic alignment, organizational structure and individual learning of information technology adoption, significantly affect service innovation, it is imperative for top management to carefully consider the role of information technology managers in innovation initiatives. Before beginning major service innovation programmes, managers may want to think about implementing managerial mechanisms that will improve information technology adoption. Similarly, information technology managers are often faced with supporting organizational service innovation programmes and having a managerial mechanism in place can guide them in adopting guidelines and managerial postures that will ensure successful information technology adoption. Third, organizations may want to implement a two-pronged _process innovation and product change_ strategy in dealing with the service innovation challenges posed by the use of information technology, because both of these factors will yield significant competitive advantage. Additionally, the adoption of information technology will be more challenging for managers operating in environments with a high level of supplier interdependence and intense information technology activity. Careful consideration must be given to planning and implementing information technology in such environments. Finally, the study provides a basis for managers to think about the types of competitive advantage and should assure top management that investments in information technology for service innovation practices are worthwhile.

### Limitations and future research

A few limitations should be kept in mind in interpreting the findings of this study. This research is subject to some data-related limitations. First, all of the data were self-reported from one information technology manager in each of the surveyed firms, which could potentially induce certain subjective biases. In this study, the information technology managers' perspectives on firm practices in information technology adoption were well represented. However, their views may not exactly represent the extent of practices in service innovation and competitive advantage. Still, we believe that the respondents, as managers, must have a certain level of awareness about their organization's practices and performance. Second, all of the companies were financial firms located in Taiwan. Therefore, the results should be interpreted with caution when considering other industries or regions.

Future research should consider information technology adoption from a different perspective, to investigate how using information technology applications in workflow and project management, communication and coordination and knowledge management would affect service innovation practices and performance in different service design stages (e.g., idea generation, service specification and modification and new service launch. Also, a cross-industry comparison study of information technology adoption for service innovation practices to examine whether there are different influences for different industries or service sectors would also greatly contribute to the field.

## Acknowledgements

The authors acknowledge Prof. Wynne W. Chin of the University of Houston for his support of the PLS software and Mr. Calvin J. Hsu for his assistance in the XHTML paper formatting.

## References

*   <a id="a01"></a>Afuah, A. (1998). _Innovation management: strategies, implementation and profits._ New York, NY: Oxford University Press.
*   <a id="a02"></a>Agarwal, R. & Sambamurthy, V. (2002). Principles and models for organizing the information technology function. _Minformation systems Quarterly Executive_, **1**(1), 1-16.
*   <a id="a03"></a>Alam, I. & Perry, C. (2002). A customer-oriented new service development process. _Journal of Services Marketing_, **16**(6), 515-534.
*   <a id="a04"></a>Armstrong, J.S. & Overton, T.S. (1977). Estimating non-response bias in mail surveys. _Journal of Marketing Research_, **14**(3), 396-402.
*   <a id="a05"></a>Atuahene-Gima, K. (1996). Differential potency of factors affecting innovation performance in manufacturing and services firms in Australia. _Journal of Product Innovation Management_, **13**(1), 35-52.
*   <a id="a06"></a>Avlonitis, G.J., Papastathopoulou, P. G. & Gounaris, S.P. (2001). An empirically-based typology of product innovativeness for new financial services: success and failure scenarios. _Journal of Product Innovation Management_, **18**(5), 324-342.
*   <a id="a07"></a>Bakos, J. Y. & Treacy, M. E. (1986). Information technology and corporate strategy: a research perspective. _Minformation systems Quarterly_, **10**(2), 107-119.
*   <a id="a08"></a>Barclay, D. W., Thompson, R. & Higgins, C. (1995). The partial least squares (PLS) approach to causal modeling: personal computer adoption and use an illustration. _Technology Studies_, **2**(2), 285-309.
*   <a id="a09"></a>Barczak, G. (1995). New product strategy, structure, process and performance in the telecommunication industry. _Journal of Product Innovation Management_, **12**(3), 224-234.
*   <a id="a10"></a>Barney, J. (1991). Firm resource and sustained competitive advantage. _Journal of Management_, **17**(1), 99-120.
*   <a id="a11"></a>Barrett, F. (1995). Create appreciative learning culture. _Organizational Dynamics_, **24**(2), 36-49.
*   <a id="a12"></a>Barua, A. & Mukhopadhyay, T. (2000). Business value of information technologies: past, present and future. In R.W. Zmud (Ed.), _Framing the domains of information technology management: projecting the future through the past_ (pp. 65-84). Cincinnati: Pinnaflex Educational Resources.
*   <a id="a13"></a>Barua, A., Konana, P. & Whinston, A. B. (2004). An empirical investigation of net-enabled business value. _Minformation systems Quarterly_, **28**(4), 585-620.
*   <a id="a14"></a>Berry, L.L., Shankar, V., Parish, J.T., Cadwallader, S. & Dotzel, T. (2006). Creating new markets through service innovation. _Sloan Management Review_, **47**(2), 56-63.
*   <a id="a15"></a>Bharadwaj, A. S. (2000). A resource-based perspective on information technology capability and firm performance: an empirical investigation. _Minformation systems Quarterly_, **24**(1), 169-196.
*   <a id="a16"></a>Bharadwaj, S. G., Varadarajan, P. R. & Fahy, J. (1993). Sustainable competitive advantage in service industries: a conceptual model and research propositions. _Journal of Marketing_, **57**(4), 83-99.
*   <a id="a17"></a>Bhatt, G. & Grover, V. (2005). Types of information technology capabilities and their role in competitive advantage: an empirical study. _Journal of Management Information Systems_, **22**(2), 253-277.
*   <a id="a18"></a>Blazevic, V. & Lievens, A. (2004). Learning during the new financial service innovation process: antecedents and performance effects. _Journal of Business Research_, **57**(4), 374-391.
*   <a id="a19"></a>Blazevic, V., Lievens, A. & Klein, E. (2003). Antecedents of project learning and time-to-market during new mobile service development. _International Journal of Service Industry Management_, **14**(1), 120-147.
*   <a id="a20"></a>Brown, J. S. & Duguid, P. (1991). Organizational learning and communities-of-practice: toward a unified view of working, learning and innovating. _Organization Science_, **2**(1), 40-57.
*   <a id="a21"></a>Chan, A., Go, F. M. & Pine, R. (1998). Service innovation in Hong Kong: attitudes and practice. _The Service Industries Journal_, **18**(2), 112-124.
*   <a id="a22"></a>Chan, Y. E., Huff, S. L., Barclay, D. W. and Copeland, D. G. (1997). Business strategic orientation, information system strategic orientation and strategic alignment. _Information Systems Research_, **8**(2), 125-150.
*   <a id="a23"></a>Chin, W. W. (1998). Issues and opinion on structure equation modeling. _Minformation systems Quarterly_, **22**(1), vii-xvi.
*   <a id="a24"></a>Chin, W. W. (2001). _PLS-Graph manual. Version 3.0_. Unpublished manuscript.
*   <a id="a25"></a>Chin, W. W., Marcolin, B. L. & Newsted, P. R. (1996). A partial least squares latent variable modeling approach for measuring interaction effects: results from a Monte Carlo simulation study and voice mail emotion/adoption study. In J. I. DeGross, A. Srinivasan & S. L. Jarvenpaa (Eds.), _Proceedings of the Seventeenth International Conference on Information Systems_ (pp. 21-41). Cleveland, OH: Association for Information Systems.
*   <a id="a26"></a>Chonko, L. B., Dubinsky, A. J., Jones, E. & Roberts, J. A. (2003). Organizational and individual learning in the sales force: an agenda for sales research. _Journal of Business Research_, **56**(12), 935-946.
*   <a id="a27"></a>Churchill, G. (1979). A paradigm for developing better measures of marketing constructs. _Journal of Marketing Research_, **16**(1), 64-73.
*   <a id="a28"></a>Cooper, R. B. & Zmud, R. W. (1990). Information technology implementation research: a technology diffusion approach. _Management Science_, **34**(2), 123-139.
*   <a id="a29"></a>Corso, M. & Paolucci, E. (2001). Fostering innovation and knowledge transfer in product development through information technology. _International Journal of Technology Management_, **22**(1/2/3), 126-148.
*   <a id="a30"></a>Crawford, M. C. & Di Benedetto, A. (2002). _New Product Management_, 7th edition, Boston, MA: McGraw Hill.
*   <a id="a31"></a>Daft, R. L. (1978). A dual-core model of organizational innovation. _Academy of Management Journal_, **21**(2), 193-210.
*   <a id="a32"></a>Davenport, T. H. & Short, J. E. (1990). The new industrial engineering: information technology and business process redesign. _Sloan Management Review_, **31**(4), 11-27.
*   <a id="a33"></a>Davenport, T., Hammer, M. & Metsisto, T. (1989). How executives can shape their companies' information systems. _Harvard Business Review_, **67**(5), 130-134.
*   <a id="a34"></a>Day, G. S. (1994). The capabilities of market-driven organizations. _Journal of Marketing_, **58**(4), 37-52.
*   <a id="a35"></a>De Brentani, U. & Ragot, E. (1996). Developing new business-to-business professional service: what factors impact performance? _Industrial Marketing Management_, **25**(6), 517-531.
*   <a id="a36"></a>De Brentani, U. (1989). Success and failure in new industrial services. _Journal of Product Innovation Management_, **6**(6), 239-258.
*   <a id="a37"></a>Dedrick, J., Gurbaxani, V. & Kraemer, K. L. (2003). Information technology and economic performance: a critical review of the empirical evidence. _ACM Computing Surveys_, **35**(1), 1-28.
*   <a id="a38"></a>Demirhan, D., Jacob, V. & Raghunathan, S. (2006). Information technology investment strategies under declining technology cost. _Journal of Management Information Systems_, **22**(3), 321-350.
*   <a id="a39"></a>Dewett, T. & Jones, G. R. (2001). The role of information technology in the organization: a review, model and assessment. _Journal of Management_, **27**(3), 313-346.
*   <a id="a40"></a>Drejer, I. (2004). Identifying innovation in surveys of services: a Schumpeterian perspective. _Research Policy_, **33**(3), 551-562.
*   <a id="a41"></a>Easingwood, C. J. (1986). New product development for services companies. _Journal of Product Innovation Management_,**3**(4), 264-275.
*   <a id="a42"></a>Fichman, R. G. (2001). The role of aggregation in the measurement of information technology-related organizational innovation. _Minformation systems Quarterly_, **25**(4), 427-455.
*   <a id="a43"></a>Flippo, E. B. (1966). _Management: a behavioral approach_. Boston. MA: Allyn and Bacon.
*   <a id="a44"></a>Fornell, C. & Larcker, D. (1981). Evaluating structural equation models with unobservable variables and measurement error. _Journal of Marketing Research_, **18**(1), 39-50.
*   <a id="a45"></a>Gadrey, J., Gallouj, F. & Weinstein, O. (1995). New modes of innovation: how services benefit industry. _International Journal of Service Industry Management_, **6**(3), 4-16.
*   <a id="a46"></a>Galbraith, J.R. (1973). _Designing complex organizations_. Reading, MA: Addison-Wesley
*   <a id="a47"></a>Gallouj, F. & Weinstein, O. (1997). Innovation in services. _Research Policy_, **26**(4/5), 537-556.
*   <a id="a48"></a>Grover, V., Fiedler, K. D. & Teng, J. T. C. (1999). The role of organizational and information technology antecedents in reengineering initiation behavior. _Decision Sciences_, **30**(3), 749-781.
*   <a id="a49"></a>Henard, D. H. & Szymanski, D. M. (2001). Why some new products are more successful than others. _Journal of Marketing Research_, **38**(3), 362-375.
*   <a id="a50"></a>Henderson, J. C. & Venkatraman, N. (1993). Strategic alignment: leveraging information technology for transforming organizations. _IBM Systems Journal_, **38**(2/3), 472-484.
*   <a id="a51"></a>Hertog, P. D. (2000). Knowledge-intensive business services as co-producers of innovation. _International Journal of Innovation Management_, **4**(4), 491-528.
*   <a id="a52"></a>Hipp, C., Tether, B. & Miles, I. (2000). The incidence and effects of innovation in services: evidence from Germany. _International Journal of Innovation Management_, **4**(4), 417-453.
*   <a id="a53"></a>Iansiti, M. (1995). Shooting the rapids: managing product development in turbulent environments. _California Management Review_, **38**(1), 37-58.
*   <a id="a54"></a>Jackson, C. (1989). Building a competitive advantage through information technology. _Long Range Planning_, **22**(4), 29-39.
*   <a id="a55"></a>Joglekar, N. R. & Yassine, A. (2002). Management of information technology driven product development processes. In T. Boone & R. Ganeshan (Eds.), _New directions in supply-chain management_. (pp. 125-152 )New York, NY: Amacon Press.
*   <a id="a56"></a>Joreskog, K. G. & Wold, H. (1982). The ML and PLS techniques for modeling with latent variables: historical and comparative aspects. In H. Wold & K. Joreskog (Eds.), _Systems under indirect observation: causality, structure, prediction_ (Vol. I). (pp. 263-270 ) Amsterdam: North-Holland.
*   <a id="a57"></a>Karagozoglu, N. & Brown, W. B. (1993). Time-based management of the new product development process. _Journal of Product Innovation Management_, **10**(3), 204-215.
*   <a id="a58"></a>Lievens, A., Moenaert, R. K. & S&#8217;Jegers, R. (1999). Linking communication to innovation success in the financial services industry: a case study analysis. _International Journal of Service Industry Management_, **10**(1), 23-47.
*   <a id="a59"></a>Lyytinen, K. & Rose, G. M. (2003). The disruptive nature of information technology innovations: the case of internet computing in systems development organizations. _Minformation systems Quarterly_, **27**(4), 557-595.
*   <a id="a60"></a>Magnusson, P.R., Mathing, J. & Kristensson, P. (2003). Managing user involvement in service innovation. _Journal of Service Research_, **6**(2), 111-124.
*   <a id="a61"></a>Martin, C.R. & Horne, D.A. (1993). Services innovation: successful versus unsuccessful firms. _International Journal of Service Industry Management_, **4**(1), 49-65.
*   <a id="a62"></a>Martin, C.R. & Horne, D.A. (1995). Level of success inputs for service innovations in the same firm. _International Journal of Service Industry Management_, **6**(4), 40-56.
*   <a id="a63"></a>Mintzberg, H. (1979). _The structuring of organizations_. Englewood Cliffs, NJ: Prentice Hall.
*   <a id="a64"></a>Nunnally, J. C. (1978). _Psychometric theory_ (2nd Edition), New York, NY: McGraw-Hill.
*   <a id="a65"></a>Ozer, M. (2000). Information technology and new product development: opportunities and pitfalls. _Industrial Marketing Management_, **29**(5), 387-396.
*   <a id="a66"></a>Palmer, J. W. & Markus, L. M. (2000). The performance impacts of quick response and strategic alignment in specialty retailing. _Information System Research_, **11**(3), 241-259.
*   <a id="a67"></a>Piccoli, G. & Ives, B. (2005). information technology-dependent strategic initiatives and sustained competitive advantage: a review and synthesis of the literature. _Minformation systems Quarterly_, **29**(4), 747-776.
*   <a id="a68"></a>Pierce, J. L. & Delbecq, A. L. (1977). Organization structure, individual attitudes and innovation. _Academy of Management Review_, **2**(1), 26-37.
*   <a id="a69"></a>Podsakoff, P. M. & Organ, D. W. (1986). Self-reports in organizational research: problems and prospects. _Journal of Management_, **12**(4), 531-544.
*   <a id="a70"></a>Porrass, J. I. & Robertson, P. J. (1992). Organizational development: theory, practice and research. In M. Dunnette & L. M. Hough (Eds.), _Handbook of industrial and organizational psychology_, (pp. 719-822 ) Palo Alto: Consulting Psychologist Press.
*   <a id="a71"></a>Preissl, B. (1999). Service innovation: what makes it different? Empirical evidence from Germany. In J. S. Metcalfe & I. Miles (Eds), _Innovation systems in the service economy: measurement and case study analysis_ (Chapter 7). (pp. 125-147) Boston, MA: Kluwer Academic Publishers.
*   <a id="a72"></a>Rai, A., Patnayakuni, R. & Seth, N. (2006). Firm performance impacts of digitally enabled supply chain integration capabilities. _Minformation systems Quarterly_, **30**(2), 225-246.
*   <a id="a73"></a>Reich, B. H. & Benbasat, I. (1996). Measuring the linkage between business and information technology objectives. _Minformation systems Quarterly_, **20**(1), 55-81.
*   <a id="a74"></a>Roberts, P. W. & Amit, R. (2003). The dynamics of innovative activity and competitive advantage: the case of Australian retail banking 1981 to 1995\. _Organization Science_, **14**(2), 107-122.
*   <a id="a75"></a>Rogers, E. M. (1983). _Diffusion of innovations_. New York, NY: Press.
*   <a id="a76"></a>Rubery, J., Earnshaw, J., Marchington, M., Cooke, F. L. & Vincent, S. (2002). Changing organizational forms and the employment relationship. _Journal of Management Studies_, **39**(5), 645-672.
*   <a id="a77"></a>Sabherwal, R. & Kirs, P. (1994). The alignment between organizational critical success factors and information technology capability in academic institutions. _Decision Sciences_, **25**(2), 301-330.
*   <a id="a78"></a>Sambamurthy, V. & Chin, W. W. (1994). The effects of group attitudes toward alternative GDSS designs on the decision-making performance of computer-supported groups. _Decision Sciences_, **25**(2), 215-241.
*   <a id="a79"></a>Sambamurthy, V., Bharadwaj, A. & Grover, V. (2003). Shaping agility through digital options: reconceptualizing the role of information technology in contemporary firms. _Minformation systems Quarterly_, **27**(2), 237-263.
*   <a id="a80"></a>Schumpeter, J. A. (1934). _The theory of economic development: an inquiry into profits, capital, credit, interest and the business cycle_. Cambridge, MA: Harvard University Press.
*   <a id="a81"></a>Scott Morton, M. (1995). _The corporations of the 1990s_. New York, NY: Oxford University Press.
*   <a id="a82"></a>Sircar, S., Turnbow, J. L. & Bordoloi, B. (2000). A frame work for assessing the relationship between information technology investments and firm performance. _Journal of Management Information Systems_, **16**(4), 69-97.
*   <a id="a83"></a>Smith, K. G., Collins, C. J. & Clark, K. D. (2005). Existing knowledge, knowledge creation capability and the rate of new product introduction in high-technology firms. _Academy of Management Journal_, **48**(2), 346-357.
*   <a id="a84"></a>Stewart, K. J. & Gosain, S. (2006). The impact of ideology on effectiveness in open source software development teams. _Minformation systems Quarterly_, **30**(2), 291-314.
*   <a id="a85"></a>Swanson, E. B. (1994). Information systems innovation among organizations. _Management Science_, **40**(9), 1069-1088.
*   <a id="a86"></a>Tanriverdi, H. (2005). Information technology relatedness, knowledge management capability and performance of multibusiness firms. _Minformation systems Quarterly_, **29**(2), 311-334.
*   <a id="a87"></a>Tinnilä M. & Veps?l?inen, A. P. J. (1995). A model for strategic repositioning of service processes. _International Journal of Service Industry Management_, **6**(4), 57-80.
*   <a id="a88"></a>Tushman, N. L. & Nadler, D. A. (1986). Organizing for innovation. _California Management Review_, **28**(3), 74-92.
*   <a id="a89"></a>Uchupalanan, K. (2000). Competition and information technology-based innovation in banking services. _International Journal of Innovation Management_, **4**(4), 455-489.
*   <a id="a90"></a>Van de Ven, A. H. (1986). Central problems in the management of innovation. _Management Science_, **32**(5), 590-607.
*   <a id="a91"></a>Van Riel, A. C. R., Lemmink, J. & Ouwersloot, H. (2004). High-technology service innovation success: a decision-making perspective. _Journal of Product Innovation Management_, **21**(5), 348-359.
*   <a id="a92"></a>Venkatraman, N. (1989). Strategic orientation of business enterprise: the construct, dimensionality and measurement. _Management Science_, **35**(8), 942-962.
*   <a id="a93"></a>Vermeulen, P. & Dankbaar, B. (2002). The organization of product innovation in the financial sector. _Service Industries Journal_, **22**(3), 77-98.
*   <a id="a94"></a>Weill, P. & Broadbent, M. (1998). _Leveraging the new infrastructure: how market leaders capitalize on information technology_. Boston, MA: Harvard Business School Press.
*   <a id="a95"></a>Weill, P., Subramani, M. & Broadbent, M. (2002). Building information technology infrastructure for strategic agility. _Sloan Management Review_, **44**(1), 57-65.
*   <a id="a96"></a>Xu, H., Sharma, S. K. & Hackney, R. (2005). Web services innovation research: towards a dual-core model. _International Journal of Information Management_, **25**(4), 321-334.
*   <a id="a97"></a>Zahra, S. A. & George, G. (2002). Absorptive capacity: a review, reconceptualization and extension. _Academy of Management Review_, **27**(2), 185-203.
*   <a id="a98"></a>Zaltman, G., Duncan, R. & Holbek, J. (1973). _Innovation and organizations_. New York, NY: Wiley.

* * *

## <a id="app"></a>APPENDIX A

### Survey measurement scales

<table>

<tbody>

<tr>

<td>Strongly disagree = 1</td>

<td>Disagree = 2</td>

<td>Neither agree nor disagree = 3</td>

<td>Agree = 4</td>

<td>Strongly agree = 5</td>

</tr>

</tbody>

</table>

> Please indicate, on a scale of 1 to 5, the degree to which you agree or disagree with the following statements.

**_Information technology adoption questionnaires_**

<u>_Information technology infrastructure (ITI)_</u>

> For the past few years, our company

> ITI1\. has allocated a generous budget for purchasing information technology hardware.  
> ITI2\. has allocated a generous budget for purchasing information technology software.  
> ITI3\. has emphasized information technology staffing and training.  
> ITI4\. has embraced sophisticated Internet applications.

<u>_Strategic alignment (SA)_</u>

> For the past few years,

> SA1\. our information technology capability has supported business strategies that strengthen customer service.  
> SA2\. our information technology projects have been implemented in compliance with business strategies.  
> SA3\. our information technology applications have supported business strategies to improve process management.  
> SA4\. our information technology applications have supported business strategies to improve product/service offerings.

<u>_Organizational structure (OS)_</u>

> For the past few years, our organizational structure, by adopting new information technology systems and applications,

> OS1\. has been changed to enhance employee empowerment.  
> OS2\. has been changed to enable inter-department (cross-function) integration.  
> OS3\. has been adjusted for new business practices.  
> OS4\. has been changed to increase operations mobility.  
> OS5\. has been changed to help managers make more timely decisions.

<u>_Individual learning (IL)_</u>

> For the past few years,

> IL1\. our company has provided sufficient training while implementing new information technology systems and applications.  
> IL2\. our employees have been able to learn new information technology applications quickly.  
> IL3\. our employees have been able to adopt new information technology applications for their work.  
> IL4\. our employees have been able to innovate new ideas and approaches to work effectively by adopting new information technology applications.  
> IL5\. our employees have shown little resistance to adopting new information systems and applications.

**_Service innovation practices questionnaires_**

_Process innovation (PRI)_

> For the past few years, our company has often offered new practices in …

> PRI1\. customer service.  
> PRI2\. customer information inquiry and consultation.  
> PRI3\. selling products/services.  
> PRI4\. providing after-sales services.  
> PRI5\. developing new products/services.  
> PRI6\. promoting new products/services.  
> PRI7\. internal administration and operations.

_Product innovation (PDI)_

> For the past few years, our company has often …

> PDI1\. revised and improved existing products/services.  
> PDI2\. repackaged existing products/services.  
> PDI3\. extended products/services.  
> PDI4\. created and established new lines of products/services.

**_Competitive advantage questionnaires_**

_External advantage (EA)_

> For the past few years, our company has been successful in providing new services …

> EA1\. to enter new markets.  
> EA2\. to gain more competitive advantage.  
> EA3\. to offer higher quality than competitors.

_Internal advantage (IA)_

> For the past few years, our company has been able to provide new services …

> IA1\. and increase employee job satisfaction.  
> IA2\. and increase employee-related experience and domain knowledge.  
> IA3\. and enhance the innovative capabilities of employees.

* * *

## <a id="appb"></a>APPENDIX B

### Summary Analysis of the Measurement Model: Factor Structure

<table>

<tbody>

<tr>

<th>Measurement Items</th>

<th colspan="4">Factor structure & loadings</th>

</tr>

<tr>

<th>Information technology adoption</th>

<th>Information technology infrastructure (ITI)</th>

<th>Strategic alignment (SA)</th>

<th>Organizational structure (OS)</th>

<th>Individual learning (IL)</th>

</tr>

<tr>

<td>ITI1</td>

<td>0.87</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>ITI2</td>

<td>0.88</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>ITI3</td>

<td>0.82</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>ITI4</td>

<td>0.75</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>SA1</td>

<td> </td>

<td>0.81</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>SA2</td>

<td> </td>

<td>0.81</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>SA3</td>

<td> </td>

<td>0.87</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>SA4</td>

<td> </td>

<td>0.88</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>OS1</td>

<td> </td>

<td> </td>

<td>0.83</td>

<td> </td>

</tr>

<tr>

<td>OS2</td>

<td> </td>

<td> </td>

<td>0.84</td>

<td> </td>

</tr>

<tr>

<td>OS3</td>

<td> </td>

<td> </td>

<td>0.72</td>

<td> </td>

</tr>

<tr>

<td>OS4</td>

<td> </td>

<td> </td>

<td>0.81</td>

<td> </td>

</tr>

<tr>

<td>OS5</td>

<td> </td>

<td> </td>

<td>0.81</td>

<td> </td>

</tr>

<tr>

<td>IL1</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.73</td>

</tr>

<tr>

<td>IL2</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.90</td>

</tr>

<tr>

<td>IL3</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.92</td>

</tr>

<tr>

<td>IL4</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.81</td>

</tr>

<tr>

<td>IL5</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.72</td>

</tr>

<tr>

<th>Service innovation practices</th>

<th colspan="2">Service process innovation (PRI)</th>

<th colspan="2">Service product innovation (PDI)</th>

</tr>

<tr>

<td>PRI1</td>

<td colspan="2">0.86</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PRI2</td>

<td colspan="2">0.82</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PRI3</td>

<td colspan="2">0.83</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PRI4</td>

<td colspan="2">0.84</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PRI5</td>

<td colspan="2">0.83</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PRI6</td>

<td colspan="2">0.87</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PRI7</td>

<td colspan="2">0.73</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>PDI1</td>

<td colspan="2"> </td>

<td colspan="2">0.88</td>

</tr>

<tr>

<td>PDI2</td>

<td colspan="2"> </td>

<td colspan="2">0.94</td>

</tr>

<tr>

<td>PDI3</td>

<td colspan="2"> </td>

<td colspan="2">0.93</td>

</tr>

<tr>

<td>PDI4</td>

<td colspan="2"> </td>

<td colspan="2">0.87</td>

</tr>

<tr>

<th>Competitive advantage</th>

<th colspan="2">External advantage (EA)</th>

<th colspan="2">Internal advantage (IA)</th>

</tr>

<tr>

<td>EA1</td>

<td colspan="2">0.93</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>EA2</td>

<td colspan="2">0.91</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>EA3</td>

<td colspan="2">0.95</td>

<td colspan="2"> </td>

</tr>

<tr>

<td>IA1</td>

<td colspan="2"> </td>

<td colspan="2">0.93</td>

</tr>

<tr>

<td>IA2</td>

<td colspan="2"> </td>

<td colspan="2">0.85</td>

</tr>

<tr>

<td>IA3</td>

<td colspan="2"> </td>

<td colspan="2">0.89</td>

</tr>

</tbody>

</table>