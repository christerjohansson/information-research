#### vol. 20 no. 2, June, 2015

# Information practices and competences that evidence information capability in a company

#### 
[Marta Zárraga-Rodríguez](#author)  
University of Navarra-ISSA, Pamplona, Spain  
[Maria Jesús Álvarez](#author)  
University of Navarra-Tecnun, San Sebastian, Spain

#### Abstract

> **Introduction**. The aim of this study is to obtain deeper knowledge about the information practices and competences that evidence information capability in a company. The first objective of this study is to define a set of practices that can be implemented in order to develop information capability in a company and to explore the common underlying dimensions of these practices, that is, the information competences. The second is to explore which information competences can be expected to have a greater influence on the quality of the information managed.  
> **Method**. Our target population for the study was companies committed to total quality management models because those models are information-intensive management systems. Data was collected with a questionnaire designed to elicit information regarding information practices. Forty-six companies that have applied the European framework for quality management excellence model were respondents to the survey.  
> **Analysis**. Quantitative analyses with the support of the Minitab software package were carried out on the data. More specifically we performed an exploratory factor analysis and a multiple regression analysis to achieve the objectives of the study.  
> **Results**. Main findings reveal eighteen information-related practices that can be pooled in 6 information competences that make up information capability. Key information competences which would most affect information quality are also found.  
> **Conclusion**. The competences identified by this study are presented in other literature as being equally important to business performance; however just three of these competences have a significant influence on the quality of information managed.

## Introduction

The efficient use and management of information, that is, information capability, is a source of competitive advantage for a company. A capability is defined and enacted through the application of a set of competences, and organizations only attain a sustainable competitive advantage if they can assemble a set of competences ([Ashurst, Doherthy and Peppard, 2008](#ash)). There is not a commonly adopted definition of competence ([Peppard and Ward, 2004](#pep)), but it is commonly accepted that a capability is underpinned by a set of competences. Furthermore, capability development is evidenced by a set of practices commonly adopted by the organization.

Marchand, Kettinger and Rollins ([2000](#mar)) proposed the information orientation model, in which they defined key information practices and found empirical evidence to support a causal relationship between information-related practices and improved business performance.

Later studies proposed more information practices to be taken into account and extended the original set of practices proposed by Marchand _et al._ ([2000](#mar)). The present study offers an in-depth exploration of the common underlying dimensions to these practices, and it uncovers and defines six information competences that support information capability.

Once a company has developed information capability, it seems reasonable to expect a higher quality of information is managed. It is generally accepted that having reliable and complete information has been always a key factor in making the right decision. Therefore, quality information leads to the achievement of higher levels of efficiency and better results. The need for quality information is additionally enhanced nowadays due to the changeable world in which companies compete.

In order to obtain more in-depth knowledge about information capability and its outcome in a company (i.e., quality information) we also explore which information competences can be expected to have a greater influence on the quality of the information managed.

The rest of this paper is organized as follows. The next section provides the theoretical framework. It is followed by a presentation of the research methodology. The final section presents and discusses the summary of empirical findings, and the paper ends by presenting the conclusions derived from the study.

## Theoretical framework

The resource based view in organizational theory states that the key to the strategic success of a company lies in the strategic use the company makes of its resources. This theory deals with organizational capabilities and considers a capability as a source of competitive advantage for the company, which allows the generation of value and differentiation through the combined use of a series of resources ([Peppard and Ward, 2004](#pep); [Ashurst _et al._, 2008](#ash)). We consider that a company has information capability when the use and management of information is so efficient that it is a source of competitive advantage for the company.

It is difficult to observe capabilities, but we can detect practices that evidence their presence. According to Ashurst _et al._ ([2008](#ash)), practices, which are described as a set of socially defined ways of doing things in order to achieve an outcome, are more concrete and observable than capabilities.

Marchand _et al._ ([2000](#mar)) proposed the information orientation model to explain how different information-related practices are needed to improve business performance. They found empirical evidence to support the model, which was used in later studies ([Choo _et al._, 2006](#cho); [Hwang, 2011](#hwa); [Mithas, Ramasubbu and Sambamurthy, 2011](#mit); [Kettinger and Marchand, 2011](#ket)). Using a powerful set of statistical techniques Marchand _et al._ ([2000](#mar)) defined key information practices and pooled them into three major groups. They proposed a set of fifteen practices that can be observed and measured in a company and a set of three information constructs: 1) information management, where practices related to the management of the information life cycle are considered, 2) information technologies, where practices related to the integration of information technology into day-to-day business are considered, and 3) information culture, which considers practices where the behaviour and values assumed by employees in relation to the use and management of information are shown.

There exist several studies dealing with some of the practices defined as evidences of information capability development under the information orientation model ([Sabherwal and Chan, 2001](#sab); [Chou, Chan, Cheng and Tsai, 2007](#chu); [Anantatmula, 2008](#ana); [Lai, Li, Wang and Zhao, 2008](#lai); [Sharma, Bhagwat and Dangayach, 2008](#sha); [Song, Nason and Di Benedetto, 2008](#son); [Coltman, Devinney and Midgley, 2011](#col); [Carmichael, Palacios-Marques and Gil-Pechuan, 2011](#car); among others, cited in Zárraga-Rodríguez and Alvarez, 2013). However, these studies do not consider the set of practices defined in the information orientation model as a whole. Taking into account those studies, Zárraga-Rodríguez and Alvarez ([2013](#zar)) completed the original set of practices pointed out by Marchand _et al._ ([2000](#mar)) and addressed three more practices to be considered when assessing the use and management of information within a company.

One of the new practices suggested by Zárraga-Rodríguez and Alvarez ([2013](#zar)) deals with the dissemination of information (IM_D), which would be part of the information management construct. The other two new practices suggested deal with the use of information technologies as support for information sharing (IT_ISS) and as strategy support (IT_SS). These practices are pooled into the information technologies construct. In this study we use the set of eighteen practices when assessing information capability.

A capability implies the combined use of a series of competences, and a competence is defined as the ability of the organization to deploy a combination of resources and achieve a given target using the processes of the organization ([Amit and Shoemaker, 1993](#ami) cited by [Peppard and Ward, 2004](#pep); [Ashurst _et al._, 2008](#ash); [Bharadwaj, 2000](#bha)). In this study we consider the common underlying dimensions to the information practices to be the information competences.

Although the terms data and information are often used synonymously, in practice managers differentiate information from data intuitively and describe information as data that has been processed ([Lin, Gao, Koronios and Chanana, 2007](#lin); [Pipino, Lee and Wang, 2002](#pip)). Therefore, it could be reasonable to suppose that practices associated with the information management construct can be pooled into two competences: the data management competence, which would comprise practices related to the management of data, and the information management competence, which would comprise practices related to the management of information. We present the first hypothesis of this work as follows:

_H1: information practices related to the management of the information life cycle can be pooled in two main competences._

The information technologies construct refers to practices related to the integration of information technology into day-to-day business. The concept of day-to-day business comprises both the operational and strategic levels. It could therefore be reasonable to suppose that practices associated with the information technologies construct can be pooled into two competences: the operational information technology use competence, which would comprise practices related to the use of information technologies as support at the operational level, and the strategic information technology use competence, which would comprise practices related to the use of information technologies as support at the strategic level. The second hypothesis of this work is:

_H2: information practices related to the integration of information technology into day-to-day business can be pooled in two main competences._

The information culture construct considers practices where behaviour and values assumed by employees in relation to the use and management of information are shown. Some behaviour and values can be naturally shared, assumed and internalized by the people in the organization, but other behaviour and values may to a certain extent have to be forced from the outside. Hence, it could be reasonable to suppose that practices associated with the information culture construct can be pooled into two competences: the internally assumed culture competence, which would comprise behaviour and values that are voluntary shared, assumed and internalized by the people in the organization and involve a higher-level culture in relation to the use and management of information, and the instilled culture competence, which would comprise values and behaviour shared as a result of the efforts made by managers to ensure effective communication in the organization. These are behaviour or values that are conditioned by and to a certain extent forced from the outside. Our third hypothesis is:

_H3: information practices where behaviour and values assumed by employees in relation to the use and management of information are shown can be pooled in two main competences._

A capability is embedded within the organization and the presence and effectiveness of the capability is reflected in business performance ([Peppard and Ward, 2004](#pep)). The information orientation model proposed by Marchand _et al._ ([2000](#mar)) provides the theoretical foundation for linking information capability and job performance, and is supported by other studies ([Owens, Wilson and Abell, 1995](#owe); [Sila and Ebrahimpour, 2005](#sil); [Ooi, Lin, Tan and Chong, 2011](#ooi); [Laosirihongthong, The and Adebanjo, 2013](#lao)). There also exist many studies that provide evidences for linking information quality and job performance (see [DeLone and McLean, 1992](#del92); [DeLone and McLean, 2003](#del03); [Petter, DeLone and McLean, 2008](#pet); [Madnick, Wang, Lee and Zhu, 2009](#mad); [Gorla, Somers and Wong, 2010](#gor); among others).

In this study we are focused on the link between information capability and information quality. It is not unreasonable to suppose that quality information could be considered an expected output when information is used and managed in an efficient way, that is, when a company has information capability. In particular we explore which information competence can be expected to have influence on the quality of the information managed. The fourth hypothesis of this work is:

_H4: information competences affect information quality_.

<table class="center" style="width:95%;"><caption>  
Table 1: Information capability associated practices (Adapted from: [Zárraga-Rodríguez and Alvarez, 2013](#zar))</caption>

<tbody>

<tr>

<th style="width:33%;">Information management construct</th>

<th style="width:33%;">Information technologies construct</th>

<th>Information culture construct</th>

</tr>

<tr>

<td>IM_S: Sense the information*</td>

<td>IT_OS: Employ information technology to support daily operations*</td>

<td>IC_I: Prevent the manipulation or hiding of information*</td>

</tr>

<tr>

<td>IM_C: Collect the information*</td>

<td>IT_BPS: Employ information technology for business process support*</td>

<td>IC_F: Establish formal and reliable sources of information*</td>

</tr>

<tr>

<td>IM_O: Organize the information*</td>

<td>IT_IS Employ information technology for innovation support*</td>

<td>IC_C: Transmit information about the performance of the company to all employees*</td>

</tr>

<tr>

<td>IM_P: Process the information*</td>

<td>IT_MS: Employ information technology for management support*</td>

<td>IC_S: Exchange sensitive and non-sensitive information collaboratively*</td>

</tr>

<tr>

<td>IM_M: Maintain the information*</td>

<td>IT_SS: Employ information technology for strategy support</td>

<td>IC_T: Trust each other*</td>

</tr>

<tr>

<td>IM_D: Disseminate the information</td>

<td>IT_ISS: Employ information technology for information sharing support</td>

<td>IC_P: Be concerned about obtaining and applying new information*</td>

</tr>

<tr>

<td colspan="3">* Practices proposed by Marchand _et al_. (2000)</td>

</tr>

</tbody>

</table>

Table 1 shows the meaning of the information practice codes in Figure 1\. Observe that the set of practices considered in the information capability model in Figure 1 reflects not only those proposed by Marchand _et al._ (2000) but also the new ones proposed by Zárraga-Rodríguez and Alvarez ([2013](#zar))

<figure class="centre">![Figure1: Information capability model](p664fig1.png)

<figcaption>Figure 1: Information capability model</figcaption>

</figure>

Figure 1 summarises the model we want to confirm, which is based in the original information orientation model proposed by Marchand _et al._ (2000); our model provides more in-depth knowledge about the information practices and the information competences that evidence information capability in a company.

In order to measure the quality of the information managed, the fact that information quality is a multidimensional construct should be taken into account. Therefore, we focused on establishing a set of attributes that information had to fulfil in order to be considered quality information, and we used them when we defined the statements in the questionnaire survey. We selected the information quality attributes that have been the most extensively studied; they are shown in Table 2.

<table class="center" style="width:50%;"><caption>  
Table 2: Information quality dimensions</caption>

<tbody>

<tr>

<th>Information attributes</th>

<th>References</th>

</tr>

<tr>

<td style="vertical-align:top;">Accuracy  
Consistency  
Believability  
Relevance  
Completeness  
Accessibility  
Timely for use</td>

<td style="vertical-align:top;">Huh, Keller, Redman and Watkins ([1990](#huh))  
Wand and Wang ([1996](#wan))  
Gable, Sadera and Chan ([2008](#gab))  
Sedera, Gable and Chan ([2004](#sed))  
Wang and Strong ([1996](#wang))  
Doll, Xia and Torkzadeh ([1994](#dol)  
DeLone and McLean ([1992](#del92))  
Nelson, Todd and Wixom ([2005](#nel))  
Gorla _et al._ ([2010](#gor))</td>

</tr>

</tbody>

</table>

## Research method

This section presents the research method applied in the exploratory study. The data collection method used was a questionnaire survey. The final instrument contained four main sections: practices related to the management of the information life cycle (that is, the information management section), practices related to the integration of information technology in day-to-day business (the information technology section), practices related to behaviour and values assumed by employees and displayed when using information (the information culture section), and information quality dimensions (the information quality section).

The instrument was tested by faculty members, senior managers and Euskalit members in order to make sure the items’ meanings were clear and that the questionnaire was easy to answer (Euskalit is the Basque Foundation for Excellence, a private non-profit organization founded in 1992 by the Department of Industry and Energy of the Basque Government, which supports the Basque Government’s policy of promoting quality in the Basque Country).

A total of 27 statements that had been developed from a literature review were presented in the questionnaire. Respondents had to indicate their agreement to statements on scale of 1 (strongly disagree) to 10 (strongly agree). In the information management section, there were 7 items, in the information technology section there were 6 items, in the information culture section there were 7 items, and in the information quality section there were 7 items.

The questionnaire was administered via a web page, which participants accessed via a link included in an e-mail they received. Therefore we used a self-administered survey technique which avoids the interviewer error or bias and reduces the cost of reaching the sample. However, one of the most important disadvantages of self-administered surveys is their low response rate. In order to improve the response rate a covering letter of appeal was written. The covering letter explained the objective of the study, stressed why the study was important and advanced the time needed to fulfil it. It also assured confidentiality and provided the name and the contact e-mail of the lead researcher to allow participants to verify the legitimacy of the questionnaire. After one week a second reminder e-mail was sent.

Our target population for the study was companies committed to total quality management. We focused on these companies because total quality management is considered to be an information intensive management system (Matta, Chen and Tama, 1998). Thus, it seems reasonable to expect that companies that have obtained quality awards have also promoted information capability development and that information practices are actually common practices in these companies.

Our empirical study was carried out in the Basque Country in northern Spain. The Basque region is the European region with the highest density of quality awards (Heras-Saizarbitoria, Miramon and Casadesús, 2012). The Basque government provides quality awards according to points obtained by the companies during external evaluation, which employs the scoring system used by the European model of excellence. The participants were selected according to their commitment to quality management and were contacted by Euskalit, who sent an e-mail with the covering letter and the link to the questionnaire to 292 companies that had received a quality award; the respondents were managers, who reported their agreement with each statement. 46 of them responded, yielding a response rate of 15 per cent.

A low response rate can lead to a bias in the sample and therefore can undermine the generalizability or the external validity of the results. It is therefore important to assess the non-response bias. The most commonly used methods to assess non-response bias are the comparison of respondents and non-respondents and the comparison of early to late respondents using statistical tests (Miller and Smith, 1983; Forza, 2000; Lindner, Murphy and Briers, 2001; King and He, 2005; Clottey and Grawe, 2014; among others).

In this study it was difficult to assess the non-response bias by comparing respondents and non-respondents because relevant data necessary to sample non respondents were not available. Following the protocol proposed by Lindner _et al._ (2001), we defined late respondents as the later 50% of the respondents and we assessed non-response bias by comparing responses of each group on multiple survey items using a t-test comparing group means. We found no statistically significant differences (p-values = >0.05) between the early responders and late responders. However, the small size of the groups of early and late respondents threatens the statistical power of the test to detect differences between them.

Our data analysis had two main objectives. First, we wanted to explore the common underlying dimensions (factors) of the data obtained through the questionnaire. We performed a factor analysis in order to summarize the information obtained in a smaller set of new composite dimensions with a minimum of information loss. Second, we want to explore the relationship between information quality and practices related to the management of the information life cycle, practices related to the integration of information technology in day-to-day business, and practices related to behaviour and values assumed by employees and displayed when using information. Using the new variables obtained in the first step, we used multiple regression analysis to examine these relationships.

## Results and discussion

This section presents the key findings of the study. We report our findings in terms of the two main objectives of the study. First we present a factor analysis of the questionnaire items pooled into four domains: information management, information technologies, information culture and information quality. Then we analyse the relationship between the factors identified by responses in the four domains using multiple regression analysis.

### Factor analysis

The variables used in the factor analysis are on the metric scale and correspond to the items from the questionnaire pooled into four domains: information management, information technologies, information culture and information quality. The correlation matrix between each set of variables justifies the application of factor analysis since there are a substantial number of correlations greater than 0.3.

The statistical procedure used to obtain the factor solution was the analysis of main components. To determine the number of factors to be extracted we used a combination of two criteria: the latent root criterion (considering the factors that have latent roots or eigenvalues greater than 1) and the criterion of percentage of variance (setting the percentage of the total variance that factors should represent).

Although among the social sciences a solution that represents 60% of the total variance is considered satisfactory, in this paper we have established an upper threshold. Therefore, when choosing the number of factors to consider, if the eigenvalue of the factor is greater than 1.0, the factor is taken directly into account, and if the eigenvalue of the factor is smaller than 1.0 but the cumulative variance improvement is significant when we include it, then the factor is considered. After selecting the number of factors to be considered, in order to correctly interpret the factors an orthogonal rotation with the varimax method was performed. To decide which factor for each variable should be included, we considered factor loadings above 0.5 as our cut-off point, and they are presented in bold.

To check the internal consistency of the scales, Cronbach's alpha was calculated and it was always within the acceptable range of 0.7 to 0.95.

<table class="center" style="width:95%;"><caption>  
Table 3: Information management - factor analysis</caption>

<tbody>

<tr>

<th>Factors and items</th>

<th>1</th>

<th>2</th>

</tr>

<tr>

<td colspan="3">_**Information management-after**_ (a=0.9)</td>

</tr>

<tr>

<td>My organization has defined processes to ensure that the information is available to stakeholders as needed (people can find it easily).</td>

<td style="text-align:center;">0.768</td>

<td style="text-align:center;">0.474</td>

</tr>

<tr>

<td>My organization has defined processes to have updated databases so as to ensure that people are using the best information available.</td>

<td style="text-align:center;">0.842</td>

<td style="text-align:center;">0.392</td>

</tr>

<tr>

<td>My organization has defined processes that ensure the distribution and exchange of information and the scope to be covered.</td>

<td style="text-align:center;">0.923</td>

<td style="text-align:center;">0.130</td>

</tr>

<tr>

<td colspan="3">_**Information management-before**_ (a=0.85)</td>

</tr>

<tr>

<td>My organization cares about knowing the information needed by employees, customers, suppliers and other stakeholders and systematically collects it.</td>

<td style="text-align:center;">0.435</td>

<td style="text-align:center;">0.752</td>

</tr>

<tr>

<td>My organization systematically gathers information from the environment (competitors, legislation, markets) and the inner workings, filtering it to prevent overload.</td>

<td style="text-align:center;">0.483</td>

<td style="text-align:center;">0.625</td>

</tr>

<tr>

<td>My organization has defined processes for competitive and technology surveillance and it has defined processes that facilitate the detection and identification of information to anticipate problems with suppliers and partners.</td>

<td style="text-align:center;">0.082</td>

<td style="text-align:center;">0.916</td>

</tr>

<tr>

<td>My organization has defined processes to transform data into useful information that can be used for decision-making.</td>

<td style="text-align:center;">0.426</td>

<td style="text-align:center;">0.749</td>

</tr>

<tr>

<td>Eigenvalues</td>

<td style="text-align:center;">4.593</td>

<td style="text-align:center;">0.919</td>

</tr>

<tr>

<td>Cumulative percentage of variance</td>

<td style="text-align:center;">65.6</td>

<td style="text-align:center;">78.7</td>

</tr>

</tbody>

</table>

Table 3 shows the results of the exploratory factor analysis for the information management domain. The results show the two components that accounted for 78.7% of the common variance: information management-before and information management-after. As mentioned previously, although the terms data and information are often used synonymously, in practice managers differentiate information from data intuitively and describe information as data that has been processed (Lin, 2007; Pipino _et al._, 2000). Therefore, information management-before refers to the perception that the organization has processes for data management, and information management-after refers to the perception that the organization has processes for information management. Therefore, H1 (_information practices related to the management of the information life cycle can be pooled in two main competences_) is confirmed.

<table class="center"><caption>  
Table 4: Information technology use - factor analysis</caption>

<tbody>

<tr>

<th>Factors and items</th>

<th>1</th>

<th>2</th>

</tr>

<tr>

<td colspan="3">_**_information technology use - strategic_**_ (a=0.89)</td>

</tr>

<tr>

<td>My organization uses information technology to automate and integrate the management of business processes, to facilitate people management and to interact and strengthen relationships with stakeholders.</td>

<td style="text-align:center;">0.759</td>

<td style="text-align:center;">0.473</td>

</tr>

<tr>

<td>My organization uses information technology to facilitate the development and exchange of new ideas. This also includes new products and services.</td>

<td style="text-align:center;">0.876</td>

<td style="text-align:center;">0.238</td>

</tr>

<tr>

<td>My organization uses information technology in competitive and technology surveillance, to anticipate possible outcomes of decisions before they are made, to predict values of indicators, etc.</td>

<td style="text-align:center;">0.785</td>

<td style="text-align:center;">0.333</td>

</tr>

<tr>

<td>My organization uses information technology to facilitate the sharing and exchange of information and to automate document location.</td>

<td style="text-align:center;">0.785</td>

<td style="text-align:center;">0.363</td>

</tr>

<tr>

<td colspan="3">_**_information technology use - tactical_**_ (a=0.73)</td>

</tr>

<tr>

<td>My organization uses information technology to support daily operations in order to improve individual productivity.</td>

<td style="text-align:center;">0.283</td>

<td style="text-align:center;">0.864</td>

</tr>

<tr>

<td>My organization uses information technology to facilitate the monitoring and analysis of internal and external business aspects (indicators) to assist decision-making.</td>

<td style="text-align:center;">0.372</td>

<td style="text-align:center;">0.780</td>

</tr>

<tr>

<td>Eigenvalues</td>

<td style="text-align:center;">4.047</td>

<td style="text-align:center;">0.625</td>

</tr>

<tr>

<td>Cumulative percentage of variance</td>

<td style="text-align:center;">67.5</td>

<td style="text-align:center;">77.9</td>

</tr>

</tbody>

</table>

Table 4 shows the results of the exploratory factor analysis for the information technology domain. The results show the two components that accounted for 77.9% of the common variance: __information technology use - strategic__ and __information technology use - tactical__. _information technology use - tactical_ refers to the perception that the organization uses information technology as support at the operational level, and _information technology use - strategic_ refers to the perception that the organization uses information technologies as support at the strategic level. Therefore, H2 (_information practices related to the integration of information technology into day-to-day business can be pooled in two main competences_) is confirmed.

<table class="center"><caption>  
Table 5: Information culture - factor analysis</caption>

<tbody>

<tr>

<th>Factors and items</th>

<th>1</th>

<th>2</th>

</tr>

<tr>

<td colspan="3">_**_information culture - instilled_**_ (a=0.88)</td>

</tr>

<tr>

<td>My organization values people sharing sensitive information rather than manipulating or hiding it for their own benefit.</td>

<td style="text-align:center;">0.753</td>

<td style="text-align:center;">-0.332</td>

</tr>

<tr>

<td>In my organization there are formal and reliable sources of information and the organization members use them.</td>

<td style="text-align:center;">0.877</td>

<td style="text-align:center;">-0.208</td>

</tr>

<tr>

<td>My organization reveals information about the performance of the company to all employees in order to influence and direct individual performance and consequently the company's performance.</td>

<td style="text-align:center;">0.910</td>

<td style="text-align:center;">0.036</td>

</tr>

<tr>

<td>In my organization the free exchange of sensitive and non-sensitive information in a collaborative way is common practice among team members and between areas.</td>

<td style="text-align:center;">0.836</td>

<td style="text-align:center;">-0.238</td>

</tr>

<tr>

<td>In my organization the free exchange of sensitive and non-sensitive information in a collaborative way with external stakeholders (customers, partners, suppliers, society, etc.) is common practice.</td>

<td style="text-align:center;">0.660</td>

<td style="text-align:center;">-0.285</td>

</tr>

<tr>

<td colspan="3">_**_information culture - internally assumed_**_ (a=0.79)</td>

</tr>

<tr>

<td>Members of my organization trust each other enough to talk about failures and mistakes in an open and constructive manner and without fear of unfair repercussions.</td>

<td style="text-align:center;">0.179</td>

<td style="text-align:center;">-0.874</td>

</tr>

<tr>

<td>Members of my organization show concern and preoccupation with obtaining and applying new information that enables them to respond quickly to changes and that enables them to promote innovation in products and services.</td>

<td style="text-align:center;">0.195</td>

<td style="text-align:center;">-0.901</td>

</tr>

<tr>

<td>Eigenvalues</td>

<td style="text-align:center;">3.965</td>

<td style="text-align:center;">1.271</td>

</tr>

<tr>

<td>Cumulative percentage of variance</td>

<td style="text-align:center;">56.7</td>

<td style="text-align:center;">74.8</td>

</tr>

</tbody>

</table>

Table 5 shows the results of the exploratory factor analysis for the information culture domain. The results show the two components that accounted for 74.8% of the common variance: information culture-instilled and information culture-internally assumed. Information culture-instilled refers to the perception that employees share a set of values and behaviour as a result of the efforts made by managers to ensure effective communication in the organization; these are behaviour or values that are conditioned by and to a certain extent forced from the outside. Information culture-internally assumed refers to the perception that there are also behaviour and values that are shared, assumed and internalized by the people in the organization and involve higher-level culture in relation to the use and management of information. Therefore, H3 (_information practices where behaviour and values assumed by employees in relation to the use and management of information are shown can be pooled in two main competences_) is confirmed.

<table class="center"><caption>  
Table 6: Information quality dimensions</caption>

<tbody>

<tr>

<th colspan="2">Items</th>

</tr>

<tr>

<td colspan="2">_**Information quality**_ (a=0.94)</td>

</tr>

<tr>

<td colspan="2">I can easily locate and access the information I need.</td>

</tr>

<tr>

<td colspan="2">The information is integrated and coordinated (although I make use of different sources the information does not change).</td>

</tr>

<tr>

<td colspan="2">Information is relevant, clear, and concise, and it is processed (not just data).</td>

</tr>

<tr>

<td colspan="2">I get the information I need to perform my job.</td>

</tr>

<tr>

<td colspan="2">The information I receive is accurate.</td>

</tr>

<tr>

<td colspan="2">I can trust the information I receive.</td>

</tr>

<tr>

<td colspan="2">I receive the information in a timely manner.</td>

</tr>

<tr>

<td>Eigenvalues</td>

<td style="text-align:center;">5.244</td>

</tr>

<tr>

<td>Cumulative percentage of variance</td>

<td style="text-align:center;">74.9</td>

</tr>

</tbody>

</table>

Table 6 shows the results of the exploratory factor analysis for the information quality domain. The results show that one component accounted for 74.9% of the common variance: information quality; it is also the only component with an eigenvalue >1.0\. Information quality refers to the perception that there are some quality dimensions that are usually perceived in the information used and managed.

## Multiple regression analysis

From the factor analysis, the original set of variables was replaced with new ones created from additive scales. All the variables that loaded high on one factor were combined, and the average score of the variables was used as a substitution variable. As a result we obtained a total of seven variables to consider: _information management - before_ (IMB) and information management -after (IMA), which are associated with the information management domain; _information technology use - strategic_ (ITS) and _information technology use - tactical_ (ITT), which are associated with the information technology domain; _information culture - instilled_ (ICI) and _information culture - internally assumed_ (ICIA), which are associated with the information culture domain; and information quality (IQ), which is associated with the information quality domain.

We wanted to explore the relationship between the set of the six predictor variables (information management-before, information management-after, _information technology use - strategic_, _information technology use - tactical_, _information culture - instilled_ and _information culture - internally assumed_) and the dependent variable, namely the quality of the information managed.

Using multiple regression analysis, we were able to get a model that allowed us to explain how the independent or predictor variables influenced the dependent variable. We first employed the best subsets regression method to determine which predictor (independent) variables should be included in the multiple regression model. This method allowed us to identify the best-fitting regression models that could be constructed with the specified predictor variables. We computed this method using the statistical software program Minitab.

<table class="center"><caption>  
Table 7: Best subsets analysis</caption>

<tbody>

<tr>

<th>Vars</th>

<th>R<sup>2</sup></th>

<th>Adj.  
R<sup>2</sup></th>

<th>Mallow's  
Cp</th>

<th>S</th>

<th>IMB</th>

<th>IMA</th>

<th>ITT</th>

<th>ITS</th>

<th>ICI</th>

<th>ICIA</th>

</tr>

<tr>

<td style="text-align:center;">1</td>

<td style="text-align:center;">60.4</td>

<td style="text-align:center;">59.5</td>

<td style="text-align:center;">20</td>

<td style="text-align:center;">0.73453</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td style="text-align:center;">1</td>

<td style="text-align:center;">47.5</td>

<td style="text-align:center;">46.3</td>

<td style="text-align:center;">40.3</td>

<td style="text-align:center;">0.84621</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td style="text-align:center;">2</td>

<td style="text-align:center;">69..3</td>

<td style="text-align:center;">67.9</td>

<td style="text-align:center;">8.1</td>

<td style="text-align:center;">0.65432</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td style="text-align:center;">2</td>

<td style="text-align:center;">64.9</td>

<td style="text-align:center;">63.2</td>

<td style="text-align:center;">15.0</td>

<td style="text-align:center;">0.70012</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td style="text-align:center;">**3**</td>

<td style="text-align:center;">**72.8**</td>

<td style="text-align:center;">**70.8**</td>

<td style="text-align:center;">**4.7**</td>

<td style="text-align:center;">**0.62364**</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">**X**</td>

<td style="text-align:center;">**X**</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">**X**</td>

</tr>

<tr>

<td style="text-align:center;">3</td>

<td style="text-align:center;">71.6</td>

<td style="text-align:center;">69.5</td>

<td style="text-align:center;">6.6</td>

<td style="text-align:center;">0.63742</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td style="text-align:center;">4</td>

<td style="text-align:center;">73.6</td>

<td style="text-align:center;">71.1</td>

<td style="text-align:center;">5.3</td>

<td style="text-align:center;">0.62100</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td style="text-align:center;">4</td>

<td style="text-align:center;">73.1</td>

<td style="text-align:center;">70.5</td>

<td style="text-align:center;">6.1</td>

<td style="text-align:center;">0.62693</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td style="text-align:center;">5</td>

<td style="text-align:center;">74.5</td>

<td style="text-align:center;">71.3</td>

<td style="text-align:center;">6.0</td>

<td style="text-align:center;">0.61850</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td style="text-align:center;">5</td>

<td style="text-align:center;">73.7</td>

<td style="text-align:center;">70.4</td>

<td style="text-align:center;">7.2</td>

<td style="text-align:center;">0.62786</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td style="text-align:center;">6</td>

<td style="text-align:center;">75.1</td>

<td style="text-align:center;">71.3</td>

<td style="text-align:center;">7.0</td>

<td style="text-align:center;">0.61885</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

</tr>

</tbody>

</table>

Table 7 shows the results. Each line of the output represents a different model, and the two best models for each number of predictors appear.

R2 shows the percentage of the dependent variable that can be explained by the predictor variables in the model. In general, the higher the R2, the better the model fits the data. Adjusted R2 shows the percentage of the dependent variable that can be explained by the predictor variables in the model, adjusted for the number of predictors in the model. This adjustment is important because the R2 for any model will always increase when a new term is added. A model with more terms may appear to have a better fit simply because it has more terms. The adjusted R2 is a useful tool for comparing the explanatory power of models with different numbers of predictors. The adjusted R2 will increase only if the new term improves the model more than would be expected by chance, and it will decrease when a predictor improves the model less than expected by chance. Mallows' Cp is a statistic that is commonly used as an aid in choosing between competing multiple regression models. A Mallows' Cp value that is close to the number of predictors plus the constant indicates that the model is relatively precise and unbiased in estimating the true regression coefficients and predicting future responses. However, the Mallows’ Cp should be evaluated in conjunction with other statistics included in the best subsets output, such as R2, Adjusted R2 and S (standard error of the regression), for more accurate decision-making.

Table 7 shows that moving from the best model with three variables to models with four or five variables slightly improves the model fit (adj. R2). It should also be noted that moving from the best model with three variables to models with four or five variables slightly improves the value of S, and the lower the value of S is the more accurate the predictions made with the regression line are. We started by considering that the best subset of variables would be given by these three variables: IMA, ITT and ICIA.

Once we selected the best subset, by using a multiple regression analysis we obtained a model that allowed us to explain how the independent or predictor variables influence the dependent variable. A three-predictor multiple linear regression model with three predictor variables was proposed: information management-after (IMA), _information technology use - tactical_ (ITT) and _information culture - internally assumed_ (ICIA).

<table class="center"><caption>  
Table 8: Multiple regression analysis of a three-variable model</caption>

<tbody>

<tr>

<th>Dependent variable</th>

<th>Independent variables</th>

<th>Coefficient</th>

<th>Significance</th>

<th>Model Adj. R<sup>2</sup></th>

<th>F</th>

<th>Significance</th>

</tr>

<tr>

<td>IQ (Information quality)</td>

<td style="text-align:center;">IMA  
ITT  
ICIA</td>

<td style="text-align:center;">0.382  
0.279  
0.268</td>

<td style="text-align:center;">0.000  
0.026  
0.001</td>

<td style="text-align:center;">0.708</td>

<td style="text-align:center;">37.414</td>

<td style="text-align:center;">0.000</td>

</tr>

</tbody>

</table>

We used Minitab statistical software for the regression analysis, and Table 8 shows the output provided by the tool. According to the value of the adjusted R2 (0.708), the model has a very reasonable fit. Moreover, the proposed model is adequate as the F-statistic = 37.414 is significant at the 1% level (p-value = < 0.001).

Three variables were found to have a positive and significant relationship with the information quality dimension: information management-after (IMA; b=0.382; p = < 0.01), _information technology use - tactical_ (ITT; b=0.279; p=0.026) and _information culture - internally assumed_ (ICIA; b=0.268; p=<0.01). Therefore, H4 ( _information competences affect information quality._) is not confirmed because only three information competences were found to have positive and significant relationship with the information quality.

We also considered two other models with four and five variables, but neither of the coefficients of these new variables was statistically significant.

## Conclusions

Companies that are aware of the importance of handling quality information when making the decisions will find in this study a set of practices that can implemented in order to develop their information capability. As the study reveals, to the extent that a company has information capability, the quality of the information it manages will be higher.

As evidenced by the exploratory factor analysis, practices related with the management of the information life cycle can be summarized in two dimensions: _information management - before_ and _information management - after_. In practice, managers intuitively differentiate information from data and describe information as data that has been processed. According to this idea _information management - before_ comprises practices associated with data management and _information management - after_ comprises practices associated information management.

Practices related to the integration of information technology into day-to-day business can also be summarised in two dimensions: _information technology use - strategic_ and _information technology use - tactical_. _information technology use - tactical_ comprises practices associated with the use of information technology as support at the operational level, and _information technology use - strategic_ comprises practices associated with the use of information technologies as support the strategic level.

Practices where the behaviour and values assumed by employees in relation to the use and management of information are shown can be summarized in two dimensions: _information culture - instilled_ and _information culture - internally assumed_. _Information culture - instilled_ comprises practices conditioned by and to a certain extent forced from the outside. _Information culture - internally assumed_ comprises practices that reveal a positive and proactive attitude towards the use and management of information shared by employees.

We have found a positive and significant relationship between some information practices and the quality of the information managed. The study has allowed areas of improvement to be identified by pointing out practices that, if they were commonly implemented, would improve the use and management of information and make it more efficient, which would be reflected in the quality of the information managed. Specifically, practices associated with information management, practices associated with the use of information technologies as support at the operational level and practices that reveal a positive and proactive attitude towards the use and management of information shared by employees are key and have a significant influence on the quality of managed information in companies that took part in this study.

The companies analysed were committed to total quality management and had won a quality award. Correctly applying a total quality management model involves establishing processes that must be under control by using the right indicators. The monitoring of the overall performance of the quality system would be enabled by using the appropriate indicators, which have to be produced and updated. An information system designed by taking information capability into account would be the most suitable because it would provide information with higher quality dimensions.

As for research limitations, the first one is that the questionnaires were answered by managers, which could have introduced a bias. Given that information flows throughout the company and involves everyone’s work, it would be better to know the perception of all the employees. The second limitation is the low response rate which can lead to a bias.

## Acknowledgements

The researchers would like to acknowledge the support of Euskalit and those companies which took part in the study, as well as the helpful comments of reviewers.

## About the author

**Marta Zárraga-Rodríguez** received her PhD in Industrial Engineering at the University of Navarra and lectures in Business Administration, Statistics and Economics at ISSA-School of Management Assistants of the University of Navarra. She has been conducting and publishing research on the analysis and development of companies’ capabilities. She can be contacted at [mzarraga@unav.es](mailto:mzarraga@unav.es).  
**Maria Jesús Alvarez** holds a Ph.D. degree in Agricultural Engineering from the UMP (Universidad Politécnica de Madrid). She is a professor of Operational Research at TECNUN (Universidad de Navarra) in the Management Industrial Engineering Department. Her research activities are in Operations Research applied to logistics and to the improvement of the productivity of the systems. She can be contacted at [mjalvarez@tecnun.es](mailto:mjalvarez@tecnun.es).

#### References

*   Ashurst, C., Doherty, N.F. & Peppard, J. (2008). Improving the impact of IT development projects: the benefits realization capability model. _European Journal of Information Systems, 17_(4), 352-370.
*   Amit, R. & Schoemaker, P.J.H. (1993). Strategic assets and organizational rent. _Strategic Management Journal, 14_(1), 33-46.
*   Anantatmula, V. (2008). The role of technology in the project manager performance model. _Project Management Journal, 39_(1), 34-48.
*   Bharadwaj, A.S. (2000). A resource-based perspective on information technology capability and firm performance: an empirical investigation. _MIS Quarterly, 24_(1), 169-196.
*   Carmichael, F., Palacios-Marques, D. & Gil-Pechuan, I. (2011). How to create information management capabilities through web 2.0\. _The Service Industries Journal, 31_(10), 1613-1625.
*   Choo, C.W., Furness, C., Paquette, S., van den Berg, H., Detlor, B., Bergeron, P. & Heaton, L. (2006). Working with information: information management and culture in a professional services organization. _Journal of Information Science, 32_(6), 491-510.
*   Chou, T., Chan, P., Cheng, Y. & Tsai, C. (2007). A path model linking organizational knowledge attributes, information processing capabilities, and perceived usability. _Information and Management, 44_(4), 408-417.
*   Clottey, T.A. & Grawe, S.J. (2014). Non-response bias assessment in logistics survey research: use fewer tests? _International Journal of Physical Distribution & Logistics Management, 44_(5), 412-426.
*   Coltman, T., Devinney, T.M. & Midgley, D.F. (2011). Customer relationship management and firm performance. _Journal of Information Technology, 26_(3), 205-219.
*   DeLone, W.H. & McLean, E.R. (1992). Information systems success: the quest for the dependent variable. _Information System Research, 3_(1), 60-95.
*   DeLone, W. & McLean, E.R. (2003).The DeLone and McLean model of information system success: a ten-year update. _Journal of Management Information Systems, 19_(4), 9-30.
*   Doll, W.J., Xia, W. & Torkzadeh, G. (1994). A confirmatory factor analysis of the end-user computing satisfaction instrument. _MIS Quarterly, 18_(4), 453-461.
*   Forza, C. (2002). Survey research in operations management: a process-based perspective. _International Journal of Operations & Production Management, 22_(2), 152-194.
*   Gable, G.G., Sedera, D. & Chan, T. (2008). Re-conceptualizing information system success: the IS-impact measurement model. _Journal of the Association for Information Systems, 9_(7), 377-408.
*   Gorla, N., Somers, T.M. & Wong, B. (2010). Organizational impact of system quality, information quality, and service quality. _Journal of Strategic Information Systems, 19_(3), 207-228.
*   Heras-Saizarbitoria, I., Marimon, F. & Casadesús, M. (2012). An empirical study of the relationships within the categories of the EFQM model. _Total Quality Management & Business Excellence, 23_(5-6), 523-540.
*   Huh, Y.U., Keller, F.R., Redman, T.C. & Watkins, A.R. (1990). Data quality. _Information and Software Technology, 32_(8), 559-565.
*   Hwang, Y. (2011). Measuring information behaviour performance inside a company: a case study. _Information Research, 16_(2), paper 480\. Retrieved from http://InformationR.net/ir/16-2/paper480.html. (Archived by WebCite® at http://www.webcitation.org/6WZwJOsVV).
*   Kettinger, W.J. & Marchand, D.A. (2011). Information management practices (IMP) from the senior manager’s perspective: an investigation of the IMP construct and its measurement. _Information System Journal, 21_(5), 385-406.
*   King, W.R. & He, J. (2005). External validity in IS survey research. _Communications of the Association for Information Systems, 16_, Article 45.
*   Lai, F., Li, D., Wang, Q. & Zhao, X. (2008). The information technology capability of third-party logistic providers: a resource-based view and empirical evidence from China. _Journal of Supply Chain Management, 44_(3), 22-38.
*   Laosirihongthong, T., Teh, P.L. & Adebanjo, D. (2013). Revising quality management and performance. _Industrial Management & Data Systems, 113_(7), 990-1006.
*   Lin, S., Gao, J., Koronios, A. & Chanana, V. (2007). Developing a data quality framework for asset management in engineering organizations. _International Journal of Information Quality, 1_(1), 100-126.
*   Lindner, J.R., Murphy, T.H. & Briers, G.E. (2001). Handling nonresponse in social science research. _Journal of Agricultural Education, 42_(4), 43-53.
*   Madnick, S.E., Wang, R. Y., Lee, Y. W. & Zhu, H. (2009). Overview and framework for data and information quality research. _ACM Journal of Data and Information Quality, 1_(1), Article 2.
*   Marchand, D.A., Kettinger, W.J. & Rollins, J.D. (2000). Information orientation: people, technology and the bottom line. _Sloan Management Review, 41_(4), 69-80.
*   Matta, K., Chen, H.G. & Tama, J. (1998). The information requirements of total quality management. _Total Quality Management and Business Excellence, 9_(6), 445-461.
*   Miller, L.E. & Smith, K.L. (1983). Handling nonresponse issues. _Journal of Extension, 21_(1), 45-50.
*   Mithas, S., Ramasubbu, N., & Sambamurthy, V. (2011). How information management capability influences firm performance. _MIS Quarterly, 35_(1), 237-256.
*   Nelson, R.R., Todd, P.A. & Wixom, B.H. (2005). Antecedents of information and system quality: an empirical examination within the context of data warehousing. _Journal of Management Information Systems, 21_(4), 199-235.
*   Ooi, K.B., Lin, B.S., Tan, B.I. & Chong, A.Y.L. (2011). Are TQM practices supporting customer satisfaction and service quality? _Journal of Services Marketing, 25_(6), 410-419.
*   Owens, I., Wilson, T.D. & Abell, A. (1995). Information and business performance: a study of information systems and services in high-performing companies. _Information Research, 1_(1). Retrieved from http://informationr.net/ir/1-2/paper5.html. (Archived by WebCite® at http://www.webcitation.org/6WZwPNTwb).
*   Peppard, J. & Ward, J. (2004). Beyond strategic information systems: towards an IS capability. _Journal of Strategic Information Systems, 13_(2), 167-194.
*   Petter, S., DeLone, W. & McLean, E.R. (2008). Measuring information system success: models, dimensions, measures, and interrelationships. _European Journal of Information Systems, 17_(3), 236-263.
*   Pipino, L.L., Lee, Y.W. & Wang, R.Y. (2002). Data quality assessment. _Communications of the ACM, 45_(4), 211-218.
*   Sabherwal, R. & Chan, Y.E. (2001). Alignment between business and IS strategies: a study of prospectors, analyzers, and defenders. _Information System Research, 12_(1), 11-33.
*   Sedera, D., Gable, G. & Chan, T. (2004). A factor and structural equation analysis of the enterprise systems success measurement model. In _Proceedings Twenty-Fifth International Conference on Information Systems_ (pp. 449-464). New York. Retrieved from http://eprints.qut.edu.au/4733/1/SederaGable2004ICIS.pdf. (Archived by WebCite® at http://www.webcitation.org/6WZxPwKRa).
*   Sharma, M.K., Bhagwat, R. & Dangayach, G.S. (2008).Performance measurement of information systems in small and medium sized enterprises: a strategic perspective. _Production Planning & Control, 19_(1), 12-24.
*   Sila, I. & Ebrahimpour, M. (2005). Critical linkages among TQM factors and business results. _International Journal of Operations & Production Management, 25_(11), 1123-1155.
*   Song, M., Nason, R.W. & Di Benedetto, C.A. (2008). Distinctive marketing and information technology capabilities and strategic types: a cross-national investigation. _Journal of International Marketing, 16_(1), 4-38.
*   Wand, Y. & Wang, R.Y. (1996). Anchoring data quality dimensions in ontological foundations. _Communications of the ACM, 39_(11), 86-95.
*   Wang, R.Y. & Strong, D.M. (1996). Beyond accuracy: what data quality means to data consumers. _Journal of Management Information Systems, 12_(4), 5-34.
*   Zárraga-Rodríguez, M. & Alvarez, M.J. (2013). Exploring the links between information capability and the EFQM business excellence model: the case of Basque Country quality award winners. _Total Quality Management & Business Excellence, 24_(1), 5-6.