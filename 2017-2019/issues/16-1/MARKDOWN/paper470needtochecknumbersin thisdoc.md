#### vol. 16 no. 1, March 2011

# Icelanders and trust in the Internet as a source of health and life-style information

#### [Ágústa Pálsdóttir](mailto:agustap@hi.is)  
Department of Library and Information Science, University of Iceland, Oddi v/Sturlugotu, 101 Reykjavik, Iceland

#### Abstract

> **Introduction.** The development in Icelander's evaluation of health and life-style information from the media, health specialists and the Internet, between 2002 and 2007, is examined.  
> **Method.** Data were gathered as postal surveys in 2002 and 2007\. The data were re-analysed to examine trends in the evaluation of the usefulness and reliability of the information.  
> **Analysis.** Cluster analysis was used to draw participants in four clusters: _passive_, _moderately passive_, _moderately active_ and _active_. Evaluation of information was examined by analysis of variance and post-hoc test (Tukey) conducted to examine significant differences across the clusters. For each cluster 95% confidence intervals were used to examine differences across the information channels.  
> **Results.** Health specialists were considered more useful and reliably than either the media or the Internet in 2002 and 2007\. In 2002, _passive_ and _moderately active_ clusters considered Internet as less useful and reliable than the _media_. For _active_ and _moderately passive_ clusters there was no significant difference across the channels. In 2007, no significant difference was found betwee the media and the Internet.  
> **Conclusions.** The results indicate that the Internet may be well on its way to establish itself as equivalent to the more traditional information channels in respect of information relating to health care and life-style.

## Introduction

The means of seeking health information has undergone huge changes in recent years, particularly the options provided by the Internet. As a result, people's assessment of information in various sources and channels may have altered. The paper examines how different groups of Icelander's evaluate the usefulness and the reliability of health and life-style information in different information channels. Furthermore, it explores how the evaluation has developed between 2002 and 2007\. The study uses the term health and life-style which refers to the personal life-style choices made by people who are normally thought of as healthy and the impact this can have on their health and wellbeing (see also Pálsdóttir, [2009](#p%EF%BF%BDl09)).

The concept of relevance has been identified as fundamental in relation to information behaviour. Reviews of the literature can for example be found by Borlund ([2003](#bor03)), Saracevic ([1975](#sar75)), Schamber ([1994](#sch94)) and Mizzaro ([1997](#miz97)). In spite of the significance of the concept it has proved difficult to define. The discussion has, to some extent, circled around the difference between ideas about two main classes of relevance, topicality or "system view", that is the match between the document and the query terms, and relevance as judged by the user, or how well the user feels that the information need is being met (see, for example, [Harter 1992](#har92); [Schamber 1994](#sch94)). Relevance judgements are described as being dynamic by their nature and change as people's knowledge structures change, as well as being multi-dimensional, that is different persons may judge the relevance of the same information differently ([Schamber _et al._ 1990](#sch90)).

It has been suggested that the importance of relevance criteria, or the values that people use when they decide whether or not information is relevant to their need, is being assessed differently according to the topic and tasks which are being dealt with ([Tang and Solomon 2001](#tan01)). Relevance criteria has been a subject of a number of studies and a range of criteria has been suggested (e.g., [Barry 1994](#bar94); [Schamber 1994](#sch94)). Barry and Schamber ([1998](#bar98)) have identified ten categories of relevance. Among them is depth, scope or specificity which refers how useful the information is, or how well it relates to the information need. Others have also identified usefulness of information as important criteria. Greisdorf ([2003](#gre03)) argues that there exists a continuum, with people being most likely to evaluate topicality of the information, then pertinence and after that, the usefulness of the information.

According to Tang and Solomon ([1998](#tan98)) the criteria used to judge relevance changes during the relevance process, leading to a modification of what information is considered to be needed, with usefulness being a criteria which people use in the later stages of the process. Another criteria of relevance which has been identified is reliability ([Barry and Schamber 1998](#bar98); [Xu and Chen 2006](#xuc06)). Reliability is related to and overlaps with several concepts such as trustworthiness, believability, credibility and quality of information ([Savolainen 2007](#sav07)). In particular, the reliability of health information on the Internet has been discussed, with health specialists raising concern about the quality of the information ([Bedell _et al._ 2004](#bed04); [Cline and Haynes 2001](#cli01); [Eysenbech _et al._ 2002](#eys02)). Previous studies have reported that people seek information on the Internet mainly because it is convenient, it offers a broad range of information and is up-to-date ([Fox _et al._ 2000](#fox00); [Savolainen and Kari 2004](#sav04)) while less attention is being paid to the quality of the sources, such as accuracy, reliability and usefulness ([Savolainen and Kari 2004](#sav04)).

Little is known about how the general public evaluates and compares health information in different information sources or channels but Fox and Jones ([2009](#fox09)) have reported, that of those who seek health information online, 13% claim it to have had a major impact and 44% to have a minor impact on their health care. It has also been indicated that information on the Internet may be thought of as less reliable than information provided by health centres ( [Marshall and Williams 2006](#mar06)). Furthermore, there has been a lack of studies who examine whether or not, or how, people's assessment of information has changed in line with the changes that have occurred within the information environment in the past years. Much of the research work in recent years has concentrated on how people seek and evaluate information on the Internet. In this study, however, the aim is to gain a more holistic picture by investigating how the general public's evaluation of information within a broad network of sources and channels has developed over a period of five years, between 2002 and 2007\. The study concentrates on two criteria of evaluation, the usefulness and the reliability of information. It, furthermore, differs from traditional relevance research as it examines the participant's evaluations through a survey.

## Method

### Data collection

The data were gathered as postal surveys during the autumn of 2002 and spring 2007\. For both data sets the sample consisted of 1,000 people, aged 18 to 80, randomly selected from the National Register of Persons in Iceland. The response rate was 51% in 2002 and 47% in 2007\. Tables 1 and 2 present a comparison of the characteristics of the respondents with population parameters derived from Statistics Iceland.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Characteristics of the participants compared with the population in 2002**</caption>

<tbody>

<tr>

<th>Demographic characteristics</th>

<th>Sample % (raw number)</th>

<th>Variation %</th>

<th>Confidence interval</th>

<th>Population % (raw number)</th>

</tr>

<tr>

<td colspan="5">_**Sex**_</td>

</tr>

<tr>

<td>Men</td>

<td align="center">45.4 (231)</td>

<td align="center">+/- 4.3</td>

<td align="center">41.0-49.7</td>

<td align="center">50.01 (144,287)</td>

</tr>

<tr>

<td>Women</td>

<td align="center">54.6 (277)</td>

<td align="center">+/- 4.3</td>

<td align="center">50.3-59.0</td>

<td align="center">49.99 (144,184)</td>

</tr>

<tr>

<td>_Total_</td>

<td align="center">100% (508)</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">100% (203,046)</td>

</tr>

<tr>

<td colspan="5">Age</td>

</tr>

<tr>

<td>18-29</td>

<td align="center">22.6 (115)</td>

<td align="center">+/- 3.7</td>

<td align="center">18.9-26.3</td>

<td align="center">15.1 (51,736)</td>

</tr>

<tr>

<td>30-39</td>

<td align="center">20.8 (106)</td>

<td align="center">+/- 3.6</td>

<td align="center">17.2-24.4</td>

<td align="center">20.3 (41,714)</td>

</tr>

<tr>

<td>40-49</td>

<td align="center">22.0 (112)</td>

<td align="center">+/- 3.7</td>

<td align="center">18.4-25.7</td>

<td align="center">21.1 (41,258)</td>

</tr>

<tr>

<td>50-59</td>

<td align="center">16.0 (81)</td>

<td align="center">+/- 3.2</td>

<td align="center">12.7-19.2</td>

<td align="center">25.4 (31,482)</td>

</tr>

<tr>

<td>60-80</td>

<td align="center">18.6 (94)</td>

<td align="center">+/- 3.4</td>

<td align="center">15.2-22.0</td>

<td align="center">18.1 (36,856)</td>

</tr>

<tr>

<td>_Total_</td>

<td align="center">100% (508)</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">100% (203,046)</td>

</tr>

<tr>

<td colspan="5">_**Education**_</td>

</tr>

<tr>

<td>Primary</td>

<td align="center">31.6 (161)</td>

<td align="center">+/- 4.1</td>

<td align="center">27.5-35.7</td>

<td align="center">43.9 (89,137)</td>

</tr>

<tr>

<td>Secundary</td>

<td align="center">44.2 (224)</td>

<td align="center">+/- 4.3</td>

<td align="center">39.9-48.5</td>

<td align="center">43.9 (89,137)</td>

</tr>

<tr>

<td>University</td>

<td align="center">24.2 (123)</td>

<td align="center">+/- 3.7</td>

<td align="center">20.5-27.9</td>

<td align="center">12.2 (24,772)</td>

</tr>

<tr>

<td>_Total_</td>

<td align="center">100 (508)</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">100% (203,046)</td>

</tr>

</tbody>

</table>

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Characteristics of the participants compared with the population in 2007**  
</caption>

<tbody>

<tr>

<th>Demographic characteristics</th>

<th>Sample % (raw number)</th>

<th>Variation %</th>

<th>Confidence interval</th>

<th>Population % (raw number)</th>

</tr>

<tr>

<td colspan="5">_**Sex**_</td>

</tr>

<tr>

<td>Men</td>

<td align="center">45.9 (215)</td>

<td align="center">+/- 4.5</td>

<td align="center">41.4-50.4</td>

<td align="center">51.3 (114.424)</td>

</tr>

<tr>

<td>Women</td>

<td align="center">54.1 (253)</td>

<td align="center">+/- 4.5</td>

<td align="center">49.6-58.6</td>

<td align="center">48.7 (108.437)</td>

</tr>

<tr>

<td>_Total_</td>

<td align="center">100% (468)</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">100% (222.861)</td>

</tr>

<tr>

<td colspan="5">_**Age**_</td>

</tr>

<tr>

<td>18-29</td>

<td align="center">16.9 (79)</td>

<td align="center">+/- 3.4</td>

<td align="center">13.5-20.3</td>

<td align="center">24.2 (53.970)</td>

</tr>

<tr>

<td>30-39</td>

<td align="center">18.9 (88)</td>

<td align="center">+/- 3.6</td>

<td align="center">15.3-22.5</td>

<td align="center">19.8 (44.148)</td>

</tr>

<tr>

<td>40-49</td>

<td align="center">20.4 (96)</td>

<td align="center">+/- 3.7</td>

<td align="center">16.7-24.1</td>

<td align="center">20.3 (45.174)</td>

</tr>

<tr>

<td>50-59</td>

<td align="center">17.6 (82)</td>

<td align="center">+/- 3.5</td>

<td align="center">14.1-21.1</td>

<td align="center">17.0 (37.914)</td>

</tr>

<tr>

<td>60-80</td>

<td align="center">26.2 (123)</td>

<td align="center">+/- 4.0</td>

<td align="center">22.2-30.2</td>

<td align="center">18.7 (41.655)</td>

</tr>

<tr>

<td>_Total_</td>

<td align="center">100 (468)</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">100% (222.861)</td>

</tr>

<tr>

<td colspan="5">_**Education**_</td>

</tr>

<tr>

<td>Primary</td>

<td align="center">27.0 (127)</td>

<td align="center">+/- 4.0</td>

<td align="center">23.0-31.0</td>

<td align="center">33.4 (74.435)</td>

</tr>

<tr>

<td>Secundary</td>

<td align="center">42.2 (197)</td>

<td align="center">+/- 4.5</td>

<td align="center">37.7-46.7</td>

<td align="center">40.3 (89.813)</td>

</tr>

<tr>

<td>University</td>

<td align="center">30.8 (144)</td>

<td align="center">+/- 4.2</td>

<td align="center">26.6-35.0</td>

<td align="center">26.3 (58.613)</td>

</tr>

<tr>

<td>_Total_</td>

<td align="center">100% (468)</td>

<td align="center"></td>

<td align="center"></td>

<td align="center">100% (222.861)</td>

</tr>

</tbody>

</table>

Inspection of the confidence intervals in Table 1 and Table 2 shows that the distribution of age falls within the 95% confidence limits for three out of five age groups. Examination of the confidence intervals for sex in Table 1 and Table 2 shows that the distribution of men and women does not quite reflect the population. Educational level was measured as the highest level of education completed. Three levels were distinguished: primary school includes those who have finished compulsory education (ten years of education); secondary education (fourteen years) includes those who have completed vocational training, or secondary school; and university education. Inspection of confidence intervals in Table 1 and Table 2 shows that the ratio of people with secondary education is representative of the population. However, more people with university education and fewer with primary school education responded to the questionnaire, compared with the population.

### Measurements

The same sets of measurements were used for the data from 2002 and 2007.

(1) Socio-demographic information included the variables sex, age, marital status, geographical residence, education and income. Based on results of previous analysis of the data the variables sex, age and education were used in the analysis.

(2) Information-seeking clusters. Respondents purposive information seeking was examined by asking: '_Have you sought information about health and life-style in any of the following sources_'? A list of over twenty information sources was presented and people asked to provide answers about every source. A five-point response scale was used (Very often - Never), 1 is the lowest activity level and 5 is the highest.

(3) Evaluation of information was examined by asking two questions: '_How useful do you find information about health and life-style in the following sources_'?, and '_How reliable do you find information about health and life-style in the following sources_'? The questions had a five-point response scale (5: Very useful/reliable - 1: Don't know).The same list of information sources was presented at both questions as at the question about purposive information seeking.

Factor analysis was used to extract latent factors on the questions about evaluation of information in the data from 2002 and 2007\. It was expected that some of the items on the list of information sources presented were measuring the same factor or different aspects of the same factor, and that a scale could be used to measure each factor. The Principal Component Factoring method of extraction was employed to examine the factor structure of each question. In all cases, the criteria for factor loadings were set above 0.4, and oblique rotation (Oblimin) was adopted in all the analyses. For all the analyses, multiple criteria, based on eigenvalue > 1.00, a scree test and conceptual interpretability of the factor structure, suggested that extracting three factors would be adequate. The factors were named: _media_, _health specialists_ and _Internet_. For the question about usefulness of information the factors explained 66.5% of the total variance in the data from 2002 and 67.3% in the data from 2007\. The scales were checked for internal reliability. Cronbach's alpha ranged from 0.84 to 0.91 for the scales in 2002 and from 0.83 to 0.92 for the scales in 2007\. Internal reliability, which reflects the precision of the measure (Cronbach 1951), of 0.70 is considered adequate (Nunnally 1978). For the question about reliability of information the factors explained 67.2% of the total variance in the data from 2002 and 70.3% in the data from 2007\. Cronbach's alpha ranged from 0.86 to 0.90 for the scales in 2002 and from 0.85 to 0.92 for the scales in 2007.

### Data analysis

Cluster analysis was used to determine how the participants formed distinct groups, based on their purposive information seeking in all the sources on the list presented by the question. This method was chosen because it can provide a perspective on the data different from traditional analysis of sociodemographic variables ([Everitt, _et al._ 2001](#eve01)).

In 2002 the cluster analysis was performed in two steps. The first step involved an agglomerative hierarchical clustering method, the Ward's method, which is a Euclidian distance method ([Everitt _et al._ 2001](#eve01)) and has been shown to outperform most other methods in identifying the number of clusters ([Aldenderfer and Blashfield 1984](#ald84)). This method was used to produce a dendrogram illustrating a cluster fusion in order to identify the possible number of existing clusters. The next step in the analysis involved k-means clustering method, based on Euclidian distances. The k-means method has the advantage of not requiring individuals to be irrevocably allocated to a cluster. To improve the statistical fit of the solution, individuals are reassigned iteratively to clusters until each person is closer to the mean of their cluster than to any other cluster mean ([Everitt _et al._ 2001](#eve01)). The k-means method is therefore likely to create more homogeneous clusters than, for example, hierarchical clustering methods. The k-means method requires the number of clusters to be specified beforehand. Ward's method indicated that three to four clusters might exist and in order to follow the recommendation to use the highest number of clusters, a four-cluster solution was drawn by the k-means method using the Iterate and Classify option within SPSS, version 11 ([Everitt _et al._ 2001](#eve01)). Based on theoretical reasons which were built on analysis of the data in 2002, a four-cluster solution was also drawn in 2007 by the same k-means method ([Aldenderfer and Blashfield 1984](#ald84); [Everitt _et al._ 2001](#eve01); [P?lsd?ttir 2005](#pal05) [2008](#pal08) [2010](#pal10)).

The analysis of the clusters evaluations of the usefulness and the reliability of information was performed with ANOVA (one-way) and a post-hoc test (Tukey) conducted to examine significant differences across the clusters.

For each cluster, 95% confidence intervals were computed for the means of the scales _media_, _health specialists_ and _Internet_ to examine if differences in the clusters' judgements of the usefulness and reliability of them were statistically significant. If the confidence limits of the scales do not overlap it can be said with 95% safety that, within each cluster, a statistically significant difference exists across the means of the scales. If the confidence limits, on the other hand, overlap, the difference between the means is not statistically significant.

## Results

Four clusters were drawn based on participants' health and life-style purposive information seeking. The clusters, which were named with reference to their purposive information seeking activity as it was in 2002, differed in size. The _passive_ cluster was largest with 192 members in 2002 and 148 in 2007\. The _moderately active_ cluster was the second largest with 112 members in 2002 and 105 in 2007\. The other two clusters, the _active_ cluster with 77 members in 2002 and 84 in 2007 and _moderately passive_ with 90 members in 2002 and 52 in 2007, were smaller

### Sex, age and education of the clusters

The socio-demographic characteristics of the clusters were examined and it was found that there was a difference by sex across the clusters, with women being in a majority in all clusters both in 2002 (?<sup>2</sup>(3)=23.78, p=0.001) and in 2007 (?<sup>2</sup>(3)=38.74, p=0.001), except for the _passive_ cluster, where there are more men. The age division of the clusters in 2002 (?<sup>2</sup>(12)=48.76, p=0.001) and 2007 (?<sup>2</sup>(12)=40.29, p=0.001) shows that the _active_ and _moderately passive_ clusters have a higher rate of young participants than the _moderately active_ and the _passive_ clusters, which have a fairly even age distribution and a higher rate of members from the older age groups than the other two clusters. The results for educational difference across the clusters in 2002 (?<sup>2</sup>(6)=240.07, p=0.001) and 2007 (?<sup>2</sup>(6)=18.40, p=0.010) show that the _moderately passive_ cluster is the most educated one, with the highest rate of members with university education and the lowest with primary school education. The _active_ cluster has the next most education. The passive cluster members have the lowest educational level, with the lowest percentage of members having a university degree. (See also [P?lsd?ttir 2010](#pal10)).

### Evaluations of the usefulness of information in media, from health specialists and on the Internet across the clusters, in 2002 and 2007

ANOVA was used to examine differences in judgements about the usefulness of information across the clusters. The results show a significant difference across the clusters for the _media_ in 2002 (F(3,298)=8.4, p<0.001) and 2007 (F(3,370)=24.62, p<0.001), for _health specialists_ in 2002 (F(3,233)=35.8, p<0.05) and 2007 (F(3,353)=27.71, p<0.001) and for the _Internet_ in 2002 (F(3,117)=3.45, p<0.05). The clusters, however, did not differ significantly in their evaluation of information on the Internet in 2007 (p=.116). The results are presented in mean figures where, 1 is lowest and 5 is highest (Table 3).

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Differences in evaluation of the usefulness of information in the media, by Health specialists and on the Internet across the clusters, in 2002 and 2007**  
</caption>

<tbody>

<tr>

<th>Topics</th>

<th>Passive</th>

<th>Moderately passive</th>

<th>Moderately active</th>

<th>Active</th>

</tr>

<tr>

<td>  
</td>

<td align="center">**2002 2007**  
</td>

<td align="center">**2002 2007**  
</td>

<td align="center">**2002 2007**  
</td>

<td align="center">**2002 2007**  
</td>

</tr>

<tr>

<td>media</td>

<td align="center">2.54<sup>ab</sup> 2.47<sup>a</sup></td>

<td align="center">2.31<sup>a</sup> 2.39<sup>a</sup></td>

<td align="center">2.79<sup>b</sup> 2.76<sup>b</sup></td>

<td align="center">2.61<sup>b</sup> 2.82<sup>b</sup></td>

</tr>

<tr>

<td>Health specialists</td>

<td align="center">2.99<sup>a</sup> 2.99<sup>a</sup></td>

<td align="center">3.34<sup>b</sup> 3.39<sup>b</sup></td>

<td align="center">3.22<sup>ab</sup> 3.16<sup>ab</sup></td>

<td align="center">3.23<sup>ab</sup> 3.14<sup>ab</sup></td>

</tr>

<tr>

<td>Internet</td>

<td align="center">2.01<sup>a</sup> 2.27<sup>a</sup></td>

<td align="center">2.13<sup>ab</sup> 2.35<sup>a</sup></td>

<td align="center">2.22<sup>ab</sup> 2.38<sup>a</sup></td>

<td align="center">2.52<sup>b</sup> 2.62<sup>a</sup></td>

</tr>

</tbody>

</table>

A cluster mean is significantly different from another mean (Tukey, p<0.05) if they have different superscripts. A cluster mean with two superscripts is not statistically different from the means of two of the other clusters.

Table 3 shows that in 2002 the _active_ and _moderately active_ clusters found information in the media more useful than the _moderately passive_ cluster. The _passive_ cluster did not differ significantly from the other clusters (Tukey, p<0.05). In 2007 the _active_ and _moderately active_ clusters found information in the media more useful than both the _moderately passive_ and the _passive_ clusters (Tukey, p<0.05). Results about health specialists show that in both 2002 and 2007 the _moderately passive_ cluster considered the information as more useful than the _passive_ cluster. The other two clusters did not differ from neither the _passive_ nor the _moderately passive_ cluster (Tukey, p<0.05). In 2002 the _passive_ cluster found information on the Internet least useful and the _active_ cluster most useful. The other two clusters did not differ from neither the _passive_ nor the _active_ cluster (Tukey, p<0.05). In 2007, however, there was no significant difference across the clusters for their evaluation of the usefulness of information on the Internet (Tukey, p<0.05).

### Evaluations of the usefulness of information in the media, from health specialists and on the Internet by each cluster, in 2002 and 2007

For each cluster, the confidence intervals (95%) were computed for the means of the information channels, to examine if the differences in the clusters evaluation of the usefulness of the information in the channels were significant. Results about the confidence intervals in 2002 are presented in Figure 1 and results from 2007 in Figure 2\. The vertical lines in the pictures indicate the position of the upper and lower confidence limits for the media, Health specialists and the Internet, for each cluster. Furthermore, the median for the confidence limits is presented for each information channel.

<div align="center">![Confidence intervals for evaluations of the usefulness of information in the media, from health specialists and on the Internet by each cluster, in 2002](p470fig1.jpg)</div>

<div align="center">  
**Figure 1: Confidence intervals for evaluations of the usefulness of information in the media, from health specialists and on the Internet by each cluster, in 2002**</div>

All the clusters found information from health specialists to be significantly more useful than information in the media and on the Internet in 2002\. Information on the Internet was considered significantly less useful than information in the media by the _passive_ and the _moderately active_ clusters. As the confidence limits for evaluations of the media and the Internet overlapped for the _moderately passive_ and _active_ clusters, there were no significant differences in the evaluation (Figure 1).

<div align="center">![Confidence intervals for evaluations of the usefulness of information in the media, by health specialists and on the Internet by each cluster, in 2007](p470fig2.jpg)</div>

<div align="center">  
**Figure 2: Confidence intervals for evaluations of the usefulness of information in the media, from health specialists and on the Internet by each cluster, in 2007**</div>

In 2007, information from health specialists was considered to be significantly more useful than information in the media and on the Internet, by all clusters, as in 2002\. However, in 2007, the confidence limits for evaluations of the media and the Internet overlapped for all four clusters and therefore no significant difference were found in the evaluation (Figure 2).

### Evaluations of the reliability of information in media, from health specialists and on the Internet across the clusters, in 2002 and 2007

ANOVA revealed a significant difference across the clusters for the media in 2002 (F(3,237)=10.5, p<0.001) and 2007 (F(3,279)=16.4, p<0.001), for health specialists in 2002 (F(3,337)=4.99, p<0.01) and 2007 (F(3,267)=13.85, p<0.001), and for the Internet in 2002 (F(3,128)=4.6, p<0.05) and 2007 (F(3,155)=6.93, p<0.001) (see Table 4).

<table width="60%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4 Differences in evaluation of the reliability of information in the media, by health specialists and on the Internet across the clusters, in 2002 and 2007**  
</caption>

<tbody>

<tr>

<td>**Topics**  
</td>

<td align="center">**Passive**  
</td>

<td align="center">**Moderately passive**  
</td>

<td align="center">**Moderately active**  
</td>

<td align="center">**Active**  
</td>

</tr>

<tr>

<td>  
</td>

<td align="center">**2002 2007**  
</td>

<td align="center">**2002 2007**  
</td>

<td align="center">**2002 2007**  
</td>

<td align="center">**2002 2007**  
</td>

</tr>

<tr>

<td>media</td>

<td align="center">2.36<sup>a</sup> 2.20<sup>a</sup></td>

<td align="center">2.20<sup>a</sup> 2.24<sup>a</sup></td>

<td align="center">2.68<sup>b</sup> 2.65<sup>b</sup></td>

<td align="center">2.60<sup>b</sup> 2.66<sup>b</sup></td>

</tr>

<tr>

<td>health specialists</td>

<td align="center">2.99<sup>a</sup> 2.99<sup>a</sup></td>

<td align="center">3.34<sup>b</sup> 3.39<sup>b</sup></td>

<td align="center">3.22<sup>ab</sup> 3.16<sup>ab</sup></td>

<td align="center">3.23<sup>ab</sup> 3.14<sup>ab</sup></td>

</tr>

<tr>

<td>Internet</td>

<td align="center">3.19<sup>a</sup> 2.95<sup>a</sup></td>

<td align="center">3.49<sup>b</sup> 3.55<sup>b</sup></td>

<td align="center">3.36<sup>ab</sup> 3.41<sup>b</sup></td>

<td align="center">3.35<sup>ab</sup> 3.35<sup>b</sup></td>

</tr>

</tbody>

</table>

A cluster mean is significantly different from another mean (Tukey, p<0.05) if they have different superscripts. A cluster mean with two superscripts is not statistically different from the means of two of the other clusters.

The _active_ and _moderately active_ clusters found information in the media more reliable than the _moderately passive_ and _passive_ clusters in both 2002 and 2007 (Tukey, p<0.05). Results about health specialists show that in 2002 the _passive_ cluster considered the information as less reliable than the _moderately passive_ cluster. The other two clusters did not differ significantly from the _passive_ and _moderately passive_ clusters in 2002\. In 2007 the _passive_ cluster found information by health specialists as less reliable than all the other clusters (Tukey, p<0.05). In 2002, the _passive_ and _moderately passive_ clusters found information on the Internet less reliable than the _active_ cluster but the _moderately active_ cluster did not differ significantly from the other three clusters. In 2007 the _passive_ cluster still found information on the Internet as less reliable than the _active_ cluster but neither _moderately passive_ nor _moderately active_ clusters differed significantly from the other two clusters (Tukey, p<0.05). (Table 4)

### Evaluations of the reliability of information in media, by health specialists and on the Internet by each cluster, in 2002 and 2007

For each cluster, the confidence intervals (95%) were computed for the means of the channels, to examine if the differences in the clusters judgements of the reliability of the information of the channels were significant. Results about the confidence intervals in 2002 are presented in Figure 3 and results from 2007 in Figure 4\. The vertical lines in the pictures indicate the position of the upper and lower confidence limits for the media, health specialists and the Internet, for each cluster. Furthermore, the median for the confidence limits is presented for each information channel.

<div align="center">![Confidence intervals for evaluations of the reliability of information in the media, deom health specialists and on the Internet by each cluster, in 2002](p470fig3.jpg)</div>

<div align="center">**Figure 3: Confidence intervals for evaluations of the reliability of information in the media, deom health specialists and on the Internet by each cluster, in 2002**</div>

Figure 3 shows that in 2002 all clusters considered information by health specialists to be significantly more reliable than information in the media and on the Internet. The _passive_ and the _moderately active_ clusters found information on the Internet to be significantly less reliable than information in the media. For the _moderately passive_ and the _active_ clusters, the confidence limits for evaluations of the media and the Internet overlapped and therefore no significant difference was found.

<div align="center">![Confidence intervals for evaluations of the reliability of information in the media, by health specialists and on the Internet by each cluster, in 2007](p470fig4.jpg)</div>

<div align="center">**Figure 4: Confidence intervals for evaluations of the reliability of information in the media, from health specialists and on the Internet by each cluster, in 2007**</div>

All clusters considered information from health specialists to be significantly more reliable than information in the media and on the Internet in 2007, as in 2002\. The confidence limits for evaluations of the media and the Internet overlapped for all four clusters and therefore no significant difference was found.

## Discussion

Knowledge about how the general public evaluates health information in diverse information sources and channels and how the evaluation has developed in line with changes in the information environment has been scarce. The present study attempts to shed some light on the subject by examining how four clusters of people evaluated the usefulness and reliability of health and life-style information, within a broad network of sources in four information channels. It, furthermore, explored whether or not there has been a statistically significant change in the evaluation from 2002 to 2007.

The results from the study show that the _passive_ cluster evaluates the usefulness and reliability of information, in general, lower than the other three clusters (Tables 3 and 5). The _passive_ cluster has, furthermore, been found to seek information least often in all information channels (P?lsd?ttir [2010](#pal10)). Although a statistical examination of the relationship between information seeking activities and the participants' evaluation of information has not been performed, the findings nevertheless show some signs that there might be a connection between these two factors. It might therefore be worthwhile to look into this matter in the future.

An examination of each cluster's judgements of how useful and reliable information in the media, health specialists and the Internet were in 2007, compared to 2002, found that in both 2002 and 2007, all the clusters considered information by health specialists to be more useful and more reliable than information in the other two channels. Results about the _moderately passive_ and the _active_ clusters in 2002 show that the means for the media were a bit higher than for the Internet, both for the evaluation of the usefulness and the reliability of information, but the difference was small and not statistically significant. There was, on the other hand, a bigger difference across the media and the Internet by the _passive_ and _moderately active_ clusters, who considered information on the Internet as neither as useful nor as reliable as information in the media. In 2007, however, this had changed and for none of the clusters a significant difference was found across the media and the Internet. Even thought the _moderately active_ cluster still rated the usefulness and reliability of information on the Internet (2.38/2.35) lower than in the media (2.76/2.65) the difference across these two information channels was not statistically significant. The results confirm prior findings that information on the Internet is deemed less reliable than information provided by health centres ([Marshall and Williams 2006](#mar06)). The findings are also somewhat similar to the results of two studies, conducted in seven European countries in 2005 and 2007, which examined how important people thought that health information on the Internet were compared to more traditional sources. The results indicate that the perceived importance of online health information is growing, particularly in countries that had high Internet usage ([Kummervold _et al._ 2008](#kum08)).

Among the factors that may have an impact on how people perceive the Internet is how easily they can access it as how experienced they are at seeking online information ([Ratchford _et al._ 2001](#rat01)). The prevalence of Icelanders with Internet access has increased steadily in the past years. In 2002, a total of 73% Icelanders had access to the Internet from their homes ([Nordic Council of Ministers 2002](#nor02)), in 2007 the figure had gone up to 84%; of those 89% had high-speed access, ([Statistics Iceland 2007](#sta07)). Previous examinations of the data from the current study has found the _passive_ and _moderately active_ clusters to seek information on the Internet less often than the other two clusters in 2002 and in 2007\. However, the means of the _passive_ and _moderately active_ clusters for information seeking on the Internet in 2007 are a bit higher than the means for 2002 ([Pálsdóttir 2010](#pal10)).

Thus, it is possible that better options of reaching online information, together with more experience at information seeking and thereby getting more acquaintance with health and life-style information on the Internet, has to some extent had an effect on the evaluation of the information. Another reason for the lack of difference between the media and the Internet in 2007 could be the blurring of the boundaries between publications in print and in digitalised form. Although an investigation comparing the availability of health information in Icelandic in the period 2002 and 2007 has not been made, it nevertheless can be presumed that the amount of online information has increased since 2002\. Information that used to appear only in print was often also available in digitalised form in 2007, e.g., newspapers or magazines offering Websites. Furthermore, the amount of information on the Website of the Public Health Institute of Iceland has grown steadily in the period 2002 to 2007, with brochures, articles and news about research findings, published by the institute appearing on the Website.

The study is limited by the response rate which was 51% in 2002 and 47% in 2007\. This can be considered satisfactory in a postal survey but nevertheless raises a question about the validity of the findings. On the other hand, the studies' findings are strengthened by the fact that the samples of respondents were compared to population parameters and found to reflect the population fairly well.

The Internet was still a relatively new medium in 2002, compared to the more traditional information channels, such as the media and health specialists. Bearing in mind that it takes time for people to adapt to new technology it may be considered natural that some groups in society were not ready to appreciate the information that could be found there in the same way as information in the more established information channels. The results of the study indicate that the importance of the Internet is growing and that may be well on its way to establish itself as equivalent to the more traditional information channels.

## Acknowledgements

The study was funded by the University of Iceland Research Fund. The author wishes to acknowledge the contribution of The Social Science Research Institute, University of Iceland, for assistance and statistical advice during data collection and analysis.

## About the author

Ágústa Pálsdóttir is a Professor in the Department of Library and Information Science, University of Iceland. She received her Bachelor's degree in Library and Information Science and Master of Library and Information Science from University of Iceland and PhD in Information studies from the Department of Information Studies, Abo Akademi University, Finland.

<h>References

*   Aldenderfer, M.S. & Blashfield, R.K. (1984). _Cluster analysis._ Newbury Park, CA: Sage Publications Inc. (Quantitative Applications in the Social Sciences, 44)
*   Barry, C.L. (1994). User-defined relevance criteria: an exploratory study. _Journal of the American Society for Information Science,_ **45**(3), 149-159
*   Barry, C.L. & Schamber, L. (1998). Users' criteria for relevance evaluation: a cross-situational comparison. _Information Processing & Management,_ **34**(2/3), 219-236
*   Bedell, S.E., Agrawal, A. & Petersen, L.E. (2004). A systematic critique of diabetes on the world wide Web for patients and their physicians. _International Journal of Health Informatics,_ **73**(9-10), 687-694
*   Borlund, P. (2003). The concept of relevance in IR. _Journal of the American Society of Information Science and Technology,_ **54**(10), 913-925
*   Cline, R.J.W. & Haynes, K.M. (2001). Consumer health information seeking on the Internet: the state of the art. _Health Education Research,_ **16**(6), 671-692
*   Cronbach, L.J. (1951). Coefficient alpha and the internal structure of tests. _Psychometrika,_ **16**(3), 297-334
*   Everitt, B.S., Landau, S. & Leese M. (2000). _Cluster analysis._ 4th. ed. London: Arnold
*   Eysenbach G., Powell J., Kuss O. & Sa E.R. (2002). Empirical studies assessing the quality of health information for consumers on the world wide Web: a systematic review. _Journal of the American Medical Association,_ **287**(20), 2691-2700
*   Fox, S., Rainie, L., Horrigan, J., Lenhart, A., Spooner, T., Burke _et al._ (2000). _[The online health care revolution: how the Web helps Americans take better care of themselves](http://www.Webcitation.org/5wIpLkMAI)._ Washington, DC: Pew internet & American life Project. Retrieved 21 November, 2007 from http://www.pewinternet.org/~/media//Files/Reports/2000/PIP_Health_Report.pdf (Archived by WebCite? at http://www.Webcitation.org/5wIpLkMAI)
*   Fox, S. & Jones, S. (2009). _The social life of health information: Americans' pursuit of health takes place within a widening network of both online and offline sources._ Washington, D.C.: Pew Internet & American Life Project. Retrieved 10 February, 2010 from http://www.pewinternet.org/Reports/2009/8-The-Social-Life-of-Health-Information.aspx (Archived by WebCite? at http://www.Webcitation.org/5wIptt5O2)
*   Greisdorf, H. (2003). Relevance thresholds: a multi-stage predictive model of how users evaluate information. _Information Processing & Management,_ **39**(3), 403-423
*   Harter, S.P. (1992). Psychological relevance and information science. _Journal of the American Society for Information Science,_ **43**(9), 602-615
*   Kummervold, P.E., Chornaki, C.E., Lausen, B., Prokosch, H.U., Rassmussen, J., Santana, S. _et al._ (2008). [eHealth trends in Europe 2005-2007: a population-based survey](http://www.Webcitation.org/5wIq4tKdd). _Journal of Medical Internet Research,_ **10**(4), e42\. Retrieved 20 January, 2010 from http://www.ncbi.nlm.nih.gov/pmc/issues/175768/ (Archived by WebCite? at http://www.Webcitation.org/5wIq4tKdd)
*   Marshall, L.A. & Williams, D. (2006). Health information: does quality count for the consumer? How consumers evaluate the quality of health information materials across a variety of media. _Journal of Librarianship and Information Science,_ **38**(3), 141-156
*   Mizzaro, S. (1997). Relevance: the whole history. _Journal of the American Society for Information Science,_ **48**(9), 810-832
*   Nordic Council of Ministers. (2002). _Nordic information society statistics 2002._ Helsinki: Nordic Council of Ministers
*   Nunnally, J.C. (1978). _Psychometric theory._ (2nd. ed.). New York, NY: McGraw-Hill
*   Pálsdóttir, A. (2005). _Health and life-style: Icelanders' everyday life information behaviour_. ?bo (Turku), Finland: ?bo Akademi University press. (Published PhD dissertation).
*   Pálsdóttir, A. (2008). [Information behaviour, health self-efficacy beliefs and health behaviour in Icelanders' everyday life.](http://www.Webcitation.org/5wIqCw301) _Information Research,_, **13**(1) paper 334\. Retrieved 20 May, 2009 from http://InformationR.net/ir/13-1/paper334.html (Archived by WebCite? at http://www.Webcitation.org/5wIqCw301)
*   Pálsdóttir, A. (2009). [Seeking information about health and life-style on the Internet](http://www.Webcitation.org/5wIqMD0Sk). _Information Research,_ **14**(1) paper 389\. Retrieved 22 Januar, 2010 at http://InformationR.net/ir/14-1/paper389.html (Archived by WebCite? at http://www.Webcitation.org/5wIqMD0Sk)
*   Pálsdóttir, A. (2010). The connection between purposive information seeking and information encountering: a study of Icelanders' health and life-style information seeking. _Journal of Documentation,_ **66**(2), 224-244
*   Ratchford, B.T., Talukdar, T. & Lee, M.S. (2001). A model of consumer choice of the internet as an information source. _International Journal of Electronic Commerce,_ **5**(3), 7-21
*   Saracevic, T. (1975). Relevance: a review of and a framework for the thinking on the notion in information science. _Journal of the American Society for Information Science,_ **26**(6), 321-343
*   Savolainen, R. (2007). [Media credibility and cognitive authority: the case of seeking orienting information](http://www.Webcitation.org/5wIqVGyk0). _Information Research,_, **12**(3) paper 319\. Retrived 20 February, 2010 from http://InformationR.net/ir/12-3/paper319.html (Archived by WebCite? at http://www.Webcitation.org/5wIqVGyk0)
*   Savolainen, R. & Kari, J. (2006). User-defined relevance criteria in Web searching. _Journal of Documentation,_ **62**(6), 685-707
*   Schamber, L. (1994). Relevance and information behavior. _Annual Review of Information Science and Technology,_ **29**, 3-48
*   Schamber, L., Eisenber, M.B. & Nilan, M. (1990). A re-examination of relevance: toward a dynamic, situational definition. _Information Processing & Management,_ **26**(6), 755-776
*   Spink, A., Greisdorf, H. & Bateman, J. (1998). From highly relevant to not relevant: examining different regions of relevance. _Information Processing & Management,_ **34**(5), 599-621
*   Statistics Iceland (2007). _[Use of ICT by households and individuals in 2007](http://www.Webcitation.org/5wIqdFpuY)._ Reykjav?k: Statistics Iceland. Retrieved 28 January, 2010 from https://hagstofa.is/?PageID=421&itemid=b2f90b6b-6383-4c04-83ac-2385068d1722 (Archived by WebCite at http://www.Webcitation.org/5wIqdFpuY)
*   Tang, R. & Solomon, P. (1998). Toward an understanding of the dynamics of relevance judgment: an analysis of one person's search behavior. _Information Processing & Management,_ **34**(2/3), 237-256
*   Tang, R. & Solomon, P. (2001). Use of relevance criteria across stages of document evaluation: on the complementary of experimental and naturalistic studies. _Journal of the American Society for Information Science and Technology,_ **52**(8), 676-685

