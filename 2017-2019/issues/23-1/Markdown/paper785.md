<header>

#### vol. 23 no. 1, March, 2018

</header>

<article>

# Which academic papers do researchers tend to feature on ResearchGate?

## [Xuan Zhen Liu](#author), and [Hui Fang](#author).

> **Introduction**. The academic social network site ResearchGate (www.researchgate.net) enables researchers to feature up to five of their research products (including papers, datasets and chapters) in a 'Featured research' section on their ResearchGate home page. This provides an opportunity to discover how researchers view their own publications.  
> **Method**. We investigated ResearchGate members who self-selected their featured publications and compared these publications with other publications listed in ResearchGate.  
> **Analysis**. We analysed the distribution of featured publications in the ResearchGate members' whole publications in terms of their importance, recency, citation and source reputation.  
> **Results**. Researchers prefer to feature publications in which they played important roles, those that were recently published, highly cited and published by reputable sources.  
> **Conclusion**. By featuring their research, researchers can highlight their academic achievements and create the potential to contribute to the wider scientific community through the future application or discussion of their work. Instances of featured research published for a relatively long period in somewhat obscure journals, with few citations, show that there is a difference between how these papers are valued by the scientific community and the authors who featured them. The presented method may be applied to find latent sleeping beauties.

<section>

## Introduction

Research publications are often evaluated with bibliometric indicators from an outside perspective, such as institutions or other researchers. One indicator is the number of citations a paper receives, which is a measure of a paper's impact on the scientific community. Though researchers dispute the association of citation numbers with importance, creativity, quality, eminence and persuasiveness ([Shadish,Tolliver, Gray and Gupta 1995](#sha95)), a paper's citation number is generally regarded as providing credit to its authors (e.g., [Gazni and Ghaseminik, 2016](#gaz16); [Sahoo, 2016](#sah16)). When a paper is written by more than one author, credit assignment methods aim to fairly allocate credit to each author according to their importance to the paper. As the proportion of papers with alphabetical authorship decreases ([Waltman, 2012](#wal12)), credit assignment has changed from even assignment, such as normal (or standard) counting ([Lindsey, 1980](#lin80)) and fractional counting ([de Solla Price, 1981](#de81)), to uneven assignment (e.g., [Egghe, Rousseau and Van Hooydonk, 2000](#egg00); [Trueba and Guerrero, 2004](#tru04); [Liu and Fang, 2012](#liu12); [Stallings _et al._, 2013](#sta13)). Another simple indicator to assess a paper is the reputation of the journal in which it is published. Not all papers published in high-reputation journals are of a high quality ([Haunschild and Bornmann, 2015](#hau15)) and prominent journals have rejected high-quality papers, even those of Nobel-class ([Campanario, 2009](#cam09)). The scientific community values papers published in prominent journals because authors are most likely to be proud of the associated prestige. Another possible reason is that papers in these journals have a high possibility of being highly cited ([Wang _et al._, 2012](#wan12)).

Contrary to evaluation from an outside perspective, how researchers perceive their own work is seldom considered. A potential reason for this is that there have been few channels for obtaining such information. The academic social network website ResearchGate, www.researchgate.net ([Thelwall and Kousha, 2014](#the14), [2015a](#the15a); [Jeng _et al._, 2017](#jen17)) provides a chance to address this problem. ResearchGate has been used in the study of altmetrics ([Haustein _et al._, 2014](#hau14); [Ortega, 2015](#ort15); [Thelwall and Kousha, 2015b](#the15b); [Hoffmann, Lutz and Meckel, 2016](#hof16)) and found to have a self-quantification function for its members ([Hammarfelt, de Rijcke and Rushforth, 2016](#ham16)), although research evaluations based on indicators provided by ResearchGate or other social media are still in dispute ([Jordan, 2015](#jor15); [Kraker and Lex, 2015](#kra15); [Jamali, Nicholas and Herman, 2016](#jam16); [Memon, 2016](#mem16); [Nicholas, Clark and Herman, 2016](#nic16); [Orduna-Malea _et al._, 2017](#ord17); [Thelwall and Kousha, 2017](#the17)). Here we try to mine other useful information from its contents. ResearchGate members are mainly researchers or working in the sciences and their research products (including papers, datasets and chapters) are listed on ResearchGate. Up to five research products can be selected by each member to be listed in the 'Featured research' section of their ResearchGate home page. Most research products listed as featured research are members' academic publications. A research product has higher visibility when chosen as featured research than it would have when not featured.

To the best of our knowledge, there has yet to be an investigation of featured research in ResearchGate. There are some reports about featured articles on other platforms, such as Wikipedia, where editors award the distinction of featured article to those they have determined to have the highest quality ([Ransbotham and Kane, 2011](#ran11); [Ingawale _et al._, 2013](#ing13)). Additionally, journals feature papers on their cover as the cover paper. Some journals feature the best paper of an issue (such as a future Nobel Prize contender) while others choose those with eye-catching or striking images ([Wang, Liu and Mao, 2015](#wan15)). For featured articles both on Wikipedia and in journals, the selection is made by people other than the authors. In contrast, ResearchGate enables its members to select their own research products to feature. According to the criteria for featuring articles in Wikipedia and journals, we supposed that if the featured products are selected by members themselves, ResearchGate members would feature their best or most important publications, or those to which they want to draw the most attention. Under the current academic research evaluation system, citations, source reputation of a paper, and roles played in a paper, can bring credit to researchers. Recent publications represent an author's research advancements and capability.

In this paper, we investigated how researchers view their own publications by comparing ResearchGate members' featured research and other publications. Our research questions are: (a) What kinds of publications are featured by their authors? (b) What do researchers consider when choosing publications to feature on ResearchGate? (c) Are there papers that the author values but others have not recognised? Suggestions for researchers and the scientific community regarding these points were provided according to the answers.

</section>

<section>

## Data collection

Information was collected about ResearchGate members' publications and featured research from universities in the US. These universities were selected from the 2016 National University Rankings by US News & World Report. Collecting data from ResearchGate is time consuming and requires considerable resources, so only one of every five universities out of the first 231 listed (those with a rank provided) were selected (See Table A1). The selected universities had their ranks schemed as : 1, 6, 11, 16, ..., 221, 226, 231\. Some universities may have the same rank. For example, Columbia University and Stanford University tie for 5th (they cover rank 6). For such cases, the university with the most ResearchGate members was selected. Following this approach, for the twelve-university tie for 220th (covering ranks 221, 226 and 231), New Mexico State University, University of Massachusetts Boston and Utah State University were selected based on the number of ResearchGate members.

If a member has more than three research products, ResearchGate lists three of them as the default, featured research according to the recency of the publication, its citation and view counts, among other criteria. Members can change this listing and select up to five featured research products. As many members might not notice or care to use this function and do not change the default, we needed to identify those who did select their featured research. Members with more than three featured research products set the featured research by themselves. Therefore, for each university selected, we investigated only the ResearchGate members with four or five featured research products. [Table A1](#A1) lists the numbers of such members from each university.

In this study, we compared the reputation of sources (academic journals and conferences) publishing the featured research and other publications of the selected members. We only considered papers whose sources are listed in ResearchGate. Some ResearchGate members feature all their publications; thus, information about such members is meaningless for the statistics in this study. Therefore, we only counted ResearchGate members with at least 10 publications whose sources are listed in ResearchGate. A total of 2,708 ResearchGate members were investigated. We collected information about their cumulative 95,424 publications and 11,821 of those chosen as featured research. Note, featured research is designated in ResearchGate, while all the featured research products we selected for investigation were papers published in academic journals and conferences that were denoted as featured publications.

The reputation of the publication source is represented by SCImago Journal Rank (SJR) from Elsevier-Scopus, a size-independent indicator of journals' and conferences' influence or prestige ([Gonzalez-Pereira, Guerrero-Bote and Moya-Anegon, 2010](#gon10); [Olmeda-Gomez and de Moya-Anegon, 2016](#olm16)). 2015 SJR was used to measure the sources. For the renamed journals, we used the SJR value of the journal under its new title. If a journal or conference had no SJR, then we set it as 0.

Besides the source, we also obtained the publications' ages, citations received (collected by ResearchGate), the author rank of the ResearchGate member, number of authors per publication and the academic positions of selected ResearchGate members who listed their academic positions in ResearchGate. Data collection was completed on 1 November 2016.

Some readers might not know how to edit their own featured research in ResearchGate. The steps for ResearchGate members to set their featured research are as follows: click on their avatar in the upper-right corner of the ResearchGate page and find the Featured research section in the Overview tag page. Next, scroll to the Edit link in the upper-right corner of the section (the link is an inconspicuous pen icon when the mouse is outside the Featured research section). Click this link and a pop-up dialog box will appear, in which ResearchGate members can select or delete their own featured research products on their ResearchGate home page.

</section>

<section>

## Methodology

### Estimation of authorship tendency of featured research

ResearchGate only lists up to four authors for each publication: i.e., the first, second, third and last author. If a publication has more than four authors, ResearchGate provides a link indicating the number of remaining authors. When the link is clicked, up to 50 additional authors are visible. If the target author is not in the expanded list, the link can be clicked again to reveal the next 50 authors.

We used the author rank information of the inspected ResearchGate member based on the following criteria: whether he or she is the first, second, third or last author. This is because those positions are usually used to represent author importance. Our investigation is to check whether the member is important to his or her publications and thus there is little need for the time-consuming and tedious work required to check author position by expanding the link.

To differentiate the importance of a ResearchGate member in each paper with his or her various positions in the by-line among his or her publications, we used the axiomatic approach ([Stallings _et al._, 2013](#sta13)). This assigns credit of a given paper to the co-authors to estimate the proportion of the contribution made by the ResearchGate member to the paper. We used the axiomatic approach with equal contribution groups. We tied the first and last author for the most important author, because many last authors are leaders of the research ([Zhao and Strotmann, 2011](#zha11)) and are the corresponding or senior authors ([Riesenberg and Lundberg, 1990](#rie90); [Liu and Fang, 2014](#liu14)). This created the possibility for error because some last authors are the least important; this kind of error will be discussed in the following sections. We tied the authors other than the first, second, third and last (denoted as middle authors for short) for the fourth and least important authors. This also presented the potential for introduced error and will be discussed in the following sections. Then the credit assigned to an author was estimated as ([Stallings _et al._, 2013](#sta13)):

<figure>

![equation 1](../p785fig1.png)

<figcaption>(1)</figcaption>

</figure>

where _m_ is the number of author groups based on author importance. If the paper has one or two authors, then _m_ = 1\. If the author number is three or four, then _m_ is 2 or 3, respectively. If the authors are more than four, then _m_ is 4\. _r_ is the rank of author group. If the ResearchGate member is the last author, then _r_ is 1, tied for the first author. If there are more than four authors of the paper and the ResearchGate member is a middle author, then _r_ is 4\. Otherwise, _r_ is the author rank of the member in the by-line. _n<sub>k</sub>_ is the number of authors in the _k_-th author group. If the author number is 1, then _n_<sub>1</sub> = 1, otherwise _n_<sub>1</sub> = 2\. If the paper has more than two or three authors, then _n_<sub>2</sub> = _n_<sub>3</sub> = 1\. If there are more than four authors, then _n_<sub>4</sub> equals the number of authors minus 4.

We then ranked each selected ResearchGate member's papers using the member's estimated importance to the papers, according to Equation 1\. The higher the _c_(_r_, the greater the importance of the member to the paper. Each member's papers are ranked in descending order of _c_(_r_). We gave each rank in this queue a normalised score from 0 to 1\. Supposing a member has _N_ papers in such a queue, the score of the _k_-th (1 ≤ _k_ ≤ _N_) paper is:

<figure>

![equation 2](../p785fig3.png)

<figcaption>(2)</figcaption>

</figure>

If several papers have the same _c_(_r_), we used the average of their scores to replace the individual scores. The average and median value of all the paper scores in the queue is 0.5.

Suppose a member has _N_<sub>f</sub> featured publications inspected in this study. We denoted the score given to the _i_-th (1 ≤ _i_ ≤ _N<sub>i</sub>_) featured publication as _s_<sub>ac_f</sub>(_k<sub>i</sub>_), where _k<sub>i</sub>_ is the rank of the _i_-th featured publication in the queue. If the member featured publications in which he or she played an important role, then the average value of all _s_<sub>ac_f</sub>(_k<sub>i</sub>_) is low.

</section>

<section>

### Estimation of recency tendency of featured research

To investigate the recency tendency of featured publications, we ranked each ResearchGate member's papers in descending order of publication time, which is when a paper was published by the journal that accepted it. The first paper (rank 1) in this queue is the most recently published. We gave each rank in this queue a normalised score, similar to Equation 2\. The only difference is that we used the rank of recency of the papers to replace the rank of author's importance. We denoted the score of the _i_-th featured publication in this queue as _s_<sub>rec_f</sub>(_k<sub>i</sub>_).

</section>

<section>

### Estimation of citation tendency of featured research

To investigate the impact tendency of featured publications, we ranked each selected ResearchGate member's papers in descending order of their number of citations. Because citations require time to accumulate, we only compared papers published before 2015; thus, most of them had at least two years to accumulate citations. We gave each rank in this queue a normalised score, similar to Equation 2\. The only difference is that we used the rank of paper citation numbers to replace the rank of author's importance. We denoted the score of the _i_-th featured publication in this queue as _s_<sub>cit_f</sub>(_k<sub>i</sub>_). In this comparison, we chose the ResearchGate members with more than ten publications published before 2015\. We used 67,397 selected papers, 6,200 of which are ResearchGate members' featured publications.

The number of citations increases with a paper's age. A newer publication may have lower citations at the time of investigation than an older one, not because its potential total impact is low, but because it is more recent and thus has not had enough time to accumulate a high number of citations. To correct this bias in comparing papers with different ages, we also ranked the papers of each selected member according to their average citations a year, or citation rate ([Cano and Lind, 1991](#can91)), in descending order. The score of the _i_-th featured publication in this queue is denoted as _s_<sub>citr_f</sub>(_k<sub>i</sub>_). The samples used in this comparison are same as those in the previous paragraph.

</section>

<section>

### Estimation of source reputation tendency of featured research

To investigate the source reputation tendency of featured publications, we ranked each selected ResearchGate member's papers in descending order of the publication sources' SJR. The first paper (rank 1) in this queue was published by the most reputable source. We gave each rank in this queue a normalised score, similar to Equation 2\. The only difference is the use of the rank of source SJR in place of the author's importance. We denoted the score of the _i_-th featured publication in this queue as _s_<sub>sr_f</sub>(_k<sub>i</sub>_).

</section>

<section>

## Results and discussion

### Authorship tendency of featured research

Figure 1 shows the cumulative distribution of _s_<sub>ac_f</sub> of all the selected ResearchGate members' featured publications in their own paper queues ranked in the descending order of the proportion of paper credit assigned to the member. The average and median value of _s_<sub>ac_f</sub> is 0.418 and 0.381\. If there is no preference in the selection of featured publications by ResearchGate members, _s_<sub>ac_f</sub> obeys uniform distribution and its mathematical expectation is 0.5\. Thus, it denies that ResearchGate members did not consider their rank in by-lines when they selected publications to feature, with _p_ < 0.001\. Members tended to select papers in which they were important authors as featured publications.

<figure>

![Figure 1](../p785fig6.png)

<figcaption>

Figure 1: Cumulative distribution of _s_<sub>ac_f</sub> of all of the selected ResearchGate members' featured publications. _s_<sub>ac_f</sub> is defined in the text.</figcaption>

</figure>

When we estimated the importance of ResearchGate members to their publications, we regarded the last author as the most important; however, there are situations in which the last author is the least important. Additionally, middle authors have differing levels of importance and might have a higher status in some papers (e.g., they may be corresponding authors or co-senior author). Thus, this could introduce errors in the estimations. Here, we use the data in Table 1 to support the above conclusion. In more than 60% of the featured publications, ResearchGate members are the first author, who is of high importance for papers other than those with alphabetical authorship. When all ResearchGate publications are considered, the proportion of papers for which the ResearchGate member is the first author is lower (36.1%). The proportion of papers naming the ResearchGate member first is comparatively higher among featured publications, which holds true for all the ResearchGate member groups that we identified. In addition, for all identified member groups, the proportions of papers in which the ResearchGate member is the second, third, middle and last author are smaller for featured publications than among the complete list. The only exception is that for the group of ResearchGate members identified as research leaders (defined in Table 1 and the following paragraph), the proportion of featured publications with the ResearchGate member as last author is higher than that of all the papers inspected. In particular, papers in which the ResearchGate members are the middle and last authors only account for 7.2% and 14.9%, respectively, of featured publications; both proportions are distinctly lower than the corresponding proportions of all inspected publications. Additionally, among the featured publications in which the ResearchGate members are the last authors, about one-third of papers (572 out of 1762) have only two authors, the second of whom may have also played an important role even if he or she is not the corresponding or senior author.

<table><caption>Table 1: Authorship distribution of ResearchGate members among papers of different groups</caption>

<tbody>

<tr>

<th>Author Rank <sup>a</sup></th>

<th>All Papers</th>

<th>Featured publications (FP)</th>

<th>FP of research leaders <sup>b</sup></th>

<th>FP of assist. prof. and lecturer</th>

<th>FP of juniors <sup>c</sup></th>

</tr>

<tr>

<td>First</td>

<td>34473 (36.1%)</td>

<td>7133 (60.3%)</td>

<td>1608 (48.9%)</td>

<td>1884 (66.9%)</td>

<td>1647 (68.4%)</td>

</tr>

<tr>

<td>Second</td>

<td>15371 (16.1%)</td>

<td>1511 (12.8%)</td>

<td>335 (10.2%)</td>

<td>348(12.4%)</td>

<td>358 (14.9%)</td>

</tr>

<tr>

<td>Third</td>

<td>8786 (9.2%)</td>

<td>560 (4.7%)</td>

<td>85 (2.6%)</td>

<td>109 (3.9%)</td>

<td>130 (5.4%)</td>

</tr>

<tr>

<td>Middle</td>

<td>15857 (16.6%)</td>

<td>855 (7.2%)</td>

<td>207 (6.3%)</td>

<td>152 (5.4%)</td>

<td>199 (8.3%)</td>

</tr>

<tr>

<td>Last</td>

<td>20937 (21.9%)</td>

<td>1762 (14.9%)</td>

<td>1054 (32.0%)</td>

<td>324 (11.5%)</td>

<td>73 (3.0%)</td>

</tr>

<tr>

<td colspan="6">

Note:
a. Papers counted in the last author rank row do not include papers by one author. Papers counted in the second and third author rank rows do not include those in which the inspected ResearchGate members are the last author. The middle author rank row refers to authors other than the first, second, third and last author.  
b. Research leaders include professors, associate professors and deans.  
c. Juniors include undergraduate students, graduate students, doctoral, and post-doctoral students.</td>

</tr>

</tbody>

</table>

Table 1 also shows that for nearly 15% of the featured publications with ResearchGate members as last authors, the ResearchGate members are research leaders (including professors, associate professors and deans). Some ResearchGate members were marked only as deans (or department chairmen) without other academic titles. As deans usually are of high academic reputation, we grouped them together with professors and associate professors as research leaders. Nearly one-third of research leaders' featured publications are those in which they are the last authors, indicating senior authorship. This proportion is much higher than that of all publications inspected (21.9%), all featured publications inspected (14.9%), featured publications of assistant professors and lecturers (11.5%) and those of junior researchers (3%). Especially, the much lower value of this proportion for junior researchers demonstrates that junior researchers are less likely to be the last author (which is mainly occupied by senior authors) or they avoided featuring papers in which they did not have an important role.

Table 1 indicates that the importance of the ResearchGate members for some of their featured research is lower than their importance for most of their publications. One cause might be the errors introduced from the treatment of the middle and last authors, as aforementioned. The other cause is most likely, as we will show that ResearchGate members considered other aspects when they selected publications to feature.

</section>

<section>

### Recency tendency of featured research

The cumulative distribution of _s_<sub>rec_f</sub> of all the selected ResearchGate members' featured publications in their own paper queues, ranked in the order of the recency of their papers, is similar to the curve in Figure 1\. The average and median value of _s_<sub>rec_f</sub> is 0.381 and 0.328, respectively. Similarly, we reached the conclusion that ResearchGate members tend to select recently published papers to feature (_p_ < 0.001), which is consistent with the long academic tradition that researchers list their most recent publications first.

</section>

<section>

### Citation tendency of featured research

Figure 2 shows the cumulative distribution of _s_<sub>cit_f</sub> and _s_<sub>citr_f</sub> of all the selected ResearchGate members' featured publications published before 2015, in their own paper queues ranked in descending order of citations and citation rate. The average and median value of _s_<sub>cit_f</sub> is 0.409 and 0.387, respectively. The average and median value of _s_<sub>citr_f</sub> is 0.340 and 0.283, respectively. Thus, we concluded that ResearchGate members tend to select highly cited papers to feature (_p_ < 0.001).

Figure 2 shows that the cumulative distribution of _s_<sub>citr_f</sub> is higher than that of _s_<sub>cit_f</sub>, and the average and median values of _s_<sub>citr_f</sub> are distinctly lower than those of _s_<sub>cit_f</sub>. The figure demonstrates that the selected ResearchGate members considered citation number and recency in their selection of publications to feature, or they valued papers with a high citation rate.

Figure 1 shows the cumulative distribution of _s_<sub>ac_f</sub> of all the selected ResearchGate members' featured publications in their own paper queues ranked in the descending order of the proportion of paper credit assigned to the member. The average and median value of _s_<sub>ac_f</sub> is 0.418 and 0.381\. If there is no preference in the selection of featured publications by ResearchGate members, _s_<sub>ac_f</sub> obeys uniform distribution and its mathematical expectation is 0.5\. Thus, it denies that ResearchGate members did not consider their rank in by-lines when they selected publications to feature, with _p_ < 0.001\. Members tended to select papers in which they were important authors as featured publications.

<figure>

![Figure 2](../p785fig7.png)

<figcaption>

Figure 2: Cumulative distribution of _s_<sub>cit_f</sub> (curve 1) and _s_<sub>citr_f</sub> (curve 2) of all of the selected ResearchGate members' featured publications published before 2015\. _s_<sub>cit_f</sub> and _s_<sub>citr_f</sub> are defined in the text.</figcaption>

</figure>

</section>

<section>

### Source reputation tendency of featured research

The cumulative distribution of _s_<sub>sr_f</sub> of all of the selected ResearchGate members' featured publications in their own paper queues, ranked in descending order of the reputation of the publication source, is similar to the curve in Figure 1\. The average and median value of _s_<sub>sr_f</sub> is 0.386 and 0.348, respectively. Similarly, we concluded that ResearchGate members tend to select papers published by high-reputation sources to feature (_p_ < 0.001)

### Multi-aspect considerations in selection of featured research

Figure 3 shows a scatter plot of _s_<sub>rec_f</sub> (x axis) versus _s_<sub>sr_f</sub> (y axis) for the featured publications inspected. The samples ranged nearly over all possible positions in the space, but were not distributed evenly with a higher density close to the origin. The closer a point is to the co-ordinate origin, the denser are the samples at that point. The scatter plots of other combinations of featured publications rank scores (among _s_<sub>ac_f</sub>, _s_<sub>rec_f</sub>, _s_<sub>citr_f</sub> and _s_<sub>sr_f</sub>) are similar. The distribution of points in the scatter plots demonstrates that the investigated ResearchGate members tend to feature publications in which they are important authors, those that are published recently, have a high impact (reflected in a high citation number) and are published by high-reputation sources. If a featured publication does not meet all the above conditions, it usually meets at least one of the above conditions.

<figure>

![Figure 3](../p785fig8.png)

<figcaption>

Figure 3: Scatter plot of _s_<sub>rec_f</sub> and _s_<sub>sr_f</sub> of the featured publications inspected. _s_<sub>rec_f</sub> and _s_<sub>sr_f</sub> are defined in the text.</figcaption>

</figure>

Some ResearchGate members value certain conditions (such as the number of citations) more than other conditions; thus, they neglect the latter in their selection of publications to feature. To show their academic standing, some ResearchGate members may prefer to feature highly cited publications, which are usually their old work. Conversely, some members may intend to contribute to the scientific community by featuring their publications and further their careers by seeking co-operation and financial support for their research and/or further development that could increase their citation numbers and thus credit their research, in which case they tend to use recent publications.

ResearchGate members can change the order of their listed featured publications. We ranked the top publication as 1 because visitors usually see it first among the featured publications and the bottom publication is ranked last. We performed multivariate linear regression on the rank of featured publications with _s_<sub>rec_f</sub>, _s_<sub>cit_f</sub>, _s_<sub>sr_f</sub> and _s_<sub>ac_f</sub>. We obtained: _R_<sub>FR</sub> = 1.433 x _s_<sub>rec_f</sub> + 0.555 x _s_<sub>cit_f</sub> + 1.241 x _s_<sub>sr_f</sub> + 3.558 x _s_<sub>ac_f</sub>, where _R_<sub>FR</sub> is the rank of each featured publication on its author's list of featured publications. When the multivariate linear regression was performed on the rank of featured publications with _s_<sub>rec_f</sub>, _s_<sub>citr_f</sub>, _s_<sub>sr_f</sub> and _s_<sub>ac_f</sub>, we obtained _R_<sub>FR</sub> = 1.743 x _s_<sub>rec_f</sub> + 0.893 x _s_<sub>citr_f</sub> + 0.837 x _s_<sub>sr_f</sub> + 3.406 x _s_<sub>ac_f</sub>. This demonstrates that ResearchGate members regard their importance to the paper as the most important factor in featuring their publications.

The second important factor is publication recency. Citations (or citation rate) and source reputation are less important than the above two factors. Whether the intention of a ResearchGate member in featuring their publication is to show academic standing or to contribute to the community, he or she needs to choose publications in which he or she played an important role. The coefficient of _s_<sub>rec_f</sub> in the regression equation is larger than that of _s_<sub>cit_f</sub> (or _s_<sub>citr_f</sub>) and _s_<sub>sr_f</sub>, which is reasonable because featuring recent publications can both highlight the author's high-level research and create the potential to contribute to the wider scientific community, whereas featuring publications with high citations or those published in high-reputation sources can only show an author's academic standing.

Finally, ResearchGate members may have considered other factors when choosing publications to feature, which we will discuss in the next subsection.

</section>

<section>

### Other considerations in cases of choosing publications to feature

To find whether publications are valued differently by their authors and the scientific community, we conducted a survey of ResearchGate members with a featured publication satisfying all of the following conditions: the member is an important author, the paper was published before 2010 in a low-reputation source and it has few citations. Additionally, the surveyed member should have more than 20 papers used in this study.

We selected featured publications in which the ResearchGate member had an important role because that implies familiarity with the paper. We selected featured publications published before 2010 because these papers have had enough time to accumulate citations. We surveyed the ResearchGate members with more than 20 papers to avoid surveying members with a narrow choice in their selection of featured publications. Eight hundred sixty-eight featured publications authored by 497 members satisfied the above three conditions.

We chose researchers with featured publications with few citations and low source reputation to survey because these factors indicate that the research has not yet been recognised by the research community. A low number of citations of the selected featured publications shows that other researchers have not used the research presented since publication. Publication by a low-reputation source implies that they may have been underestimated by more reputable sources before their publication. These conclusions are based on general experience and may not apply to all ResearchGate members because authors might not have submitted to high-reputation sources. Five of the 868 featured publications selected in the last paragraph were published in low-reputation source and had few citations.

Finally, we surveyed the five ResearchGate members who featured the above five publications that had few citations and were published in low-reputation source. Our question to them is shown below.

> We are investigating academic publications using www.researchgate.net. We found that you set several publications as featured publications. Compared with your other publications, the following featured publication is not a highly cited paper and it was not published in a journal with a high reputation. This featured article is: _[Featured publication - anonymised]_.  
> Would you please tell us why you set this paper as a featured publication?

Three of the five ResearchGate members responded. Response 1 provides an alternative research evaluation criterion, social impact rather than citations in scientific community.

> The essay appears originally in the New York Times. Not certain what this cite is.

Response 2 shows that, as first reports of a method or concept may not be referenced in later studies on the same topic, the indication of worth based on citation is potentially flawed (Waltman, van Eck and Wouters, 2013).

> Hello! This paper was the first (to my knowledge) of a key new process-full waveform inversion-in borehole seismic work. So, I wanted to establish the technique's lineage.

Response 3 shows the member is confident with the quality of this featured publication. She attributes its low citation to the low popularity (reputation) of the journal in which it appeared. She wants to expand its impact.

> Interesting research. My highly cited articles are known, read and cited already. I try to publicize my less cited articles using this feature. These are lesser-known journals, so the awareness of these articles is low. I am trying to increase the awareness on these in order to improve their impact through reading and citation by the academic audience.

Based on the results of a questionnaire on researchers' perceptions of their papers, [Aksnes and Rip (2009)](#aks09) concluded that for most of the investigated publications, citation counts reflected well the degree of contribution by articles. However, they found that there are some unimportant papers that were highly cited for a variety of reasons, such as the bandwagon effect or "friend" citations within some groups. Likewise, papers that were considered important by their authors had few citations for a variety of reasons, including the low visibility of the publishing journal, others citing follow-up studies of the paper, papers without follow-up studies because they completely solved a problem, relatively narrow topic, or work considered ahead of its time. The method used here provides a way to automatically search lowly cited papers that were valued by at least one author. However, the performance of the method depends on how many ResearchGate members featured their valued but lowly-cited publications.

</section>

<section>

## Limitations

There are two possible sources of error in our investigation. One is that ResearchGate data have potential errors, such as incorrect paper publication times. In addition, there is uneven coverage on ResearchGate among scholars with different ages because younger researchers are more active on social media than older researchers ([Nicholas _et al._, 2015](#nic15); [Nicholas, Clark and Herman, 2016](#nic16)). Therefore, there are fewer "old" publications in ResearchGate. Consequently, only a small number of featured publications were searchable that were valued by their author(s) but otherwise ignored by the scientific community. The other source of error is our method, such as our estimation of the relative importance of ResearchGate members to their papers. However, we believe these errors do not lead to systematic deviation in the evaluation. Therefore, they have little effect on our conclusions.

One limitation of this work is that the proportion of ResearchGate members investigated in this study to all ResearchGate members at the selected universities is low. Many ResearchGate members may possibly have trouble locating the function to edit their featured research. Thus, the Featured research section on the ResearchGate members' home page is not yet fully effective.

Another limitation of this work is that it does not explore whether ResearchGate members feature their most characteristic research, which is a natural assumption to make. Future work should conduct a content analysis comparing ResearchGate members' self-featured publications and his or her other publications, as well as with the featured work and publications of their peers.

The last limitation is that this study does not validate whether featuring publications in ResearchGate increases their citations. Despite the effect of featuring publications on citation counts, this work found featured articles with none or few citations.

</section>

<section>

## Conclusion

Using the collected ResearchGate data, we found that researchers tend to feature their own publications in which they are important authors, or were recently published, highly cited or published in high-reputation sources. We also identified research publications valued by the author, but which have not been recognised by others.

ResearchGate provides opportunities for researchers to highlight their academic level. Therefore, it is unsurprising that researchers prefer to feature publications in which they are important authors. When researchers evaluate their peers' publications, they consider the citation number and the reputation of the publication source. Our findings show that most researchers treat their own achievements and those of others similarly, or they cater to such evaluation criteria in the selection of featured publications. However, this is not always the case. In some cases, quality is not indicated by citations or source reputation. Pioneering work may be neglected in citations.

ResearchGate's Featured research function also facilitates its members to contribute to the greater scientific community. From this perspective, some ResearchGate members prefer to feature recent published papers in which they played an important role to show what they can do for others.

We did not find variations in trends of authorship, recency, citation and source reputation tendency of the featured publications based on the rank of the universities affiliated with the ResearchGate members or the members' academic position (this was not listed for brevity's sake).

We did find academic papers that were valued differently by the authors versus the scientific community. We believe there must be other similar papers because our data did not cover all universities and many ResearchGate members may not be aware of the option to select their own publications to feature. Some ResearchGate members may have featured their self-archived papers that were not published in journals or conferences. The survey results show that featuring lowly cited papers published in less-reputable journals implies the member's insistence that such featured publications are important, although their peers have failed to recognise them.

The ability of our investigation method to find articles that were valued differently by the authors versus the scientific community can be applied to find potentially important published studies that have not been recognised by the research community, such as sleeping beauties or delayed recognition ([Glanzel and Garfield, 2004](#gla04); [van Raan, 2004](#van04)) and resisted discoveries ([Barber, 1961](#bar61); [Fang, 2015](#fan15)). Such studies are usually innovative and important to scientific progress ([Barber, 1961](#bar61)) because they are ahead of their time ([van Raan, 2004](#van04)). The authors of the papers of two such cases valued these papers, despite the lack of recognition or acceptance from others. If a researcher values their low-citation paper, s/he deems that there are some important findings or methods in it and it might be recognised by others someday in the future, and then it will be a future sleeping beauty. The method in this investigation may provide opportunities for finding and identifying such undervalued work. However, this does not mean that all unrecognised papers that are valued by their authors are important, because their authors may insist on wrong ideas. Detailed inspection is needed to determine whether such authors are correct. Seriously and carefully studying others' lowly cited featured publications that were published in low-reputation sources may possibly facilitate promising breakthrough research directions. This method can benefit individual researchers and the whole scientific community. We believe that our method will have higher efficiency if the Featured Research function on ResearchGate is utilised fully. With time elapsing, now young members will provide a wealth of featured publications for this article's approach to identify more unrecognised important papers.

</section>

<section>

## Acknowledgements

This study was supported by Humanities and Social Sciences Foundation of the Ministry of Education of China (16YJE870002); Philosophy and Social Sciences Foundation of Jiangsu High School (2016SJB870005).

## <a id="author"></a>About the authors

**Xuan Zhen Liu** is an associate senior librarian at Library of Nanjing Medical University, No. 140, Hanzhong Road, Nanjing, China. Her research interests include bibliometrics, research evaluation, scientometrics. She can be contacted at: [lxz@njmu.edu.cn](mailto:lxz@njmu.edu.cn).  
**Hui Fang** is an associate professor at School of Electronic Science and Engineering, Nanjing University, No. 163, Xianlin Avenue, Qixia District, Nanjing, China. His research interests include research evaluation, research policy, scientometrics, semantic analysis, Data Mining. He can be contacted at: [fanghui@nju.edu.cn](mailto:fanghui@nju.edu.cn).

</section>

<section>

## References

<ul>
<li id="aks09">Aksnes, D.W. &amp; Rip A. (2009). Researchers' perceptions of citations. <em>Research Policy, 38</em>(6), 895-905.</li>
<li id="bar61">Barber, B. (1961). Resistance by scientists to scientific discovery. <em>Science, 134</em>(347), 596-602.</li>
<li id="cam09">Campanario, J.M. (2009). Rejecting and resisting Nobel class discoveries: accounts by Nobel Laureates. <em>Scientometrics, 81</em>(2), 549-565.</li>
<li id="can91">Cano, V. &amp; Lind, N.C. (1991). Citation life-cycles of 10 citation-classics.  <em>Scientometrics, 22</em>(2), 297-312.</li>
<li id="de81">de Solla Price, D.J. (1981). Multiple authorship. <em>Science, 212</em>(4498), 986-986.</li>
<li id="egg00">Egghe, L., Rousseau, R. &amp; Van Hooydonk, G. (2000). Methods for accrediting publications to authors or countries: consequences for evaluation studies. <em>Journal of the American Society for Information Science, 51</em>(2), 145-157.</li>
<li id="fan15">Fang, H. (2015). An explanation of resisted discoveries based on construal-level theory. <em>Science and Engineering Ethics, 21</em>(1), 41-50.</li>
<li id="gaz16">Gazni, A. &amp; Ghaseminik, Z. (2016). Author practices in citing other authors, institutions, and journals. <em>Journal of the Association for Information Science and Technology, 67</em>(10), 2536-2549.</li>
<li id="gla04">Glanzel, W. &amp; Garfield, E. (2004). The myth of delayed recognition. <em>The Scientist, 18</em>(11), 8-8.</li>
<li id="gon10">Gonzalez-Pereira, B., Guerrero-Bote, V.P. &amp; Moya-Anegon, F. (2010). A new approach to the metric of journals' scientific prestige: the SJR indicator. <em>Journal of Informetrics, 4</em>(3), 379-391.</li>
<li id="ham16">Hammarfelt, B., de Rijcke, S.D. &amp; Rushforth, A.D. (2016). Quantified academic selves: the gamification of research through social networking services. <em>Information Research, 21</em>(2), SM1.</li>
<li id="hau15">Haunschild, R. &amp; Bornmann, L. (2015). Discussion about the new Nature Index. <em>Scientometrics, 102</em>(2), 1829-1830.</li>
<li id="hau14">Haustein, S., Peters, I., Bar-Ilan, J., Priem, J., Shema, H. &amp; Terliesner, J. (2014). Coverage and adoption of altmetrics sources in the bibliometric community.<em>Scientometrics, 101</em>(2), 1145-1163.</li>
<li id="hof16">Hoffmann, C.P., Lutz, C. &amp; Meckel, M. (2016). A relational altmetric? Network centrality on ResearchGate as an indicator of scientific impact. <em>Journal of the Association for Information Science and Technology, 67</em>(4), 765-775.</li>
<li id="ing13">Ingawale, M., Dutta, A., Roy, R. &amp; Seetharaman, P. (2013). Network analysis of user generated content quality in Wikipedia. <em>Online Information Review, 37</em>(4), 602-619.</li>
<li id="jam16">Jamali, H.R., Nicholas, D. &amp; Herman, E. (2016). Scholarly reputation in the digital age and the role of emerging platforms and mechanisms. <em>Research Evaluation, 25</em>(1), 37-49.</li>
<li id="jen17">Jeng, W., DesAutels, S., He, D. &amp; Li, L. (2017). Information exchange on an academic social networking site: a multidiscipline comparison on ResearchGate Q&amp;A. <em>Journal of the Association for Information Science and Technology, 68</em>(3), 638-652.</li>
<li id="jor15">Jordan, K. (2015). <a href="http://www.webcitation.org/6wm5Ij1TS">Exploring the ResearchGate score as an academic metric: reflections and implications for practice</a>. In <em>Proceedings of the Quantifying and Analysing Scholarly Communication on the Web workshop (ASCW'15).</em> Oxford. Retrieved from http://ascw.know-center.tugraz.at/wp-content/uploads/2015/06/ASCW15_jordan_response_kraker-lex.pdf.  (Archived by WebCite&reg; at http://www.webcitation.org/6wm5Ij1TS)</li>
<li id="kra15">Kraker, P. &amp; Lex, E. (2015). <a href="http://www.webcitation.org/6wm5iySse">A critical look at the ResearchGate score as a measure of scientific reputation</a>. In <em>Proceedings of the Quantifying and Analysing Scholarly Communication on the Web workshop (ASCW'15).</em> Oxford. Retrieved from http://ascw.know-center.tugraz.at/wp-content/uploads/2016/02/ASCW15_kraker-lex-a-critical-look-at-the-researchgate-score_v1-1.pdf.  (Archived by WebCite&reg; at http://www.webcitation.org/6wm5iySse)</li>
<li id="lin80">Lindsey, D. (1980). Production and citation measures in the sociology of science: the problem of multiple authorship. <em>Social Studies of Science, 10</em>(2), 145-162.</li>
<li id="liu12">Liu, X.Z. &amp; Fang, H. (2012). Fairly sharing the credit of multi-authored papers and its application in the modification of <em>h</em>-index and <em>g</em>-index. <em>Scientometrics, 91</em>(2), 37-49.</li>
<li id="liu14">Liu, X.Z. &amp; Fang, H. (2014). Scientific group leaders' authorship preferences: an empirical investigation. <em>Scientometrics, 98</em>(2), 909-925.</li>
<li id="mem16">Memon, A.R. (2016). ResearchGate is no longer reliable: leniency towards ghost journals may decrease its impact on the scientific community. <em>Journal of the Pakistan Medical Association, 66</em>(12), 1643-1647.</li>
<li id="nic16">Nicholas, D., Clark, D. &amp; Herman, E. (2016). ResearchGate: Reputation uncovered. <em>Learned Publishing, 29</em>(3), 173-182.</li>
<li id="nic15">Nicholas, D., Jamali, H.R., Watkinson, A., Herman, E., Tenopir, C., Volentine, R., Allard, S. &amp; Levine, K. (2015). Do younger researchers assess trustworthiness differently when deciding what to read and cite and where to publish? <em>International Journal of Knowledge Content Development &amp; Technology, 5</em>(2), 45-63.</li>
<li id="olm16">Olmeda-Gomez, C. &amp; de Moya-Anegon, F. (2016). Publishing trends in library and information sciences across European countries and institutions. <em>Journal of Academic Librarianship, 42</em>(1), 27-37.</li>	
<li id="ord17">Orduna-Malea, E., Mart&iacute;n-Mart&iacute;n, A., Thelwall, M. &amp; L&oacute;pez-C&oacute;zar, E.D. (2017). Do ResearchGate scores create ghost academic reputations? <em>Scientometrics, 112</em>(1): 443-460.</li>
<li id="ort15">Ortega, J.L. (2015). Relationship between altmetric and bibliometric indicators across academic social sites: the case of CSIC's members. <em>Journal of Informetrics, 9</em>(1), 39-49.</li>
<li id="ran11">Ransbotham, S. &amp; Kane, G.C. (2011). Membership turnover and collaboration success in online communities: explaining rises and falls from grace in wikipedia. <em>MIS Quarterly, 35</em>(3), 613-627.</li>
<li id="rie90">Riesenberg, D. &amp; Lundberg, G.D. (1990). The order of authorship: who's on the first. <em>The Journal of the American Medical Association, 264</em>(14), 1857-1857.</li>
<li id="sah16">Sahoo, S. (2016). Analyzing research performance: proposition of a new complementary index. <em>Scientometrics, 108</em>(2), 489-504.</li>
<li id="sha95">Shadish, W.R., Tolliver, D., Gray, M. &amp; Gupta, S.K.S. (1995). Author judgements about works they cite - 3 studies from psychology journals. <em>Social Studies of Science, 25</em>(3), 477-498.</li>
<li id="sta13">Stallings, J., Vance, E., Yang, J., Vannier, M.W., Liang, J., Pang, L., Dai, L., Ye, I. &amp; Wang, G. (2013). Determining scientific impact using a collaboration index. <em>PNAS, 110</em>(24), 9680-9685.</li>
<li id="the14">Thelwall, M. &amp; Kousha, K. (2014). ResearchGate articles: age, discipline, audience size, and impact. <em>Journal of the Association for Information Science and Technology, 68</em>(2), 468-479.</li>
<li id="the15a">Thelwall, M. &amp; Kousha, K. (2015a). ResearchGate: disseminating, communicating, and measuring scholarship? <em>Journal of the Association for Information Science and Technology, 66</em>(5), 876-889.</li>
<li id="the15b">Thelwall, M. &amp; Kousha, K. (2015b). Web indicators for research evaluation. Part 2: social media metrics. <em>Profesional de la Informacion, 24</em>(5), 607-620.</li>
<li id="the17">Thelwall, M. &amp; Kousha, K. (2017). ResearchGate articles: Age, discipline, audience size, and impact. <em>Journal of the Association for Information Science and Technology, 68</em>(2), 468-479.</li>
<li id="tru04">Trueba, F.J. &amp; Guerrero, H. (2004). A robust formula to credit authors for their publications. <em>Scientometrics, 60</em>(2), 181-204.</li>
<li id="van04">van Raan, A. F. J. (2004). Sleeping beauties in science. <em>Scientometrics, 59</em>(3), 467-472.</li>
<li id="wal12">Waltman, L. (2012). An empirical analysis of the use of alphabetical authorship in scientific publishing. <em>Journal of Informetrics, 6</em>(4), 700-711.</li>
<li id="wal13">Waltman, L., van Eck, N.J. &amp; Wouters P. (2013). Counting publications and citations: is more always better? <em>Journal of Informetrics, 7</em>(3), 635-641.</li>
<li id="wan12">Wang, M.Y., Yu, G., Xu, J.Z., He, H.X., Yu, D.R. &amp; An, S. (2012). Development a case-based classifier for predicting highly cited papers. <em>Journal of Informetrics, 6</em>(4), 586-599.</li>
<li id="wan15">Wang, X.W., Liu, C. &amp; Mao, W.L. (2015). Does a paper being featured on the cover of a journal guarantee more attention and greater impact? <em>Scientometrics, 102</em>(2), 1815-1821.</li>
<li id="zha11">Zhao, D. &amp; Strotmann A.  (2011). Counting first, last, or all Authors in citation analysis: a comprehensive comparison in the highly collaborative stem cell research field. <em>Journal of the American Society for Information Science and Technology, 62</em>(4), 654-676.</li>
</ul>

</section>

<section>

## Appendix

<table><caption>Table A1: The universities whose ResearchGate members are used in this study</caption>

<tbody>

<tr>

<th>Schemed rank <sup>a</sup></th>

<th>Rank <sup>a</sup></th>

<th>

_n_<sub>tie</sub> <sup>a</sup></th>

<th>

University (_n_<sub>rm_f</sub>) <sup>b</sup></th>

</tr>

<tr>

<td>1</td>

<td>1</td>

<td>1</td>

<td>Princeton University (52)</td>

</tr>

<tr>

<td>6</td>

<td>5</td>

<td>2</td>

<td>Columbia University (126)</td>

</tr>

<tr>

<td>11</td>

<td>11</td>

<td>1</td>

<td>Dartmouth College (34)</td>

</tr>

<tr>

<td>16</td>

<td>15</td>

<td>4</td>

<td>Cornell University (102)</td>

</tr>

<tr>

<td>21</td>

<td>20</td>

<td>3</td>

<td>University of California, Berkeley (98)</td>

</tr>

<tr>

<td>26</td>

<td>24</td>

<td>3</td>

<td>University of California, Los Angeles (146)</td>

</tr>

<tr>

<td>31</td>

<td>31</td>

<td>1</td>

<td>Boston College, USA (19)</td>

</tr>

<tr>

<td>36</td>

<td>36</td>

<td>1</td>

<td>New York University (66)</td>

</tr>

<tr>

<td>41</td>

<td>39</td>

<td>5</td>

<td>Boston University (72)</td>

</tr>

<tr>

<td>46</td>

<td>44</td>

<td>6</td>

<td>University of Wisconsin-Madison (127)</td>

</tr>

<tr>

<td>51</td>

<td>50</td>

<td>4</td>

<td>University of Florida (140)</td>

</tr>

<tr>

<td>56</td>

<td>56</td>

<td>4</td>

<td>University of Texas at Austin (117)</td>

</tr>

<tr>

<td>61</td>

<td>60</td>

<td>6</td>

<td>Purdue University (95)</td>

</tr>

<tr>

<td>66</td>

<td>66</td>

<td>2</td>

<td>Clemson University (41)</td>

</tr>

<tr>

<td>71</td>

<td>71</td>

<td>3</td>

<td>University of Minnesota Twin Cities (99)</td>

</tr>

<tr>

<td>76</td>

<td>74</td>

<td>5</td>

<td>Texas A&M University (101)</td>

</tr>

<tr>

<td>81</td>

<td>79</td>

<td>3</td>

<td>University of Delaware (42)</td>

</tr>

<tr>

<td>86</td>

<td>86</td>

<td>6</td>

<td>Binghamton University (27)</td>

</tr>

<tr>

<td>91</td>

<td>86</td>

<td>6</td>

<td>Indiana University Bloomington (50)</td>

</tr>

<tr>

<td>96</td>

<td>96</td>

<td>3</td>

<td>Stony Brook University (47)</td>

</tr>

<tr>

<td>101</td>

<td>99</td>

<td>4</td>

<td>Auburn University (32)</td>

</tr>

<tr>

<td>106</td>

<td>103</td>

<td>4</td>

<td>University of Tennessee (47)</td>

</tr>

<tr>

<td>111</td>

<td>111</td>

<td>7</td>

<td>University of Missouri (53)</td>

</tr>

<tr>

<td>116</td>

<td>111</td>

<td>7</td>

<td>University of Utah (81)</td>

</tr>

<tr>

<td>121</td>

<td>118</td>

<td>6</td>

<td>University of Kansas (58)</td>

</tr>

<tr>

<td>126</td>

<td>124</td>

<td>5</td>

<td>The University of Arizona (81)</td>

</tr>

<tr>

<td>131</td>

<td>129</td>

<td>4</td>

<td>Colorado State University (50)</td>

</tr>

<tr>

<td>136</td>

<td>135</td>

<td>8</td>

<td>Louisiana State University (41)</td>

</tr>

<tr>

<td>141</td>

<td>135</td>

<td>8</td>

<td>University of Cincinnati (50)</td>

</tr>

<tr>

<td>146</td>

<td>146</td>

<td>6</td>

<td>Ohio University (26)</td>

</tr>

<tr>

<td>151</td>

<td>146</td>

<td>6</td>

<td>University of Texas at Dallas (20)</td>

</tr>

<tr>

<td>156</td>

<td>152</td>

<td>7</td>

<td>University of Illinois at Chicago (73)</td>

</tr>

<tr>

<td>161</td>

<td>159</td>

<td>5</td>

<td>University of South Florida (61)</td>

</tr>

<tr>

<td>166</td>

<td>164</td>

<td>5</td>

<td>Virginia Commonwealth University (51)</td>

</tr>

<tr>

<td>171</td>

<td>171</td>

<td>5</td>

<td>University of Louisville (35)</td>

</tr>

<tr>

<td>176</td>

<td>176</td>

<td>7</td>

<td>University of Central Florida (44)</td>

</tr>

<tr>

<td>181</td>

<td>176</td>

<td>7</td>

<td>University of New Mexico (40)</td>

</tr>

<tr>

<td>186</td>

<td>183</td>

<td>5</td>

<td>West Virginia University (32)</td>

</tr>

<tr>

<td>191</td>

<td>188</td>

<td>6</td>

<td>Kent State University (22)</td>

</tr>

<tr>

<td>196</td>

<td>194</td>

<td>3</td>

<td>University of Houston (43)</td>

</tr>

<tr>

<td>201</td>

<td>197</td>

<td>5</td>

<td>University of Colorado (60)</td>

</tr>

<tr>

<td>206</td>

<td>202</td>

<td>8</td>

<td>University of North Carolina at Charlotte (17)</td>

</tr>

<tr>

<td>211</td>

<td>210</td>

<td>4</td>

<td>Old Dominion University (21)</td>

</tr>

<tr>

<td>216</td>

<td>214</td>

<td>6</td>

<td>Nova Southeastern University (9)</td>

</tr>

<tr>

<td>221</td>

<td>220</td>

<td>12</td>

<td>New Mexico State University (15)</td>

</tr>

<tr>

<td>226</td>

<td>220</td>

<td>12</td>

<td>University of Massachusetts Boston (14)</td>

</tr>

<tr>

<td>231</td>

<td>220</td>

<td>12</td>

<td>Utah State University (31)</td>

</tr>

<tr>

<td colspan="4">

Note: a) The rank of the universities is provided by US New & World Report. _n_<sub>tie</sub> is the number of universities which tie for the rank. Schemed rank is the rank of the university we intended to select in this study, and it is included in the range of the tied rank of the universities in the same row. b) For each university, _n_<sub>rm_f</sub> is the number of members whose featured research and publications were counted in this study.</td>

</tr>

</tbody>

</table>

</section>

</article>