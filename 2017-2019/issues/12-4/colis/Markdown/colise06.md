#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Educational Forum paper

# A new direction for library and information science: the communication aspect of information literacy

#### [Tibor Koltay](mailto:koltay.tibor@abk.szie.hu)  
Department of Library and Information Science, 
Berzsenyi Dániel College, Szombathely; 
and Department of Information and Library Studies, 
Szent István University Jászberényi College 
H-5100 Jászberényi, Rákóczi út 53., 
Hungary

#### Abstract

> **Introduction.** Information literacy gained an important place in library and information science education. information literacy however often includes (predominantly oral) communication. This is important for library and information science education and has to be studied.  
> **Argument.** After defining information literacy, we examine why and where it is needed, explain the role of libraries and librarians, discuss the relationship between e-literacy and information literacy and how it could be integrated into library and information science curricula. Different models of literacy as well as curricula point towards the close ties between information literacy and communication. Information seeking, a central issue to information literacy, can be put into the context of writing. Some understandings of literacy involve different types of communication that is not uncommon in library and information science education. Stressing communication relates information literacy to literacy and functional literacy in their traditional sense. Issues of text production, relevance, critical reading and academic literacy offer interesting agendas.  
> **Conclusions.** information literacy or at least its communicative features should be made a subject of psycholinguistics and cognitive science as information literacy stresses the importance of gaining control over how we interact with information and it consists of processes that are widely unknown thus require cognitive enquiry. It would be however premature to draw consequences of a cognitive enquiry not yet carried out.

## Introduction

The work of libraries and librarians of the 21st century and consequently Library and Information Science (library and information science) education is increasingly characterised by a necessity to include information literacy (information literacy) into the curricula. information literacy thus undoubtedly gained an important place also in library and information science education. information literacy however often goes beyond the most basic and most important skills of recognizing a need for information, finding and critically evaluating information.

Coupling information literacy and communication in library and information science education has strong potential and possibilities that have to be studied. The purpose of this paper is to examine this potential.

We begin by presenting some of the definitions of information literacy. As part of our argument we will reiterate that information literacy is of interdisciplinary nature characterized by the presence of ties to linguistics and will define what types of communication can be related to information literacy.

We will also discuss the deficiencies of approaches that narrow down information literacy to some kind of electronic literacy and discuss the necessity of integrating information literacy into library and information science curricula. We will also examine the literature related to information literacy and writing (composition) instruction in the higher education of the United States and will shortly examine some aspects of academic literacy, the ability to read, interpret, and produce information valued in academia. The paper concludes by stating a need of a cognitive enquiry that would throw light on the nature of processes underlying information literacy.

### Definitions of information literacy

What is information literacy? How does its notion and application influence library and information science education?

Information literacy is used both as an umbrella term that encompasses computer, internet (or network), digital and media literacies as well as one of these. There seems to be a consensus forming that the former is the more correct approach to this question.

A well-known definition of information literacy is the one developed by the American Library Association ([ALA 1989, 1](#ala)) "Information literate [people] are not only able to recognize when information is needed, but they are also able to identify, locate, evaluate, and use effectively information needed for the particular decision or issue at hand. The information literate person, therefore, is empowered for effective decision making, freedom of choice, and full participation in a democratic society."

Varis ([2003](#varis)) directs our attention to the fact that there are many more definitions of information literacy, nonetheless they all contain the following personal competencies:

*   <a>Recognition of an information need;</a>
*   <a>Development of a search strategy and selection of sources;</a>
*   <a>Evaluation, synthesis;</a>
*   <a>Effective use of the new information.</a>

<a>The latter one directs our attention to the communicative aspects of information literacy.</a>

<a>Information literacy is often mentioned together with the concept of critical thinking skills, as well as with problem-based and lifelong learning (</a>[Roes 2001](#roes))

### The linguistic nature of information literacy

As Hjorland ([2000](#hor)) points out library and information science is a professional domain that draws on many kinds of knowledge, among others language and communication skills. And, indeed, the interdisciplinary nature of library and information science is well known ([Saracevic 1999](#sara)). If we agree that information literacy is closely tied to library and information science it inherits its interdisciplinary connections. Among these there are psychology, linguistics and psycholinguistics ( [Webber and Johnston 2000](#webber)).

Limberg and Sundin ([2006](#limb)) argue that information seeking research and information literacy education do not influence each other in the way that they have potential to do. This means that information literacy education would benefit from using concepts and models from information seeking research. We argue in a similar vein, but our argument is related to verbal communication. Just like Attfield, Blandford & Dowell ([2003](#attfield)) who argue that information seeking can be put into the context of writing as writing is amongst the most common tasks within which information seeking is embedded.

### Different understandings of communication

information literacy is characterized by the presence of communication in different understandings. Dealing with communication is not uncommon in library and information science education. Most Hungarian library and information science curricula, for example, contain some class of the basics of communication often addressing both the theory of communication and practical communication skills. This is done not haphazardly. One of the a broader educational contexts of library and information science is given by communication studies ([Hjorland 2000](#hor)).

We also know that many processes of library and information activities, especially reference and user education are based in many regards on communication and require communication skills.

The definitions of information literacy often show that information literacy takes shape in a process of communication. Limberg and Sundin ([2006](#limb)) speak about an approach to information literacy education and to information literacy itself that emphasizes the social and communicative aspects of information seeking practices.

We however want to stress an understanding of communication in relationship with information literacy in a simple and straightforward way. We state namely that information literacy and (predominantly) verbal communication are closely connected in many regards as information literacy includes the abilities and the activities of reading and writing.

## The argument

Before arguing for the necessity to couple information literacy and communication we have to examine why and where is it needed. It seems also to be useful to explain the role of libraries and librarians in information literacy.

### The need for information literacy

Information literacy has its roots in library user education. Nowadays it has significance within education in a wider sense. When exercising information literacy we handle complex situations and follow patterns of behaviour. This is the feature of information literacy that makes it applicable in the educational context.

The rapid development of digital technologies resulted in a proliferation of information sources and caused information overload. This development serves as the basis for the information society and the knowledge economy that require competent information customers and informed workforce. Information literacy developed to meet these challenges and requirements ( [Andretta 2005](#andretta)). This is the point where and this is the reason why library user education became information literacy, and this is why information literacy acquired especial importance recently even if the existence of its notion can be traced back to the 1970s ([Roes 2001](#roes)).

Explaining the general importance of information literacy Virkus ([2003](#virkus)) cites the following observation by the OECD “The knowledge-based economy is characterized by the need for continuous learning of both codified information and the competencies to use this information. As access to information becomes easier and less expensive, the skills and competencies relating to the selection and efficient use of information become more crucial... Capabilities for selecting relevant and discarding irrelevant information, recognizing patterns in information, interpreting and decoding information as well as learning new and forgetting old skills are in increasing demand” ( [OECD 1996, 13](#oecd)).

Harmonizing with this an expert meeting of the UNESCO declared that:

*   <a>Information literacy is a concern to all sectors of society and should be tailored by each to meet its specific context;</a>
*   <a>Information literacy enables people to access information about their health, their environment, their education and work;</a>
*   <a>There is a need to embed information literacy in the curriculum at primary, secondary and tertiary levels, as well as in the training for information professionals, including education and health care workers (</a> [Thematic Debate 2005](#them)).

### The role of the library and of librarians

Librarians are educated in collecting, evaluating, organizing and providing access to information thus they possess appropriate knowledge and skills. This means that librarians are potentially equipped with all the abilities and skills necessary not only to be information literate but to teach information literacy to other persons. Without getting a proper education themselves however they will not be able to fulfil this role.

In other words, to be able to act as providers of information literacy for the community, librarians first have to acquire competencies themselves and to have an opportunity to upgrade their skills constantly ( [Virkus 2003](#virkus)).

As Jones ([1996](#jones)) points out, “the knowledge domain of the librarian is the acquisition and evaluation of information resources. The librarian usually does nothing more with this knowledge base beyond offer it succinctly and freely to those who request it. This evaluation of resources across subject domains, selecting the valid and useful, is common practice of librarians. Free from the coaching presence of the librarian, the online information seeker must exercise these skills independently.”

### E-literacy only?

The review on the concepts of information and digital literacies by Bawden ([2001](#bawden)) also shows that information literacy and e-literacy (digital literacy, information technology literacy, computer literacy) are two interrelated but different concepts. The former is concerned with content and communication, the latter with an understanding of the technological infrastructure.

Bundy ([2004b](#bundyb)) wants us to be aware of myths about information literacy.

The first myth is that information literacy is about using information and communication technologies effectively. There seems to be no evidence that the huge investments into information and communication technologies would have brought improved student performance despite thefact that e-literacy has become a prerequisite for information literacy.

In his another writing it is also Bundy ([2004a](#bundya)) who says that the sheer abundance of information and technology will not in itself create more informed citizens without a complementary understanding and capacity to use information effectively. What is really needed – he says – is technology mediated but not technology focused pedagogy.

A second myth is that students entering higher education are information literate because student centred, resource based and ICT focused learning are pervasive in secondary education. Despite the rhetoric about educational change this is not true. We have to add that this is true not only in Australia but basically everywhere.

A third myth about information literacy development is that it can be addressed largely by continuing traditional library user education that again seems to underestimate the need for change. A fourth myth is that information literacy is just about information, rather than about learning how to learn ( [Bundy 2004b](#bundyb)).

Our attempt to widen the horizon of library and information science education towards information literacy and communication addresses only a small segment of these complex issues.

### Integrating information literacy into library and information science curricula

The basic requirements for integrating information literacy into library and information science education have been fixed in 2005 by Virkus _et al._ ([2005](#virkuse))

library and information science students are required:

*   <a>To be aware of information literacy as a concept;</a>
*   <a>To become information literate themselves;</a>
*   <a>To learn about some key aspects of teaching information literacy.</a>

Key aspects include:

*   <a>Key definitions and models of information literacy;</a>
*   <a>Different contexts for information literacy;</a>
*   <a>The relationship of information literacy with other literacies;</a>
*   <a>The relationship between information literacy and other library and information science skill and knowledge areas (e.g. Knowledge Management, Information Retrieval);</a>
*   <a>Key models and theories;</a>
*   <a>The functions and scope of key information literacy organisations and initiatives;</a>
*   <a>The history and origins of information literacy (</a>[Virkus _et al._2005](#virkuse)).

Integration of information literacy into whatever curricula requires either creating new courses for this purpose or integration information literacy to already existing courses. The latter seems to be the most appropriate approach.

In the case of library and information science it is difficult to associate information literacy with any particular course of curricula. information literacy thus could be “built in” in some courses.

There should be at least a minimal framework of information literacy in library and information science education that could contain the following elements:

*   <a>Literacy, functional literacy (in the original sense),</a>
*   <a>The different literacies (Internet, digital etc.);</a>
*   <a>Literacy as an umbrella term;</a>
*   <a>The definition of information literacy (e.g. the ALA definition);</a>
*   <a>The importance of information literacy for information professionals in context of the changing role of library and lifelong learning.</a>

This minimal framework has been put into practice in the library and information science programme at Szent István University, Hungary. information literacy figures in the course “Information systems”.

### Information literacy and verbal communication

The before mentioned review of Bawden ([2001](#bawden)) shows that some understandings of literacy involve the integration of listening, speaking, reading, writing, and numeracy. information literacy also may include a cultural knowledge which enables us to recognize and use language appropriate to different social situations: among others thinking and questioning.

_Greater Expectations_, a document of the Association of American Colleges and Universities goes somewhat furter. It envisions “empowered” and informed learners who are able to effectively communicate orally, visually, in writing, and in a second language ([AACU 2002](#aacu)).

Coupling information literacy with communication relates information literacy to literacy and functional literacy in their traditional sense. As – among others – Varis ([2003](#varis)) points out, literacy itself has many definitions. It relates to an individual’s ability to understand printed text and to communicate through print. Functional literacy is not different in this regard. Still, most commonly it denotes the ability to read and use information essential for everyday life ([Bawden 2001](#bawden)).

Shapiro and Hughes ([1996](#shap)) speak about seven dimensions of literacy. One of these is publishing literacy that encompasses the ability to format and publish research and ideas electronically, in textual and multimedia forms. They put information literacy in a broad context by comparing it to the trivium of basic liberal arts (grammar, logic and rhetoric) was to the educated person in medieval society.

This approach is the basis of the big six model. Beside of activities that can be attributed to any planned activity, the model contains a number of elements that pertain to verbal communication, that are the following:

*   <a>Engage with information (read, view, listen) [Step 4, Information use];</a>
*   <a>Extract information [Step 4, Information use]</a>;
*   <a>Present information [Step 5, Synthesis];</a>
*   <a>Judge the product (effectiveness) [Step 6, Evaluation];</a>
*   <a>Judge the process (efficiency) [Step 6, Evaluation] (</a> [Eisenberg 2007](#eis)).

Evaluation through judging both the product and the process is useful for many processes however it is especially characteristic for writing activities.

Loo and Chung ([2006](#loo)) add to this that the big six theory does not include every aspect of an information literacy course. Students often do not have the proper writing and citing skills, they are not able to delineate their and others’ contributions. The latter is related to plagiarism that is often dealt with in the literature of writing instruction, though it is not exclusively a communication issue, but an ethical one. This nonetheless shows the complexity of information literacy education and interrelatedness with different problems of varied origin. The agenda proposed by Loo and Chung ([2006](#loo)) contains new elements. They dedicate a separate heading to the communication and presentation of information that includes the following issues:

*   <a>selection of the communication medium for the intended purpose and audience;</a>
*   <a>integration of new and prior information/knowledge;</a>
*   <a>citation styles and formal referenced report;</a>
*   <a>delineation of one’s own and others’ contributions;</a>
*   <a>oral presentation.</a>

Under “Evaluation” they include extraction and summarization of information that are of especial interest as they are the main components of classification, subject indexing and abstracting and are of undoubtedly linguistic nature.(About some of the processes see, e.g. [Koltay 1997](#kol).)

There is a relatively big number of papers examining different issues related to information literacy education in connection with writing (composition) instruction in the higher education of the United States and especially in regard to year 1 students ([Jabro and Corinth 2005](#jabro), [Samson and Granath 2004](#samson), [Sult and Mills 2006](#sult)).

The experiences of the US higher education (HE) however could be useful beyond the Anglo-Saxon educational system especially as the HE in the European Union experiences the Bologna reform designed to change it.

As an example of synthesis of information literacy and writing or rather an organic inclusion of information literacy into a writing programme can be seen the Multimedia Writing and Technical Communication course at Arizona State University East, reported by D’Angelo and Maid ([2004](#angelo)). This programme builds on rhetorical knowledge of which we should mention the understanding the role of a variety of technologies in communicating information and the ability of using appropriate technologies to communicate information. Under the heading “Critical Thinking, Reading, and Writing” the following points are especially noteworthy:

*   <a>Using information, writing, and reading for learning, thinking, and communicating;</a>
*   <a>Understanding that research, like writing, is a series of tasks, including accessing, retrieving, evaluating, analyzing, and synthesizing appropriate information from varied sources;</a>
*   <a>Understanding the relationships among language, knowledge, and power;</a>
*   <a>Recognizing, understanding, and analyzing the context within which language, information, and knowledge are communicated and presented;</a>

Some of the above aspects are put as processes. Such points are, for example, the following ones:

*   <a>Using appropriate technologies to manage information collected or generated for future use.</a>
*   <a>Learning to critique our own and others’ works;</a>
*   <a>Learning to balance the advantages of relying on others with the responsibility of doing our part;</a>

The last two processes are related to critical thinking and plagiarism.

Jones ([1996](#jones)) enumerates a number of criteria that pertain to critical thinking. Let us see the most important ones.

*   <a>Differentiating between fact and opinion;</a>
*   <a>Examining underlying assumptions, including our own ones;</a>
*   <a>Looking for explanations, causes and solutions;</a>
*   <a>Being aware of fallacious arguments, ambiguity, and manipulative reasoning;</a>
*   <a>Focusing on the whole picture, while examining the specifics;</a>
*   <a>Looking for reputable sources.</a>

The remark on critiquing our own and others’ works reminds us of the following words of Adams ([2003](#adams)) cited by Bundy ([2004a](#bundya)) about information literate citizens, who are able to “Read more widely, see more clearly, think more clearly, challenge authorities on every occasion, more importantly challenge themselves.” We have to be aware of the fact however that the above competencies are part of a degree programme geared towards professional specification thus they go outside any general framework of information literacy.

The knowledge of conventions is also of importance as it relates again to the use of technology as well as to stressing a “traditional” writing issue, genre knowledge.

All the above competencies show the complexity of writing and the numerous ties it has with information literacy. The use of technology, information management and other processes show one of the closest ties. As to writing itself the knowledge of conventions is not only important in itself, but as a wider context that has to be (or could be) acquired in the framework of information literacy education. Genre knowledge builds on the basic rhetorical issues like the ones of purpose, audience and tone ([Ahmad and McMahon 2006](#ahmad)).

Attfield, Blandford & Dowell ([2003](#attfield)) direct our attention to the interplay between text production and reflection on the text produced. The writer is devoted to the task of turning ideas into text. They then read the text to form new ideas and to make decisions about what has to be written and how should it be organised. Reflecting to texts includes information seeking as an analytic activity.

One of the most important issues of information seeking and also of information literacy is relevance that appears in linguistic contexts, as well. One of the important assumptions of the relevance theory elaborated by _Sperber and Wilson_ is that every utterance has a variety of possible interpretations, all compatible with the information that is linguistically encoded ( [Wilson 1994](#wilson)). This assumption seems to be appropriate for information literacy where finding the appropriate interpretation occurs in wider contexts.

Analysis and synthesis are central to many activities covered by library and information science education. Classification, subject indexing and abstracting – already mentioned above – are such activities. The results of classification materialises in the form of some code, while indexing in the form of words and phrases, abstracting means producing texts, thus it is a writing activity. An important element of the classification, indexing and abstracting process is reading, obviously closely connected to literacy and communication.

One of the “meeting points” of information literacy and communication is critical reading, closely related to critical thinking. As Knott ([2005](#knott)) explains, many of our writings involve reflection on written texts as the thinking and research on the given subject has already been done.

Critical reading includes (among others) the following:

*   <a>Determining the purpose of the text and assessing how the central claims are developed;</a>
*   <a>Making judgements about the intended audience of the text;</a>
*   <a>Distinguishing the different kinds of reasoning in the text;</a>
*   <a>Examining the evidence and sources of the writing.</a>

### Academic literacy

If literacy is the ability to read, interpret, and produce texts, academic literacy is the ability to read, interpret, and produce information valued in academia according to beliefs about how research should be done. Academic literacy thus involves the comprehension of the entire system of thinking, values and information flows of academia. All this is based on a cultural identity of academia in which professional language and literature play a key role. In this system information has a grammatical dimension that information literate academics must master and students must be taught ([Elmborg 2006](#elmborg)). One of the principal issues of academic literacy, genre knowledge has been already addressed in the previous chapters.

### A subject to cognitive inquiry

As Ward ([2006](#ward)) underlines, a robust and holistic conceptualization of information literacy must recognize the interplay between an external, verifiable reality and an internal, more subjective reality.

This is because to work with information successfully requires a special mentality and skill set to handle situations, tasks, problems-at-hand ([Pinto 2003](#pinto)). In other words, the effective use of information also should include a “critical awareness of the factors surrounding knowledge production” ([Mutch 1997, 386](#mu)).

In addition to this information seeking behaviour, an important constituent of information literacy incorporates complex thought processes ([Rader 2002](#rader)).

These are the reasons why information literacy should be made a subject of psycholinguistics and cognitive science as it stresses the importance of gaining control over how we interact with information. With this information literacy sensitizes us to the need of developing a metacognitive approach to learning.

Metacognition plays an especially important role in reading. It is important for native and non-native readers to be aware of the significant strategies that reading requires. These strategies have to be integrated into curricula. The reader’s metacognitive knowledge about reading includes an awareness of a variety of reading strategies and reading is influenced by the awareness of reading strategies .

As techniques and skills related to information literacy strengthen, so too do the individual’s metacognitive processes. Beside the importance of metacognition and metacognitive processes in it, information literacy consists of processes that are widely unknown thus require cognitive enquiry.

## Conclusions

The definitions of information literacy itself suggest that it is concerned more with the content than with technologies. There are many pieces of evidence, a number of approaches by different researchers that information literacy involves at least reading and writing.

Information seeking, an important part of information literacy can be put into the context of writing and related to communication competencies. Critical reading is also an important intersection of information literacy and communication as it is academic literacy.

Information literacy must recognise the existence of internal realities, complex thought processes should thus be made a subject of psycholinguistics and cognitive science. It would be however premature to draw information literacy consequences of a cognitive enquiry not yet carried out.

We can nonetheless say that a programme of information literacy that integrates aspects of communication should be integrated into the curriculum as the Writing Across the Curriculum movement that strived for a high level involvement of the librarian into writing curricula (see e.g. [Sheridan 1992](#sher)).

## References

*   <a id="aacu"></a>AACU (2002). _Greater expectations: A new vision for learning as a nation goes to college._ Washington, DC: Association of American Colleges and Universities.
*   <a id="adams"></a>Adams, P. (2003). The misinformation age, _The Australian_ 19-20 April 2003. Cited by Bundy ([2004a](#bundya))
*   <a id="ahmad"></a>Ahmad, R. & McMahon, K. (2006). The benefits of good writing. _Writing matters. The Royal Literary Fund Report on Student Writing in Higher Education._ London: Royal Literary Fund. 1–6.
*   <a id="ala"></a>ALA (1989). _American Library Association Presidential Commission on Information Literacy. Final report_. Chicago, Ill.: American Library Association.
*   <a id="andretta" "=""></a>Andretta, S. (2005). _Information literacy: A practitioner’s guide._ Oxford, etc.: Chandos Publishing.
*   <a id="attfield"></a>Attfield, S., Blandford, A. & Dowell, J. (2003). Information seeking in the context of writing: a design psychology interpretation of the 'problematic situation’. _Journal of Documentation,_ **59**(4), 430–453.
*   <a id="bawden"></a>Bawden, D. (2001). Information and digital literacies: a review of concepts. _Journal of Documentation,_ **45**(2), 218–259.
*   <a id="bundya"></a>Bundy, A. (2004)a. [One essential direction: information literacy, information technology fluency](http://www.jelit.org/6/). _Journal of eLiteracy_, **1**(1), Retrieved 1 March 2006 from http://www.jelit.org/6/
*   <a id="bundyb"></a>Bundy, A. (2004b). [_Zeitgeist: information literacy and educational change_.](http://www.library.unisa.edu.au/about/papers/zeitgeist-info-lit.pdf) Paper presented at the 4th Frankfurt Scientific Symposium. 4 October 2004 Information Seeking in Context and the development of information systems. Retrieved 7 February, 2006 from http://www.library.unisa.edu.au/about/papers/zeitgeist-info-lit.pdf
*   <a id="angelo"></a>D’Angelo, B.J. & Maid, B.M. (2004). Moving Beyond Definitions: Implementing Information Literacy Across the Curriculum. _Journal of Academic Librarianship,_ **30**(3), 212–217.
*   <a id="eis"></a>Eisenberg, M. (2007). [_A Big6 Skills Overview._,](http://www.big6.com/showarticle.php?id=16) Retrieved 8 June 2007 from http://www.big6.com/showarticle.php?id=16
*   <a id="elmborg"></a>Elmborg, J. (2006). Critical Information Literacy: Implications for Instructional Practice. _Journal of Academic Librarianship,_ **32**(2), 192–199.
*   <a id="hor"></a>Hjorland, B. (200). Library and information science: practice, theory, and philosophical basis. _Information Processing and Management,_ **36**(3), 501–531.
*   <a id="jabro"></a>Jabro, A. & Corinth, J. (2005). [Crisis in Information Literacy.](http://www.educause.edu/pub/er/reviewArticles/31131.html) _Academic Exchange Quarterly_, **9**(2). Retrieved 6 February 2004 from http://www.educause.edu/pub/er/reviewArticles/31131.html
*   <a id="jones"></a>Jones, D. (1996). [Critical Thinking in an Online World. Untangling the Web](http://www.library.ucsb.edu/untangle/jones.html). _Proceedings of the Conference Sponsored by the Librarians Association of the University of California, Santa Barbara and Friends of the UCSB Library. April 26, 1996\._ University Center, University of California, Santa Barbara. Retrieved 19 December 2004 from http://www.library.ucsb.edu/untangle/jones.html
*   <a id="knott"></a>Knott, D. (2005). [_Critical Reading Towards Critical Writing_](http://www.utoronto.ca/writing/critrdg.html)_._ Retrieved 23 February 2005 from http://www.utoronto.ca/writing/critrdg.html
*   <a id="kol"></a>Koltay, T. (1997). A wider horizon to information handling: teaching abstracting to students of translation. _Education for Information_.**15**(1) 35–42\.
*   <a id="limb"></a>Limberg, L. & Sundin, O. (2006). [Teaching information seeking: relating information literacy education to theories of information behaviour](http://informationr.net/ir/12-1/paper280.html). _Information Research,_ **12**(1), paper 280. Retrieved 8 May 2007 from http://InformationR.net/ir/12-1/paper280.html
*   <a id="loo"></a>Loo, A. & Chung, C.W. (2006). A model for information literacy course development: a liberal arts university perspective. _Library Review_, **55**(4), 249–258\.
*   <a id="mu"></a>Mutch, A. (1997). Information Literacy: An Exploration _International Journal of lnformation Management,_ **17**(5), 377–386.
*   <a id="oecd"></a>OECD (1996). _The knowledge based economy_. Paris: OECD. Cited by [Virkus 2003.](#virkus)
*   <a id="pinto"></a>Pinto, M. (2003). Abstracting/abstract adaptation to digital environments: research trends. _Journal of Documentation,_ **59**(5), 581–608.
*   <a id="rader"></a>Rader, H.B. (2002). [_Information Literacy – An Emerging Global Priority_](http://www.nclis.gov/libinter/infolitconf&meet/papers/rader-fullpaper.pdf)_._ White Paper prepared for UNESCO, the U.S. National Commission on Libraries and Information Science, and the National Forum on Information Literacy, for use at the Information Literacy Meeting of Experts, Prague, The Czech Republic. Retrieved 17 November 2006 from http://www.nclis.gov/libinter/infolitconf&meet/papers/rader-fullpaper.pdf
*   <a id="roes"></a>Roes, H. (2001). [Digital libraries and education: Trends and opportunities](http://www.dlib.org/dlib/july01/roes/07roes.html). _D-Lib Magazine,_ **7**(7-8). Retrieved 12 January 2005 from http://www.dlib.org/dlib/july01/roes/07roes.html
*   <a id="samson"></a>Samson, S. & Granath, K. (2004). Reading, writing, and research: added value to university first-year experience programmes. _Reference Services Review_, **32**(2), 149–156\.
*   <a id="sara"></a>Saracevic, T. (1999). Information science. _Journal of the American Society for Information Science_, **50**(12), 1051–1063.
*   <a id="shap"></a>Shapiro, J.J. & Hughes, S. K. (1996). [Information Literacy as a Liberal Art. Enlightenment proposals for a new curriculum](http://www.educause.edu/pub/er/reviewArticles/31131.html). _Educom Review,_ **31**(2), Retrieved 6 February 2004 from http://www.educause.edu/pub/er/reviewArticles/31131.html
*   <a id="sheo"></a>Sheorey, R. & Mokhtari, K. (2001). Differences in the metacognitive awareness of reading strategies among native and non-native readers. _System,_ **29**(4), 431–449.
*   <a id="sher"></a>Sheridan, J. (1992). WAC and libraries: A look at the literature. _Journal of Academic Librarianship,_ **18**(2), 90–94.
*   <a id="sult"></a>Sult, L. & Mills, V. (2006). A blended method for integrating information literacy instruction into English composition classes. _Reference Services Review_, **34**(4), 368–388\.
*   <a id="them"></a>Thematic Debate on Information Literacy (2005). [_UNESCO WebWorld Newsletter_](http://portal.unesco.org/ci/en/ev.php-URL_ID=18565&URL_DO=DO_TOPIC&URL_SECTION=201.html)_,_ 8 April 2005\. Retrieved 21 April 2005 from http://portal.unesco.org/ci/en/ev.php-URL_ID=18565&URL_DO=DO_TOPIC&URL_SECTION=201.html
*   <a id="varis"></a>Varis, T. (2003). [](http://www.elearningeuropa.info/)_New literacies and e-learning competences._Retrieved 13 February 2003 from http://www.elearningeuropa.info/
*   <a id="virkus"></a>Virkus, S. (2003). [Information literacy in Europe: a literature review](http://informationr.net/ir/8-4/paper159.html). _Information Research,_ **8**(4), paper 159\. Retrieved 31 March 2004 from http://informationr.net/ir/8-4/paper159.html
*   <a id="virkuse"></a>Virkus, S. _et al._ (2005). Information Literacy and Learning. _European Curriculum Reflections on Library and Information Science Education._ Ed. by Leif Kajberg and Leif Lorring. Copenhagen: Royal School of Library and Information Science, 65–83\.
*   <a id="ward"></a>Ward, D. (2006). Revisioning information literacy for lifelong learning. _Journal of Academic Librarianship,_ **32**(4), 396–402.
*   <a id="webber"></a>Webber, S. & Johnston, B. (2000). Conceptions of information literacy: new perspectives and implications, _Journal of Information Science_, **26**(6), 381–397.
*   <a id="xxx"></a>White, A. (2007). [Understanding hypertext cognition: Developing mental models to aid users’ comprehension](http://firstmonday.org/issues/issue12_1/white/index.html) , _First Monday,_ **12**(1), Retrieved 10 March 2007 from http://firstmonday.org/issues/issue12_1/white/index.html
*   <a id="wilson"></a>Wilson, D.(1994). Relevance and understanding, In: G. Brown, K. Malmkjaer, A. Pollitt and J. Williams, eds., _Language and understanding._ Oxford: Oxford University Press. 35–58\. Cited by [Yus Ramos 1998](#yus)
*   <a id="yus"></a>Yus Ramos, F. (1998). A decade of relevance theory, _Journal of Pragmatics,_ **30**(3), 305–345.