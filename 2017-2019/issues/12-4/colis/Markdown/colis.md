**Volume 12 No 4 October, 2007**

### Proceedings of the Sixth International Conference on Conceptions in Library and Information Science, Borås, Sweden, 13-16 August, 2007

[Guest Editorial](colis00.html)

#### Keynote papers

#### Elisabeth Orna  
[Collaboration between library and information science and information design disciplines. On what? Why? Potential benefits?](colis02.html)

#### Andrew Dillon  
[Library and information science as a research domain: problems and prospects](colis03.html)

#### Peer-reviewed papers

* * *

#### Sanna Talja and Jenna Hartel  
[Revisiting the user-centered turn in information science research: an intellectual history perspective](colis04.html)

#### R. David Lankes, Joanne Silverstein, Scott Nicholson and Todd Marshall  
[Participatory networks: the library as conversation](colis05.html)

#### Birger Hjørland  
[Arguments for 'the bibliographical paradigm'. Some thoughts inspired by the new English edition of the UDC](colis06.html)

#### Melanie Feinberg  
[Hidden bias to responsible bias: an approach to information systems based on Haraway's situated knowledges](colis07.html)

#### Hsia-Ching Chang and Terrence Maxwell  
[A synergistic alternative to readers' advisory services: managing customer knowledge](colis08.html)

#### Yang Sok Kim and Byeong Ho Kang  
[Tracking government Websites for information integration](colis09.html)

#### Theresa Dirndorfer Anderson  
[Settings, arenas and boundary objects: socio-material framings of information practices](colis10.html)

#### Jan Nolin  
[What's in a turn?](colis11.html)

#### Debbie L. Rabina and David J. Walczyk  
[Information professionals' attitude toward the adoption of innovations in everyday life](colis12.html)

#### Nishikant Kapoor, John T. Butler, Gary C. Fouty, James A. Stemper and Joseph A. Konstan  
[Resolvability of references in users' personal collections](colis13.html)

#### Birgitta Olander  
[Information interaction among computer scientists. A longitudinal study](colis14.html)

#### Jennie A. Abrahamson and Karen E. Fisher  
['What's past is prologue': towards a general model of lay information mediary behaviour](colis15.html)

#### Douglas Colbeck  
[Understanding knowledge genesis by means of multivariate factor analysis of epistemological belief structures](colis16.html)

#### Lars Qvortrup  
[The public library: from information access to knowledge management - a theory of knowledge and knowledge categories](colis17.html)

#### Michael René Kristiansson  
[Strategic reflexive conversation – a new theoretical-practice field within LIS](colis18.html)

#### Anna-Karin Tötterman and Gunilla Widén-Wulff  
[What a social capital perspective can bring to the understanding of information sharing in a university context](colis19.html)

#### Ragnar Andreas Audunson, Andreas Vårheim, Erling Dokk-Holm and Svanhild Aabø  
[Public libraries, social capital and low intensive meeting places](colis20.html)

#### Dominique Maurel and Pierrette Bergeron  
[Problem situations encountered by middle managers working in a municipality in transition](colis21.html)

#### Hong (Iris) Xie  
[Shifts in information-seeking strategies in information retrieval in the digital age: Planned-situational model](colis22.html)

#### Jeppe Nicolaisen and Tove Faber Frandsen  
[The handicap principle: a new perspective for library and information science research](colis23.html)

#### J. Tuomas Harviainen  
[Live-action, role-playing environments as information systems: an introduction](colis24.html)

#### Justine Carlisle  
[Digital music and generation Y: discourse analysis of the online music information behaviour talk of five young Australians](colis25.html)

#### Katriina Byström  
[Approaches to _task_ in contemporary information studies](colis26.html)

#### Jette Hyldegård and Peter Ingwersen  
[Task complexity and information behaviour in group based problem solving](colis27.html)

#### Louise Limberg  
[Learning assignment as task in information seeking research](colis28.html)

#### Marcia J. Bates  
[Defining the information disciplines in encyclopedia development](colis29.html)

#### Gerald Benoit  
[Critical theory and the legitimation of library and information science](colis30.html)

#### David Bawden  
[Information as self-organized complexity; a unifying viewpoint](colis31.html)

#### Deborah Turner  
[Conceptualizing oral documents](colis32.html)

#### Vesa Suominen  
[The problem of 'userism', and how to overcome it in library theory](colis33.html)

#### Annemaree Lloyd  
[Recasting information literacy as socio-cultural practice: implications for library and information science researchers](colis34.html)

#### Educational forum

* * *

#### David Bawden  
[Introduction: Facing the educational future](colise01.html)

##### Invited papers

#### Michael Seadle and Elke Greifeneder  
[Envisioning an iSchool curriculum](colise02.html)

#### Polona Vilar, Maja Zumer and Jessica Bates  
[Information seeking and information retrieval curricula development for modules taught in two library and information science schools: the cases of Ljubljana and Dublin](colise03.html)

#### Theresa Dirndorfer Anderson  
[Information, media, digital industries and the library and information science curriculum. [Abstract only]](colise04.html)

##### Peer-reviewed papers

* * *

#### Hairong Yu and Mari Davis  
[The case for curriculum reform in Australian information management & library and information science education: Part 1\. Technology and digitization as drivers.](colise05.html)

#### Tibor Koltay  
[A new direction for library and information science: the communication aspect of information literacy](colise06.html)

#### Prudence W. Dalrymple and Nancy Roderer  
[Library information science and biomedical informatics: converging disciplines](colise07.html)

#### Lars Seldén  
[The Bologna process and the ups and downs of professionalisation in Swedish public libraries](colise08.html)

#### Poster abstracts

* * *

#### Fredrik Åström  
[Heterogeneity and homogeneity in library and information science research](colisp01.html)

#### Johanna Rivano Eckerdal  
[Young women evaluating information sources before choosing a contraceptive. Dimensions of information literacy and democracy](colisp02.html)

#### Rhiannon Gainor, Lisa Given, Stan Ruecker, Andrea Ruskin, Elisabeth (Bess) Sadler and Heather Simpson  
[Visualization as a research and design approach for library and information science: exploring seniors’ use of a visual search interface](colisp03.html)

#### Mirka Grešková  
[Human-agent interaction from the perspective of information behaviour and usability](colisp04.html)

#### Päivi Helminen  
[From novice to nutrition expert - exploration of nutrition expertise and information use of nutritionists](colisp05.html)

#### Eva Hornung  
[The smart ones: one-person librarians in Ireland and continuing professional development](colisp06.html)

#### Evangelina Koundouraki  
[The information cycle in the European Commission's policy-making process](colisp07.html)

#### Jan Larsson  
[The retrievability of a discipline : a domain analytic view of classification](colisp08.html)

#### Staša Milojević  
[Big science, nano science? Mixed method approach to mapping evolution and structure of nanotechnology](colisp09.html)

##### _Information Research: an international electronic journal_, is published four times a year by Professor Tom Wilson with technical support from Lund University, Sweden and editorial support from the Swedish School of Librarianship and Information Science, Högskolan in Borås.  
These works are licensed under a [Creative Commons License.](http://creativecommons.org/licenses/by-nc-nd/3.0/) The licensors are the authors of each respective article. Design and Editorial content © T.D. Wilson 1996-2006

![Creative Commons License](http://creativecommons.org/images/public/somerights.gif)