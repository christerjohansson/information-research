#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Understanding knowledge genesis by means of multivariate factor analysis of epistemological belief structures

#### [Doug Colbeck](mailto:Doug.Colbeck@utas.edu.au)  
School of Computing, 
University of Tasmania, 
PO Box 1359, 
Launceston 7250, Tasmania, 
Australia

#### Abstract

> **Introduction.** The purpose of this study is to construct an epistemological beliefs survey instrument by extending an existing and accepted model and conducting a confirmatory analysis. The new instrument would then be used to conduct an exploratory analysis on newly acquired data.  
> **Method.** From a total of approximately 515 first year undergraduate Computer Science, Information Systems, Nursing and Health students approached, 435 completed survey forms (84.4%). The data was then naïve response recoded and a sequence of multivariate factor analyses applied.  
> **Results.** The results proved that there was indeed a hierarchical structure of prior epistemological beliefs held by the participants.Developing an understanding of what these beliefs are, how they are formed, and how they are influenced is of significant value as during this study it became apparent that these initial epistemological belief structures could be identified, isolated and developed by educators, enabling improvements in future educational outcomes.  
> **Conclusions.** This research shows that learners’ maintain a unique set of epistemological beliefs that are constantly being constructed and modified by individual as well as social interaction within their learning environment.

## Introduction

This research provides innovative insight into issues currently in focus within higher educational spheres, particularly within knowledge construction, evaluation and utilisation, as well as illuminating principal areas within pedagogical study that are increasingly under scrutiny, as educational institutions attempt to match the outcomes and expectations of today’s society and improving learning outcomes.

Issues such as educator facilitation and learner-centred instructional paradigms have emerged as critically important aspirations within new dynamic educational environments and domains ([Schuman and Ritchie, 2006](#schu06)).

Learners, however, have their own unique perspectives, experiences, and learning styles, as Gopnik and Meltzoff ([1997](#gop97)) state in discussion of their “Theory theory”, underlying any human cognitive activity is an abstract structure that is not all that apparent in superficial phenomenology or practice ([Gopnik and Meltzoff, 1997](#gop97)).

These uniquely developed structures affect how learners interact socially and intellectually within their learning environment, as well as how they interact with any forms of technology used within their selected educational facility ([Colbeck, 2003](#col03)).

The educator’s role is also changing in order to help facilitate the learner’s progression, with constructive, guiding and efficacious methodologies that give the learner time to practice new skills, and draw conclusions relevant to their own unique experiences.

This research is aimed toward understanding learner’s epistemological belief structures as well as understanding ontological knowledge genesis process.

The goals of this research include;

1.  The format of the new research instrument would be easy to distribute, collate and analyse.
2.  The data gathered by the new research instrument would illustrate a clearer structure of the beliefs of learners.
3.  The new research instrument would be reliable enough to be able to extend into additional exploratory research.
4.  The exploratory results would assist understanding the knowledge creation processes.

## Rationale behind the research

This project commenced with the desire to understand more intimately the initial processes that humans undertake when creating knowledge.  
Providing a high degree of confidence within the research would be necessary so it was decided that peer acknowledged pieces of research be studied and if possible, extended to suit the needs of the study.

In 1990, Marlene Schommer (now Schommer-Aikins) developed an epistemological beliefs survey instrument within her dissertation (89-24938) that assessed her hypothesised structure of five more-or-less independent beliefs among college students ([Schommer-Aikins, 1990](#sch90)). This pencil & paper instrument, along with some personal encouragement from Marlene Schommer-Aikins ([Schommer-Aikins, 2005](#sch05)), appeared to offer the ability to provide the necessary baseline data for this research project.

This existing survey instrument was re-crafted so that the language within the statements would be more easily comprehended by the targeted participants, and the analysis of the results being more explicit and particular to the purpose required within this research.  
After careful study of the original Schommer-Aikins research, the Epistemological Beliefs Sampler (EBS) was developed.

## Literature review

Epistemology and epistemological beliefs are those concepts concerning the nature of knowledge and learning as well as beliefs about how individuals come to know, how knowledge is constructed and how knowledge is evaluated ([Tolhurst and Debus, 2002](#tol02), [Hofer and Pintrich, 2002](#hof02), [Schommer-Aikins, 2002](#sch02), [Schommer-Aikins, 2004](#sch04), [Schommer-Aikins, 1998](#sch98)).

Some argue that epistemological theories do have major limitations, but Hjorland ([2002](#hjo02)) states that these theories are the best general models we have and that their importance is widely recognised ([Hjorland, 2002](#hjo02)).

Perry ([1968](#per68)) is generally credited for initiating the current interest in the exploration of peoples’ beliefs about knowledge and learning and how they change over time. He and his colleagues developed a paper-and-pencil Likert scale to measure peoples’ beliefs. This instrument, called the Checklist of Educational Views (CLEV), was introduced and readministered to 31 students at Harvard over a period of some 4 years, resulting in some 98 recorded interviews.

Perry later confirmed the model developed by his earlier work by conducting a further 366 interviews. Perry and his colleagues had found evidence that college students’ beliefs about knowledge changed over time, but more importantly they found that these beliefs could be assessed with their pencil-and–paper test, the CLEV ([Perry, 1968](#per68), [Schommer-Aikins, 1990](#sch90), [Schommer-Aikins, 2004](#sch04), [Schraw and Bendixen et al., 2002](#schr02)).

Perry concluded that many first year students believe that simple, unchangeable facts are handed down by omniscient authority. By the time they reach their senior year, students believe that complex tentative knowledge is derived from reason and empirical enquiry ([Schommer-Aikins, 2004](#sch04)).

In 1990 Marlene Schommer-Aikins developed the Epistemological Beliefs Questionnaire (EQ), this was a departure from the more developmental approach to a system of five (5) more-or-less independent beliefs. These hypothesised beliefs included beliefs about (a) the stability of knowledge, (b) the structure of knowledge, (c) the source of knowledge, (d) the speed of learning and (e) the ability to learn ([Schommer-Aikins, 2004](#sch04)).

Since the inception of this instrument; many other researchers have taken it upon themselves to attempt the development of better instruments. Schommer-Aikins (2002) states that there has been some discussion toward some of these developments insomuch as some researchers have found her instrument to be a useful predictor of a learner’s belief structure ([Hall and Chiarello et al., 1996](#hal96), [Windschitl and Andre, 1998](#win98), [Schommer-Aikins, 2002](#sch02)).

Some researchers have worked towards a more psychometrically sound instrument. For example, Jheng et al ([1993](#jhe93)) followed up on Schommer-Aikins work by comparing epistemological beliefs of students across different majors and between educational levels (Jheng and Johnson et al., 1993). His instrument was constructed based on questionnaires developed by Schommer-Aikins ([1990](#sch90)) in ([Schommer-Aikins, 2002](#sch02)).

Jheng et al’s questionnaire attempted to measure four of the five epistemological beliefs hypothesized by Schommer-Aikins including beliefs in the stability of knowledge, the source of knowledge, the speed of learning, and the ability to learn. A fifth belief, the orderly process of learning replaced Schommer-Aikins hypothesized belief about the structure of knowledge ([Duell and Schommer-Aikins, 2001](#due01)).

Other researchers have used the instrument as a starting point to go on and develop their own method of measuring epistemological beliefs. Schraw et al ([1995](#schr95)) proposed and created the Epistemological Beliefs Inventory (EBI). Their goal was to develop an alternate tool that would capture all the original beliefs initially hypothesised by Schommer-Aikins ([Schraw and Dunkle et al., 1995](#schr95), [Kardash and Scholes, 1996](#kar96), [Duell and Schommer-Aikins, 2001](#due01)).

The EBS instrument developed for this study would be found in the latter developmental discussions as it is primarily based on the concepts explored by the Schommer-Aikins 63 question Epistemological Beliefs Questionnaire.

## The Epistemological Beliefs Sampler (EBS)

However, like some of the researchers mentioned previously, some concepts in developing this new instrument would have to be re-examined, as it was essential that the results gathered and analysed by the research maintain a statistical reliability ([Neuman, 2003](#neu03)).

### Vocabulary review

Some statements within the new instrument were altered to give an Australian lexis perspective and to ensure that the comprehension of the statements were not distorted so that they could be easily understood by the participants.

To fit into an Australian University level educational environment, words like teacher or instructor were replaced with the word lecturer; the word school was replaced by the word University, etc.

Other statements required more than single word changes e.g. the statement “People who challenge authority are over-confident” was replaced with “People who challenge authority come across as a bit full of themselves”. This form of wording would relate more comfortably to Australian students and allow them to comprehend the underlying context of the statement.

However other statements were introduced to actively scope of the participant’s comprehension e.g. “Events from the past do not influence events in the future”. This statement was designed to explore the student’s belief toward whether or not they viewed knowledge as conditional, and would they expect knowledge to be certain or changeable – implying contextually alterable knowledge.

### EBS acceptance

To warrant an easier acceptance of the EBS by both staff and students, and ensure a good percentage of responses, the survey was distributed during the participants’ first orientation lecture at the University.

This was acceptable on two points;  
(1) Less time to overtly think about the statements by the participants would produce more significant levels of first response answers and  
(2) It would also allow the instrument to be distributed, answered, and collected easily within the first fifteen minutes of the participant’s first lecture of semester one while the lecturer was concurrently completing other initial administration tasks.

This strategy enabled a response return rate of 84.4% to be achieved for this study.

### Participant demographics

From a total of four hundred and thirty five (435) student responses received, one hundred and sixty six (166) were male, and two hundred and sixty nine (269) were female - see table 1\. Participant demographics

<table><caption>

**Table 1: Participant demographics**</caption>

<tbody>

<tr>

<th rowspan="2">Age Groups</th>

<th colspan="2">Gender</th>

<th colspan="1" rowspan="2">Survey Totals</th>

</tr>

<tr>

<th colspan="1">M</th>

<th colspan="1">F</th>

</tr>

<tr>

<td>< 20</td>

<td>106</td>

<td>140</td>

<td>246</td>

</tr>

<tr>

<td>20 – 24</td>

<td>29</td>

<td>43</td>

<td>72</td>

</tr>

<tr>

<td>25 - 29</td>

<td>12</td>

<td>15</td>

<td>27</td>

</tr>

<tr>

<td>30 – 39</td>

<td>13</td>

<td>37</td>

<td>50</td>

</tr>

<tr>

<td>40 – 49</td>

<td>3</td>

<td>28</td>

<td>31</td>

</tr>

<tr>

<td>50 +</td>

<td>3</td>

<td>6</td>

<td>9</td>

</tr>

<tr>

<td></td>

<td>

**166**</td>

<td>

**269**</td>

<td>

**435**</td>

</tr>

</tbody>

</table>

Students from four broad content domains participated in this study, the four domains being, Computing, Information Systems, Nursing and Health students.

#### Maintaining a measurable indicator

As this researcher’s initial intention was a confirmatory analysis of the epistemological beliefs held by first year university level students, it was considered necessary that the demographics of the participants also conformed as closely as possible to the original EQ 1990 test group as stated in Schommer-Aikins ([1990](#sch90)).

### The EBS design

After extensive study and examination of the research literature, two of the original Schommer-Aikins pre-defined twelve subsets appeared to have negligible effect on the study (see Table 2: Statement allocation).

In Schommer-Aikins’s original results, Concentrated Effort had the smallest loading coefficient value (0.09552), and Cant’ learn how to learn” posed statements that most learner’s would not have had the experience or ability to answer with any measure of confidence or understanding ([Dixon, 2000](#dix00)). Both these subsets were consequently discarded.

Some researchers had also argued that some of the statements in the original Schommer-Aikins study may not have necessarily fulfilled the needs of the research initially proposed within the original data analysis ([Schraw and Bendixen et al., 2002](#schr02)).

Other researchers reached similar conclusions when attempting to recreate the results based on the Schommer-Aikins instrument, or even during their efforts to modify the original EQ survey instrument [(Jheng and Johnson et al., 1993](#jhe93), [Hall and Chiarello et al., 1996](#hal96), [Tolhurst and Debus, 2002](#tol02)).

The number of statements within each subset also seemed excessive, as several of the original Schommer-Aikins statements appeared to be only reworked versions of other similar statements within her study.

After applying the Australian perspective to the original statements and removing those statements considered redundant to the designs of the study, additional statements were added in an attempt to redress any imbalance.

The statements that were finally selected for inclusion in the EBS were also objectively balanced to ensure that the valence use within each subset closely mirrored the percentage of negative valence to positive valence found in the original Schommer-Aikins instrument (27(-), 36(+), (75%)) as compared to the EBS (15(-), 19(+), (78%)).

<table><caption>

**Table 2: Statement allocation**</caption>

<tbody>

<tr>

<th rowspan="1">No.</th>

<th>Statement Subsets</th>

<th rowspan="1">EQ</th>

<th>EBS</th>

</tr>

<tr>

<td>1</td>

<td>Seek single answers</td>

<td>7(+)  
4(-)</td>

<td>3(+)  
3(-)</td>

</tr>

<tr>

<td>2</td>

<td>Avoid integration</td>

<td>4(+)  
4(-)</td>

<td>2(+)  
2(-)</td>

</tr>

<tr>

<td>3</td>

<td>Avoid ambiguity</td>

<td>4(+)  
1(-)</td>

<td>3(+)  
1(-)</td>

</tr>

<tr>

<td>4</td>

<td>Knowledge is certain</td>

<td>3(+)  
3(-)</td>

<td>2(+)  
2(-)</td>

</tr>

<tr>

<td>5</td>

<td>Depend on authority</td>

<td>3(+)  
1(-)</td>

<td>1(+)  
1(-)</td>

</tr>

<tr>

<td>6</td>

<td>Don’t criticize authority</td>

<td>3(+)  
3(-)</td>

<td>2(+)  
2(-)</td>

</tr>

<tr>

<td>7</td>

<td>Ability to learn is innate</td>

<td>4(+)  
0(-)</td>

<td>1(+)  
0(-)</td>

</tr>

<tr>

<td>8</td>

<td>Learn the first time</td>

<td>2(+)  
1(-)</td>

<td>2(+)  
1(-)</td>

</tr>

<tr>

<td>9</td>

<td>Learning is quick</td>

<td>3(+)  
2(-)</td>

<td>3(+)  
2(-)</td>

</tr>

<tr>

<td>10</td>

<td>Success is unrelated to hard work</td>

<td>1(+)  
3(-)</td>

<td>0(+)  
1(-)</td>

</tr>

<tr>

<td>11</td>

<td>Can’t Learn how to learn</td>

<td>1(+)  
4(-)</td>

<td>Not used</td>

</tr>

<tr>

<td>12</td>

<td>Concentrated effort is a waste of time</td>

<td>1(+)  
1(-)</td>

<td>Not used</td>

</tr>

<tr>

<td></td>

<td>

**Total statements**</td>

<td>

**63**</td>

<td>

**34**</td>

</tr>

</tbody>

</table>

Table 2: Statement allocations, illustrates the differences in statement distribution between the Schommer-Aikins instrument and the EBS instrument proposed in this research

From this breakdown of the statement allocation; it can be seen that all subsets deemed to be critical within the original EQ instrument, are well represented. Detailed analysis of the gathered responses from the participants using EBS confirmed that this particular statement distribution matrix proved satisfactory. A full list of the statements used in the EBS can be found in Annex A.

## Research methodology

This project is a phenomenological case study utilising quantitative research methods to gain an understanding of the epistemological beliefs of a group of new undergraduate university students.

### Phenomenology

Phenomenology is the study of "phenomena": appearances of things, or things as they appear in our experience, or the ways we experience things, thus the meanings things have in our experience.

We all experience various types of experience including perception, imagination, thought, emotion, desire, volition, and action ([Woodruff-Smith, 2003](#)). This axiom that people experience the world in different ways ideally suited the requirements of this study.

### Justification for using the case study methodology

Benbasat et al ([1987](#ben87)) state that case research is suitable for studies that are in early or formative stages or where the experiences of the subjects are important and the context within which they operate is vital. The case study approach can be helpful in identifying and exploring areas for further research and aiding hypothesis generation ([Carroll and Daeson et al., 1998](#car98)). This corresponds to the particular area under investigation.

### Reliability and validity

Any interpreted qualification of data has been based on quantitatively collected data from participants involved with the study and should be recognisable as being both conceivable and verifiable by readers of the research.

A facet of validity relates to the generalisation of research findings, the result of this research was produced from a relatively large sample population.  
Therefore it is suggested that the findings presented are repeatable and valid within the context discussed.

## Confirmatory replication analysis

The gathered data was initially entered into an MS Excel spreadsheet, which allowed quick calculation of the mean responses for each question by the participants.  
Since approximately half of the statements were worded so that a naïve individual would simply agree with them and the other half were worded so that the naïve individual would simply disagree with them, some of the statements need to be recoded.

Factor analysis is based on the assumption that the higher the score, the more naïve the individual. So, all statements that a naïve individual would disagree with needed to be changed, for example if the participant responded with a four (4) to a question with a negative valence (-) then this would have to be recoded to a two (2), see Annex A: EBS Statement structure for all valence relations.

Finally the subset averages for each statement were amalgamated into a final table ready for the factor analysis stage.

### Factor analysis

The recoded subset data was entered into the application; Statistical Package for the Social Sciences v12.0.1 (SPSS) and a factor analysis conducted using Varimax rotation. From the detailed readout provided by this application a definitive comparison was able to be made as to the suitability of the data for such an analysis, as well as favourable comparison to the original Schommer-Aikins (1990) sample group.

#### Statistical validity

Kaiser-Meyer-Olkin measure of Sampling Adequacy is a measure of whether or not the distribution of values is adequate for conducting factor analysis. A measure of >0.9 is marvellous, >0.8 is meritorious, >0.7 is middling, > 0.6 is mediocre, > 0.5 is miserable and < 0.5 is unacceptable. The data returned a value sampling adequacy of 0.768 which is middling, almost meritorious.

Bartlett’s test of Sphericity is a measure of the multivariate normality of the set of distributions. It also tests whether the correlation matrix conducted within the factor analysis is an identity matrix. Factor analysis would be meaningless with an identity matrix. A significance value < 0.05 indicates that the data do NOT produce an identity matrix and are thus approximately multivariate normal and acceptable for factor analysis (George and Mallery, 2003). The data within this study returned a significance value of 0.000, indicating that the data was acceptable for factor analysis.

Cronbach’s alpha calculated by SPSS for this investigation was 0.696, and deemed acceptable.

#### Factor extraction

After the Eigenvalues for each subset were plotted on a bicoordinate plane to establish the number of significant components, the solution was then rotated to enhance the view of the results. There were four significant components extracted during the analysis that had maintained an acceptable Eigenvalue greater than or equal to 1.0\.  
These four extracted components comprised a total of 61.083% of the data analysed, see figure: 1 Component Plot.  
This is the same number of postulated factors that Schommer-Aikins had produced, and suggests that the EBS should be able to successfully produce similar results.

<figure>

![Component plot in rotated space](../colis16fig1.gif)

<figcaption>

**Figure 1: Component plot in rotated space**</figcaption>

</figure>

#### Component score coefficient matrix

Having accepted the rotated plot of the data, a component score coefficient matrix was generated to show how the pre-defined subgroups of statements had loaded within their respective factors. This also allowed a more direct comparison with the original Schommer-Aikins results.

Table 3: Confirmatory Coefficient Matrix, illustrates the distribution of subset to factor relationships. All EBS factors loaded significantly higher than the results recorded in the original EQ study.

<table><caption>

**Table 3: Confirmatory Coefficient Matrix**</caption>

<tbody>

<tr>

<th rowspan="2"></th>

<th colspan="4">Component</th>

</tr>

<tr>

<td>

**1**</td>

<td>

**2**</td>

<td>

**3**</td>

<td>

**4**</td>

</tr>

<tr>

<td>

**SS1**</td>

<td>

**.482**</td>

<td>-.242</td>

<td>.022</td>

<td>-.028</td>

</tr>

<tr>

<td>

**SS2**</td>

<td>.098</td>

<td>.030</td>

<td>-.248</td>

<td>

**.381**</td>

</tr>

<tr>

<td>

**SS3**</td>

<td>

**.340**</td>

<td>.172</td>

<td>-.176</td>

<td>-.085</td>

</tr>

<tr>

<td>

**SS4**</td>

<td>

**.424**</td>

<td>-.065</td>

<td>.062</td>

<td>-.095</td>

</tr>

<tr>

<td>

**SS5**</td>

<td>-.187</td>

<td>-.111</td>

<td>.051</td>

<td>

**.771**</td>

</tr>

<tr>

<td>

**SS6**</td>

<td>.200</td>

<td>-.058</td>

<td>.094</td>

<td>

**.343**</td>

</tr>

<tr>

<td>

**SS7**</td>

<td>.040</td>

<td>-.195</td>

<td>

**.657**</td>

<td>.002</td>

</tr>

<tr>

<td>

**SS8**</td>

<td>-.119</td>

<td>

**.519**</td>

<td>-.034</td>

<td>.014</td>

</tr>

<tr>

<td>

**SS9**</td>

<td>-.116</td>

<td>

**.593**</td>

<td>-.069</td>

<td>-.112</td>

</tr>

<tr>

<td>

**SS10**</td>

<td>-.083</td>

<td>.149</td>

<td>

**.490**</td>

<td>-.111</td>

</tr>

<tr>

<td colspan="5">

Extraction Method: Principal Component Analysis.  
Rotation Method: Varimax with Kaiser Normalization.</td>

</tr>

</tbody>

</table>

## Results and evaluations of the confirmatory analysis

When the subset loadings were considered in relation to their groupings, the four factors began to take on a similarity to those proposed by Schommer-Aikins.

**Factor 1**: Simple Knowledge  
• SS1: Seek single answers  
• SS3: Avoid ambiguity  
• SS4: Knowledge is certain

**Factor 2:** Quick Learning  
• SS8: Learn the first time  
• SS9: Learning is quick

**Factor 3:** Fixed Ability  
• SS7: Ability to learn is innate  
• SS10: Success is unrelated to hard work

**Factor 4:** Omniscient Authority  
• SS2: Avoid integration  
• SS5: Depend on authority  
• SS6: Don’t criticize authority

The subset grouping appeared to be more similar in nature to the Omniscient Authority factor initially proposed by Schommer-Aikins and further discussed by Shraw et al. ([2002](#shr02)) in their research into the development of the Epistemic Beliefs Index (EBI) ([Schraw and Bendixen et al., 2002](#schr02)).

### Epistemic belief structures

The results revealed by the EBS showed that by using the pre-defined subset groupings proposed by Schommer-Aikins it was possible to reproduce a four factor belief structure. While these factors may differ slightly from the original EQ study, this is not surprising given the difference in chronology and changed social attitudes, let alone the difference in geographical locale.

The results did show a clear belief structure of the participants. They believe that questions posed to them during their studies required simple unambiguous answers (facts) and this information would rarely alter. This is seen to be driven by the need to find the answer that would gain the most favourable assessment, not necessarily the correct answer.

They also expressed a belief that the information should be easy to learn or else they would struggle to comprehend it. The belief, uniform across all studies, that some individuals have innate talent and learning comes easy to them, was also clearly expressed.

Finally, the dependence on a belief that information and knowledge handed down from their lecturer or educator should be accepted and preserved with minor or no integration with previously experienced knowledge.

## Exploratory analysis

To further test the EBS to see if it was capable of producing interpretable results, the original 34 statements were re-analysed by the SPSS application and then analysed again based on the naturally forming subsets found in the original pass. This has never been reported by Schommer-Aikins as having been done before during her study.  
It is not uncommon for a data set to be subjected to a series of factor analysis and rotation before the obtained factors can be considered “clean” and interpretable ([Ho, 2000](#ho00)).

### First exploratory analysis

The first unrestricted analysis of the data was conducted exactly as the confirmatory analysis using all the data ungrouped as opposed to predefined subsets. This produced eleven (11) discernibly different possible subsets. See Table 4: Factor loadings.

<table><caption>

**Table 4: Factor loadings**</caption>

<tbody>

<tr>

<td>

**No**</td>

<td>

**Subset**</td>

<td>

**Statement No's**</td>

</tr>

<tr>

<td>

**1**</td>

<td>Innate Ability</td>

<td>16, 25, 34</td>

</tr>

<tr>

<td>

**2**</td>

<td>Success unrelated to hard work</td>

<td>17, 23, 27, 32</td>

</tr>

<tr>

<td>

**3**</td>

<td>Learn the first time</td>

<td>26, 31, 33</td>

</tr>

<tr>

<td>

**4**</td>

<td>Accept authority</td>

<td>5, 9, 19, 21, 24</td>

</tr>

<tr>

<td>

**5**</td>

<td>Certain knowledge</td>

<td>3, 12, 14, 15</td>

</tr>

<tr>

<td>

**6**</td>

<td>Seek single answers</td>

<td>2, 4, 18</td>

</tr>

<tr>

<td>

**7**</td>

<td>Avoid ambiguity</td>

<td>7, 11, 13</td>

</tr>

<tr>

<td>

**8**</td>

<td>Don’t criticize authority</td>

<td>20, 22</td>

</tr>

<tr>

<td>

**9**</td>

<td>Learning is quick</td>

<td>28, 29, 30</td>

</tr>

<tr>

<td>

**10**</td>

<td>Avoid integration</td>

<td>6, 8, 10</td>

</tr>

<tr>

<td>

**11**</td>

<td>Relational knowledge</td>

<td>1</td>

</tr>

</tbody>

</table>

Subsets were now formed using those loadings illustrated in table 4 and again analysed by the SPSS application and the output scrutinized to see if the EBS instrument could provide interpretable results.

#### First pass statistical validity

Within the first pass of the exploratory analysis the Kaiser-Meyer-Olkin measure of Sampling Adequacy was calculated to be 0.768\. Bartlett’s test of Sphericity was measured to be 0.000\. Cronbach’s alpha was calculated to be 0.744.

These eleven factors explained a total of 53.50% of the analysis.

### Second exploratory analysis

The newly formed subset data was the processed and analysed, with the output revealing that the eleven subsets loaded onto four factors, see Table 5: Exploratory Coefficient Matrix.

Factor 1: Fixed ability (1, 2 and 9)  
Factor 2: Simple knowledge (5, 6 and 7)  
Factor 3: Quick learning (3, 8 and 10)  
Factor 4: Omniscient authority (4 and 11)

The certain knowledge factor described by Schommer-Aikins (1990) appears to load within the simple knowledge belief for this study’s participants.

<table><caption>

**Table 5: Exploratory Coefficient Matrix**</caption>

<tbody>

<tr>

<th rowspan="2"></th>

<th colspan="4">Component</th>

</tr>

<tr>

<td>

**1**</td>

<td>

**2**</td>

<td>

**3**</td>

<td>

**4**</td>

</tr>

<tr>

<td>

**SS1**</td>

<td>

**.437**</td>

<td>-.105</td>

<td>-.080</td>

<td>.151</td>

</tr>

<tr>

<td>

**SS2**</td>

<td>

**.367**</td>

<td>.050</td>

<td>.109</td>

<td>-.123</td>

</tr>

<tr>

<td>

**SS3**</td>

<td>.203</td>

<td>.055</td>

<td>

**.314**</td>

<td>-.280</td>

</tr>

<tr>

<td>

**SS4**</td>

<td>.030</td>

<td>-.002</td>

<td>.018</td>

<td>

**.508**</td>

</tr>

<tr>

<td>

**SS5**</td>

<td>-.028</td>

<td>

**.584**</td>

<td>-.256</td>

<td>-.085</td>

</tr>

<tr>

<td>

**SS6**</td>

<td>-2.18</td>

<td>

**.406**</td>

<td>.005</td>

<td>.253</td>

</tr>

<tr>

<td>

**SS7**</td>

<td>-.019</td>

<td>

**.445**</td>

<td>.022</td>

<td>-.049</td>

</tr>

<tr>

<td>

**SS8**</td>

<td>-.081</td>

<td>-.139</td>

<td>

**.588**</td>

<td>-.034</td>

</tr>

<tr>

<td>

**SS9**</td>

<td>

**.476**</td>

<td>-.105</td>

<td>-.128</td>

<td>-.001</td>

</tr>

<tr>

<td>

**SS10**</td>

<td>-.100</td>

<td>-.091</td>

<td>

**.483**</td>

<td>.224</td>

</tr>

<tr>

<td>

**SS11**</td>

<td>-.035</td>

<td>-.043</td>

<td>-.006</td>

<td>

**.572**</td>

</tr>

<tr>

<td colspan="5">

Extraction Method: Principal Component Analysis.  
Rotation Method: Varimax with Kaiser Normalization.</td>

</tr>

</tbody>

</table>

#### Second pass statistical validity

Within the second pass of the exploratory analysis the Kaiser-Meyer-Olkin measure of Sampling Adequacy was calculated to be 0.740\. Bartlett’s test of Sphericity was measured to be 0.000\. Cronbach’s alpha was calculated to be 0.641.

These four extracted factors explained a total of 54.22% of the second analysis.

## Conclusions and recommendations

The beliefs demonstrated by the participants of this study clearly indicate that the vast majority are still at the immature dualistic thinking level. Also the beliefs that successful students comprise an innate ability combined with an even firmer belief that problem and questions posited by higher level educators during lectures and tutorials have clearly defined simple answers that the learner need only memorize and regurgitate at a later date during assessment.

The participants also exhibited an obvious need to maintain a guiding or directing authority figure that will supply factual information in an attempt to preserve their comfort zone in the new educational environment.

Within the scope of a broader research project on understanding knowledge genesis and methodologies of improving personal literacy, the development and construction of an instrument capable of indicating a naissance framework of learners epistemological belief structures, presents a sound preliminary point of reference that provides valuable insight toward understanding how learners view, evaluate, and construct knowledge.

The statistical information provided by the factor analysis used in this study proves that the EBS is capable of tentatively elucidating epistemological beliefs from participants.

The initial epistemological belief structures containing perceptions such as the beliefs uncovered within this study can now be identified and isolated. These ideals may then be compared, allowing contrasts and variances to be observed illustrating trends and changes of attitude within those beliefs.

It is widely acknowledged that Schommer-Aikins ([1990](#sch90)) work is seminal in the understanding of epistemological beliefs, but given the arguments uncovered in the literature and throughout the developmental work on the EBS, consideration needs to be given toward construction of better methods of gathering these sensitive views of learners.

While the actual factor loadings did not mirror the original results reported in Schommer-Aikins ([1990](#sch90)), the obvious similarity of factor loading between the EQ and the EBS demonstrates that the theories behind personal epistemological beliefs can be considered reliable and reproducible.

Previous research into other literary articles along similar lines of discussion by noted experts such as Paul Pintrich, Barbara Hofer, Marcia Baxter Magolda, Gregory Shraw, Denise Tolhurst and Phillip Wood, has convinced this researcher that the EBS instrument constructed for this study is an enabling tool that is capable of gathering conceptual information that can be reproduced in a form that gives educators and researchers an insight into the development of theories within the field of personal epistemological belief structures.

Although the development, completion and future online use of the EBS instrument will help alleviate the problems of lengthy administration and scoring time, it is important to remember that it is still being developed by this researcher.

## Educational implications

The role of epistemological beliefs is subtle, yet ubiquitous. These beliefs do influence how students learn, how teachers instruct, and subsequently how teachers knowingly or unknowingly modify their students’ epistemological beliefs.

Epistemological belief structures affect how the learner controls their information needs and the processes used when accepting new evidence as relevant or superfluous. This idea of information relevance and cognitive development based on pre-understandings is a fundamental concept in learner development as well as Information Science and ([Hjorland, 2000](#hjo00)).

Evidence is also accumulating to support the notion that the student’s epistemological beliefs play an important role in their learning. The “Theory theory” also proposes that there are powerful cognitive processes that revise existing theories or beliefs in response to new evidence ([Gopnik and Meltzoff, 1997](#gop97)). For example, various studies indicate that the more students believe in certain knowledge, the more likely they are to draw absolute conclusions from tentative text. The more students believe in fixed ability, simple knowledge, and quick learning, the more likely they are to display lower levels of reflective judgment. The more students believe in quick learning, the more likely they are to comprehend text poorly or earn lower grade point averages. The more students believe in fixed ability, the less likely they are to value schooling or persist on difficult academic tasks.

If educators can ascertain individual students’ epistemological beliefs by comparison to group norms, they can adapt instruction to guide lower achieving students into higher level thinking, and conversely, they can adapt instruction for higher achieving students to assist their growth.

Understanding how humans create and develop personal knowledge is also of significant interest to AI project development.

## Annex A: EBS Statement structure

<table><caption>

**Appendix A: EBS Statement Structure**</caption>

<tbody>

<tr>

<th colspan="4" rowspan="1">EBS Statement structure</th>

</tr>

<tr>

<td>

**Subset**</td>

<td>

**No**</td>

<td>

**Statement**</td>

<td>

**Valence**</td>

</tr>

<tr>

<td rowspan="6">

**Subset 1:**  
Seek single answers</td>

<td>1</td>

<td>You never know what a book is about unless you know the intentions of the author</td>

<td>(-)</td>

</tr>

<tr>

<td>2</td>

<td>Most words have one clear meaning  
</td>

<td>(+)</td>

</tr>

<tr>

<td>3</td>

<td>A sentence has little meaning unless you know the context in which it is used</td>

<td>(-)</td>

</tr>

<tr>

<td>4</td>

<td>The best thing about science courses is that most problems have only one right answer</td>

<td>(+)</td>

</tr>

<tr>

<td>5</td>

<td>The most important part of scientific work is original thinking</td>

<td>(-)</td>

</tr>

<tr>

<td>6</td>

<td>A good lecturer will keep their students from wandering off the right track  
</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="4">

**Subset 2:**
Avoid integration

</td>

<td>7</td>

<td>You will just get confused if you try and integrate new ideas in a textbook with knowledge that you already have about the subject</td>

<td>(+)</td>

</tr>

<tr>

<td>8</td>

<td>Studying means understanding the big issues, rather than details</td>

<td>(-)</td>

</tr>

<tr>

<td>9</td>

<td>A really good way to understand a textbook is to reorganise the information according to your own personal way of looking at it</td>

<td>(-)</td>

</tr>

<tr>

<td>10</td>

<td>Being a good student means that you can memorise a lot of facts</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="4">

**Subset 3:**
Avoid ambiguity

</td>

<td>11</td>

<td>It is a waste of time working on problems that have no possibility of coming out with a clear cut and unambiguous answer</td>

<td>(+)</td>

</tr>

<tr>

<td>12</td>

<td>I find it refreshing to think about issues that experts can’t agree on</td>

<td>(-)</td>

</tr>

<tr>

<td>13</td>

<td>If lecturers would stick more to the facts and less about theory, students would get more out of University</td>

<td>(+)</td>

</tr>

<tr>

<td>14</td>

<td>I don’t like movies that don’t have a clear-cut ending</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="4">

**Subset 4:**
Knowledge is certain

</td>

<td>15</td>

<td>Truth is unchanging</td>

<td>(+)</td>

</tr>

<tr>

<td>16</td>

<td>The only thing certain in life is uncertainty itself</td>

<td>(-)</td>

</tr>

<tr>

<td>17</td>

<td>Events from the past do not influence events in the future</td>

<td>(-)</td>

</tr>

<tr>

<td>18</td>

<td>If scientists try hard enough, they can find out the truth about almost everything</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="2">

**Subset 5:**  
Depend on Authority  

</td>

<td>19</td>

<td>When you first encounter a difficult concept in a textbook, it is better for you to work it out on your own rather than ask your lecturer</td>

<td>(-)</td>

</tr>

<tr>

<td>20</td>

<td>Sometimes you need to accept answers from a lecturer even though you don’t understand them</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="4">

**Subset 6:**  
Don’t criticize Authority

</td>

<td>21</td>

<td>Even advice from experts should be questioned</td>

<td>(-)</td>

</tr>

<tr>

<td>22</td>

<td>People who challenge authority come across as a bit full of themselves</td>

<td>(+)</td>

</tr>

<tr>

<td>23</td>

<td>You can believe almost everything you read</td>

<td>(+)</td>

</tr>

<tr>

<td>24</td>

<td>If you believe you are familiar with the topic, you should evaluate the accuracy of the information in your textbook</td>

<td>(-)</td>

</tr>

<tr>

<td>

**Subset 7:**
Ability to learn is innate

</td>

<td>25</td>

<td>Some people are born to be good learners; others are stuck with a limited ability</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="3">

**Subset 8:**
Learn the first time

</td>

<td>26</td>

<td>Almost all the information you can learn from a text you will get from the first reading</td>

<td>(-)</td>

</tr>

<tr>

<td>27</td>

<td>If you find the time to re-read a textbook chapter, you would get more out of it the second time around</td>

<td>(+)</td>

</tr>

<tr>

<td>28</td>

<td>Going over and over a difficult textbook chapter usually won’t help you understand it</td>

<td>(+)</td>

</tr>

<tr>

<td rowspan="5">

**Subset 9:**
Learning is quick

</td>

<td>29</td>

<td>If you can’t understand something within a short period of time, you should just keep on trying</td>

<td>(+)</td>

</tr>

<tr>

<td>30</td>

<td>Working hard on a difficult problem for an extended period of time only pays off for really smart students</td>

<td>(-)</td>

</tr>

<tr>

<td>31</td>

<td>If you are ever going to understand something, it will make sense to you the first time</td>

<td>(+)</td>

</tr>

<tr>

<td>32</td>

<td>Successful students understand things quickly</td>

<td>(-)</td>

</tr>

<tr>

<td>33</td>

<td>Learning is a slow process of building up knowledge</td>

<td>(+)</td>

</tr>

<tr>

<td>

**Subset 10:**
Success is unrelated to hard work

</td>

<td>34</td>

<td>Wisdom is not necessarily knowing the answers, but knowing how to find the answers</td>

<td>(-)</td>

</tr>

</tbody>

</table>

## References

*   <a id="ben87"></a>Benbasat. I, Goldstein, D. K. and Mead, M. (1987). The Case Research Strategy in Studies of Information Systems, _MIS Quarterly_, **5**, 369-386
*   <a id="car98"></a>Carroll, J., Dawson, L.L., Swatman, P.A. (1998). _Using case studies to build theory: structure and rigour_. Paper presented at the 9th Australasian Conference on Information Systems, 30 September-2 October, University of NSW, Sydney.
*   <a id="col03"></a>Colbeck, D. (2003). _Perceptions of E-Learning within Primary Education in Tasmania with regards to future design, direction and policies_. Launceston: University of Tasmania.
*   <a id="dix00"></a>Dixon, N. (2000). _The Organizational Learning Cycle: How We Can Learn Collectively_. Aldershot: Gower.
*   <a id="due01"></a>Duell, O. & Schommer-Aikins, M. (2001). Measure of people's beliefs about knowledge and learning. _Educational Psychology Review_, **13**, 419-449.
*   <a id="geo03"></a>George, D. & Mallery, P. (2003). _SPSS for Windows Step by Step: A Simple Guide and Reference, 11.0 Update_ (4th Edition). Boston : Allyn and Bacon.
*   <a id="gop97"></a>Gopnik, A. & Meltzoff, A. N. (1997). _Words, Thoughts, and Theories_. Cambridge, Ma: MIT Press.
*   <a id="hal96"></a>Hall, V., Chiarello, K. & Edmondson, B. (1996), Deciding where knowledge comes from depends on where you look, Journal of Educational Psychology, **88**, 305-313\.
*   <a id="hjo00"></a>Hjorland, B. (2000), Relevence research: The missing perspective(s): "Non-relevence" and "epistemological relevance", Journal of the American Society for Information Science, **51**(2), 209-211.
*   <a id="hjo02"></a>Hjorland, B. (2002). Epistemology and the Socio-Cognitive Perspective in Information Science. _Journal of the American Society for Information Science and Technology_, **53**(4), 257-270.
*   <a id="ho00"></a>Ho, R. (2000). _Handbook of Univariate and Multivariate Data Analysis and Interpretation: An SPSS Approach_. Rockhampton: Central Queensland University.
*   <a id="hof02"></a>Hofer, B. & Pintrich, P. (2002). _Personal Epistemology: The psychology of beliefs about knowledge and knowing_. Mahwah, NJ: Lawrence Erlbaum .
*   <a id="jhe93"></a>Jheng, J., Johnson, S. & Anderson, R. (1993). Schooling and student's epistemological beliefs about learning. _Journal of Contemporary Educational Psychology_, **18**, 23-35.
*   <a id="kar96"></a>Kardash, C. and Scholes, R. (1996). Effects of preexisting beliefs, epistemological beliefs, and need for cognition on interpretation of controversial issues. _Journal of Educational Psychology_, **88**, 260-271.
*   <a id="neu03"></a>Neuman, W. L. (2003). _Social Research Methods: Qualitative and Quantitative Approaches_. Boston: Allyn and Bacon.
*   <a id="per68"></a>Perry, W. G. (1968). _Patterns of development in thought and values of students in a liberal arts college: A validation of a scheme_. Cambridge, MA: Bureau of Study Council, Harvard University.
*   <a id="sch04"></a>Schommer-Aikins (2004). Explaining the epistemological belief system: Introducing the embedded systemic model and coordinated research approach._Educational Psychologist_, **39**(1), 19-29.
*   <a id="sch90"></a>Schommer-Aikins, M. (1990). Schommer epistemological questionnaire [for college students]. _The British Journal of Educational Psychology_, **82**(3), 498-504.
*   <a id="sch98"></a>Schommer-Aikins, M. (1998). The influence of age and schooling on epistemological beliefs. _The British Journal of Educational Psychology_, **68**, 551-562.
*   <a id="sch02"></a>Schommer-Aikins, M. (2002). An evolving theoretical framework for an epistemological belief system. In Hofer, B. and Pintrich, P.(Eds.), Personal Epistemology: The psychology of beliefs about knowledge and knowing . Mahwah, NJ: Lawrence Erlbaum.
*   <a id="sch05"></a>Schommer-Aikins, M. (2005). Personal communication (Colbeck, D.), via e-mail.
*   <a id="schr02"></a>Schraw, G., Bendixen, L. & Dunkle, M. (2002), Development and Validation of the Epistemic Belief Inventory (EBI)In Hofer, B. and Pintrich, P.(Eds.), Personal Epistemology: The psychology of beliefs about knowledge and knowing . Mahwah, NJ: Lawrence Erlbaum.
*   <a id="schr95"></a>Schraw, G., Dunkle, M. & Bendixen, L. (1995), Cognitive processes in well-defined and ill-defined problem solving., Applied Cognitive Psychology, **9**, 523-538.
*   <a id="schu06"></a>Schuman, L. and Ritchie, D. (2006). [How is Knowledge Processed?](http://www.gdrc.org/kmgmt/learning/learners.html) Retrieved 5 July, 2006 from http://www.gdrc.org/kmgmt/learning/learners.html
*   <a id="tol02"></a>Tolhurst, D. and Debus, R. (2002). Influence of prior knowledge, attitudes, ability, and activity structure on student's learning and use of software. Journal of Educational Computing Research, **27**(3), 275-313.
*   <a id="win98"></a>Windschitl, M. and Andre, T. (1998). Using computer simulations to enhance conceptual change: The roles of constructivist instruction and student epistemological beliefs. Journal of Research in Science Teaching, **35**, 145-160.
*   <a id="woo03"></a>Woodruff-Smith, D. (2003). [Phenomenology](http://plato.stanford.edu/entries/phenomenology/) Retrieved 19th February, 2007 from http://plato.stanford.edu/entries/phenomenology/