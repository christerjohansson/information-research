#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future". Poster abstract

# From novice to nutrition expert: exploration of nutrition expertise and information use of nutritionists

#### [Päivi Helminen](mailto:paivi.helminen@helsinki.fi)  
University of Helsinki, Finland

## Introduction.

Everyone is an expert of her/his own eating habits, and anyone can claim that he/she is a nutrition expert. Nutrition expertise is defined differently by different population or professional groups. Professions aim to control and enhance the legitimacy and the status of their profession ([Evetts 2006](#eve06)), although professions are losing their monopoly ([Evetts 2006](#eve06), [Kuhlmann 2006](#kuh06), [Schlesinger 2002](#sch02)). For the Finnish nutrition profession the interest organization is the Association of Clinical and Public Health Nutritionists in Finland, which requires that its members base their work on research information.

Despite the vast research on the expertise and information use of different professionals, little is known about the nutrition professionals' perspective of nutrition expertise and how expertise is related to their information use.

This study focuses on two key concepts: nutrition expertise and the use of information resources. This research will explore the nature of nutrition expertise and the use of information resources by different actors in the arena of the nutritional field: students, nutrition researchers, and nutrition practitioners. By studying both novices and experienced researchers and practitioners, it is possible to compare the nature and patterns of information use in the varying stages of the careers of the nutritionists. The goals of this study are to examine 1) the concept of nutrition expertise and how nutrition expertise is defined by different groups: nutrition students, nutrition researchers, and practicing nutritionists, 2) how the information use patterns of the nutrition students change during their studies, 3) how the information use patterns of students, researchers, and practitioners in the field differ, 4) what characterizes information use and information resources in nutrition.

## Method.

This study explores nutrition expertise through qualitative interviews with nutrition students and professionals:

1.  Fourteen nutrition students who were accepted to the nutritional sciences programme at the Faculty of Agriculture of Forestry, University of Helsinki, Finland, in fall 2005\. All seventeen students who were accepted to the programme were contacted and asked to participate. The students will be interviewed four times during their studies: the first autumn, the second autumn, after completing their Bachelors' thesis, and after completing their Masters' thesis.
2.  around ten researchers of the Department of Nutritional Sciences at the University of Helsinki, and
3.  10-15 nutrition practitioners who are not affiliated with the universities. Nutritionists will be contacted through The Association of Clinical and Public Health Nutritionists in Finland, which is the professional interest organization for nutritionists in Finland. All the members of the association have at least a Masters' degree in nutritional sciences.

A modified long interview approach will be used to collect data ([McCracken 1988](#mcc88)). The following themes will be explored in the qualitative interviews: information seeking skills, sources for nutrition information, and nutrition expertise and the profession of nutrition. The exact sample size for the nutrition researchers and practitioners cannot be determined yet because sampling will be continued "to the point of redundancy", which means that interviewing will be continued until the same themes start to reappear in new interviews. Qualitative researchers' estimates for sample sizes vary from eight for long interviews ([McCracken 1988](#mcc88)) to between twelve and twenty when the aim is to achieve maximum variation in the sample ([Kuzel 1992](#kuz92)).

## Analysis.

An emic perspective will be used in the research ([Pelto and Pelto 1978](#pel78)), which means eliciting meaning and perceptions from the participant's point of view while the researcher avoids imposing her beliefs or theoretical perspectives on the data. On the other hand, the researcher needs to be aware of her own perceptions and conceptions. I will use a grounded theory approach which is "a qualitative research method that uses a systematic set of procedures to develop an inductively derived grounded theory about a phenomenon" ([Strauss and Corbin 1998](#str98)). The theoretical framework will evolve from the data during the research, and I do not adhere strictly to previously developed theories which could get on the way of new discoveries ([Glaser 1998](#gla98)). However, after the analysis the grounded theory will be compared with the existing theories.

## Results.

So far, only the preliminary analysis of the interviews of the first year students and the second year students is available. The results indicate that according to the first year and the second year nutrition students, the following characterizes nutrition experts: 1) an ability to acquire information and stay up to date, and constant development, 2) an authorized education (a university degree in nutritional sciences), and 3) an appropriate, healthy look.

## Conclusions.

The ability to acquire information emerged as the most important characterizing factor of a nutrition expert, and this is a theme which will be explored in more detail later on.

## References

*   <a id="eve06"></a>Evetts, J. (2006). Introduction: Trust and professionalism: Challenges and occupational changes. _Current Sociology_, **54**(4), 515-531.
*   <a id="gla98"></a>Glaser, B.G. (1998). _Doing grounded theory : Issues and discussions_ (2nd ed.). Mill Valley, CA: Sociology Press.
*   <a id="kuh06"></a>Kuhlmann, E. (2006). Traces of doubt and sources of trust: Health professions in an uncertain society. _Current Sociology_, **54**(4), 607-620.
*   <a id="kuz92"></a>Kuzel, A.J. (1992). Sampling in qualitative inquiry. In B.F. Crabtree, & W.L. Miller (Eds.), _Doing qualitative research. research methods for primary care_, vol. 3.. Newbury Park, CA: Sage.
*   <a id="mcc88"></a>McCracken, G. (1988). _The long interview_. Newbury Park, Calif.: Sage.
*   <a id="pel78"></a>Pelto, P.J., & Pelto, G.H. (1978). _Anthropological research : The structure of inquiry_ (2th ed.). Cambridge: Cambridge University Press.
*   <a id="sch02"></a>Schlesinger, M. (2002). A loss of faith: The sources of reduced political legitimacy for the american medical profession. _Milbank Quarterly_, **80**(2), 185-235.
*   <a id="str98"></a>Strauss, A.L., & Corbin, J. (1998). _Basics of qualitative research : Techniques and procedures for developing grounded theory_ (2nd ed.). Thousand Oaks, CA: Sage.