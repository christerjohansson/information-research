#### Vol. 12 No. 4, October, 2007

* * *

## Proceedings of the Sixth International Conference on Conceptions of Library and Information Science—"Featuring the Future"

# Information professionals' attitude toward the adoption of innovations in everyday life

#### [Debbie L. Rabina and David J. Walczyk](mailto:drabina@pratt.edu)  
#### Pratt Institute, 144 W. 14th St., New York, NY 10011

#### Abstract

> **Introduction.** Willingness to adopt new technologies is key to their successful implementation in libraries. Libraries' commitment to information and communication technologies (ICTs) has traditionally been led by two complementary beliefs: first, that once new technologies are adopted, services to patrons will be improved; second, that after implementation is completed and the potential of a new ICTs has been achieved, the anticipated fiscal benefits and those associated with efficiency and productivity will begin to be realized. Frequently missing from this belief system is a consideration of the effect that employees' willingness to adopt new innovations might have on successful diffusion and the realization of these two beliefs.  
> **Method.** The study is based on a online questionnaire distributed to library and librarian related listervs and distributed during two weeks in April 2006\. 1,417 responses were received. Of these 1,128 were fully completed, considered valid, and used for inquiry. Of the valid responses, 88% were from the United States and 12% were international responses.  
> **Results.** Based on a survey of more than 1,000 librarians who answered an online questionnaire during April 2006, findings show a consistent deviation from the normal adopter innovativeness identified in previous diffusion research. The findings indicate that the majority of librarians fall into two contrasting adopter categories, regardless of demographic variables. Three problem areas for the diffusion of innovation among librarians were identified: the plethora of opinion leadership, deceleration in adoption, and improper re-invention.

## Introduction

There are probably few, if any, technological innovations relating to information and communication technologies (ICTs), from the telephone to the e-book, that have not found their way to libraries. Making sense of technological innovations and adapting them effectively into the library environment are seen as the two most important roles for librarians at the start of the 21st century ([Martell, 2003](#martell03)). Technological innovations are introduced to the library with the intention of providing better library service and increasing efficiency of library work ([Rubin, 2004](#rubin04)). The library profession attracts individuals of varying ages, varying degrees of experience, and a range of technological capabilities, all of which influence the way they perceive new information and communication technologies. It is the role of librarians to introduce new technologies to library users, instruct in their use, and help patrons recognize meaningful ways to benefit from ICTs. Therefore, understanding the attitude of librarians toward ICTs, and the factors that contribute to ICTs' perceived innovativeness, is helpful in planning, introducing, and incorporating ICTs successfully into the library.

The purpose of this study is to identify general characteristics and patterns that exist with regard to the innovativeness of librarians as it relates to the adoption of ICTs. Specifically, the study addresses the following questions: How innovative are librarians in relation to their peers in the adoption of information and communication technologies? Do demographic variables affect librarian innovativeness? What implications might the innovativeness of librarians have on the diffusion of innovations within libraries?

This type of user-centeredness has been a focus of research in library and information science for quite some time, yet the focus has traditionally been on the library patron and on information-seeking skills and behaviors. Unfortunately, librarians have not been actively acknowledged as users, and therefore research that focuses on librarians as adopters of ICTs is scarce. This study is driven by the recognition of the key role librarians play in the diffusion of innovation in the library, and by the recognition of the increased need to understand their behavior toward new ICTs in their everyday life as a precursor to their role as adopters and disseminators within the library setting.

## Literature review

Research regarding adoption, rejection, and dissonance of technology by librarians is scarce. Much of the research regarding innovativeness and diffusion of library technology has focused on library patrons such as faculty or students, and largely uses interviews and other qualitative methods of data collection ([Starkweather & Wallin, 1999](#starkweather99)). Research related to this study is research that examines librarians' innovativeness, their attitudes toward and beliefs about technology, and literature that places innovativeness in the context of adoption of technology. While some of the literature identifies opinions of librarians regarding the contribution of specific technologies to their work, it rarely provides much insight into attitudes toward ICTs.

### Librarians as users of technology

Most of the research focusing on librarians as users of technology investigates librarians' attitudes toward technology in general ([Chiang, 1986](#chiang86); [Kahan, 1997](#kahan97); [Ramzan 2004](#ramzan04); [Spacey Goulding and Murray 2003](#spacey03); [Stover 2000a](#stover00a)), or more specifically toward the Internet ([Curry & Harris 2000](#curry00); [Spacey, Goulding and Murray 2004](#spacey04); [Stover 2000](#stover00)). In recognition of the fact that innovativeness affects implementation of ICTs in libraries, staff training was used as an opportunity to examine attitudes of librarians ([Spacey et al., 2003](#spacey03)). Although Spacey et al. were primarily concerned with examining the effect of staff training on acclimating people to the changes around them, they contend in their 2003 paper that attitudes toward information and communication technology need to be further examined. The increasing dominance of the Internet as a reference tool in libraries generated several studies of attitudes of librarians toward the Internet, some using personal interviews and online open-ended questionnaires ([Curry & Harris, 2000](#curry00); [Stover, 2000](#stover00)) and others using established models such as the Technology Acceptance Model ([Spacey et al., 2004](#spacey04)). Curry and Harris ([2000](#curry00)) interviewed 14 subjects about their views of using the World Wide Web as a reference tool, and found that the majority of librarians interviewed stated that the Web had a positive effect on reference work. Stover used a qualitative survey to find patterns and themes among librarians with regard to their attitudes toward using the Web as reference tool. Stover discovered that most librarians see the Web as an important and productive tool, but suggested that the relatively small sample of 41 used for his study should be expanded ([Stover, 2000](#stover00)).

Studies that tried to identify factors predictive of information technology (IT) application in libraries include the librarians' level of technological knowledge and librarians' utilization ([Ramzan, 2004](#ramzan04)). Focusing solely on librarians in Pakistan, Ramzan found that knowledge of information technologies had a significant relationship with librarians' attitudes and with the likelihood they will use information technology ([Ramzan, 2004](#ramzan04)). A 2002 article by Woodard and Hinchliffe offers Rogers' diffusion of innovation theory as a framework for understanding the reasons and likelihood that librarians will incorporate technology into library instruction provided to patrons. The authors approach this question from a theoretical standpoint only and examine whether the theory of the diffusion of innovations is an appropriate one for comprehending the intricate relationship between adopter categories among library staff and the integration of technology into library instruction programs ([Woodard and Hinchliffe, 2002](#woodard02)). Another researcher focusing more on theoretical aspects of diffusion within libraries, but offering some case studies throughout, is Peter Clayton ([Clayton, 1997](#clayton97)). In his book Implementation of organizational innovation, Clayton focuses primarily on information retrieval as an example of the implementation of technological innovation in library settings. While the book is of value to many groups within the library environment, it is particularly helpful to library managers who want to learn more about implementation of innovations. Like Clayton, Pungitore is also concerned with innovation from an organizational point of view ([Pungitore, 1995](#pungitore95)). Her book Innovation and the library describes the spread of innovations within public libraries. Innovation is seen as the means of keeping the public library a viable institution and helping in the transition of the public library from an institution geared primarily at lower and middle class adults in the mid-nineteenth century, to the cultural institution it is today ([Pungitore, 1995](#pungitore95)).

A 2000 qualitative study solicited librarians' opinions on the role of technology in libraries ([Stover, 2000](#stover00)). Librarians were asked five questions, including whether they thought that libraries should be on the cutting edge in terms of technology. Responses varied greatly, and among the fears expressed were the steep learning curve required for some technologies ([Stover, 2000](#stover00)). Stover's study was not focused exclusively on use of the Web; rather, it examined librarians' attitudes in general toward managing technology and technological change. Stover found that with regard to introducing cutting-edge technology to the library, librarians hold a wide spectrum of opinions: Some are strong advocates, others have moderate opinions, and some are distrustful of using advanced technology in the library ([Stover, 2000](#stover00)).

Research that looked at innovativeness in libraries using a diffusion of innovation framework is infrequent. Two relevant studies, one by Starkweather and Wallin ([1999](#starkweather99)) and the other by White ([2001](#white01)), were identified. Starkweather and Wallin conducted focus group interviews with a total of 26 faculty members to study how they adopted new technology at a university library. The study yielded many interesting findings about the patterns of use of library technology by faculty interviewed; however, the researchers acknowledged that they were unable to link these patterns to adopter categories, perhaps due to their data collection method.

White ([2001](#white01)) examined digital reference service. She applied the diffusion of innovation framework to interpret the implementation of digital reference services within academic libraries. White found that the libraries that were early adopters of digital reference services tended to be larger, wealthier, had more staff, and provided more computer-based services than did libraries in other categories.

## Diffusion and adopter innovativeness

Researchers have applied many theoretical approaches to understand how people adopt innovations. A comprehensive examination of the various theoretical approaches is included in a review article by Dillon and Morris ([1996](#dillon96)), who contend that all models of adoption are part of the Rogers diffusion of innovation theory.

Since its publication as a methodology by Rogers in 1962, diffusion studies have become common in disciplinary and interdisciplinary research ([Ratzen, 2004](#ratzen04)). Rogers and subsequent researchers have used diffusion to analyze and explain individuals' and social systems' adoption of new and improved ideas, inventions, and practices from the perspective of those affected. Rogers ([2003](#rogers03)) identifies eight types of diffusion research earliness of knowing about innovations; rate of adoption of different innovations in a social system; opinion leadership; diffusion networks; rate of adoption in different social systems; communication channel usage; consequences of innovation; and adopter innovativeness.

In 1958, Rogers empirically standardized the innovativeness of individuals and social systems and organized it into adopter categories. Adopter categories are a descriptive method for classifying the members of a system based on their adoption of new innovations in relation to their peers. In a normal-frequency distribution, there are five adopter categories: innovators (2.5%), early adopters (13.5%), early majority (34%), late majority (34%), and laggards (16%).

Innovators, the earliest adopters, are the most entrepreneurial and likely to adopt a new technology before their peers. As tinkerers, this group tends to have an uncanny willingness to seek out new innovations that interest them. They have a high tolerance for learning new innovations on their own, and the result tends to be intrinsic, rather than peer-related, reward. They tend to be not as influential in the innovation-decision process as the next category.

Unlike innovators, early adopters tend to be interested in the extrinsic reward, the admiration that early adoption will bring from their peers. This category tends to have the highest level of opinion leaders. Because they tend to be opinion leaders, by their adoption or nonadoption early adopters serve as the tipping point for the eventual success or failure of new innovations.

As a large group, the early majority tend not to be opinion leaders. Yet their adoption decision represents the first mainstream adoption group and often legitimizes the innovation as positive and therefore adoptable. Adoption by the late majority, the fourth category, is often driven by either economic necessity or motivated by peer pressure ([Rogers 2003](#rogers03)). This group tends to accept innovation slowly and often requires formal or informal training or remedial assistance before full adoption.

Laggards represent the final adopter category. Laggards have a tendency to move "forward through the rearview mirror" and associate with like-minded peers. By looking to the past as a framework for coping with change, laggards tend to justify their opposition to new innovations by stating their continued happiness with things the way they currently are or have always been.

## Methodology and data collection

Since surveys are typically evaluative rather than generative, and provide broad results that can be projected to the total population, the researchers determined that an online questionnaire was the best method for gathering the most diverse data. The questionnaire was tested for reliability and validity with a sample population, then refined and retested with a different population and refined yet again. The final distributed questionnaire included two parts for a total of 39 questions. Part one consisted of eight questions that sought demographic data and the respondents' general everyday-life attitude toward the adoption of ICTs. Results from part one were used for this study on librarian innovativeness. Part two was used for a study on the relative advantage of innovations.

To ensure a wide distribution, identical e-mails were sent to library- and librarian-related listservs. The e-mail contained a brief introduction to the study and a link to the online questionnaire. The questionnaire was available for two weeks during late spring 2006.

In response to the survey, 1,417 responses were received. Of these 1,128 were fully completed, considered valid, and used for inquiry. Of the 1,128 valid responses 926 (88%) were from the United States and 202 (12%) were international responses. The U.S. responses were diverse, representing more than 300 distinct zip codes. The quantity and diversity of international responses were surprising and welcome. Surprising, because the survey was distributed to listservs intended for a U.S. audience, and welcome because this demonstrated the existence of wide "nets" cast by library- and librarian-related listservs.

To develop a general demographic picture of the respondent population, a few personal and career-oriented questions were asked. Two questions related to personal demographics. The first was the respondents' age, the second was the number of years they have worked in a library. Questions related to career also included two, the type of library environment respondents worked in, and their primary responsibility within that environment.

A single question was used to identify adopter category. Respondents were asked to self-identify which of five categories they fall under when adopting new technologies such as cellular phones and electronic gadgets. Category options available were: first on the block; among the first; just before everyone else; when everyone else does; last on the block. As per Rogers, adopter categories are associated with an individual's willingness to adopt new technological innovations and are, by extension, a measure of innovativeness. For this question, no library-specific technology was used to lead respondents. Instead, everyday-life technologies were referenced, and the respondents' likelihood of adopting these technologies in relation to "friends and colleagues" was assessed.

## Findings

Results revealed that the age of respondents increased consistently from the first group under 30 (9.8%) to 51 - 60 (35.1%). A major drop-off was observed in the number of respondents over age 60 (TABLE 1). This age distribution was consistent with the 1999 American Library Association (ALA) survey that demonstrated the existence of a "graying" of the profession ([Lynch, 2000](#lynch99)).

<table><caption>

**Table 1: Distribution of Respondents by Age**</caption>

<tbody>

<tr>

<th>Age</th>

<th># of responses</th>

<th>% of respondents</th>

</tr>

<tr>

<td>Under 30</td>

<td>110</td>

<td>9.8</td>

</tr>

<tr>

<td>30 - 40</td>

<td>245</td>

<td>21.7</td>

</tr>

<tr>

<td>41 - 50</td>

<td>273</td>

<td>24.2</td>

</tr>

<tr>

<td>51 - 60</td>

<td>396</td>

<td>35.1</td>

</tr>

<tr>

<td>Over 60</td>

<td>104</td>

<td>9.2</td>

</tr>

</tbody>

</table>

When asked the number of years working in a library, responses were well distributed over most time periods (Table 2). The exception, perhaps not surprisingly, was the sharp decline in the over 30 category.

<table><caption>

**Table 2: Distribution of Respondents by Years Working in Library**</caption>

<tbody>

<tr>

<th>Years</th>

<th># of respondents</th>

<th>% of respondents</th>

</tr>

<tr>

<td>0 - 5</td>

<td>240</td>

<td>21.3</td>

</tr>

<tr>

<td>6 - 10</td>

<td>218</td>

<td>19.3</td>

</tr>

<tr>

<td>11 - 20</td>

<td>287</td>

<td>25.4</td>

</tr>

<tr>

<td>21 - 30</td>

<td>253</td>

<td>22.4</td>

</tr>

<tr>

<td>Over 30</td>

<td>130</td>

<td>11.5</td>

</tr>

</tbody>

</table>

Two questions related to respondents' environment and their primary role within that environment. Responses to the type of environment revealed that the vast majority (more than 70%) worked in either public (38.7) or academic libraries (32.7).

<table><caption>

**Table 3: Distribution of Respondents by Type in Library**</caption>

<tbody>

<tr>

<th>Type</th>

<th># of respondents</th>

<th>% of respondents</th>

</tr>

<tr>

<td>Public</td>

<td>437</td>

<td>38.7</td>

</tr>

<tr>

<td>Academic</td>

<td>369</td>

<td>32.7</td>

</tr>

<tr>

<td>School</td>

<td>102</td>

<td>9.0</td>

</tr>

<tr>

<td>Corporate</td>

<td>51</td>

<td>4.5</td>

</tr>

<tr>

<td>Government</td>

<td>42</td>

<td>3.7</td>

</tr>

<tr>

<td>Special</td>

<td>127</td>

<td>11.3</td>

</tr>

</tbody>

</table>

Primary role responses (Table 4) revealed that the majority of respondents (again over 70%) were involved in one of two roles, public services (41%) or administration (31.4%). Of international responses 22% were for other. This high percentage was attributed to the different ways in which librarian roles are defined internationally.

<table><caption>

**Table 4: Distribution of Respondents by Professional Role in Library**</caption>

<tbody>

<tr>

<th>Role</th>

<th># of respondents</th>

<th>% of respondents</th>

</tr>

<tr>

<td>Public Services</td>

<td>462</td>

<td>41.0</td>

</tr>

<tr>

<td>Technical Services</td>

<td>141</td>

<td>12.5</td>

</tr>

<tr>

<td>Paraprofessional</td>

<td>52</td>

<td>4.6</td>

</tr>

<tr>

<td>Administration</td>

<td>354</td>

<td>31.4</td>

</tr>

<tr>

<td>Other</td>

<td>119</td>

<td>10.5</td>

</tr>

</tbody>

</table>

Table 5 shows the results for the adopter category question. More than 60% of respondents were in two categories: late majority (37%) and early adopters (24.6%). Following these were laggards (17.7%) and early majority (17.2%), which were just about equal. Lastly, innovators represented 3.6% of respondents. These results were surprising. In some instances they deviated considerably from the normal distribution frequently attributed to adopter categories. Aggregate data provided by Rogers (2003) suggests the following normal distribution of adopters based on their innovativeness: 2.5% innovators, 13.5% early adopters, 34% early majority, 34% late majority, and 16% laggards.

<table><caption>

**Table 5: Distribution of Respondents by rate of Adoption**</caption>

<tbody>

<tr>

<th>Adopter Category</th>

<th># of respondents</th>

<th>% of respondents</th>

</tr>

<tr>

<td>Innovators</td>

<td>40</td>

<td>3.6</td>

</tr>

<tr>

<td>Early Adopters</td>

<td>277</td>

<td>24.6</td>

</tr>

<tr>

<td>Early Majority</td>

<td>194</td>

<td>17.2</td>

</tr>

<tr>

<td>Late Majority</td>

<td>417</td>

<td>37.0</td>

</tr>

<tr>

<td>Laggards</td>

<td>200</td>

<td>17.7</td>

</tr>

</tbody>

</table>

As presented graphically in Figure 1, the distribution of librarians among adopter categories deviates substantially from the normal adopter distribution curve described by Rogers.

The comparison between Roger's normal distribution and the respondent data shows that late majority (+3.0%), laggards (+1.7%), and innovators (+1.1%) were consistent. Disparity was significant in two categories, early adopters (+11.1%) and early majority (-16.8%). When plotted, these findings reveal a chasm surrounded by two humps. The data suggests that innovations in library settings may be difficult to diffuse efficiently and effectively.

<figure>

![Figure 1](../colis12fig1.jpg)

<figcaption>

**Figure 1\. _Rate of Adoption of Technology_**</figcaption>

</figure>

Problems in the efficient and effective diffusion of innovations in libraries may begin early in the diffusion process, appearing as soon as early adopters come into contact with the innovation and then continuing, with compounding consequences, throughout the diffusion process. The unusually high percentage of early adopters, one-quarter of all respondents, may cause problems with opinion leadership, mainly too many opinion leaders promoting too many variant opinions. This problem then steamrolls into the next stage of adopters: early majority. Here the percentage of adopters was unusually low. The result is too many opinion leaders with too many opinions attempting to influence too few early majority. As one heads back up the far side of the chasm to the late majority, problems will likely continue. Here, though, one encounters the reverse of the previous problem: instead of too much influence there seems to be too little.

With these general findings in mind, the next step was to compare the general distribution of adopter categories with the demographic variables toward innovativeness. These findings reinforced the general distribution findings. Regardless of age range (Figure 2), the type of library environment (Figure 3), years working in a library (Figure 4), or primary role (Figure 5), one observes a chasm surrounded by two humps.

<figure>

![Figure 2](../colis12fig2.jpg)

<figcaption>

**Figure 2\. _Rate of Adoption by Age_**</figcaption>

</figure>

<figure>

![Figure 3](../colis12fig3.jpg)

<figcaption>

**Figure 3\. _Rate of Adoption by Type of Library_**</figcaption>

</figure>

<figure>

![Figure 4](../colis12fig4.jpg)

<figcaption>

**Figure 4\. _Rate of Adoption by Years in Library_**</figcaption>

</figure>

<figure>

![Figure 5](../colis12fig5.jpg)

<figcaption>

**Figure 5\. _Rate of Adoption by Role in Library_**</figcaption>

</figure>

## Discussion

Given the surprising and consistent findings presented above, how might innovation diffusion problems manifest themselves, regardless of specific demographic consideration? We identified three problem areas, of perhaps many: the plethora of opinion leadership, deceleration in adoption, and improper re-invention. Each of these problems lies farther along the diffusion process than the previous one. Therefore one can anticipate that they may be connected and build off one another.

### Diversity of impact of opinion leaders

Diffusion researchers have found that early adopters, rather than innovators, are the group that speeds up or slows down the diffusion of innovations. Early adopters are seen by their peers as the key influencers of attitudes, beliefs, and overt opinions about the value of new innovations. This means that most opinion leaders cluster within the early adopter category. As opinion leaders, early adopters tend to develop, test, and evaluate the benefits and side effects of an innovation within specific contexts of use (e.g., within the library where they work). If they perceive an innovation to be beneficial early adopters, through their opinion leadership, seek to trigger the critical mass of early majority adopters that is needed to encourage sustained diffusion.

Acknowledging early adopters as opinion leaders in the diffusion process, the data suggest that at least two serious problems may emerge in the diffusion of innovations into a library setting: Too many opinion leaders influencing too few early adopters.

Too many opinion leaders may create too many divergent opinions and fragment perceptions about the benefits of new innovations. This occurs not only in evaluating benefits but in deciding which competing innovations are evaluated at all. So at such an early point in the diffusion process, unusual complexities may be abnormally diverse. The problem is compounded by the sharp decline in the number of adopters from the early adopter to the early majority category. This chasm is problematic because it represents the amount of resistance that must be overcome for adoption to continue (see [Moore, 1991](#moore91)). Here too many opinion leaders are influencing and vying for the support from too few early majority. This makes the innovation-decision process unnecessarily complex, and in turn, time consuming, for the early majority. This problem may then instigate another problem, a deceleration in the rate of adoption.

### Deceleration in rate of adoption

As an innovation diffuses through adopter categories, the willingness of individuals to accept an innovation decreases. Therefore it is more challenging and requires more effort for an early majority to accept a new innovation than it is for an early adopter to do so. To alleviate this pressure, members of later adopter categories look to those individuals in the previous adopter category for guidance and support when adopting new innovations. A potential by-product of insufficient guidance from adopters from earlier adopter categories is a deceleration in the rate of adoption.

The data show an uncharacteristic dip between early adopters and early majority. The potential effect of this dip is that, due to lack of influence from their closest peer adopter category, the diffusion of innovations in library settings may decelerate rather than accelerate, as is usual. The tipping point is thus imperiled, and the effort needed to move toward and then through it may be quite high. When the diffusion of an innovation slows, or if the influence of previous adopter categories is simply too weak, another situation may arise: Re-invention.

### Re-invention

Re-invention is a third problem that tends to arise when adopter categories are unequally distributed or influence from previous adopter categories is weak. In much the same way that a messages changes each time it is passed on, as participants in the adoption and implementation process, individuals actively deviate from the intended use of new innovations. This deviation, whether intentional or unintentional, conscious or unconscious, is called re-invention.

Under normal diffusion, re-invention is considered both normal and healthy. It is normal in that new innovations should be flexible enough to adapt to the unique needs of the environment in which they are being implemented. It is healthy in that the potential for limited re-invention gives adopters a greater feeling, actual or perceived, of ownership and inclusion in the diffusion of the innovation. In order to accept an innovation, adopters make it their own and in making it their own, they change it to fit their perceived needs. Re-invention causes problems when innovations are adopted and used in unintended or discursive ways. A major cause of improper re-invention is partial or inaccurate knowledge or information of the innovations' intended or preferred use.

Data revealed a sharp decline from early adopters to early majority and then a sharp increase from early majority to late majority. This chasm, particularly as it relates to the rise up the far side, should cause concern for improper re-invention in libraries. Here, the late majority may lack the peer guidance, or peer pressure, to adopt innovations and use them in their intended ways. Problems may arise either psychologically or through use. Psychologically, adopters without sufficient guidance and thorough knowledge may use re-invention as a defense mechanism. Here, the issue is one of control. The adopters will accept the innovation, but only on their own terms. Through use, re-invention may occur when adopters do not perceive the intended relationships between themselves and the innovation. Rather than forming the required concomitant relationship, a fragmented one is formed.

## Conclusion and recommendations

Our study examined the innovativeness of librarians with regard to their willingness to adopt ICTs. While the responses to the survey drew an instantly recognizable pattern, one needs to keep in mind the limitations of this study. The results were self reported by respondents who were asked to define their innovativeness in relation to their peers. As with all self reporting, personal biases may taint the results. In addition, the method of distribution for this survey was unrestricted. Future research may try to triangulate the results with a controlled sample.

Using an online questionnaire and results from more than 1,000 respondents, distinct and consistent patterns were revealed. The data suggest that, when considered as a whole, librarians' attitude towards new innovations is unevenly distributed, with most either accepting of new innovations or being late adopters. These findings were consistent regardless of demographic variables. Age, role, tenure, and library type were shown to have little effect on librarians' attitude toward the adoption of ICT innovations.

The findings suggest that some of the conventional wisdoms regarding information professionals and their willingness to adopt new ICT innovations may be incorrect. Contrary to common beliefs, librarians in academic or special libraries are no more innovative than public or school librarians. Technical service librarians are not more innovative than public service librarians. Older librarians seem only a little less likely to accept innovations, and administrators appear to be no more innovative than the employees they supervise.

We discussed three concerns associated with the effects of adopter distribution on the successful diffusion of ICT innovations within libraries: lack of impact of opinion leaders, deceleration in the rate of adoption, and distortion during the re-invention process.

The unique effect of context on attitude and diffusion was not a variable in this study. Additional research using case studies of individual libraries should be completed. Further research also needs to take into consideration the advantages and disadvantages that librarians assign to specific innovations as they begin to diffuse into their working environment. This exploratory research identified the diffusion of innovation framework as a viable, and perhaps more people-centered, approach to investigating the implementation and acceptance of ICTs within libraries by the people who use them.

## References

*   <a id="chang86"></a>Chaing, K. S. (1986). The implications of the Matherson-Cooper report recommendation for the practicing science and technology librarians. _Implementation of organizational innovation_ **6**(4), 99-105
*   <a id="clayton97"></a>Clayton, P. (1997). _Implementation of organizational innovation_. San Diego: Academic Press
*   <a id="curry00"></a>Curry, A. & Harris, G. J.E. (2000). Reference librarians' attitudes towards the World Wide Web. _Public Library Quarterly_, **18**(2), 25-39
*   <a id="dillon96"></a>Dillon, A. & Morris, M. G. (1996). User acceptance of new information technology: theories and models. In M. Williams (ed.) _Annual review of information science and technology_, Vol. 31, pp. 3-32\. Medford, NJ: Information Today
*   <a id="kahan97"></a>Kahan, R. (1997). Attitudes of East Tennessee medical librarians about evolving computer information technology. _Tennessee Librarian_, **49**(1), 19-26
*   Lynch, M. J. (2000). What we now know about libraries. _American Libraries_, **31**(2), 8-9
*   <a id="martell03"></a>Martell, C. (2003). The role of librarians in the twenty-first century. In M.A. Drake (ed.) _Encyclopedia of library and information science_. New York: Marcel Dekker
*   <a id="moore91"></a>Moore, G. (1991). _Crossing the chasm: Marketing and selling technology products to mainstream customers_. New York, NY: Harper Business
*   <a id="pungitore95"></a>Pungitore, V.L. (1995). _Innovation and the library: The adoption of new ideas in public libraries_. Westport, CT: Greenwood Press
*   <a id="ramzan04"></a>Ramzan, M. (2004). Effects of IT utilization and knowledge on librarians' IT attitudes. _The Electronic Library_, **22**(5), 440-447
*   <a id="ratzen04"></a>Ratzen, S. C. (2004). Editor's note. _Journal of Health Communication_, **9**(1)
*   <a id="rogers62"></a>Rogers, E. M. (1962). _Diffusion of Innovations_. New York: Free Press
*   <a id="rogers03"></a>Rogers, E. M. (2003). _Diffusion of innovations_. New York: Free Press
*   <a id="rubin04"></a>Rubin, R. E. (2004). _Foundations of library and information science_. 2nd ed. New York: Neal-Schuman
*   <a id="spacey03"></a>Spacey, R., Goulding, A. & Murray, I. (2003). ICT and change in UK public libraries: Does training matter? _Library Management_, **24**(1/2) 61-69
*   <a id="spacey04"></a>Spacey, R., Goulding, A. & Murray, I. (2004). Exploring the attitudes of public library staff to the Internet using the TAM. _Journal of Documentation_, **60**(5), 550-564
*   <a id="starkweather99"></a>Starkweather, W. M. & Wallin, C. C. (1999). Faculty response to library technology: Insights on attitudes. _Library Trends_, **47**(4), 640-668
*   <a id="stover00"></a>Stover, M. (2000). Reference librarians and the Internet: A qualitative study. _Reference Service Review_, **28**(1), 39-46
*   <a id="stover00a"></a>Stover, M. (2000a). Technological concerns for library managers. _Library Management_, **21**(9), 472-482
*   <a id="white01"></a>White, M. D. (2001). Diffusion of an innovation: Digital reference service in Carnegie Foundation master's (comprehensive) academic institution libraries. _The Journal of Academic Librarianship_, **27**(3), 173-187
*   <a id="woodard02"></a>Woodard, B.S. & Hinchliffe, L.J (2002). Technology and innovation in library instruction management. _Journal of Library Administration_, **36**(1/2), 39-55