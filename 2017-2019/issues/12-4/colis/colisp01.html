<!DOCTYPE html>
<html lang="en">

<head>
	<title>Heterogeneity and homogeneity in LIS research</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="dc.title" content="Heterogeneity and homogeneity in LIS research">
	<meta name="dc.creator" content="Fredrik Åström">
	<meta name="dc.subject.keywords"
		content="organization of research fields, disciplinary crisis, science studies, meta-analyses">
	<meta name="robots" content="all">
	<meta name="dc.publisher" content="Professor T.D. Wilson">
	<meta name="dc.coverage.placename" content="global">
	<meta name="dc.type" content="text">
	<meta name="dc.identifier" scheme="ISSN" content="1368-1613">
	<meta name="dc.identifier" scheme="URI" content="http://InformationR.net/ir/12-4/colis/colisp01.html">
	<meta name="dc.relation.IsPartOf" content="http://InformationR.net/ir/12-4/colis/colis.html">
	<meta name="dc.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dc.date.available" content="2007-10-15">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information
		Science—&quot;Featuring the Future&quot;. Poster abstract</h2>
	<h1>Heterogeneity and homogeneity in library and information science research</h1>
	<h4 id="fredrik-%C3%A5str%C3%B6m"><a href="mailto:fredrik.astrom@lub.lu.se">Fredrik Åström</a></h4>
	<h4>Lund University Libraries, Head Office,<br>
		P.O. Box 134,<br>
		SE-221 00 Lund,<br>
		Sweden</h4>
	<h2 id="introduction">Introduction</h2>
	<p>Library and information science has been described as a field in crisis with a debated identity, lacking
		theoretical development and vulnerable to competition from other fields (e.g. (<a href="#hjo00">Hjørland
			2000</a>, <a href="#war01">Warner 2001</a>), a notion reflecting theories on 'fragmented adhocracies' (<a
			href="#fuc93">Fuchs 1993</a>, <a href="#whi00">Whitley 2000</a>). To better understand and face the
		challenges, a discussion on the challenges and perceived crisis is suggested. The focus of the discussion is on
		library and information science origins, perceptions and definitions, as well as contextual relations, related
		to the concept pairs 'homogeneity - heterogeneity' and 'integration - fragmentation'.</p>
	<h2 id="method">Method</h2>
	<p>The analysis is based on meta-analytical library and information science literature, with a certain emphasis on
		empirical investigations, where the findings of these previous studies are related to a set of theories on the
		organization of the sciences, primarily the framework developed by Whitley (<a href="#whi00">2000</a>); and to
		ideas by Gibbons <em>et al.</em> (<a href="#gib04">2004</a>) for analytical contrast.</p>
	<h2 id="results">Results</h2>
	<p>The development of contemporary library and information science can be viewed from three different perspectives.
		The first perspective is related to a disciplinary based view on the organization of the sciences, where library
		and information science can be characterized as a 'fragmented adhocracy' (<a href="#whi00">Whitley 2000</a>),
		heading for further fragmentation (<a href="#fuc93">Fuchs 1993</a>). An interpretation strongly related to the
		notion of library and information science as a field in crisis (<a href="#hjo00">Hjørland 2000</a>, <a
			href="#war01">Warner 2001</a>). This development can be seen in how the dual origin of the field has lead to
		great variations in terms of research orientations and scientific organization (<a href="#ast06">Åström
			2006</a>, <a href="#buc96">Buckland 1996</a>, <a href="#ray96">Rayward 1996</a>). Another aspect is the
		great variety of meta-studies, (<a href="#ast06">Åström 2006&gt;</a>), reflecting a diverse self-understanding,
		mirroring high levels of task uncertainty and low degrees of mutual dependency and reputational autonomy (<a
			href="#whi00">Whitley 2000</a>). This has implications for the internal organization of the field, as well
		as opening up for external competition, making it easier for external fields to have an impact on library and
		information science research, as well as on definitions of the field and its purposes. This can be seen in e.g.
		the large 'import' of ideas (<a href="#cro90">Cronin and Pearson 1990</a>), in competition from other fields on
		researching information related phenomena and in one extreme case: the use of the name information science for a
		department not including library and information science, but at the same university as a library and
		information science unit (<a href="#ast06">Åström</a>).</p>
	<p>There are, however, indications on an alternative line of development. Signs of library and information science
		research areas integrating can be seen, e.g., information retrieval and information seeking as well as
		information retrieval and informetrics (<a href="#ast07">Åström 2007</a>, <a href="#ing05">Ingwersen and
			Järvelin 2005</a>), suggesting a process of homogenization in at least some parts of the field; and
		contradicting Fuchs' (<a href="#fuc93">1993</a>) theories on the dynamics of research fields.</p>
	<p>The development of library and information science can also be seen from an alternative point of view,
		reinterpreting many of the characteristics of library and information science as a field in crisis into traits
		closely connected to the development of the sciences since 1945, formulated in terms of 'Mode 2' research (<a
			href="#gib94">Gibbons et al 1994</a>). From this point of view, the heterogeneous nature of library and
		information science does not signify fragmentation but interdisciplinarity, a trait further emphasized by an
		increase in cooperation between different information oriented fields in the form of e.g. information schools.
		Another aspect of the interdisciplinary trait is the shift of emphasis from competition and the protection of
		disciplinary boundaries to cooperation across disciplinary limits (<a href="#ast06">Åström 2006</a>). One
		important aspect of the 'Mode 2' research is how the organization of research not only stretches across
		disciplinary borders, but involves non-academic participants to a higher degree and a stronger emphasis on
		applications oriented research (<a href="#gib94">Gibbons et al 1994</a>). This makes library and information
		science characteristics such as the close connection to the field of practice, and a definition of the library
		and information science raison d'être oriented towards supporting the dissemination of relevant information,
		more valid as a basis for academic legitimacy.</p>
	<h2 id="conclusions">Conclusions</h2>
	<p>To understand the dynamics in current library and information science development and the notion of library and
		information science as a field in crisis, concept pairs such as 'homogeneity - heterogeneity' and 'integration -
		fragmentation' needs to be taken into account. Depending on what parts of the pairs are emphasized - and whether
		the development is seen from a disciplinary based or 'Mode 2' perspective on the organization of the science -
		how the development of library and information science is perceived differs significantly. The notion of library
		and information science in crisis can for instance be reinterpreted from the point of view of Gibbons et al <a
			href="#gib94">(1994)</a>, where an interdisciplinary and applications oriented organization of research
		where library and information science characteristics becomes a trait along the lines of modern scientific
		organization rather than a cause for concern in terms of academic legitimacy.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="ast06" name="ast06"></a>Åström, F. (2006). <em>The social and intellectual development of library and
				information science</em>. Umeå: Dept. of Sociology. Diss.</li>
		<li><a id="ast07" name="ast07"></a>Åström, F. (2007). Changes in the library and information science research
			front: timesliced co-citation analyses of library and information science journal articles, 1990-2004.
			<em>Journal of the American Society for Information Science and Technology</em>, <strong>58</strong>(7),
			947-957</li>
		<li><a id="buc96" name="buc96"></a>Buckland, M.K. (1996). Documentation, information science and library science
			in the U.S.A., <em>Information processing &amp; management</em>, <strong>32</strong>(1), 63-76</li>
		<li><a id="cro90" name="cro90"></a>Cronin, B. &amp; Pearson, S. (1990). The export of ideas from information
			science. <em>Journal of information science</em>, <strong>16</strong>, 381-391</li>
		<li><a id="fuc93" name="fuc93"></a>Fuchs, S. (1993). A sociological theory of scientific change. <em>Social
				forces</em>, <strong>71</strong>(4), 933-953</li>
		<li><a id="gib94" name="gib94"></a>Gibbons, M. <em>et al.</em> (1994). <em>The new production of scientific
				knowledge: the dynamics of science and research in contemporary society</em>. London: Sage</li>
		<li><a id="hjo00" name="hjo00"></a>Hjørland, B. (2000). Library and information science: practice, theory, and
			philosophical basis. <em>Information Processing &amp; Management</em>, <strong>36</strong>, 501-531</li>
		<li><a id="ing05" name="ing05"></a>Ingwersen, P. &amp; Järvelin, K. (2005). <em>The turn: integration of
				information seeking and retrieval in context</em>. Dordrecht: Springer</li>
		<li><a id="ray96" name="ray96"></a>Rayward, W.B. (1996). The history and historiography of information science:
			some reflections. <em>Information processing &amp; management</em>, <strong>32</strong>(1), 3-17</li>
		<li><a id="war01" name="war01"></a>Warner, J. (2001). W(h)ither information science?/! <em>Library
				Quarterly</em>, <strong>71</strong>(2), 243-255</li>
		<li><a id="whi00" name="whi00"></a>Whitley, R. (2000). <em>The intellectual and social organization of the
				sciences</em>. Oxford: University Press</li>
	</ul>

</body>

</html>