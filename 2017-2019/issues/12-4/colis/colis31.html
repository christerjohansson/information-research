<!DOCTYPE html>
<html lang="en">

<head>
	<title>Information as self-organized complexity; a unifying viewpoint</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta content="Information as self-organized complexity; a unifying viewpoint" name="dc.title">
	<meta content="David Bawden" name="dc.creator">
	<meta
		content="a unified concept of information as a form of self-organized complexity may be equally applicable to the physical, biological and human/social domains."
		name="dc.subject">
	<meta
		content="This short paper proposes that a unified concept of information as a form of self-organized complexity may be equally applicable to the physical, biological and human/social domains. This is seen as the evolutionary emergence of organized complexity in the physical universe, meaning in context in the biological domain, and understanding through knowledge in the human domain.This study is based on analysis of literature from a wide range of disciplines. This perspective allows for the possibility that not only may the library/information sciences be able to draw insights from the natural sciences, but that LIS research and scholarship may in turn contribute insights to these disciplines, normally thought of as more 'fundamental'."
		name="dc.description">
	<meta content="information, physical, biological, meaning, complexity" name="dc.subject.keywords">
	<meta content="all" name="robots">
	<meta content="Professor T.D. Wilson" name="dc.publisher">
	<meta content="global" name="dc.coverage.placename">
	<meta content="text" name="dc.type">
	<meta scheme="ISSN" content="1368-1613" name="dc.identifier">
	<meta scheme="URI" content="http://InformationR.net/ir/12-4/colis/colis31.html" name="dc.identifier">
	<meta content="http://InformationR.net/ir/12-4/colis/colis.html" name="dc.relation.IsPartOf">
	<meta content="text/html" name="dc.format">
	<meta content="en" name="dc.language">
	<meta content="http://creativecommons.org/licenses/by-nd-nc/1.0/" name="dc.rights">
	<meta content="2007-01-15" name="dc.date.available">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information
		Science—&quot;Featuring the Future&quot;</h2>
	<h1>Information as self-organized complexity: a unifying viewpoint</h1>
	<h4 id="david-bawden"><a href="mailto:db@soi.city.ac.uk">David Bawden</a></h4>
	<h4 id="city-university-london-ec1v-0hb">City University, London, EC1V 0HB</h4>
	<h4 id="abstract">Abstract</h4>
	<blockquote>
		<p><strong>Introduction.</strong> This short paper proposes that a unified concept of information as a form of
			self-organized complexity may be equally applicable to the physical, biological and human/social domains.
			This is seen as the evolutionary emergence of organized complexity in the physical universe, meaning in
			context in the biological domain, and understanding through knowledge in the human domain.<br>
			<strong>Method.</strong> This study is based on analysis of literature from a wide range of disciplines.<br>
			<strong>Conclusions.</strong> This perspective allows for the possibility that not only may the
			library/information sciences be able to draw insights from the natural sciences, but that library and
			information science research and scholarship may in turn contribute insights to these disciplines, normally
			thought of as more 'fundamental'.</p>
	</blockquote>
	<h2 id="introduction">Introduction</h2>
	<blockquote>
		<p>&quot;The seemingly empty space around us is seething with information. Much of it we cannot be aware of
			because our senses do not respond to it. Much of it we ignore because we have more interesting things to
			attend to. But we cannot ignore it if we are seeking a general theory of information. We cannot live only by
			reading and writing books&quot; (<a href="#bro80">Brookes 1980</a>: 132)</p>
	</blockquote>
	<p>This short paper presents an approach to a theoretical framework for understanding information in the physical,
		biological and human domains. It suggests that not only may an understanding of information in the physical and
		biological domains be helpful in dealing with the concerns of library and information science (LIS), but also
		that insights from LIS studies may shed light on the sciences usually thought of as more fundamental. It is
		based on the concept of information as a form of self-organizing complexity, which manifests as complex patterns
		in the physical world, meaning in context in the biological realm, and understanding through knowledge in the
		human domain.</p>
	<p>This short paper draws on a more extensive and more fully argued and referenced study (Organized complexity,
		meaning and understanding: an approach to a unified view of information for information science, Aslib
		Proceedings, in press 2007). This paper focuses on, and presents new ideas on, the way in which LIS scholarship
		and research may contribute to the understanding of information in other domains.</p>
	<p>Three authors in particular have proposed approaches to the idea of a unified view of information, albeit in very
		different ways: Tom Stonier, Andrew Madden, and Marcia Bates. Such ideas had been suggested before, notably by
		Brookes, and these pioneering suggestions have been developed by these three authors.</p>
	<p>Stonier (<a href="#sto90">1990</a>, <a href="#sto92">1992</a>, <a href="#sto97">1997</a>) made one of the first
		detailed attempts to unify the concept of information in the physical, biological and human domains. Starting
		from the concept of information as a fundamental constituent of the physical world, Stonier proposed relations
		between information and the basic physical quantities of energy and entropy, and suggested that a general theory
		of information may be possible, based on the idea that the universe is organized into a hierarchy of information
		levels. Stonier identified self-organizing information processing systems as the &quot;physical roots of
		intelligence&quot;, based on his conception of information as a basic property of the universe.</p>
	<p>Madden (<a href="#mad04">2004</a>) focused on the biological domain in his evolutionary treatment of information,
		examining information processing as a fundamental characteristic of most forms of life. He argued that
		Lamarckian evolution - the idea that characteristics acquired by a biological organizm during its lifetime can
		be passed on to their descendants - while discredited in general biology, may be appropriate for understand the
		evolution of human societies, including their information behaviour. Madden proposed for the first time so far
		as I am aware that insights from the information sciences may be valuable to the supposedly more 'basic'
		sciences, in this case the biological sciences, because of the commonality of the 'information' concept.</p>
	<p>Bates (<a href="#bat05">2005</a>), seeking like Stonier to reconcile the physical, biological and human forms of
		information, took the general definition that &quot;information is the pattern of organization of
		everything&quot;. All information is 'natural information', existing in the physical universe of matter and
		energy. 'Represented information' is either 'encoded' (having symbolic, linguistic or signal-based patterns of
		organization) or 'embodied' (encoded information expressed in physical form), and can only be found in
		association with living creatures. Beyond this, Bates defined three further forms of information: Information1 -
		the pattern of organization of matter and energy; Information 2 - some pattern of organization of matter and
		energy given meaning by a living being (or its constituent parts); - Knowledge: information given meaning and
		integrated with other contents of understanding.</p>
	<p>This paper builds upon these three approaches, to outline an approach to the expansion of a unified concept of
		information of relevance to information science. Following Stonier and Bates, tries to account for information -
		perhaps of different kinds - in the physical, biological and human domains, and to allow for the ideas of
		meaning, understanding, knowledge. Following all three authors, it will assume an evolutionary approach, which
		in an information science context, evokes Karl Popper's 'evolutionary epistemology' (<a href="#pop79">Popper
			1979</a>). Following Madden, it allows for the possibility that the insights of the library and information
		sciences may contribute to the development of the physical and biological sciences, in so far as information
		concepts are involved.</p>
	<h2 id="information-in-the-physical-domain">Information in the physical domain</h2>
	<p>In recent years, the role of information in physics, and the more general adoption of an 'information
		perspective' in the physical sciences has become much more widely accepted (<a href="#von04">von Baeyer
			2004</a>).</p>
	<p>There are three main areas of the physical sciences in which 'information' is widely regarded as a particularly
		important issue: the study of entropy; aspects of quantum mechanics, and the study of self-organizing systems. A
		very brief commentary on these must suffice to show the increasing recognition of the relevance of 'information
		concepts'.</p>
	<p>Entropy, a concept emerging from the development of thermodynamics in the nineteenth century is a measure of the
		disorder of a system (<a href="#pen04">Penrose 2004</a>, chapter 27). Given that order, or organization, is a
		quality generally associated with information, a qualitative link between information and entropy is evident;
		quantitatively, the 'information content' or 'entropy' in Shannon-Weaver information theory takes the same
		mathematical form as that of physical entropy <a href="#roe05">(Roederer 2005</a>, <a href="#lef90">Leff and Rex
			1990</a>, <a href="#lef03">2003</a>). Information may therefore, in this sense, be regarded as a kind of
		'negative entropy', an indication that it may indeed be a fundamental physical quantity.</p>
	<p>Quantum mechanics devised in the first years of the twentieth century, is the most successful physical theory yet
		developed, in terms of its ability to account accurately of experiments and observations. Interpreting it,
		however, and understanding what it 'means', is notoriously difficult. Intriguingly for our purposes, many of the
		interpretations available make some reference to information or knowledge (<a href="#pen04">Penrose 2004</a>,
		chapter 29).The physicist John A Wheeler, generally credited with initiating the trend to regard the physical
		world as basically made of information, with matter, energy, and even space and time, being secondary
		'incidentals' (<a href="#bar04">Barrow, Davies and Harper 2004</a>), has taken this approach farther than most,
		in insisting that 'meaningful information' is necessarily involved, and hence that 'meaning', in the mind of a
		conscious observer, in effect constructs the physical world: &quot;physics is the child of meaning even as
		meaning is the child of physics&quot;.</p>
	<p>Self-organizing systems are a topic of relatively recent interest, but are proving to be of importance in a
		variety of areas in the physical sciences (<a href="#dav87">Davies 1987</a>, <a href="#dav98">1998</a>). The
		interest in them comes from two perspectives. On the small-scale, it may be observed that simple physical and
		chemical systems show a propensity to 'self-organize': to spontaneously move towards a mode which is both
		organized and also highly complex. On the large scale, science must account for the emergence of highly complex
		organized structures - stars, galaxies, clusters of galaxies, and so on - in a universe which theorists assure
		us was entirely uniform and homogenous immediately after its creation. It is still not clear what the origins of
		this complexity are; it is generally assumed to come from gravitational effects, acting on very small
		inhomgeneities (<a href="#dav98">Davies 1998</a>, chapter 2). Gravity in the early universe can therefore be
		seen as &quot;the fountainhead of all cosmic organization .. triggering a cascade of self-organizing
		processes&quot; (<a href="#dav87">Davies 1987</a>, page 135).</p>
	<p>The ubiquitousness of self-organization has led some scientists to propose that there may be 'laws of
		complexity', such that the universe has an 'in-built' propensity to organize itself in this way; this view is
		far from generally accepted, but is gaining support.</p>
	<p>The relevance of these issues to information science is that any such complexity laws would be informational in
		character; that is to say they would act on the information content of the organization of matter and energy,
		tending to its increase. This would therefore form the basis of any unified view of information, rooted in its
		emergence in the physical world.</p>
	<h2 id="information-in-the-biological-domain">Information in the biological domain</h2>
	<p>The 'informatisation' of biology has been a remarkable of feature of science over the past decades, from the
		elucidation of the genetic code in 1953 to the sequencing of the human genome exactly 50 years later, and
		accompanied by a consequent detailed understanding of the ways in which information is passed through
		generations of living creatures. The concepts of information theory have been extensively applied to biological,
		and specifically genetic, information from a relatively early stage (<a href="#gat72">Gatlin 1972</a>). These
		arguments follow on from those relating to the physical world, in terms of increasing levels of organized
		complexity and information content, the latter generally understood in term's of Shannon's formalism and its
		successors (<a href="#ave03">Avery 2003</a>, <a href="#yoc05">Yockey 2005</a>).</p>
	<p>With the increasing emphasis on the understanding of genetic information is the tendency to describe life itself
		as an informational phenomenon. Rather than defining living things, and their differences from non-living, in
		terms of arrangements of matter and energy, and of life processes - metabolism, reproduction, etc. - it is
		increasingly usual to refer to information concepts. Life, thought of in these terms, is the example of
		self-organized complexity par excellence. But with life comes a change from the organized complexity in the
		physical universe: with life we find the emergence of meaning and context. The genetic code, for example, allows
		a particular triplet of DNS bases to have the meaning a particular amino acid is to be added to a protein under
		construction; but only in the context of the cell nucleus.</p>
	<p>It has also become clear that the origin of life itself may best be viewed as an 'information event': the crucial
		aspect is not the arrangement of materials to form the anatomy of a living creature, nor the beginning of
		metabolic processes; rather it is the initiation of information storage and communication between generations
		which marks the origin of life (<a href="#dav98">Davies 1998</a>, chapters 2 and 3).</p>
	<h2 id="information-in-the-human-domain">Information in the human domain</h2>
	<p>Here we move to the sort of 'information' most familiar in the library / information sciences: communicable
		recorded information, produced by humans in order to convey what Popper terms 'objective knowledge'. In addition
		to the organized complexity and meaning in context of the physical and biological domains, we have conscious
		participants with an internal mental comprehension of knowledge and an ability to make it explicit, leading to
		patterns of information behaviour which are certainly both organized and complex.</p>
	<p>Even in this familiar domain, there is some controversy about the best way to unnderstand information.
		Information may be regarded as an entity or 'thing' (<a href="#buc91">Buckland 1991</a>), as a cognitive
		attribute of the individual mind (<a href="#bel90">Belkin 1990</a>), or as something created collaboratively (<a
			href="#tal06">Talja, Tuominen and Savolainen 2006</a>). There is a particular issue of how information is to
		be understood to relate to similar entities, most particularly knowledge; see Meadow and Yuan (<a
			href="#mea97">1997</a>) and Chaim (<a href="#cha06">2006</a>).</p>
	<p>Floridi (<a href="#flo05">2005</a>), an exponent of a new interest in the 'philosophy of information' within the
		discipline of philosophy itself, recasts the idea of knowledge as 'justified, true belief' into the idea that
		information is 'well-formed, meaningful and truthful data'. This seems more suitable for the needs of
		information science, but does not reflect the rather muddled reality of the human record. Perhaps the most
		interesting philosophical approach is that of Kvanvig (<a href="#kva03">2003</a>), who argues that we should
		replace 'knowledge' with 'understanding' as a focus for interest. Understanding, for Kvanvig, requires &quot;the
		grasping of explanatory and other coherence-making relationships in a large and comprehensive body of
		information&quot;. It allows for there to be greater or lesser degrees of understanding, rather than just having
		knowledge/information or not. Crucially, it allows for understanding to be present even in the presence of
		missing, inconsistent, incompatible, and downright incorrect, information. It is firmly based on the idea of
		meaning in context, basic to biological information, and therefore underlying human information, which must
		build on the biological foundation. This seems to be a more appropriate entity than the philosophers'
		traditional ideas of knowledge for LIS.</p>
	<p>We can therefore see human information, characterised as understanding through knowledge, as a further stage in
		the emergence of self-organized informational complexity.</p>
	<h2 id="information-in-three-domains-a-unified-view">Information in three domains; a unified view ?</h2>
	<p>This paper argues that information may be seen in the physical domain as patterns of organized complexity of
		matter and energy; in the biological domain, meaning-in-context emerges from the self-organized complexity of
		biological organizms; in the human domain, understanding emerges from the complex interactions of Popper's World
		2, the mental product of the human consciousness, with World 3, the social product of recorded human knowledge.
		The term 'emerges' is used deliberately, for these are emergent properties, that is to say they appear
		appropriate to their level: physical, biological, conscious and social.</p>
	<p>The linking thread, and the unifying concept of information here, is self-organized complexity. The crucial
		events which allow the emergence of new properties are: the origin of the universe, which spawned organized
		complexity itself; the origin of life, which allowed meaning-in-context to emerge; and the origin of
		consciousness, which allows self-reflection, and the emergence of understanding, at least partly occasioned when
		the self reflects on the recorded knowledge created by other selves.</p>
	<p>If, therefore, we understood these three origins fully, we would, presumably, understand information itself
		equally fully, and the ways in which its various forms emerged. Sadly, the beginnings of the universe, of life,
		and of consciousness, are among the most deep and difficult problems for science (<a href="#gle04">Gleiser
			2004</a>).</p>
	<h2 id="reflecting-back-lis-insights-for-the-fundamental-sciences">'Reflecting back': LIS insights for the
		fundamental sciences</h2>
	<p>The framework described above is an evolutionary one, with meaning in context and understanding through knowledge
		emerging from the self-organized complexity of the physical universe. If this is accepted, then it seems clear
		enough that an understanding of complexity processes in the physical and biological realms might be valuable in
		understanding the issues of LIS.</p>
	<p>The converse - that the findings of LIS research may be valuable in understanding the emergence of complexity in
		the physical and biological realms - may also be true, in two ways.</p>
	<p>Most straightforwardly, we might expect that 'complexity laws', governing the ways in which self-organized
		complexity emerges, and what results from it, may - at the least - take the same general form in all situations
		and environments. Insights gained in the realm of the communication of human information might therefore be
		valuable for those studying the same general phenomena in the physical and biological sciences. It may be that
		the added richness and levels of complexity found with human information may make the identification of laws and
		concepts somewhat easier than in the 'sparser' environments of the natural sciences. Whether such laws and
		concepts would be directly applicable and relevant at all levels, or whether they would be emergent properties
		applicable on at their own levels remains to be seen; but in either case they would be a genuine contribution to
		the study of the supposedly more fundamental sciences.</p>
	<p>More ambitiously, there has been a trend in science, following the so-called 'strong anthropic principle', to
		conjecture that the emergence of life and consciousness may, in some ill-understood way, have an effect of
		backward causation, so as to affect the nature of the universe which have rise to it. The analogy for our
		purposes would be to allow the possibility that the emergence of human information, knowledge and understanding
		is in itself a force in the physical universe, which can influence the generation of complexity in all domains.
		This is an intriguing speculation, but it is not necessary to accept it in order to believe that LIS studies may
		have some value for the understanding of self-organization and complexity in other domains.</p>
	<p>We may then want to ask the basic question: what kind of LIS studies or concepts could be of value in this way ?
		This question has not been considered in detail, still less answered. But it seems clear that that they must be
		studies of the emergence of patterns within the recording and communication of human knowledge. Example might
		be: bibliometric, webliometric and scientometric analyses of publication; studies of emergent networks of
		information transfer; and studies of information seeking, and more general information behaviour, with an
		emphasis on the kind of patterns of behaviour which may be observed.</p>
	<h2 id="conclusions">Conclusions</h2>
	<p>Adoption of the unifying concept of information as self-organized complexity allows for research and study
		linking this concept in several domains, from the physical to the social, and allows the possibility that
		library and information science research may provide insights for the physical and biological sciences.</p>
	<h2 id="acknowledgements">Acknowledgements</h2>
	<p>I am grateful to Jack Meadows, Jutta Haider, Toni Weller, and an anonymous referee for helpful suggestions.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="ave03"></a>Avery, J., (2003), <em>Information theory and evolution</em>, Singapore: World Scientific
			Publishing</li>
		<li><a id="bar04"></a>Barrow, J.D., Davies, P.C.W. and Harper, C.L. (eds.), (2004), <em>Science and ultimate
				reality</em>, Cambridge: Cambridge University Press</li>
		<li><a id="bat05"></a>Bates, M.J., (2005), Information and knowledge: an evolutionary framework, <em>Information
				Research</em>, <strong>10</strong>(4), paper 239, available from
			http://informationr.net/ir/10-4/paper239.html</li>
		<li><a id="bel90"></a>Belkin, N., (1990), The cognitive viewpoint in information science, <em>Journal of
				Information Science</em>, <strong>16</strong>(1), 11-15</li>
		<li><a id="bro80"></a>Brookes, B.C., (1980), The foundations of information science: Part 1: Philosophical
			aspects, <em>Journal of Information Science</em>, <strong>2</strong>(3/4), 125-133</li>
		<li><a id="buc91"></a>Buckland, M., (1991), Information as thing, <em>Journal of the American Society for
				Information Science</em>, <strong>42</strong>(5), 351-360</li>
		<li><a id="cha06"></a>Chaim, Z., (2006), Redefining information science: from &quot;information science&quot; to
			&quot;knowledge science&quot;, <em>Journal of Documentation</em>, <strong>62</strong>(4), 447-461</li>
		<li><a id="dav98"></a>Davies, P., (1998), <em>The fifth miracle: the search for the origin of life</em>, London:
			Penguin</li>
		<li><a id="dav87"></a>Davies, P., (1987), <em>The cosmic blueprint: order and complexity at the edge of
				chaos</em>, London: Penguin</li>
		<li><a id="flo05"></a>Floridi, L., (2005), Is semantic information meaningful data ?, <em>Philosophy and
				Phenomenological Research</em>, <strong>70</strong>(2), 351-370 [available from
			http://www.philosophyofinformation.net/pdf/iimd.pdf]</li>
		<li><a id="gat72"></a>Gatlin, L.L., (1972), <em>Information theory and the living system</em>, New York NY:
			Columbia University Press</li>
		<li><a id="gle04"></a>Gleiser. M., (2004), The three origins: cosmos, life, and mind, in <em>Science and
				ultimate reality</em>, J.D. Barrow, P.C.W. Davies, and C.L. Harper (eds.), Cambridge: Cambridge
			University Press, pages 637-653</li>
		<li><a id="kva03"></a>Kvanvig, J.L., (2003), <em>The value of knowledge and the pursuit of understanding</em>,
			Cambridge: Cambridge University Press</li>
		<li><a id="lef90"></a>Leff, H.S. and Rex, A.F. (1990), <em>Maxwell's demon: entropy, information,
				computing</em>, Bristol: Adam Hilger</li>
		<li><a id="lef03"></a>Leff, H.S. and Rex, A.F. (2003), <em>Maxwell's demon 2: entropy, classical and quantum
				information, computing</em>, Bristol: Institute of Physics Publishing</li>
		<li><a id="mad04"></a>Madden, A.D, (2004), Evolution and information, <em>Journal of Documentation</em>,
			<strong>60</strong>(1), 9-23</li>
		<li><a id="mea97"></a>Meadow, C.T. and Yuan, W., (1997), Measuring the impact of information: defining the
			concepts, <em>Information Processing and Management</em>, <strong>33</strong>(6), 697-714</li>
		<li><a id="pen04"></a>Penrose, R. (2004), <em>The road to reality</em>, London: Jonathan Cape</li>
		<li><a id="pop79"></a>Popper, K.R., (1979), <em>Objective Knowledge: an evolutionary approach (revised
				edition)</em>, Oxford: Oxford University Press</li>
		<li><a id="roe05"></a>Roederer, J.G., (2005), <em>Information and its role in nature</em>, Berlin: Springer
			Verlag</li>
		<li><a id="sto90"></a>Stonier, T., (1990), <em>Information and the internal structure of the universe: an
				exploration into information physics</em>, London: Springer-Verlag</li>
		<li><a id="sto92"></a>Stonier, T., (1992), <em>Beyond information: the natural history of intelligence</em>,
			London: Springer-Verlag</li>
		<li><a id="sto97"></a>Stonier, T., (1997), <em>Information and meaning: an evolutionary perspective</em>,
			London: Springer-Verlag</li>
		<li><a id="tal06"></a>Talja, S., Tuominen, K., and Savolainen, R., (2006), &quot;Isms&quot; in information
			science: constructivism, collectivism and constructionism, <em>Journal of Documentation</em>,
			<strong>61</strong>(1), 79-101</li>
		<li><a id="von04"></a>Von Baeyer, C., (2004), <em>Information: the new language of science</em>, Harvard MA:
			Harvard University Press</li>
		<li><a id="yoc05"></a>Yockey, H.P., (2005), <em>Information theory, evolution and the origin of life</em>,
			Cambridge: Cambridge University Press</li>
	</ul>

</body>

</html>