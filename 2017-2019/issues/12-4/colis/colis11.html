<!DOCTYPE html>
<html lang="en">

<head>
	<title>What's in a turn?</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<link rev="made" href="mailto:t.d.wilson@shef.ac.uk">
	<meta name="dc.title" content="What's in a turn?">
	<meta name="dc.creator" content="Jan Nolin">
	<meta name="dc.subject"
		content="Information science is discussed as a divergent research field in relation to various cognitive interruptions that are promoted as 'turns'.  These often imply a movement into a more convergent research field.">
	<meta name="dc.description"
		content="The concepts of 'divergent' and 'convergent' research fields are utilised to discuss the strategy of introducing 'turns' in information science. This is a discussion paper which uses conceptual analyses in combination with a discussion of research ideals. Information science is a divergent research field which includes many subfields with diverse perspectives.  They are influenced by different schools of thought and when 'turns' are introduced, there will be many attempting to turn the field in different directions. Different turns have various implications for the research field.  The metaphor of 'the turn' is highly flexible and the research field can be bent in many different ways and in a more or less fundamental manner.  The situation of information science being a relatively small research field, including many subdisciplines and perspectives serves as a setting for a divergent field with many turns in different directions.  However, several ideas that address a more fundamental turn, that would make the field more convergent, is discussed in the article. An image of a stronger more convergent research field is often a hidden implication of a turn. Perhaps strategies of introducing different 'turns' need to be connected to a position on what kind of research field information science should be.  Do we want a field with many minor turns or do we want to move over to a more convergent research field and turn as one unit?">
	<meta name="dc.subject.keywords"
		content="cognitive viewpoint, turn, divergent, convergent, information science, research methods">
	<meta name="robots" content="all">
	<meta name="dc.publisher" content="Professor T.D. Wilson">
	<meta name="dc.coverage.placename" content="global">
	<meta name="dc.type" content="text">
	<meta name="dc.identifier" scheme="ISSN" content="1368-1613">
	<meta name="dc.identifier" scheme="URI" content="http://InformationR.net/ir/12-4/colis/colis11.html.">
	<meta name="dc.relation.IsPartOf" content="http://InformationR.net/ir/12-4/colis/colis.html">
	<meta name="dc.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dc.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dc.date.available" content="2007-10-15">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="vol-12-no-4-october-2007">Vol. 12 No. 4, October, 2007</h4>
	<h2>Proceedings of the Sixth International Conference on Conceptions of Library and Information
		Science—&quot;Featuring the Future&quot;</h2>
	<h1 id="whats-in-a-turn">What's in a turn?</h1>
	<h4 id="jan-nolin"><a href="mailto:jan.nolin@hb.se">Jan Nolin</a></h4>
	<p>The Swedish School of Library and Information Science,<br>
		University College of Borås,<br>
		Sweden</p>
	<h4 id="abstract">Abstract</h4>
	<blockquote>
		<p><strong>Introduction.</strong> The concepts of &quot;divergent&quot; and &quot;convergent&quot; research
			fields are utilised to discuss the strategy of introducing &quot;turns&quot; in information science.<br>
			<strong>Method.</strong> This is a discussion paper which uses conceptual analyses in combination with a
			discussion of research ideals.<br>
			<strong>Analysis.</strong> Information science is a divergent research field which includes many subfields
			with diverse perspectives. They are influenced by different schools of thought and when &quot;turns&quot;
			are introduced, there will be many attempting to turn the field in different directions.<br>
			<strong>Results.</strong> Different turns have various implications for the research field. The metaphor of
			&quot;the turn&quot; is highly flexible and the research field can be bent in many different ways and in a
			more or less fundamental manner. The situation of information science being a relatively small research
			field, including many subdisciplines and perspectives serves as a setting for a divergent field with many
			turns in different directions. However, several ideas that address a more fundamental turn, that would make
			the field more convergent, is discussed in the article.<br>
			<strong>Conclusions.</strong> An image of a stronger more convergent research field is often a hidden
			implication of a turn. Perhaps strategies of introducing different &quot;turns&quot; need to be connected to
			a position on what kind of research field information science should be. Do we want a field with many minor
			turns or do we want to move over to a more convergent research field and turn as one unit?</p>
	</blockquote>
	<h2 id="introduction">Introduction</h2>
	<p>Becher and Trowler (<a href="#bec01">2001</a>), makes an interesting distinction, noting that the boundaries of a
		research field can be convergent, signifying a homogeneous research community that sets up a very clear boundary
		which therefore is easy to defend. Contrary to this, other research communities may work with divergent
		boundaries, which are much less clear and more difficult to defend. In this case, many researchers colonise
		areas very close to the border and may frequently cross over to the other side. This characterisation of the
		divergent research field seem to fit information science which is often said to be a multidisciplinary
		discipline, (Saracevic, <a href="#sar99">1999</a>, White and McCain, <a href="#whi98">1998</a>). Machlup and
		Mansfield (<a href="#mac83">1983</a>) suggested that one should speak about &quot;the information
		sciences&quot;, similar to the way one speaks of the social sciences. Such an approach would allow for
		heterogeneity and would make questions of an overall identity much less important. Indeed, the implication of
		this idea is that information science consists of a number of subdisciplines which just happened to be grouped
		together through a series of arbitrary historical circumstances. Others subfields or research areas could with
		equal logic have been part of the family. Working with such a perspective one could conclude that information
		science does not have a common identity and all discussions on similar topics (paradigms, boundaries, key
		notions etc) are otiose.</p>
	<p>It is possible to make a very different interpretation of the dilemma posed by Machlup and Mansfield (<a
			href="#mac83">1983</a>), that since information is everywhere, then every research field could be said to be
		information science. It could be argued that this dilemma is not unique for information science, but instead a
		feature that has enabled strong disciplines to build perspectives and models applicable to wide range of
		phenomena. Sociologists say that social situations are everywhere. Linguists argue the primacy of language etc.
	</p>
	<p>Still another alternate way of conceptualising an identity for information science is to utilise the metaphor of
		a core. As soon as one starts talking about connecting the subfields, then models of hierarchical structure are
		introduced. Which subfields are the most important? Are some more and some less central? Does there exist a core
		either in the form of a subfield or in the form of shared values/theories/ideas? If the model of a core is
		accepted, we may query: what is the relationship between the core and the other parts of the discipline? In what
		ways does it facilitate communication and the creation of an identity? It is not uncommon within information
		science to suggest that information retrieval serves or has served the role of a core (<a href="#ell99">Ellis
			<em>et al.</em> 1999</a>, <a href="#sar92">Saracevic 1992</a>).</p>
	<p>However, there are ways of conceptualising the subfields of information science in yet another way. In a
		co-citation analyses of information science, White and McCain (<a href="#whi98">1998</a>) come to the important
		conclusion that: &quot;<em>information science lacks a strong central author, or group of authors, whose work
			orients the work of others across the board. The field consists of several specialties around a weak
			center</em>.&quot; (343)</p>
	<p>We would like to suggest that that all these variations in identifying information science are actually united in
		a way that they articulate a divergent research field. Taking this as a starting point, one might ask how this
		situation affects research practice within information science. In this article, we will pursue one particular
		dimension: &quot;the turn &quot;, which can be characterised as some sort of cognitive interruption within a
		research tradition. It must be emphasised that this very broad definition allows for several more precise
		interpretations and one purpose of this article is to identify some of these and compare them to each other as
		well as to the cognitive needs of information science. Our line of thinking is that in the research field that
		is divergent, with less of a common identity, it will be relatively easily influenced by changes in stronger,
		trendsetting disciplines. As they turn, we tend to turn with them. Furthermore, as information science is such a
		heterogeneous research area, influenced by so many and so very different strong research traditions, there will
		be many turns in many different ways. Ultimately, we feel that the articulation of the turn is a cognitive act
		with the intent to make the research field stronger, more mature, more congruent and one step closer to a clear
		identity.</p>
	<p>In this article, it is suggested, therefore, that the divergent field such as information science is a lively
		breeding ground for many different attempts at turning the field toward various perspectives. However, we will
		close this article with a discussion on an attempt to pursue the idea of a major turn, (Ingwersen &amp;
		Järvelin, <a href="#ing05">2005</a>) where the authors have an ambition to radically integrate major traditions
		within the field and as a consequence convert information science to more of a convergent research field. Such a
		strategy is also consistent with a vision of information science becoming more mature, less influenced by other
		research fields and more involved with integrating the different specific research traditions that primarily
		have an identity within information science.</p>
	<p>To name something &quot;a turn&quot; is often a rhetorical act. It is a way of bringing attention to a new way of
		thinking. Sometimes this act is more serious than at others times. The turn can be presented by a single author
		and it can proceed with very few, even no, followers. At other times it can be launched with great power, with
		influential researchers who already have established a network of followers.</p>
	<p>Social status within the research field therefore matters, as does the intent of the researcher or researchers
		that propose a turn. However, another complication is that sometimes a move is made that could quite likely be
		named a turn, but it is instead called a new paradigm, a new perspective or new viewpoint.</p>
	<h2 id="the-historical-turn-the-search-for-an-identity">The historical turn: the search for an identity</h2>
	<p>In a review of research on the history of information science in <a href="#buc96">1995</a>, Buckland and Liu
		conclude that information science for decades have been ahistorical and &quot;much of the historical commentary
		has been anecdotal, superficial, or uncritical&quot; (400). However, they were optimistic about the future and
		indeed the history of information science has been a practice on the rise for the last 15 years. Rayward even
		talks about not one, but two &quot;historical turns&quot; (Rayward, <a href="#ray04">2004</a>). The first one of
		these concerns the practice of utilising historical accounts to &quot;reduce philosophical confusion&quot;,
		while the second is focused on the role of various interests in the development of the research field.</p>
	<p>In reflecting on these kinds of statements, we instantly encounter a number of alternative notions of &quot;the
		turn&quot;. In itself, the transformation of a field moving from limited historical reflection to quite a lot is
		a kind of cognitive interruption. It is a turn toward more historical reflection. However, it need not mean that
		we turn away from something else, it could just mean that the new strand of thinking has grown thicker. By
		introducing the idea of a historical turn, Rayward is suggesting something more, that the historical reflection
		constitutes a resource that information science need in order to become more congruent and be able to move
		forward. In other words, mainstream information scientists need to turn toward and reflect on the historical
		research in order to better understand their own practice. This kind of turn is therefore nothing that
		transforms mainstream researchers in the way that they include an historical perspective in everything they do,
		rather it suggests that they turn their interests to what historians are doing.</p>
	<p>In evaluating the merits of this idea, it would seem reasonable to ask if such a recommendation is more urgent
		for information science than for other research fields? Based on the idea of information science being a
		divergent research field, we would be inclined to say an emphatic &quot;yes&quot;. We could add more arguments
		to this affirmation. Rayward (<a href="#ray96">1996</a>) suggests that the history of information science
		encounters some very specific historiographic problems. One of these is that information in itself is such a
		wide and abstract concept with a multitude of different connotations. This is very different to the historical
		study of phenomenon such as geological structures, music or human rights.</p>
	<p>As becomes obvious in this kind of discussion, historical reflection and reflections on disciplinary identity are
		very closely related. As Rayward (<a href="#ray05">2005</a>) points out, asking questions about the identity of
		a subject is to ask historically contingent questions. One can once again claim that information science serves
		us a specific twist on the relationship between a subject and historical reflection on the subject.</p>
	<p>This line of reasoning brings us, again, to the perhaps unsettling conclusion that both information and history
		are flexible, conceptual monsters which can be applied to any practice. Still, the sophisticated researcher must
		have some kind of grasp of both in order to do the work. You must know your identity in order to know where to
		turn. Therefore, it might be suitable to characterise the historical turn as the mother of other turns. The
		researcher must know the history of the tradition in order to judge what kind of cognitive interruption is
		feasible given what has been done before. Similar arguments can be made for other research traditions that
		basically serve to strengthen the identity of the research.</p>
	<h2 id="the-reflexive-turn-identifying-weakness">The reflexive turn: identifying weakness</h2>
	<p>A turn can be about strengthening the research field, but it can also be about weakening the field. In arguing
		for a new place to go, a place of cognitive strength, it is common to identify weaknesses in established
		traditions. It is therefore possible to imagine a turn where the most important task is really to criticise and
		identify weaknesses. By taking a radically different approach, it is possible to see the old tradition in a new
		light and generate new insights. In such cases, the strength of the new position may be of less importance in
		itself, more crucial is that it establishes a point outside the tradition for identifying weaknesses. In science
		and technology studies this was sometimes called the reflexive turn and Woolgar made this kind of reflexive
		identification of weaknesses into a fine art (for instance Woolgar, <a href="#woo91">1991</a>). In an article
		entitled &quot;Turn, turn, and turn again: the Woolgar formula&quot; Pinch (<a href="#pin93">1993</a>) made this
		kind of approach into a joke. It was always possible, Pinch argued, to find a reflexive weakness in any account
		by showing that relevant alternative interpretations had been ignored or avoided. Woolgar would characterise
		research approaches as being driven by a formula in which relativism was applied selectively. In his article,
		Pinch made a new turn by identifying Woolgar's approach as being driven by a formula and then successfully
		applied that formula on Woolgar's own texts.</p>
	<p>The reflexive turn: as it was introduced by Woolgar, was actually a criticism against strong theoretical
		approaches, the tendency of creating a convergent research field. It is often the ambition of new research
		fields, such as information science, to become more mature, stronger and therefore more convergent. However, the
		strong research traditions suffer from other difficulties, always running a risk of systematically ignoring
		relevant aspects. While a reflexive turn is often problematic, as shown by Pinch, every research field should
		really have one.</p>
	<h2 id="many-different-turns-that-touch-upon-each-other-information-science-as-a-relatively-small-research-field">
		Many different turns that touch upon each other: information science as a relatively small research field</h2>
	<p>We talked earlier about aspects which make information science different and/or more complex than other research
		fields, something which makes historical reflection and reflections on identity more urgent. There is more to be
		said about this. The practice of studying information in its various forms was constructed and solidified in a
		social context very different from today's digital society, information age, information society, network
		society or whatever we want to call it. A crucial question for information science today is if the current
		organisation of research fields and subfields, created in another context, is optimal for studying information
		in its various forms today?</p>
	<p>The most obvious mismatch concerns the way information science was created and given an institutionalised
		position within universities as a rather minor academic subject. As the importance of information has exploded
		in the last decades, the study of information has not grown in the same pace. Instead, its status, identity and
		territory have been threatened by stronger disciplines. Ellis, Allen &amp; Wilson (<a href="#ell99">1999</a>)
		discuss this in the form of a &quot;low power budget&quot;. Information science remains a relatively minor
		academic field, despite the fact that the object of its study has become a crucial vehicle for cultural, social
		and economic relationships/transactions.</p>
	<p>An additional explanation for this lack of suitable growth can be that although information science has a long
		history, it didn't emerge as a mature (and still growing discipline) until the late 1960s and early 1970s.
		Ingwersen (<a href="#ell92">1992</a>) identifies the years 1977-1980 as a turning point in which information
		science becomes more mature and well defined. Similar analyses can be made about the social relevance of
		information science. Saracevic (<a href="#sar92">1992</a>) reminds us that information problems changed their
		relevance to society. Information problems always existed, they were always present, but their perceived or real
		social importance changed. They were perceived as much more important in the 1970s than in the 1940s and the
		re-evaluation of the importance of information problems would only escalate with time.</p>
	<p>Now, the 1970s or 1980s was not a good time to start thinking about expansion in the academic system. Research
		policy was at this time starting to move away from the post war ideas of great expansion. In the decades to
		come, government spending on research budgets would become increasingly modest and evolve into an idea of
		&quot;steady state science&quot; (Ziman, <a href="#zim94">1994</a>). In a sense, this made it very difficult for
		new subjects to establish themselves as major players. Information science did indeed expand but not in relation
		to the shift in social revaluation of the importance of information problems.</p>
	<p>Naturally, it did not help that during this time of potential expansion, many leading researchers in the field
		expressed doubts about the quality of what they were doing. Researchers such as Farradane (<a
			href="#far80">1980</a>), Brookes (<a href="#bro80">1980</a>) and Shera (<a href="#she83">1983</a>) all in
		their own way painted a picture of information science as a research area lacking in theories of its own and
		instead loaning from other, stronger disciplines, a minor cog wheel dependent on the turns of the big wheels.
	</p>
	<p>This lack of expansion space, probably can serve as an explanation for why information science contains such a
		wide diversity of small subfields. In another more generous funding context, they would have surely all been
		given their own discipline and the space to develop apart from each other. However, as they have all stayed
		within the information science umbrella, it makes for an interesting, multidisciplinary intellectual setting.
	</p>
	<p>The setting described in this way is a breeding ground for the import of new trends and indeed various turns.
		Still, the whole research field will not turn as one, rather like cogwheels they will turn in different
		directions and speeds. As the research field is so crammed together, they will tend to scrape into each other.
	</p>
	<p>Does this make historical reflection and other types of reflection more urgent than in other fields? We would
		think so.</p>
	<h2 id="the-cognitive-turn-and-the-cognitive-viewpoint">The cognitive turn and the cognitive viewpoint</h2>
	<p>Without doubt, the most influential turn in the history of information science has been the cognitive turn of the
		late 1980s. The cognitive viewpoint is often in information science literature linked closely to &quot;the
		cognitive turn&quot;, which however must be seen to be a broader phenomenon, in a fundamental way influencing
		many disciplines, among them information science. In information science, the cognitive turn is most frequently
		described as a drawn out procedure that starts in the 70s and is concluded in the 80s or early 90s. German
		researcher Rafael Capurro (<a href="#cap92">1992</a>, 86) suggests that it took place as early as 1972 &quot;and
		it became particularly clear&quot; (86) in 1982. David Ellis, in his discussion of a cognitive paradigm in
		information science (<a href="#ell92">1992</a>) would instead see early work being made in the late 1970s.</p>
	<p>Ingwersen (<a href="#ing92">1992</a>) identifies the central point of the cognitive viewpoint to view all
		processes involved in information retrieval, human as well as nonhuman, as cognitive. The main task is therefore
		to bring these cognitive structures into accord with each other and create a balance between system processes,
		authors, system designers, information workers and users. However, the cognitive viewpoint has been under fire
		from researchers for not taking into account social aspects of information processors (Capurro, <a
			href="#cap92">1992</a>, Frohmann, <a href="#fro92">1992</a>, Vakkari, <a href="#vak94">1994</a>).</p>
	<h2 id="the-turn-of-direction">The turn of direction</h2>
	<p>In <a href="#sar92">1992</a>Saracevic stated that an obvious question is to ask from which end we should address
		the major research problems in the field: from the human or technological vantage point? &quot;Is the technology
		by itself a problem or is it a solution? Or both?...Or are the human aspects (knowledge, knowledge records,
		communication, social, institutional, and individual contexts, information use and need...) fundamental on the
		basis of which technological solution are to be built?&quot; (20) He added that the prevailing philosophy seemed
		to be that it was easier to teach humans to adjust to the machine then to adjust the machines to humans.</p>
	<p>Saracevic paints a picture of an either or dilemma. Either we build on the technology and integrate psychological
		and social aspects into our understanding or we build on the user. Saracevic does not talk about this in terms
		of a turn, but it is still an interesting variation of the metaphor. Saracevic's approach is essentially the
		same topic as the cognitive turn/cognitive viewpoint, which entails seeing human cognition and machine cognition
		as basically the same. However, from the vantage point of Saracevic, if we interpret him correctly, such a
		position is problematic since there is a basic conflict on whose terms integration will be done. An exchange
		between different social worlds will not be characterised by &quot;win-win solutions&quot;, but by one side
		moving into the domain of the other and in this process totally reconceptualising the object of research. This
		image is given to us by Frohmann (<a href="#fro92">1992</a>) in his criticism of the cognitive viewpoint,
		linking claims of universalism to theoretical imperialism.</p>
	<p>The discussion of direction has of course been continuously discussed since Saracevic wrote his text in 1992.
		Hjørland (<a href="#hjo02">2002</a>) has promoted a socio-cognitive viewpoint inspired by a pragmatic turn in
		which human actions and activities are seen as the most basic entities. &quot;In this way socio-cognitive views
		in many respects turns the cognitive view upside down. They are interested in individual cognition, but approach
		this from the social context, not from the isolated mind or brain. They are not working inside-out, but
		outside-in.&quot; (258-259)</p>
	<p>A turn in direction described in this way can be said to be a fundamental turn, one which moves the field away
		from a divergent identity and toward a more integrated and convergent identity. Interestingly enough, the
		cognitive viewpoint can also be discussed as such a fundamental turn.</p>
	<h2 id="the-turn-according-to-ingwersen-and-j%C3%A4rvelin-2005">The turn according to Ingwersen and Järvelin (<a
			href="#ing05">2005</a>)</h2>
	<p>Ingwersen and Järvelin in their ambitious and important book <em>The Turn: Integration of Information Seeking and
			Retrieval in Context</em> (<a href="#ing05">2005</a>) are daring in their attempt to move beyond boundaries
		and take into account interest of all research traditions. The authors take inventory of all possible criticism
		against the cognitive viewpoint and device counter strategies for each one. They also admit past mistakes in a
		refreshing way that is rare in academic texts.</p>
	<p>However, the general outlook taken in the book is actually the very opposite of what we usually take to be a turn
		within research. It usually means to take on a new perspective, such as the cognitive viewpoint. Instead, the
		aim of The Turn is to be all inclusive, to construct a perspective that is, well, actually not a perspective at
		all, but aims at taking into account everything at all times. In a way, if implemented, this can indeed be the
		big turn, the one that ends it all. If everything is included in your position, then you have nowhere else to
		turn. It is an attempt to turn away from the model of a divergent field into more of a convergent model.</p>
	<p>When Ingwersen and Järvelin discuss the new &quot;turn&quot;, it can clearly be characterised as extending
		previous interests from the cognitive view, rather than turning in a new direction:</p>
	<blockquote>
		<p>&quot;The quite individualistic perspective laid down in the former monograph is hence expanded into a social
			stance towards information behaviour, including generation, searching and use of information.&quot; (Page
			vii)</p>
	</blockquote>
	<p>The idea is not to turn away from the cognitive viewpoint, but rather to expand it. This is a rather different
		way of utilising the metaphor of a turn compared to other attempts discussed in this article. We actually find
		two different versions of what the turn means on the very first page of the preface. In one of these versions,
		it is referred to a prediction made in Ingwersen (<a href="#ing92">1992</a>) that a cognitive turn would come in
		the future and the framework in the new books is now said to be that turn. This would mean that the authors are
		involved in some kind of cognitive interruption and moving in a new direction. However, more often the authors
		prefer to talk about their new position as an extension of the cognitive viewpoint rather than something
		different. Let us therefore move over to a second version of the turn:</p>
	<blockquote>
		<p>&quot;We therefore chose The Turn as the main title to communicate the idea that it is time to look back and
			look forward to develop a new integrated view of information seeking and retrieval: the field should turn
			off its separate narrow paths of research and construct a new avenue.&quot; (Page vii)</p>
	</blockquote>
	<p>A few lines later this new avenue has been transformed into a highway. As they are utilising this metaphor, it is
		actually not they themselves that are turning, they are extending their perspective. With this extension, they
		expect to be outlining a much more attractive, integrated perspective than earlier, one that everyone should be
		able to participate in. It is a broad highway with room for everybody. So, it is actually other researchers that
		they hope will turn towards them, changing the course of their traditions. In this way, traditions will be
		integrated and instead of there being many different traditions, all of them by themselves too weak to make a
		difference, there will be one strong tradition that will be able to make an impact.</p>
	<p>It is actually possible to make a third interpretation of the turn, which is close to the second. By entitling
		the book The Turn, there is an implicit message sent that this is the big one, the one to end all turns. It is
		not any old turn, but The Turn.</p>
	<p>However, in all these variations this is still the same cognitive strategy that Frohmann criticised in <a
			href="#fro92">1992</a>, calling the cognitive viewpoint theoretically imperialistic. The idea promoted in
		the metaphors is obviously to either let all other traditions flow into the extension of the cognitive viewpoint
		or to create such a strong &quot;highway&quot; that all other traditions as an implication becomes marginalised.
		In both cases, the strategy can be called imperialistic if one wants to utilise a negative metaphor or an
		attempt to make the field stronger and more convergent, to utilise a more positive metaphor. However, the
		authors seem uncertain about the metaphor and sometimes paint a picture of a possible future where there indeed
		exist many perspectives, as in the closing chapter:</p>
	<blockquote>
		<p>&quot;We feel that this book opens up an intriguing avenue for research into IS&amp;R. At the same time we
			hope that many among out (sic!) readers find the avenue equally intriguing, and those who do not, at least
			find ingredients for the development their own, possibly conflicting approaches to Information Science.
			There obviously is much to do in the area of IS&amp;R and many approaches are welcome and possible. Progress
			may be achieved also from disagreement.&quot; (Page 379)</p>
	</blockquote>
	<p>From our perspective, it would seem that utilising the metaphor of the turn in the way that is done here opens up
		many questions on what kind of research field information science should be. Given this divergent tradition, is
		it desirable and possible to create a more integrated research field? What is gained? What is lost? Is the
		cognitive viewpoint, which earlier has functioned as a polarising device, a suitable foundation for integration?
		Would such a strengthened research field be perceived as threatening and dominating to other subdisciplines
		within information science?</p>
	<p>An obvious criticism is that the cognitive viewpoint builds on a foundation of the cognitive perspective
		extending toward the social. As such, this model would tend to alienate researchers with a sociological
		perspective who view cognitive dimensions as basically social. With an imperialistic interpretation of Ingwersen
		and Järvelin (<a href="#ing05">2005</a>), their book would seem to be an attempt to marginalise social studies
		of information seeking, by including them as a secondary aspect under a dominating cognitive paradigm.</p>
	<h2 id="conclusion">Conclusion</h2>
	<p>This paper has discussed several different types of &quot;turns&quot;. The most important difference can be
		linked to the image of research fields as either divergent or convergent. We have discussed several ideas that
		can be characterised as fundamental turns that would serve to move the whole field in one direction and
		therefore manifest a more convergent research field. An image of a stronger more convergent research field is
		often a hidden implication of a turn. Perhaps strategies of introducing different &quot;turns&quot; need to be
		connected to a position on what kind of research field information science should be. Do we want a field with
		many minor turns or do we want to move over to a more convergent research field and turn as one unit?</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="bec01"></a>Becher, T. &amp; Trowler, P. R. (2001). <em>Academic tribes and territories: intellectual
				enquiry and the culture of disciplines. Second edition.</em> Buckingham: Open University Press.</li>
		<li><a id="bro80"></a>Brookes, B. C. (1980). The foundations of information science. Part one: philosophical
			aspects. <em>Journal of Information Science</em>, <strong>2</strong>(3-4), 125-133.</li>
		<li><a id="buc96"></a>Buckland, M. K., &amp; Liu, Z. (1996) . The history of information science. <em>Annual
				Review of Information Science and Technology</em>, <strong>30</strong>, 383-416.</li>
		<li><a id="cap92"></a>Capurro, R. (1992). What is information science for? A philosophical reflection. In P.
			Vakkari &amp; B. Cronin (eds.) Conceptions of library and information science: historical, empirical and
			theoretical perspectives (pp. 82-96). London: Taylor Graham. .</li>
		<li><a id="ell92"></a>Ellis, D. (1992). The physical and cognitive paradigms in information retrieval research.
			<em>Journal of Documentation</em>, <strong>48</strong>(1), 45-64.</li>
		<li><a id="ell99"></a>Ellis, D., Allen D. &amp; Wilson, T. (1999). Information Science and Information Systems:
			Conjunct Subjects Disjunct Disciplines. <em>Journal of the American Society for Information Science</em>,
			<strong>50</strong>(12), 1095-1107.</li>
		<li><a id="far80"></a>Farradane, J. (1980). Information science? <em>Journal of Information Science</em>
			<strong>2</strong>(2), 313-314.</li>
		<li><a id="fro92"></a>Frohmann, B. (1992). The power of images: a discourse analyses of the cognitive viewpoint.
			<em>Journal of documentation</em>, <strong>48</strong>(4), 365-386.</li>
		<li><a id="hjo02"></a>Hjørland, B. (2002). Epistemology and the socio-cognitive perspective in information
			science. <em>Journal of the American Society for Information Science and Technology</em>,
			<strong>53</strong>(4), 257-270.</li>
		<li><a id="ing92"></a>Ingwersen, P. (1992). <em>Information Retrieval Interaction</em>. London: Taylor Graham.
		</li>
		<li><a id="ing05"></a>Ingwersen, P. &amp; Järvelin, K. (2005). <em>The turn: integration of information seeking
				and retrieval in context</em>. Berlin: Springer/Kluwer.</li>
		<li><a id="mac83"></a>Machlup, F. &amp; Mansfield, U. (1983). Prologue: cultural diversity in studies of
			information. In F. Machlup &amp; U. Mansfield (Eds.) The study of information: interdisciplinary messages.
			New York: John Wiley.</li>
		<li><a id="pin93"></a>Pinch, T. (1993). Turn, turn, and turn again: the Woolgar formula. <em>Science,
				Technology, &amp; Human Values</em>, <strong>18</strong>(4), 511-522.</li>
		<li><a id="ray96"></a>Rayward, W. B. (1996). The history and historiography of information science: Some
			reflections. <em>Information processing and management</em>, <strong>32</strong>(11), 3-17.</li>
		<li><a id="ray04"></a>Rayward, W. B. (2004). Scientific and Technological Information Systems in Their Many
			Contexts: The Imperatives, Clarifications, an Inevitability of Historical Study. <em>Proceedings of the 2002
				Conference on the History and Heritage of Scientific and Technological Information Systems</em>. The
			American Society for information science and technology.</li>
		<li><a id="ray05"></a>Rayward, W. B. (2005). The historical development of information infrastructures and the
			dissemination of knowledge: a personal reflection. <em>Bulletin of the American Society for Information
				Science and Technology</em>, <strong>31</strong>(4), 19-22.</li>
		<li><a id="sar92"></a>Saracevic, T. (1992). Information science: origin, evolution and relations. In P. Vakkari
			&amp; B. Cronin (eds.) Conceptions of library and information science: historical, empirical and theoretical
			perspectives (pp. 5-27). London: Taylor Graham.</li>
		<li><a id="sar99"></a>Saracevic, T. (1999). Information science. <em>Journal of the American Society for
				Information Science</em>, <strong>50</strong>(12), 1051-1063.</li>
		<li><a id="she83"></a>Shera, J. H. (1983). Librarianship and information science. In Machlup, F. &amp;
			Mansfield, U. (Eds.) The study of information: interdisciplinary messages (pp. 386-388). New York: Wiley.
		</li>
		<li><a id="vak94"></a>Vakkari, P. (1994). Library and information science: its content and scope. Advances in
			librarianship, <strong>18</strong>, 1-55</li>
		<li><a id="whi98"></a>White, H.D., &amp; McCain, K.W. (1998). Visualising a discipline: An author co-citation
			analysis of information science, 1972-1995. <em>Journal of the American Society for Information
				Science</em>, <strong>49</strong>(4), 327-355.</li>
		<li><a id="woo91"></a>Woolgar, S. (1991). The turn to technology in social studies of science. <em>Science,
				Technology, &amp; Human Values</em>, <strong>16</strong>(1), 20-50.</li>
		<li><a id="zim94"></a>Ziman, J. (1994). <em>Prometheus bound: science in a dynamic steady state</em>. Cambridge:
			Cambridge University press.</li>
	</ul>

</body>

</html>