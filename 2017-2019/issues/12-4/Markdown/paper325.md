#### Vol. 12 No. 4, October, 2007

* * *

# Las revistas argentinas de Ciencias Antropológicas: visibilidad en bases de datos internacionales

#### [Susana Romanos de Tiratel](mailto:sromanos@filo.uba.ar) y [Graciela M. Giunti](mailto:gmgiunti@filo.uba.ar)  
Instituto de Investigaciones Bibliotecológicas,  
Facultad de Filosofía y Letras,  
Universidad de Buenos Aires,  
Buenos Aires,  
Argentina

#### Abstract

> **Introducción.** Se presenta el escenario actual donde se desenvuelve la publicación periódica y su compleja problemática en América Latina, enfocando la investigación en las revistas argentinas de Ciencias Antropológicas.  
> **Objetivos.** Determinar los porcentajes de visibilidad, representatividad y solapamiento de las revistas académicas argentinas de Ciencias Antropológicas en los servicios de indización y resúmenes de alcance internacional.  
> **Métodos.** Dentro del marco teórico provisto por Keresztesi respecto de las interrelaciones entre investigación y bibliografía, se usan métodos cuantitativos para determinar los indicadores de representatividad, de visibilidad y de vacancia de las revistas argentinas de Ciencias Antropológicas, en bases de datos bibliográficos internacionales pluridisciplinarias y unidisciplinarias.  
> **Resultados.** Las revistas argentinas de Ciencias Antropológicas tienen una visibilidad de 44,44 % en bases de datos internacionales y el _Anthropological Index Online_ es la base que más revistas indiza, dado que la producción argentina de Ciencias Antropolológicas alcanza en ésta una visibilidad de 40,74 %.  
> **Conclusiones.** Las bases de datos de Ciencias Antropológicas tienen un alto grado de solapamiento en la indización de las revistas argentinas de la disciplina investigada. Los datos comprobados son de interés para el desarrollo de las colecciones de referencia en unidades de información dedicadas a las Ciencias Antropológicas. Por su parte, los editores científicos del área contarán con datos comprobados para diseñar las estrategias de difusión de sus publicaciones.

#### **[Abstract in English](#abs)**

## Introducción

Las nuevas tecnologías de la información y de la comunicación han influido en la revista especializada, y se presentan como una alternativa promisoria que, quizás, permita superar los males endémicos de la publicación académica impresa, sobre todo en los países en vías de desarrollo, como es el caso de los latinoamericanos. Esta situación se acentúa en las Humanidades y las Ciencias Sociales. Sin embargo, los actores clásicos de la revista impresa aún siguen desempeñando sus papeles tradicionales; así, autores, editores científicos, evaluadores, publicadores, bibliógrafos (gestores de bases de datos), bibliotecólogos (gestores de colecciones), y usuarios o consumidores se combinan y aúnan sus esfuerzos para que el producto de las investigaciones se difunda y llegue a todos los estamentos de la comunidad interesada en ellos o afectada por sus resultados.

Si bien es probable que la edición electrónica llegue a superar la falta de cumplimiento de la frecuencia declarada por los editores científicos, es muy difícil que pueda mejorar las demoras inherentes a los procesos de edición: envío y recepción de los artículos, evaluación, correcciones, nueva evaluación, etc. También existen esperanzas en cuanto a la rebaja de los costos crecientes de las suscripciones de las versiones en papel, sin embargo, la edición electrónica de revistas académicas no ha demostrado que pueda solucionar la cuestión, siempre preocupante, de la proliferación de títulos y de la dispersión de artículos en publicaciones que no son nucleares para la disciplina involucrada. Ambos problemas siguen comprometiendo a autores, bibliotecólogos, productores de servicios secundarios y usuarios de la bibliografía especializada. El ISSN (International Standard Serial Number) resolvió la cuestión de la caracterización unívoca de los títulos de revistas. Ahora se agrega la inestabilidad digital de las direcciones URL y la necesidad de usar metadatos que permitan la correcta identificación de la revista por los buscadores de Internet.

Sencillamente enunciados, los múltiples problemas que plantea un formato bibliográfico tan apreciado por los investigadores de todas las disciplinas se presentan complejos, con enfoques diversos y, la mayoría de las veces, contradictorios, porque existe una gran variedad de intereses, en muchas ocasiones contrapuestos, asociados con la revista especializada.

De esta situación se deriva uno de los inconvenientes para procesar la bibliografía latinoamericana referida a la problemática que, a las cuestiones antes mencionadas, agrega otras propias de la región: incumplimiento de la frecuencia de las publicaciones periódicas, escasez de fondos permanentes dedicados a su financiamiento, falta de editores profesionales, carencia de políticas científicas claras y sostenidas en el tiempo, fallas en la distribución y difusión de los títulos, etc.; presencia casi nula de títulos latinoamericanos en las bases de datos internacionales, sobre todo en las del ISI Web of Knowledge de Thomson Scientific (antes producidas por el Institute for Scientific Information); debate sobre el uso del idioma inglés en lugar del español o portugués; establecimiento de parámetros válidos y aceptados por todos para evaluar la calidad de las publicaciones; oscilación continua entre dependencia e independencia de los países centrales o difusión endogámica de los resultados de investigación ([Cetto y Hillerud, (Eds.) 1995](#cet95); [Fuenmayor y Abdel 1995](#fuen95); [Krauskopf y Vera 1995](#kra95); [Armendáriz Sánchez y Ordóñez Alonso 1998](#arm01); [Cetto y Alonso (Eds.) 1999](#cet99); [Graças Targino y Garcia 2000](#gra00); [Haupt 2000](#hau00); [Ratto de Sala y Dellamea 2001](#rat01)).

En este sentido, no se puede dejar de mencionar un esfuerzo sostenido que se inició en Brasil en 1998, [SciELO: Scientific Electronic Library Online](http://www.scielo.org ) (Biblioteca Científica Electrónica en Línea). Es un modelo para la publicación electrónica cooperativa de revistas científicas en Internet. Especialmente desarrollado para responder a las necesidades de la comunicación científica en los países en desarrollo y particularmente de América Latina y el Caribe, el modelo proporciona una solución eficiente para asegurar la visibilidad y el acceso universal a su literatura científica, contribuyendo a la superación del fenómeno conocido como "ciencia perdida". Además, el Modelo SciELO contiene procedimientos integrados para la medida del uso y del impacto de las revistas científicas incluídas en su base.

Sin desconocer los trabajos sobre revistas españolas de Humanidades y Ciencias Sociales, como por ejemplo, el recientemente publicado de Angel Borrego y Cristóbal Urbano no se incorporan en este estado de la cuestión dado que España, como integrante de la Comunidad Europea, se agrupa dentro de los países desarrollados, tipología que no comparten los países latinoamericanos, entre ellos la Argentina, objeto de estudio de este proyecto. ([Borrego y Urbano 2006](#bor06); [García y Román 1998](#gar98); [Giménez, Román y Sánchez 1999](#gim99); [Gímenez y Román 2000](#gim00); [Hortal Muñoz 2006](#hor06); [Lamarca, et al. 2005](#lam05); [Osca-Lluch y Mateo 2003](#osc03); [Osca-Lluch y Haba 2005](#osc05)).

Esta investigación se ubica dentro de los estudios bibliográficos porque se origina en la observación de las fallas en el control de la producción argentina en Humanidades y en Ciencias Sociales, y este artículo, en particular, se centra en las revistas especializadas en Ciencias Antropológicas.

Un presupuesto subyacente es que "...compete a cada uno de los países el control exhaustivo de la literatura derivada de las actividades de investigación constituyéndose así la bibliografía nacional especializada de cada país..." ([Romanos de Tiratel 2003](#rom03): 47). Esta presunción reconoce su origen en la historia misma de las publicaciones científicas en los países mejor desarrollados dado que, en 1665 y con una diferencia de tres meses, aparecen las dos primeras revistas eruditas -_Journal des Sçavants y Philosophical transactions_- que instauran un modelo de comunicación seguido por el resto de Europa. Poco más de cien años después (1778) y ante la necesidad de alertar sobre los avances y descubrimientos de sus pares nacionales y extranjeros, se compila en Alemania la primera revista de resúmenes especializada, obra de Lorenz von Crell, que se tradujo al inglés en 1791\. Muy pronto, esta tarea se presenta como imposible para un solo individuo y son las sociedades profesionales y las academias quienes asumen el proceso de organización y difusión considerándolo como uno de sus deberes básicos ([Houghton 1975](#hou75): 81-82). Sin embargo, este proceso no sigue las mismas etapas en la Argentina, donde sólo se registran esfuerzos aislados y fugaces que cubren parte de la producción retrospectiva de Ciencias Antropológicas ([Saugy de Kliauga 1986](#sau86); [Instituto Nacional de Antropología 1986](#ina86); [Galeotti y Davasse 1983](#gal83)). Con un alcance más amplio, pluridisciplinario, se pueden mencionar tres servicios de indexación de revistas argentinas, el _Proyecto Padrinazgo: publicaciones periódicas argentinas (4PAr)_[(Unired 2003)](#uni03) que, hasta el momento, indexa dos títulos argentinos de Ciencias Antropológicas, el _Índice de Publicaciones de la Facultad de Filosofía y Letras (IPuFil)_ [(Romanos de Tiratel, Giunti, Contardi y Dibucchianico 1999)](#rom99) que analiza cuatro y _Scielo Argentina_ con un título. [(SciELO Argentina)](#Scie05). (Véase la lista de la tabla 12).

Ahora bien, se puede suponer que los servicios de indización y resúmenes de alcance internacional controlan, analizan y difunden la producción total de los países que abarcan y, por lo tanto, no es necesario que en cada lugar se hagan índices nacionales. Confirmar o no este supuesto con datos fiables es uno de los objetivos perseguidos por este estudio que, en primer lugar, determinó los porcentajes de visibilidad de las revistas argentinas especializadas en Humanidades y Ciencias Sociales en bases de datos internacionales pluridisciplinarias ([Romanos de Tiratel, Giunti y Parada, 2002](#rom02); [Romanos de Tiratel, 2003](#rom03)) y que ha seguido con cada una de las materias, replicando el modelo ya probado, en las bases de datos internacionales unidisciplinarias ([Romanos de Tiratel, Giunti y Parada 2003](#rom03b); [Romanos de Tiratel y López 2004](#rom04); [Romanos de Tiratel y Giunti 2005](#rom05)).

En síntesis, la investigación emprendida persigue los siguientes objetivos:

*   Contribuir, mediante un diagnóstico descriptivo, a la planificación de la bibliografía nacional argentina especializada en Humanidades y Ciencias Sociales.
*   Identificar y diagnosticar las fallas en el control bibliográfico de la producción científica argentina en Humanidades y Ciencias Sociales, publicada en revistas nacionales.
*   Determinar el grado y nivel de cobertura que las bases de datos internacionales, tanto pluridisciplinarias como unidisciplinarias, hacen de las revistas científicas argentinas de Humanidades y Ciencias Sociales.
*   Diseñar y aplicar fórmulas para determinar las tasas de representatividad, visibilidad, solapamiento y vacancia de las revistas argentinas de Humanidades y Ciencias Sociales en general, y de las distintas disciplinas que conforman ambos campos del conocimiento en particular, tal como se presentan en las bases de datos internacionales, tanto pluridisciplinarias como unidisciplinarias.
*   Establecer listados de revistas científicas argentinas de las distintas disciplinas que conforman las Humanidades y las Ciencias Sociales:
    *   Títulos indexados en bases de datos internacionales, tanto pluridisciplinarias como unidisciplinarias.
    *   Títulos no indexados en bases de datos internacionales, tanto pluridisciplinarias como unidisciplinarias.
    *   Títulos indexados en bases de datos nacionales.
    *   Títulos candidatos a su indexación.

Este artículo se limita a estudiar las revistas argentinas en curso de publicación, especializadas en Ciencias Antropológicas, en relación con su inclusión o no en los servicios de indización y condensación pluridisciplinarios y unidisciplinarios internacionales. Es necesario hacer notar que, en muchos aspectos, trabajar con revistas y con bases de datos bibliográficos es como apuntar a un blanco móvil dada la inestabilidad y constante mutación de estos recursos de información ([Sutton y Foulke 1999](#sut99): 135).

Las revistas de Ciencias Antropológicas de nuestro país no han quedado al margen de los avatares políticos de la Argentina. Como puede verse en la tabla 9 de este trabajo, la proliferación de títulos nuevos coincide con el retorno de la democracia a partir de 1984\. En este sentido es muy útil la consulta de las investigaciones desarrolladas en la década de 1990 por Cristina Cajaraville, sobre la institucionalización de la Antropología Social en el período de consolidación democrática argentina a través de sus revistas ([Cajaraville 1997 y 1998](#caj97)).

## Contexto teórico

La investigación se encuadra dentro de las formulaciones planteadas por Michael Keresztesi [(1982)](#ker82), quien define a las disciplinas como conocimiento que produce y difunde sistemas, donde la investigación surge como la actividad central, pero no puede ocurrir aislada de las fuerzas que influyen sobre la ejecución de la tarea de indagación. La bibliografía, por lo tanto, debe ver a una disciplina como un sistema de vínculos, con muchos niveles, donde cada acción productiva y de apoyo engendra los efectos correspondientes en la literatura de la disciplina y en el mecanismo informativo que provee el acceso a ésta. Las interrelaciones entre investigación y bibliografía se vuelven obvias cuando se examinan las disciplinas científicas en una matriz evolutiva. Así Keresztesi, identifica tres estadios en el crecimiento de un área: el estadio pionero, el de elaboración y proliferación, y el estadio del establecimiento; y tipifica la producción que caracteriza a cada estadio y las herramientas de acceso a la misma. Además, el autor traza la topografía de las disciplinas científicas cuando identifica sus cuatro dimensiones: epistemológica, sociológica, histórica y bibliográfica ([Keresztesi, 1982](#ker82): 8-13).

Un área primaria de interés en la dimensión bibliográfica son los procesos de investigación, comunicación y difusión del conocimiento junto con el origen, la cantidad y la distribución de la literatura especializada por formatos. El aspecto más crítico es el aparato de la información; algunos de sus puntos clave son: cómo satisface las necesidades de la disciplina, la estructura y el nivel de sofisticación del acceso bibliográfico, las capacidades de las herramientas de referencia, el estado del alfabetismo informativo, las consideraciones políticas y de servicio en los planes locales, nacionales e internacionales ([Keresztesi, 1982](#ker82): 23-24).

## Métodos y procedimientos

Antes de pasar a los procedimientos seguidos es necesario formular algunas precisiones terminológicas, tal como el concepto de revista académica, científica, especializada o de investigación. En español no existen palabras distintas para denominar a este tipo de periódica y diferenciarlo de las revistas de interés general o de los boletines de noticias. El inglés tiene matices más ricos para este universo; así, discrimina entre journal, magazine y newsletter.

Nisonger [(1998)](#nis98) citando al ALA Glossary of Library and Information Science de 1983, define journal como una periódica que contiene artículos especializados y/o difunde información en curso sobre investigación y desarrollo en un campo temático particular. Más adelante, enumera las características de las revistas científicas: los autores son expertos que escriben en un estilo académico para una audiencia especializada sobre temas de investigación; suelen tener un aspecto físico serio, notas al pie de página y bibliografías, un comité editorial, pueden contar con evaluadores externos así como estar cubiertas por servicios de indización y condensación, dedicados a analizar la literatura de distintas disciplinas y tienen una circulación y tiradas limitadas ([Nisonger, 1998](#nis98): 4-5). A esto se puede agregar el tipo de publicador: universidades, asociaciones y colegios profesionales, academias, institutos o centros de investigación y editoriales comerciales dedicadas a la publicación científica.

En cuanto a los procedimientos, en primer término, se estableció una lista de seriadas especializadas en Ciencias Antropológicas, utilizando como fuente la _[Bibliografía Nacional de Publicaciones Periódicas Argentinas Registradas (BINPAR)](http://www.caicyt.gov.ar/issn)_, compilada por el Centro Argentino de Información Científica y Tecnológica; estas publicaciones pertenecen a las siguientes categorías definidas en la mencionada base de datos: Antropología, Antropología Cultural, Antropología Filosófica, Antropología Física, Antropología Social, Arqueología, Etnología, Etnografía, Folklore; así, el grupo inicial sumó 142 títulos [Consulta febrero 2006].

Dado que se aceptó la definición de journal antes mencionada, se descartaron los títulos de divulgación, los boletines de noticias o newsletters, las series monográficas, y los boletines bibliográficos o de reseñas. Además, se decidió no incluir las revistas cuyo inicio se ubica después de 2004, porque hay que dejar un lapso suficiente como para que los títulos sean reconocidos internacionalmente, tal como puede verse en la tabla 9.

También se hizo otra selección eliminando los duplicados que aparecían bajo los distintos descriptores, las revistas cuyo estado era desconocido, las cesadas y las publicadas por individuos a los que no se pudo identificar; de este modo, se obtuvo una lista de 63 títulos.

Dada la problemática de la irregularidad que padecen las revistas latinoamericanas y la comprobación de que algunos datos proporcionados por [la BINPAR](http://www.caicyt.gov.ar/issn) no siempre reflejaban la realidad presente por las características cambiantes de este tipo de formato, se llevó a cabo una comprobación título por título con todas las herramientas bibliográficas a nuestro alcance, en una secuencia sucesiva: visita a la hemeroteca del Instituto Nacional de Ciencias Antropológicas y Pensamiento Latinoamericano (INAPL); comprobación de las existencias en el _[Catálogo Colectivo de Publicaciones Periódicas](http://www.caicyt.gov.ar/ccpp)_ y en los catálogos de grandes bibliotecas universitarias de Ciencias Sociales de nuestro país; también se consultaron las páginas web de las instituciones editoras, o bien se las contactó telefónicamente o por correo electrónico; en última instancia, se recurrió a la consulta con algunos expertos en la disciplina. Una vez cumplido este procedimiento se obtuvo una lista final de 27 títulos.

Los pasos descritos, con sus diferentes estrategias y la aplicación de criterios, llevaron a conformar una lista confiable y segura de revistas argentinas de Ciencias Antropológicas, que permitió la posterior confrontación con las listas de publicaciones indexadas por los servicios bibliográficos internacionales. Para identificar dichos servicios o bases de datos bibliográficos se recurrió a guías de obras de referencia; luego se localizaron los sitios Web para extraer las nóminas de revistas que dichas bases declaran indizar. Hay que aclarar que se tomó la decisión de no considerar, para esta investigación, las bases de datos de texto completo por la alta tasa de variabilidad en la cobertura de títulos indexados.

Una vez obtenidas las listas de recursos indexados por las bases de datos internacionales pluridisciplinarias y unidisciplinarias, se enfrentaron nuevas dificultades: extensión de los listados (en algunos casos de varios cientos de títulos), omisión del lugar de edición y del ISSN, etc. El propósito era el de identificar los títulos argentinos de Ciencias Antropológicas y del resto de América Latina incluidos en las bases, para ello hubo que separar todos los títulos en lengua española y portuguesa, conformándose un primer listado en el que se investigó cada revista para determinar su procedencia geográfica, descartando luego las de España y las de Portugal; finalmente, se agruparon bajo los países latinoamericanos con el objeto de determinar y comparar los indicadores de representatividad.

En este estudio se aplicaron métodos cuantitativos, por lo tanto, hubo que manejar variables previamente definidas de representatividad, visibilidad, vacancia y solapamiento, cuyas fórmulas y conceptos se detallan a continuación:

> **Indicador de representatividad (R)**: indica el porcentaje de títulos de un país, de una o más disciplinas, indexados en una base de datos; su fórmula: **R=(P<sub>i</sub>/N<sub>b</sub>) x 100**, donde: **P<sub>i</sub>** = total de títulos de un país, de una o más disciplinas, en una base de datos, y **N<sub>b</sub>** = total de títulos indexados en esa base de datos.  
> **Indicador de visibilidad (Vi)**: indica la relación porcentual entre el total de títulos de un país, de una o más disciplinas, indexados en una base de datos y el total de títulos que produce ese país en una o más disciplinas. **Vi=(P<sub>i</sub>/N<sub>t</sub>) x 100**, donde **P<sub>i</sub>** = total de títulos de un país, de una o más disciplinas, en una base de datos, y **N<sub>t</sub>** = total de títulos de ese país en una o más disciplinas.  
> **Indicador de vacancia(Va)**: es inverso al indicador de visibilidad; indica la relación porcentual entre el total de títulos de un país, de una o más disciplinas, no indexados en una base de datos y el total de títulos que produce ese país en una o más disciplinas. **Va=(N<sub>v</sub>/N<sub>t</sub>) x 100**, donde **N<sub>v</sub>** = total de títulos de un país, de una o más disciplinas, no indexados en una base de datos, y **N<sub>t</sub>**= total de títulos de ese país en una o más disciplinas.  
> **Indicador de solapamiento**: indica las veces en que un mismo título se repite en distintas bases de datos.

La aplicación de estas fórmulas permite responder a las preguntas: ¿Cuáles son las tasas de representatividad, de visibilidad, de solapamiento y de vacancia de la producción hemerográfica argentina especializada en Ciencias Antropológicas en las bases de datos bibliográficos internacionales, tanto pluridisciplinarias como unidisciplinarias?; y ¿existen tasas diferenciales entre las bases de datos pluridisciplinarias y las unidisciplinarias?

## Análisis de datos

Como se ha dicho precedentemente, se identificaron 27 títulos de revistas argentinas dedicadas a las Ciencias Antropológicas (Veáse tabla 12). Para determinar su tasa de representatividad, de visibilidad, de vacancia y de solapamiento, se confrontaron con las listas de revistas cubiertas por las bases de datos pluridisciplinarias internacionales y latinoamericanas y, finalmente, con las internacionales unidisciplinarias.

### Bases de datos pluridisciplinarias

#### a) De alcance internacional

En primera instancia, para identificar el grado de presencia de revistas argentinas en importantes bases de datos internacionales, se consideraron tres bases pluridisciplinarias:

*   _[Francis(FR)](http://connectsciences.inist.fr/bases/internes/perana/test.php?baserev=francis)_
*   _[IBZ : International Bibliography of Periodical Literature in the Fields of Arts and Humanities and the Social Sciences](http://www.dgbiblio.unam.mx/servicios/dgb/publicdgb/bole/fulltext/volIII2/octava.PDF)_.
*   _[Social Sciences Citation Index (SSCI)](http://scientific.thomson.com/products/ssci/)_.

Entre los tres servicios de indexación y resúmenes, sólo _Francis (FR)_ incluye un título argentino de Ciencias Antropológicas. Esta base permite hacer búsquedas temáticas dentro del listado de revistas indexadas, así cuando nos restringimos a las Ciencias Antropológicas, encontramos 724 títulos dedicados a dicha disciplina, divididos en anthropologie (12 títulos), archeologie (487 títulos) y ethnologie (225 títulos). De este modo, la representatividad para el área es de R = 0,13\. La visibilidad es Vi = 3,70 y la vacancia es Va = 96,30.

_IBZ_ y _SSCI_ no incluyen ninguna revista argentina dedicada a las Ciencias Antropológicas.

#### b) De alcance latinoamericano

Se consideró una base de datos latinoamericana pluridisciplinaria: _[HAPI Online: The database of Latin American journal articles.](http://hapi.ucla.edu/web/index.php?token=416515002dc59178565194c10a2afa30)_. Producida en el UCLA Latin American Center. Sólo analiza revistas y lo hace en profundidad. Se trabajó con la lista de títulos en curso que incluye 325 ítem. La representatividad con un título argentino de Ciencias Antropológicas es de R= 0,30, su visibilidad es de Vi= 3,70 y la vacancia es Va = 96,30\.

Por otra parte, dado que tanto Francis como HAPI Online indizan el mismo título (véase tabla 12)en las bases de datos pluridisciplinarias analizadas el solapamiento alcanza el máximo porcentaje.

Hay que señalar que no ha sido considerado otro repertorio representativo de este grupo, el _[Handbook of Latin American Studies: HLAS Online](http://rs6.loc.gov/hlas/)_. Compilado por la Hispanic Division de la Library of Congress. Esta decisión se debe a la política seguida por el equipo que lo elabora: realizar una indización **selectiva** de los títulos y no depurar la lista, dado que incluye cualquier revista que se haya indizado en alguna oportunidad, aunque en la actualidad no se la analice. Por lo tanto, la lista de títulos seleccionados no siempre se ve representada con artículos indexados en dicha base.

### Bases de datos unidisciplinarias

Si bien se identificaron cuatro bases de datos internacionales que cubren el área temática estudiada, una de ellas, _[Abstracts in Anthropology (AA)](http://anthropology.metapress.com/app/home/main.asp?referrer=default)_, sólo incluye títulos de revistas en lengua inglesa, por lo tanto, no se la ha considerado para nuestro estudio.

Las otras tres bases son _Anthropological Index Online(AIO)_; _Anthropological Literature (AL)_; y la _International Bibliography of the Social Sciences/ Anthropology (IBSS/A)._ Esta última base cubre cuatro disciplinas de las Ciencias Sociales, pero permite la búsqueda sólo en la sección dedicada a Antropología, por lo tanto, la hemos considerado como si fuera una base unidisciplinaria.

El _[Anthropological Index Online (AIO)](http://aiotest.anthropology.org.uk/aiosearch/)_ es un índice de revistas que cubre Antropología Física, Arqueología, Etnografía Cultural y Lingüística. Analiza las colecciones de una biblioteca a la que podría considerarse la Biblioteca Nacional de Antropología de Gran Bretaña. Las entradas se extraen de 1.204 periódicas del British Museum Department of Ethnography -conocido ahora como la Anthropology Library- que incorpora a la antigua biblioteca del Royal Anthropological Institute.

De esos 1.204 títulos, 11 son argentinos, lo que se traduce en una representatividad de R = 0,91; en una visibilidad de Vi = 40,74 y en una vacancia de Va= 59,26\. Por otra parte, 5 de los 11 títulos indexados por AIO se solapan con los de las otras dos bases unidisciplinarias. (Véase tabla 12)

<table><caption>

**Tabla 1: Indicadores de representatividad (R) de revistas latinoamericanas en _Anthropological Index Online (AIO)_**</caption>

<tbody>

<tr>

<th>

_País_</th>

<th>

_Cantidad de títulos indexados_</th>

<th>

_R %_</th>

</tr>

<tr>

<td>México</td>

<td>22</td>

<td>1,83</td>

</tr>

<tr>

<td>Perú</td>

<td>12</td>

<td>0,99</td>

</tr>

<tr>

<td>

**Argentina**</td>

<td>

**11**</td>

<td>

**0,91**</td>

</tr>

<tr>

<td>Brasil</td>

<td>9</td>

<td>0,75</td>

</tr>

<tr>

<td>Chile</td>

<td>8</td>

<td>0,66</td>

</tr>

<tr>

<td>Colombia</td>

<td>7</td>

<td>0,58</td>

</tr>

<tr>

<td>Guatemala</td>

<td>4</td>

<td>0,33</td>

</tr>

<tr>

<td>Venezuela</td>

<td>3</td>

<td>0,25</td>

</tr>

<tr>

<td>Ecuador</td>

<td>3</td>

<td>0,25</td>

</tr>

<tr>

<td>Costa Rica</td>

<td>1</td>

<td>0,08</td>

</tr>

<tr>

<td>Puerto Rico</td>

<td>1</td>

<td>0,08</td>

</tr>

<tr>

<td>Cuba</td>

<td>1</td>

<td>0,08</td>

</tr>

<tr>

<td>Honduras</td>

<td>1</td>

<td>0,08</td>

</tr>

<tr>

<td>Paraguay</td>

<td>1</td>

<td>0,08</td>

</tr>

<tr>

<td>República Dominicana</td>

<td>1</td>

<td>0,08</td>

</tr>

<tr>

<td>

**_Total_**</td>

<td>

**85**</td>

<td>

**7,03**</td>

</tr>

</tbody>

</table>

Los valores que muestra la tabla 1 están en el rango de lo esperable respecto de la baja representatividad latinoamericana en las bases de datos internacionales. En nuestras anteriores investigaciones, las tablas de representatividad estaban encabezadas alternativamente por México-Brasil-Argentina, pero en las Ciencias Antropológicas se rompe ese orden, dado que se incorpora un nuevo país, Perú, en el segundo puesto. De este modo, Brasil pasa a un cuarto lugar, algo alejado de la Argentina y muy cerca de Chile. La preeminencia de México, que duplica el número de títulos indizados en relación con Perú y la Argentina, se puede explicar por la larga tradición de los estudios etnoarqueológicos así como por la riqueza de sus yacimientos y de su población originaria de América. Sin embargo, la representatividad de la Argentina no es despreciable si se la compara con México y Brasil, porque en términos relativos -proporcionales a la cantidad de habitantes de cada país- la Argentina tiene una representatividad, dentro de la región, considerable. (La población estimada para la Argentina es de 36.260.130, para Brasil 186.112.794, para México 106.202.903 y para Perú 28.302.603).

_[Anthropological Literature (AL)](http://www.rlg.org/en/page.php?Page_ID=162)_ es un índice bibliográfico de los artículos incluidos en las revistas y las obras colectivas que recibe la Tozzer Library (Harvard University). Se actualiza trimestralmente, es internacional y los temas principales que incluye son Antropología Cultural/ Social, Arqueología, Antropología Biológica/ Física, Lingüística, Historia, Sociología, Folklore, Geografía Humana y Geología.

Esta base indexa 720 títulos, de los cuales 6 son argentinos: 4 en curso y 2 cesados. La representatividad se calculó con 6 revistas, porque es lógico suponer que la base también liste otros títulos cesados, por lo tanto, R = 0,83\. Mientras que para la visibilidad y la vacancia se han considerado sólo los 4 títulos en curso, de este modo, Vi = 14,81 y Va = 85,19\. Por otra parte, el solapamiento en los títulos indexados por AL es total respecto de AIO, mientras que no hay solapamiento con el IBSS/A. (Véase tabla 12)

<table><caption>

**Tabla 2: Indicadores de representatividad (R) de revistas latinoamericanas en _Anthropological Literature (AL)_**</caption>

<tbody>

<tr>

<th>

_País_</th>

<th>

_Cantidad de títulos indexados_</th>

<th>

_R %_</th>

</tr>

<tr>

<td>México</td>

<td>16</td>

<td>2,22</td>

</tr>

<tr>

<td>Colombia</td>

<td>8</td>

<td>1,11</td>

</tr>

<tr>

<td>Chile</td>

<td>7</td>

<td>0,97</td>

</tr>

<tr>

<td>Perú</td>

<td>7</td>

<td>0,97</td>

</tr>

<tr>

<td>

**Argentina**</td>

<td>

**6**</td>

<td>

**0,83**</td>

</tr>

<tr>

<td>Brasil></td>

<td>6</td>

<td>0,83</td>

</tr>

<tr>

<td>Guatemala</td>

<td>5</td>

<td>0,69</td>

</tr>

<tr>

<td>Venezuela</td>

<td>2</td>

<td>0,28</td>

</tr>

<tr>

<td>Ecuador</td>

<td>2</td>

<td>0,28</td>

</tr>

<tr>

<td>Uruguay</td>

<td>2</td>

<td>0,28</td>

</tr>

<tr>

<td>Costa Rica</td>

<td>1</td>

<td>0,14</td>

</tr>

<tr>

<td>Granada</td>

<td>1</td>

<td>0,14</td>

</tr>

<tr>

<td>Honduras</td>

<td>1</td>

<td>0,14</td>

</tr>

<tr>

<td>Nicaragua</td>

<td>1</td>

<td>0,14</td>

</tr>

<tr>

<td>Panamá</td>

<td>1</td>

<td>0,14</td>

</tr>

<tr>

<td>

**_Total_**</td>

<td>

**66**</td>

<td>

**9,16**</td>

</tr>

</tbody>

</table>

Al comparar la tabla 2 con la anterior, se observa que México sigue encabezando la lista y también duplica en número de títulos indizados al país subsiguiente (Colombia). Este, sin embargo, no coincide con el de la tabla 1, donde Perú ocupaba el segundo lugar. Por nuestra parte, aparentemente, la Argentina ha perdido terreno, dado que descendió del tercero al quinto lugar, pero la realidad es que ese puesto en esta base implica sólo la diferencia de un título. En términos globales _Anthropological Index Online_ es una base mucho más conveniente para acceder a la producción latinoamericana en general y a la argentina en particular, dado que la _Anthropological Literature_ indiza un 22 % menos de revistas de la región. Para la Argentina la diferencia es mayor aún, porque analiza un 45 % de títulos menos y los que analiza se solapan con la base _Anthropological Index Online_. Notablemente, las dos periódicas que no se solapan son las que han cesado su publicación.

_[IBSS International Bibliography of the Social Sciences/Anthropology (IBSS/A)](http://www.lse.ac.uk/collections/IBSS/about/anthropology_cov.htm)_ producida por la London School of Economics and Political Science indexa artículos y reseñas de 575 revistas en curso, especializadas en Ciencias Antropológicas. Su cobertura temporal se retrotrae a 1951\. De la Argentina, hay un solo título, por lo tanto, la representatividad para la disciplina estudiada es de R = 0,17; la visibilidad de Vi = 3,70 y la vacancia de Va = 96,30\. Por otra parte, el único título indexado por IBSS/A se solapa con la lista de títulos indexados por AIO. (Véase tabla 12)

<table><caption>

**Tabla 3: Indicadores de representatividad (R) de revistas latinoamericanas en la _International Bibliography of the Social Sciences/Anthropology (IBSS/A)_**</caption>

<tbody>

<tr>

<th>

_País_</th>

<th>

_Cantidad de títulos indexados_</th>

<th>

_R %_</th>

</tr>

<tr>

<td>Brasil</td>

<td>6</td>

<td>1,04</td>

</tr>

<tr>

<td>México</td>

<td>5</td>

<td>0,87</td>

</tr>

<tr>

<td>

**Argentina**</td>

<td>

**1**</td>

<td>

**0,17**</td>

</tr>

<tr>

<td>Colombia</td>

<td>1</td>

<td>0,17</td>

</tr>

<tr>

<td>Cuba</td>

<td>1</td>

<td>0,17</td>

</tr>

<tr>

<td>Chile</td>

<td>1</td>

<td>0,17</td>

</tr>

<tr>

<td>Puerto Rico</td>

<td>1</td>

<td>0,17</td>

</tr>

<tr>

<td>

**_Total_**</td>

<td>

**16**</td>

<td>

**2,76**</td>

</tr>

</tbody>

</table>

Es evidente que la cobertura que la _IBSS/A_ hace de la región y de su producción científica en el área de las Ciencias Antropológicas es menor a la de las bases anteriores. En cuanto al primer aspecto cubre 7 países a diferencia de los 15 analizados por la _AIO_ y la _AL_. Respecto del segundo rasgo, indiza 16 títulos contra los 85 de la _AIO_ y los 66 de la _AL_. Por su parte, la Argentina está representada con un título que también registra la _AIO_, entonces, la utilidad de esta bibliografía es, prácticamente, nula para acceder a la producción nacional de la disciplina estudiada.

Para facilitar la comparación entre las tres bases unidisciplinarias examinadas se presenta la siguiente tabla que demuestra una mayor cobertura de la región y de nuestro país por el _Anthropological Index Online_:

<table><caption>

**Tabla 4: Características comparativas de las bases internacionales unidisciplinarias**</caption>

<tbody>

<tr>

<th></th>

<th>

_AIO_</th>

<th>

_AL_</th>

<th>

_IBSS/A_</th>

</tr>

<tr>

<td>Títulos latinoamericanos</td>

<td>85</td>

<td>66</td>

<td>16</td>

</tr>

<tr>

<td>Títulos argentinos</td>

<td>11</td>

<td>6<sup>*</sup></td>

<td>1</td>

</tr>

<tr>

<td>Países representados</td>

<td>15</td>

<td>15</td>

<td>7</td>

</tr>

<tr>

<td>Representatividad argentina</td>

<td>0,91</td>

<td>0,83</td>

<td>0,17</td>

</tr>

<tr>

<td>Visibilidad argentina</td>

<td>40,74</td>

<td>14,81</td>

<td>3,70</td>

</tr>

<tr>

<td colspan="4">* 4 en curso y 2 cesados</td>

</tr>

</tbody>

</table>

Cuando se toman las bases de datos pluridisciplinarias (internacionales y latinoamericanas), y unidisciplinarias en su conjunto se cuentan 12 títulos analizados sobre 27 que produce la Argentina en Ciencias Antropológicas, por lo tanto, la visibilidad es de Vi = 44,44 y la vacancia de Va = 55,56\. Respecto del solapamiento, 6 títulos de los 12 son indexados por más de una base de datos simultáneamente. (Véase Tabla 12).  
La respuesta a la pregunta anteriormente formulada ¿existen tasas diferenciales entre las bases de datos pluridisciplinarias y las unidisciplinarias? se puede contestar observando la tabla 5:

<table><caption>

**Table 5: Comparación entre las bases pluridisciplinarias y unidisciplinarias**</caption>

<tbody>

<tr>

<th rowspan="2"></th>

<th colspan="2">Pluridisciplinaria</th>

<th colspan="3">Unidisciplinaria</th>

</tr>

<tr>

<th>Francis</th>

<th>Hapi</th>

<th>AIO</th>

<th>AL</th>

<th>IBSS/A</th>

</tr>

<tr>

<td>

**_R_**</td>

<td>0,13</td>

<td>0,30</td>

<td>0,91</td>

<td>0,83</td>

<td>0,17</td>

</tr>

<tr>

<td>

_**Vi**_</td>

<td>3,70</td>

<td>3,70</td>

<td>40,74</td>

<td>14,81</td>

<td>3,70</td>

</tr>

</tbody>

</table>

Esta investigación, hasta el momento, ha podido determinar los indicadores de representatividad, de visibilidad, de vacancia y de solapamiento de las publicaciones periódicas argentinas de Ciencias Antropológicas en bases de datos internacionales. Si bien, para los cálculos, este estudio no toma en consideración las bases de datos nacionales, en las tablas que se presentan a continuación se ha agregado una columna dedicada a destacar los títulos incluidos tanto en el _Proyecto Padrinazgo de Publicaciones Periódicas Argentinas (4Par)_ [(Unired 2003)](http://cdi.mecon.gov.ar/unired/unired.html) como en el _Índice de Publicaciones de la Facultad de Filosofía y Letras (IPuFiL)_ [(Romanos de Tiratel, Giunti, Contardi y Dibucchianico 1999)](#rom99) y en [SciELO Argentina](http://www.scielo.org.ar) porque en la actualidad, son los proyectos que abarcan más revistas argentinas de Humanidades y Ciencias Sociales. A continuación, se irá discutiendo cada una de las variables consideradas.

Al tomar en cuenta las instituciones editoras del conjunto de 27 revistas argentinas para el área, encontramos que de estas, 15 son publicadas por universidades estatales y 8 por centros estatales de investigación; quedan 4 revistas editadas por entidades privadas (universidades, asociaciones, fundaciones, etc.). (Tabla 6).

<table><caption>

**Tabla 6: Títulos indexados según el tipo de entidad editora**</caption>

<tbody>

<tr>

<th>

_Entidades responsables_</th>

<th>

_Títulos_</th>

<th>

_Indexados en BD internacionales_</th>

<th>

_Indexados en BD nacionales_</th>

</tr>

<tr>

<td>Universidades estatales</td>

<td>15</td>

<td>6</td>

<td>5</td>

</tr>

<tr>

<td>Centros estatales de investigación, academias, etc.</td>

<td>8</td>

<td>5</td>

<td>-</td>

</tr>

<tr>

<td>Asociaciones, institutos, etc. privados; fundaciones</td>

<td>4</td>

<td>1</td>

<td>-</td>

</tr>

<tr>

<td>

**_Total_**</td>

<td>

**_27_**</td>

<td>

**12**</td>

<td>

**5**</td>

</tr>

</tbody>

</table>

<table><caption>

**Tabla 7: Visibilidad (Vi) según el tipo de entidad editora**</caption>

<tbody>

<tr>

<th>

_Entidades responsables_</th>

<th>

_Títulos_</th>

<th>

_Indexados en BD internacionales_</th>

<th>

_Vi %_</th>

<th>

_Indexados en BD internacionales %_</th>

</tr>

<tr>

<td>Entidades estatales</td>

<td>23</td>

<td>11</td>

<td>40,74</td>

<td>47,82</td>

</tr>

<tr>

<td>Entidades privadas</td>

<td>4</td>

<td>1</td>

<td>3,70</td>

<td>25</td>

</tr>

<tr>

<td>

**Totales**</td>

<td>

**27**</td>

<td>

**12**</td>

<td>44,44</td>

<td>-</td>

</tr>

</tbody>

</table>

Del análisis de la tabla 7 se desprende que la visibilidad de las revistas publicadas por entidades estatales es de Vi = 40,74 mientras que la de las entidades privadas es de Vi = 3,70\. Al sacar los porcentajes de títulos indexados en cada fila de la tabla se ve que, en cada caso, siguen la misma tendencia de la visibilidad: se analiza el 47,82 % de revistas publicadas por entidades estatales y el 25 % por entidades privadas. De estos números se deduce que es mayor la visibilidad y, por lo tanto, el porcentaje de títulos indexados dentro de los publicados en la esfera estatal. Respecto del lugar de edición, Buenos Aires absorbe la mayor cantidad de títulos, casi tantos como los del resto del país sumados; además tiene un 66 % de títulos indexados y una visibilidad de Vi = 29,62\. En el resto del país se publican 15 títulos, de los cuales 4 están indexados lo que representa un porcentaje de 26,66 y una Vi = 14,81\. Estos títulos analizados representan la mitad de los títulos producidos en La Plata, Mendoza y Olavarría, y el total del de Córdoba. (Tabla 8)

<table><caption>

**Tabla 8: Visibilidad (Vi) por lugar de edición**</caption>

<tbody>

<tr>

<th>

_Lugar_</th>

<th>

_Títulos_</th>

<th>

_Indexados en BD internacionales_</th>

<th>

_Indexados en BD nacionales_</th>

<th>

_Vi %_</th>

<th>

_Indexados en BD internacionales %_</th>

</tr>

<tr>

<td>Buenos Aires</td>

<td>12</td>

<td>8</td>

<td>4</td>

<td>29,62</td>

<td>66</td>

</tr>

<tr>

<td>La Plata</td>

<td>2</td>

<td>1</td>

<td>-</td>

<td>3,70</td>

<td>50</td>

</tr>

<tr>

<td>Mendoza</td>

<td>2</td>

<td>1</td>

<td>1</td>

<td>3,70</td>

<td>50</td>

</tr>

<tr>

<td>Olavarría</td>

<td>2</td>

<td>1</td>

<td>-</td>

<td>3,70</td>

<td>50</td>

</tr>

<tr>

<td>Rosario</td>

<td>2</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Salta</td>

<td>2</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Córdoba</td>

<td>1</td>

<td>1</td>

<td>-</td>

<td>3,70</td>

<td>100</td>

</tr>

<tr>

<td>Jujuy</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Posadas</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>San Miguel de Tucumán</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Tilcara</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**27**</td>

<td>

**12**</td>

<td>

**5**</td>

<td>-</td>

<td>-</td>

</tr>

</tbody>

</table>

¿Qué sucede con la permanencia del título en el tiempo? ¿Los títulos más antiguos tienen más chances de ser indexados que los más nuevos? Para contestar estos interrogantes se ha dividido desde 1930 a la fecha en segmentos de 10 años; a primera vista lo que resalta es la proliferación de títulos concentrados en los últimos 20 años (1980-2000) coincidiendo con el restablecimiento de la democracia en la Argentina. Lo segundo es que todos los títulos aparecidos entre 1940 y 1979, 6 revistas, se indexan sin excepción. Mientras que a partir de 1980 se revierte esta tendencia porque los títulos indexados bajan de un 100 % en los tres períodos previos a un 45 % en los últimos 24 años. Esta diferencia se explica por la persistencia temporal de los títulos creados en el período 1940-1979\. (Tabla 9)

<table><caption>

**Tabla 9: Visibilidad (Vi) por año de inicio de las revistas**</caption>

<tbody>

<tr>

<th>

_Año de inicio_</th>

<th>

_Títulos_</th>

<th>

_Indexados en BD internacionales_</th>

<th>

_Indexados en BD nacionales_</th>

<th>

_Vi %_</th>

<th>

_Indexados en BD internacionales %_</th>

</tr>

<tr>

<td>1930-1939</td>

<td>2</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>1940-1949</td>

<td>2</td>

<td>2</td>

<td>2</td>

<td>7,40</td>

<td>100</td>

</tr>

<tr>

<td>1960-1969</td>

<td>3</td>

<td>3</td>

<td>-</td>

<td>11,11</td>

<td>100</td>

</tr>

<tr>

<td>1970-1979</td>

<td>1</td>

<td>1</td>

<td>-</td>

<td>3,70</td>

<td>100</td>

</tr>

<tr>

<td>1980-1989</td>

<td>4</td>

<td>2</td>

<td>-</td>

<td>7,40</td>

<td>50</td>

</tr>

<tr>

<td>1990-1999</td>

<td>10</td>

<td>4</td>

<td>3</td>

<td>14,81</td>

<td>40</td>

</tr>

<tr>

<td>2000-</td>

<td>5</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**27**</td>

<td>

**12**</td>

<td>

**5**</td>

<td>

**44,42**</td>

<td>-</td>

</tr>

</tbody>

</table>

Para la frecuencia, si se observa la tabla 10, se verá que el 63 % de los títulos se agrupan en las frecuencias anual y semestral, con 7 revistas indexadas en bases de datos internacionales, mientras que el restante 37 % se reparte en las frecuencias bienal, trimestral, mensual (electrónica) e irregular con 5 títulos indexados.

<table><caption>

**Tabla 10: Visibilidad (Vi) por frecuencia**</caption>

<tbody>

<tr>

<th>

_Frecuencia_</th>

<th>

_Títulos_</th>

<th>

_Indexados en BD internacionales_</th>

<th>

_Indexados en BD nacionales_</th>

<th>

_Vi %_</th>

<th>

_Indexados en BD internacionales %_</th>

</tr>

<tr>

<td>Bienal</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Anual</td>

<td>11</td>

<td>3</td>

<td>2</td>

<td>11,11</td>

<td>27,27</td>

</tr>

<tr>

<td>Semestral</td>

<td>5</td>

<td>4</td>

<td>2</td>

<td>14,81</td>

<td>80</td>

</tr>

<tr>

<td>Trimestral</td>

<td>2</td>

<td>2</td>

<td>-</td>

<td>7,40</td>

<td>100</td>

</tr>

<tr>

<td>Mensual</td>

<td>1</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Irregular</td>

<td>7</td>

<td>3</td>

<td>1</td>

<td>11,11</td>

<td>42,85</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**27**</td>

<td>

**12**</td>

<td>

**5**</td>

<td>

**44,43**</td>

<td>-</td>

</tr>

</tbody>

</table>

Tal como se observa en la tabla 11, el número de títulos con arbitraje ([Listado de revistas argentinas de Ciencias Sociales y Humanidades con arbitraje](http://www.caicyt.gov.ar/issn/documentos/revargcsh.pdf/view)). duplica a los que no lo tienen y, como era de esperarse, esto influye en una mayor visibilidad de las revistas que siguen las pautas de evaluación académica.

<table><caption>

**Tabla 11: Visibilidad (Vi) por sistema de arbitraje**</caption>

<tbody>

<tr>

<th>

_Arbitraje_</th>

<th>

_Títulos_</th>

<th>

_Indexados en BD internacionales_</th>

<th>

_Indexados en BD nacionales_</th>

<th>

_Vi %_</th>

<th>

_Indexados en BD internacionales %_</th>

</tr>

<tr>

<td>Con arbitraje</td>

<td>18</td>

<td>10</td>

<td>5</td>

<td>37,03</td>

<td>55,55</td>

</tr>

<tr>

<td>Sin arbitraje</td>

<td>9</td>

<td>2</td>

<td>-</td>

<td>7,40</td>

<td>22,22</td>

</tr>

<tr>

<td>

**Total**</td>

<td>

**27**</td>

<td>

**12**</td>

<td>

**5**</td>

<td>

**44,43**</td>

<td>-</td>

</tr>

</tbody>

</table>

Las variables que se han puesto a consideración: entidades responsables, lugar de edición, año de inicio del título, frecuencia y sistema de arbitraje, aplicadas a un pequeño universo de títulos da resultados dispares que impiden generalizar a partir de los mismos. La historia de cada una de las revistas argentinas en las diferentes disciplinas, que se agrupan dentro de las Ciencias Humanas, está atravesada por factores más aleatorios que lógicos, por lo cual se impone la necesidad de confrontar los datos cuantitativos con estudios cualitativos que completen el panorama y ayuden a explicar las irregularidades.

## Conclusiones

Las Ciencias Antropológicas, a nivel internacional, cuentan con cuatro bases de datos unidisciplinarias, de las cuales una no indexa títulos latinoamericanos (Anthropological Abstracts), y otra analiza un solo título argentino (IBSS/A), por lo tanto, conviene tomar en cuenta las otras dos bases para sacar algunas conclusiones.

Dado que _AL_ indexa cuatro títulos que también son analizados por el _AIO_ la alternativa para una biblioteca argentina es esta última base donde 11 revistas académicas, de las 27 dedicadas a las Ciencias Antropológicas, están incluidas en su listado. Por otra parte, los servicios bibliográficos nacionales, para la disciplina, no ayudan a ampliar el número anterior de revistas indexadas, porque se repiten los títulos analizados. En consecuencia, quedarían 16 títulos sin visibilidad, esto dificulta la difusión y el acceso a la producción científica argentina en el campo estudiado en este artículo. Además, la identificación de estas revistas permitirá que los editores científicos mejoren las estrategias de difusión de sus publicaciones.

Cuando enmarcamos las conclusiones de estas investigaciones dentro de las formulaciones teóricas de Keresztesi, el caso argentino presenta una situación paradójica porque las Ciencias Antropológicas han alcanzado, hace ya tiempo, su estadio de establecimiento: están bien asentadas en las Universidades; se organizan en departamentos autónomos; han formalizado rígidamente sus programas educativos, requerimientos y calificaciones; desarrollan curricula estructurados; implementan programas de grado y de postgrado (doctorados, maestrías, especializaciones); la investigación está institucionalizada en centros, departamentos, cátedras e institutos; existe una estratificación académica; y se obtienen contratos de investigación gubernamentales y privados ([Keresztesi 1982](#ker82): 8-13). Sin embargo, la estructura bibliográfica de las Ciencias Antropológicas en la Argentina no concuerda con este estadio, dado que el aparato de la información proporciona niveles limitados de satisfacción de las necesidades de los estudiosos, el nivel de acceso bibliográfico dista mucho de ser sofisticado o siquiera medianamente aceptable, y las consideraciones políticas y de servicio están ausentes en los planes de las instituciones dedicadas al estudio de esta disciplina. Porque, los servicios nacionales actualmente en curso, _Proyecto padrinazgo de publicaciones periódicas argentinas, Índice de Publicaciones de la Facultad de Filosofía y Letras_ y _SciELO Argentina_ son pluridisciplinarios y además, el segundo de ellos se limita a indexar la producción editorial de una Facultad en particular.

Una vez más, el decurso de esta investigación, tal como se formulara en sus objetivos, aporta datos comprobados para la toma de decisiones, facilitando así las tareas tanto de los responsables de las unidades de información cuando desarrollan sus colecciones, como las de las entidades que deberían asumir el control de la bibliografía argentina especializada en Humanidades y Ciencias Sociales.

Por otra parte, sería conveniente que aquellos bibliotecarios de universidades y centros especializados en Antropología, asuman la dimensión bibliográfica de la disciplina como una de sus tareas fundamentales y coordinen sus esfuerzos para lograr el control de la literatura publicada en revistas argentinas, convirtiéndose de este modo en copartícipes de la producción del conocimiento y en agentes activos de su difusión.

## Listado de revistas argentinas especializadas en Ciencias Antropológicas

<table><caption>

**Tabla 12: Listado de revistas argentinas especializadas en Ciencias Antropológicas**</caption>

<tbody>

<tr>

<th>

_Título_</th>

<th>

_ISSN_</th>

<th>

_Indexados por BD internacionales_</th>

<th>

_Indexados por BD nacionales_</th>

</tr>

<tr>

<td>Anales de Arqueología y etnología, Mendoza</td>

<td>0325-0288</td>

<td>AL - AIO</td>

<td>4P</td>

</tr>

<tr>

<td>Andes, Salta</td>

<td>0327-1676</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Archivos, Buenos Aires</td>

<td>1668-4737</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Arqueología, Buenos Aires</td>

<td>0327-5159</td>

<td>AIO</td>

<td>IPuFIL - 4P</td>

</tr>

<tr>

<td>Avá, revistas de Antropología, Posadas</td>

<td>1515-2413</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Claroscuro, Rosario</td>

<td>1666-1842</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Cuadernos de Antropología Social, Buenos Aires</td>

<td>0327-3776</td>

<td>AIO</td>

<td>IPuFIL-SciELO AR</td>

</tr>

<tr>

<td>Cuadernos del Instituto Nacional de Antropología, Buenos Aires</td>

<td>0570-8346</td>

<td>AIO</td>

<td>-</td>

</tr>

<tr>

<td>Estudios Sociales del NOA, Tilcara</td>

<td>0329-8256</td>

<td>-</td>

<td>IpuFIL</td>

</tr>

<tr>

<td>Etnia, Olavarría</td>

<td>0046-2632</td>

<td>AL - AIO</td>

<td>-</td>

</tr>

<tr>

<td>Intersecciones en Antropología, Olavarría</td>

<td>1666-2105</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Kallawaya, Salta-La Plata</td>

<td>s.n.</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Memoria Americana, Buenos Aires</td>

<td>0327-5752</td>

<td>AIO</td>

<td>IPuFIL</td>

</tr>

<tr>

<td>Mitológicas, Buenos Aires</td>

<td>0326-5676</td>

<td>AIO - IBSS/A</td>

<td>-</td>

</tr>

<tr>

<td>Mundo de antes, San Miguel de Tucumán</td>

<td>1514-982X</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Noticias de Antropología y Arqueología, Buenos Aires</td>

<td>0329-0735</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Pacarina : Arqueología y Etnografía Americana, San Salvador de Jujuy</td>

<td>1667-4308</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Pinaco: Programa de Investigaciones sobre Antropología Cognitiva, La Plata</td>

<td>1668-1754</td>

<td>AIO</td>

<td>-</td>

</tr>

<tr>

<td>Publicaciones - Arqueología, Córdoba</td>

<td>0326-4572</td>

<td>AIO</td>

<td>-</td>

</tr>

<tr>

<td>Relaciones. Sociedad Argentina de Antropología, Buenos Aires</td>

<td>0325-2221</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Revista Argentina de Antropología Biológica, Buenos Aires</td>

<td>1514-7991</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Revista de Investigaciones Folkóricas, Buenos Aires</td>

<td>0327-0734</td>

<td>HAPI - FR</td>

<td>-</td>

</tr>

<tr>

<td>Revista de la Escuela de Antropología, Rosario</td>

<td>s.n</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Revista del Museo de La Plata. Sección Antropología, La Plata</td>

<td>0376-2149</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td>Runa, Buenos Aires</td>

<td>0325-1217</td>

<td>AL - AIO</td>

<td>IPuFIL</td>

</tr>

<tr>

<td>Scripta Ethnologica, Buenos Aires</td>

<td>0325-6669</td>

<td>AL - AIO</td>

<td>-</td>

</tr>

<tr>

<td>Xama, Mendoza</td>

<td>0327-1250</td>

<td>-</td>

<td>-</td>

</tr>

<tr>

<td colspan="4">

AIO: _Anthropological Index Online_  
AL: _Anthropological Literature_  
4PAr : _Proyecto Padrinazgo de publicaciones periódicas argentinas_  
FR: _Francis_  
IBSS/A: _International Bibliography of the Social Sciences/Anthropology_  
IPuFIL: _Indice de Publicaciones de la Facultad de Filosofía y Letras_  
SciELO AR: _SciELO Argentina_</td>

</tr>

</tbody>

</table>

## Agradecimientos

Las autoras desean agradecer el subsidio otorgado al Proyecto de investigación aprobado por la Secretaría de Ciencia y Técnica de la Universidad de Buenos Aires, e incorporado a la Programación Científica UBACYT 2004-2007 (F056): _Indicadores de visibilidad de la producción científica aplicados a las revistas argentinas_. Directora: Susana Romanos de Tiratel. También desean agradecer a Alicia Balsells la traducción al inglés del abstract de este artículo.

## Referencias bibliográficas

*   <a id="arm01"></a>Armendáriz Sánchez, S. & Ordoñez Alonso, M. M. (2001). [Las revistas académicas de Historia en Hispanoamérica: un punto de vista](http://clio.rediris.es/articulos/las_revistas_academicas_de_histo.htm). _Clio_ No. 24\. Retrieved 22 May, 2003 from http://clio.rediris.es/articulos/las_revistas_academicas_de_histo.htm
*   <a id="bor06"></a>Borrego, A. & Urbano, C. (2006). [La evaluación de revistas científicas en Ciencias Sociales y Humanidades.](http://www.filo.uba.ar/contenidos/investigacion/institutos/inibi_nuevo/ICS14abs_borrego.htm) _Información, Cultura y Sociedad_, No. 14, 11-27.
*   <a id="caj97"></a>Cajaraville, M. C. (1997). [Publicaciones periódicas. Antropología Social y Democracia, las revistas de Antropología en Buenos Aires, 1984-1995.](http://www.naya.org.ar/articulos/epistem01.htm) _Noticias de Antropología y Arqueología_, **2**(16). Retrieved 23 August, 2006 from http://www.naya.org.ar/articulos/epistem01.htm
*   <a id="caj98"></a>Cajaraville, M. C. (1998). Los procesos informacionales en la constitución del campo de la Antropología Social en Argentina: un abordaje bibliotecológico sobre las revistas especializadas en el período 1983-1995\. _Redes: revista de Estudios Sociales de la Ciencia_, **5**(11), 207-230.
*   <a id="cet95"></a>Cetto, A. M. & Hillerud, K.-I. (1995). _Publicaciones científicas en América Latina_ [Scientific journals in Latin America] (1a. ed.). Paris: International Council of Scientific Unions.
*   <a id="cet99"></a>Cetto, A. M. & Alonso, O. (1999). _Publicaciones científicas en América Latina_ [Scientific journals in Latin America]. (2a. ed.). Paris: International Council of Scientific Unions.
*   <a id="fuen95"></a>Fuenmayor P. & Abdel, M. (1995). [Investigar y publicar](http://www.interciencia.org/v20_01/ensayo02.html). _Interciencia_, **20**(1), 40-46\. Retrieved 23 June, 2003 from http://www.interciencia.org/v20_01/ensayo02.html
*   <a id="gal83"></a>Galeotti, E. & Davasse, M. E. (1983). Aportes para un índice bibliográfico de Antropología argentina. _Scripta Ethnologica_, No. 7, 85-145.
*   <a id="gar98"></a>García, A. & Román, A. (1998). Las publicaciones periódicas de Historia Antigua, Prehistoria y Arqueología: difusión internacional. _Trabajos de Prehistoria_, **55**(1), 139-146.
*   <a id="gim99"></a>Giménez, E., Román, A. & Sánchez, J.M. (1999). Aplicación de un modelo de evaluación a las revistas científicas españolas de Economía: una aproximación metodológica. _Revista Española de Documentación Científica_, **22**(3), 309-324.
*   <a id="gim00"></a>Giménez, E., Román, A. (2000). Evaluación de revistas científicas: análisis comparativo de dos modelos y su aplicación a cinco revistas españolas de Biblioteconomía y Documentación. _Interciencia_, **25**(5), 234-241.
*   <a id="gra00"></a>Graças Targino, M. das & Garcia, J. C. R. (2000). Ciência brasileira na base de dados do Institute for Scientific Information (ISI). _Ciência da Informação_, **29**(1), 103-117.
*   <a id="hau00"></a>Haupt, C. (2000). [La 8ª Reunión sobre las Revistas Académicas y de Investigación. Las revistas científicas latinoamericanas: su difusión y acceso a través de bases de datos.](http://www.webcitation.org/5RBrEh8GM) _Biblioteca Universitaria_, **3**(4), 122-127\. Retrieved 23 June, 2003 from http://www.dgbiblio.unam.mx/servicios/dgb/publicdgb/bole/fulltext/volIII2/octava.PDF
*   <a id="hor06"></a>Hortal Muñoz, C. (2006). Criterios de selección de revistas utilizados por las base de datos internacionales que incluyen revistas españolas de ciencias sociales y humanidades. _Revista Española de Documentación Científica_, **29**(3), 409-422\.
*   <a id="hou75"></a>Houghton, B. (1975). _Scientific periodicals: their historical development, characteristics and control._ London: Linnet Books and C. Bingley.
*   <a id="ina86"></a>Instituto Nacional de Antropología. (1986). _Índice de publicaciones periódicas de Antropología_. Buenos Aires: Instituto Nacional de Antropología.
*   <a id="ker82"></a>Keresztesi, M. (1982). The science of bibliography: theoretical implications for bibliographic instruction. In C. Oberman & K. Strauch (Eds.), _Theories of Bibliographic Education._ (pp. 1-26). New York, NY: R.R. Bowker.
*   <a id="kra95"></a>Krauskopf, M. & Vera, M. I. (1995). [Las revistas latinoamericanas de corriente principal: indicadores y estrategias para su consolidación.](http://www.interciencia.org/v20_03/art05/index.html) _Interciencia_, **20**(3), 144-148\. Retrieved 23 June, 2003 from http://www.interciencia.org/v20_03/art05/index.html
*   <a id="lam05"></a>Lamarca, Genaro, et al. (2005). Evaluación de la calidad de las revistas científicas españolas en Humanidades y Ciencias Sociales. _Boletín de la Anabad_, **55**(1-2), 377-391.
*   <a id="nis98"></a>Nisonger, T. E. (1998). _Management of serials in libraries_. Englewood, CO: Libraries Unlimited.
*   <a id="osc03"></a>Osca-Lluch, J. & Mateo, M.E. (2003). Difusión de las revistas españolas de Ciencias Sociales y Humanidades: acercamiento bibliométrico. _Revista General de Información y Documentación_, **13**(1), 115-132.
*   <a id="osc05"></a>Osca-Lluch, J. & Haba, J. (2005). Dissemination of Spanish Social Siences and Humanities journals. _Journal of Information Science_, **31**(3), 230-237.
*   <a id="rat01"></a>Ratto de Sala, M. C. & Dellamea, A. B. (2001). [Difusión, acceso y visibilidad de publicaciones científicas seriadas de Iberoamérica. El sistema Latindex.](http://www.webcitation.org/5RBrnEQN4) _Dominguezia_, No. 17, 377-391\. Retrieved 23 June, 2003 from http://dominguezia.org.ar/volumen/articulos/17-5.pdf
*   <a id="rom03"></a>Romanos de Tiratel, S. (2003). Acceso a la producción argentina en Humanidades y Ciencias Sociales: representatividad en bases de datos internacionales multidisciplinarias. _Investigación Bibliotecológica_, **17**(35), 45-62.
*   <a id="rom05"></a>Romanos de Tiratel, S. & Giunti, G. M. (2005). Las revistas argentinas de Filosofía: visibilidad en bases de datos internacionales. _Información, Cultura y Sociedad_, No. 13, 57-80.
*   <a id="rom03b"></a>Romanos de Tiratel, S., Giunti, G. M. & Parada, A. E. (2003). Las revistas argentinas de Filología, Literatura y Lingüística: visibilidad en bases de datos internacionales. _Ciência da Informação_, **32**(3), 128-139.
*   <a id="rom02"></a>Romanos de Tiratel, S., Giunti, G. M. & Parada, A. E. (2002). Visibilidad de las revistas argentinas en bases de datos internacionales: proyecto UBACYT F28 (Programación científica 2001-2002). _Información, Cultura y Sociedad_, No. 6, 76-83.
*   <a id="rom99"></a>Romanos de Tiratel, S., Giunti, G. M., Contardi, S. & Dibucchianico, A. (1999). _[Índice de las publicaciones de la Facultad de Filosofía y Letras](http://www.filo.uba.ar/contenidos/investigacion/institutos/inibi_nuevo/indice-publ-ffl.htm)_ [Web Page]. Retrieved 23 August, 2006 from http://www.filo.uba.ar/contenidos/investigacion/institutos/inibi_nuevo/indice-publ-ffl.htm
*   <a id="rom04"></a>Romanos de Tiratel, S. & López; N.C. (2004). Las revistas argentinas de Historia: visibilidad en bases de datos internacionales. _Información, Cultura y Sociedad_, No. 11, 95-115.
*   <a id="sau86"></a>Saugy de Kliauga, C. (1986). _Bibliografía antropológica argentina 1980-1985_ (Español-inglés ed.). Buenos Aires: Colegio de Graduados en Antropología. (Serie bibliográfica No. 2).
*   <a id="sut99"></a>Sutton, E. & Foulke, L. (1999). Coverage of Anthropology by major electronic indexes: a comparison. _Reference Services Review_, **27**(11), 134-157.

#### <a id="abs"></a>Abstract

> **Introduction.** A perspective on the scene of periodical publication today and its particularly complex Latin American scenario is attempted, with a focus on Argentinean journals in the field of Anthropology.  
> **Aims.** To establish the degree of visibility, representation and overlapping of Argentinean academic journals in Anthropology in indexing and abstracting services world-wide.  
> **Methodology.** Within Keresztesi's theoretical framework as regards the connection between research and bibliography, quantitative methods have been applied to establish the representation, visibility and vacancy indicators of Argentinean Journals in Anthropology in both single and multidiscipline international bibliographical data bases.  
> **Results.** The percentage of visibility of Argentinean Journals in Anthropology in international data bases is 44.44 %, the Anthropological Index Online being the base in which Argentinean publications in Anthropology have the highest percentage of visibility at 40.74%.  
> **Conclusions.** Data bases in Anthropology present a high degree of overlapping in their indexing of Argentinean publications in the discipline. The data resulting from this research are relevant to the development of reference collections in information units in the discipline of Anthropology. The information will also be valuable to specialist publishers in the discipline of Anthropology in developing their publication strategies.