#### Vol. 12 No. 4, October, 2007

* * *

# Determining the information needs of small and medium-sized enterprises: a critical success factor analysis.

#### [B. A. Sen](mailto:B.A.Sen@sheffield.ac.uk)

Department of Information Studies,  
University of Sheffield,  
Sheffield S10 2TN,  
UK.

#### [R. Taylor](ricky@resourcehouse.co.uk)

Liverpool Business School,  
Liverpool John Moores University,  
Liverpool,  
UK

#### Abstract

> **Introduction.** This paper reports the results of a critical success factor analysis of two small businesses that share staff and premises, operating within the same industry (educational resources), but within different market sectors (early years literacy and religious education).  
> **Method.** Data were collected by interviews with managers and from analysis of documents. The approach is defined and explored as being useful for small to medium sized enterprises in determining their information needs, which are critical to strategy, business development and growth.  
> **Findings.** Nine critical success factors emerged, one of which was _corporate information competence_. Information was also a sub-factor underlying all other critical success factors.  
> **Conclusion.** It is essential for small businesses in today's competitive environment to take a strategic approach to their information needs if they wish to develop and remain competitive. If information expertise is not present within the company it is advisable to invest in that expertise through recruitment, training, partnership, or outsourcing. This study confirms previous research findings relating to the 'critical' role of information in organizations and specifically small companies.

## Introduction

The business environment is constantly changing and evolving. Businesses themselves change over time and as they grow and develop, the information needs of the business will change. At the same time the information systems needed to support growth and development will also need to change ([Vakola and Wilson 2004](#vak04)). It is essential, therefore, to review those information needs so as to continue to align business operations with changing market needs, particularly in a business environment where even small companies face the impacts of globalisation with overseas competition, using world wide supply chains, expanding their own markets internationally and engaging in e-business ([Winch and Bianchi 2006](#win06); [Taylor and Murphy 2004](#tay04); [Simpson and Docherty 2004](#sim04)). The critical success factor (CSF) method can be useful in assessing changing information and strategic needs to meet market demands ([Friesen and Johnson 1995](#fri95)).

### Critical success factors

The classic paper by Daniel ([1961](#dan61)) proposed that, in times of change and growth, companies need to focus on their critical information needs relating to corporate _success factors_ to fully achieve control and management effectiveness. Rockart ([1979](#roc79): 85) developed the concept, popularising the _critical success factor_ method, which identifies 'for any business the limited number of areas in which results, if they are satisfactory, will ensure successful competitive performance for the organization'. They are areas that should receive constant and careful attention by managers in order to monitor performance and improve existing areas of business ([Rockart 1979](#roc79)).

Bullen and Rockart ([1981](#bul81)) identify three main uses for the method: (1) to determine an individual manager's information needs; (2) to aid an organization in its strategic planning process; and (3) to aid an organization in its information systems planning process. However, Friesen and Johnson ([1995](#fri95)) illustrate how the tool can be used as a 'versatile management tool' and be applied to solving a wide range of planning, quality and operational problems across a range of industries.

Five prime sources of critical success factors have been proposed by Bullen and Rockart ([1981](#bul81)): the industry; competitive strategy and positioning; the environment; temporal factors; and managerial position. Further to this, they suggested that factors can be classified by these five aspects, or by internal versus external, or _monitoring_ versus _building_. _Monitoring_ involves the scrutiny of existing situations such as performance management, whilst _building_ factors are those that are needed to plan or change for the future, e.g., the factor, 'Establish and communicate a clear strategy for corporate development' could be classified as _competitive strategy and positioning_, _internal_, and _building_.

### Critical success factors and small and medium-sized enterprises.

The critical success factor method has been widely applied in large organizations ([Saraph _et al._ 1989](#sar89); [Ahire _et al_. 1996](#ahi96); [Black and Porter 1996](#bla96)). The method can have equally useful applications within small or medium-sized businesses in securing and maintaining competitive advantage ([Dickinson and Ferguson 1984](#dic84); [Yusof and Aspinwall 1999](#yus99)) and can help in decision making, developing management information systems, strategic planning and focusing organizational development ([Dickinson and Ferguson 1984](#dic84)).

Yusof and Aspinwall ([1999](#yus99)) proposed ten success factors specifically for small and medium-sized businesses engaged in implementing total quality management; these were established from reviewing past literature. They later surveyed companies to confirm their results and established four main areas that formed a critical core for those companies:

1.  management leadership;
2.  measuring results, progress and performance;
3.  appropriate training for employees; and
4.  adopting a quality assurance system ([Yusof and Aspinwall 2000](#yus00)).

Similarly, Achanga _et al._ ([2006](#ach06)) identified four major success factors for _lean implementation,_ within small and medium-size companies. Lean implementation is the implementation of lean manufacturing, which is a productivity initiative aimed at streamlining operations, improving efficiencies, competitiveness, and reducing costs, resulting in increased customer value ([Womack, _et al_. 1991](#wom91)). Achanga's 'lean implementation' success factors are:

1.  leadership and management strategy;
2.  finance;
3.  organizational culture; and
4.  skills and expertise. ([Achanga 2006: ??](#ach06))

These factors were mirrored byWong and Aspinwall ( [2005](#won05)) who investigated small and medium companies adopting knowledge management. They proposed a set of eleven factors from their survey of twenty-six companies; their top ranking factors were:

1.  management leadership and support;
2.  culture;
3.  strategy and purpose; and
4.  resources. ([Wong and Aspinwall 2005: ??)](#won05)

[Feindt _et al._ (](#won05)[2001](#fei01)) and Jeffcote _et al._ ([2002](#jef02)) identified sets of factors for small companies with fast growth who were adopting e-commerce, which focused heavily on aspects of information content and information technology. Much earlier, Dickinson and Ferguson ([1984](#dic84)) identified the necessity of information, particularly management information systems, for survival, even within small companies, but acknowledged that investment in such systems could not be afforded by small businesses.

Information management was determined to be a 'very' critical factor in Tibar's ([2002](#tib02)) Estonian industry study, crucial for gathering both internal and external information and for the dissemination and management of information within the company and dissemination to stakeholders. The critical nature of information management and information systems to sustaining competitive advantage and achieving change is discussed in the literature ([Karababas and Cather 1994](#kar94); [Huotari and Wilson 2001](#huo01); [Guimaraes 2000](#gui00)). This view is shared by Marchand ([2000](#mar00): 8) who states that 'Effective use of information is critical to how executives manage their companies and create value in their markets'. Information needs vary from manager to manager and change over time ([Rockart 1979](#roc79)).

This paper considers a case of two small companies at a time of growth and change, where, as a result of critical success factor analysis, corporate information competence emerged as a major factor supporting organizational development and diversification into new markets.

### The case study

The project developed from a need to re-engineer business processes to sustain growth. The companies' situation fitted with 'leadership crisis' described in Greiner's ([1972](#gre72)) classic corporate growth model, where initial creativity leads to problems as a company grows. Growing companies find themselves in situations that can no longer be managed through informal communication and they need increased financial control, so the founders of the company are burdened with unwanted management responsibilities, for which they often lack the skills and knowledge critical for corporate development.

To achieve the growth necessary for the two companies in this case study, a [Knowledge Transfer Partnership](http://www.webcitation.org/5RqclUyZa) was set up between the businesses and the School of Business Information at Liverpool John Moores University in the northwest of the UK.. The companies' Directors recognised a skills gap with which they felt the University could help, as the aim of Knowledge Transfer Partnerships is to strengthen competitiveness in UK companies by the stimulation of innovation through collaborative partnerships with a university, college or research organization. The projects can be for between one and three years, with the aim of helping the business make change in an area that is high priority. The Partnership supports the project (from a combination of government and company funding) and places an associate within the company to manage the project.

With any Knowledge Transfer Partnership a detailed work plan is submitted for the duration of the project, in this case twenty-seven months. The project plan included a number of analytical tools to provide a rich picture of the current business situation, e.g., process mapping, information technology and information audit and critical success factor analysis to analyse the current business situation, outline a number of strategic options and design and implement the appropriate information support systems to allow for corporate growth.

### The companies

Company A (suppliers of a complete range of religious artefacts) and Company B (suppliers of early years literacy and educational resources) sell high quality teaching resources to schools, nurseries and colleges. Both companies specialize in many innovative, exclusive designs, particularly in early years resources. Company B has a registered trademark and the company works hard at protecting and exploiting its brand. Both companies also do a small amount of publishing and in-service training on the use of their products. The market is predominantly UK-based currently, although both companies have plans to expand globally.

The two companies are operationally linked, sharing staff, plant and processes, but each is a separate limited company with its own accounts. In addition, Company B has a third Director who is not connected with Company A.

Company A has shown stable and steady growth over the past fifteen years. The introduction of the Company B business over the last few years has compounded the need for a more integrated, streamlined and efficient operation to enable further growth.

The Knowledge Transfer Partnership, therefore, was designed to enable the companies to improve operational efficiency and provide enhanced managerial control and information systems. The Directors would then have both the information and the time to engage in effective strategic planning. The Partnership was designed to provide the company with the necessary analytical, process modelling, information and information systems management and change management skills to successfully redesign and re-engineer their business to ensure its continuing growth and to develop new business opportunities.

The focus of this project was internal (and operational) rather than external. This was a conscious decision on behalf of the Directors to meet the specific needs of their companies and the confines of what was achievable in the time frame of the project. The Directors wanted to get the internal processes right before focussing on external issues. The external factors were planned for a future project with a strategic marketing focus, allowing the companies to expand and develop in a targeted way within the business environment, with the knowledge that their internal structures could support that future development.

What emerged from the initial familiarisation with the corporate environment was a realisation that the lack of business information was more serious than first realised. The situation within the companies was what [Daniel (1961](#dan61)) describes as a common problem, a 'management information crisis', where the relevant data are not available for decision making or performance measurement, creating a management information problem which can hamper or paralyse business. Such problems arise from a gap between the 'static information system' and the changing organizational structure at a time of change and growth ([Daniel 1961](#dan61)). The need to examine the critical aspects of the business had become of primary importance as the information was not available to make decisions to enable the project to proceed as planned.

## Methods

The method used for this analysis was adapted from that used by Caralli, which is based on Rockhart's ([1979](#roc79)) classic research and which is robust and practical for use in small businesses: the method has five phases:

1.  Defining the scope.
2.  Collecting data.
3.  Analysing data.
4.  Deriving CSFs.
5.  Analysing CSFs. ([Caralli 2004: ??](#car04))

### Defining the scope.

The decision was made to identify organizational success factors to aid decision making and strategic direction. Caralli ([2004](#car04): 47) recommends that '..the scope of the exercise must traverse the entire organization so the domain of each executive-level manager is included and considered'. Interviews, therefore, took place with Directors, departmental managers and team supervisors. Interviewing, significant analysis and discussion with the manager are required to successfully determine critical factors ([Caralli 2004](#car04)).

### Collecting the data.

The data collection involved two methods: reviewing critical documents within the organization and conducting interviews with key staff.

#### The document review:

A document review is effective in gaining an understanding of the focus or direction of the organization and its departments. Many organizations document their mission, vision, values and strategic objectives and make these known to stakeholders. This documentation reflects what is important or _critical_ to managers with regard to the organization ([Caralli 2004](#car04)).

The document review included: mission statements, business objectives and the staff appraisal document.

#### The interviews

Eight interviews were carried out with the companies' Directors, departmental managers and team supervisors, ensuring representation from each operational unit. The interview schedule (based on [Rockart 1979](#roc79)) was amended (rewording Question 4, adding Questions 8 and 9) to add value in terms of personal success factors aligned to business goals ([Caralli 2004](#car04)). The rewording of Question 4 was minor, changing the original from 'darkened room' to 'desert island'; the essence of the question remained the same, 'What would you most want to know about the organization... three months later?' The managers had not been familiar with the concept of critical success factors before the exercise.

The interview schedule included the following questions:

1.  What are the critical success factors in your job right now?
2.  In what one or two areas would failure to perform well hurt you most?
3.  In what area would you hate to see something go wrong?
4.  Assume you are on a desert island with no access to the outside world, what would you most want to know about the organization when you were rescued three months later?
5.  What is you personal mission and role in the organization?
6.  What are your most critical goals and objectives?
7.  What are your three greatest business problems or obstacles?
8.  What measures are implemented to determine if your goals and objectives are being met?
9.  What do you hope to achieve with the organization over the next two years?

There was no particular order to the interviews, which, to some extent, was determined by availability. However, the interviews took place within one working day to avoid discussion or collaboration between colleagues that might have affected the content. The interview team were the Partnership associate and an academic supervisor who had been working with the company on the Partnership. The interview schedule was structured, but the team were prepared to ask follow-up questions, or clarifying questions where deemed necessary. The interviewees were asked to consider the differences, if any, between the two companies in their answers.

The participants were encouraged to understand their role in contributing to the process and its importance in the development of the organization. Huotari and Wilson ([2001](#huo01)) discuss the value of critical success factors in relating the information needs of personnel to organizational objectives. This view is supported by Yusuf ([1995](#yus95)), who sees individual factors, such as key skills and good character, needing to be aligned with environmental factors, such as government and political demands. The participants were reassured that their data would be confidential; this was of concern to all participants, as some sensitive issues emerged. As a result of the interviews, the Project Manager and principal interviewer were in possession of facts regarding workplace tensions that may not otherwise have come to light so readily. Establishing trust between the investigators and interviewees allowed for deep and honest responses, which contributed to richness of the data and success of the study. The interviews lasted approximately thirty to forty minutes. All interviews were taped with permission of the participant.

Having two interviewers helped with the discussion of what should be an acceptable course of action. Decisions were made to strip out emotional content and concentrate on factual data for analysis. Tensions and sensitive or personal issues that came to the fore were not acted on as a result of the interviews, it being deemed inappropriate for reasons of confidentiality and professional integrity. However, having being made aware of some tensions before the exercise, the Project Manager had already put in place, just before the interviews took place, staff forums and other communication initiatives to encourage more open communication among staff, giving the opportunity for these issues to re-emerge appropriately within the workplace, at which point it would be legitimate to act on them.

### Organization of data.

The data were cleaned by removing identities, irrelevant comments and emotive statements. This left factual data and valid perceptions descriptive of the corporate situation which could be formed into a series of 'activity statements':

> Activity statements are statements that are harvested from interview notes and documents that reflect what managers do or believe they and the organization should be doing to ensure success. They collectively describe the operational goals, objectives and activities performed by managers throughout the organization or in the operational unit that supports existence and/or attainment of a CSF. ([Caralli 2004](#car04): 65)

Activity statements can be statements that represent particular actions taking place or needing to take place; they can illustrate organizational aims or objectives. The identification and creation of the activity statements takes the raw data and transforms it into manageable statements that can be analysed and form the basis for the critical success factors. For example, the raw data, _we have to be more professional in the way we project our corporate image. I see what Amazon do even with their packing and delivery notes - we should be thinking on those lines, not just brown paper and photocopied notes_ is analysed and transformed into the activity statement: _Present a more professional corporate image._ (See Figure 1 for examples of activity statements.)

The interviews and document analysis generated a total of 479 activity statements. Ninety-seven statements came from the documentation, leaving 372 statements generated by the interviews. Of the total statements, seventy-four were specific to Company A and 113 were specific to Company B, giving 292 statements considered relevant to both companies. At times during the interviews managers had difficulty in separating the two companies in their responses, as they were so intrinsically linked, particularly in certain functions such as operations. At this stage it was useful to have investigators with in-depth knowledge of the companies to clarify responses. The activity statements were managed within spreadsheets.

### Data analysis.

The activity statements were developed and placed into affinity groupings (similar or related statements). Affinity groupings enable the categorisation of data that share common characteristics or qualities so that a common description can be developed on which to base further analysis. It might be necessary to clarify the original statement, so it is often wise to note the origin of each statement. The process used was as follows:

1.  To note the origin of each activity statement, considering the core content, what was intended by the interviewee, what was meant by the statement.
2.  Clarify any ambiguity, without detracting from the immediacy and essence of the original statement.
3.  With each individual statement, make connections between it and similar data considering the essence of each statement in terms of content and meaning, placing each statement into an affinity group. It is helpful here to have more than one person for discussion. Any statements that cannot easily be placed in a grouping, should be left to one side. It may be appropriate to create new group.
4.  Check the statements to ensure they are placed with data with which there is an affinity. Also look for any emerging sub-groups. Duplicates confirm the importance of an issue and help confirm the affinity groupings, but can be eliminated for ease of data management.
5.  Those statements that do not fit into a group must be re-examined. Decisions must then be made to see if they do fit into a group, or if a new group is required. If an individual statement has value it may warrant a success factor of its own.

The data analysis was carried out by the primary investigator, the associate, who had immersed himself in the companies for the previous six months, developing an in-depth knowledge of the businesses, their structures, staff, systems, product and markets. Themes emerged from the affinity groupings: nine main themes emerged, with supporting themes in all groupings.

The supporting themes represent the activity statements in as few a number as possible and are used as a guide to deriving the factors. Group discussion can be helpful in developing the supporting themes, which communicate the key messages, which can be restated concisely as a success factor, e.g., 'Achieving market success through effective strategic sales and marketing'. Caralli ([2004](#car04)) stresses the importance of aiming for the smallest number of factors that accurately and completely characterise the organization. The aim in developing the supporting themes is to clarify the underlying intentions or concepts that the activity statements represent (See Table 1.).

At this stage, the data have been reduced to a small number of supporting themes which are easier to manage than large numbers of activity statements. Establishing the themes from the activity statements is an iterative process, going back and reviewing making sure the meaning and essence of the data is apparent in the themes.

The supporting themes highlight the underlying content or intent of a CSF; they provide a description or definition of a CSF, as shown in Table 1\.

<table><caption>

**Table 1: Examples of activity statements (in affinity groupings) and supporting themes.**</caption>

<tbody>

<tr>

<th>

Activity statements (in affinity groupings - _promotion & marketing affinity group_):</th>

<th>Initial supporting theme</th>

</tr>

<tr>

<td>Increase promotion activities and techniques [possible promotion sub-group]</td>

<td rowspan="3">

Invest in promotion, advertising and branding supporting _strategic marketing_</td>

</tr>

<tr>

<td>Present a more professional corporate image [possible branding sub-group]</td>

</tr>

<tr>

<td>Produce quality marketing material that gives the company its cutting edge [possible branding sub-group]</td>

</tr>

<tr>

<th>

Activity statements: (in affinitygroupings - _sales affinity group_)</th>

<th>Supporting theme</th>

</tr>

<tr>

<td>Increase customer base [Possible customer sub-group]</td>

<td rowspan="3">

Develop _sales capability_</td>

</tr>

<tr>

<td>Increase sales turnover [Possible sales sub-group]</td>

</tr>

<tr>

<td>Develop sales techniques [Possible sales sub-group]</td>

</tr>

</tbody>

</table>

#### Derive critical success factors.

Critical success factors were derived in relation to the main supporting themes and the statements developed and are drawn from the data through the process outlined in Figure 1:

<figure>

![The CSF Process Diagram](../p329fig1.gif)

<figcaption>

**Figure 1: The critical success factor process**</figcaption>

</figure>

The supporting themes are used as a guide for deriving the factors, which are refined versions of the supporting themes. They are concise so as to be useable in a management context, have impact and clarity. They are usually limited to a few words for ease of communication, though for some management purposes a lengthier explanation can help to expand the meaning.

#### Analysis of the critical success factors.

In the analysis of the success factors we looked at relationships between factors and business functions, examining emerging themes and key issues to incorporate into the business strategy. A management team meeting was set up to discuss and disseminate the findings of the analysis. Here the team was able to discuss the issues that had emerged, discuss and refine the final wording of the factors and consider how they should be taken forward and influence the strategic planning process and impact on operational objectives. The factors clearly defined key areas in which the business had to ensure excellence, giving a basis for corporate action. Information and communication appeared to be issues at all levels of the organization and throughout all functions. This having been clarified in the analysis, the process could then be taken forward both operationally and strategically.

This stage of the analysis also served as a check that the factors had been genuinely raised by the interviewees (the management team) and not engineered by the interviewers. It meant that the team had involvement in the final outcome; it helped with communication and dissemination of the results and supported the discussion of key issues.

## Findings

Nine identical factors emerged for both companies and a number of supporting themes emerged for each factor. The supporting themes highlighted differences between the companies, reflected by their different markets, the customer base and the differing nature of the products. These factors also led to some differences in operational requirements. Table 2 sets out the supporting themes and related organizational success factors, showing where differences exist. The differences reflect different products and markets and the resulting operational activities.

<table><caption>

**Table 2: Organizational critical success factors.**</caption>

<tbody>

<tr>

<th>Supporting themes</th>

<th>Organizational success factors</th>

</tr>

<tr>

<td>

Nature of the products **  
Product innovation  
Product management**  
Product knowledge</td>

<td>

**1\. Product supply:** Offer a range of innovative, quality, well priced products appropriate to market need</td>

</tr>

<tr>

<td>

Branding & promotion**  
Company profile  
Strategic marketing**  
Marketing communications**  
Sales capability**  
Market intelligence</td>

<td>

**2\. Market success:** Achieving market success through effective strategic sales and marketing</td>

</tr>

<tr>

<td>

Communication skills  
Management communications  
Customer communications  
Communications with suppliers  
General communications</td>

<td>

**3\. Effective corporate communications:** Develop the culture and systems that support communication throughout the whole organization and with its stakeholders+</td>

</tr>

<tr>

<td>

Customer service  
Customer complaints  
Customer information and communications</td>

<td>

**4\. Customer relations:** Focus on the customer improving customer relations</td>

</tr>

<tr>

<td>

Skills, knowledge, training and development  
Encouragement and reward  
Staff retention  
Staff relations and teamwork  
Management skills and roles  
Appraisal and job descriptions  
Working environment</td>

<td>

**5\. Company morale:** Provide a working environment conducive to high corporate morale</td>

</tr>

<tr>

<td>

Operational processes**  
Delivery and work output  
Equipment, materials and systems  
Stock levels and stock information</td>

<td>

**6\. Efficiency of operations:** Improve efficiency of all business operations</td>

</tr>

<tr>

<td>

Financial information, management & skills  
Cash flow, debtors/credit control  
Sales & purchase ledger  
Overheads  
Customer accounts & invoicing  
Pricing</td>

<td>

**7\. Financial stability:** Maintain financial stability through effective financial management</td>

</tr>

<tr>

<td>

Vision  
Strategic planning and strategic direction  
Strategic communications  
Organizational culture  
Inter-functional co-ordination</td>

<td>

**8\. Strategic management:** Establish and communicate a clear strategy for corporate development</td>

</tr>

<tr>

<td>

Information quality  
Information systems  
Information relating to other CSFs:  
Performance data (operational)  
Decision support (strategic)  
Financial information  
Product information  
Market intelligence  
Stock and product information  
Supplier information  
Customer information</td>

<td>

**9\. Corporate information competence:** Develop information management skills and systems to support business strategy and operations: generate, gather, analyse, disseminate and use the appropriate information effectively, ensuring information security, validity and integrity</td>

</tr>

<tr>

<td colspan="2">

** shows where differences apply between the two companies.  
+stakeholders: refers to Freeman and Reed ([1983](#fre83)), and their 'wide sense' of stakeholders being 'Any identifiable group or individual who can affect the achievement of an organization's objectives or who is affected by the achievement of an organizations' objectives.' e.g. trade associations, employees, customer segments, competitors.</td>

</tr>

</tbody>

</table>

### Information as a critical success factor

#### Corporate information competence

_Corporate information competence_ emerged as a critical factor in itself. Information also emerged as a critical supporting theme, either explicitly, having clear reference to corporate information competence (e.g. _'we need to have management information of the system')_ or implicitly, not immediately identifiable as corporate information competence (e.g. '_I don't have what I need to make decisions on product development_'), throughout all other success factprs.

Examples of activity statements under this heading:

*   **_Establish management information on the system._**
*   **_Improve data accuracy, validity, integrity and security._**

Corporate information competence is defined here as a company or organization having information management skills and systems to support business strategy and operations: generating, gathering, analysiing, disseminating and using appropriate information effectively, ensuring information security, validity and integrity. Information emerged throughout the interviews as being critical for managers at an individual level to enable them to carry out their work and make decisions for future development. A major issue was the lack of information available to managers and the integrity and quality of the information that was to hand. Problems arose such as the inability to make decisions based on sound information and the inability to monitor performance. Staff lack the skills to fully generate, gather, analyse and disseminate information expertly, making them unable to exploit the full information potential to support business development and decision making.

Systems support for information management was identified as being on the whole inadequate. The main system was based on the accounts requirements and developed piecemeal over time. As staff had changed and needs changed, the system had not developed with the company. The potential might exist within the system, through add-on modules and staff training, to provide a system that supports all business functions more fully and in a more integrated way than is currently allowed. An alternative would be to purchase a new system. Specific problems arose with inaccurate data input, gaps in the data, inadequate coding and classification of data and inability of the system to produce easily readable reports with the right information.

#### Product supply

Examples of activity statements:

*   **_Ensure that staff have good product knowledge._**
*   **_Have the information to price products appropriately_**

In general, all staff had good knowledge of the products and this was seen as important, particularly for Company A, dealing with a wide range of religious artefacts and needing to advise and support customers. What was lacking was a clear pricing structure for products and complete and accurate data on best selling products. This lack of management data hindered a planned approach to product development and formal product portfolio analysis.

#### Market success

Examples of activity statements:

*   **_Produce catalogues that are exciting, fun, informative and colourful giving the company its cutting edge._**
*   **_Have excellent market research and knowledge of the educational markets._**

There is no formal strategic market intelligence within the organization. More detailed factual information is needed relating to the companies' position within their relative markets and the success of their products. Focused market research is not within the range of skills and expertise of staff in-house. Markets cannot be targeted efficiently without this market research, hampering the companies' development. Key managers have knowledge of the industry, the competitive environment, specific markets and gather intelligence on an _ad hoc_ basis; but that knowledge is not always communicated throughout the company. Promotion and marketing was also seen as key to providing customers with product information.

#### Effective corporate communications

Examples of activity statements:

*   **_Stock information needs communicating to the customer._**
*   **_Investigate the potential for using email and e-commerce more effectively._**

Two main information aspects emerged as being critical within this theme. First, there was the need for effective communication of information throughout the company to encourage inter-departmental understanding. Secondly, there was need for effective communication of information externally, mainly to the customers to ensure good customer service and from the suppliers, with whom communications can be difficult because of their remote locations (especially for Company A).

In addition, the companies' Websites lacked sophistication and information content, being only a basic shop window, lacking any e-commerce functionality. Company B was further developed than Company A, having started to outsource its Web development to external contract.

#### Customer relations:

Examples of activity statements:

*   **_Be informative._**
*   **_Pass up-to-date information to the customer._**

Having good customer relations was seen as critical. A need to develop the information held about customers so that customer relationship management within the company could be enhanced to support increased sales and marketing activities was identified. This development relies on a good CRM database. Communicating accurate stock and order information was also seen as important in maintaining good customer relations. This information was not always to hand.

#### Company morale

Examples of activity statements:

*   **_Have well trained knowledgeable staff._**
*   **_Have the appropriate technical skills and understanding to carry out the job._**

The critical nature of company morale was strongly reported during the interviews. The working environment was seen as important, as were a number of human resource management functions such as management roles and relationships, skills training and development, and encouragement and reward. The main information-critical aspect of this factor lay in having the information to do an effective job in terms of training and development. Information literacy was a key aspect and was more critical within some business functions, e.g., accounts and finance, than others. Being informed in general about corporate business was also identified as having an impact on corporate morale.

#### Efficiency of operations:

Examples of activity statements:

*   **_Have a good system supporting operations._**
*   **_More accurate stock information needed._**

Stock control data were often inaccurate. This compounded problems of over- or under-stocking and effectiveness in customer supply and delivery. There was some limited information on operational activity, but not detailed enough to allow for efficient monitoring of operations activities or to allow for improvements to be made. Information on and from suppliers was also important and not always available, given the complex and diverse nature of the companies' supply chains, especially Company A, which dealt with a large number of small suppliers worldwide (e.g., rural village co-operatives in less developed countries).

#### Financial stability

Examples of activity statements:

*   **_Keep accurate records._**
*   **_Use information to measure and monitor finances._**

The existing information system is based on the accounts function, which, consequently, is currently best served in terms of information. However, there emerged some problems with data completeness, accuracy, integrity and validity, which illustrated a need for data input discipline. Reporting facilities were identified as inefficient, requiring substantial amounts of staff time to produce meaningful reports. Efficiencies were needed for effective data output as these reports subsequently required time to be invested in data manipulation, cleansing and analysis. Skills in financial information analysis also needed developing.

#### Strategic management

Examples of activity statements

*   **_Establish clear key performance indicators._**
*   **_Use information to improve organizational planning._**

The main critical information issue related to this factor was not having the information needed to make strategic decisions based on sound evidence, expensive mistakes sometimes being made in terms of product or market diversification, purchasing decisions, or strategic options. Much of the decision making was based on sound personal knowledge and experience, but without the underpinning of hard factual evidence.

## Discussion

The nine factors that emerged from this case study were: (1) product supply; (2) market success; (3) effective corporate communications; (4) customer relations; (5) company morale; (6) efficiency in operation; (7) financial stability; (8) strategic management; and (9) corporate information competence. Information was a prevailing theme with _corporate information competence_ emerging as a critical success factor and information a critical supporting theme either explicit or implicit in all other factors.

The critical importance of information in relation to all the case success factors is supported by research from the literature. As early as [1995, Huotari](#hu095) identified information management as one of eight critical success factor in two cases (pharmaceutical industry and publishing industry); the other factors being; personnel, marketing, research and development, resources, productivity and quality and finance. In Huotari's study, the cases are described as _polar_ industries, having very different businesses and organizational goals, yet information management was identified as critical to performance in both cases.

Ledwith ([2006](#led06): 437) states that 'the growth of small firms is most likely to occur through the innovation and development of new product and services'. In her consideration of a number of small firms, she found that the communication of information required to develop new products could be a problematic organizational factor hampering product development. _Product supply_, innovation and development are critical to the small firms in this case study. Holland ([2000](#hol00)) supports the sharing of information and ideas for effective product development, highlighting as dysfunctional behaviour the withholding of information and information gate-keeping. The literature is supportive of the present study's findings of extremely innovative small businesses, with no formal product portfolio analysis and lacking information to enable them to develop their products on sound financial costings, or target them efficiently. Holland ([2000](#hol00)) believes the business environment is one where businesses have to 'innovate or die'. Critical to achieving product success, she proposes the development of cross-functional teams who share and use information.

Team working further enhances _company morale,_ which can contribute positively to the growth and development of SMEs and the establishment of a working environment and culture conducive to excellence ([Pasanen and Laukkanen 2006](#pas06); [Wijewardena and Cooray 1996](#wij96); [Yusof and Aspinwall 1999](#yus99)). Organizational culture was identified by Achanga _et al._ (2006) as one of four key critical factors for achieving 'lean implementation' in their study of ten European organizations. Important to this cultural factor were other aspects such as managerial personality and ability and acceptance of change. One of the prime purposes for critical success factor analysis is to aid strategic planning especially in times of organizational change ([Dickinson 1984](#dic84)). For companies with a small staff and key leadership component, business continuity and succession planning are important issues which can seriously affect business stability ([Sambrook 2005](#samo5)).

_Effective corporate communication_ of information was an important factor affecting individual managers in the two companies. Poor communication of information, particularly at times of growth, development and change, can have a negative impact on all other critical business areas and, ultimately, corporate strategy and performance ([Friesen and Johnson 1995](#fri95); [Holland _et al_. 2000](#hol00); [Ledwith _et al_. 2006](#led06)).

The communication of information was seen as important throughout the supply chain to achieve market success. This view is upheld in the literature, with Stone ( [2003](#sto03)) and Ngai _et al._ ([2004](#nga04)) considering how technology can transform the supplier-customer relationship for businesses. At the customer end of that supply chain, the communication of information was seen as important by managers in maintaining and improving customer relations, a relationship that Jones ([2000](#jon00): 31) insists does not '...just happen. It needs to be managed'. Central to management of the customer relationship is the need for customer information; using information to increase customer satisfaction ([Marchand 2001](#mar01)). This need for customer relationship management information was identified in the activity statements underpinning the _Customer relations_ factor.

Other market intelligence is of equal importance to achieving the factor of _market success,_ as small businesses operate in an increasingly competitive global environment. Even small businesses need to have a systematic approach to the gathering, management and analysis of market intelligence, using competitive, customer and operational information for leverage within the industry ([Marchand 2001](#mar01); [Guimaraes 2000](#gui00)). This information is critical to support decision making and ensure that financial and investment decisions are based on sound evidence. Financial pressures can hamper small companies: Achanga _et al._ ([2006](#ach06)) found the small businesses in his study to be 'financially inept', lacking financial skills, knowledge and resources. Access to in-depth financial data was a problem in the companies studied here and, in agreement with the study by Achanga _et al._ ( [2006](#ach06)), finance emerged as a critical success factor for small companies. Dickinson and Ferguson ([1984](#dic84)) stated that cash was a universal success factor for small businesses, an opinion confirmed by this study, where _finance_ emerged as being critical. Yusof and Aspinwall ([1999](#yus99)) also identified a lack of resources as an ongoing problem faced by small businesses, impacting on negatively on the quality of business operations.

The quality of business operations, efficiency and effectiveness can only be measured if the operations are correctly monitored and that information is gathered, stored and analysed ([Yusof and Aspinwall 2000](#yus00)). This can be seen as a three-stage process: (1) identify success factors; (2) link performances measurements to the factors; and (3) measure only those factors that can be controlled ([Taylor and Convey 1993](#tay93); [Martin 1997](#mar97)). Information needed for the critical _efficiency of operations_ was not always available in this case study. The process of achieving operational efficiency is information-intensive, but crucial to corporate decision making and establishing corporate information competence.

The definition employed in this study for _corporate information competence,_

> a company or organization having information management skills and systems to support business strategy and operations: generating, gathering, analysis, dissemination and use of the appropriate information effectively, ensuring information security, validity and integrity

fits well with Peppard _et al._ ([2000](#pep00)) who propose an 'organization information competence framework' based on three broad domains: information strategy competencies, information exploitation competencies and information systems and technology supply competencies. Peppard _et al._ ([2000](#pep00)) proposed that information competencies should not be a solely information systems function, but should be distributed throughout the whole organization for business value creation. Marchand ([2001](#mar01)) goes further and argues that managers should adopt an information orientation to business strategy to improve business performance, achieve business leadership and compete with information. He sees information orientation as having three elements: information technology practices, information management practices and information behaviour and values that link to improved business value in terms of market share growth, financial performance, product and service innovations and superior company reputation.

Earl ([2000](#ear00): 16) argues that 'every business is an information business' with technology, systems and information itself supporting and determining business strategy. Kirk ([1999](#kir99)) describes the work of managers in small companies as 'information intensive' and argues that managers of such businesses can utilise information, integrating information and business strategy just as successfully as managers in large organizations, when supported by effective information management. From this case study, Company A and Company B show a developing information intensity, they are aware of gaps in their knowledge and skills and recognise the need for more effective utilisation of information and information competencies within their businesses. The analysis has been successful in identifying key areas for strategic focus, with information being critical in all aspects of the business, supporting developments in more integrated information systems, supporting business growth.

### Lessons learned

The findings in this study were limited from the point of view of being from just two companies and reflecting the situation specific to those companies. However, this is in keeping with the wider application of the critical success factors approach in considering business problems as described by Friesen and Johnson ([1995](#fri95)). The research method and findings could still be useful to practitioners to replicate and test within other organizations. Equally, researchers can build on this case study and use the method across a larger number of organizations to test and assess the reliability of both the methods and findings. With this in mind, each organization is different and likely to be at different stages of development, thereby needing sensitivity to each specific case. Qualitative research methods such as this allow for this flexibility in interpreting the findings.

The analysis is intensive and time consuming. In this case, there was a lack of strategic documentation and the interviews proved more useful, giving a closer reflection of reality within the company. To some extent, the documents reflected _how it ought to be_, rather than _how it is_.

In this case, the interviewees were open and honest, sharing a number of sensitive issues and feelings. The need for confidentiality is therefore paramount. It also meant that unless carefully managed, the interviewees unburdened themselves, with a tendency to stray from the question. Interview skills are therefore critical. It is important for interviewers to immerse themselves in the corporate situation, so that a full and thorough understanding of the data can be achieved. All the managers stated they found the exercise useful in helping them to reflect on critical business issues.

The data analysis stage can be lengthy. Care must be taken to strip out only irrelevant data in the data cleansing process. The classification of the activity statements for defining the main themes is important and is an iterative process. A temptation to reduce the data to a bare minimum, i.e., single word factors, should be resisted as this detracts from the understanding of what is critical. The supporting themes provide illustration and description and allow for greater understanding. Establishing the factors can be confounded by overlaps of interest; it is important at this stage of analysis to establish the true _essence_ of what was being said. This may require the researcher to go back to the original data to confirm the context and meaning of the activity statement.

Ranking factors in such small businesses is difficult where individual managers will each have their own agenda to some extent. The ranking is best carried out at the strategic planning stage by the management team.

Further mapping of the factors to organizational functions can establish which functions require more support and strategic development, enabling critical issues to be fed into the organizational strategy. This should be done with the involvement of the key managers and staff

This method used in this study, which was based on that used by Caralli ([2004](#car04)), was manageable within a small business environment. In larger organizations, there would need to be teams of interviewers, who would require training to ensure standardisation. The amount of data gathered in a very large organization might be difficult to manage.

The method is subjective and open to bias which requires skills, discipline and integrity on behalf of the interview team to guard against. The interview team need good communications skills, interview skills, powers of analysis, professionalism, trustworthiness and to have business insight.

The method is time-consuming. The processes and terminology can be confusing and take time to absorb and understand. A commitment is required on behalf of the research team. The analysis stage is also open to bias and because of this is best not carried out by one individual.

However, the data are rich. If properly carried out, they can reveal the real essence of the corporate situation or specific business problem. They give a foundation for corporate decisions to be built on evidence rather than instinct and gut feelings. The data, having been gathered in confidence, are helpful removing the problem of team meetings, where a few individuals are able to dominate the proceedings and push through their agendas, with the possibility of important issues being kept covered up because individuals do not feel able to speak out.

## Conclusion

The method adopted was practical for the small business environment. This case study confirms the value of critical success factors as Bullen and Rockart ([1981](#bul81)) proposed: determining the individual managers' information needs, aiding the strategic planning process and aiding the information systems planning process. The initial aim of this study was to help determine strategic direction; however, the results achieved reaped benefits in all three areas (individual, strategic and information system planning), with managers having knowledge of key areas of performance which, when made explicit through the definition of factors, 'provide a common point of reference for the entire organization' ([Caralli 2004](#car04): 2).

It is essential for small businesses in today's competitive environment to take a strategic approach to their information needs if they wish to develop and remain competitive. If information expertise is not present within the company, it is advisable to invest in that expertise through recruitment, training, partnership, or outsourcing. This study confirms previous research findings relating to the critical role of information in organizations and specifically in small and medium-sized enterprises ([Huotari 1995](#huo95); [Marchand 2001](#mar01); [Wong and Aspinwall 2005](#won05); [Achanga _et al_. 2006](#ach06)).

As a result of this study, mangers have been able to reflect on how their individual needs align with the organizations' goals. Critical areas have been identified for strategic development and information management and systems planning have been identified as being especially critical to improving business performance, supporting strategic development and maintaining competitive advantage.

## References

*   <a id="ach06"></a>Achanga, P., Shelab, E., Roy, R. & Nelder, G. (2006). Critical success factors for lean implementation within SMEs. _Journal of Manufacturing Technology_, **17**(4), 460-471.
*   <a id="ahi96"></a>Ahire, S.L., Golhar, D.Y. & Waller, M.A. (1996). Development and validation of TQM implementation constructs. _Decision Sciences_, **27** (1), 23-56.
*   <a id="bla96"></a>Black, S.A. & Porter, L.J. (1996). Identification of critical factors of TQM. _Decision Sciences_, **27**, (1), 1-21.
*   <a id="bul81"></a>Bullen, C.V. & Rockart, J.F. (1981). _A primer on critical success factors._ Cambridge, MA: Massachusetts Instute of Technology, Sloan School of Management, Centre for Information Systems Research. (CISR No. 69\. Sloan WP No. 1220-81.)
*   <a id="car04"></a>Caralli, R.A. (2004). _The critical success factor method: establishing a foundation for enterprise security management._ Pittsburgh, PA: Carnegie Mellon Software Engineering Institute. (Technical report CMU/SEI-2004-TR-010\. ESR-TR-2004-010.)
*   <a id="dan61"></a>Daniel, D.R. (1961). Management information crisis. _Harvard Business Review,_ **39**(5), 111-121.
*   <a id="dic84"></a>Dickinson, R.A. & Ferguson, C.R. (1984). Critical success factors and small business. _American Journal of Small Business,_ **8**(3), 49-57.
*   <a id="ear00"></a>Earl, M.J. (2000). Every business is an information business. In D.A. Marchand & T.H. Davenport (Eds.), _Mastering information management._ (pp. 16-22). London: Prentice Hall.
*   <a id="fei02"></a>Feindt, S., Jeffcoate, C. & Chappell, C. (2002). Identifying success factors for rapid growth in SME E-commerce. _Small Business Economics_, **19**(1), 51-62.
*   <a id="fre83"></a>Freeman, R.E., & Reed, D.L. (1983). Stockholders and stakeholders: a new perspective on corporate governance. _California Management Review,_ **25**(3), 88-106.
*   <a id="fri95"></a>Friesen, M.E. & Johnson, J.J.A. (1995). _The success paradigm: creating organizational effectiveness through quality and strategy._ Westport, CT: Quorum Books.
*   <a id="gre72"></a>Greiner, L.E. (1972). Evolution and revolution as organizations grow: a company's past has clues for management that are critical to future success. _Harvard Business Review,_ **50**(4),37-46.
*   <a id="gui00"></a>Guimaraes, T. (2000). The impact of competitive intelligence and IS support in changing small business organizations. _Logistic Information Management_, **13**(3), 117-125.
*   <a id="hol00"></a>Holland, S., Gaston, K. & Gomes, J. (2000). Critical success factors for cross-functional teamwork in new product development. _International Journal of_ _Management Reviews_, **2**(3), 231-259.
*   <a id="huo95"></a>Huotari, M.L (1995). _Information management and competitive advantage: a case study approach._ Unpublished doctoral dissertation, University of Sheffield, Sheffield, U.K.
*   <a id="huo01"></a>Huotari, M.L. & Wilson, T.D. (2001). Determining organizational needs: the critical success factors approach. _Information Research_, **6**(3), paper 108\. Retrieved 29 April, 2007 from http://InformationR.net/ir/6-3/paper108.html.
*   <a id="jef02"></a>Jeffcote, J., Chappell, C. & Feindt, S. (2002). Best practice in SME adoption of e-commerce. _Benchmarking: an International Journal_, **9**(2), 1463-5771.
*   <a id="jon00"></a>Jones, C.A. (2000). Extraordinary customer service management: the critical success factors. _Business Perspectives_, **12**(4), 26-31,
*   <a id="kar94"></a>Karababas, S. & Cather, H. (1994). Developing strategic information systems. _Integrated Manufacturing Systems_, **5**(2), 4-11.
*   <a id="kir99"></a>Kirk, J. (1999). [Information in organizations: directions for information management.](http://www.webcitation.org/5RlqsoV7D) _Information Research_, **4**(3), paper 57\. Retrieved 29 April, 2007 from http://informationr.net/ir/4-3paper57.html.
*   <a id="led06"></a>Ledwith, A., Richardson, I. & Sheahan, A. (2006). Small firm-large firm experiences in managing NPG projects. _Journal of Small Business and Enterprise Development._ **13**(3), 425-440.
*   <a id="mar00"></a>Marchand, D.A. (2000). _Competing with information: a manager's guide to creating business value with information content_. Chichester, UK: Wiley.
*   <a id="mar01"></a>Marchand, D.A. (2001). _Information orientation: the link to business performance._ Oxford: Oxford University Press.
*   <a id="mar97"></a>Martin, R. (1997). Do we practise quality principles in the performance measurement of critical success factors? _Total Quality Management,_ **8**(6), 429-444.
*   <a id="nga04"></a>Ngai, E.W.T., Cheng, T.C.E. & Ho, S.S.M. (2004). Critical success factors of web-based supply chain management system: an exploratory study. _Production Planning and Control_, **15**(6), 622-630.
*   <a id="pas06"></a>Pasenen, M. & Laukkanene, T. (2006). Team-managed growing SMEs: a distinct species? _Management Research News_, **29**(11), 684-700.
*   <a id="pep00"></a>Peppard, J., Lambert, R. & Edwards, C. (2000). Whose job is it anyway? Organizational information competencies and value creation. _Information Systems Journal_, **10**, 291-322.
*   <a id="roc00"></a>Rockart, J.F. (1979). Chief executives define their own data needs. _Harvard Business Review_, **57**(2), 81-93.
*   <a id="sam05"></a>Sambrook, S. (2005). Exploring succession planning in small, growing firms. _Journal of Small Business and Enterprise Development,_ **12**(4), 579-594.
*   <a id="sar89"></a>Saraph, J.V., Benson, P.G. & Schroeder, R.G. (1989). An instrument for measuring the critical success factors of quality management. _Decision Sciences_, **20**(4), 810-829.
*   <a id="sto03"></a>Stone, M. (2003). SME e-business and supplier-customer relations. _Journal of Small Business and Enterprise Development,_ **10**(3), 345-353.
*   <a id="sim04"></a>Simpson, M. & Docherty, A.J. (2004). E-commerce adoption support and advice for UK SME. _Journal of Small Business and_ _Enterprise_ _Development,_ **11**(3), 315-328.
*   <a id="tam95"></a>Tamimi, N. & Gershon, M. (1995). A tool for assessing industry TQM practice versus Deming philosophy. _Production and Inventory Management Journal,_ **36**(1), 27-32.
*   <a id="tay93"></a>Taylor, L. & Convey, S. (1993). Making performance measurements meaningful to the performers. _Canadian Manager,_ **18**(3), 22-24.
*   <a id="tay04"></a>Taylor, M. & Murphy, A. (2004). SMEs and e-business. _Journal of Small Business and_ _Enterprise_ _Development_, **11**(3), 280-289.
*   <a id="tib02"></a>Tibar, A. (2002) [Critical success factors and information needs in Estonian industry.](http://www.webcitation.org/5RlqvTPE4) _Information Research_, **7**(4), paper 139\. Retrieved 29 April, 2007 from http://Informationr.net/ir/4/paper138.html.
*   <a id="vak04"></a>Vakola, M. & Wilson, I.E., (2004). The challenge of virtual organization: critical success factors in dealing with constant change. _Team Performance_ _Management_, **10** (5/6), 112-120.
*   <a id="wij96"></a>Wijewardena, H. & Cooray, S. (1996). Factors contributing to the growth of small manufacturing firms: perceptions of Japanese owner/managers. _Journal of Enterprise Culture_, **4**(4), 351-361.
*   <a id="win06"></a>Winch, G.W. & Bianchi, C. (2006). Drivers and dynamic processes for SMEs going global. _Journal of Small Business Development_, **13**(1), 73-88.
*   <a id="wom91"></a>Womack, J.P., Jones, D.T. & Roos, D. (1991). _The machine that changed the world: the story of lean production._ New York, NY: Harper Collins.
*   <a id="won05"></a>Wong, K.W. & Aspinwall, E. (2005). An empirical study of the important factors for knowledge-management adoption in the SME sector. _Journal of Knowledge Management,_ **9**(3), 64-82.
*   <a id="yus99"></a>Yusof, S.M. & Aspinwall, E.M. (1999). Critical success factors for total quality management implementation in small and medium enterprises. _Total Quality Management,_ **10**(4-5), S803-S809.
*   <a id="yus00"></a>Yusof, S.M. & Aspinwall, E.M. (2000). Critical success factors in small and medium enterprises: survey results. _Total Quality Management,_ **11**(4-6), S448-S462.
*   <a id="yus95"></a>Yusuf, A. (1995). Critical success factors for small business: perceptions of South Pacific entrepreneurs. _Journal of Small Business Management,_ **33**(2), 68-73.