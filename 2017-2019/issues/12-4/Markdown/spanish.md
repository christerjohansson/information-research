#### Vol. 12 No. 4, October, 2007

* * *

# Resúmenes en Español / Abstracts in Spanish

#### [¿Qué es realmente "navegación"? Un modelo descriptivo para la investigación científica conductual](paper330.html)  
Marcia J. Bates

> **Introducción.** Se sostiene que los elementos actuales en episodios típicos de "navegación" no han sido capturados bien hasta la fecha por las aproximaciones comunes al concepto.  
> **Método.** Se presentan y analizan atentamente los resultados empíricos de investigación informados por anteriores investigadores.  
> **Análisis.** Basándose en los problemas surgidos en la revisión de la investigación mencionada anteriormente, se analizan atentamente y se desarrollan los componentes de la "navegación". Se ve que la "navegación" consiste en unas series de cuatro pasos, iterados indefinidamente hasta el final de un episodio de "navegación": 1) vislumbrar un campo de visión, 2) seleccionar o probar un objeto físico o informativo dentro del campo de visión, 3) examinar el objeto, 4) obtener el objeto (conceptual y/o físicamente) o abandonarlo. No todos estos elementos necesitan estar presentes en cada episodio de "navegación", aunque múltiples vislumbres se ven que son lo mínimo que constituye el acto.  
> **Resultados.** Se muestra entonces que este concepto de "navegación" tiene soporte persuasivo en la bibliografía psicológica y antropológica, dónde la investigación sobre búsqueda visual, curiosidad y comportamiento exploratorio se encuentra en armonía con esta perspectiva.  
> **Conclusiones.** Se argumenta que esta concepción de "navegación" es más cercana al comportamiento humano real que otras aproximaciones. Se desarrollan las implicaciones para un mejor diseño de sistemas de información.

#### [El uso de Weblogs (blogs) por bibliotecarios y bibliotecas para la diseminación de información](paper323.html)  
Judit Bar-Ilan

> **Introducción.** El Blogging es un fenómeno relativamente nuevo pero que ya ha ganado una alta popularidad. Este artículo informa del uso de blogs (Weblogs) por bibliotecas y bibliotecarios.  
> **Método.** Se compiló una lista de blogs de datos obtenidos de varias listas/directorios en dos intervalos de tiempo, en diciembre de 2003 y febrero de 2005\. Se caracterizaron los blogs y el contenido de un mes de entradas (postings) usando análisis de contenido multi-facetado. Se compararon los blogs identificados en 2003 con los blogs listados en 2005.  
> **Resultados.** En general, los resultados indican que los blogs tienen impacto en las actividades de los profesionales de la información y estos son un nuevo cauce de información para transferir información tanto a los compañeros profesionales como a otros usuarios del Web. Además también sirven como una herramienta eficaz para "promocionar" eventos y recursos de la biblioteca. Los bibliotecarios usan los blogs para diseminar información general y profesional, mientras que las bibliotecas usan a menudo los blogs para anuncios.  
> **Conclusiones.** Las bibliotecas utilizan los blogs de una nueva manera que les permite diseminar la información a sus patrocinadores. Aunque ha habido un aumento considerable en el número de bibliotecas con blogs, el puede esperarse un crecimiento posterior, puesto que en febrero de 2005 sólo una minoría de bibliotecas utilizaba esta herramienta.

#### [Benchmarking del compromiso estratégico con la alfabetización de información en la educación superior: hacia un modelo de trabajo](paper328.html)  
Sheila Corrall

> **Introducción.** La alfabetización en información es un problema importante para las organizaciones en la sociedad de información. Este estudio investigó el compromiso estratégico con este concepto en la educación superior y exploró el desarrollo de indicadores para evaluar el nivel de compromiso de una institución.  
> **Método.** Se realizó un estudio en universidades del Reino Unido (n = 114) para identificar la evidencia de compromiso estratégico hacia la alfabetización en información. La colección de datos se limitó a documentos de dominio público accesibles en sitios Web institucionales, buscados y navegados sistemáticamente.  
> **Análisis.** Se realizó un análisis de contenido cualitativo sobre los datos, relativos a setenta y cinco instituciones. Los datos fueron codificados, clasificados y posteriormente interpretados, usando técnicas de análisis matricial para identificar y registrar los comentarios sobre los temas comunes y las características opuestas.  
> **Resultados.** El estudio descubrió que la alfabetización en información estaba bien representada en las estrategias institucionales sobre información, habilidades del estudiante, y aprendizaje y enseñanza, pero era menos evidente en los atributos del graduado y los documentos de estrategia de investigación. Había diferencias llamativas en los niveles de compromiso y en la calidad de las declaraciones de estrategia. Los resultados fueron usados para definir indicadores de compromiso y desarrollar un marco de evaluación que comprende una herramienta matricial y un modelo visual.  
> **Conclusión.** Las universidades de Reino Unido se están comprometiendo con la alfabetización en información a un nivel estratégico, pero la actuación es desigual en el sector y en las instituciones. Los resultados reflejan dominios de compromiso tratados en la bibliografía, pero también identifica otras áreas de actividad y oportunidades para el desarrollo estratégico alineado al interés actual en los recursos humanos y la transferencia del conocimiento. Se necesita investigación adicional para desarrollar, probar y refinar el marco de evaluación propuesto.

#### [La concentración de los comportamientos de información en línea de usuarios Web](paper324.html)  
Chun-Yao Huang, Yung-Cheng Shen, I-Ping Chiang y Chen-Shun Lin

> **Introducción.** Centrándonos en la concentración conductual de los usuarios Web a través de los sitios Web que han visitado, investigamos la heterogeneidad en los comportamientos de información en línea de los usuarios Web.  
> Método. Se usa el coeficiente de Gini para medir el grado de concentración de comportamientos de información en línea de un usuario Web en términos de páginas vistas y duración de la visita. Exploramos cómo las dimensiones conductuales del número de sitios visitados, el número de páginas vistas por sitio y la duración por página predice la concentración conductual de información en **línea.  
> Análisis.** Se analizan los datos de un panel en línea usando modelos de regresión múltiple que revelan que las tres dimensiones de comportamiento de información en línea predicen más de tres cuartos de las variaciones en concentración conductual.  
> **Resultados.** El número de sitios visitados y el número de páginas vistas por sitio predice el grado de concentración conductual positivamente (en términos de páginas vistas y duración de la visita), mientras que la dimensión de velocidad de comportamiento de información en línea predice positivamente el grado de concentración conductual en términos de páginas vistas pero negativamente en términos de duración de la visita. También se analiza la importancia relativa de las variables en la explicación del grado concentración conductual de usuarios Web.  
> **Conclusión.** El marco analítico cuantitativo presentado proporciona una vista de la heterogeneidad del comportamiento de información en línea. Este artículo es un paso más para un entendimiento más exhaustivo del comportamiento de información en línea desde una macro perspectiva.

#### [Pronosticador de sobrecarga de información sobre cáncer: los resultados de un estudio nacional](paper326.html)  
Kyunghye Kim, Mia Liza A. Lustria , Darrell Burke y Nahyun Kwon

> **Introducción.** Exploramos los pronosticadores de sobrecarga de información entre usuarios buscadores de información sobre cáncer que informaron haber sufrido sobrecarga de información. Estas personas se caracterizaron mediante características socio-demográficas, estado de salud, información de salud y entorno de comunicación y búsqueda de información sobre cáncer conductual, cognoscitiva, y afectiva.  
> **Método.** Se desarrolló un análisis secundario del Estudio de Tendencias Nacionales sobre Información de Salud de 2003 realizado por el Instituto Nacional del Cáncer de Estados Unidos con 6.369 los participantes seleccionados aleatoriamente. Se analizó un subconjunto de datos con las respuestas de 3.011 usuarios buscadores de información sobre cáncer. Se usó análisis bi-variante para identificar los factores significativamente asociados con la sobrecarga de información. En estos factores se utilizaron entonces como entrada en un modelo de regresión logístico para identificar los pronosticadores de sobrecarga. .  
> **Resultados.** Se asoció con la sobrecarga el bajo estatus socio-económico, la salud pobre, la baja atención a los medios de comunicación y los componentes afectivos altos búsqueda de información. Los pronosticadores más fuertes fueron el nivel de educación y los aspectos cognoscitivos de la búsqueda de información, lo que indica que la sobrecarga de información se predice por las habilidades en alfabetización en información en salud. El uso de Internet y la alta atención a los medios de comunicación, dos factores que normalmente se piensa que causan sobrecarga, se encontró que no están asociados con la sobrecarga. .  
> **Conclusión.** Los resultados enfatizan la importancia de la alfabetización informacional en salud en el manejo de la sobrecarga de información e implican la necesidad de diseñar mejores campañas de información sobre salud y sistemas de suministro.

#### [Determinando las necesidades de información de pequeñas y medianas empresas: un análisis de factor de éxito crítico](paper329.html)  
B. A. Sen y R. Taylor

> **Introducción.** Este artículo informa sobre los resultados del análisis de factor de éxito crítico de dos pequeños negocios que comparten personal y local, operando dentro de la misma industria (recursos educativos), pero dentro de sectores de mercado diferentes (la alfabetización en los primeros años y la educación religiosa).  
> **Método.** Los datos se recolectaron mediante entrevistas con gestores y análisis de documentos. La aproximación se define y explora como útil para empresas pequeñas a medianas para la determinación de sus necesidades de información, que son críticas para la estrategia, desarrollo comercial y crecimiento.  
> **Resultados.** Surgieron nueve factores de éxito críticos, uno de los cuales era la competencia en información corporativa. La información también era un sub-factor subyacente a todos los demás factores de éxito críticos.  
> **Conclusión.** En el competitivo entorno actual es esencial para los pequeños negocios adoptar un acercamiento estratégico a sus necesidades de información si quieren desarrollarse y mantenerse competitivos. Si la experiencia (habilidades, competencia) en información no está presente dentro de la empresa es aconsejable invertir en esa experiencia a través de la contratación, entrenamiento, asociación, u outsourcing. Este estudio confirma los resultados de investigaciones anteriores que relacionan el papel crítico de la información en las organizaciones y específicamente en las empresas pequeñas.

#### [¿Qué tipos de noticias atraen a los blogeros?](paper327.html)  
Mike Thelwall, Aidan Byrne y Melissa Goody

> **Introducción.** Se han aclamado a los Blogs como los transformadores potenciales de los valores del periodismo y las noticias. No obstante, a pesar de algunas noticias importantes gestadas en blogs, no está claro los tipos de noticias que se discuten en los blogs y la magnitud de la influencia potencial del blogespacio.  
> **Método.** Muestreamos 556 noticias de cuatro páginas de inicio de sitios Web de noticias en junio de 2006.  
> **Análisis.** Cada noticia era clasificada por tema, tipo de evento y geografía, y se estimó el número de entradas (postings) de blog pertinentes del día de publicación.  
> **Resultados.** Los resultados mostraron un emparejamiento promedio sorprendentemente cercano entre los intereses de los blogeros y la cobertura de la BBC, CNN, LA Times y Fox News, probablemente porque los sitios de noticias tienden a publicar más noticias de tipos populares. El análisis posterior sugirió que los blogs favorecen eventos participativos y perspectivas de derechas, y pueden impulsar la corriente principal de noticias en esta dirección.  
> **Conclusiones.** No se restringen las discusiones de Blog a tipos particulares de noticias, son de amplio rango, aunque son evidentes algunos prejuicios. También recomendamos pautas simples para evaluar si las noticias individuales atraen un interés sobre la media en el "blogespacio".

#### [La "ruta de platino" al acceso abierto: un estudio del caso de E-JASL: The Electronic Journal of Academic and Special Librarianship (Revista Electrónica de Biblioteconomía Académica y Especializada)](paper321.html)  
Paul G. Haschak

> **Introducción.** En 1999, sin dinero y sin apoyo de ninguna organización bibliotecaria, el autor se asoció con el Consorcio Internacional para la Publicación Académica Alternativa (International Consortium for Alternative Academic Publication, ICAAP), posteriormente renombrada como Consorcio Internacional para el Avance de la Publicación Académica (International Consortium for the Advancement of Academic Publication), para fundar una nueva revista electrónica, The Journal of Southern Academic and Special Librarianship (Revista de Biblioteconomía Académica y Especializada del Sureste), renombrada E-JASL: The Electronic Journal of Academic and Special Librarianship en 2002.  
> **Descripción.** Este estudio de casos se basa en las propias experiencias del autor en la fundación y desarrollo de una revista electrónica bibliotecaria profesional, independiente, permanentemente archivada, revisada por pares, y de acceso abierto, empleando un modelo de publicación conducido por académicos (especialistas). Se discute la asociación del autor con el ICAAP enfatizando los beneficios de esta colaboración.  
> **Conclusión.** El ICAAP ha demostrado al mundo que es posible formar una revista académica (especializada) independiente que publica los proyectos fuera de la corriente principal comercial. También, el ICAAP ha mostrado que hay una alternativa al pago de cientos o miles de dólares a editores comerciales para volver a comprar la investigación académica (especializada, erudita) de nuestros colegas en la academia. La alternativa es empezar y/o apoyar los proyectos de publicación de revistas académicas que toman 'la ruta de platino' al acceso abierto. Animamos a todos a trabajar para conseguir una investigación académica libre y libremente accesible en el Web.

#### Traducciones realizadas por [José Vicente Rodríguez](mailto:jovi@um.es) y [Pedro Díaz](mailto:diazor@um.es), Universidad de Murcia, España.

##### Last updated 30th October, 2007