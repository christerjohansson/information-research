#### Vol. 10 No. 1, October 2004

# Choosing people: the role of social capital in information seeking behaviour

#### [Catherine A. Johnson](mailto:)  
School of Information Studies  
University of Wisconsin-Milwaukee  
Milwaukee, Wisconsin 53202, USA

#### **Abstract**

> It is an almost universal finding in studies investigating human information behaviour that people choose other people as their preferred source of information. An explanation for the use of people as information sources is that they are easier to approach than more formal sources and therefore are a _least effort_ option. However there have been few studies that have investigated who the people chosen as information sources are and what their relationship to the information seeker is. This paper reports findings that come out of a larger investigation of the information seeking behaviour of a random sample of residents of Ulaanbaatar, Mongolia. Using the theory of social capital as a conceptual framework and the methods of social network analysis, this study investigated the relational factors associated with the choice of people as information sources. Results indicate that respondents chose people who had better resources than they had and were not well known by them. This suggests that respondents were deliberate in their choice of people information sources and therefore it is speculated that people are not necessarily the least effort option but may require considerable effort to seek out and consult.

## Introduction

It is an almost universal finding in studies investigating human information behaviour that people choose other people as their preferred source of information. Studies of academic researchers in both the sciences and the humanities have revealed the importance of consulting with colleagues at different stages of their research ([Ellis](#ellis), 1993; [Case](#case1991), 1991). Professionals, such as engineers, nurses, physicians and dentists rely on co-workers and knowledgeable colleagues in their search for work-related information ([Leckie, _et al._](#leckie), 1996). Personal sources are also among the most important sources consulted by chief executive officers during their environmental scanning ([Choo](#choo), 1993). Studies of ordinary citizens' preferred sources of information also confirm the importance of personal contacts in information seeking behaviour ([Warner, _et al._](#warner), 1973; [Chen & Hernon](#chen), 1982). The poor, as well, prefer interpersonal sources over other sources of information ([Dervin & Greenberg](#dervin1972), 1972; [Childers](#childers), 1975; [Spink & Cole](#spink), 2001 [Sligo & Jameson](#sligo), 2001; [Agada](#agada), 1999; [Chatman](#chatman), 1996).

The explanation for the use of people as information sources has often been that they are 'typically easier and more readily accessible than the most authoritative printed sources' ([Case](#case2002), 2002: 142). The use of people is a _least effort_ option in the search for information and, therefore, may not be the best sources available ([Childers](#childers), 1975; [Dervin](#dervin1983), 1983). The theory of social capital, however, suggests that the use of people as information sources is not necessarily an easy option, but may also require a considerable effort. This paper looks specifically at the use of personal sources of information and how factors related to social capital, including the strength of the relationship between information seeker and information source, and the resources possessed by the information source, affect who the information seeker chooses to consult. The conceptual framework for this analysis is Lin's ([2001a](#lin2001a)) network theory of social capital.

## The network theory of social capital and information seeking behaviour

The network theory of social capital is rooted in the concepts of social network analysis which provides a series of methodological tools to investigate the relationships, or ties, between individuals. The network of relationships comprises the social network. The theory as proposed by Lin ([2001a](#lin2001a)) explains how the quality of social resources available to an individual within his or her social network influences the success of achieving desired outcomes or goals. Social resources are the goods possessed by individuals in the network and can consist of intangible goods such as social status, research collaboration, and information as well as material goods, such as money or a car. Access to these resources depends on the relationship with the individual possessing the resource and where one is located in the social structure. There are three ingredients fundamental to social capital: resources embedded within the network, access to these resources through relationships, and the use of the resources for purposive action. Social capital, therefore, is defined '...as resources embedded in a social structure which are accessed and/or mobilized in purposive actions' ([Lin](#lin2001b), 2001b: 12). This theory of social capital explains how the social network (or the social structure) constrains or enables access to resources embedded within the network.

The hierarchical nature of the social structure lies at the heart of the network theory of social capital. People who occupy the same level of the hierarchy tend to be more like each other and possess similar resources than people who occupy different levels. It is in the nature of human beings that they prefer to interact with people who are similar to themselves in terms of lifestyles and socio-economic status, a predisposition which Laumann ([1966](#laumann)) calls the principle of homophily. That is, people with whom one interacts more frequently tend to have the same interests, be from the same social class, and have resources similar to one's own. Because of the proclivity of people to interact with others who are similar to themselves, strong ties tend to develop between these individuals rather than between individuals who are dissimilar. Consequently, strong ties tend to link people who are similar, have access to the same resources, and are part of the same social strata ([McPherson, _et al._](#mcpherson), 2001). When seeking different resources, therefore, people need to utilize weak ties.

According to the theory of social capital, however, different resources not only lie outside one's social group they reside in a different level of the social hierarchy. In order to access new resources, therefore, people need to reach up or down in the social hierarchy. While people may use weak ties to access lower status individuals, Laumann's prestige principal states that people prefer to associate with others of similar or somewhat higher social status than they, rather than lower status individuals ([Laumann](#laumann), 1966; [Lin](#lin2001a), 2001a: 69). The importance of weak ties for gaining access to new resources is critical to Lin's conceptualization of social capital and forms the basis for his 'Strength-of-Weak-Ties' proposition. This proposition draws extensively from Granovetter's ([1973](#granovetter)) strength of weak ties argument that stresses the advantage of weak bridging ties for linking into other networks to gain information about new jobs. Lin takes this argument one step further and contends that in order to acquire new and better resources individuals will undertake instrumental action requiring the use of weak ties that reach upwards in the social structure. The search for better information resources may also require the use of weak ties which link to higher status individuals.

This is not to claim, however, that information is not found from consulting close ties. Much information necessary to conduct one's everyday life is often found close to home and from close friends and relatives. When information needs arise a reasonable solution would be to observe how other people in one's group have dealt with similar situations or to ask those close to you for possible explanations or solutions. In most cases, therefore, the situations are resolved to some extent through interactions within one's close network. An information problem becomes apparent when there is not a ready solution; that is, resources within one's close social network are not adequate to resolve the problem. At this stage, if one wants to continue the search to find a solution, one must go further afield. Based on the theory of social capital it is expected that respondents will use their weak ties in order to access people with better quality resources than theirs in terms of their life experience (age and marital status), knowledge (level of education) and employment status when searching for the information they need.

## Methodology

This study was carried out as part of my dissertation research that investigated the everyday information behaviour of residents of Ulaanbaatar, Mongolia. It was conducted in the spring and summer of 2002\. Mongolia is a large, sparsely populated country of two and a half million people in East Asia, wedged between Russia to the north and China to the south. It is a transitional economy that adopted a democratic form of government and free-market reforms following the withdrawal of the Soviet Union in the early 1990s. By World Bank standards, Mongolia is a low-income economy that had a per capita Gross Domestic Product of US$452 in 2002 ([World Bank](#worldbank), 2002). It is also a highly literate country compared with other low-income economies, with a literacy rate of nearly 90% (UNDP & Government of Mongolia, 2000). Ulaanbaatar is the capital and main city of Mongolia with a population of nearly 800,000 ([Bat](#bat), 2002).

## Data collection

Data were collected using a personal network questionnaire that collected both network and information seeking data. It was translated into Mongolian and administered in a face to face format to a stratified random sample[<sup>1</sup>](#note1) of the general population of Ulaanbaatar by research assistants who were undergraduate students in the Department of Library and Information Science at the Cultural College of Ulaanbaatar[<sup>2</sup>](#note2). The questionnaires were used to collect demographic information about the respondents, the names of members of the respondents' social networks, and to identify an information problem and the strategies undertaken to find information about it. Two network matrices were used to collect demographic and relational information on the network members and the strength of relationships between the network members. Because of the length and complexity of the questionnaire, its administration took from one and a half to two hours to complete. There were some inconsistencies in the collection of data by the research assistants particularly related to the identification of network members, which resulted in some missing data. Out of 320 questionnaires that were administered, 313 were complete enough to be used for the study. See Johnson ([2003](#johnson)) for a full description of the data collection methods.

Names of members of the social networks were generated using two methods. The first method, called the _name generator_, elicited names by asking respondents who of their acquaintances, friends or relatives they would call upon in certain situations. These situations involved discussing important matters or borrowing money or household items. They were also asked to name their closest friends. Because of the nature of these situations, names elicited in this way tended to be close ties to the respondent. The second method used to elicit names, called the _position generator_, was devised by Lin & Dumin ([1986](#lin1986)) in order to determine respondents' access to resources. Rather than tying names to specific emotional and exchange situations, names are sought in connection to 'ordered, structural positions salient in a society' ([Lin, _et al._](#lin2001), 2001: 63). These structural positions can be occupations, authorities, work units, classes or sectors. For this study respondents were asked to indicate if they knew anyone well enough to talk to in each of seventeen occupations relevant to Ulaanbaatar, ranging from housemaid at the bottom of the scale to politician at the top end. Names elicited through this method tend to be weak ties of the respondent. Social capital is measured based on how high up the position generator the respondent could reach (reach) and how many different occupations in which they knew people (diversity). The names elicited in this way comprise part of the respondents' social networks and consequently form part of the cohort out of which the respondent chose an information source. Names were also elicited through questions which asked for names of individuals approached for information in the course of seeking information to resolve their information problem. In some cases, the names of these individuals had already been elicited through the two methods mentioned above, and in other cases they were new names added to the social network. To preserve the confidentiality of the respondents, the research assistants replaced the names with code numbers which were recorded in the questionnaire.

Data were collected on each individual included in the respondents' social networks. These consisted of demographic data such as age, sex, marital status, level of education, and employment status. Data were also collected that described the respondent's relationship with the network member. These included role relationship (kin, friend, acquaintance), emotional closeness (close, moderately close, not close), frequency of contact (once a week or more, about once a month, less than three or four times a year), duration of relationship (more than ten years, six to ten years, one to five years, and less than one year), and residential proximity (within walking distance, need transportation to visit, and not in Ulaanbaatar).

The information problem was educed through the critical incident technique ([Flanagan](#flanagan), 1954) that allows the respondents to describe a problem from their own perspective and reports on their behaviour relevant to that particular problem. According to Choo ([1993](#choo): 107), this technique allows the respondents to describe the information problem in their own words and talk about the elements of the problem and the search for information that are meaningful to them. By thinking of a concrete situation, the respondents are able to describe the actual steps taken to find information. This is more effective in discovering how people actually search for information than asking questions about how they would resolve a hypothetical problem. Respondents were asked to, '_think about a recent occasion when you needed information or advice to help deal with a particular situation_'. Some types of problems were suggested as examples: '_information related either to your health or the health of a family member; about finding a job or setting up a business; or information about government programmes related to pensions or welfare entitlements or employment training programmes..._' If none of these situations applied to the respondent, they were asked to indicate '_some other situation when you were able to find information that helped you_'. The respondents were asked to indicate where they went first to get information about this problem. There were given the choice of a person, organization, or media source. When respondents chose an organizational source, they were asked to indicate whether they knew the person they approached and to name that person. The respondents were also asked in follow-up questions whether they approached anyone else they knew for information. All the individuals whom respondents approached for information were included in the analysis.

## Findings

The respondents were almost equally likely to choose each of the three types of information sources as their first choice in their search for information. Thirty-five percent chose people, 34% chose organizations, and 27% chose media sources as their first choice of information source. Fourteen respondents did not indicate an information source. Because studies have found that the poor are associated with particular types of information behaviour, I investigated whether the level of income of the respondents had an influence on the type of information source chosen. A chi-squared test showed that there was no relationship between income and choice of source (chi-squared = 3.658, df = 6, p = .723). Table 1 presents the data.

<table><caption>

**Table 1: Distribution of first choice of source within income (n=286)**</caption>

<tbody>

<tr>

<th colspan="5">

Income per month<sup>**a**</sup></th>

</tr>

<tr>

<th>Source</th>

<th>Less than US$18  
%</th>

<th>$18-45  
%</th>

<th>$46-90  
%</th>

<th>Greater than $90  
%</th>

</tr>

<tr>

<td>People</td>

<td>23</td>

<td>31</td>

<td>38</td>

<td>36</td>

</tr>

<tr>

<td>Organizations</td>

<td>43</td>

<td>41</td>

<td>32</td>

<td>34</td>

</tr>

<tr>

<td>Media</td>

<td>33</td>

<td>28</td>

<td>30</td>

<td>31</td>

</tr>

<tr>

<td>Total</td>

<td>10</td>

<td>25</td>

<td>44</td>

<td>21</td>

</tr>

<tr>

<td colspan="5">

<sup>**a**</sup> I have estimated income in US dollars in order to make it more understandable to readers. At the time of the study US \$1 was equal to 1100 Mongolian tugrogs. The poverty line was set at nearly \$18 per month in 2002 (Bat, 2002) and the average wage was approximately \$50 per month in 1999 (UNDP & Government of Mongolia, 2000). The ranges reflect, roughly, below poverty line, low income, middle income, and upper income.</td>

</tr>

</tbody>

</table>

The absence of an effect of income on information seeking may be owing to the transitional nature of Mongolian society. Since most people are suffering under the effects of the poor economy, income by itself may not be an influential factor in information behaviour as it is in the West. Furthermore, respondents were asked to indicate their individual level of income, while household income may be a more accurate indication of their relative economic status.

I also investigated whether the type of problem experienced by the respondent influenced which information source they chose. The respondents' information problems fell into thirteen categories. For the purposes of the analysis, I focused on the four problems mentioned most frequently by the respondents: looking for work (19%), health-related problems (19%), financial matters (13%) and education (13%). Together these four categories comprised 64% of all problems mentioned by the respondents. I performed a logistic regression analysis on the choice of information source as a function of the type of problem faced by the respondent ([Johnson](#johnson), 2003: 198). The analysis showed that people were significantly more likely to be chosen as information sources when the problem was related to either finance or education, while organizations were more likely to be chosen for health related problems, and media sources when respondents were seeking employment related information.

### Characteristics of people information sources

Ninety-six respondents named 109 people who they approached for information[<sup>3</sup>](#note3). In order to determine how respondents chose their information sources out of the pool of available people in their networks I performed a logistic regression analysis on choice of informant as a function of the demographic characteristics of age, sex, marital status and level of education of the information sources. Logistic regression indicates the characteristics that are most strongly associated with the information sources and are predictive of who is chosen as an information source. The characteristics of the people information sources were compared with the characteristics of all the other people in the networks of the ninety-six respondents. The number of people in the networks of these respondents was 1448\. Logistic regression evaluates the influence of independent variables on an event either happening or not happening; for instance, either choosing one type of information source or not choosing that source. Logistic regression analysis is used when the dependent variable is dichotomous and the independent variables are continuous, dichotomous, or categorical. In this analysis the independent variable, _Age_, is categorical. Each of the categories is assessed for its impact on the outcome or dependent variable (the choice of people information sources) with reference to the youngest age category, _19-30_. _Sex_ is a dichotomous independent variable with one equal to _Male_ and zero equal to not a male, or female. The results of the logistic regression on choice of information source as a function of their age and sex are presented in Table 2.

<table><caption>

**Table 2: Logistic regression analysis of choice of people information sources as a function of age and sex of network members (n=1448).**</caption>

<tbody>

<tr>

<th>Age (ref=19-30)</th>

<th>Odds ratio</th>

<th colspan="2">95% confidence interval</th>

</tr>

<tr>

<td>31-40</td>

<td>2.41***</td>

<td>1.33</td>

<td>4.36</td>

</tr>

<tr>

<td>41-50</td>

<td>2.12**</td>

<td>1.17</td>

<td>3.84</td>

</tr>

<tr>

<td>>50</td>

<td>1.70</td>

<td>0.82</td>

<td>3.55</td>

</tr>

<tr>

<th colspan="4">Sex (Male=1)</th>

</tr>

<tr>

<td> </td>

<td>1.71**</td>

<td>1.11</td>

<td>2.62</td>

</tr>

<tr>

<td colspan="4">

Nagelkerke R<sup>2</sup>=0.03 
**p<.05, 
***p<.01</td>

</tr>

</tbody>

</table>

The interpretation is done in terms of the exponentiated beta coefficients which are equivalent to the odds ratios. Subtracting one from the odds ratio and multiplying by 100 gives the odds of something happening, for instance the odds of an information source being in one age category compared to the odds of their being in the reference category. An odds ratio greater than 1 increases the odds of something happening, while an odds ratio less than 1 lowers the odds of something happening. In this analysis, the odds of choosing an information source aged 31-40 was 140% greater than the odds of choosing an information source aged 19-30, and the odds of choosing someone 41-50 was 110% greater than the odds of choosing someone in the youngest age category. The odds of choosing males as information sources were also 70% greater than the odds of choosing females. The analysis suggests that respondents who chose people as information sources were more likely to choose middle-aged individuals[<sup>4</sup>](#note4) over younger people, and men over women. Middle-aged people may be sources of good quality information with their greater involvement in the work world and their greater life experience than younger members of the network. The preference for this age group over even older individuals may be owing to their greater ability to adapt to the new conditions brought about by transition and therefore their better understanding of how the new system works. The preference for male information sources over females may reflect the greater authority role and status of men in Mongolian society.

I also performed a logistic regression to determine the influence of acquired characteristics—employment, education, and marital status—on the choice of an information source. Table 3 indicates the influence of each of these factors in predicting who gets chosen as an information source.

<table><caption>

**Table 3: Logistic regression analysis of the choice of people information sources as a function of employment status, education, and marital status of the network members. (n=1287<sup>**a**</sup>).**</caption>

<tbody>

<tr>

<th>Employment (Ref = Employed)</th>

<th>Odds ratio</th>

<th colspan="2">95% confidence interval</th>

</tr>

<tr>

<td> Student</td>

<td>0.18</td>

<td>0.02</td>

<td>1.41</td>

</tr>

<tr>

<td> Unemployed</td>

<td>0.67</td>

<td>0.30</td>

<td>1.50</td>

</tr>

<tr>

<th>Education (University = 1)</th>

<td>2.43***</td>

<td>1.41</td>

<td>4.17</td>

</tr>

<tr>

<th>Marital Status (Married = 1)</th>

<td>2.11</td>

<td>0.88</td>

<td>5.08</td>

</tr>

<tr>

<td colspan="4">

Nagelkerke R<sup>2</sup>=0.06
***p<.01</td>

</tr>

<tr>

<td colspan="4">

<sup>**a**</sup> Observations were excluded where respondents indicated they did not know the characteristics of information sources, for example their marital status or level of education.</td>

</tr>

</tbody>

</table>

The analysis indicates that level of education had a significant influence on who was chosen as an information source. University educated people were 140% more likely to be chosen as information sources than others in the respondents' networks. The preference for university educated people as information sources over other members in the network indicates that the respondents made clear choices of whom they approached for information to help resolve their problems. The greater knowledge of these individuals may have suggested to the respondents a greater ability to help them than others in their networks.

It was also expected that the strength of the tie between the respondent and the information source would influence who was approached for information. To determine the effect of relationship factors on the choice of people as information sources, I performed a logistic regression analysis on choice of source as a function of the relationship characteristics: role relationship, emotional closeness, duration of relationship, frequency of contact, and residential proximity. Table 4 presents the results of the analyses.

<table><caption>

**Table 4: Logistic regression analyses of the choice of people information sources as a function of relational factors of closeness, duration of relationship, frequency of contact, and residential proximity. (All factors entered separately).**</caption>

<tbody>

<tr>

<th>Factors</th>

<th>Odds ratio</th>

<th colspan="2">95% confidence interval</th>

</tr>

<tr>

<th>Relationship (Ref = Acquaintance)</th>

<td colspan="3"> </td>

</tr>

<tr>

<td>Kin</td>

<td>0.29***</td>

<td>0.17</td>

<td>0.49</td>

</tr>

<tr>

<td>Friend</td>

<td>0.34***</td>

<td>0.20</td>

<td>0.60</td>

</tr>

<tr>

<th>Closeness (Ref = Close)</th>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Moderately close</td>

<td>2.63***</td>

<td>1.63</td>

<td>4.25</td>

</tr>

<tr>

<td>Not close</td>

<td>5.72***</td>

<td>3.17</td>

<td>10.32</td>

</tr>

<tr>

<th>Duration (Ref = More than 10 years)</th>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>6-10 years</td>

<td>0.67</td>

<td>0.29</td>

<td>1.56</td>

</tr>

<tr>

<td>1-5 years</td>

<td>2.94***</td>

<td>1.77</td>

<td>4.87</td>

</tr>

<tr>

<td>Less than 1 year</td>

<td>4.85***</td>

<td>2.71</td>

<td>8.68</td>

</tr>

<tr>

<th colspan="4">Frequency of contact (Ref=More than once a week)</th>

</tr>

<tr>

<td>About once a month</td>

<td>1.91**</td>

<td>1.10</td>

<td>3.31</td>

</tr>

<tr>

<td>Less than 3 or 4 times a year</td>

<td>3.50***</td>

<td>2.15</td>

<td>5.70</td>

</tr>

<tr>

<th>Residence (Ref = Walkable)</th>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Need transportation</td>

<td>4.69***</td>

<td>2.38</td>

<td>9.21</td>

</tr>

<tr>

<td>Not in Ulaanbaatar</td>

<td>4.19***</td>

<td>1.74</td>

<td>10.06</td>

</tr>

<tr>

<td colspan="4">

**p<.05,
***p<.01</td>

</tr>

</tbody>

</table>

The results of the analyses indicate that for all indicators of closeness, respondents were significantly more likely to choose people as information sources with whom they were less closely related. The odds of choosing relatives or friends were significantly lower than the odds of choosing an acquaintance as an information source. Information sources were less likely to be close ties with the respondent, were more likely to be known by the respondent for less than five years, to be in contact with the respondent less frequently than other members of his or her network, and to live further than walking distance away from the respondent. For all of these closeness indicators, factors associated with weak ties were highly predictive of whom respondents would choose as information sources. In order to find information to help them with their problems respondents went to their weak ties, who presumably possessed the new information not possessed by people with whom they were more closely related.

### Comparison of information sources and respondents

In the previous section we saw that the people respondents chose as information sources differed significantly from other people in their networks in terms of education and tended to be weak ties with the respondents. In this section I investigate how the information seekers (the respondents) differ from the information sources. Since the purposive search for information usually involves seeking new resources, I expected that the principle of homophily would not apply and that people chosen as information sources would have characteristics that are different and, in some senses, better than the respondents. For this analysis I conducted a chi-squared (χ<sup>2</sup>) test that compared the frequency distribution of aggregated characteristics of the respondents with the distribution of the aggregated characteristics of the information sources. The results of the χ<sup>2</sup> tests are presented in Table 5\.

<table><caption>

**Table 5: χ<sup>2</sup> tests comparing respondent characteristics with characteristics of information sources**</caption>

<tbody>

<tr>

<th> </th>

<th>Frequency of respondent characteristics  
n=96  
%</th>

<th>Frequency of info-sources characteristics  
n=109  
%</th>

<th>χ<sup>2</sup></th>

<th>Df</th>

<th>Sig</th>

</tr>

<tr>

<th colspan="6">Age</th>

</tr>

<tr>

<td>19-30</td>

<td>50</td>

<td>21</td>

<td rowspan="4">39.123</td>

<td rowspan="4">3</td>

<td rowspan="4">.000***</td>

</tr>

<tr>

<td>31-40</td>

<td>17</td>

<td>29</td>

</tr>

<tr>

<td>41-50</td>

<td>21</td>

<td>36</td>

</tr>

<tr>

<td>>50</td>

<td>12</td>

<td>14</td>

</tr>

<tr>

<th colspan="6">Sex</th>

</tr>

<tr>

<td>Male</td>

<td>46</td>

<td>54</td>

<td rowspan="2">3.046</td>

<td rowspan="2">1</td>

<td rowspan="2">.081</td>

</tr>

<tr>

<td>Female</td>

<td>54</td>

<td>46</td>

</tr>

<tr>

<th colspan="6">Marital status</th>

</tr>

<tr>

<td>Married</td>

<td>54</td>

<td>92</td>

<td rowspan="2">57.560</td>

<td rowspan="2">1</td>

<td rowspan="2">.000***</td>

</tr>

<tr>

<td>Single</td>

<td>46</td>

<td>8</td>

</tr>

<tr>

<th colspan="6">Education level</th>

</tr>

<tr>

<td>Below university</td>

<td>65</td>

<td>24</td>

<td rowspan="2">71.811</td>

<td rowspan="2">1</td>

<td rowspan="2">.000***</td>

</tr>

<tr>

<td>

University<sup>**a**</sup></td>

<td>35</td>

<td>76</td>

</tr>

<tr>

<th colspan="6">Employment status</th>

</tr>

<tr>

<td>Employed</td>

<td>62</td>

<td>86</td>

<td rowspan="3">26.503</td>

<td rowspan="3">2</td>

<td rowspan="3">.000***</td>

</tr>

<tr>

<td>

Student<sup>**b**</sup></td>

<td>15</td>

<td>4</td>

</tr>

<tr>

<td>Unemployed</td>

<td>23</td>

<td>10</td>

</tr>

<tr>

<td colspan="6">

***p<.001  
<sup>**a**</sup> Includes both students attending university as well as those who had completed university.  
<sup>**b**</sup> Includes both full-time and part-time students with jobs.</td>

</tr>

</tbody>

</table>

The results of the χ<sup>2</sup> tests indicate that respondents tended to choose people who differed significantly from themselves in terms of age and marital status; information sources are older than respondents more frequently, and are more often married compared with respondents. There was no significant difference in the distribution of males and females between respondents and information sources. This may indicate homophilous interactions, with males preferring to approach males, and females preferring to approach females. An analysis of the dyadic interactions between respondents and information sources supports this assumption. Fifty-six percent of the information seeking interactions between respondents and information sources were between same sex pairs which were evenly divided between male-male pairs and female-female pairs. When respondents approached opposite sex information sources, males approached females 18% of the time, and females somewhat more frequently approached males, 26% of the time.

While respondents tended to approach information sources who were different from themselves in terms of age and marital status, they were also significantly more likely to approach people who had better quality resources than themselves in terms of education and employment status. There was an almost inverse relationship between the education level of respondents and information sources, with 65% of respondents having less than a university education and 77% of information sources having a university education. Information sources also tended to be employed more frequently than the respondents who included a greater proportion of students and the unemployed. The results of the χ<sup>2</sup> tests confirm the main proposition of the theory of social capital that when seeking new resources, in this case new information, people will engage in heterophilous interactions; that is, will approach people who are different from themselves and have better quality resources.

## Discussion

The findings of the study support the strength-of-weak-tie-proposition of the theory of social capital which predicted that when seeking new information, people will use their weak ties to access people with better resources than theirs. Out of all the people available in their social networks, respondents were more likely to choose those with whom they were weakly tied and who possessed better quality resources than others in their network in terms of age (middle-aged) and level of education (university-educated). Respondents were also significantly different from their information sources; information sources tended to be older, married, and to have a university education and, in addition, were more frequently employed than the respondents.

Weak ties are valuable resources when seeking new information because they provide access to information outside the network of close ties. Because people with close ties tend to interact frequently with each other and know the same people, information possessed by one person can quickly spread throughout the network. In order to find new information, one must often seek out people in other social circles. According to Lin, homophilous interaction is the normal type of interaction, and there is a '_normative match between effort and return_' ([Lin](#lin2001a), 2001a: 59). Lin goes on to suggest that since heterophilous interactions are between people who are dissimilar there may be a greater effort involved in initiating this kind of interaction as well as greater uncertainty about the outcome. However, because of the effort involved there may also be a greater expectation of the benefit that will result from the interaction (see [Lin](#lin2001a): 50-51). The choice of a person, therefore, is not necessarily the easiest option, or 'the path of least resistance' as suggested by Durrance ([1988](#durrance)) and others. It indicates, rather, that there may be an even greater psychological effort since the persons chosen as information sources tend not to be well known by and to be different from the respondents. A weak tie on its own, however, was not a sufficient qualification as an information source. Weak ties also tended to connect respondents to people with better resources in terms of life experience, knowledge, and status, indicating the purposive and deliberate nature of their information seeking. The theory of social capital explains why the use of people as sources of new information is not always a _least effort_ option since respondents appear to take great pains to scan their networks for the best sources they feel will help them, rather than those most readily at hand.

## Conclusion

This study demonstrates the importance of weak ties in the information seeking process, and suggests that there are factors other than least effort that explain the use of people as information sources. The findings of this study, however, may not be applicable to the general population and may be confined to the study participants and relevant to the particular information problems used in the analyses. The missing data from the thirty-two respondents who also chose people as information sources but whose tie information was not collected, may also have affected the results. The setting for the study, as well, may be a mitigating factor since the constraints on Mongolians' information behaviour may differ from those in the West and the information problems may have different cultural meanings. The findings do suggest, however, that the choice of a person as a source of information is complex and needs to be investigated at a more finely-grained level to determine what factors affect who is chosen and under what circumstances. The theory of social capital provides a useful framework within which to examine information behaviour and to understand how social structure affects choice.

## Notes

1. <a id="note1"></a>Respondents were chosen from five districts in the city of Ulaanbaatar including one of the ger districts (semi-formal suburbs on the city's fringe).

2. <a id="note2"></a>As part of my agreement to receive research assistance for this project from the Department of Library and Information Science at the Cultural College, I gave ten hours of lectures on information science subjects to senior students. At the close of these sessions, I and one of the teachers at the college selected twenty-six students to administer the questionnaires based on their aptitude for the task and their willingness and motivation to take part. These students attended a three-day workshop where they were instructed in the concepts of information seeking and social network analysis, the importance of abiding by the confidentiality requirements and of following the proper protocol for dealing with respondents.

3. <a id="note3"></a>An additional thirty-two respondents approached people for information, but the code numbers of the people approached were not included in the questionnaires by the research assistants and consequently their demographic and relational characteristics could not be identified. Therefore they could not be included in the analysis

4. <a id="note4"></a>Since the life expectancy in Mongolia is 63 years, describing people aged 31-50 as middle aged is appropriate (UNDP & Government of Mongolia, 2000).

## References

*   <a id="agada"></a>Agada, J. (1999). Inner-city gatekeepers: an exploratory survey of their information use environment. _Journal of the American Society for Information Science and Technology_, **50**(1), 74-85\.
*   <a id="bat"></a>Bat, C. (2002). [_Urban poverty profile, Ulaanbaatar City, Mongolia_](http://infocity.org/F2F/poverty/papers2/UB(Mongolia)%20Poverty.pdf). Paper presented at the Learning Workshop on Urban Poverty Reduction in East Asia, Singapore, June 10-11\. Retrieved 20 November, 2002 from http://infocity.org/F2F/poverty/papers2/UB(Mongolia)%20Poverty.pdf
*   <a id="case1991"></a>Case, D.O. (1991). The collection and use of information by some American historians: a study of motives and methods. _Library Quarterly_, **61**(1), 61-82.
*   <a id="case2002"></a>Case, D.O. (2002). _Looking for information: a survey of research on information seeking, needs, and behaviour_. Amsterdam: Academic Press.
*   <a id="chatman"></a>Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science and Technology_, **47**(3), 193-206.
*   <a id="chen"></a>Chen, C., & Hernon: (1982). _Information seeking: assessing and anticipating user needs_. New York, NY: Neal-Schumann Publishers, Inc.
*   <a id="childers"></a>Childers, T. (1975). _The information-poor in America._ Metuchen, NJ: Scarecrow Press, Inc.
*   <a id="choo"></a>Choo, C.W. (1993). _Environmental scanning: acquisition and use of information by chief executive officers in the Canadian telecommunications industry_. Unpublished Ph.D. Thesis, University of Toronto, Toronto.
*   <a id="delgadillo"></a>Delgadillo, R., & Lynch, B.P. (1999). Future historians: their quest for information. _College & Research Libraries_, **60**(3), 245-259.
*   <a id="dervin1983"></a>Dervin, B. (1983). _[An overview of sense-making: concepts, methods, and results to date](http://communication.sbs.ohio-state.edu/sense-making/art/artabsdervin83smoverview.html)_. Paper presented at the International Communication Association Annual Meeting, May, Dallas, TX. Retrieved 22 June, 2000 from http://communication.sbs.ohio-state.edu/sense-making/art/artabsdervin83smoverview.html
*   <a id="dervin1992"></a>Dervin, B. (1992). From the mind's eye of the user: the sense-making qualitative-quantitative methodology. In J.D. Glazier & R.R. Powell, (Eds.), _Quantitative research in information management_. Englewood, CO: Libraries Unlimited.
*   <a id="dervin1972"></a>Dervin, B., & Greenberg, B.S. (1972). The communication environment of the urban poor. In F.G. Klein & P.G. Tichenor (Eds.), _Current perspectives in mass communication research_ (Vol. 1). Beverly Hills, CA: Sage.
*   <a id="durrance"></a>Durrance, J.C. (1988). Information needs: old song, new tune. In _Rethinking the library in the information age: a summary of issues in library research_ (pp. 159-177). Washington, DC: Office of Library Programs, Office of Educational Research and Improvement, U.S. Department of Education.
*   <a id="ellis"></a>Ellis, D. (1993). Modeling the information-seeking patterns of academic researchers: a grounded theory approach. _Library Quarterly_, **63**(4), 469-486.
*   <a id="flanagan"></a>Flanagan, J.C. (1954). The critical incident technique. _Psychological Bulletin_, **51**(4), 327-358.
*   <a id="granovetter"></a>Granovetter, M.S. (1973). The strength of weak ties. _American Journal of Sociology_, **78**(6), 1360-1380.
*   <a id="johnson"></a>Johnson, C.A. (2003). _Information networks: investigating the information behaviour of Mongolia's urban residents_. Unpublished doctoral dissertation, University of Toronto.
*   <a id="laumann"></a>Laumann, E. O. (1966). _Prestige and association in an urban community_. Indianapolis, IN: Bobbs-Merrill.
*   <a id="leckie"></a>Leckie, G.J., Pettigrew, K.E., & Sylvain, C. (1996). Modeling the information seeking of professionals: a general model derived from research on engineers, health care professionals, and lawyers. _Library Quarterly_, **66**(2), 161-193.
*   <a id="lin2001a"></a>Lin, N. (2001a). _Social capital: a theory of social structure and action._ Cambridge: Cambridge University Press.
*   <a id="lin2001b"></a>Lin, N. (2001b). Building a network theory of social capital. In N. Lin, K. Cook & R. S. Burt (Eds.), _Social capital: theory and research_ (pp. 3-29). New York, NY: Aldine de Gruyter.
*   <a id="lin1986"></a>Lin, N., & Dumin, M. (1986). Access to occupations through social ties. _Social Networks_, **8**(4), 365-385.
*   <a id="lin2001"></a>Lin, N., Fu, Y.-c., & Hsung, R.-M. (2001). The position generator: measurement techniques for investigations of social capital. In N. Lin, K. Cook & R. S. Burt (Eds.), _Social capital: theory and research_ (pp. 57-81). New York, NY: Aldine de Gruyter.
*   <a id="mcpherson"></a>McPherson, J.M., Smith-Lovin, L., & Cook, J.M. (2001). Birds of a feather: homophily in social networks. _Annual Review of Sociology,_ **27**, 415-444.
*   <a id="sligo"></a>Sligo, F. X., & Jameson, A. M. (2000). The knowledge-behaviour gap in use of health information. _Journal of the American Society for Information Science and Technology_, **51**(9), 858-869.
*   <a id="spink"></a>Spink, A., & Cole, C. (2001). Information and poverty: information-seeking channels used by African American low-income households. _Library & Information Science Research,_ **23**(1), 45-65.
*   <a id="un"></a>United Nations Development Programme and Government of Mongolia (2000). _Human development report—Mongolia—2000: reorienting the state_. Ulaanbaatar, Mongolia: United Nations Development Programme.
*   <a id="warner"></a>Warner, E.S., Murray, A.D., & Palmour, V.E. (1973). _Information needs of urban residents_ . Baltimore, MD: Regional Planning Council. (Final Report Contract No. OEC-0-71-4555)
*   <a id="worldbank"></a>World Bank. (2002). _[Mongolia—country brief](http://www.worldbank.org/Countries/Mongolia)_. Retrieved 4 March, 2003 from http://www.worldbank.org/Countries/Mongolia.