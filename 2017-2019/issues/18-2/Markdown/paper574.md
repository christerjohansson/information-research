<header>

#### vol. 18 no. 2, June, 2013

</header>

<article>

# Functional relevance and inductive development of an e-retailing product information typology

#### [Xiaoli Huang](#author)  
Business School, Sun Yat-sen University, 
No. 135 Xingang West Road, Guangzhou 510275, China

#### [Dagobert Soergel](#author)  
Department of Library and Information Studies, 
Graduate School of Education, University at Buffalo, 
534 Baldy Hall, Buffalo, NY 14260-1020, USA

<section>

#### Abstract

> **Introduction.** Hardly any in-depth knowledge is currently available on how different types of product-relevant information influence online consumer trust and purchase decisions. To address this research gap, we apply a generic function-based information typology to systematically classify the large variety of online product information and plan for a focused comparison of their functional roles and differential effects on online consumer decision making.  
> **Method.** This paper reviews information research in e-commerce, discusses the conceptual basis of applying the generic function-based topical relevance typology to analyse online product information, and uses a variety of product examples from Amazon.com to demonstrate the process. As an exploratory study, sixteen product cases were collected from Amazon, including five search products, six experience products (three electronics and three traditional goods), and five credence products.  
> **Analysis.** We used qualitative content analysis and pattern matching to conduct in-depth analyses of the information types present from the samples. To improve methodological precision and rigor we used template analysis and editing analysis to guide the qualitative coding process.  
> **Results.** Through this study, direct (matching topic), context (including condition), evaluation, and comparison are identified as the four most applicable and prevalent information types from Amazon product pages. The focus or functional role of each information type is summarised and discussed.  
> **Conclusions.** The preliminary findings from this exploratory study provide a theoretical basis for guiding and prioritising online information organization and provision, which becomes increasingly important in the current context of information overload.

## Introduction

This study develops a typology of the types of product information given on an e-commerce website. Such a typology is a prerequisite for investigating and comparing the effects of various types of product information on online consumer decision-making.

Online product information provided by dot-com storefronts plays a central role in informing the consumer of the product value and strengthening the consumer's confidence in making the right purchase ([Jeong and Lambert 2001](#jeo01)). Since they cannot physically inspect a product as they could in brick-and-mortar stores, online shoppers base their decisions mostly on information they receive on the Internet. In the virtualized shopping environment, they feel more risk ([Antony _et al._ 2006](#ant06)) and become highly responsive to the online product information they are getting.

Effective provisioning and organization of online product information becomes increasingly critical for the profitability and survival of Internet retailers and manufacturers ([Lee and Lee 2004](#lee04)). As dot-com storefronts provide an increasing amount of product information to attract Internet shoppers, _information overload_ has become a pressing issue for online shopping. Information overload occurs as the amount of provided information exceeds individuals' information processing capability ([Jacoby 1984](#jac84)). Just as information inadequacy, too much information also leads to declines in the quality of decision-making performance ([Allen and Shoard 2005](#all05)). The phenomenon of information overload has caught a great deal of attention from marketers and marketing researchers since the 1970s. A series of works thoroughly demonstrated that information overload not only decreases the accuracy of consumer decisions, but also leads to less satisfaction and reduced confidence in the decision-making process ([Chen _et al._ 2009](#che09); [Jacoby _et al._ 1974](#jac74); [Allen and Shoard 2005](#all05); [Yang _et al._ 1998](#yan98); [Keller and Staelin 1987](#kel87); [Lee and Lee 2004](#lee04); [Lurie 2004](#lur04); [Maes _et al._ 1999](#mae99); [Malhotra 1982](#mal82), [1984](#mal84)). With the increasing popularization of small-screen mobile e-commerce, information overload poses an even more serious challenge for practitioners and researchers.

E-commerce information research to date has focused primarily on _information quality_ and _information display_, but lacks studies on content-based _information types_. The types of product-relevant information and their mechanisms for affecting consumer decisions are not well understood. The existing information-type research seems limited to only one information type, _customer review_. Regarding many other types of product information such as product comparisons, return policy, manufacturing details, retailer reputation, use scope, side effects and so on, research is largely absent or only scratching the surface. There is not yet research that systematically investigates and compares the functional roles played by different types of product information and hardly any in-depth knowledge is currently available on their impact on _consumer trust_, _perceived value_, _perceived risk_, and _purchase intention_ in the e-retailing context.

Our project aims to bridge this gap by systematically analysing online product information types and their differential effects on Internet consumer decision making. To carry out an in-depth investigation and comparison, first we need a scheme to systematically categorize different information _types_ from amass of online product information, preferably by different _functional roles_ they play in consumer perception and decision making. This paper reports on a pilot study of applying a typology of function-based topical relevance relationships ([Huang 2009a](#hua09), [2009b](#hua09-2), [2013](#hua13); [Huang and Soergel 2004](#hua04), to analyse online product information types.

## Literature review

### A generic typology of topical relevance relationships

_Topical relevance_ is the fundamental concept of information organization and information retrieval ([Cooper 1971](#coo71); [Green 1995](#gre95); [Green and Bean 1995](#gre95-2); [Hjørland 2010](#hjo10); [Huang and Soergel 2004](#hua04); [Wilson 1973](#wil73)). Topical relevance is the logical connection between _topic_ and _information_, between _topic_ and _topic_, and between _information_ and _information_. It provides the cognitive foundation for human thinking, reasoning, communication, and decision making. Without recognizing and understanding the relevance connection between an input (or a stimulus) and its context, we could hardly make it through simple daily conversations in which inferential communicative intentions are often embedded, nor could we come to any meaningful judgment or conclusion about a given situation ([Wilson and Sperber 2002](#wil02)). Therefore, topical relevance with its close linkage to thinking and reasoning is central to many disciplines.

Huang ([2009a](#hua09), [2009b](#hua09-2), [2013](#hua13)) conducted a multidisciplinary inquiry into topicality by analysing theories and thinking from communication, rhetoric, cognitive psychology, education, information science, argumentation, logic, law, medicine, and history. As the major outcome, the inquiry constructed a theory-grounded and empirically-verified typology of topical relevance relationships, providing a conceptual basis for a generic, function-based, topic-oriented information framework (Figure1) that is meaningful across topics, domains, and data forms. In particular, the framework is based on _functional role_, that is, the role a piece of information plays in the overall structure of a topic, by taking into account its relations with other parts of the given information. It adopts the perspective of _rhetorical structure theory_ in discourse analysis ([Mann and Thompson 1988](#man88), [Taboada and Mann 2006](#tab06)): '_for every part of a coherent text, there is some function for its presence, evident to readers_' ([Taboada and Mann 2006](#tab06): 425). The emphasis of the functional classification is on the _cognitive effect_ achieved on the receiver, such as a reader, a hearer, an information user, a consumer, etc. The _cognitive effect_ refers to a substantial change of the receiver's knowledge state or viewpoint after receiving a piece of relevant information, which can confirm, reinforce, revise, or disprove the receiver's original beliefs. As applying to e-commerce, each kind of product information relates to the product in a different topical way and plays a specific role in enhancing the consumer's perception of a product's value as well as associated risks, and strengthening or adjusting his/her buying intention. For example, _product feature description_ and _return policy_ relate to a product in two very different ways and each plays a different functional role in influencing the consumer's purchase decision: feature description directly improves the consumer's understanding of the product's value while return policy contributes to reducing his/her anxiety of making the buying decision. Essentially, the function-based relevance typology provides a relationship framework for specifying the functional role of each type of product-relevant information and differentiating their cognitive effects on the consumer. The functional classification directly ties with the consumer's knowledge state and cognitive process underlying online decision making.

The function-based classification has been successfully applied to analyse three heterogeneous datasets in oral history, clinical question answering, and art image tagging ([Huang 2009a](#hua09), [2013](#hua13); [Huang and Soergel 2004](#hua04), [2006](#hua06) ). Moreover, Huang and White ([2005](#hua05)) developed _policy-capturing models_ (regression-based) to explore individual effects of different relevant information on the user's relevance judgment; in these models, highly consistent results were found regarding the relative importance of each information type for the user's overall judgment. Similarly, the current study extends the research to the e-business context by investigating individual effects of various types of product-relevant information on the overall purchase decision. The findings from this study also help to further enrich the generic topical relevance typology.

<figure>

![Function-Based Relevant Information Typology](../p574fig1.gif)

<figcaption>Figure 1: Function-based relevant information typology</figcaption>

</figure>

### Product classification, information type, and online shopping preferences

From the perspective of advertising, a number of studies have proved strong associations between the information type and the consumer's shopping preferences for product classes ([Girard _et al._ 2002](#gir02), [Korgaonkar _et al._ 2006](#kor06), [Nelson 1970](#nel70), [1974](#nel74)). However, this line of research never systematically defined various information types, neither did they systematically investigate and compare their individual effects on consumer decision making.

Nelson's _Advertising as information_ ([1970](#nel70)) introduced a product classification which soon gained wide acceptance and has since provided a fruitful avenue for understanding merchants' information provision (advertising) in relation to consumers' buying behaviour. The product classification originally began with two major categories, _search product_ and _experience product_. It was later refined and extended ([Darby and Karni 1973](#dar73), [Klein 1998](#kle98), [Nelson 1974](#nel74)) and eventually adapted to analyse Internet retailing ([Girard _et al._ 2002](#gir02), [Korgaonkar _et al._ 2010](#kor10), [Korgaonkar _et al._ 2006](#kor06), [Lal and Sarvary 1999](#lal99)). The product classification as adapted to e-retailing is summarised as follows (recognizing that in the context of e-retailing these distinctions are fuzzy, [Korgaonkar _et al._ 2006](#kor06)):

*   **Search products**: the consumer can determine the quality of the product _before_ the purchase by searching information about the product. Examples are books and laptops.
*   **Experience products**: the consumer cannot (or only with great difficulty and expense) determine the quality of the product before the purchase; instead s/he needs to personally _experience_ the product to determine the quality. Examples are clothing and perfumes.
*   **Credence products**: the consumer has no way of fully evaluating and determining the quality of the product neither _before_ nor _after_ the purchase; instead s/he has to base the purchase on credence or trust of the brand. '_This occurs because the consumer may not possess sufficient technical expertise to assess the product's true performance_' ([Ford _et al._ 1988](#for88): 240) and the effects of these products are often not directly perceivable. Examples are water purifier, vitamins, and anti-aging cream.

Research found significant differences between search, experience, and credence products with respect to the types of information consumers prefer in advertising. _Direct information_ is preferred for shopping search products and _indirect information_ (such as brand reputability and endorsements) is preferred for shopping experience products ([Nelson 1974](#nel74)). For e-retailing, products with “digital attributes (which can be communicated on the web at very low cost)” are more likely to succeed than products associated with '_nondigital attributes (for which physical inspection of the product is necessary)_' ([Lal and Sarvary 1999](#lal99): 485). Digital attributes are mostly corresponding to search products whereas nondigital attributes are often associated with experience and credence products ([Korgaonkar _et al._ 2006](#kor06)). From a series of empirical analyses, consumers demonstrate stronger preference for shopping search products rather than experience and credence products on the web ( [Girard _et al._ 2002](#gir02), [Korgaonkar _et al._ 2010](#kor10), [Korgaonkar _et al._ 2006](#kor06)). This preference can be attributed to online information provisioning: the provided standard direct product information is usually sufficient for consumers to make informed decisions about purchasing search products, yet it is inadequate for consumers to judge experience and credence products. Instead, when evaluating experience and credence products, consumers tend to draw on indirect, contextual, and evaluative information, such as the retailer's reputability information, third-party evaluation, product comparison guides, and customer reviews.

Given its strong influence on consumers' information preferences, our study considers _product type_ as a significant factor to moderate the decision effects of information type. It guides us to collect online product cases for the following analysis and provides a conceptual basis for identifying useful strategies for organizing online product-related information.

## Research questions

This pilot study intends to answer the following two research questions:

1.  What are the manifestations of the function-based topical relevance typology in the e-retailing context? Which relevance categories are applicable and which are not?
2.  What information types are present in online product pages? And what information types are absent? Do these vary with the type of product?

## Method

This is an exploratory study with sixteen product cases were collected from Amazon, including five search products, six experience products (three electronics and three traditional goods), and five credence products.. We used qualitative content analysis and pattern matching to conduct in-depth analyses of the information types present from the samples. Many studies using qualitative content analysis to look for themes ([Trochim and Kane 2005](#tro05)), patterns ([Thompson 1996](#tho96)), or the largely _rhetorical structure_ of texts ([Kiser 1997](#kis97)). This study uses qualitative content analysis to specify the function-based topical connections between the product of interest and the product information provided.

To improve methodological precision and rigour we used _template analysis_ and _editing analysis_ ([Marsh 2002](#mar02); [Miller and Crabtree 1992](#mil92)) to guide the analysis.

1.  _Template analysis_ is a semi-structured coding process that uses _templates_ and _code books_ developed before data analysis. The template can be thought of as a coding scheme or an analysis guide; it can be developed based on a variety of preconceptions, such as '_theory, research tradition, pre-existing knowledge, and/or a summary reading of text_' ([Miller and Crabtree 1992](#mil92): 19). We used the initial generic typology as a guiding template to analyse how the broader topical relevance categories apply to analysing Amazon product pages. Interaction of the data and the template (scheme) introduced continuous revisions to the template and involved several iterations until no new revisions were identified (reaching the point of saturation, [Lindlof and Taylor 2002](#lin02)).
2.  _Editing analysis_ mirrors the _open coding_ phase in inductive analysis ([Lindlof and Taylor 2002](#lin02), [Marsh 2002](#mar02)), whereby the inquirer approaches the data with minimal preconceptions and proceeds to develop conceptual categories through an inductive process ([Strauss and Corbin 1990](#str90)). We used it for analysing finer levels of topical relationships to further extend the hierarchy and thus developed the fine-grained structure under each broad relevance category inductively from the data.

## Results and discussions

The findings confirm that the overall topical relevance framework with the broad relevance categories remain meaningful and stable as adapted to the e-commerce domain. _Direct_, _context/condition_, _comparison_, and _evaluation_ are the prevalent information types identified from the Amazon case analysis. Table 1 summarizes all the relevant information types and subtypes identified from the analysis. Although the broad relevance categories remain similar, many domain-specific subcategories are suggested from the study. Taking the _context_ category as an example, _purchase (selection)-related_ and _use-related_ context are completely new subcategories without direct mapping to the original typology. They are specific to analysing e-commerce cases and quite important for online consumer decision making.

<table><caption>

Table 1: A Summary of relevant information types analysed from Amazon cases.  
<small>(* As mapped to the generic function-based relevant information typology in Figure 1.)</small></caption>

<tbody>

<tr>

<th>Relevance category</th>

<th>Sub-categories analysed from Amazon cases</th>

<th>Relevance category</th>

<th>Sub-categories analysed from Amazon cases</th>

</tr>

<tr>

<td>

**Direct**</td>

<td>

Title or product name  
Price  
Colour  
Dimension or size  
Shipping weight  
Length  
Format or version  
ID or item model  
Language  
Parts or components  
Ingredients  
Features, capabilities or functions  
Technical specifications (standards)  
Image or view  
Manifestations*, sample, preview or excerpt  
Illustration  
</td>

<td rowspan="2">

**Context**</td>

<td rowspan="2">

Provenance-related context:  
Provenance or biographic information*  
About provenance info elements:  
Info. about the author  
Info. about the manufacturer  
Purchase (selection)-related context:  
Stock  
Promotion  
Selection guide  
Shipping  
Warranty & return policy  
Use-related context:  
User instruction  
Use scenario  
Life span  
Safety information  
Maintenance  
Relations with other products:  
Broader scope*  
Used by  
Recommendation  
Brought together  
</td>

</tr>

<tr>

<td>

**Comparison**</td>

<td>

By difference*:  
Price comparison  
Format comparison  
By similarity*:  
Similar product  
By category  
By subject (topic)  
By the same author  
</td>

</tr>

<tr>

<td>

**Evaluation**</td>

<td>

Comparative evaluation*:  
Ranking  
Formal evaluation  
By evaluator:  
Customer review  
Expert review  
</td>

<td>

**Condition**</td>

<td>

Helping factor (condition)*  
Compatibility  
with software  
with hardware  
Requirement  
</td>

</tr>

</tbody>

</table>

In the following, each of the five product-relevant information types is discussed in more detail and illustrated with exemplar cases.

### Direct information (matching topic)

Matching-topic or direct information in the e-retailing context refers to information that directly describes the characteristics, specifications, functions, and features of the product. It also includes manifestations, samples, and illustrations of the product as shown in the following table. As implemented on [Amazon.com](http://www.amazon.com), the consumer could listen to a part of an mp3 song online, _look inside_ a book by a click, and read its excerpts and key phrase extractions. Illustration is considered direct information as well, such as sample pictures taken by a digital camera the consumer is considering, or how a sofa set of interest fits in a home setting.

Direct information answers the questions of _what the product is_ and _what it can do_. It provides the most relevant and immediate information for the consumer to make judgments of the value and suitability of the product. As expected, direct product information is prevalent and placed at the centre of focus for all the collected cases. Table 2 summarizes specific direct information types with associated case samples.

<table><caption>Table 2: Examples of direct information collected from the case analysis</caption>

<tbody>

<tr>

<th>Direct info type</th>

<th colspan="2">llustrating examples from the case analysis</th>

</tr>

<tr>

<td rowspan="2">

**Format or version**</td>

<td>

_Ex.1\. Music:_</td>

<td>MP3 download, CD format</td>

</tr>

<tr>

<td>

_Ex.2\. Book:_</td>

<td>Paperback, paperback (large print), perfect paperback, hardcover, hardcover (large print), hardcover (2009), audiobook (abridged), audiobook (unabridged), etc</td>

</tr>

<tr>

<td rowspan="2">

**Parts or omponents**</td>

<td>

_Ex.1. Sofa set:_</td>

<td>The Hungtinton sectional sofa includes a three-seat sofa, a left/right-reversible chaise, and an oversized ottoman. Crafted with sturdy hardwood legs, the sectional features dark brown faux leather covering over its hardwood frame.</td>

</tr>

<tr>

<td>

_Ex.2. Water dispenser:_</td>

<td>

![Water dispenser](../p574fig2.gif)</td>

</tr>

<tr>

<td>

**Features capabilities/functions**</td>

<td>

_Ex.1. Wireless keyboard:_</td>

<td>

![Wireless keyboard](../p574fig3.gif)</td>

</tr>

<tr>

<td>

**Image or view**</td>

<td>

_Ex.1\. Tablet:_</td>

<td>

![Tablet computer](../p574fig4.gif)</td>

</tr>

<tr>

<td rowspan="2">

**Manifestation/sample/preview/excerpt**</td>

<td>

_Ex.1\. Book:_</td>

<td>

![Book example](../p574fig5.gif) ![Book example](../p574fig6.gif)</td>

</tr>

<tr>

<td>

_Ex.2\. Music:_</td>

<td>

![Music example](../p574fig7.gif)</td>

</tr>

<tr>

<td rowspan="2">

**Illustrations**</td>

<td>

_Ex.1. Digital camera:_</td>

<td>

Sample picture taken 
![Digital camera](../p574fig8.gif)</td>

</tr>

<tr>

<td>

_Ex.2\. Sofa set:_</td>

<td>

Bobkona sofa set fitting in a home  
![Sofa](../p574fig9.gif)</td>

</tr>

</tbody>

</table>

### Context information

Context information does not directly address the specifications, features, functions, and performance of the product; instead context supplies information about _its making_, _its purchase_, _its use_, and _its relations with other products_. It may not lead to direct judgments about the product's value; however, context information helps to address consumers' additional concerns, set their thinking within a broader and richer background, and thus better inform their decisions. Compared to direct information, context is secondary information and its presence and richness are more subject to individual products. For example, the collected traditional experience goods (shoes, jeans, and sofa set) in this study seem to have little context information.

As compared to other information types, context is most interesting and much diversified; it is very inclusive and contains a large variety of manifestations. In the following, different types of context information are exemplified.

1.  _Provenance-related context_: answers the questions of _who made the product_, _where and how it was made_, as well as _when it was made available_. These types of information can be used to imply the quality of a product and to strengthen the consumer's confidence in the product. (See examples in Table 3).  

    <table><caption>Table 3: Examples of provenance-related context information.</caption>

    <tbody>

    <tr>

    <th>Context type</th>

    <th colspan="2">llustrating examples from the case analysis</th>

    </tr>

    <tr>

    <td>
    
    **Provenance info**</td>

    <td colspan="2">Author, artist, publisher, translator, actor, manufacturer, brand, copyright, etc.</td>

    </tr>

    <tr>

    <td>
    
    **Informaton about author**</td>

    <td>
    
    _Ex.1\. Book:_</td>

    <td>
    
    **Dan Brown** is the #1 _New York Times_ bestselling author of _The Da Vinci Code_ and, previously, _Digital Fortress_, _Deception Point_, and _Angels & Demons_. He is a graduate of Amherst College and Phillips Exeter Academy, where he spent time as an English teacher before turning his efforts to writing full-time. He lives in New England with his wife. Visit the author's website at www.danbrown.com.</td>

    </tr>

    <tr>

    <td>
    
    **Info about manufacturer**</td>

    <td>
    
    _Ex.2. Home theatre:_</td>

    <td>
    
    **Klipsch – Power. Detail. Emotion.**  
    Since 1946, proprietary horn-loaded technology has been the driving force behind the Klipsch's highly acclaimed, stunningly precise sound. Superior quality, horn-loaded speakers are an absolute must for delivering the powerful, detailed soundtracks of today's demanding blockbusters. Perhaps that's why 50 percent of all new theatres in North and South America, as well as Korea and Australasia install Klipsch products.</td>

    </tr>

    </tbody>

    </table>

2.  _Purchase (selection)-related context_: such as quantity in stock, shipping, promotion, warranty and return policy, and product selection guide. This type of context information helps to develop interest and confidence in the process of purchase. (See examples in Table 4).  

    <table><caption>Table 4: Examples of purchase-related context information.</caption>

    <tbody>

    <tr>

    <th>Context type</th>

    <th colspan="2">Illustrating examples from the case analysis</th>

    </tr>

    <tr>

    <td rowspan="2">
    
    **Promotion**</td>

    <td>
    
    _Ex.1. Anti-aging cream:_</td>

    <td>
    
    ![Anti-aging cream](../p574fig10.gif)</td>

    </tr>

    <tr>

    <td>
    
    _Ex.2\. Book:_</td>

    <td>
    
    ![Book](../p574fig11.gif)</td>

    </tr>

    <tr>

    <td>
    
    **Selection guide**</td>

    <td>
    
    _Ex.1. Android phone:_</td>

    <td>
    
    ![Android phone](../p574fig12.gif)</td>

    </tr>

    </tbody>

    </table>

3.  _Use-related context_: provides information related to product use; instructs customers how to use the product safely and care for it. The use-related context allows potential buyers to place themselves in future typical use scenarios, think through some of the potential problems, and use the information to evaluate the product against their own using habits or constraints. For example, for clothing the maintenance work of '_Machine wash_' vs. '_Dry clean_' may make a difference in the consumer's choice. (See examples in Table 5).  

    <table><caption>Table 5: Examples of use-related context information.</caption>

    <tbody>

    <tr>

    <th>Context type</th>

    <th colspan="2">Illustrating examples from the case analysis</th>

    </tr>

    <tr>

    <td rowspan="2">
    
    **User instruction**</td>

    <td>
    
    _Ex.1. Anti-aging cream:_</td>

    <td>Apply nightly to cleaned face and neck. Smooth a pearl-sized amount evenly in gentle massaging strokes until fully absorbed.  
    </td>

    </tr>

    <tr>

    <td>
    
    _Ex.2. Water dispenser:_</td>

    <td>
    
    ![Water dispenser](../p574fig13.gif)</td>

    </tr>

    <tr>

    <td>
    
    **Life span**</td>

    <td>
    
    _Ex.1. Water dispenser:_</td>

    <td>Each filter has a lifespan of about one to two months (or roughly 40 gallons of water). A handy gauge indicates when the filter needs to be replaced.</td>

    </tr>

    <tr>

    <td>
    
    **Safety information**</td>

    <td>
    
    _Ex.1. Multivitamin:_</td>

    <td>As with any supplement, if you are pregnant, nursing, or taking medication, consult your doctor before use. Accidental overdoes of iron-containing products is a leading cause of fatal poisoning in children under 6\. Keep this product out of reach of children.</td>

    </tr>

    <tr>

    <td rowspan="2">
    
    **Maintenance**</td>

    <td>
    
    _Ex.1\. Jeans:_</td>

    <td>Machine wash.</td>

    </tr>

    <tr>

    <td>
    
    _Ex.2\. Sofa set:_</td>

    <td>To care for the sectional set, spot clean with a gentle soap and water.</td>

    </tr>

    </tbody>

    </table>

4.  _Relations with other products_: Situates the current product within a network of products (such as broader category) and makes recommendations. The primary purpose here is to increase the consumer's awareness of a broader product network, so as to induce more interests and expand sales. The category of “Bought together” is a typical Amazon feature based on consumer data mining, which in turn encompasses many kinds: “Frequently bought together”, “Customers who bought this item also bought”, “What other items do customer buy after viewing this item?”, “Consumers also bought this items by”, and so on. Apparently there are overlapping results among different “Bought together” algorithms. As observed, these algorithms not only bring up similar products (e.g.,![frequently bought together](../p574fig14.gif) ), but also brings up products likely used together (e.g.,![frequently bought together](../p574fig15.gif) ). Rather than being measured directly as in _similarity-based comparison_ discussed later, the achieved similarities here is simply the by-product (instead of the aim) of customer data analytics. This puts them into the user-based context category rather than the similarity-based comparison category. (See examples in Table 6).

<table><caption>Table 6: Examples of other products related context information</caption>

<tbody>

<tr>

<th colspan="2">Context type</th>

<th colspan="2">Illustrating examples from the case analysis</th>

</tr>

<tr>

<td colspan="2" rowspan="2">

**Broader scope**</td>

<td>

_Ex.1\. Music:_</td>

<td>

![Music](../p574fig16.gif)</td>

</tr>

<tr>

<td>

_Ex.2\. Sofa set:_</td>

<td>

![Sofa](../p574fig17.gif)</td>

</tr>

<tr>

<td rowspan="2">

**Used by**</td>

<td>

**1) Citation**</td>

<td>

_Ex.1\. Book-Angels & Demons:_</td>

<td>

![Book](../p574fig18.gif)</td>

</tr>

<tr>

<td>

**2) Movies**</td>

<td>

_Ex.1\. Book-Angels & Demons:_</td>

<td>

Adapted as motion picture:  
![Movie](../p574fig19.gif)</td>

</tr>

<tr>

<td colspan="2" rowspan="2">

**Recommendation**</td>

<td>

_Ex.1. Android phone:_</td>

<td>

![Android phone](../p574fig20.gif)</td>

</tr>

<tr>

<td>

_Ex.2. Home theatre:_</td>

<td>Amplifier and receiver recommendations</td>

</tr>

<tr>

<td colspan="2" rowspan="5">

**Bought together**</td>

<td>

_Ex.1\. Book:_</td>

<td>

![Book](../p574fig21.gif)</td>

</tr>

<tr>

<td>

_Ex.2. Digital camera:_</td>

<td>

![Digital camera](../p574fig22.gif)</td>

</tr>

<tr>

<td>

_Ex.3. Digital camera:_</td>

<td>

![Digital camera](../p574fig23.gif)</td>

</tr>

<tr>

<td>

_Ex.4\. Book:_</td>

<td>

![Book](../p574fig24.gif)</td>

</tr>

<tr>

<td>

_Ex.5\. Book:_</td>

<td>

![Book](../p574fig25.gif)</td>

</tr>

</tbody>

</table>

### Condition information

Following the generic typology, the information about compatibility and system requirements falls into the category of _Condition–Helping or hindering factor (condition)_. Similar to context information discussed above, it also assists the customer in making better informed decisions. In the original topical relevance typology, the category of _Condition_ was included in _Context_ but was later separated out to emphasize its higher degree of specificity (such as in clinical analysis). However, in the current e-retailing context, it is arguable whether there is enough justification to differentiate _Condition_ from _Context_. The collected cases in this pilot study supply very limited instances and more data are required for us to draw the final conclusion. Speaking solely for the following two condition information types, they may well fit into the category of use-related context. (See examples in Table 7).

<table><caption>Table 7: Examples of condition information</caption>

<tbody>

<tr>

<th>Condition type</th>

<th colspan="2">Illustrating examples from the case analysis</th>

</tr>

<tr>

<td>

**Compatible with**</td>

<td>

_Ex.1\. Music:_</td>

<td>Compatible with MP3 Players (including with iPod), iTunes, Windows Media Player.</td>

</tr>

<tr>

<td>

**Requirement**</td>

<td>

_Ex.1. Android phone:_</td>

<td>

**System requirements:**

*   Windows 7/Windows Vista/Windows XP
*   Or Mac OS X v10.4-10.6x
*   USB 2.0
*   2 AAA batteries (included)

</td>

</tr>

</tbody>

</table>

### Comparison information

This category emphasizes the element of comparison and provides similarity-based options. Just as _Relations with other products_ (context), its purpose is also to deliberately broaden consumers' horizon of products and enhance their awareness and interests of related products. However, it is important to note that these similarities are measured based on the _intrinsic_ properties of the products, rather than the external, contextual associations created by consumer browsing and buying activities, and therefore different from the “Brought together” category. Amazon.com uses various types of algorithms to detect and calculate similarities between products, such as “by category”, “by subject (topic)”, “by the same author”, and so on. (See examples in Table 8).

<table><caption>Table 8: Examples of comparison information</caption>

<tbody>

<tr>

<th colspan="2">Comparison type</th>

<th colspan="2">Illustrating examples from the case analysis</th>

</tr>

<tr>

<td rowspan="2" colspan="2">

**Price comparison**</td>

<td>

_Ex.1\. Book:_</td>

<td>

![Book](../p574fig26.gif)</td>

</tr>

<tr>

<td>

_Ex.1. Digital camera:_</td>

<td>

![Digital camera](../p574fig27.gif)</td>

</tr>

<tr>

<td colspan="2">

**Format comparison**</td>

<td>

_Ex.1\. Book:_</td>

<td>

![Book](../p574fig28.gif)</td>

</tr>

<tr>

<td rowspan="3">

**Similar product**</td>

<td>

**1) By category**</td>

<td>

_Ex.1\. Sofa set:_</td>

<td>

![Sofa set](../p574fig29.gif)</td>

</tr>

<tr>

<td>

**2) By subject**</td>

<td>

_Ex.1\. Book:_</td>

<td>

![Book](../p574fig30.gif)</td>

</tr>

<tr>

<td>

**3) By author**</td>

<td>

_Ex.1\. Book:_</td>

<td>

![Book](../p574fig31.gif)</td>

</tr>

</tbody>

</table>

### Evaluation information

A great number of studies have confirmed that consumers are highly dependent on user evaluations (reviews) on the web, even though they might not always follow these reviewers' opinions and suggestions ([Sen and Lerman 2007](#sen07), [Senecal and Nantel 2004](#sen04)). Moreover, positive reviews seem to have a stronger influence on the consumer than negative reviews do ([Vermeulen and Seegers 2009](#ver09)). Coming from a source of neutrality and user perspective, customer evaluation is regarded more reliable, unbiased, and informative compared to the information supplied directly by the seller. The reviews on Amazon.com are highly detailed and organized, covering many aspects of the product. It will be interesting to further investigate whether different types of evaluation information affect consumer decision making differently, for instance, does the source of evaluation make a difference, i.e., customer reviews vs. expert reviews? How much attention would consumers place to formal evaluation (such as double-blind clinical study) as compared to informal evaluation? What about ranking information? (See examples in Table 9).

Note: For simplicity, we have placed customer reviews and expert reviews under _Evaluation_ because they are most important for the evaluative information they contain. However, reviews may also include many types of information other than evaluation, such as tips on how to use a product.

<table><caption>Table 9: Examples of evaluation information</caption>

<tbody>

<tr>

<th>Evaluation type</th>

<th colspan="2">Illustrating examples from the case analysis</th>

</tr>

<tr>

<td>

**Ranking**</td>

<td>

_Ex.1. Andriod phone:_</td>

<td>

![Android phone](../p574fig32.gif)</td>

</tr>

<tr>

<td>

**Like**</td>

<td>

_Ex.1._</td>

<td>

![Liking](../p574fig33.gif)</td>

</tr>

<tr>

<td>

**Customer review**</td>

<td>

_Ex.1. Water dispenser:_</td>

<td>

**Average Customer Review**  
![Stars](../p574fig34.gif)</td>

</tr>

<tr>

<td rowspan="2">

**Expert review**</td>

<td colspan="2">Editorial reviews, e.g., 'From Publishers Weekly', Critic's evaluation</td>

</tr>

<tr>

<td>

_Ex.1. Home theater:_</td>

<td>

![Review text](../p574fig35.gif)</td>

</tr>

<tr>

<td>

**Formal evaluation**</td>

<td>

_Ex.1. Anti-aging cream:_</td>

<td>

**Double-blind, 8-week clinical study of 40 women 40 to 65 years of age**  
![Clinical graph](../p574fig36.gif)</td>

</tr>

</tbody>

</table>

## Conclusions

Making online purchase decisions is a process combining quality/price evaluation and relevance judgment. Consumers need to find out not only the quality but also the relevance of the product to their personalized needs and requirements. Applying the function-based topical relevance typology to analyse online product-relevant information provides a basis for us to further discuss how different types of information contribute to this evaluation and judgment process.

Through this study, _direct (matching topic)_, _context_ (including _condition_), _evaluation_, and _comparison_ are identified as the four most applicable and prevalent information types from Amazon product pages. The more argument-linked and reasoning-centred information types such as _evidence_ and _cause/effect_, and _purpose/motivation_ are not noticeably significant from the analysis. However, this may be due to the small sample gathered from a single source ([Amazon.com](http://www.amazon.com)). As guided by this pilot study, more cases will be collected from different sites in US as well as in China to allow more systematic comparison between product types in relation to cultural contexts.

The focus or role of each information type is summarised in the following:

*   **Direct (matching topic)**: answer the questions of what the product is and what it can do;
*   **Context**: supply additional information related to its making, it purchase and selection, its use, and its position in the product network;
*   **Comparison**: identify similarity-based products and options; here similarity is broadly defined and has many meanings, such as, common feature, shared use context, same brand/manufacturer, etc.
*   **Evaluation**: answer the question of how well it performs.

The in-depth analysis also extends and enriches the original function-based typology by introducing new sub-categories. Another finding is that a lot of site features and provided information serve the same purpose of introducing other related products. In many cases, these features and information occupy over 50% of the entire product page. The intention is clear but what are the effects on consumers' buying is yet to be discovered. Instead, with the potential risk of information overload, does the extra information further distract the consumer from making focused comparison and decision?

The presence and richness of individual information types seem to vary largely with the type of product, for instance, electronics in general have more detailed accounts than clothing, shoes, and furniture. From our observation, this variance cannot be directly attributed to the search-experience-credence differences; instead, whether or not the critical features of the product can be evaluated digitally seems to play a more significant role. For example, the critical features of a dress are much more difficult to be digitized than those of a camera, even though they are both experience products. As presented at e-storefronts, the distinction between search products and experience products needs to be revisited and refined.

Our ultimate research goal is to systematically investigate and compare the effects of various types of product information on the online consumer decision-making process. We first apply a function-based topical information typology ([Huang 2009a](#hua09), [Huang 2013](#hua13)) to guide the systematic categorization of online product information and thus enable a focused comparison between different information types. The findings will provide a basis for prioritized information provisioning and organization which helps to remedy the pressing issue of information overload in e-retailing especially with small-screen mobile devices. Since the information type preferred for making online buying decisions varies significantly with the product type, retailers need to customize their information strategy to different types of products ([Girard _et al._ 2002](#gir02)). For example, on the one hand, Internet retailers could focus on direct information for selling search products and be more selective about providing other types of information. On the other hand, they want to intentionally offer more context, comparison, and evaluation information to complement standard product description, so as to “motivate consumers to purchase the difficult-to-sell products online” ([Girard _et al._ 2002](#gir02): Discussion). To combat information overload in e-commerce, simply limiting the absolute amount of product information is not enough; and it can also be problematic since insufficient information might in fact compromise consumers' ability to make fully informed decisions. A more promising strategy will be better designing, prioritizing, organizing, and tailoring product information to assist online shoppers to fully compare and evaluate products of interest and make well-informed purchases.

## Acknowledgements

We thank the anonymous reviewers for their constructive comments. This work is supported by the National Natural Science Foundation of China (NSFC) research grants 71102100 and 70971141, by China Postdoctoral Science Foundation research grant 1204014, and by the Fundamental Research Funds for the Central Universities of China grant 1209126.

## About the author

**Xiaoli Huang** is an Assistant Professor in Business School at Sun Yat-sen University, Guangzhou, China. She worked as an Enterprise Information Architect at the International Monetary Fund (IMF) from 2009-2010\. She received her MLIS and PhD from the College of Information Studies, University of Maryland at College Park. She can be contacted at: [hxiaoli3@sysu.edu.cn](mailto:hxiaoli3@sysu.edu.cn)  
**Dagobert Soergel** is a Professor in the Department of Library and Information Studies, Graduate School of Education, at the University at Buffalo, NY. He is also a Professor Emeritus in the College of Information Studies at the University of Maryland, College Park. He received his PhD (Dr.phil) in Political Science from University of Freiburg, Germany. He can be contacted at: [dsoergel@buffalo.edu](mailto:dsoergel@buffalo.edu)

<section>

## References

<ul>
    <li id="all05">Allen, D.K. &amp; Shoard, M. (2005). <a href="http://www.webcitation.org/6FGV5KZU2">Spreading the load: mobile information and communications technologies and their effect on information overload.</a> <em>Information Research</em>, <strong>2</strong>(10), paper 227. Retrieved 21 March, 2013 from http://informationr.net/ir/10-2/paper227.html (Archived by WebCite® at http://www.webcitation.org/6FGV5KZU2)
    </li>
    <li id="ant06">Antony, S., Lin, Z. &amp; Xu, B. (2006). <a href="http://www.webcitation.org/6FGVVDWVa">Determinants of escrow service adoption in consumer-to-consumer online auction market: an experimental study.</a> <em>Decision Support Systems</em>, <strong>42</strong>(3), 1889-1900. Retrieved 21 March, 2013 from http://zlin.ba.ttu.edu/papers/published/wits01-last.pdf (Archived by WebCite® at http://www.webcitation.org/6FGVVDWVa)
    </li>
    <li id="che09">Chen, Y., Shang, R. &amp; Kao, C. (2009). The effects of information overload on consumers' subjective state towards buying decision in the internet shopping environment. <em>Electronic Commerce Research &amp; Applications</em>, <strong>8</strong>(1), 48-58.
    </li>
    <li id="coo71">Cooper, W. (1971). A definition of relevance for information retrieval. <em>Information Storage and Retrieval</em>, <strong>7</strong>(1), 19-37.
    </li>
    <li id="dar73">Darby, M.R. &amp; Karni, E. (1973). Free competition and the optimal amount of fraud. <em>Journal of Law and Economics</em>, <strong>16</strong>(1), 67-88.
    </li>
    <li id="gir02">Girard, T., Silverblatt, R. &amp; Korgaonkar, P. (2002). <a href="http://www.webcitation.org/6FH0MvZsU">Influence of product class on preference for shopping on the internet.</a> <em>Journal of Computer-Mediated Communication</em>, <strong>8</strong>(1). Retrieved 21 March, 2013 from http://jcmc.indiana.edu/vol8/issue1/girard.html (Archieved by WebCite® at http://www.webcitation.org/6FH0MvZsU)
    </li>
    <li id="gre95">Green, R. (1995). Topical relevance relationships. I. Why topic matching fails. <em>Journal of the American Society for Information Science</em>, <strong>46</strong>(9), 646-653.
    </li>
    <li id="gre95-2">Green, R. &amp; Bean, C.A. (1995). Topical relevance relationships. II. An exploratory study and preliminary typology. <em>Journal of the American Society for Information Science</em>, <strong>46</strong>(9), 654-662.
    </li>
    <li id="for88">Ford, G.T., Smith, D.B. &amp; Swasy, J.L. (1988). An empirical test of the search, experience and credence attributes framework. In Micheal J. Houston (Ed.), <em>Advances in Consumer Research Volume 15</em>, (pp. 239-244). Provo, UT: Association for Consumer Research.
    </li>
    <li id="hjo10">Hjørland, B. (2010). <a href="http://www.webcitation.org/6FH8MPu5h">The foundation of the concept of relevance.</a> <em>Journal of the American Society for Information Science and Technology</em>, <strong>61</strong>(2), 217-237. Retrieved 21 March, 2013 from http://www2.hawaii.edu/~donnab/lis670/hjorland_relevance_2010.pdf (Archived by WebCite® at http://www.webcitation.org/6FH8MPu5h)
    </li>
    <li id="hua09">Huang, X. (2009a). <a href="http://www.webcitation.org/6FH6a2dP2">Developing a cross-disciplinary typology of topical relevance relationships as the basis for a topic-oriented information architecture</a>. <em>Advances in Classification Research Online</em>, <strong>20</strong>(1). Retrieved 21 March, 2013 from http://journals.lib.washington.edu/index.php/acro/article/download/12884/11380 (Archived by WebCite® at http://www.webcitation.org/6FH6a2dP2)
    </li>
    <li id="hua09-2">Huang, X. (2009b). <em>Relevance, rhetoric, and argumentation: a cross-disciplinary inquiry into patterns of thinking and information structuring.</em> Unpublished doctoral dissertation. University of Maryland, College Park, MD, USA
    </li>
    <li id="hua13">Huang, X. (2013). Applying a generic function-based topical relevance typology to structure clinical questions and answers. <em>Journal of the American Society for Information Science and Technology</em>, <strong>64</strong>(1), 65�85.
    </li>
    <li id="hua04">Huang, X. &amp; Soergel, D. (2004). <a href="http://www.webcitation.org/6FH92WhKD">Relevance judges' understanding of topical relevance types: an explication of an enriched concept of topical relevance.</a> <em>Proceedings of the American Society for Information Science and Technology</em>, <strong>41</strong>(1), 156-167. Retrieved 21 March, 2013 from http://malach.umiacs.umd.edu/pubs/XH_04_Rel_Judges.pdf (Archived by WebCite® at http://www.webcitation.org/6FH92WhKD)
    </li>
    <li id="hua06">Huang, X. &amp; Soergel, D. (2006). <a href="http://www.webcitation.org/6FH1bLXSr">An evidence perspective on topical relevance types and its implications for exploratory and task-based retrieval.</a> <em>Information Research</em>, <strong>12</strong>(1), paper 281. Retrieved 21 March, 2013 from http://informationr.net/ir/12-1/paper281.html (Archived by WebCite® at http://www.webcitation.org/6FH1bLXSr)
    </li>
    <li id="hua05">Huang, X. &amp; White, R.W. (2005). <a href="http://www.webcitation.org/6FH5PuqS4">Policy capturing models for multi-faceted relevance judgments.</a> <em>Proceedings of the American Society for Information Science and Technology</em>, <strong>42</strong>(1). Retrieved 21 March, 2013 from http://research.microsoft.com/en-us/um/people/ryenw/papers/huangasist2005.pdf (Archived by WebCite® at http://www.webcitation.org/6FH5PuqS4)
    </li>
    <li id="jac84">Jacoby, J. (1984). Perspectives on information overload. <em>Journal of Consumer Research</em>, <strong>10</strong>(4), 432-435.
    </li>
    <li id="jac74">Jacoby, J., Speller, D.E. &amp; Berning, C.K. (1974). Brand choice behavior as a function of information load: replication and extension. <em>Journal of Consumer Research</em>, <strong>1</strong>(1), 33-42.
    </li>
    <li id="jeo01">Jeong, M. &amp; Lambert, C.U. (2001). Adaptation of an information quality framework to measure customers' behavioral intentions to use lodging Web sites. <em>International Journal of Hospitality Management</em>, <strong>20</strong>(2), 129-146.
    </li>
    <li id="kel87">Keller, K.L. &amp; Staelin, R. (1987). Effects of quality and quantity of information on decision effectiveness. <em>Journal of Consumer Research</em>, <strong>14</strong>(2), 200-213.
    </li>
    <li id="kis97">Kiser, E. (1997). Comment: evaluating qualitative methodologies. <em>Sociological Methodology</em>, <strong>27</strong>(1), 151-158.
    </li>
    <li id="kle98">Klein, L.R. (1998). Evaluating the potential of interactive media through a new lens: search versus experience goods. <em>Journal of Business Research</em>, <strong>41</strong>(3), 195-203.
    </li>
    <li id="kor10">Korgaonkar, P., Becerra, E., O Leary, B. &amp; Goldring, D. (2010). Product classifications, consumer characteristics, and patronage preference for online auction. <em>Journal of Retailing and Consumer Services</em>, <strong>17</strong>(4), 270-277.
    </li>
    <li id="kor06">Korgaonkar, P., Silverblatt, R. &amp; Girard, T. (2006). Online retailing, product classifications, and consumer preferences. <em>Internet Research</em>, <strong>16</strong>(3), 267-288.
    </li>
    <li id="lal99">Lal, R., &amp;Sarvary, M. (1999). <a href="http://www.webcitation.org/6FH9wDLTf">When and how is the internet likely to decrease price competition?</a> <em>Marketing Science</em>, <strong>18</strong>(4), 485 -503. Retrieved 21 March, 2013 from http://www.econ2.jhu.edu/People/Harrington/375/ls99.PDF (Archived by WebCite® at http://www.webcitation.org/6FH9wDLTf)
    </li>
    <li id="lee04">Lee, B. &amp; Lee, W. (2004). The effect of information overload on consumer choice quality in an on-line environment. <em>Psychology and Marketing</em>, <strong>21</strong>(3), 159-183.
    </li>
    <li id="lin02">Lindlof, T.R. &amp; Taylor, B.C. (2002). <em>Qualitative Communication Research Methods</em> (2nd ed.). Thousand Oaks, CA: Sage Publications.
    </li>
    <li id="lur04">Lurie, N.H. (2004). <a href="http://www.webcitation.org/6FHAJPUez">Decision making in information-rich environments: the role of information structure.</a> <em>Journal of Consumer Research</em>, <strong>30</strong>(4), 473-486. Retrieved 21 March, 2013 from http://users.business.uconn.edu/nlurie/lurie_jcr_3_2004.pdf (Archived by WebCite® at http://www.webcitation.org/6FHAJPUez)
    </li>
    <li id="mae99">Maes, P., Guttman, R.H. &amp; Moukas, A.G. (1999). Agents that buy and sell: transforming commerce as we know it. <em>Communications of the ACM</em>, <strong>42</strong>(3), 81-91.
    </li>
    <li id="mal82">Malhotra, N.K. (1982). Information load and consumer decision making. <em>Journal of Consumer Research</em>, <strong>8</strong>(4), 419-430.
    </li>
    <li id="mal84">Malhotra, N.K. (1984). Reflections on the information overload paradigm in consumer decision making. <em>Journal of Consumer Research</em>, <strong>10</strong>(4), 436-440.
    </li>
    <li id="man88">Mann, W.C. &amp; Thompson, S.A. (1988). <a href="http://www.webcitation.org/6FHB8hixp">Rhetorical structure theory: toward a functional theory of text organization.</a> <em>Text</em>, <strong>3</strong>(8), 243-281. Retrieved 21 March, 2013 from http://www.cis.upenn.edu/~nenkova/Courses/cis700-2/rst.pdf (Archived by WebCite® at http://www.webcitation.org/6FHB8hixp)
    </li>
    <li id="mar02">Marsh, E.E. (2002). <em>Rhetorical relationships between images and text in web pages.</em> Unpublished doctoral dissertation. University of Maryland, College Park, MD, USA
    </li>
    <li id="mil92">Miller, W.L. &amp; Crabtree, B.F. (1992). Primary care research: a multimethod typology and qualitative road map. In B. F. Crabtree &amp; W. L. Miller (Eds.), <em>Doing qualitative research</em> (pp. 3-28). Thousand Oaks, CA, US: Sage Publications, Inc.
    </li>
    <li id="nel70">Nelson, P. (1970). Information and consumer behavior. <em>Journal of Political Economy</em>, <strong>78</strong>(2), 311-329.
    </li>
    <li id="nel74">Nelson, P. (1974). <a href="http://www.webcitation.org/6FHBr1bD3">Advertising as information.</a> <em>Journal of Political Economy</em>, <strong>82</strong>(4), 729-754. Retrieved 21 March, 2013 from http://time.dufe.edu.cn/jingjiwencong/waiwenziliao/20031148365645706.pdf (Archived by WebCite® at http://www.webcitation.org/6FHBr1bD3)
    </li>
    <li id="sen07">Sen, S. &amp; Lerman, D. (2007). <a href="http://www.webcitation.org/6FHCI9HBK">Why are you telling me this? An examination into negative consumer reviews on the Web.</a> <em>Journal of Interactive Marketing</em>, <strong>21</strong>(4), 76-94. Retrieved 21 March, 2013 from http://www.sba.oakland.edu/faculty/kim/2009/e-wom.pdf (Archived by WebCite® at http://www.webcitation.org/6FHCI9HBK)
    </li>
    <li id="sen04">Senecal, S. &amp; Nantel, J. (2004). <a href="http://www.webcitation.org/6FHCY8uL2">The influence of online product recommendations on consumers' online choices.</a> <em>Journal of Retailing</em>, <strong>80</strong>(2), 159-169. Retrieved 21 March, 2013 from http://ses.telecom-paristech.fr/survey/CanauxInformBienExpe/senecalnantel.pdf (Archived by WebCite® at http://www.webcitation.org/6FHCY8uL2)
    </li>
    <li id="str90">Strauss, A.L. &amp; Corbin, J.M. (1990). <em>Basics of qualitative research: grounded theory procedures and techniques</em>. Thousand Oaks, CA: Sage Publications.
    </li>
    <li id="tab06">Taboada, M. &amp; Mann, W.C. (2006). <a href="http://www.webcitation.org/6FHCtbg2I">Rhetorical structure theory: looking back and moving ahead.</a> <em>Discourse Studies</em>, <strong>8</strong>(3), 423-459. Retrieved 21 March, 2013 from http://www.sfu.ca/~mtaboada/docs/Taboada_Mann_RST_Part1.pdf (Archived by WebCite® at http://www.webcitation.org/6FHCtbg2I)
    </li>
    <li id="tho96">Thompson, I. (1996). Competence and critique in technical communication. <em>Journal of Business and Technical Communication</em>, <strong>10</strong>(1), 48-80.
    </li>
    <li id="tro05">Trochim, W. &amp; Kane, M. (2005). Concept mapping: an introduction to structured conceptualization in health care. <em>International Journal for Quality in Health Care</em>, <strong>17</strong>(3), 187-191.
    </li>
    <li id="ver09">Vermeulen, I.E. &amp; Seegers, D. (2009). <a href="http://www.webcitation.org/6FHDM9r36">Tried and tested: the impact of online hotel reviews on consumer consideration.</a> <em>Tourism Management</em>, <strong>30</strong>(1), 123-127. Retrieved 21 March, 2013 from http://home.fsw.vu.nl/ie.vermeulen/vermeulen_ea_tm2008.pdf (Archived by WebCite® at http://www.webcitation.org/6FHDM9r36)
    </li>
    <li id="wil02">Wilson, D. &amp; Sperber, D. (2002). <a href="http://www.webcitation.org/6FHDVRdCi">Truthfulness and relevance.</a> <em>Mind</em>, <strong>111</strong>(443), 583-632. Retrieved 21 March, 2013 from http://www.dan.sperber.fr/wp-content/uploads/2009/09/wilson_sperber.pdf (Archived by WebCite® at http://www.webcitation.org/6FHDVRdCi)
    </li>
    <li id="wil73">Wilson, P. (1973). Situational relevance. <em>Information Storage and Retrieval</em>, <strong>9</strong>(8), 457-471.
    </li>
    <li id="yan98">Yang, J., Honavar, V., Miller, L. &amp; Wong, J. (1998). Intelligent mobile agents for information retrieval and knowledge discovery from distributed data and knowledge sources. <em>IEEE Information Technology Conference (1998), Information Environment for the Future,</em> (pp. 99-102). Washington, DC: IEEE.
    </li>
</ul>

</section>

</article>