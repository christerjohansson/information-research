#### Information Research, Vol. 3 No. 2, September 1997


#Five things to change first when academic libraries embrace Web access

#### [Dr Kay Flatten](mailto:Kay.Flatten@uce.ac.uk)  
Project Manager, TAPin  
University of Central England  
Birmingham, UK

#### Abstract

> This article presents the lessons learned when six universitiesin the West Midlands, UK worked collaboratively within the eLib ([Electronic Libraries Programme](http://www.ukoln.ac.uk/services/elib/)). These libraries and learning resource centres spent two years focused on a culture change in the support provided to education, law, and life sciences subject departments. Their efforts resulted in [a mediation Model](http://www.uce.ac.uk/tapin/) for networked information. The project manager for [TAPin](http://www.uce.ac.uk/tapin/tapin2.htm) reflects upon the importance of attitudes, infrastructure, staff skills, appearances and service practices in the process of change.

## Attitudes

**_Management:_** Change required a commitment from senior management within their libraries and learning resource centres. This commitment was unlikely if attitudes were less than 100% in favour of change. Early adoption of technology remained costly, thus posing a threat to limited finances. Heads of LIS (Library and Information Services) met regularly to share plans and concerns. This collaborative environment resulted in positive attitudes about a range of networked information services. Managers received comfort in knowing what others were doing. It was not just a matter of "keeping up", it was a means of assuring a tested way forward.

Managers practised their commitment. Early in the change process they adopted e-mail communication and Web access from their desktops. They began to share a language which encompassed networked resources and access. They asked for outputs from staff which indicated the importance they placed upon the Internet.

At this writing, less is known about the managers of the subject departments. An _[Impact Study]( http://www.uce.ac.uk/tapin/impact.htm)_ is due out in February (note that this link will not work before then). However, it appeared that the subject department heads had a wide variation in their understanding and appreciation of networked information in teaching and learning. No "champion" among equals surfaced, and in fact the term "Luddite" was occasionally heard. The subject department heads were consumed with the management of devolved budgets and meeting enrolment targets. This fostered a closed shop attitude. The outward searching and collaborative processes used by the LIS managers was not present.

The attitudes of senior managers in computing services likewise varied. Less enthusiasm for change emerged than expected. Perhaps a threat was perceived in new roles in networked information support. More likely, the IT services were faced with such colossal changes they did not actively encourage uptake in networked behaviours. This would have increased the demand on their services, which they were already working full out to provide.

The LIS managers influenced the attitudes of the subject department heads and heads of computing. They were positive and non-confrontational and appeared in step with university practices and policies.

**_Professional staff:_** The LIS professional staff were cautious about Web resources and services. Their initial attitudes were apprehension and doubt. It was clear this would only change when they received hands on training with networked resources. Without computers on their desks they had a limited and commercial vision of the Web. They were not in a position to control change, yet they knew change was imminent. This increased their negative attitudes and resistance. The attitudes of their senior managers and a forty-hour training programme moved things on. This programme offered the same benefits experienced by their managers in their own collaborative meetings. It took eight months to begin to see the positive messages in LIS professional staff communications and practices. It took 10 months to enlist their enthusiasm. By the end of one full year their attitudes had matured to reflect the wisdom of experience.

## Infrastructure

**_Networked campus:_** Which came first, the attitude change or the computer networks? Both scenarios were true in the TAPin project. Some universities were well advanced in their network hardware and software, others were still laying cable two years ago. A culture change occurred in both; however, the universities where attitudes preceded hardware experienced the greatest change. Perhaps this was because they had further to go. Even so, it was the vision to use new resources that fostered implementation. Attitudes before hardware meant new behaviours were placed alongside new technology.This sustained the first year of change. If the campus networks were not in place by year two, it was unlikely that libraries and information resource centres would have moved to Web access. Telneting was not acceptable for regular access and development work. Reliable e-mail and Web servers underwrote the other four points in this article.

**_Standards:_ **The eLib programme understood early on the importance of seamless access. [UKOLN has collected opinions and written in this area](http://www.ukoln.ac.uk/services/elib/papers/other/standards/intronew.html). These were loosely followed in the project. The important point was compatibility between machines crossing service boundaries. When the LIS professionals accessed the Web through interfaces at variance with their end-users, difficulties resulted. These were only resolved when the LIS staff learned to use a variety of interfaces or tailored their support. These LIS staff initially doubted the functionality of the Web. Their attitude change was not helped when cross campus platforms varied. There was little the libraries and learning centre managers could do to solve these problems. Unfortunately the change process involved everyone's ability to cope.

**_End-user machines:_ **The absence of desk top access for LIS professionals and academics was a barrier to culture change. When a networked 486 or faster PC or MAC was readily available new behaviours were possible. Web access services required machines running Windows with Web browsers and e-mail software. Other software was also beneficial, such as Adobie Acrobat editor and reader, HTML authoring tools, PowerPoint, and FTP packages. Two years on we are looking at Windows '95, MS Office '97 and Web management software like FrontPage. These would quickly exceed the capacity of the machines on desktops today. No one knows where, if, or when this will settle to a reasonable expense for LIS and subject departments.

## Staff Skills

The endless requirement to upgrade staff skills followed on from the constant upgrades in IT. Perceptions and reality were at odds when initially assessing LIS staff skills. Dan Fogelberg said it best in his song, _The Higher I climb_ from _[High Country Snows](http://www.in-your-ear.com)_:

> _The higher I climb, the more that I see.  
> The more that I see, the less that I know.  
> The less that I know, the more that I yearn.  
> The more that I yearn, the higher I climb._

Once training began, the need for more training followed.

Working together and learning new skills built confidence and positive attitudes. Increased confidence and positive attitudes made further learning possible. By the end of year one, LIS staff were requesting further training. They credited the training programme for making their Web support possible. [Netskills](http://www.netskills.ac.uk/) provided most of the training, and their on-line TONIC package is recommended. The social element of training together as a group of subject specialists from different universities accounted for much of the success.

## Appearances

**_Promotion:_ **Changing the image of the academic libraries from warehouses of books and journals to places of access for print and electronic information was urgent. The LIS professional staff did not market a new image. One academic gave this response when advised to seek help from the LIS staff when searching the Web . "Why would I go to the library to search the Web?" The LIS staff skills in information searching and quality checking did not seem to translate to this new medium.

[Jane Farmer and Fiona Campbell](#farm) researched skills used by librarians and by managers. They reported the managers were significantly more skilled in the following three areas:

1.  Promotion of product
2.  Marketing of product
3.  Oral presentation of information

Perhaps the service ethos of the profession led to the lack of awareness raising activity in TAPin LIS. It may also be related to a low level of confidence in network performance and Web searching skills. The blurring of computing services and library services may also have kept promotion activity low. One academic was heard to say that librarians should not be teaching about the Internet to classes of students. Whatever the reasons, the need to promote Web mediation and services was needed within the LIS, across campus, and on-line.

**_Signage:_ **The messages conveyed on signsand bulletin boards within the LIS did not indicate the cultural shift from holdings to access. Directional signs pointed to collections of print material. Signs also indicated where OPACs or CD-ROMs were located. Seldom were there signs to terminals dedicated to Web access. When these were found they were usually set to one URL such as the British Library Web site. The sign indicated which Web resource was available from that machine. Terminals for general Web searching were seldom found. Even the enquiries desks where LIS staff had Web access did not have signs telling patrons that Web mediation was available from the desk.

Signs were also used to convey policy. The content of these assumed the user was physically in the library, e.g. opening hours, fines for overdue materials. Rare was the sign indicating how to access resources outside the building. Areas of the LIS where subject specific collections were housed did not have bulletin boards displaying useful Web sites and e-mail addresses for subject support. Patrons were expected to find the information in-house or find a member of staff to consult directly. There was little in appearances indicating that surfing the Web was welcomed or even possible from within the LIS.

Remote access to the LIS gave a better image of the Web. University homepages had links to the libraries and learning resource centres. These LIS pages linked to subject support pages where collections of URLs and e-mail links were evident. These valuable resources were hidden within larger Web sites. They remained unsupported by signs and printed publications.

**_Printed guides:_ **The messages conveyed in signs and posted notices were reflected in printed LIS publications. These guides focused on locating and using in-house holdings or LIS policies. Guides for electronic resources were usually specific to CD-ROM titles and assumed the patron was in the LIS. Information available from the LIS Web site was not supported in parallel print. Signs and printed guides promoting Web access may happen in the near future. There was talk about not wanting to build unfulfilled expectations. Until the IT and staff training were complete and service procedures in place, a high demand for Web access and expertise could prove problematic.

## Service practices

The cascade affect of TAPin had not reached the counter staff. Enquiries about Web access resulted in blank looks and confused messages from various service points e.g., the issue desk. Presumably, every worker in the LIS knew how printed books and journals were ordered, processed and issued. With the exception of inter-library loans, they did not have the same understanding about remote resources. The usual service practice was to direct the patron to a particular member of staff who "knew about the Web". Service practices; regarding accessing the Web from within the LIS, printed and human support for Web access, and addressing remote users, will enevitably change. Judith [Quilter](#quil), a member of the diglibns discussion list made this observation: "Since our webpage has been available our photocoying requests and ILLs by e-mail have increased. Some of the e-mails have up to 40 requests at a time."

## Conclusion

It was easy to identify the changes which have not occurred in the TAPin LIS. Years of establishing quality collections and reliable services created an inertia which will take more than two years to modify. The management commitment and staff training did much to change attitudes. The campus networks and high performance PCs are now in place. These three factors will result in changes in how the Web is presented and supported at all levels within the LIS. Should promotional strategies accompany these changes, the Web will play an active role in academic libraries and learning resource centres. Mediation and support for Web use will provide a value added service.

## References

*   <a name="farm"></a>[Farmer](mailto:j.farmer@rgu.ac.uk), Jane & Campbell, Fiona (1997). Measuring continuing education needs: identifying transferable skills through mentoring <u>in</u> Ward, Patricia & Weingand, Darlene (eds) (1997). _Human Development: Competencies for the Twenty-First Century._ Muchen: K.G. Saur pp284-289\. E-mail at: j.farmer@rgu.ac.uk
*   <a name="quil"></a>[Quilter](mailto:jquilter@rveeh.vic.gov.ac), Judith, on mailing list diglibns [DIGLIBNS:515] Benefits of Internet Access at: diglibn@sunsite.berkeley.edu