<!DOCTYPE html>
<html>
<head>
 <link rev="made" href="mailto:t.d.wilson@shef.ac.uk"> 
<link rel="stylesheet" href="style.css">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta name="keywords" content="information strategy, Ghana, university libraries, universities, finance, planning">
<meta name="description" content="This article describes on-going research on the development of a library strategy for universities in Ghana.  The research focuses on the factors affecting  the development of a strategic planning process aimed at improving the libraries' capacity to deliver  information services effectively and efficiently. Since the structure of universities in Ghana is, to a great extent, derived from or modelled on that of universities in the United Kingdom the project of necessity also includes some consideration of  current attitudes to the strategic planning process in a number of  university libraries in the United Kingdom. It is hoped that the study and evaluation of this aspect of the management of United Kingdom university libraries may provide pertinent guidelines for university library management in Ghana.">
<meta name="VW96.objecttype" content="Database">
<link rel="schema.vw96" href="http://vancouver-webpages.com/VWbot/VW96-schema.html">
<meta name="ROBOTS" content="ALL">
<meta name="DC.title" content="Towards an information provision strategy for university libraries in Ghana">
<meta name="DC.creator" content="Edwin Ellis Badu and Brendan Loughridge">
<meta name="DC.subject" content="Planning information strategies in university libraries in Ghana">
<meta name="DC.description" content="This article describes on-going research on the development of a library strategy for universities in Ghana.  The research focuses on the factors affecting  the development of a strategic planning process aimed at improving the libraries' capacity to deliver information services effectively and efficiently. Since the structure of universities in Ghana is, to a great extent, derived from or modelled on that of universities in the United Kingdom the project of necessity also includes some consideration of  current attitudes to the strategic planning process in a number of  university libraries in the United Kingdom. It is hoped that the study and evaluation of this aspect of the management of United Kingdom university libraries may provide pertinent guidelines for university library management in Ghana.">
<meta name="DC.publisher" content="Department of Information Studies, University of Sheffield">
<meta name="DC.coverage.placeName" content="Ghana">
</head>
<body>
<h4 id="vol-3-no-2-september-1997">Vol. 3 No. 2, September 1997</h4>
<h1 id="towards-an-information-provision-strategy-for-university-libraries-in-ghana">Towards an information provision strategy for university libraries in Ghana</h1>
<h4 id="edwin-ellis-badu-and-brendan-loughridge">Edwin Ellis Badu and <a href="mailto:b.loughridge@shef.ac.uk">Brendan Loughridge</a></h4>
<p>Department of Information Studies<br>
University of Sheffield</p>
<h2 id="introduction">Introduction</h2>
<p>This article describes on-going research on the development of a library strategy for universities in Ghana. The research focuses on the factors affecting the development of a strategic planning process aimed at improving the libraries' capacity to deliver information services effectively and efficiently. Since the structure of universities in Ghana is, to a great extent, derived from or modelled on that of universities in the United Kingdom the project of necessity also includes some consideration of current attitudes to the strategic planning process in a number of university libraries in the United Kingdom. It is hoped that the study and evaluation of this aspect of the management of United Kingdom university libraries may provide pertinent guidelines for university library management in Ghana.</p>
<h2 id="problems-facing-university-libraries-in-ghana">Problems facing University libraries in Ghana</h2>
<p>The main problem facing university libraries in Ghana in the 1990's is their inability to acquire library materials and to provide information services even at the levels achieved as long ago as the late 1950s. Amongst the many reasons for this the most important has been government instability, particularly during the period between 1966 and 1983 when various governments, mainly led by the military, showed little regard for higher education and universities were consequently inadequately funded. Additionally, student involvement in the political struggle against military rule also had adverse consequences on university and university library development since universities were closed down on a number of occasions, in some cases for months on end. Closely related to Ghana's political instability has been the gradual and persistent decline in the country's economy. When it achieved independence from Britain in 1957 Ghana inherited an economy that was seen as among the best of all the developing countries. Its per capita national income of £70 in 1960, for example, was significantly higher than that of Egypt (£56), Nigeria (£29) or India (£25) (Huq, 1989: 2). However, the national currency, the cedi, which averaged 350 to the US dollar in 1990 was, by the middle of December, 1996, trading at 1,725 to the dollar (<a href="#fina">Financial Times, 1996</a>).</p>
<p>One result of this has been the inability of the universities and their libraries to purchase computers, books, other information products and to develop their communication infrastructure owing to the constant shortage of foreign exchange. This economic predicament has also affected the purchasing power of the salaries and wages of Ghanaian workers, resulting in negative organisational behaviour (<a href="#kiss">Kissiedu, 1994</a>). In a situation where a worker's official monthly pay-cheque is barely sufficient to maintain him for a week, the need to look elsewhere to make up the considerable shortfall has resulted in conflicts between employer and the employees. People work for the minimum number of hours at their 'official' jobs and then spend the rest of the time at all kinds of 'extra' personal pursuits just to make ends meet.</p>
<p>Absenteeism, bad time keeping at all levels and lax supervision at senior level are all manifestations of the frustration, lack of commitment, absence of direction and laziness that dominate the university labour scene, resulting in a drastic decline in the effectiveness of universities and their libraries as institutions of higher education.</p>
<p>Other demographic issues such as a decrease in the number of university staff and a 30% increase in the size of the student population, a result of a government directive to the universities to absorb the back-log of two successive A' Level cohorts who had not gained admission to the universities because of university closures over the years (an increase not matched by increases in funding) have had a serious affect on library accommodation.. University libraries that were built to cater for about 2,000 students in the middle of the twentieth century now have to accommodate about 7,000 students.</p>
<p>In the global context too, the cost of books, periodicals and other library materials and equipment has risen steeply. New technologies are changing the ways information is generated, recorded, gathered, stored, preserved, analysed, disseminated and used by people and consequently the ways in which libraries are expected to provide access to information. Economic conditions have effectively prevented Ghanaian university libraries from developing such services at the required level so far.</p>
<p>In the midst of all these environmental uncertainties and turbulence, typical of most African and other developing countries (<a href="#flor">Flores, 1972</a>; <a href="#osha">O'Shaughnessy, 1985</a>; <a href="#yava">Yavas, Kaynak and Dibe, 1985</a>; <a href="#adeg">Adegbite, 1986</a>), universities in Ghana are trying to develop a management culture which they hope will improve university management in terms of planning, cost effectiveness, efficiency and accountability. It is against this background that universities and their university libraries in Ghana have become involved in the strategic planning process.</p>
<h2 id="theoretical-perspective">Theoretical perspective</h2>
<p><a href="#genu">Genus</a> (1995) presents five varieties of approach to strategy formulation:</p>
<ul>
<li>i) Linear(or 'rational') planning</li>
<li>ii) Adaptive/Incremental view</li>
<li>iii) Interpretative view</li>
<li>iv) Systems thinking</li>
<li>v) 'Garbage can' and population ecology view (<a href="#genu">Genus, 1995:10</a>)</li>
</ul>
<p>However the dominant approach to strategy in the main stream textbooks has been the linear approach (<a href="#whit">Whittington, 1993</a>). Labelled 'linear' by <a href="#chaf">Chaffee</a> (1983), 'rational' by <a href="#pete">Peters and Waterman</a> (1982), 'formal' by many others and the 'planning mode' by <a href="#mint73">Mintzberg</a> (1973), this classical approach to strategy has been defined by <a href="#chan">Chandler</a> (1962) as:</p>
<blockquote>
<p>&quot;...the determination of the basic, long-term goals and objectives of an enterprise, and the adoption of courses of action and the allocation of measures necessary for those goals.&quot; (<a href="#chan">Chandler, 1962:13</a>)</p>
</blockquote>
<p>Based on Chandler's definition, the linear approach assumes three basic tenets:</p>
<blockquote>
<p>Strategy formulation should be a controlled process of thought, derived directly from the notion of rational economic man- strategy as product of a single entrepreneurial individual acting with perfect rationality to maximise 'his' economic advantage.</p>
</blockquote>
<blockquote>
<p>Strategies emerge from the decision-making process fully formulated, explicit and articulated: strategies are in a sense orders for others to carry out</p>
</blockquote>
<blockquote>
<p>Implementation is a distinct phase in the strategy process only coming after the earlier phase of explicit and conscious formulation. (<a href="#mint90">Mintzberg, 1990</a>)</p>
</blockquote>
<p>The stages involved in linear planning are shown in figure 1.</p>
<p>Several authors have defined strategy in terms of the relationship between an organisation and its environment. One such definition is:</p>
<blockquote>
<p>&quot;The positioning and relating of the firm/organisation to its environment in a way which will assure its continued success and make it secure from surprises&quot; (Ansoff, 1984)</p>
</blockquote>
<p>The environment therefore dictates what are called key success factors - the factors that an organisation really needs to address for long-term competitive advantage or strategic success (Thompson, 1990).</p>
<p>Wheelan and Hunger (1990) discuss these factors under external and internal environments. The external environment consists of variables that are outside the organisation and not within the short-term control of top-management. It has two parts: the task environment and the societal environment. The task environment involves those elements or groups that directly affect and are affected by an organisation's major operations such as stake-holders, governments, suppliers, competitors, customers, interest groups, unions and associations. The societal environment variables include economic, sociocultural, technological and political/legal forces. These do not directly touch on the short-term activities of the organisation but they can, and often do, influence its long-term decisions. The internal environment variables form the context in which work is done and include the organisation's structure, culture and resources.</p>
<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #444444 solid; border-top: #444444 solid; font-size: smaller; border-left: #444444 solid; border-bottom: #444444 solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fffff"> 
<caption align="bottom"><br><strong>
**Figure 1 A linear model of strategic management. (Source: Genus, 1995))**</caption>
<tbody>
<tr>
<th width="40%">Stage</th>
<th width="60%">Action</th>
</tr>
<tr>
<td valign="top">1\. Objective Setting</td>
<td valign="top">Decide objectives; define performance targets</td>
</tr>
<tr>
<td valign="top">2\. Gap Analysis</td>
<td valign="top">Use forecasts to estimate gap between performance and existing strategy and targets set above</td>
</tr>
<tr>
<td valign="top">3\. Strategic Appraisal</td>
<td valign="top">Perform external/internal analysis to evaluate current competitive standing. Alter targets and objectives if needed.</td>
</tr>
<tr>
<td valign="top">4\. Strategic formulation</td>
<td valign="top">Generate alternative options. Evaluate the options and select a strategy</td>
</tr>
<tr>
<td valign="top">5\. Strategic Implementation</td>
<td valign="top">Detail action plans and resource requirements; monitor and control strategy</td>
</tr>
</tbody>
</table>
<p>Applying this theory to the study of libraries, Vincent (1988) identified specific variables that affect the success of strategy in libraries in a multiple case study in which she concludes that:</p>
<ul>
<li>Lack of resources is one reason why libraries may be reluctant to engage in strategic planning.</li>
<li>The stability of the environment and sufficient control over it ensures the attainment of organisational objectives hence strategy formulation.</li>
<li>Publicly funded service organisations have a relationship with the environment which is often characterised by turbulence, unpredictability and the imposition of short-term objectives and structures which, bear little relation to the dependant organisations' circumstances.</li>
<li>Success of strategy in libraries is affected by weight of tradition, vested interests and corporate expectations.</li>
<li>A single, agreed, clearly articulated mission is an essential prerequisite for successful strategic planning. (Vincent, 1988: 42-43)</li>
</ul>
<p>Ferguson (1992), writing on strategic planning for libraries in developing countries, states that strategic planning is affected by basic influences, namely: the fundamental socio-economic characteristic and purpose of the organisation, the values and philosophy of management and the organisations strengths and weaknesses in the light of the external and internal environments. He states further that strategic planning is likely to be affected by the location of the organisation because these factors vary with different cultural socio-economic and historical environments.</p>
<p>Wilks (1990) also finds that the Anglo-Saxon cultures of the United States and the United Kingdom are biased towards an individualistic free-enterprise model of strategy that denigrates explicit reliance upon the state.</p>
<p>It is therefore proposed that if the success of strategic planning is affected by environmental factors then the model as it is presently applied in United Kingdom university libraries will not translate to Ghana because of its goal of profit maximisation (Ansoff, 1965; Porter, 1980, 1985), insensitivity to sociological culture (Whittington, 1993), its assumption of the availability of considerable resources of time, people, money and expertise (Grant, 1991b) and its reliance on stable political and economic domains (Cyert and March, 1963; Boyacigiller and Alders, 1991).</p>
<h2 id="methodology">Methodology</h2>
<h3 id="research-design">Research Design</h3>
<p>This study adopts a multiple case study approach since this permits the use of a combination of varied data collection techniques. Ford (1977), among others, has advocated the use of such a multimethod approach and asserts that &quot;... a combination of methods is often more appropriate in order to make use of their different advantages and to encounter the individual weaknesses and biases of a method used in isolation.&quot; Some traditional prejudices against the case study strategy has been debated in sociological studies since the idea was first developed by Bronislow, Malinowski, Frederic le Play and some members of the Chicago school (Hamel et al., 1993:2). The two most basic shortcomings faults of the approach debated in the literature can be summarised as:</p>
<blockquote>
<p>its lack of representativeness and especially the lack of representativeness of the case used as a point of observation for the social phenomenon or issues constituting the object of study.</p>
</blockquote>
<blockquote>
<p>Its lack of methodological rigor - rigor in the collection, construction and analysis of the empirical materials that give rise to this study. This lack of rigor is linked to the problem of bias. Such bias is introduced by the subjectivity of the researcher, as well as of the field informants on whom the researcher relies to get an understanding of the case under investigation. (Hamel et al., 1993)</p>
</blockquote>
<p>In the present study the following procedures have been used:</p>
<p>1. The research has been guided by a theoretical proposition. A series of research questions based on the study's research objectives were used to collect the data which would be used to prove or disprove the proposition made. Yin (1989:70-80) refers to this as a 'case study protocol'.</p>
<p>2. The problem of representativeness is overcome by the use of both the interpretative and positivist methods. Smith (1989) and Walsham (1993) have argued that epistemology, the basis of one's claim of knowledge and research methods are interrelated. If one adopts only a positivist epistemological stance then statistical generalisability is the key goal. However, if one assumes an interpretative position as is the case in the first phase of this research, then the validity of an extrapolation from an individual case or cases depends not on the representativeness of such cases in a statistical sense, but on the plausibility and cogency of the logical reasoning used in describing the results from the cases and in drawing conclusions from them.</p>
<p>3. The multisite and external validity measures adopted have, it is believed, contributed to the reliability of the findings. The study is uses five cases in Ghana and five in the United Kingdom. None of the cases has not been considered as a sample. In-depth study has been carried out in each case by utilising four modes of data collection techniques. The investigation has striven to generalise a set of results to its broad theoretical proposition for its first case study. The resultant theories from the first case have been tested in the subsequent cases, the intention being to discover whether the same results would occur in at least three cases to achieve literal replication. Yin (1994) refers to this manner of achieving representativeness as external validity and states that:</p>
<blockquote>
<p>&quot; A theory must be tested through replications of the findings in a second or even a third neighbourhood, where the theory has specified that the same results should occur. Once such replication has been made, the results might be accepted for a much larger number of similar neighbourhoods, even though further replications have not been performed. This replication logic is the same that underlines the use of experiments (and allows scientists to generalise one experiment to another)&quot; (Yin, 1994:33)</p>
</blockquote>
<h3 id="mixed-methodology">Mixed methodology</h3>
<p>The methodology combines qualitative and quantitative approaches in one single study. This combination of research methods in the same phenomenon has been called triangulation by Denzin (1978). The research is, therefore, based on both the interpretative and positivist paradigms. This concept of mixing methods has stimulated much debate in the social sciences (Guba, 1992; Patton, 1988). Reichardt and Cook (1979) advocate a choice of one or the other rather than combining them. Rossman and Wilson (1985) and Lancy (1993) assume a pragmatic stance and argue that a false dichotomy exists between qualitative and quantitative approaches and that researchers should make the most efficient use of both paradigms in understanding social phenomena.</p>
<p>This study has applied these two methods appropriately at different stages of the research. The interpretative has been used in an exploratory fashion to investigate the factors that affect strategy in Ghanaian universities. The search for literature on strategic planning in university libraries in Ghana produced nothing of relevance. The lack of prior research and the desire and need to explore the opinions of key university personnel indicated that an interpretative study had to be done. The results of the interpretative study were then used as the basis for a large positivist study of the same research questions.</p>
<h3 id="selection-of-cases-and-subjects">Selection of cases and subjects</h3>
<p>Five cases each were selected for the study in the United Kingdom and Ghana. These were libraries which were believed to be either going through the strategic planning process or about to start on it. The subjects in the Ghanaian situation were the major stake-holders of the university libraries who at the period of conducting the interviews (Jan-March 1996) were influential in matters regarding university and library policies, decision-making, funding/disbursement of funds, employment/promotion and all general matters affecting the libraries directly or indirectly. Some senior executives of the Ministries of Finance and Education in Ghana were also interviewed. In all, 63 individuals employed in the five universities in Ghana were interviewed. The university librarians and their deputies were selected for the UK interviews. Using a semi-structured interview schedule a broad spectrum of subjects was covered and the interviewees were invited to discuss their experiences of the management of universities and their libraries principally in relation to:</p>
<pre width="132">External environment                Goals/objectives of                  
                                    universities/libraries               

Political changes-macro and micro   Strategic plans of                   
levels                              universities/libraries               

Funding of universities/libraries   Vision statements                    

Library infrastructure              Mission statements                   

Human resources                     Organisational culture               

Information Technology                                                   
</pre>
<p>Documentary evidence was also collected and used to inform discussion of the Ghanaian environment and the libraries' internal environments. Strategic planning documents of the cases in UK and Ghana were also analysed. On some occasions non-participant observation; although this was occasional, casual and informal some organisational habits were noted. These two instruments were used to corroborate and augment evidence from the other sources of data collection.</p>
<hr>
<pre width="132">      **Library**            **Number**           **Number**        **Percentage**     
                       **distributed**       **received**        **response:**     

     Library A             40               32             80.00       

     Library B             30               24             80.00       

     Library C             30               23             76.67       

     Library D             12               11             91.67       

     Library E              8               8               100        

                                                      Overall: 81.67   

       Total               120              98        Average: 85.67   

</pre>
<hr>
<div>Table 1 Questionnaire-distribution and responses</div>
<p>A fourth instrument used was a questionnaire. The questionnaires were distributed to a larger population of library staff in all the five cases in Ghana. This population consisted of all categories of professional, sub-professional and non-professional library staff. Table 1 shows the distribution of the questionnaires by library as well as the number of responses. A very high response was achieved. A second set of questionnaires was mailed to the respondents when it became necessary to test new findings of the qualitative investigation. Responses to these are currently awaited.</p>
<h2 id="interim-results-and-conclusion">Interim results and conclusion</h2>
<p>An initial analysis of the interview data using some aspects of the grounded theory technique (Glaser and Strauss, 1967) produced the following provisional findings which were tested in the quantitative phase:</p>
<h3 id="1-vision-for-universitylibrary">1 Vision for university/library</h3>
<p>A considerable proportion of university staff would like to see an expansion of university and library physical facilities compared with the government officials surveyed who foresaw cuts in university funding in favour of increased expenditure on primary and secondary education.</p>
<h3 id="2-staff-awareness-of-and-participation-in-strategic-planning">2. Staff awareness of and participation in strategic planning</h3>
<p>Not all the library staff took part in the strategic planning process. Professional librarians participated more significantly in the planning process than the staff at lower levels. There appears to be a significant correlation between participation in the planning process and the educational level of staff. The majority of the library staff were not aware of the existence of any strategic planning documents.</p>
<h3 id="3-organisational-culture">3. Organisational culture</h3>
<p>There is a bureaucratic task type of organisational structure in libraries. The working environment is mechanistic, with staff having narrowly defined functions and a set of job descriptions. Within a hierarchical staff structure the management style is autocratic and is not conducive to participation.</p>
<h3 id="4-funding">4. Funding</h3>
<p>Libraries exist in a highly unstable economic environment. The low levels of resource allocation to universities is a result of the poor relationship between government and universities. Monthly payment of government subventions to universities makes long-term planning difficult. Libraries have no control over their own resources and have no reserves or surpluses to support new initiatives.</p>
<h3 id="5-politics">5. Politics</h3>
<p>Recent changes in government educational policies have had a significant impact on the rate university growth and development. The national government accords education a high priority but the majority of those surveyed believed it had a low regard for university education. Strike action by local political groups is a major cause of disruptions in library services and development. Some teaching staff have a very low opinion of the library which is reflected in the poor allocation of resources for library operations.</p>
<h3 id="6-external-factors">6. External factors</h3>
<p>The political and economic sectors of the environment have had a more significant impact on universities/libraries than the other three sectors ( technology, social and international). The university stake-holders reported that of the five environment factors the international environment has been the most favourable to universities and libraries. It was felt that the universities/libraries needed to deal more with the political and economic factors of the environment than with the other three.</p>
<h3 id="7-information-technology">7. Information Technology</h3>
<p>There is a low level of computer technology application in the university libraries. No library housekeeping services have been automated in any of the university libraries. A substantial proportion of the library staff do not use computers in their work. There is a low level of technology and management education among library staff. The technology required to systematically monitor the environment and to collect necessary is lacking.</p>
<h2 id="conclusion">Conclusion</h2>
<p>The analysis of the interviews with university librarians in the United Kingdom has not yet been completed and the supplementary questionnaires from Ghana are still outstanding. It is hoped to publish the final results and discussion of the findings in a future issue of <em>Information Research.</em></p>
<h2 id="references">References</h2>
<ul>
<li><a name="adeg"></a>Adegbite, O. (1986) Planning in Nigerian business. <em>Long Range Planning</em>, 19 (2), 98-103</li>
<li><a name="anso1"></a>Ansoff, H. I. (1984) <em>Implementing strategic management</em>. Englewood Cliffs, N.J.: Prentice Hall.</li>
<li><a name="anso2"></a>Ansoff, H. I. (1965) <em>Corporate strategy</em>. Harmondsworth: Penguin.</li>
<li><a name="boya">Boyacigiller, N</a>. and Alder, N. (1991) The parochial dinosaur: organisational science in a global context. <em>Academy of Management Review</em>, 16 (2), 262-90.</li>
<li><a name="chaf"></a>Chafffee, E.E. (1983) Three models of strategy. <em>Academy of Management Review</em>, 10 (1), 89-98.</li>
<li><a name="chan"></a>Chandler, A.D. (1962) <em>Strategy and structure. Chapters in the History of the Industrial Enterprises.</em> Cambridge, Mass.: MIT Press.</li>
<li><a name="cyer"></a>Cyert, R. M. and March, J.G. (1963) <em>A behavioural theory of the firm</em>. Englewood Cliffs: Prentice Hall.</li>
<li><a name="denz"></a>Denzin, N. K. (1978) The research act: a theoretical introduction to sociological methods. 2nd ed. New York: McGraw Hill</li>
<li><a name="ferg"></a>Ferguson, S. (1992) Strategic planning for national libraries in developing countries: an optimists view. <em>IFLA Journal</em>, 18 (4), 339-354.</li>
<li><a name="fina"></a>Financial Times 16th December 1966.</li>
<li><a name="flor"></a>Flores, F. (1972) The applicability of American management practices to developing countries: a case study of the Philippines. <em>Management International Review</em>, 12 (1), 83-89.</li>
<li><a name="ford"></a>Ford, G. (1977) <em>User studies: an introductory guide and select bibliography. (CRUS. Occasional Paper 1).</em> Sheffield: University of Sheffield.</li>
<li><a name="genu"></a>Genus, A. (1995) <em>Flexible strategic management</em>. London: Chapman and Hall.</li>
<li><a name="glas"></a>Glaser, B. and Strauss, A. L. (1967). <em>The discovery of grounded theory: strategies for qualitative research</em>. New York: Aldine.</li>
<li><a name="gran"></a>Grant, R.M. (1991). The resource-based theory of competitive advantage: implications for strategy formulation. <em>Management Review</em>, 33 (3), 114-22.</li>
<li><a name="guba"></a>Guba, E. (1992) <em>The paradigm dialog</em>. Newbury Park: Sage.</li>
<li><a name="hame"></a>Hamel, J., Dufuor, S. and Fortin, D. (1993) <em>Case study methods</em>. Newbury Park, Cal.: Sage.</li>
<li><a name="hirs"></a>Hirschheim. R. (1985) Information systems epistemology: an historical perspective. In: Munford, E., Hirschheim, R., Fitzgerald, G. and Wood-Harper, T. (Eds) <em>Research Methods in Colloquium.</em> Manchester Business School 1-3 September, 1994.. Amsterdam: North Holland. 13-36</li>
<li><a name="huq"></a>Huq, M.M. (1989) <em>The economy of Ghana for 25 years since independence</em>. London: Macmillan Press.</li>
<li><a name="kiss"></a>Kissiedu, C.O. (1994) <em>The realities of academic librarianship in Ghana with particular reference to the University of Ghana.</em> Seminar Paper. Penn State University, Pennsylvania 23rd March-11th April 1994.</li>
<li><a name="lanc"></a>Lancy, D.T. (1993) <em>Qualitative research in education: an introduction to the major traditions.</em> New York: Longman.</li>
<li><a name="mint73"></a>Mintzberg, H. (1973) Strategy making in three modes. <em>California Management Review</em>, Winter, 44-53.</li>
<li><a name="mint90"></a>Mintzberg, H. (1990) Does decision get in the way_? Organization Studies_, 11(1), 1-5.</li>
<li><a name="osha"></a>O'Shaughnessy, N.J. (1985) Strategy and US cultural bias. <em>European Journal of Marketing</em>, 19(4), 23-32.</li>
<li><a name="patt80"></a>Patton, M.Q. (1980) <em>Qualitative research methods</em>. Beverly Hills: Sage.</li>
<li><a name="patt88"></a>Patton, M. Q. (1988) Paradigms and pragmatism. In: M. Fetherman, D.M. (Ed). <em>Qualitative approaches to evaluation in education</em>. New York: Praeger, 116-137.</li>
<li><a name="pete"></a>Peters, T.J. and Waterman, R.H. (1982) <em>In search of excellence</em>. London: Harper and Row.</li>
<li><a name="port80"></a>Porter, M.E. (1980) <em>Competitive strategy: techniques for analysing industries and competitors</em>. New York: Free Press.</li>
<li><a name="port85"></a>Porter, M.E. (1985) <em>Competitive advantage: creating and sustaining superior performance</em>. New York: Free Press.</li>
<li><a name="reic"></a>Reichardt, C.S. and Cook, T. D. (1979) Beyond qualitative versus quantitative methods. In: Cook, T.D. and Reichardt, C.S. (Eds). <em>Qualitative and quantitative methods in evaluation research.</em> Beverly Hills: Sage, 7-32.</li>
<li><a name="ross"></a>Rossman, G.B. and Wilson, B.C. (1985) Numbers and words: combining qualitative and quantitative methods in a single large-scale evaluation study. <em>Evaluation Review</em>, 9(5), 627-643.</li>
<li><a name="smit"></a>Smith, C.(1989) The case study: a vital yet misunderstood research method for management. In: Mansfield, R. (Ed.) <em>Frontiers of management</em> London: Routledge.</li>
<li><a name="thom"></a>Thompson, J.J. (1990) <em>Strategic management: awareness and change</em>. London: Chapman and Hall.</li>
<li><a name="vinc"></a>Vincent, I. (1988) Strategic planning and libraries: does the model fit? <em>Journal of Library</em> <em>Administration</em>, 9 March, 35-47.</li>
<li><a name="wals"></a>Walsham, G. (1993) <em>Interpreting information systems in organisations</em>. Chichester: John Wiley and Son.</li>
<li><a name="whee"></a>Wheelan, T.L. and J. David Hunger (1990) Strategic management. 3rd ed. Reading, Mass: Addison-Wesley.</li>
<li><a name="whit"></a>Whittington, R. (1993) <em>What is strategy: does it matter?</em> London: Routledge.</li>
<li><a name="wilk"></a>Wilks, S. (1990) The embodiment of industrial culture in bureaucracy and management. In: Clegg, S. and Reddings, S.G.(Eds). <em>Capitalism in contrasting cultures.</em> Berlin: De Gruyter.</li>
<li><a name="yava"></a>Yavas, V., Kaynak, E. and Dibe, M. (1985) The managerial climate in less developed countries. <em>Management Decisions</em>, 23 (3), 29-40.</li>
<li><a name="yin89"></a>Yin, R. K. (1989) <em>Case study research: design and methods</em>. Newbury Park: Sage.</li>
<li><a name="yin94"></a>Yin, R. K. (1994). <em>Case study research: design and methods</em>. 2nd ed. - Newbury Park: Sage.</li>
</ul>

</body>
</html>
