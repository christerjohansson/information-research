#### vol. 20 no. 4, December, 2015

# Repurposing Facebook for documenting personal history: how do people develop a secondary system use?

#### [Sue Yeon Syn](#author)  
The Catholic University of America, 620 Michigan Ave., N.E., Washington, DC 20064  
[Donghee Sinn](#author)  
University at Albany (State University of New York), 135 Western Ave., Albany, NY 12222

#### Abstract

> **Introduction.** This study investigated a type of post-adoptive system use behaviour that is derived from the original purpose of the system. We defined this type of post-adoptive use as a _secondary system use_ as it is different from the primary use of the system for which the system was intended. We focused on the way that Facebook can be used to document personal lives, in addition to using it as a social networking tool, as one example of secondary system use. The study tested the factors that influence a _secondary system use_.  
> **Method.** An online survey was distributed through the Amazon Mechanical Turk to collect responses from Facebook users.  
> **Analysis.** We created a research model, expanded from the technology acceptance model, to examine how users perceive usefulness of a secondary use and then use Facebook for a secondary purpose. The model was tested with partial least squares and path analysis.  
> **Results.** The research model provides a baseline for understanding the formation of a _secondary system use_ within information systems. The findings show that users develop a _secondary system use_ in the post-adoptive stage of system exploitation. This behaviour is largely based on their primary use experience and the perception of its usefulness for both primary and secondary use.  
> **Conclusions.** This study emphasises and explains the importance of _secondary system use_ in the development of information technology. By learning more about users' post-adoptive use behaviour, system developers and information professionals can understand changes in system use and identify directions for future development of systems.

## Introduction

Technology acceptance has been actively investigated in both the information science and behaviour science fields. Researchers have made efforts to understand technology adoption and use behaviour over the history of technology. They have applied various theories and models to this investigation including the study of innovation diffusion theory ([Rogers, 1995](#rog95)), the technology acceptance model ([Davis, Bagozzi and Warshaw, 1989](#dav89b)), the theory of reasoned action ([Fishbein and Ajzen, 1975](#fis75)) and the theory of planned behaviour ([Ajzen, 1991](#ajz91)). As information systems evolve, they include various features and functions that allow users to repurpose them. For example, an email system is not only used to send and receive messages, but it can also be used as an ad-hoc reminder system or as a self-archiving tool by sending messages or attachments of important files to users' own email accounts ([Marshall 2008](#mar08); [Sinn, Syn and Kim, 2011](#sin11)). In the same way, blogs are considered a convenient tool for saving web content in various formats in addition to keeping and publishing online journals ([Wu and Theng, 2005](#wu05)). Moreover, it has been observed that as users gain experience of a system through use ([Bhattacherjee and Premkumar, 2004](#bha04); [Davis and Venkatesh, 2004](#dav04)) and perceptions, as well as their attitudes about the system ([Lampe, Ellison and Steinfield, 2008](#lam08)), change accordingly. People use information systems in more and more comprehensive and sophisticated ways as their experiences accumulate ([Deng and Chi, 2012](#den12); [Hsieh and Wang, 2007](#hsi07)).

While earlier studies of technology acceptance has focused on information system development and initial adoption stages, more recent research has investigated post-adoptive use of technology. A successful adoption of new technology in an organization has been observed as being highly influenced by post-adoptive use behaviour ([Bhattacherjee, 2001](#bha01); [Chin and Marcolin, 2001](#chi01)). This is because post-adoptive use facilitates increased confidence and supports uptake of the full potential of the system ([Jasperson, Carter and Zmud, 2005](#jas05); [Hsieh and Wang, 2007](#hsi07)). At the post-adoption stage, users not only change their attitudes about an information system, but they develop different interaction patterns with the system because of their changed perceptions and needs ([Saeed and Abdinnour, 2008](#sae08); [2013](#sae13)).

Post-adoption of information systems has been studied under various conditions such as context, activeness, time and experience. In this study, we particularly examined post-adoptive use behaviour that is driven by a discovery of potential secondary uses for existing functions in an information system. We will refer to this type of post-adoptive use behaviour as a _secondary system use_. DeSanctis and Poole ([1994](#des94)) asserted that the adoption of advanced technologies is more related to how people use them than to what functions the technologies provide. Therefore, users may exhibit use patterns that are different from those that were expected by the uptake of pre-existing functions.

Through the examination of a _secondary system use_, the current study provides valuable insight into how users advance their use behaviour as they continue to use and become skilled with an information system. We chose Facebook to examine secondary system use for the study. Although Facebook indicates, in its own words, that its original purpose is to provide easy and fun methods “for people to stay connected with friends and family” ([Facebook Stats, n.d.](#fac-a)), some studies have found that users also use it for other purposes. Such additional uses fall into examples of secondary system uses. For this study, we focused on the secondary system use of Facebook for managing personal documentation. We use the term personal documentation in a broad sense to include any information pattern that archives personal history. This includes activities for recording individuals’ life stories in any format whether or not there is an intention to share and/or use it in the future.

Social network sites have been deeply integrated into the everyday activities of contemporary Internet users and have changed the ways in which people communicate with friends and family. When using social networking sites, people usually share personal matters and daily happenings. In this way, social network sites such as Facebook has become a place for the digital traces of users’ personal information and life stories to be mapped. This is the case even though users may not perform activities in those sites that are mutually exclusive, that is, conducting activities for social networking which in result become activities for personal documentation. In this study, we pay attention to whether social networking and personal documentation are perceived differently by the users in respect to the development of secondary system use in post-adoption stage.

A previous study found that Facebook members sometimes use the service without any clear intention of communicating with others. Such users create posts as a personal repository to remind themselves about a certain events or plans, to retrieve information later, or to manage their schedules ([Sinn and Syn, 2014](#sin14)). This leads to speculation that users have become aware of the usefulness of using Facebook for personal documentation. Consequently, questions can be raised about how secondary system use has been developed and how it is related to the primary use of Facebook. Perceptions of the worth of these secondary uses are also of interest. This study investigated how Facebook users perceived and developed a post-adoptive use of the system, and how this perception and consequent use had been influenced by the way it had been used for its primary purpose.

In order to understand Facebook use within this context, we created a research model based on the technology acceptance model and related studies. The technology acceptance model has been applied mainly to software in an organizational context. Through our research model, we applied the model to a social network system which had not yet been examined fully in respect to user acceptance. In addition, this study examined the association of factors surrounding the development of a secondary use, such as perception about and use of Facebook for personal documentation.

In order to do this we developed the following research questions:

> RQ1: What is the relationship between the primary and the secondary system uses?,  
> RQ2: How do users develop the secondary system use from its primary use?, and  
> RQ3: How do the current users’ interactions impact their post-adoptive system use?

The hypotheses to address these research questions are presented in the following section.

Moreover, we used this research model to examine a specific post-adoptive use behaviour of Facebook, namely, personal documentation activities. Social network data have been of great interest to researchers in many fields because such data could provide a wealth of information about digital traces that reveal users identities and life stories that help to researchers to understand contemporary life styles and socialisation. We present some insights from this perspective on the use of many other social networking services where we may find historical data of users regardless of whether users know of the existence of these data or not.

## Background

### Post-adoptive use behaviour

With the increase in sophisticated information systems that serve multiple functions, people are becoming more and more creative in making use of available features of systems for their own purposes. People easily discover ways of using systems to meet their particular needs as they become familiar with these systems in the post-adoption stage. In research studies, the post-adoptive context has been examined from various perspectives, although the potential of post-adoptive interventions has been somewhat overlooked ([Jasperson, Carter and Zmud, 2005](#jas05)). Hsieh and Wang ([2007](#hsi07)) stated that a great deal of technology acceptance research has focused on simple adoption decisions or the amount of use. They argued that more research is needed to understand advanced uses in a post-adoptive context, especially for complex information systems. Understanding use patterns in a post-adoptive context is important because the eventual success of information systems depends on the post-adoptive use rather than the initial acceptance of information systems ([Bhattacherjee, 2001](#bha01)).

In understanding the post-adoptive use of information systems, researchers have applied behavioural theories, and proposed models, to explain the changes in technology use. Many researchers note that in the post-adoption stage, users engage in reflective uses of information systems and begin _technology sensemaking_ that confirm or disconfirm the cognition of prior use experiences ([Jasperson, Carter and Zmud, 2005](#jas05)). That is, even after users have settled on an initial attitude and perception toward a system, their information system use can be influenced by changing views of the system. Such awareness has been applied to several theories. For example, Fulk ([1993](#ful93)) emphasised social influences on individuals’ attitudes and behaviour toward technology in her _social construction theory_.

In their _adaptive structuration theory_, DeSanctis and Poole ([1994](#des94)) tried to explain the nature of social structures within advanced information technologies and key interaction processes when using the technologies. Both theories stress the social influences that impact on technology use rather than the worth of the technology itself to its adoption.

On the other hand, the _structurational model of technology_ focuses on the human interactions with the system. It is based on two key aspects of technology: duality of technology and its interpretive flexibility ([Orlikowski, 1992](#orl92)). The duality of technology reflects two dimensions of technology use, in that it is created and changed by human actions while it is also used by humans to accomplish certain actions. Interpretive flexibility signifies that an individual’s interpretations about the functions of technology and understanding levels of those functions further influence the use patterns of technology ([Orlikowski, 2000](#orl00)). The structurational model is useful to describe the interactions between an information system and users because it explains how post-adoptive perceptions and behaviour are formulated through those interactions ([Orlikowski, 1992](#orl92); [2000](#orl00)).

Another stream of theoretical discussion related to post-adoption deals with the changing behaviour that occurs in different stages of technology acceptance and the different levels of expertise in technology use. Researchers have recognised that use changes over time and have described the evolving stages according to the levels of exploration of and interactions with the information system. This is dependent upon how and when an information system becomes part of routine activities.

As a result, researchers have examined post-adoptive use behaviour by using different definitions for various conditions. Bhattacherjee ([2001](#bha01)) used the term, _continued use_ or _continuance_, to indicate users’ intention to continue to use a technology after experiencing it or initially adopting it. Hsieh and Wang ([2007](#hsi07)), based on Saga and Zmud’s ([1994](#sag94)) technology acceptance and infusion, described _extended use_ as taking advantage of available features in a technology to improve an individual’s performance of routine tasks as well as more comprehensive tasks. Their study suggested that _extended use_ helps users utilise available functions of an information system in a more sophisticated fashion.

Wang, Li and Hsieh ([2013](#wan13)) argued that perceived usefulness and satisfaction are the antecedents of innovative use of information systems, that is, users apply the system in novel ways to support their task performance, and in that self-efficacy of the system, users positively associate the effect of perceived usefulness. On the one hand, Zmud proposed a concept called _deep usage_, which is defined as the extent to which users fully use information systems to enhance their productivity ([Chin and Marcolin, 2001](#chi01)).

On the other hand, new uses of an information system that users develop from exploring the system was studied by Saeed and Abdinnour ([2008](#sae08)) and referred to as _exploratory usage_. _Improvisation_ is a metaphor from jazz music that indicates a form of intuition which guides actions in a spontaneous way, or the creative process in which individuals engage in the attempt to find a new solution from the recombination of available resources in a short time frame ([Magni, Provera and Proserpio, 2010](#mag10)).

After reviewing the types of post-adoptive uses defined by previous studies, we define a _secondary systems use_ as a type of post-adoptive use that was derived while using the system for the primary and originally intended purposes.

The comparisons between pre- and post-adoption stages and surveys of various stages in post-adoption are useful to understand changes of use attitudes and behaviour. Some researchers have argued that users’ post-adoptive use behaviour varies according to the extent to which they explore existing or new functions of an information system. Karahanna, Straub and Chervany ([1999](#kar99)) compared pre- and post-adoption attitudes and behaviour based on the innovation diffusion theory and the theory of reasoned action. Their findings show that there are different factors within pre- and post-adoptive stages. In addition, Khalifa and Liu ([2003](#kha03)) asserted that there is no significant relationship in users’ satisfaction between the post-adoption stage and the initial-adoption stage since users’ goals and expectations of the functionality of technology change over time.

In sum, a great deal of research has incorporated theories and models to explain post-adoptive use behaviour. These theories and models critique a number of elements of behaviour. They effectively describe the ways that

1.  social factors influence the formation and change of technology adoption and use;
2.  patterns of post-adoptive use vary depending on different stages of adoption, the length of time in use, and level of use expertise;
3.  the factors that influence use in pre- and post-adoption are different; and
4.  users’ expectations and use patterns of information systems differ in the post-adoption stage from the pre-adoption stage.

However, there has been little investigation that seeks to understand the development of post-adoptive uses for a secondary purpose that has evolved from primary system use. There is also little research about secondary system use of a social network site.

### Post-adoptive use of Facebook

While Facebook’s major functions are designed for social networking, users also expand additional use patterns. A previous study identified that social networking activities in Facebook, i.e., communicating with friends and sharing information about life events, result in leaving life traces in the digital environment whether or not users are aware of it ([Sinn and Syn, 2014](#sin14)). Such post-adoptive uses appear while using the functions that Facebook provides to support its primary purposes. Zhao, Grasmuck and Martin ([2008](#zha08)) argued that since Facebook is not an anonymous service, the content that users create including profile and wall posts, are very similar to the identity of individual users.

The information posted on Facebook, such as photos and affiliations, has become a kind of new identity maker with which people show themselves ([Lenhart and Madden, 2007](#len07); [Pempek, Yermolayeva and Calvert, 2009](#pem09)). In this sense, the content users create on Facebook can be real life information and very close to how they (and they want others to) affirm themselves. Lampe, Ellison and Steinfield ([2008](#lam08)) found that Facebook users show changes in use patterns and perceptions about Facebook over time. Their study demonstrated that after users become familiar with Facebook features, the use pattern stabilises as it is unnecessary to explore the site to learn how Facebook works. They noted that users are more satisfied with the system as they learn more about Facebook features for networking.

Some studies have examined secondary uses of Facebook. Wong ([2012](#won12)) investigated how personal use of Facebook is associated with using it for business purposes. He showed a direct association between these two use patterns, that is, those who actively use Facebook for personal purposes tend to use it vigorously for business purposes. The results from Wong ([2012](#won12)) presented evidence that active use for primary purposes would influence post-adoptive use of the systems. Similarly, Mazman and Usluel ([2010](#maz10)) observed Facebook use for educational purposes. Their study showed that utilizing Facebook in daily routine activities is highly related to methods of utilisation for education. They also found that when people are developing their secondary system use, the initial perceptions, such as individuals’ perceived ease of use, serve as important elements in developing a secondary system use. Once the post-adoptive use is realised, such awareness now shapes the post-adoptive use pattern so that it will settle into a routine.

## Research model for a secondary use in post-adoption

In order to understand the development of post-adoptive use patterns from initial perceptions and use, we propose a research model that is extended from the technology acceptance model. This extension of technology acceptance model was designed by consulting existing studies regarding post-adoptive use of information systems.

### Technology acceptance model

In the field of information science, the technology acceptance model ([Davis, Bagozzi and Warshaw, 1989](#dav89b)) is considered the most influential model in understanding what affects users’ acceptance and use of technology ([Lee, Kozer and Larsen, 2003](#lee03)). This model explains how users’ perceptions about a technology influence their intentions to use the technology which then leads to an actual use. The technology acceptance model has its foundation in social psychology theories, such as the theory of reasoned action ([Fishbein and Ajzen, 1975](#fis75)). The theory of reasoned action elucidates that a person’s specific behaviour is determined by his or her behavioural intention. This intention is jointly determined by the person’s attitude and subjective norms concerning the behaviour in question.

The technology acceptance model is extricated from the theory of reasoned action. It can be used to model how person’s behavioural intention to use a technology is determined by his or her attitudes toward using the system, and how these attitudes are the result of perceived usefulness and perceived ease of use (Figure 1). The model provides a useful framework with which to explain the development of system use behaviour and has evolved over time ([Lee, Kozer and Larsen, 2003](#lee03)). Davis, Bagozzi and Warshaw ([1989](#dav89b)) recommended dropping the factors of attitude toward use and behavioural intention to use from the model when applying the technology acceptance model for a post hoc analysis. Instead they suggest focusing on system use, perceived usefulness, and perceived ease of use. This study adopts their recommendation as we examined post-adoptive use.

<figure class="centre">![Figure 1: Technology acceptance model](p698fig1.png)

<figcaption>Figure 1: Technology acceptance model</figcaption>

</figure>

Among researchers who have applied the technology acceptance model framework to look at the acceptance of an information system and use behaviour over time, Davis and Venkatesh ([2004](#dav04)) focused on how users form initial use behaviour and how the initial use behaviour becomes settled with continuous and sustained use of the system. The results of the study demonstrated that early reaction to, and active use of, the system have a significant impact on users’ later system uses. The findings revealed that active use and familiarity with the system can lead users to further expand the use of system at post-adoptive stages. Applying the technology acceptance model and expectation-confirmation theory, Bhattacherjee and Premkumar ([2004](#bha04)) demonstrated that users’ perceptions and attitudes toward an information system can be changed after acquiring initial first-hand experience. They argued that perceptions about an information system are dependent on disconfirmation of initial expectation and the satisfaction they gained from the first experience and such perceptions can affect the consequent use. Studies like Davis and Venkatesh’s ([2004](#dav04)) and Bhattacherjee and Premkumar’s ([2004](#bha04)) provided evidence that changed perceptions and attitudes influence users’ post-adoptive use of an information system. These studies also showed that the technology acceptance model effectively provides a basic framework to analyse post-adoptive use and surrounding factors.

The technology acceptance model has been widely applied by many researchers in various domains and for varying purposes ([Lee, Kozer and Larsen, 2003](#lee03); [Legris, Ingham and Collerette, 2003](#leg03)). However, only a few studies have applied the technology acceptance model to understanding attitudes and behaviour of social networking system users. Pinho and Soares ([2011](#pin11)) used the technology acceptance model to identify what influences the adoption of social networking systems and confirmed the explanatory power of the technology acceptance model in the adoption of social networking systems. Although the technology acceptance model was useful in explaining system use in a social networking setting, their study did not consider the evolution of the model and the unique characteristics of social networking systems.

### Research model and hypotheses

Applied at various acceptance levels and in various settings, the technology acceptance model has been widely modified and extended to improve the ways it explains factors that influence technology adoption. For instance, Venkatesh, Morris, Davis and Davis ([2003](#ven03)) proposed the unified theory of acceptance and use of technology to embrace all significant determinants of intention to use or use of technology suggested from various models of the technology acceptance model. Similarly, in designing our research model, we reviewed various determinants suggested for post-adoptive uses in previous studies to identify if the model could be applied to social networking systems. The secondary system use activities in the post-adoption stage have not been identified or tested in the previous studies to our knowledge, as those studies mostly focused on intensive use patterns of primary system use. In addition, even when secondary system use was discussed in social networking systems, the uniqueness of the characteristics and design of social networking sites has not been considered. Thus, we proposed a research model with determinants that were adopted, taking the characteristics of social networking systems into account based on discussions and findings from previous studies.

Figure 2 presents the research model that includes the above-mentioned determinants and the relationships among them. In applying the research model to the case of Facebook, the factors for the primary system use, which are _perceived usefulness for primary system use_, _perceived ease of use_, and _primary system use_, indicate factors related to the primary use of Facebook, namely, social networking. Hypothesis 1 addresses the primary use of Facebook and related issues. Using Facebook for personal documentation is a secondary use of Facebook. The relationships among the _primary system use_, _perceived usefulness for secondary system use_, and the _secondary system use_ in the research model represent the factors that influence the secondary system use in the post-adoption stage of Facebook. Hypotheses 2 and 3 address the expansion of Facebook use for personal documentation that is derived from social networking activities. Hypothesis 4 addresses the influences of Facebook interaction activeness in relation to the application of our research model in understanding the secondary system use. The details of how these determinants were included in the research model along with the hypotheses are explained below.

<figure class="centre">![Figure 2: Research model](p698fig2.png)

<figcaption>Figure 2: Research model</figcaption>

</figure>

First, the research model included the _primary system use_ and its determinants, that is, _perceived ease of use_ and _perceived usefulness for primary system use_. Following the recommendation by Davis, Bagozzi and Warshaw ([1989](#dav89b)), we excluded intention to use the system as we were examining use in the post-adoption stage; in this stage, users’ intention to use the system was proved because they were already using it. Therefore, we excluded _attitude toward using_ and _behavioural intention to use_ from the technology acceptance model and only included _perceived usefulness for primary system use_, _perceived ease of use_, and _primary system use_. _Perceived usefulness for primary system use_ and _perceived ease of use_ were particularly included in our research model as they were discussed as significant determinants from previous studies (e.g., [Hsieh and Wang, 2007](#hsi07); [Venkatesh, Morris, Davis and Davis, 2003](#ven03)). This part of the model was designed to focus on the primary system use for which the system was designed, which in our case was using Facebook for social networking purposes. This part of the model was expected to provide verification that when users found Facebook useful and easy to use for social networking, they would actually use it for that purpose. Based on the model, the following hypotheses were developed.

> H1a. The _perceived ease of use_ will be positively associated with the _perceived usefulness for primary system use_.  
> H1b. The _perceived usefulness for primary system use_ will be positively associated with the _primary system use_.  
> H1c. The _perceived ease of use_ will be positively associated with the _primary system use_.

Secondly, the determinants related to secondary system use in post-adoption were considered. It has been found that usefulness has more influence than ease-of-use in post-adoption ([Karahanna, Straub and Chervany, 1999](#kar99)). According to the technology sensemaking theory ([Jasperson, Carter and Zmud, 2005](#jas05)) and the structurational model for improvised technology use ([Orlikowski, 1992](#orl92); [2000](#orl00)), when users are engaged in exploring information systems, they actively formulate new perceptions about information systems based on their experiences. Thus, we included _perceived usefulness for secondary system use_ as a new determinant of post-adoptive use focusing on usefulness rather than ease-of-use, and propose that there is an association between the _primary system use_ and the emergence of perception for a secondary system use. In the case of this study, it was the perceived usefulness of Facebook for the purpose of personal documentation. This part of the model was expected to show the process of how users develop their perception on a new purpose from the use of Facebook for its primary purpose. As users continue to utilise Facebook for social networking activities, they may explore usefulness for secondary purposes, and as a result, the perceptions on the usefulness of Facebook for a secondary purpose can be developed.

> H2\. The _primary system use_ experience will be positively associated with users’ development of _perceived usefulness of secondary system use_.

Third, determinants that directly influence _secondary system use_ were considered. Bhattacherjee ([2001](#bha01)) reported that perceived usefulness for continued use is a major determinant of post-adoption usage. Accordingly, we considered that the emergence of perception about secondary system use (_perceived usefulness for secondary system use_) would become an impetus for new ways of using the information system. This led us to consider the _perceived usefulness for secondary system use_ as a determinant for the secondary system use along with its relationship with _primary system use_. Moreover, _primary system use_ could be a direct determinant of the _secondary system use_ as users’ existing use behaviour can influence the formation of secondary system use ([Bhattacherjee and Premkumar, 2004](#bha04); [Davis and Venkatesh, 2004](#dav04)). Thus, we decided to associate the _perceived usefulness for secondary system use_ and the _primary system use_ respectively, to the _secondary system use_. This part of the model would demonstrate whether Facebook users were likely to develop a secondary use of the service for personal documentation from their primary use of social networking. This was based on either when they found the service useful for personal documentation or when they were active in using the service for social networking.

> H3a. The _perceived usefulness for secondary system use_ will be positively associated with the _secondary system use_.  
> H3b. The _primary system use_ will be positively associated with the _secondary system use_.

Some potential determinants such as _social influence_ and _satisfaction_, which were examined in previous studies as significant determinants of technology acceptance or post-adoptive use, were not included in our research model. Some researchers argue that _social influence_ should be emphasised in the advanced information technology context ([DeSanctis and Poole, 1994](#des94); [Fulk, 1993](#ful93); [Orlikowski, 1992](#orl92)). However, others found that _social influence_ was less significant for use in the post-adoption stage ([Karahanna, Straub and Chervany, 1999](#kar99)). In addition, Venkatesh, Morris, Davis and Davis ([2003](#ven03)) discussed that _social influence_ becomes significant only when the use is mandated, mostly in an organizational setting.

In the case of social networking systems, although individuals sometimes choose to use a system due to pressure by their friends, peers, or family, the decision on adoption is usually made voluntarily and personally. There may be different kinds and dynamics of social contexts influencing the use of social networking services, but the _social influence_ in an organizational setting as defined in previous research was not considered as a determinant of the model in this study.

The _satisfaction_ factor is included in some post-adoptive models such as the post-acceptance model of IS continuance ([Bhattacherjee, 2001](#bha01); [Hsieh and Wang, 2007](#hsi07)), based on the expectation-confirmation theory. _Satisfaction_ influences the advent of new perceptions due to users’ experiences with the system, and therefore, is considered to have potential impact on users’ continued use ([Bhattacherjee, 2001](#bha01); [Rogers, 1995](#rog95)). However, studies indicated that there is no significant relationship between post-adoption satisfaction and initial adoption satisfaction ([Khalifa and Liu, 2003](#kha03)) and the influence of satisfaction on post-adoptive use is found to be low ([Hsieh and Wang, 2007](#hsi07)). Based on these findings, we did not include _social influence_ and _satisfaction_ as determinants in our research model.

Finally, in addition to the determinants considered in the research model, this study examined the influence of the level of active interactions with a system as a potential moderator in the formation of _secondary system uses_. The goal was to incorporate what has been discussed in theories concerning the routine use of technology and the extent of interactions in the post-adoptive stage. Saga and Zmud ([1994](#sag94)) and Cooper and Zmud ([1990](#coo90)) noted that once users accept an information system and get familiar with its intended functions, the use of information systems becomes routine, which stabilises the post-adoption. Our previous study had also found that posting frequency and login duration affect Facebook users’ self-presentation and personal documentation activities to some extent ([Sinn and Syn, 2014](#sin14)). Thus, we examine how the activeness of interaction with Facebook would influence the application of our research model.

> H4\. The model relationships will be moderated by the extent of users’ interactions with the system.

## Method

### Data collection

We conducted an online survey to collect data. As a recruiting method, we used Amazon Mechanical Turk, a crowd-sourcing market place. We decided to use Amazon Mechanical Turk in order to have better access to a wide population who use Facebook rather than recruiting through a convenient sample. The Amazon Mechanical Turk is an automated system to display the surveys and invites participants. Registered potential participants can view a list of posted surveys and chose ones for which they are qualified to participate. To assure the quality of responses, we listed several criteria for research participants: they should be current Facebook users who are 18 years old or older, and reliable participants in the Amazon Mechanical Turk (i.e., 95% or higher acceptance rate for their previous participations). Survey requesters can reject non-qualifying responses, and based on the acceptance of their survey responses by survey requesters, the registered potential participants are assigned with the acceptance rate that indicates the reliability of their responses. The high acceptance rate means that previous responses of those participants have been mostly accepted.

The survey was distributed between February 19 and February 21, 2013\. In order to control the quality of data, we filtered responses based on satisfaction with participants’ eligibility criteria and the level of completeness. To confirm participants’ voluntary use of Facebook, we only included responses that indicated using Facebook for personal reasons. Through this process, a total of 255 out of 310 responses were selected for our analysis, with 51 percent of the respondents being male and 49 per cent female. The age of the participants ranged from 18 to 65, with the majority of the participants under the age of 35 (81.89%). The participants were steady and active users who had been Facebook members for several years (79.22% were members for 3-7 years), accessed Facebook on a daily basis (73.7%), posted to their timelines on a weekly basis (45.9%), and had approximately 100-500 Facebook friends (56.29%).

The majority of the participants had some level of college or higher education (92.5%) and an income level of $20,001-$60,000 (47.8%). The sample population appeared to represent the population of United States Facebook users, based on the data reported from the Pew Research Center ([Duggan, Ellison, Lampe, Lenhart and Madden, 2015](#dug15)). Our sample reflects the Pew data, including a high percentage of Internet users, both male and female, use Facebook; a higher percentage of internet users are using Facebook in the age range of 18-49; numbers of friends range high in 1-100 and 251-500; and U.S. Facebook users report that they do some types of activities on Facebook daily or weekly. Details of the participants’ demographics and descriptive statistics are provided in Table 1 and the levels of experience and use of Facebook are presented in Table 2.

<table class="center"><caption>  
Table 1: Demographics and descriptive statistics of the survey participants  
</caption>

<tbody>

<tr>

<th colspan="2">Items</th>

<th>Frequency</th>

<th>Percentage (%)</th>

</tr>

<tr>

<td rowspan="2">Sex</td>

<td>Male</td>

<td style="text-align:center">129</td>

<td style="text-align:center">51.0</td>

</tr>

<tr>

<td>Female</td>

<td style="text-align:center">124</td>

<td style="text-align:center">49.0</td>

</tr>

<tr>

<td rowspan="5">Age<sup>*</sup></td>

<td>18-22</td>

<td style="text-align:center">68</td>

<td style="text-align:center">26.77</td>

</tr>

<tr>

<td>23-35</td>

<td style="text-align:center">140</td>

<td style="text-align:center">55.12</td>

</tr>

<tr>

<td>36-49</td>

<td style="text-align:center">38</td>

<td style="text-align:center">14.96</td>

</tr>

<tr>

<td>50-65</td>

<td style="text-align:center">8</td>

<td style="text-align:center">3.15</td>

</tr>

<tr>

<td>65+</td>

<td style="text-align:center">0</td>

<td style="text-align:center">0</td>

</tr>

<tr>

<td rowspan="4">Education level</td>

<td>High school</td>

<td style="text-align:center">18</td>

<td style="text-align:center">7.1</td>

</tr>

<tr>

<td>Some college, community college, or vocational school</td>

<td style="text-align:center">109</td>

<td style="text-align:center">42.9</td>

</tr>

<tr>

<td>College</td>

<td style="text-align:center">110</td>

<td style="text-align:center">43.3</td>

</tr>

<tr>

<td>Graduate school</td>

<td style="text-align:center">17</td>

<td style="text-align:center">6.7</td>

</tr>

<tr>

<td rowspan="6">Income level</td>

<td>= $20,000</td>

<td style="text-align:center">46</td>

<td style="text-align:center">18.1</td>

</tr>

<tr>

<td>$20,001-$40,000</td>

<td style="text-align:center">63</td>

<td style="text-align:center">24.8</td>

</tr>

<tr>

<td>$40,001-$60,000</td>

<td style="text-align:center">59</td>

<td style="text-align:center">23.2</td>

</tr>

<tr>

<td>$60,001-$80,000</td>

<td style="text-align:center">32</td>

<td style="text-align:center">12.6</td>

</tr>

<tr>

<td>$80,001-$99,999</td>

<td style="text-align:center">27</td>

<td style="text-align:center">10.6</td>

</tr>

<tr>

<td>= $100,000</td>

<td style="text-align:center">27</td>

<td style="text-align:center">10.6</td>

</tr>

<tr>

<td colspan="4"><sup>*</sup> The age distribution followed the categories defined from the 2011 report of Pew Research Center for comparison ([Hampton, Goulet, Rainie and Purcell. 2011](#ham11)).</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 2: Participants’ levels of experience and use of Facebook  
</caption>

<tbody>

<tr>

<th>Items</th>

<th>N</th>

<th>Min.</th>

<th>Max.</th>

<th>Mean</th>

<th>St. Dev.</th>

</tr>

<tr>

<td>Facebook is part of my daily routine.<sup>*</sup></td>

<td style="text-align:center">255</td>

<td style="text-align:center">1</td>

<td style="text-align:center">5</td>

<td style="text-align:center">3.85</td>

<td style="text-align:center">1.04</td>

</tr>

<tr>

<td>I spend a lot of time on Facebook.<sup>*</sup></td>

<td style="text-align:center">255</td>

<td style="text-align:center">1</td>

<td style="text-align:center">5</td>

<td style="text-align:center">3.11</td>

<td style="text-align:center">1.18</td>

</tr>

<tr>

<td>Frequency of Facebook access.<sup>**</sup></td>

<td style="text-align:center">253</td>

<td style="text-align:center">1</td>

<td style="text-align:center">7</td>

<td style="text-align:center">5.84</td>

<td style="text-align:center">1.50</td>

</tr>

<tr>

<td>Frequency of Facebook posting.<sup>**</sup></td>

<td style="text-align:center">255</td>

<td style="text-align:center">1</td>

<td style="text-align:center">7</td>

<td style="text-align:center">3.33</td>

<td style="text-align:center">1.72</td>

</tr>

<tr>

<td>Years of membership</td>

<td style="text-align:center">251</td>

<td style="text-align:center">1</td>

<td style="text-align:center">9</td>

<td style="text-align:center">5.25</td>

<td style="text-align:center">1.85</td>

</tr>

<tr>

<td>Number of friends</td>

<td style="text-align:center">254</td>

<td style="text-align:center">4</td>

<td style="text-align:center">10,025</td>

<td style="text-align:center">335.28</td>

<td style="text-align:center">674.24</td>

</tr>

<tr>

<td colspan="6"><sup>*</sup> Measured in 5-point Likert scale where 1:strongly disagree, 2:disagree, 3:neither, 4:agree, and 5:strongly agree  
<sup>**</sup> Measured in 7 choices where 1:seldom, 2:occasionally, 3:every few weeks, 4:1-2 days a week, 5:3-5 days a week, 6:about once a day, and 7:several times a day</td>

</tr>

</tbody>

</table>

### Measurements

As the technology acceptance model is well-established, its valid measurement inventory is available for research. The measurements of _perceived usefulness for primary system use_, _perceived ease of use_, and _primary system use_ were checked and modified from those of Davis ([1989](#dav89a)). The measurements of _primary system use_ that are specific to social networking systems were adopted from Kwon and Wen ([2010](#kwo10)), especially for the primary use of Facebook for social networking. The measurements for _perceived usefulness for secondary system use_ and _secondary system use_ were newly developed since there is little research in this regard. These items were created by consulting both Davis ([1989](#dav89a)) and Kwon and Wen ([2010](#kwo10)). The measurements were designed to indicate that _primary system use_ is using Facebook for social networking and _secondary system use_ is using Facebook for personal documentation. Table 3 presents all measurement items used in this study. All items are measured on the 5-point Likert scale ranging from 1 as strongly disagree to 5 as strongly agree.

<table class="center" style="width:90%"><caption>  
Table 3: Measurements of the study  
</caption>

<tbody>

<tr>

<th>Factors</th>

<th>Indicators</th>

<th>Measure</th>

</tr>

<tr>

<td rowspan="6">Perceived ease of use (EOU)</td>

<td>EOU-1</td>

<td>I do not find Facebook difficult to use.</td>

</tr>

<tr>

<td>EOU-2</td>

<td>I learned how to use Facebook quickly.</td>

</tr>

<tr>

<td>EOU-3</td>

<td>Interacting with the Facebook interface is clear and understandable.</td>

</tr>

<tr>

<td>EOU-4</td>

<td>I find it easy to do what I want to do with Facebook.</td>

</tr>

<tr>

<td>EOU-5</td>

<td>I find it easy to become skillful at using Facebook.</td>

</tr>

<tr>

<td>EOU-6</td>

<td>Overall, I find Facebook easy to use.</td>

</tr>

<tr>

<td rowspan="6">Perceived usefulness for primary system use (PUPU)</td>

<td>PUPU-1</td>

<td>It is easier to network with my family, friends, and colleagues due to Facebook.</td>

</tr>

<tr>

<td>PUPU-2</td>

<td>Facebook supports important aspects of my social life.</td>

</tr>

<tr>

<td>PUPU-3</td>

<td>Facebook helps me get to know family, friends, and colleagues better.</td>

</tr>

<tr>

<td>PUPU-4</td>

<td>Facebook helps me stay in contact with my family, friends, and colleagues.</td>

</tr>

<tr>

<td>PUPU-5</td>

<td>Facebook helps me to improve my connections with friends.</td>

</tr>

<tr>

<td>PUPU-6</td>

<td>Overall, I find Facebook useful for building and managing my personal network.</td>

</tr>

<tr>

<td rowspan="5">Primary system use (PUSE)</td>

<td>PUSE-1</td>

<td>I spend a lot of time communicating with friends on Facebook.</td>

</tr>

<tr>

<td>PUSE-2</td>

<td>I tend to use Facebook to network with friends.</td>

</tr>

<tr>

<td>PUSE-3</td>

<td>Facebook is one of my preferred methods for communicating with my family, friends, and colleagues.</td>

</tr>

<tr>

<td>PUSE-4</td>

<td>I have some friends that I communicate with more often on Facebook.</td>

</tr>

<tr>

<td>PUSE-5</td>

<td>Facebook has increased and broadened the communication with my family, friends, and colleagues.</td>

</tr>

<tr>

<td rowspan="6">Perceived usefulness for secondary system use (PUSU)</td>

<td>PUSU-1</td>

<td>Facebook makes it easier to record my everyday activities and stories.</td>

</tr>

<tr>

<td>PUSU-2</td>

<td>Facebook helps me to save my stories and pictures.</td>

</tr>

<tr>

<td>PUSU-3</td>

<td>Facebook allows me to store everyday narratives, events and pictures in one place.</td>

</tr>

<tr>

<td>PUSU-4</td>

<td>Facebook makes it easier to record what I do, where I visit, and whom I am with.</td>

</tr>

<tr>

<td>PUSU-5</td>

<td>Facebook helps me store important moments of my life history.</td>

</tr>

<tr>

<td>PUSU-6</td>

<td>Overall, I find Facebook useful for recording my personal life.</td>

</tr>

<tr>

<td rowspan="7">Secondary system use (SUSE)</td>

<td>SUSE-1</td>

<td>A significant portion of my Facebook postings is regarding my personal life.</td>

</tr>

<tr>

<td>SUSE-2</td>

<td>I spend a lot of time on Facebook posting about what I do, where I visit, and whom I am with.</td>

</tr>

<tr>

<td>SUSE-3</td>

<td>Facebook is one of the places that I record my everyday stories.</td>

</tr>

<tr>

<td>SUSE-4</td>

<td>My Facebook postings include my day-to-day stories.</td>

</tr>

<tr>

<td>SUSE-5</td>

<td>My Facebook timeline depicts stories and pictures about interesting moments of my life.</td>

</tr>

<tr>

<td>SUSE-6</td>

<td>My user profile and timeline show information about my life in the real world.</td>

</tr>

<tr>

<td>SUSE-7</td>

<td>The frequency of posting and documenting my life stories on Facebook has increased.</td>

</tr>

</tbody>

</table>

## Results

We conducted a partial least squares analysis to test the research model. Partial least squares is a type of analysis for variance-based structural equation modelling that is more appropriate with a priori theory and pre-validated measurement scales ([Bagozzi and Phillips, 1982](#bag92)). Partial least squares analysis is useful to model complex relationships among multiple latent variables, each measured through a number of manifest variables ([Sharma and Kim, 2012](#sha12)). In addition, partial least squares analysis does not impose sample sise restrictions or require multivariate normality distribution for the underlying data ([Fornell and Bookstein, 1982](#for82)). As we were using the research model with pre-validated measurements and the model includes five latent variables, it made sense to apply partial least squares for model analysis.

### Construct validation and reliability

Scale reliability should be examined to check for internal consistency of the items of a latent construct. The Cronbach’s alpha values for each latent variable were calculated. All alpha values for each latent variable were greater than 0.8, supporting internal reliability of the data (Table 4). As all the blocks are reflective, they should be homogeneous and unidimensional. Table 4 also presents the block homogeneity and unidimensionality. All blocks have the Dillon-Goldstein’s rho greater than 0.7, thus considered homogenous. In addition, from the eigenvalues of the blocks, we can be confident that it is unidimensional and reflects the model appropriately as the first eigenvalues of each block are greater than 1.

For validation, we examined convergent validity and discriminant validity. Convergent validity checks whether the measures of a construct that are theoretically related are observed as related. A common measure for examining convergent validity is the average variance extracted. An average variance extracted of less than 0.5 is considered insufficient as more variance is due to error variance than to indicator variance ([Götz, Liehr-Gobbers and Krafft, 2010](#got10)). Table 4 shows that average variance extracted values for each construct are greater than 0.5 thresholds. Table 5 shows that all measurement loadings indicate that items in each category construct are loaded as the same component, and thus the data supports discriminant validity at the item level. It can be inferred that all constructs share more variance with the measures within the construct than with those in other constructs. From Table 5, we can also observe that the convergent validity is met as the average loadings range from 0.7 to 0.9\. Therefore, we can see that all items converge in estimating the construct.

Discriminant validity ensures that the measures of a construct that are not theoretically related are observed as not related. We examined discriminant validity based on Fornell and Larcker’s ([1981](#for81)) recommendation that a latent variable’s average variance extracted should be larger than the common variances (squared correlations) of the latent variable with any other constructs in the model. As the data presented in Tables 4 and 6 indicates, the values of average variance extracted of each construct (Table 4) are larger than squared correlations of the constructs (Table 6). Hence, the discriminant validity is met for our model supporting confidence in the adequacy of measurement.

<table class="center" style="width:95%"><caption>  
Table 4: Reliability and validity of manifest variables blocks  
</caption>

<tbody>

<tr>

<th>Latent variables</th>

<th># of manifest variables</th>

<th>Cronbach's alpha</th>

<th>Dillon-Goldstein's rho</th>

<th>Average variance extracted</th>

<th>Principal component analysis eigenvalues</th>

</tr>

<tr>

<td rowspan="6">Perceived ease of use (EOU)</td>

<td rowspan="6" style="text-align:center">6</td>

<td rowspan="6" style="text-align:center">0.915</td>

<td rowspan="6" style="text-align:center">0.935</td>

<td rowspan="6" style="text-align:center">0.703</td>

<td style="text-align:center">2.561</td>

</tr>

<tr>

<td style="text-align:center">0.384</td>

</tr>

<tr>

<td style="text-align:center">0.230</td>

</tr>

<tr>

<td style="text-align:center">0.208</td>

</tr>

<tr>

<td style="text-align:center">0.119</td>

</tr>

<tr>

<td style="text-align:center">0.113</td>

</tr>

<tr>

<td rowspan="6">Perceived usefulness for primary system use (PUPU)</td>

<td rowspan="6" style="text-align:center">6</td>

<td rowspan="6" style="text-align:center">0.872</td>

<td rowspan="6" style="text-align:center">0.909</td>

<td rowspan="6" style="text-align:center">0.615</td>

<td style="text-align:center">3.251</td>

</tr>

<tr>

<td style="text-align:center">0.537</td>

</tr>

<tr>

<td style="text-align:center">0.421</td>

</tr>

<tr>

<td style="text-align:center">0.413</td>

</tr>

<tr>

<td style="text-align:center">0.311</td>

</tr>

<tr>

<td style="text-align:center">0.164</td>

</tr>

<tr>

<td rowspan="5">Primary system use (PUSE)</td>

<td rowspan="5" style="text-align:center">5</td>

<td rowspan="5" style="text-align:center">0.842</td>

<td rowspan="5" style="text-align:center">0.891</td>

<td rowspan="5" style="text-align:center">0.613</td>

<td style="text-align:center">3.373</td>

</tr>

<tr>

<td style="text-align:center">0.651</td>

</tr>

<tr>

<td style="text-align:center">0.602</td>

</tr>

<tr>

<td style="text-align:center">0.400</td>

</tr>

<tr>

<td style="text-align:center">0.344</td>

</tr>

<tr>

<td rowspan="6">Perceived usefulness for secondary system use (PUSU)</td>

<td rowspan="6" style="text-align:center">6</td>

<td rowspan="6" style="text-align:center">0.930</td>

<td rowspan="6" style="text-align:center">0.945</td>

<td rowspan="6" style="text-align:center">0.741</td>

<td style="text-align:center">4.897</td>

</tr>

<tr>

<td style="text-align:center">0.497</td>

</tr>

<tr>

<td style="text-align:center">0.339</td>

</tr>

<tr>

<td style="text-align:center">0.330</td>

</tr>

<tr>

<td style="text-align:center">0.313</td>

</tr>

<tr>

<td style="text-align:center">0.226</td>

</tr>

<tr>

<td rowspan="7">Secondary system use (SUSE)</td>

<td rowspan="7" style="text-align:center">7</td>

<td rowspan="7" style="text-align:center">0.914</td>

<td rowspan="7" style="text-align:center">0.933</td>

<td rowspan="7" style="text-align:center">0.661</td>

<td style="text-align:center">6.593</td>

</tr>

<tr>

<td style="text-align:center">1.032</td>

</tr>

<tr>

<td style="text-align:center">0.666</td>

</tr>

<tr>

<td style="text-align:center">0.546</td>

</tr>

<tr>

<td style="text-align:center">0.449</td>

</tr>

<tr>

<td style="text-align:center">0.362</td>

</tr>

<tr>

<td style="text-align:center">0.238</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 5: Cross-loadings for the outer model  
</caption>

<tbody>

<tr>

<th></th>

<th style="text-align:center">Perceived ease of use (EOU)</th>

<th style="text-align:center">Perceived usefulness for primary system use (PUPU)</th>

<th style="text-align:center">Primary system use (PUSE)</th>

<th style="text-align:center">Perceived usefulness for secondary system use (PUSU)</th>

<th style="text-align:center">Secondary system use (SUSE)</th>

</tr>

<tr>

<td>EOU-1</td>

<td style="text-align:center">**0.818**</td>

<td style="text-align:center">0.196</td>

<td style="text-align:center">0.280</td>

<td style="text-align:center">0.274</td>

<td style="text-align:center">0.245</td>

</tr>

<tr>

<td>EOU-2</td>

<td style="text-align:center">**0.722**</td>

<td style="text-align:center">0.169</td>

<td style="text-align:center">0.214</td>

<td style="text-align:center">0.175</td>

<td style="text-align:center">0.159</td>

</tr>

<tr>

<td>EOU-3</td>

<td style="text-align:center">**0.839**</td>

<td style="text-align:center">0.211</td>

<td style="text-align:center">0.288</td>

<td style="text-align:center">0.323</td>

<td style="text-align:center">0.240</td>

</tr>

<tr>

<td>EOU-4</td>

<td style="text-align:center">**0.883**</td>

<td style="text-align:center">0.281</td>

<td style="text-align:center">0.359</td>

<td style="text-align:center">0.382</td>

<td style="text-align:center">0.322</td>

</tr>

<tr>

<td>EOU-5</td>

<td style="text-align:center">**0.857**</td>

<td style="text-align:center">0.182</td>

<td style="text-align:center">0.291</td>

<td style="text-align:center">0.262</td>

<td style="text-align:center">0.252</td>

</tr>

<tr>

<td>EOU-6</td>

<td style="text-align:center">**0.900**</td>

<td style="text-align:center">0.225</td>

<td style="text-align:center">0.334</td>

<td style="text-align:center">0.337</td>

<td style="text-align:center">0.302</td>

</tr>

<tr>

<td>PUPU-1</td>

<td style="text-align:center">0.258</td>

<td style="text-align:center">**0.718**</td>

<td style="text-align:center">0.598</td>

<td style="text-align:center">0.411</td>

<td style="text-align:center">0.341</td>

</tr>

<tr>

<td>PUPU-2</td>

<td style="text-align:center">0.155</td>

<td style="text-align:center">**0.815**</td>

<td style="text-align:center">0.712</td>

<td style="text-align:center">0.501</td>

<td style="text-align:center">0.580</td>

</tr>

<tr>

<td>PUPU-3</td>

<td style="text-align:center">0.179</td>

<td style="text-align:center">**0.790**</td>

<td style="text-align:center">0.596</td>

<td style="text-align:center">0.570</td>

<td style="text-align:center">0.471</td>

</tr>

<tr>

<td>PUPU-4</td>

<td style="text-align:center">0.283</td>

<td style="text-align:center">**0.712**</td>

<td style="text-align:center">0.590</td>

<td style="text-align:center">0.485</td>

<td style="text-align:center">0.406</td>

</tr>

<tr>

<td>PUPU-5</td>

<td style="text-align:center">0.183</td>

<td style="text-align:center">**0.849**</td>

<td style="text-align:center">0.654</td>

<td style="text-align:center">0.524</td>

<td style="text-align:center">0.454</td>

</tr>

<tr>

<td>PUPU-6</td>

<td style="text-align:center">0.236</td>

<td style="text-align:center">**0.811**</td>

<td style="text-align:center">0.698</td>

<td style="text-align:center">0.474</td>

<td style="text-align:center">0.469</td>

</tr>

<tr>

<td>PUSE-1</td>

<td style="text-align:center">0.287</td>

<td style="text-align:center">0.645</td>

<td style="text-align:center">**0.844**</td>

<td style="text-align:center">0.512</td>

<td style="text-align:center">0.618</td>

</tr>

<tr>

<td>PUSE-2</td>

<td style="text-align:center">0.327</td>

<td style="text-align:center">0.636</td>

<td style="text-align:center">**0.726**</td>

<td style="text-align:center">0.466</td>

<td style="text-align:center">0.485</td>

</tr>

<tr>

<td>PUSE-3</td>

<td style="text-align:center">0.306</td>

<td style="text-align:center">0.646</td>

<td style="text-align:center">**0.820**</td>

<td style="text-align:center">0.487</td>

<td style="text-align:center">0.525</td>

</tr>

<tr>

<td>PUSE-4</td>

<td style="text-align:center">0.228</td>

<td style="text-align:center">0.633</td>

<td style="text-align:center">**0.728**</td>

<td style="text-align:center">0.428</td>

<td style="text-align:center">0.379</td>

</tr>

<tr>

<td>PUSE-5</td>

<td style="text-align:center">0.255</td>

<td style="text-align:center">0.687</td>

<td style="text-align:center">**0.788**</td>

<td style="text-align:center">0.514</td>

<td style="text-align:center">0.447</td>

</tr>

<tr>

<td>PUSU-1</td>

<td style="text-align:center">0.297</td>

<td style="text-align:center">0.530</td>

<td style="text-align:center">0.527</td>

<td style="text-align:center">**0.840**</td>

<td style="text-align:center">0.620</td>

</tr>

<tr>

<td>PUSU-2</td>

<td style="text-align:center">0.352</td>

<td style="text-align:center">0.568</td>

<td style="text-align:center">0.549</td>

<td style="text-align:center">**0.866**</td>

<td style="text-align:center">0.538</td>

</tr>

<tr>

<td>PUSU-3</td>

<td style="text-align:center">0.305</td>

<td style="text-align:center">0.510</td>

<td style="text-align:center">0.515</td>

<td style="text-align:center">**0.868**</td>

<td style="text-align:center">0.553</td>

</tr>

<tr>

<td>PUSU-4</td>

<td style="text-align:center">0.305</td>

<td style="text-align:center">0.534</td>

<td style="text-align:center">0.477</td>

<td style="text-align:center">**0.841**</td>

<td style="text-align:center">0.565</td>

</tr>

<tr>

<td>PUSU-5</td>

<td style="text-align:center">0.287</td>

<td style="text-align:center">0.518</td>

<td style="text-align:center">0.520</td>

<td style="text-align:center">**0.861**</td>

<td style="text-align:center">0.555</td>

</tr>

<tr>

<td>PUSU-6</td>

<td style="text-align:center">0.322</td>

<td style="text-align:center">0.588</td>

<td style="text-align:center">0.571</td>

<td style="text-align:center">**0.888**</td>

<td style="text-align:center">0.649</td>

</tr>

<tr>

<td>SUSE-1</td>

<td style="text-align:center">0.202</td>

<td style="text-align:center">0.466</td>

<td style="text-align:center">0.476</td>

<td style="text-align:center">0.516</td>

<td style="text-align:center">**0.790**</td>

</tr>

<tr>

<td>SUSE-2</td>

<td style="text-align:center">0.275</td>

<td style="text-align:center">0.479</td>

<td style="text-align:center">0.530</td>

<td style="text-align:center">0.539</td>

<td style="text-align:center">**0.850**</td>

</tr>

<tr>

<td>SUSE-3</td>

<td style="text-align:center">0.301</td>

<td style="text-align:center">0.497</td>

<td style="text-align:center">0.585</td>

<td style="text-align:center">0.590</td>

<td style="text-align:center">**0.892**</td>

</tr>

<tr>

<td>SUSE-4</td>

<td style="text-align:center">0.206</td>

<td style="text-align:center">0.432</td>

<td style="text-align:center">0.493</td>

<td style="text-align:center">0.504</td>

<td style="text-align:center">**0.853**</td>

</tr>

<tr>

<td>SUSE-5</td>

<td style="text-align:center">0.253</td>

<td style="text-align:center">0.529</td>

<td style="text-align:center">0.527</td>

<td style="text-align:center">0.641</td>

<td style="text-align:center">**0.774**</td>

</tr>

<tr>

<td>SUSE-6</td>

<td style="text-align:center">0.294</td>

<td style="text-align:center">0.497</td>

<td style="text-align:center">0.500</td>

<td style="text-align:center">0.565</td>

<td style="text-align:center">**0.735**</td>

</tr>

<tr>

<td>SUSE-7</td>

<td style="text-align:center">0.236</td>

<td style="text-align:center">0.452</td>

<td style="text-align:center">0.532</td>

<td style="text-align:center">0.493</td>

<td style="text-align:center">**0.786**</td>

</tr>

</tbody>

</table>

<table class="center"><caption>  
Table 6: Inter-construct squared correlation  
</caption>

<tbody>

<tr>

<th> </th>

<th style="text-align:center">Perceived ease of use</th>

<th style="text-align:center">Perceived usefulness for primary system use</th>

<th style="text-align:center">Primary system use</th>

<th style="text-align:center">Perceived usefulness for secondary system use</th>

<th style="text-align:center">Secondary system use</th>

</tr>

<tr>

<td>EOU</td>

<td style="text-align:center">1.000</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>PUPU</td>

<td style="text-align:center">0.066</td>

<td style="text-align:center">1.000</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>PUSE</td>

<td style="text-align:center">0.129</td>

<td style="text-align:center">0.671</td>

<td style="text-align:center">1.000</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>PUSU</td>

<td style="text-align:center">0.131</td>

<td style="text-align:center">0.397</td>

<td style="text-align:center">0.377</td>

<td style="text-align:center">1.000</td>

<td> </td>

</tr>

<tr>

<td>SUSE</td>

<td style="text-align:center">0.097</td>

<td style="text-align:center">0.346</td>

<td style="text-align:center">0.411</td>

<td style="text-align:center">0.458</td>

<td style="text-align:center">1.000</td>

</tr>

</tbody>

</table>

### Testing the structural model and path analysis

The goodness of fit (GoF) indicates the overall performance of the model considering both the measurement and the structural model ([Esposito Vinzi, Trinchera and Amato, 2010](#esp10)). Table 7 reveals that the relative GoF of our model (GoFrel=0.902) is higher than the 0.9 threshold which suggests that it is a good model ([Esposito Vinzi, Trinchera and Amato, 2010](#esp10)).

<table class="center" style="width:95%"><caption>  
Table 7: Goodness-of-fit</caption>

<tbody>

<tr>

<th></th>

<th style="text-align:center">GoF</th>

<th style="text-align:center">GoF  
(Bootstrap)</th>

<th style="text-align:center">Standard  
error</th>

<th style="text-align:center">Critical  
ratio</th>

<th style="text-align:center">Lower  
bound (95%)</th>

<th style="text-align:center">Upper  
bound (95%)</th>

</tr>

<tr>

<td>Absolute</td>

<td style="text-align:center">0.529</td>

<td style="text-align:center">0.532</td>

<td style="text-align:center">0.021</td>

<td style="text-align:center">24.854</td>

<td style="text-align:center">0.487</td>

<td style="text-align:center">0.576</td>

</tr>

<tr>

<td>Relative</td>

<td style="text-align:center">0.902</td>

<td style="text-align:center">0.868</td>

<td style="text-align:center">0.024</td>

<td style="text-align:center">37.261</td>

<td style="text-align:center">0.825</td>

<td style="text-align:center">0.921</td>

</tr>

<tr>

<td>Outer model</td>

<td style="text-align:center">0.997</td>

<td style="text-align:center">0.995</td>

<td style="text-align:center">0.002</td>

<td style="text-align:center">506.171</td>

<td style="text-align:center">0.987</td>

<td style="text-align:center">0.997</td>

</tr>

<tr>

<td>Inner model</td>

<td style="text-align:center">0.905</td>

<td style="text-align:center">0.873</td>

<td style="text-align:center">0.023</td>

<td style="text-align:center">38.638</td>

<td style="text-align:center">0.831</td>

<td style="text-align:center">0.925</td>

</tr>

</tbody>

</table>

Figure 3 displays the overview of the model analysis results, including variance explained (_R<sup>2</sup>_) for each latent variable, path coefficients, and path significances. Facebook’s primary use for social networking is explained by _perceived usefulness for primary system use_ and _perceived ease of use_ with 69.4 per cent of variance; most of the explanation is supplied by _perceived usefulness for primary system use_ (_ß_=0.778). As easily assumed, users are likely to use Facebook for social networking purposes when they perceive it is useful for this purpose. Specifically, hypotheses H1a through H1c are confirmed by the partial least squares and path analysis. There is a positive influence from _perceived ease of use_ to _perceived usefulness for primary system use_. The _primary system use_ is significantly positively associated with _perceived usefulness for primary system use_ and _perceived ease of use_. The relationships in this part of the model are once again proved in accordance with the previous technology acceptance model studies, regarding technology adoption.

The part that describes the development of _perceived usefulness for secondary system use_ from the _primary system use_ shows a positive relationship with 37.7 per cent of variance. The perceived usefulness of Facebook for personal documentation (_PUSU_) is significantly associated with using it for social networking (_primary system use_) (_ß_=0.614), and therefore, hypothesis H2 is validated. Nearly 54 per cent of variance in the _secondary system use_, i.e., Facebook use for personal document, is explained by _perceived usefulness for secondary system use_, i.e., perceived usefulness for personal documentation (_ß_=0.420) and the _primary system use_, i.e., the use of Facebook for social networking (_ß_=0.397). The results support hypotheses H3a and H3b that the _secondary system use_ is explained and influenced significantly by the _primary system use_ (_PUSE_) and the _perceived usefulness_ (_PUSU_). Collectively, the results show that the positive outcomes of an information system for its primary purposes leads to post-adoptive attitudes, and hence to post-adoptive uses.

<figure class="centre">![Figure 3: Partial least squares and path analysis](p698fig3.png)

<figcaption>Figure 3: Partial least squares and path analysis</figcaption>

</figure>

### Post-adoptive use and interaction activeness

To test hypothesis 4, the partial least squares and path analysis of this research model were repeated for various groups of respondents with differing levels of active interactions in Facebook. _Active interaction_ was measured by two categories: the frequency with which users’ accessed Facebook and the frequency of posts on their Facebook timelines. Those frequencies that appeared on a daily basis were considered _active_ and those with less than daily interactions were categorised as _inactive_. For the active interaction groups, 188 participants were identified as accessing Facebook on a daily basis, and 32 participants who posted on their timeline daily. For the inactive interaction groups, there were 65 participants who accessed Facebook weekly or less, and 223 participants who posted to their timeline with the same infrequency. We conducted the partial least squares and path analysis with the same research model for these different user groups to find out whether the _interaction activeness_ influences the research model in general.

As shown in Table 8, the variance explained (_R<sup>2</sup>_) for each latent variable and path coefficients (_ß_) are different in each group. In the active interaction groups, the _secondary system use_ is better explained with _primary system use_ and _perceived usefulness for secondary system use_ with 52.3 per cent variance for the participants who frequently accessed Facebook and 50.2 per cent variance for the participants who frequently posted on the Facebook timeline. The variances appear to be slightly lower in the inactive interaction groups, with 48.3 and 48.4 per cent of variances respectively for the infrequent access group and the infrequent posting group.

As for H4, the model relationships show different patterns pertaining to _interaction activeness_ of system use. It is confirmed that _secondary system use_ is likely to be influenced more by active system use even though the differences are rather small. It is notable that _perceived usefulness for secondary system use_ is explained by _primary system use_ with 59.7 per cent variance when participants post to their timeline actively (_ß_=0.772), compared to 31.6 per cent variance for inactive users (_ß_=0.563). As the outcomes of posting activities can directly lead to personal documentation, it may be assumed that when users use Facebook vigorously for social networking and communication, they tend to become readily aware of the potential for using Facebook functions for recording their stories and leaving digital traces.

<table class="center" style="width:95%;"><caption>  
Table 8: Partial least squares and path analysis according to the level of interaction activeness  
</caption>

<tbody>

<tr>

<th rowspan="2" colspan="2" style="text-align:center">Conditions</th>

<th colspan="4" style="text-align:center">Explained variance (_R<sup>2</sup>_)</th>

<th colspan="5" style="text-align:center">Path coefficient</th>

</tr>

<tr>

<th style="text-align:center">PUPU</th>

<th style="text-align:center">PUSE</th>

<th style="text-align:center">PUSU</th>

<th style="text-align:center">SUSE</th>

<th style="text-align:center">PUPU ? PUSE</th>

<th style="text-align:center">EOU ? PUSE</th>

<th style="text-align:center">PUSE ? PUSU</th>

<th style="text-align:center">PUSE ? SUSE</th>

<th style="text-align:center">PUSU ? SUSE</th>

</tr>

<tr>

<td rowspan="2">Active Interaction</td>

<td>Frequent Access</td>

<td style="text-align:center">0.084</td>

<td style="text-align:center">0.640</td>

<td style="text-align:center">0.370</td>

<td style="text-align:center">0.523</td>

<td style="text-align:center">0.638</td>

<td style="text-align:center">0.332</td>

<td style="text-align:center">0.609</td>

<td style="text-align:center">0.393</td>

<td style="text-align:center">0.413</td>

</tr>

<tr>

<td>Frequent Post</td>

<td style="text-align:center">0.109</td>

<td style="text-align:center">0.542</td>

<td style="text-align:center">0.597</td>

<td style="text-align:center">0.502</td>

<td style="text-align:center">0.613</td>

<td style="text-align:center">0.252</td>

<td style="text-align:center">0.772</td>

<td style="text-align:center">0.366</td>

<td style="text-align:center">0.340</td>

</tr>

<tr>

<td rowspan="2">Inactive Interaction</td>

<td>Infrequent Access</td>

<td style="text-align:center">0.117</td>

<td style="text-align:center">0.628</td>

<td style="text-align:center">0.329</td>

<td style="text-align:center">0.483</td>

<td style="text-align:center">0.693</td>

<td style="text-align:center">0.214</td>

<td style="text-align:center">0.574</td>

<td style="text-align:center">0.350</td>

<td style="text-align:center">0.432</td>

</tr>

<tr>

<td>Infrequent Post</td>

<td style="text-align:center">0.044</td>

<td style="text-align:center">0.673</td>

<td style="text-align:center">0.316</td>

<td style="text-align:center">0.484</td>

<td style="text-align:center">0.712</td>

<td style="text-align:center">0.284</td>

<td style="text-align:center">0.563</td>

<td style="text-align:center">0.371</td>

<td style="text-align:center">0.415</td>

</tr>

</tbody>

</table>

## Discussion

The purpose of the study was to prove that perceived usefulness, perceived ease of use, and actual use of the primary function of Facebook social networking are correlated to the user utilizing Facebook for the secondary function of personal documentation. Hypotheses H1a-c establish baseline of study participants’ level of perceived usefulness and perceived ease of use for primary purpose of Facebook, social networking. Hypotheses H2-H3 test whether high levels of perceived usefulness and perceived ease of use for primary purpose, established in Hypotheses H1a-c, affect level of perceived usefulness and perceived ease of use in adopting secondary use for personal documentation. Hypothesis H4 test whether the level of activeness in use affect the development of the secondary use.

The results indicate that when users consider Facebook to be useful and easy to use for social networking, they are likely to use it actively for the purpose, confirming H1a to H1c. This is to say, the _perceived usefulness for primary system use_ and _perceived ease of use_ are important determinants in using a social network system as the technology acceptance model once again warrants as in previous studies ([Karahanna, Straub and Chervany, 1999](#kar99)). The results show a use pattern of post-adoption stage, that is, the _perceived usefulness for primary system use_ is a stronger determinant for using the system than the _perceived ease of use_. In the post-adoptive stage, since users already know and utilise available primary functions with ease, the _perceived ease of use_ does not discern the differences in system use.

On the other hand, the _perceived usefulness for primary system use_ influences users to continue using it and even lets users continue to derive new possible usefulness of using the system. This was especially confirmed to be true with H2 where Facebook use experience (_primary system use_) is highly associated with becoming aware of the benefits of secondary system use (_perceived usefulness for secondary system use_). In this study, we identified that the use of Facebook for social networking appears to provide opportunity to grasp that it may also be useful for personal documentation. This leads to the understanding that users’ secondary use does not just result from the increased primary use but also enhances with the development of the awareness of usefulness for the secondary use.

In fact, with the hypothesis H4, the emergence of new perceptions and secondary system use is highly influenced by the extent to which users actively interact with the system for its primary use. It was confirmed that the _perceived usefulness for secondary system use_ is likely to be influenced more by active system use. This may also signify that since the existing functionalities of Facebook support not only social networking but also personal documentation, these activities seem to occur as accompanying or in consequence, as users actively use it or gain more experience with the functionalities of the system for the initial purpose of social networking. Therefore, in the post-adoptive phase, Facebook usage can be largely dependent on how much users are engaged and become routine in using the system for its primary purposes.

Similarly, the _secondary system use_ is found to be influenced significantly by the _primary system use_ as well as the _perceived usefulness_, supporting hypotheses H3a and H3b. An awareness of the usefulness for personal documentation is an important determinant that drives users to actually use Facebook for personal documentation. Now users use Facebook for the purpose of recording their life stories with clear awareness of their activities, not as an accompanying activity as a result of social networking. This finding is in line with previous studies (e.g. [Bhattacherjee, 2001](#bha01); [Hsieh and Wang, 2007](#hsi07); [Saeed and Abdinnour, 2008](#sae08)) that say that awareness and perception of usefulness for newly discovered purposes are key factors inducing secondary system use. This finding advances the findings of the previous study that Facebook users document their life stories without the clear perception that what they do in Facebook is “documentation” ([Sinn and Syn, 2014](#sin14)).

Studies have reported that Facebook is used for various purposes ([Mazman and Usluel, 2010](#maz10); [Sinn and Syn, 2014](#sin14); [Wong, 2012](#won12)). Associations with surrounding determinants to secondary system uses in Facebook can be described with our research model, with which we can also tell the patterns and associations in patterns. The partial least squares and path analysis results confirm the existence of secondary system use of Facebook for personal documentation. The results from the partial least squares and path analysis statistically prove the relationships between Facebook use for social networking and personal documentation. Such results may provide guidance to social network system developers with potential areas of development or improvement.

The findings also demonstrate that users who actively interact with the system tend to find potential secondary system uses (H4). Particularly for the study’s case, the findings show that there is a higher chance for frequent timeline post creators to become aware of the usefulness of Facebook for personal documentation; less frequent timeline post creators are not as likely to make use of the personal documentation. The development of perception for the usefulness for personal documentation is more likely to occur to seasoned users. Their usages of Facebook for personal documentation are not just by-products of social networking, but the results of their perception and behavioural intention to use it for that purpose. As the services have been public for many years now with many active and experienced users, Facebook seems to have realised such changes. There are new functions provided to benefit the activities of personal documentation, such as “Year in Review” and “A Look Back,” which were not available when Facebook first began ([Facebook help center, n.d.](#fac-b)). We argue that such changes in behavioural patterns may occur not only in Facebook but also in other social network services.

The implications of this study could be applied in various settings of research and development. For system developers and managers, this study provides a way to understand factors that can lead to post-adoptive use and to identify the existence of secondary post-adoptive uses. Jasperson, Carter and Zmud ([2005](#jas05)) asserted that understanding post-adoptive use is essential in identifying issues for management and development of information technology and for providing resources for post-adoptive life cycles. The reason for this is that examining post-adoptive use provides insights into users’ preferences in using information systems as well as for possible expansion of system use, which can provide directions for improvement or next level development of the system. In addition, secondary system use usually occurs when users have actively utilised the information system since the current primary system use leads to an awareness of usefulness for the secondary system use. Thus, the existence of secondary system uses can be a parameter that information system developers can use to determine the status of current acceptance and use of any information system.

For researchers, this study contributes to an understanding of factors and their relationships that influence secondary uses of information systems. Our research model was grounded on previous studies on the technology acceptance model and post-adoptive uses. This research model effectively demonstrated the relationships among surrounding factors of primary and secondary system uses in the case of Facebook. Social network services could present some differences as information systems since there exists a significant factor, users’ contribution. However, this research model is found to be useful to describe the usage and changes in usage of a social network system. Thus, analysis of this model shows potential that it can be applied to post-adoptive use analyses of social network systems, suggesting a way of applying previous models and measurements for social network systems.

E-mail and blogs have been an important area of study for information science researchers from the perspective of personal information management. More recently, social network systems seem to have become popular tools for archiving personal information. Facebook is useful for recording what users do, how they feel, and what types of information are considered important and useful to them. Such additional perceptions of usefulness can be developed from the primary use of the system, as seen from our findings. In this sense, Facebook, although designed for social networking, can serve as a convenient and easily accessible personal repository in which to document daily happenings.

As various social network systems are popularly used, this study provides insights about the potential existence of abundant digital traces of personal history in the social web. Information professionals may need to pay more attention to this aspect, given that social network systems have a great potential to be a personal information management tool and/or personal repository of individuals’ histories. In addition, it is important for information professionals to recognise that different strategies and focus need to be applied when encouraging personal documentation. We found that active Facebook users are already aware of usefulness of using Facebook as a personal repository. This may further mean that immature users may leave their digital traces of personal life without knowing it. Information professionals should find ways to assist users in regards to how to manage digital traces in a way to protect privacy as well as to preserve rich information about contemporary people’s lives.

## Limitations

This study includes some limitations. First, in order to design our research model, this study considered basic factors that influence secondary uses of Facebook among those identified in the technology acceptance model and post-adoption studies. In different contexts and approaches, additional factors may be considered when investigating post-adoptive use to extend the research model. In further research, a different research model can be applied to include context-specific factors. Social factors could be one example that investigates how personal levels of documenting life stories and social levels of communicating with others influence each other in developing secondary system use. Other demographic variables or technology related factors (e.g. efficacy) can be included in future studies to learn about the users and their own context in relation to the system use. Secondly, we applied our research model to a specific system and secondary use, personal documentation in Facebook.

However, since there are numerous types of advanced information systems and various secondary system uses that could be developed, findings of this study and the research model cannot be generalised to all types of information systems or secondary system uses. Third, as it is difficult to reach out to random sample of the population of Facebook users, Amazon Mechanical Turk was used to collect responses from its registered users. Mechanical Turk has its own benefit of having a broad and varied population, however, it should be noted that Mechanical Turk users may not represent the Facebook population perfectly. Despite the limitation of Mechanical Turk, we adopted Mechanical Turk based on Ipeirotis’ ([2009](#ipe09)) argument that from the survey on Mechanical Turk, it is observed that the demographics of Mechanical Turk participants are similar to the general U.S. Internet population.

## Conclusion

A previous study identified that Facebook users indeed use the service for personal documentation purposes whether or not they perceived their use this way ([Sinn and Syn, 2014](#sin14)). Following up on the previous study, we examined what causes such secondary system use of Facebook and how it is related to the primary system use of Facebook. The goal of this study was to explain the relationship between primary and secondary system uses to understand the development of the secondary system uses.

To find a way to examine such relationships, we created a research model based on discussions and findings from previous studies on the technology acceptance model and post-adoptive use. The research model considered hypotheses to address (1) perceptions of and system use for primary purposes (i.e., social networking) to identify the pattern of post-adoptive behaviour; (2) the development of the perceived usefulness of a secondary purpose (i.e., personal documentation); and (3) secondary system use (i.e., personal documentation). Research hypotheses were addressed using the partial least squares and path analysis with the research model.

Through the research model and statistical analysis, this study found that when users actively network with their friends and family on Facebook, they are likely to recognise its usefulness for other purposes. The results presented that the _perceived usefulness for primary system use_ influences users to continue using the system in the post-adoption stage and even lets users continue to derive new possible usefulness of using the system (_perceived usefulness for secondary system use_). New perceptions can lead to secondary uses of Facebook, in our case, personal documentation.

The findings also identify that the secondary system use does not solely result from the increased primary use. It is developed with the awareness of _usefulness for the secondary use_. Therefore, the use of Facebook for personal documentation are not just by-products of social networking, but the results of users’ perception and intention to use it for such purpose. It was also found that in the post-adoptive phase, the development of perception for the usefulness for the secondary system use is more likely to occur with experienced users. Facebook has become one of the interesting places and convenient tools for managing personal information and for documenting personal life stories. The findings suggest that information professionals need to pay attention to social network systems as they serve in positions of personal information management and preservation venues.

This study contributes to the understanding of information system uses for secondary purposes by providing a baseline research model for analysing post-adoptive use. The research model, rooted in the technology acceptance model research and related studies, was designed to explain the formation and existence of secondary system uses and related factors. Our analysis of the uses of Facebook confirmed that the research model successfully identified the relationships among the factors and paths to explain the factors influencing and surrounding a secondary system use. As current information system users easily identify various uses of systems for different purposes, we expect that this research model will be useful for providing better understanding of post-adoptive use. By learning about users’ post-adoptive use, system developers and information professionals can identify directions for future development and improvement of systems.

To summarise our major findings and significant contribution of this study:

*   This study contributes to the understanding of formation of the secondary system use by providing a baseline research model and a case of its application.
*   The findings of this study emphasise the development of user perception of usefulness for the secondary use when using a system actively for the primary use.
*   From the case applied to the research model, it was found that Facebook users become aware of the usefulness of using Facebook for personal documentation while they use it for social networking.
*   Using Facebook as an example of an advanced information system, this study demonstrated that factors and measurements used for post-adoptive behaviour studies could be applied for social network systems.
*   This study provides significant implications for system developers in knowing the ways users develop secondary uses that will help improve the system further.
*   This study demonstrates the existence of users’ personal documentation activities on social network systems, which implies the needs for information professionals to consider preservation, use, and guidance for such resources, and at the same time to assist users in managing their digital traces with different strategies depending on their activeness in using the system.

There are various future research directions available with different contexts and perspectives taking additional factors into consideration. Further, this study tested the research model with a single possible case of secondary use of Facebook. The research model can be tested for other potential secondary uses of Facebook or in other social networking systems. Finally, although the use of this research model was helpful in identifying the influences of determinants, it did not provide further explanation of the formation of the secondary system use. A qualitative study could be designed as a future study to better understand the relationships between primary and secondary system uses.

## About the authors

**Sue Yeon Syn** is an assistant professor of Department of Library and Information Science at the Catholic University of America. She earned her Ph.D. in Information Science from University of Pittsburgh. Her research interests focus on social media and social informatics, and user involvement in information creation and sharing. She uses social media as a channel for users to participate in creating information, and applies various methods to make the best use of user generated information for different purposes such as metadata generation, information organization, and information sharing. Her research also addresses user’s information behavior in social media settings. She ca be contacted at: [syn@cua.edu](mailto:syn@cua.edu)  
**Donghee Sinn** is an associate professor of the Department of Information Studies, University at Albany (State University of New York). She specialises in Archives and Records Management, and her research interests focus particularly on the archival research in relation with personal archiving and public memory in the digital environment and archival use/user studies of primary sources in digital formats. Previously, she worked at the National Archives of Korea in acquisition and appraisal. She can be contacted at: [dsinn@albany.edu](mail:dsinn@albany.edu)

#### References

*   Ajzen, I. (1991). The theory of planned behavior. _organizational Behavior and Human Decision Processes, 50_(2), 179-211.
*   Bhattacherjee, A. (2001). Understanding information systems continuance: an expectation-confirmation model. _MIS Quarterly, 25_(3), 351-370.
*   Bhattacherjee, A. & Premkumar, G. (2004). Understanding changes in belief and attitude toward information technology usage: a theoretical model and longitudinal test. _MIS Quarterly, 28_(2), 229-254.
*   Bagozzi, R. P. & Phillips, L. W. (1982). Representing and testing organizational theories: a holistic construal. _Administrative Science Quarterly, 27_(3), 459-489.
*   Chin, W. W. & Marcolin, B. L. (2001). The future of diffusion research. _The DATA BASE for Advances in Information Systems, 32_(3), 8-12.
*   Cooper, R. B. & Zmud, R. B. (1990). Information technology implementation research: a technological diffusion approach. _Management Science, 36_(2), 123-139.
*   Davis, F. D. (1989). Perceived usefulness, perceived ease of use, and user acceptance of information technology. _MIS Quarterly, 13_(3), 319-340.
*   Davis, F. D., Bagozzi, R. P. & Warshaw, P. R. (1989). User acceptance of computer technology: a comparison of two theoretical models. _Management Science, 35_(8), 982-1003.
*   Davis, F. D. & Venkatesh, V. (2004). Toward preprototype user acceptance testing of new information systems: implications for software project management. _IEEE Transactions on Engineering Management, 51_(1), 31-46.
*   DeSanctis, G. & Poole, M. S. (1994). Capturing the complexity in advanced technology use: adaptive structuration theory. _organization Science, 5_(2), 121-147.
*   Deng, X. & Chi, L. (2012). Understanding post-adoptive behaviors in IS use: a longitudinal analysis of system use problems in the business intelligence context. _Journal of Management Information Systems, 29_(3), 291-325.
*   Duggan, M., Ellison, N. B., Lampe, C., Lenhart, A. & Madden, M. (2015). [Social media update 2014](http://www.webcitation.org/6bZSoDMIB). Pew Research Center. Retrieved from http://www.pewinternet.org/files/2015/01/PI_SocialMediaUpdate20144.pdf (Archived by WebCite® at http://www.webcitation.org/6bZSoDMIB)
*   Esposito Vinzi, V., Trinchera, L. & Amato, S. (2010). PLS path modeling: from foundations to recent developments and open issues for model assessment and improvement. In Esposito Vinzi, V., Chin, W. W., Henseler, J. & Wang, H. (Eds.), _Handbook of partial least squares_ (pp. 15-51). Berlin, Germany: Springer.
*   Facebook. (n.d.). _[Stats](http://www.webcitation.org/6bZTDO5um)_. Retrieved from http://newsroom.fb.com/company-info/ (Archived by WebCite® at http://www.webcitation.org/6bZTDO5um)
*   Facebook. (n.d.). _[Help center - popular features](http://www.webcitation.org/6bZTPSeHd)_. Retrieved from https://www.facebook.com/help/ (Archived by WebCite® at http://www.webcitation.org/6bZTPSeHd)
*   Fishbein, M. & Ajzen, I. (1975). _Belief, attitude, intention, and behavior: an introduction to theory and research_. Reading, MA: Addision-Wesley.
*   Fornell, C. & Bookstein, F. L. (1982). Two structural equation models: LISREL and PLS applied to consumer exit-voice theory. _Journal of Marketing Research, 19_(4), 440-452.
*   Fornell, C. & Larcker, D. F. (1981). Evaluating structural equation models with unobservable variables and measurement errors. _Journal of Marketing Research, 18_(1), 39-50.
*   Fulk, J. (1993). Social construction of communication technology. _The Academy of Management Journal, 36_(5), 921-950.
*   Götz, O., Liehr-Gobbers, K. & Krafft, M. (2010). Evaluation of structural equation models using the partial least squares (PLS) approach. In Esposito Vinzi, V., Chin, W. W., Henseler, J. & Wang, H. (Eds.), _Handbook of partial least squares_ (pp. 691-711). Berlin: Springer.
*   Hampton, K. N., Goulet, L. S., Rainie, L. & Purcell, K. (2011). _[Social networking sites and our lives: how people's trust, personal relationships, and civic and political involvement are connected to their use of social networking sites and other technology.](http://www.webcitation.org/6bZTtax5b)_ Washington, DC: Pew Research Center's Internet & American Life Project. Retrieved from http://www.pewinternet.org/files/old-media//Files/Reports/2011/PIP%20-%20Social%20networking%20sites%20and%20our%20lives.pdf (Archived by WebCite® at http://www.webcitation.org/6bZTtax5b)
*   Hsieh, J. J. P. & Wang, W. (2007). Explaining employees' extended use of complex information systems. _European Journal of Information Systems, 16_(3), 216-227.
*   Ipeirotis, P. (2009). [Turker demographics vs. Internet demographics](http://www.webcitation.org/6bZU4t2uT). [Web log post]. Retrieved from http://www.behind-the-enemy-lines.com/2009/03/turker-demographics-vs-internet.html (Archived by WebCite® at http://www.webcitation.org/6bZU4t2uT)
*   Jasperson, J., Carter, P.E. & Zmud, R.W. (2005). A comprehensive conceptualisation of post-adoptive behaviors associated with information technology enabled work systems. _MIS Quarterly, 29_(3), 525-557.
*   Karahanna, E., Straub, D.W. & Chervany, N.L. (1999). Information technology adoption across time: a cross-sectional comparison of pre-adoption and post-adoption beliefs. _MIS Quarterly, 23_(2), 183-213.
*   Khalifa, M. & Liu, V. (2003). Determinants of satisfaction at different adoption stages of Internet-based services. _Journal of the Association for Information Systems, 4_(5), 206-232.
*   Kwon, O. & Wen, Y. (2010). An empirical study of the factors affecting social network service use. _Computers in Human Behavior, 26_(2), 254-263.
*   Lampe, C., Ellison, N. B. & Steinfield, C. (2008). Changes in use and perception of Facebook. In Begole, B. & McDonald, D. W. (Eds.), _Proceedings of the 2008 ACM Conference on Computer Supported Cooperative Work (CSCW'08), San Diego, CA, USA, November 8-12, 2008._ (pp. 721-730). New York, NY: ACM.
*   Lee, Y., Kozer, K. A. &Larsen, K. R. T. (2003). The technology acceptance model: past, present, and future. _Communications of the Association for Information Systems, 12_(50), 752-780.
*   Legris, P., Ingham, J. &Collerette, P. (2003). Why do people use information technology? A critical review of the technology acceptance model. _Information & Management, 40_(3), 191-204.
*   Lenhart, A. & Madden, M. (2007). [Teens, privacy & online social networks: how teens manage their online identities and personal information in the age of MySpace.](http://www.webcitation.org/6bZUVPknK) Washington, DC: Pew Internet & American Life Project. Retrieved from http://www.pewinternet.org/Reports/2007/Teens-Privacy-and-Online-Social-Networks.aspx (Archived by WebCite® at http://www.webcitation.org/6bZUVPknK)
*   Magni, M., Provera, B. & Proserpio, L. (2010). Individual attitude toward improvisation in information systems development. _Behavior & Information Technology, 29_(3), 245-255.
*   Marshall, C. C. (2008). [Rethinking personal digital archiving. Part 1: four challenges from the field.](http://www.webcitation.org/6bZUeRcxt) _D-Lib Magazine, 14_(3/4). Retrieved from http://www.dlib.org/dlib/march08/marshall/03marshall-pt1.html (Archived by WebCite® at http://www.webcitation.org/6bZUeRcxt)
*   Mazman, S. G. & Usluel, Y. K. (2010). Modeling educational usage of Facebook. _Computers & Education, 55_(2), 444-453.
*   Orlikowski, W. J. (1992). The duality of technology: rethinking the concept of technology in organization. _organization Science, 3_(3), 398-427.
*   Orlikowski, W. J. (2000). Using technology and constituting structures: a practice lens for studying technology in organizations. _organization Science, 11_(4), 404–428.
*   Pempek, T. A., Yermolayeva, Y. A. & Calvert, S. L. (2009). College students' social networking experiences on Facebook. _Journal of Applied Developmental Psychology, 30_(3), 227–238.
*   Pinho, J.C.M.R. & Soares, A.M. (2011). Examining the technology acceptance model in the adoption of social networks. _Journal of Research in Interactive Marketing, 5_(2/3), 116-129.
*   Rogers, E. M. (1995). _Diffusion of innovations_ (4th Ed.). New York, NY: The Free Press.
*   Saeed, K. A. & Abdinnour, S. (2008). Examining the effects of information system characteristics and perceived usefulness on post adoption usage of information systems. _Information & Management, 45_(6), 376-386.
*   Saeed, K. A. & Abdinnour, S. (2013). Understanding post-adoption IS usage stages: an empirical assessment of self-service information systems. _Information Systems Journal, 23_(3), 219-244.
*   Saga, V. & Zmud, R. W. (1994). The nature and determinants of IT acceptance, routinisation, and infusion. In Levine, L. (Ed.), _Diffusion, transfer, and implementation of information technology_ (pp. 67-86). New York, NY: North-Holland.
*   Sharma, P. N. & Kim, K. H. (2012). [Model selecting in information systems research using partial least squares based structural equation modeling.](http://www.webcitation.org/6ccSHns1E) In M-H Huang, G. Piccoli & V. Sambamurthy, (Eds.), _Proceedings of the 33rd International Conference of Information Systems, Orlando, FL, December 16-19, 2012._. Atlanta, GA: Association for Information Systems, AIS Electronic Library. Retrieved from http://udel.edu/~pnsharma/sharma_kim_model_selection.pdf (Archived by WebCite® at http://www.webcitation.org/6ccSHns1E)
*   Sinn, D. & Syn, S. Y. (2014). Personal documentation on a social network site: Facebook, a collection of moments from your life? _Archival Science, 14_(2), 95-124.
*   Sinn, D., Syn, S. Y. & Kim, S. (2011). Personal records on the web: who's in charge of archiving, Hotmail or archivists? _Library & Information Science Research, 33_(4), 320-330.
*   Venkatesh, V., Morris, M. G., Davis, G. B. & Davis, F. D. (2003). User acceptance of information technology: toward a unified view. _MIS Quarterly, 27_(3), 425-478.
*   Wang, W., Li, X. & Hsieh, J. P. (2013). The contingent effect of personal IT innovativeness and IT self-efficacy on innovative use of complex IT. _Behaviour & Information Technology, 32_(11), 1105-1124.
*   Wong, C. B. (2012). Facebook usage by small and medium-sised enterprise: the role of domain-specific innovativeness. _Global Journal of Computer Science and Technology, 12_(4), 53-59.
*   Wu, P. & Theng, Y. L. (2005). [Weblog archives: achieving the recordness of web archiving.](http://www.webcitation.org/6ccXrlsj1) In _Electronic Proceedings of the Ninth International Cultural Heritage Informatics Meeting (ICHIM 05 Paris, France, September 21-23, 2005.)_. Toronto, ON: Archives & Museum Informatics. Retrieved from http://www.archimuse.com/publishing/ichim05/Wu.pdf (Archived by WebCite® at http://www.webcitation.org/6ccXrlsj1)
*   Zhao, S., Grasmuck, S. & Martin, J. (2008). Identity construction on Facebook: digital empowerment in anchored relationships. _Computers in Human Behavior, 24_(5), 1816-1836.