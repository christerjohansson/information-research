##### [**Information Research, Volume 5 No. 4 July 2000**](http://InformationR.net/ir/index.html)  
_Are Schools Library Services equipped to survive in the Age of Information?_, by [David Streatfield](mailto:streatfield@compuserve.com) and Sharon Markless Location: http://InformationR.net/ir/5-4/paper84.html   © the authors, 2000\.  
Last updated: 26th June 2000

# Are Schools Library Services equipped to survive in the Age of Information?

#### [David Streatfield](mailto:streatfield@compuserve.com) and Sharon Markless  
Information Management Associates  
28 Albion Road, Twickenham TW2 6QZ

#### Abstract

> Describes the results of an investigation in the the factors that would affect the survival and growth of School Library Services in England and Wales, including funding, staffing and service priorities. The main area of weakness identified is that most Services are not well placed to demonstrate the effectiveness of their provision in relation to the current government's and schools' agendas.

## Introduction

The School Library Services Management project was conducted last year against a background of uncertainty about their financial future because of the impact of UK government 'Fair Funding' legislation. Expressed somewhat over-simply, the new regime opens up Schools Library Services (which are offered to most local authorities through their Libraries or Education services) to the risk of potentially survival-threatening 'service buy-back' decisions by individual schools.

The **aim** of the project, which was funded by the Library and Information Commission and supported by the Association of Senior Children's and Education Librarians, was to assess the key factors leading to the recent survival and growth of Services especially fully delegated ones, and the extent to which these and other factors are likely to apply in future.

The specific project **objectives** were to:

*   establish the policies and practices adopted by SLS managers and others over the past five years to ensure the survival and growth of Services, especially where these are fully delegated;
*   examine how and to what extent Services contribute to meeting Government education and libraries priorities;
*   gather views from key players (school managers, governors, elected members, local authority officers, representative parents) on critical success factors and strategies for the continued survival and growth of Services in the next five years;
*   share the project findings with SLS managers, policy makers and the wider library management community in the UK.

The project consisted of three main phases:

_Case studies in Schools Library Services_. A series of visits was made to a total of 15 Services in different parts of England and Wales to look at the provision and to conduct structured interviews with staff, governors, elected members, local authority officers and representative parents.

_National questionnaire survey_. A questionnaire survey was conducted across all public library authorities in England and Wales to establish which authorities were offering SLSs or buying into Services elsewhere. The questionnaires were addressed to the Service manager (if any). Questions covered the management links into the local authority, whether services had been delegated or devolved, staff issues including levels, roles and training, the services offered and to whom, collaboration and integration with other agencies, marketing of services, quality control and evaluation of provision, and future development priorities.

_Preliminary review of the issues_. An invitation seminar on Schools Library Services: Survival and Growth? was held in London in July 1999\. This seminar focussed on some key issues in managing Services (the strategic presence of the Service, the LEA agenda, monitoring and evaluation, keys to success and the main report messages).

## The project survey

The core of this project consisted of a series of case studies in a total of 15 Services in different parts of England and Wales, combined with a questionnaire survey of all 169 Library authorities in those countries. This report ([Markless & Streatfield, 2000](#refs)) concentrates on the results of the questionnaire survey, which was the most exhaustive and best-supported study of its kind. A total of 153 replies was received, giving an outstandingly good overall response rate of 90%.

Twenty-three replies identified Public Library authorities with no SLS and we also heard from 30 Services involved in joint arrangements with neighbouring authorities (including the lead authorities). The project team members based in two authorities responded at length in the form of management reports on their Services and four of the case study authorities were treated in a similar way. The responses below are based on the 99 replies to the full questionnaire, including two that were only providing limited loans and advice.

Most Services were managed by the Libraries Department (75 or 77%), with 14 falling within the ambit of the Education Department and 5 operating as Independent Business Units. There has been some debate about the relative merits of being based within Education or libraries/Leisure services. According to our case study and workshop informants, a place in Education provided a strong local authority focus (although changes in LEAs have weakened the links to schools) and makes keeping up with education initiatives easier. It could also provide easier (but not automatic) inclusion in the Education Development Plan, but might lead to being left out of national initiatives, since Services are inevitably a small constituent of the LEA.

On the other hand, being linked to Public Libraries was seen as offering the potential for stronger ICT support and sometimes hidden subsidies (less likely with the arrival of Fair Funding). This positioning also offers scope for 'joined up thinking' about lifelong learning and a stronger base for involvement in national initiatives - important now that public libraries are apparently 'fashionable'. It also offers automatic inclusion in the DCMS Annual Library Plan and, potentially, a strategic 'champion' at local authority senior management level. The downside may include less involvement in educational strategies over literacy and numeracy and remoteness from 'price setting' of provision to schools.

In the end, however, "It doesn't matter if you are in Libraries or Education. What matters is that you have proactive people with good collaboration and communication skills."

### Funding and budget issues

Since Fair Funding is leading to delegated or devolved Services in England (there is no equivalent prescription in Wales), we were interested to see the extent to which this was already happening. Predictably, the extent of 'decentralization' of funds and the preferred form of reallocation varied amongst respondents and between school types:

<table><caption>Table 1: Delegated, devolved or centrally-funded Services</caption>

<tbody>

<tr>

<td> </td>

<th>Delegated</th>

<th>Partially delegated</th>

<th>Devolved</th>

<th>Centrally funded</th>

<th>Totals</th>

</tr>

<tr>

<th>Primary</th>

<td>24</td>

<td>18</td>

<td>5</td>

<td>42</td>

<td>89</td>

</tr>

<tr>

<th>Secondary</th>

<td>36</td>

<td>16</td>

<td>3</td>

<td>27</td>

<td>82</td>

</tr>

<tr>

<th>Special</th>

<td>24</td>

<td>18</td>

<td>5</td>

<td>44</td>

<td>91</td>

</tr>

</tbody>

</table>

The 'preferred' form of funding varied significantly between the different types of authority, with Counties and London boroughs tending towards full or partial delegation, Unitary authorities balancing between partial delegation and central funding and Metropolitan boroughs still predominantly centralised.

How successful were Services in recovering wholly or partially delegated funds through subscription and other buy-back arrangements? Responses varied widely, with an average recovery of just over 80% for the 37 SLSs (out of 52) who gave the figure for the previous year.

At the time of the survey, many Services were struggling with questions about the Fair Funding regime to be introduced in April 2000\. For many, the main issues were about setting an appropriate budget and, in particular, how to influence the allocation formula.

### Strategic presence of the SLS

Looking outwards from the immediate service demands, we wanted to know if people were actively involved in any national, regional or local initiatives or special projects over and above their normal work. A range of local initiatives (from homework clubs to local implementation of the National Literacy Strategy and from lifelong learning steering groups to local competitions - 33 replies or 34%) took pride of place and the other main preoccupation was with the National Year of Reading (27 responses) which was reaching its peak at the time of our survey; in addition 14 Services were involved in the Bookstart campaign and 6 with the Reading Safari.

We asked whether the Service featured in the Education Development Plan of the authority and, if so, where it featured. 44 SLSs did not appear in the Plan or did not know if they had been included (although many Services will be included in the Annual Library Plan instead, or in addition).

### Staff change and adaptation

Had pressures on the Service led to reductions in staffing? We asked whether their had been any significant changes in the staffing levels over the past three years. Over half of the respondents were in 'steady state' for that period (43 or 44%) or had managed some increase (14); by contrast 26 people reported reductions in staffing over the previous three years and 11 had experienced staff loss or limitation upon local government reorganisation.

How were staff adjusting to change? When we asked about the main focuses for any training or development provided for SLS staff in the past three years and whether these training priorities were changing, the replies could be grouped into categories, as shown:

![Figure 1](../p84fig1.gif)

### Priorities and changes in provision

All respondents were asked to rank a set of 23 activities in terms of their importance for their Service (not necessarily how much time the activity took up) by ticking one box for each on a five-point scale.

<table><caption>Table 2: Services provided and their importance (n = 98)</caption>

<tbody>

<tr>

<th scope="col"> </th>

<th scope="col">Very Important</th>

<th scope="col"> </th>

<th scope="col"> </th>

<th scope="col"> </th>

<th scope="col">Not Important</th>

<th scope="col">Scores</th>

</tr>

<tr>

<th scope="row">Project loans made up on demand</th>

<td>82</td>

<td>5</td>

<td>3</td>

<td>0</td>

<td>0</td>

<td>169</td>

</tr>

<tr>

<th scope="row">Advisory services to schools</th>

<td>75</td>

<td>14</td>

<td>7</td>

<td>1</td>

<td>0</td>

<td>163</td>

</tr>

<tr>

<th scope="row">Loans to support literacy</th>

<td>59</td>

<td>18</td>

<td>11</td>

<td>1</td>

<td>0</td>

<td>135</td>

</tr>

<tr>

<th scope="row">Direct support for school librarians</th>

<td>52</td>

<td>17</td>

<td>13</td>

<td>6</td>

<td>2</td>

<td>111</td>

</tr>

<tr>

<th scope="row">Bulk/exchange loans - fiction</th>

<td>44</td>

<td>25</td>

<td>7</td>

<td>1</td>

<td>2</td>

<td>108</td>

</tr>

<tr>

<th scope="row">Designing and refurbishing school libraries</th>

<td>43</td>

<td>31</td>

<td>11</td>

<td>5</td>

<td>2</td>

<td>108</td>

</tr>

<tr>

<th scope="row">INSET courses for teachers</th>

<td>37</td>

<td>24</td>

<td>16</td>

<td>3</td>

<td>2</td>

<td>91</td>

</tr>

<tr>

<th scope="row">Group reading collections</th>

<td>39</td>

<td>14</td>

<td>11</td>

<td>5</td>

<td>5</td>

<td>77</td>

</tr>

<tr>

<th scope="row">Mobile visits</th>

<td>33</td>

<td>7</td>

<td>3</td>

<td>0</td>

<td>1</td>

<td>71</td>

</tr>

<tr>

<th scope="row">Promotion activities</th>

<td>28</td>

<td>28</td>

<td>25</td>

<td>9</td>

<td>2</td>

<td>71</td>

</tr>

<tr>

<th scope="row">Bulk/exchange loans - non-fiction</th>

<td>29</td>

<td>16</td>

<td>10</td>

<td>5</td>

<td>4</td>

<td>61</td>

</tr>

<tr>

<th scope="row">ICT training for school librarians</th>

<td>21</td>

<td>12</td>

<td>8</td>

<td>7</td>

<td>0</td>

<td>47</td>

</tr>

</tbody>

</table>

Various services were offered by only a minority of respondents but were rated highly by them. This can be seen by calculating the mean response score (dividing the total score by the number of respondents) for each service listed, which has the effect of excluding from the reckoning those who do not offer the service. Using this method of ranking, 'Mobile visits' rose to third place and 'Recording services' figured in ninth position in an otherwise unchanged list (see table 4.1 above).

Turning to the future, the services predicted as areas of enhanced importance were:

<table><caption>Table 3: Services likely to increase in importance in the near future (n = 99)</caption>

<tbody>

<tr>

<th>Services</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>ICT training for school librarians</td>

<td>44</td>

<td>44</td>

</tr>

<tr>

<td>Loans to support literacy</td>

<td>24</td>

<td>24</td>

</tr>

<tr>

<td>Advisory services to schools</td>

<td>16</td>

<td>16</td>

</tr>

<tr>

<td>Internet services/searching</td>

<td>15</td>

<td>15</td>

</tr>

<tr>

<td>INSET courses for teachers</td>

<td>12</td>

<td>12</td>

</tr>

<tr>

<td>Project loans made up on demand</td>

<td>10</td>

<td>10</td>

</tr>

<tr>

<td>Group reading collections</td>

<td>10</td>

<td>10</td>

</tr>

<tr>

<td>Promotional activities</td>

<td>7</td>

<td>7</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

### 'Trading' beyond local boundaries

Selling services to schools and other education providers in neighbouring local authorities has not yet taken off on a grand scale. 28 SLSs (29%) reported some provision to others (excluding joint arrangements).

### Monitoring and evaluation

What methods did people adopt to gather performance information about the SLS? Our summary of the replies is given in table 4:

<table><caption>Table 4: Methods used to gather performance information (n = 98)</caption>

<tbody>

<tr>

<th>Service</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>Routine generation of performance statistics</td>

<td>83</td>

<td>85</td>

</tr>

<tr>

<td>Specific collection of performance statistics</td>

<td>64</td>

<td>65</td>

</tr>

<tr>

<td>User satisfaction surveys</td>

<td>61</td>

<td>62</td>

</tr>

<tr>

<td>Systematic logging of user comments</td>

<td>56</td>

<td>57</td>

</tr>

<tr>

<td>Focus groups to evaluate response to services</td>

<td>25</td>

<td>26</td>

</tr>

<tr>

<td>User satisfaction interviews</td>

<td>24</td>

<td>24</td>

</tr>

<tr>

<td>Benchmarking with other Services</td>

<td>18</td>

<td>18</td>

</tr>

<tr>

<td>Independently conducted user satisfaction survey</td>

<td>16</td>

<td>16</td>

</tr>

<tr>

<td>Formal groups to evaluate response to Services</td>

<td>13</td>

<td>13</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

Traditional library monitoring is well to the fore in these replies with performance statistics and logging of user comments, whilst user satisfaction surveys are also regularly employed. Only a quarter of Services had indulged in the ubiquitous focus group. However, when asked which of these methods provided the best quality evidence for assessing provision, a different picture began to emerge:

<table><caption>Table 5: Methods giving the best quality evidence (n = 88)</caption>

<tbody>

<tr>

<th>Method</th>

<th>No.</th>

<th>%</th>

<th>%  
of those  
using  
this method</th>

</tr>

<tr>

<td>User satisfaction surveys</td>

<td>29</td>

<td>33</td>

<td>48</td>

</tr>

<tr>

<td>Routine generation of performance statistics</td>

<td>16</td>

<td>18</td>

<td>19</td>

</tr>

<tr>

<td>User satisfaction interviews</td>

<td>15</td>

<td>17</td>

<td>63</td>

</tr>

<tr>

<td>Specific collection of performance statistics</td>

<td>14</td>

<td>16</td>

<td>22</td>

</tr>

<tr>

<td>Formal Service review groups</td>

<td>7</td>

<td>8</td>

<td>54</td>

</tr>

<tr>

<td>Focus groups to evaluate response to services</td>

<td>4</td>

<td>5</td>

<td>16</td>

</tr>

<tr>

<td>Benchmarking with other Services</td>

<td>4</td>

<td>5</td>

<td>22</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

### Keys to success

We asked about the three main success factors contributing to the success of the SLS over the previous three years. Again, we have summarised the main replies:

<table><caption>Table 6: Factors contributing to success (n = 99)</caption>

<tbody>

<tr>

<th>Factor</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>Professional expertise/understanding of education world</td>

<td>49</td>

<td>49</td>

</tr>

<tr>

<td>Quality of Service</td>

<td>35</td>

<td>35</td>

</tr>

<tr>

<td>Contact with/responsiveness to customers</td>

<td>27</td>

<td>27</td>

</tr>

<tr>

<td>Flexibility and commitment - staff</td>

<td>15</td>

<td>15</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

These success factors were in strong accord with the views expressed in the case study interviews, where emphasis was placed on really listening to the customers, resulting in continuous adaptation or adjustment of the provision, and requiring constantly evolving skills (do library skills matter in this changing environment?).

## Conclusion

Perhaps the most worrying aspect of this survey came in the replies from the 23 authorities where there is no current SLS. Any prospect of redressing this form of school deprivation is being dissipated by implementation of Fair Funding, since the schools in areas without an SLS will not be beneficiaries of devolved or delegated funds. In effect, these schools will be doubly deprived, since they will not be in a position to make positive choices about resourcing teaching and learning.

The main area of current weakness identified in this survey is that most Services are not well placed to demonstrate the effectiveness of their provision in relation to the current government and schools agenda. What evidence can the SLS offer that they are contributing to (or, in Ofsted-speak, 'bearing upon') school improvement, enhancement of student learning and social inclusion. Or, in terms of the local authority agenda, what does the SLS contribute to economic regeneration or to improved citizen access to electronic information, and how are the Welsh and Scottish Assemblies affecting priorities? Our work in exploring impact and achievement indicators (as distinct from, but aligned with, performance indicators), should help to provide a way forward here. If and when this type of evidence becomes routinely available, it will be possible to look objectively at the difference between schools receiving services from an SLS and those who choose not to do so or where there is no service on offer.

Overall, Services are proving remarkably resilient, given the recent history of local government cuts. There is clearly plenty of determination to keep on succeeding and, for the most part, realism about what is required to adapt to the increasingly turbulent environment. We were, however, concerned that some Services were slow to realise the extent of change likely after Fair Funding is fully introduced.

Success in the next few years is likely to depend upon more active promotion and marketing of Services and a willingness to adapt both provision and staff roles to meet changing demand.

Again, the evidence from this survey suggests that these issues are well recognised and that steps are being taken to 'gear up' in these areas. Looking further ahead, it seems likely that ICT will begin to have the major effect on teaching and learning that has regularly been predicted over the past twenty years. If schools do begin to change it will be essential for Services to respond. It remains to be seen how adaptable Services will be when the Age of Information finally arrives.

## Acknowledgements

Our thanks are offered to all the SLS Managers and other Librarians who responded to the survey at an especially challenging time. We would particularly like to thank three practising SLS Managers - Rachel Boyd (Somerset), John Dunne (Hampshire) and Pam Dix (Islington) - who worked as part of the project team, as well as our IMA colleagues, Alec Williams, Ray Swan and Greg Jefferies for all their help and support.

## Reference

*   <a id="refs"></a>Markless, S. & Streatfield, D.R. (2000) _Schools Library Services: from survival to growth?_ Library and Information Commission report no 49 Twickenham: Information Management Associates 2000, price £10 ISBN: 0-9538432-3-8