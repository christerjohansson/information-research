<header>

#### vol. 19 no. 1, March, 2014

</header>

<article>

# Proposed model of information behaviour in crisis: the case of Hurricane Sandy

#### [Irene Lopatovska](#author)  
School of Library and Information Science, Pratt Institute, 144 w. 14th street, 6th floor, New York, NY 10011-7301

#### [Bobby Smiley](#author)  
Michigan State University Libraries, 366 West Circle Drive, Room WG-1A, East Lansing, Michigan 48824

#### Abstract

> **Introduction.** The paper proposes a model of information behaviour in crisis. No previous model has attempted to integrate information resources, information behaviour and needs of the storm-affected communities within the temporal stages of a natural disaster.  
> **Method.** The study was designed as autoethnography. The data were collected through a diary of author-participant personal experiences and observations conducted prior, during and after Hurricane Sandy in the New York City area in October 2012.  
> **Analysis.** Using content analysis technique, the data were analysed for the presence of themes related to information needs and behaviour of a storm-affected community as well as available information sources.  
> **Results.** Based on the analysis of scholarly literature in the emerging field of crisis informatics and data collected during the hurricane, the paper proposes a framework for classifying information needs, information sources, and information behaviour during a crisis.  
> **Conclusions.** The proposed model enables critical examination of extraordinary situations, revealing strengths and weaknesses of current information infrastructure, and increasing our understanding of information needs and behaviour in emergency situations.

<section>

## Introduction

While a large number of studies have described information behaviour in crisis situations, few have attempted to develop a general classification of information needs, sources, and behaviour in these situations. The development and use of a classification could furnish scholars who study information seeking behaviour or crisis situations with an analytical framework within which to work.

The paper first reviews current work in the field of crisis informatics, an emerging field that focuses on information behaviour in natural and man-made emergency situations. Then, it describes a study that used autoethnographic methods to collect data on the information behaviour of inhabitants of a New York City area before and after Hurricane Sandy hit the region. The paper presents an analysis of the data that supports the use of the modified hierarchy of needs model ([Maslow 1943](#masl)) and a framework of socio-temporal stages of disaster ([Dynes, 1970](#dyne)) as ways to classify information needs, sources and behaviour in crisis. The critique of the model and directions for the future work are also discussed. The proposed model is one of the many possible lenses to examine crisis information behavior and is a first theoretical step in attempting to wed crisis informatics and autoethnography to traditional models of individual needs.

## Crisis informatics

The scholarly literature surveying how information professionals have addressed emergency situations is small but growing. Historically, library and information studies research has been concerned with institutional disaster preparedness in emergency situations rather than examining the dynamics and dissemination of information during crises ([Putnam, 2002](#putn); [Hagar, 2010](#hag10)). More recently, however, scholars have been investigating emergencies from an information communication perspective in the nascent, interdisciplinary field of crisis informatics. For Hagar ([2006](#hag06)), who is credited with coining the term _crisis informatics_, this area of research studies, the interconnectedness of people, organizations, information and technology during crises, which includes the important human ways of communicating and disseminating information that play a key role when technology infrastructure breaks down during a crisis ([Hagar, 2006, p. 10](#hag06)). Crisis informatics examines the challenges presented by emergencies in the communication of information: information overload and/or paucity, variable information needs, actors and agencies involved, integration and coordination between and among those actors, formal and informal channels of communication, information uncertainty, trustworthiness, conflicting sources, and providing the right information to the right parties when needed. Most research in this area has principally clustered around two areas: theoretical expositions ([Muhren and Van de Walle, 2010](#muhr); [Zach and McKnight, 2010](#zach)), or case-studies involving natural disasters such as fire, flood, or earthquakes ([Shklovski, Palen and Sutton, 2008](#shkl); [Bunce, Partridge and Davis, 2012](#bunc); [Dabner, 2012](#dabn)), political crises ([Shaheen, 2008](#shah)), terrorist attacks ([Fu, 2011](#fu)), or epidemiological crises ([Wan, 2010](#wan)). Due to our study's focus on information behaviour in crisis, we chose to review a segment of a growing body of emergency management scholarship that focused on crisis information behaviour. The reviewed studies helped us to understand and theorize about digital convergence and disasters in a manner not ordinarily employed in library and information studies information behaviour research.

In the individual emergency situations studied, recent scholarly literature in crisis informatics has emphasized the importance of two disparate, but interconnected trends: a shift from top-down government dissemination of information toward a bottom-up approach, where reporting is community based and citizen driven ([Hagar, 2010](#hag10); [Shklovski, _et al._, 2008](#shkl)), and the growing use of social media (in particular, Twitter and Facebook) to transmit relevant information and update communities during a disaster ([Shaheen, 2008](#shah); [Palen, Starbird, Vieweg and Hughes, 2010](#pale); [Bunce _et al._, 2012](#bunc); [Dabner, 2012](#dabn)). Implicit in these trends is the question of how information is regulated or mediated, and how this is conditioned not only by the nature of the crisis, but also by the technology available to broadcast that information to the stakeholders involved. As Shaheen ([2008](#shah)) and Wan ([2010](#wan)) have argued, exogenous factors, in particular government censorship, have complicated information flows during emergencies, irrespective of available technology. When community access to social media was blocked, citizens had to rely on government information. This control over the available information eroded citizens' trust in the government's ability to deal with the crises, and in some cases led to behaviour that actually exacerbated the situations. If government censorship is not an issue, official information complements, but does not usurp, the role of information generated through bottom-up community efforts. As Palen _et al._ ([2010](#pale)) explained after studying the tweets produced during the 2009 flooding of the Red River in North Dakota and Minnesota, while eyewitness accounts provided through social media were important, data from official sources were still actively sought. These data were made useful and locally relevant through active manipulations by social media participants who added context, supported or refuted it, or created new representations of and new channels of distribution for that information. The authors of this study stress that alignment of informal and formal sources of information is necessary in the future ([Palen _et al._, 2010, p. 16](#pale)).

On the informal, or bottom-up side, citizen reliance on social media has increasingly become an integral element in tracking the flow of information during emergency situations. Studying locally generated social media use during the 2011 Queensland flood in Australia, Bunce _et al._ ([2012](#bunc)) have argued that citizen information seeking behaviour could be grouped into four broad categories: monitoring information (oversight of the actual dissemination of information), community and communication (keep the stakeholders affected apprised of the latest information), affirmation (confirming the safety and survival of those affected), and awareness (providing warnings or alerting the population about effects to assist). An example of how these categories are reflected in crisis situation, in which both formal and informal sources are aligned, was provided during 2010 earthquake in New Zealand ([Dabner 2012](#dabn)). Shortly after the initial earthquake, the University of Canterbury in Christchurch started to use both the university's Web-based environments and Facebook as the two most prominent platforms to transmit information to students, with the latter also allowing them to contribute or correct information about the crisis. Despite manifold aftershocks, the university managed to keep their servers running and community apprised by using student-generated status updates and similar social media functions to report information back to the community at large.

As University of Canterbury officials discovered, one of besetting problems associated with relying on social media is maintaining and sustaining the available technology. In places where such technology is unavailable, communities can turn to analog methods of information transmission. In December 2004, the Indian Ocean earthquake caused a tsunami that devastated Aceh, the western most province in Indonesia. The damage to the communication infrastructure there isolated the most devastated areas. Given the terrain and the absence of widely available cellular voice and data services, relief organizations built community radio stations to help people access information, which became the frontline source for those affected ([Birowo, 2010](#biro)). The geography, the wholesale nature of devastation, and limited access to cellular communication informed the relative success of community radio as the primary channel for information in Indonesia, but relying on a single communication node also can present acute problems. Spatial dependency, where a single channel is the sole mode of updating affected populations and transmitting information, can complicate relief efforts. Following the attacks on the World Trade Center, New York City, in 2001, police and firefighter rescue efforts were hampered by reliance on two-way radios ([Fu, 2011](#fu)). Because of the volume of use, the broadcast channels were overwhelmed, with no back-up or alternative modes of communication available (eventually, _ad hoc_ communication alternatives were used: namely, the Internet). Spatial dependency accents the importance of having multiple communication nodes (radios, Internet, social media, other analog media), and robust interoperable networks to maintain contact between and among those organizing relief efforts and the affected populations.

Very few of the reviewed studies attempted to classify observed behaviour and information resources used in crisis situations. One that referenced classifications of individual and community behaviour during crisis is the work of Shklovski _et al._ ([2008](#shkl)). While the authors primarily focused on examining innovative uses of the information and communication technology during California wildfires in 2007, the authors relied on the notion of altruistic community used in the crisis sociology ([Barton, 1970](#bart)) and behavioural changes associated with the eight socio-temporal stages of disaster ([Dynes 1970](#dyne)). In surveying the literature of crisis informatics, researchers can begin to ground their analysis of information seeking behaviour in emergency situations by emphasizing how these two areas are intricately conjoined. Ultimately, the insights gleaned from research in crisis informatics provide scholars with robustly theoretical understanding enabling the construction of new models to explain information seeking behaviour.

While the devastation wrought by Hurricane Sandy on the northeastern part of the United States might be considered less dramatic than some of the above described catastrophes, it provided event-driven data for expending previous research on crisis-affected communities and their information behaviour. Using a grounded theory approach ([Glaser, 1992](#glas)), we identified information needs, behaviour, and sources associated with various temporal stages of the natural disaster, and integrated them into a proposed model of crisis information behaviour.

## Method

While information science research developed a large number of models explaining human information behaviour ([Zipf. 1949](#zipf); [Kuhlthau. 1994](#kuhl); [Wilson. 1999](#wils)), as far as we know, no particular model focuses on information behaviour in crisis situations. As a way of thinking about a problem ([Wilson, 1999](#wils)), such a model could help organize existing crisis informatics knowledge, describe information needs during crisis and inform improvements to the currently available information solutions during a crisis.

The aim of our study was to explore information behaviour affected by Hurricane Sandy and identify contextual and behavioural facets specific to crisis situations. The study relied on autoethnography method ([Chang, 2008](#chan)) to analyse personal experiences and participant observations in order to develop an understanding of the information needs and behaviour of a storm-affected community, as well as information sources available prior, during and after the storm. The participant observation and experiential data were recorded by one of the researchers in a diary that was kept for ten days before and after the hurricane, between October 27  November 6, 2012\. The secondary data included the analysis of the media coverage of the storm-related events.

By collecting real-time data, comparing it with the data from external sources, and providing details about the environment, researcher, data collection and analysis steps, the study aimed to satisfy the four criteria of the soundness of qualitative research: credibility, transferability, dependability and confirmability ([Lincoln and Guba 1985](#linc)).

### Diary

A total of fourteen pages (6,301 words) of the diary notes were collected by a researcher-participant. The diary data were structured into four rubrics: date and time related to storm progression, personal experiences, observations, and information obtained through external sources.

Participant observation notes became part of a diary. Being in this storm-affected area provided the researcher with an opportunity to engage in the daily practices of the affected community and to glean an understanding of its culture and behaviour ([Howell 1972](#howe)). While participant observation is not a very popular method in library and information studies research, it has previously been used in several studies (the work of Chatman ([1999](#chat)), the study of faculty members ([Westbrook, 1995](#west)), young public library visitors ([McKechnie, 1996](#mck)), including several crisis informatics studies in which researchers were active participants of the unfolding events ([Shklovski _et al._, 2008](#shkl); [Dabner, 2012](#dabn)), and was incorporated into the research diary.

The initial coding scheme for the diary entries included three basic categories that mirror the basic language structure and are implicitly or explicitly present in most information behaviour models: 1) information need (subject), 2) information source (object) and 3) information behaviour (action). Table 1 provides examples of the diary entries in each of the analysed diary rubric. After the initial round of coding, additional subcategories of themes have emerged and are presented in the data analysis section.

<table><caption>Table 1: Diary rubrics and initial coding themes</caption>

<tbody>

<tr>

<th>Diary rubric</th>

<th>Example of a diary entry</th>

</tr>

<tr>

<td>Date/time related to storm progression</td>

<td>Sun, 10/28/12, 1 day (25hr) before estimated Sandy's landfall on NJ shore</td>

</tr>

<tr>

<td>Personal experience</td>

<td>Just hooked up the [gas] generator. Happy to have enough power for the frig, phones and computer.  
Kids saw images of the storm devastation. Scared. [Parents] need to research how to properly handle the situation and minimize the psych[ological] damage.</td>

</tr>

<tr>

<td>Observation</td>

<td>Waited for 4.5hr in a line for gas, at least 70 people with canisters (between 2 to 5 canisters per person)  
Only two businesses are open in town: grocery store (sell non-perishables items, crackers, toilet paper), and a Chinese restaurant ([they] have a generator).</td>

</tr>

<tr>

<td>External information</td>

<td>Found useful updates on the progress of power repairs on the [neighboring town] [Web]site; checked ours [town Website]  nothing useful  
Local NBC reports on the cases of fraud related to FEMA support</td>

</tr>

<tr>

<th>Initial coding theme</th>

<td> </td>

</tr>

<tr>

<td>Information need</td>

<td>had to call 10 stores in PA [Pennsylvania] today to find who sells gas cans  
were thinking about hosting a family who lost their house, especially family with kids. Cannot find anything on the web, mostly sites for general donations</td>

</tr>

<tr>

<td>Information behaviour</td>

<td>walked to town to see what's happening  
searched Twitter for useful posts on the open gas stations</td>

</tr>

<tr>

<td>Information source</td>

<td>[neighbor] said that he was driving around for 2 hours and did not see a single open gas station  
JCPNL[electric company] reports 600 more people [in town ] getting power since yesterday  
[employer company] sent out a list of opened gas stations.</td>

</tr>

</tbody>

</table>

### Participant characteristics

While participant observations and personal experiences can represent experiences and views of a certain population, they are not generalizable to the whole population affected by the crisis being studied. The following characteristics of a researcher influenced her behaviour, experiences and observations of the hurricane:

*   _Physical and financial resources and mobility_  

    Participant had adequate resources for weathering the storm, including a house in a safe flood-free zone, sufficient funds to escape from the storm-affected area if necessary, and means of personal transportation (a car). If any of these resources were unavailable, the participant experiences would have been significantly different. (There is an abundance of stories about storm survivors whose choices for riding out the storm were constrained by limited resources ([Otterman, 2012](#otte); [Semple and Goldstein, 2012](#semp)).
*   _Information literacy and access to information channels_  

    Participant was aware of, and actively consumed a number of information channels that provided diverse information about the storm, including the National Hurricane Center Website ([www.nhc.noaa.gov](http://www.nhc.noaa.gov)), federal and local government Websites, social media sites ([Facebook](http://facebook.com)), national and local news channels ([www.cnn.com](http://www.cnn.com)). Without knowledge of and access to the diversity of information sources, the participant would have more limited information for making important decisions during the storm.
*   _Prior knowledge/experiences_  

    Participant had prior experience living in a hurricane-affected area and had first-hand knowledge of the effects of a major storm (loss of electricity, closed roads, damaged property, etc.) Without having prior experiences, it would have been harder to evaluate the risks of some of the decisions made (e.g., a decision to stay in a storm-affected zone). However, in a number of observed cases, prior experiences offered a false sense of confidence and influenced some residents of affected states to stay in a dangerous zone. As one lower Manhattan resident opting not to evacuate declared in a _The New York Times_ interview, Last time [during Hurricane Irene in 2011] they made such a big hype out of it and nothing happened ([Buckley, 2012](#buck)).
*   _Level of social responsibility_  

    The sense of social responsibility for dependents (elderly, children, pets) influenced many important decisions made during Sandy. Indeed, stories after the storm reported that a few people remained with their pets in flooded homes; this reluctance to move also claimed the lives of some elderly residents ([Robbins, 2012](#rob); [Semple and Goldstein , 2012](#semp)). Researcher family included young and elderly dependents; this added a layer of complexity to information needs and decision making during the storm.

### Hurricane Sandy (October 22-29, 2012)

The National Hurricane Center report ([Blake _et al._, 2013](#blak)) characterized Sandy as a classic late-season hurricane; that originated in the southwestern Caribbean Sea and made its first landfall in Jamaica as a category 1 hurricane. As it moved northeastward parallel to the coast of the southeastern United States, the hurricane system weakened and was downgraded to a post-tropical cyclone when it made a landfall on the coast of New Jersey. Due to the rise in sea level, strong winds and rains, Sandy caused major devastation to the New Jersey and New York coastlines and neighbouring areas.

Preliminary U.S. damage estimates are near $50 billion, making Sandy the second-costliest cyclone to hit the United States since 1901\. There were at least 147 direct deaths, 2 recorded across the Atlantic basin due to Sandy, with 72 of these fatalities occurring in the mid-Atlantic and northeastern United States. This is the greatest number of U.S. direct fatalities related to a tropical cyclone outside of the southern states since Hurricane Agnes in 1972 ([Blake, Kimberlain, Berg, Cangialosi and Beven, 2013, p. 1](#blak)).

Prior to landfall, some residents were required to evacuate, spending the duration of the storm in the shelters, hotels, or with relatives. The total number of evacuees included 375,000 in New York City, 50,000 in Delaware, and 30,000 in Atlantic City, New Jersey ([NBCNews.com, 2012](#nbc)). Some people chose to stay in the dangerous zones due to work requirements (news reporters), while others remained owing to an inability or unwillingness to leave their homes ([Semple and Goldstein, 2012](#semp)). After the landfall in New Jersey, about 8.1 million homes in New Jersey, New York and Connecticut, including the participant's, lost power ([Sheppard and DiSavino, 2012](#shep)).

## Findings

The analysis of the researcher's diary produced a number of themes that clustered around the four main categories: 1) temporal stage of disaster, 2) type of an information need, 3) information behaviour and 4) information field. The four categories and relationships among them formed a preliminary model of crisis information behaviour. The model also includes two contextual facets, such as disaster type and participant(s) demographic characteristics, which influence temporal states, information needs, behaviour and sources available during crisis (see Figure 1). This section reviews and discusses findings within the four categories of the proposed model.

### Temporal stage of disaster

An important determinant of the information behaviour during Sandy was the temporal stage of the hurricane. The time before during or after the hurricane correlated with availability of certain information channels and determined the information needs. We used a modified framework of socio-temporal stages of disaster ([Dynes 1970](#dyne)) as a coding scheme for the diary _date/time_ entries. In examining the changing focus from personal to communal interests during the disaster, Dynes ([1970](#dyne)) proposed an eight-stage model: pre-disaster, warning, threat, impact, inventory, rescue, remedy and recovery. We made the following changes to the Dynes' disaster stages to better represent our data: (a) we merged warning and threat stages since we did not note significant differences in information behaviour and available sources during these two stages; (b) since no rescue activities were experienced or recorded, the Dynes' recovery stage was removed; (c) remedy stage was renamed into survival. Figure 1 outlines the six stages of the modified Dynes' model used in the study: Pre-disaster, Warning/threat, Impact, Inventory, Survival and Recovery. Pre-disaster stage refers to a period prior to disaster awareness and is associated with individual's routine information behaviour. Inclusion of this stage in the model is necessary for understanding effects of routine behaviour on crisis behaviour. While we did not collect data during a pre-disaster stage, the researcher's profile offers insight into resources, habits, and other factors that influenced their information behaviour during crisis.

The diary entries started during the Warning/threat stage, which is characterized by the awareness of an imminence of an upcoming event and subsequent behavioural modifications. The duration of the Impact stage during Sandy was short and was associated with elevated senses of anticipation, alertness, and other intense emotions (not coded or represented in this model). After the hurricane passed, a few hours were spent assessing the damage (inspecting the property and immediate surroundings). The Survival phase refers to the period after the storm when the resources are the most limited and/or living conditions are the most different from the routine ones, and before a situation starts changing for the better (Recovery). The diary was kept for the four days of the Recovery stage that began with an attainment of some electric power (power generator) and ended with full restoration of electric power to the area. However, as some of the reviewed literature suggests, the physical and psychological recovery of many storm-affected individuals and communities is still ongoing ([Robbins, 2012](#rob); [Rosenberg, 2012](#rose)).

<figure>

![Model of information behaviour during Sandy](../p610fig1.jpg)

<figcaption>Figure 1: Model of information behaviour during Sandy</figcaption>

</figure>

### Information needs

We initially coded all words and phrases that referred to the information inadequacy in resolving uncertainty as _information need_. We later refined this coding scheme by using modified hierarchy of needs ([Maslow, 1943](#masl); see Figure 2) for coding specific themes within broader information needs category.

<figure>

![Maslow's hierarchy of needs](../p610fig2.png)

<figcaption>

Figure 2: Maslow's hierarchy of needs (Based on [Maslow, 1943](#masl))</figcaption>

</figure>

While previous crisis informatics research had proposed classifications of emergency information needs ([Bunce _et al._, 2012](#bunc)), these classifications did not accurately describe our data, especially in regards to esteem and self-actualization needs. The following list provides the types of information needs and their definitions, with examples from the diary.

*   _Physiological and safety needs:_ information about availability of resources to satisfy basic needs (shelter, food, energy)
    *   Stage 1 (Warning): _decided to stay in NJ [home] for the storm as they only forecast winds in our area. Still checking the NHC [National Hurricane Center] site for the updates, in case the forecast changes_.
    *   Stage 3 (Inventory): _walked around [the neighborhood] to assess the extent of the damage to our area_.
*   _Love, belonging, friendship, family_: information about family, friends, newly formed, temporary communities that cultivates a sense of belongingness
    *   Stage 3 (Inventory): _Could not reach [a friend] to see if they were OK_.
*   _Esteem:_ information that empowers, cultivates the sense of self-respect
    *   Stage 4 (Survival): _[spouse] just helped [a neighbour] to navigate through the generator manual and hook it up: everybody is happy!_
*   _Self-actualization_: information enabling one to fulfill her/his talents and potential through creativity, desire to contribute to society and other means
    *   Stage 5 (Recovery): _finally got in touch with [a work colleague] to check on the project status_.

Figure 1 provides frequencies of information need themes within each temporal stage of the hurricane. We have noticed two trends in our data:

1.  between the Warning stage and the Recovery stage, the types of information needs changed from predominantly individualistic (Physiological and Safety) to increasingly altruistic (Love, Esteem, Self-actualization), the trend that is consistent with the Dynes ([1970](#dyne)) findings;
2.  within Love, Esteem and Self-actualization categories, information needs were noted in the context of information seeking (need to obtain information) as well as information provision (need to share information).

### Information behaviour

The themes related to specific actions directed at obtaining information clustered in three categories:

*   Direct searching: a one-time search activity (or activities) directed at satisfying an information problem.  
    Examples: searching the internet for the hurricane-related charities; calling hardware stores to check availability of certain goods.
*   Monitoring: methodical searching related to the same problem, keeping up-to-date ([Ellis 1989](#elli))  
    Examples: monitoring progress of electric lines repairs through electric company Website, monitoring the progression of the hurricane on the National Hurricane Centre Website
*   Passive information consumption: obtaining information without actively seeking it.  
    Examples: receiving an email from an employer with the list of opened stores and gas stations; receiving telephone calls from the local municipal office with the updates on recovery efforts; receiving an update from the social media application.

While the Warning, Impact, and Recovery stages were characterized by the increase number of monitoring activities, the Inventory and Survival stages were associated with the high number of direct searches to solve immediate information problems.

### Information field

The concept of _information field_ refers to a range of information sources available to individual (or a group) ([Johnson _et al._, 2006](#john)) and represents a contextual variable that is necessary for understanding information behaviour. The most frequently mentioned information sources that comprised individual information field during the analysed timeframe included:

*   News media: newspapers and news aggregates, television news channels along with associated print versions, Web and social media sites (e.g., NBC, PBS).
*   Internet sites: Internet sites associated with non-news organizations (e.g. Red Cross, NHC sites).
*   Social media: "a group of Internet-based applications that build on the ideological and technological foundations of Web 2.0, and that allow the creation and exchange of User Generated Content" ([Kaplan and Haenlein, 2010, p. 61](#kapl)) (e.g., Facebook, Twitter).
*   Personal experiences: personal observations, conversations with community members.

The observed information sources could be further described in the light of the media Richness/Reach theory ([Evans and Wurster, 1997](#evan)). The concept of _reach_ refers to the number of people a channel is capable of reaching (e.g., a TV channel versus a phone call). Richness is defined by the channel bandwidth, degree of customization and interactivity. According to this theory, the larger the _reach_ potential of a channel, the more bandwidth it requires and the less customization and interactivity it can support. In our study, the examples of the _reach_ information sources were news media and Internet sites capable of reaching out a wide audience with non-customized information. These channels were largely sufficient to satisfy information needs during the Warning, Impact (partially) and Recovery stages. However, when the electric power became scarce, some _reach_ information channels (e.g. news media) became less available or less useful for satisfying information needs, while more customizable and interactive channels became more useful for information acquisition.

## Discussion

In this section we discuss storm-related information behaviour through the prism of the proposed model which helps to illustrate changes in the information needs, behaviour, and information field during the storm, as well as highlight the strength and weaknesses of the current information systems during Hurricane Sandy.

### Warning or threat stage

Before the storm, the most pressing information need related to the trajectory and severity of the expected storm, as this affected decisions about where to seek shelter during the storm, and whether that place was safe and was adequately supplied for a potentially prolonged stay (this trend is reflected in a high number of information queries related to physiological and safety needs). During the three days before the storm, we noted the decrease in work-related search activities (Self-actualization) and increase of searching related to Physiological and safety needs. Overall, this stage was characterized by the presence of a rich information field. We noted the abundance of storm-related information several days prior to its landing on the east coast of the US: even without proactively seeking information, it was impossible to avoid the news about the approaching _Frankenstorm_ transmitted on all information channels (radio, TV, print, Web and social media). Authoritative sources, like the National Hurricane Center and other federal Websites, had also provided current information about the storm and means to prepare for it ([National Weather Service, 2012](#nws); [American Red Cross, 2012](#arc); [Delaware, 2012](#dela); [Federal Emergency Management Agency, 2012](#fema)). We did not find any evidence of complaints about the quantity of pre-storm information. We did, however, find some criticism of the quality of this information. After the hurricane, many sources expressed concerns about National Hurricane Center downgrading Sandy from a hurricane to a post-tropical cyclone which in many cases lead to people perceiving Sandy as a less serious threat ([Santora, 2013](#sant)), and, in some cases, refusing to evacuate ([Buckley, 2012](#buck)).

### Impact stage

During the first hours of the hurricane, many residents of the storm-affected areas stayed in the safety of their private residences with access to their routine information channels. During this stage, information needs focused around safety of self and close family and friends, and were satisfied by monitoring surroundings, news media and Internet sites. For most of this stage, television news and other media outlets helped to visualize the effects of the various phases of the hurricane. However, with the storm's progression, electric lines sustained significant damage and with the loss of electric power, the information field of the residents in the affected areas changed as well: phones, television and Internet became either partially or completely unavailable. Even with electric power, some information channels became unreliable. For example, it was difficult to get connection on a mobile phone due to the loss of service capacity as a result of damage to cell towers of service providers; additionally, use of mobile and other portable devices was constrained by difficulties of powering these devices through publicly available back-up generators. Immediately following the storm, the most reliable information channels available to most of the power-less residents were personal observations and interactions with the members of local communities (e.g. neighbours, local government, and selected local businesses).

### Inventory stage

For a few hours right after the storm, it was important to assess the damage and resources available to cope with the post-storm consequences. The major source of information during this stage became personal experience and observations.

### Survival stage

The Survival stage was characterized by establishing new routines under new circumstances (e.g, physical isolation) and availability of limited resources (e.g., connectivity and electric power). During this stage, the majority of information needs were related to safety and satisfaction of physiological needs. A number of sources of information about food, shelter and energy were available to the residents of the storm-affected areas, including local and federal government Websites, social media ([Medley, 2012](#medl)) and news channels, corporate Websites and mailing lists. However, successful use of these sources depended on availability of Internet access, access to electric power to power Internet-enabled devices, and awareness of information sources (e.g. Twitter, Federal Emergency Management Agency Website). During this stage, much effort was invested in identifying the most relevant information sources. For example, every small town maintained their municipal Website that often provided information that was relevant to neighbouring communities but varied in quality, completeness, and accuracy. Similar to the findings of the 2009 Red River floods study ([Palen _et al._, 2010](#pale)), several Sandy victims observed in a study reported that it took them some time to identify the most informative microblogs in Twitter (e.g., following Newark Mayor Corey Booker's, or New Jersey Governor Chris Christie's tweets). A possible solution for providing relevant information to storm victims in a timely manner could be developed by aggregating up-to-date information from diverse channels into a single interface (Website, mobile application, telephone hotline, radio channel). Information about food, shelter and other basic supplies could be channeled from local resources (police, volunteers) as well as federal and non-profit resources (the Red Cross, Federal Emergency Management Agency) into a single point of access database that could ensure timely delivery of authoritative information (an Internet-enabled information equivalent of the 911 emergency phone service). Another possible way of improving existing distribution channels for crisis information can be achieved by proactively reaching out to users on all of their information channels, including land-line and mobile phones. For example, when the power was restored, the researcher discovered many voice message left by the municipal government on the landline phone which was inactive during the storm. (The storm made us realize that most of the phones connected to landlines rely on electricity. It is almost impossible to purchase a simple phone that charges from the phone line without requiring the power source.) Attempts to reach out to an individual through his/her mobile phone would be more successful but would imply that citizens shared various types of contact information with their local governments for emergency purposes.

Due to the inefficiency or limited accessibility of traditional information channels, personal experiences became the major source of information. Residents sought information through direct contact with their community (neighbours and friends), or walking and driving around the area in search of the functioning gas stations, stores, restaurants, and other types of services. While direct observation might not be considered the most efficient way to seek information, it was often the only way to obtain information as well as to reinforce the sense of belonging to a community.

### Recovery stage

During the Survival and the Recovery stages, we noted the increase in information needs related to Love-belonging-friendship, Esteem, and Self-actualization. The importance of satisfying emotional needs as well as physiological needs during crises was noted in prior research. In the study of information exchange during the 2011 Queensland flood in Australia, the authors identified the need for affirmation and the monitoring of information related to the well-being of the community ([Bunce _et al._, 2012](#bunc)). After Hurricane Sandy, a number of channels provided community-related information (news media) and supported newly-created communities of storm victims and volunteers (for instance, the Rockaways community used Facebook and Twitter to organize relief and clean-up efforts ([Parashar, 2012](#para)). However, there was also a shortage of information about storm victims and their needs, which motivated many volunteers to take an initiative and come to the front line of storm-affected areas with food, power generators, clothing, and other necessary items ([Rampell, 2012](#ramp)). An informal list where participants could exchange information about available resources (offers of shelter, food) and disaster victims; needs would support a sense of community exhibited after Sandy. We were able to find several examples of systems that supported community information exchange (e.g., Websites like [Family-to-Family](http://www.family-to-family.org/) and [Hurricanesandyadopt4christmas](http://hurricanesandyadopt4christmas.wordpress.com/)). However, the fact that we only found these sites several weeks after the storm emphasizes the need for better linkage of similar resources or a more centralized, easy-to-find system of all emergency-related information.

With the loss of the means to provide for oneself and others, many storm victims experienced the loss of a sense of independence and self-esteem. Provision of information for satisfying basic needs became a critical requirement for maintaining self-esteem. After the storm, vital information about resources available to storm victims was scattered across multiple platforms and sites. Many victims expressed concerns about the difficulty of locating or understanding available information about immediate monetary and other type of assistance. In some instances, Federal Emergency Management Agency mobile disaster-recovery stations were inadequately outfitted, or ill-equipped to assist storm victims, who expressed frustration and dispiritedness ([Rosenberg, 2012](#rose)). In other instances, the information that was readily available was not accurate (to obviate problems arising from misinformation, the Federal Emergency Management Agency provided a Sandy Rumor Control Website ([http://www.fema.gov/hurricane-sandy-rumor-control](http://www.fema.gov/hurricane-sandy-rumor-control)), which went live shortly after a man began falsely tweeting about the flooding of the New York Stock Exchange ([Gross, 2012](#gros)). Future development of emergency information systems should be guided by understanding that information provision is not only vital for one's physiological well-being, but also for individual's sense of pride and self-esteem.

The post-hurricane environment provided opportunities for storm-affected and unaffected citizens to realize their potential on professional, personal, and community levels: from individuals rescuing strangers from cars caught in the flooding during the storm, or distributing flashlights to residents without power ([Medina, 2012](#medi)). For many, the Internet offered a number of channels (in particular Twitter) to share information and provide emotional support to those in need ([Guskin and Hitlin, 2012](#gusk)). Many residents in areas that lost power and access to most of their information technology had no choice but to spend more time with their family and close community ([Rowan, 2012](#rowa)). As an unusual example of a positive externality, the Sandy aftermath offered a chance to re-evaluate priorities and focus on important relationships, whether between spouses, or among parents and children ([Starr, 2012](#star); [Wetzel, 2012](#wetz); [Turkle, 2011](#turk)).

## Conclusion

Being in the area affected by Hurricane Sandy allowed researchers to collect real-time data on information needs, behaviour, and resources available to the storm-affected community. Autoethnography method enabled researchers to collect observational and experiential data as the storm-related events were unfolding; together with the secondary data gathered from the external sources (e.g., news media) and crisis informatics literature, this data informed the development of a model of information behaviour during Sandy.

By mapping temporal stages of a disaster, types of information needs, information behaviour, and information field, the model advances our thinking about information behaviour in crisis situations and helps to identify gaps in the current emergency information infrastructure, e.g., decentralized and disconnected information sources, high dependency on electrical power, lack of flexibility in events planning.

The most important study findings include:

*   evidence of the importance of esteem and self-actualization needs in emergency situations;
*   shift from personal to community information needs during the period of recovery;
*   presence of a need to provide and share information, not just seek and consume it within the Love, Esteem and Self-actualization categories of information needs.

We noted a number of themes related to emotions experienced during the analysed timeframe (e.g., frustration, happiness, fright, sadness). Future work will include analysis of emotional states as they play an important role in influencing information behaviour during crisis.

## <a id="author"></a>About the authors

**Irene Lopatovska** is an Associate Professor in the School of Information and Library Science, Pratt Institute, New York, USA. She received her Ph.D. from Rutgers, the State University of New Jersey, USA. She can be contacted at: [ilopatov@pratt.edu](mailto:ilopatov@pratt.edu).  
**Bobby Smiley** received his Master of Library and Information Science from Pratt Institute and currently holds a position of the Digital Humanities and American History Librarian at Michigan State University Libraries. He can be contacted at: [bsmiley@msu.edu](mailto:bsmiley@msu.edu).

</section>

<section>

## References

<ul>
<li id="arc">American Red Cross (2012). <a href="http://www.webcitation.org/6MWliabkJ">Red Cross offers tips to prepare for Sandy in Connecticut</a>. Retrieved from http://www.redcross.org/news/article/Red-Cross-Offers-Tips-to-Prepare-for-Sandy-in-Connecticut (Archived by WebCite® at http://www.webcitation.org/6MWliabkJ).
</li>
<li id="bart">Barton, A.H. (1970). <em>Communities in disaster</em>. Garden City, NY: Anchor, Doubleday.
</li>
<li id="biro">Birowo, M.A. (2010). The use of community radio in managing natural disasters in Indonesia. <em>Bulletin of the American Society for Information Science and Technology, 36</em>(5), 18-21.
</li>
<li id="blak">Blake, E.S., Kimberlain, T.B., Berg, R.J., Cangialosi, J.P. &amp; Beven, J.L. (2013, February 12). <em><a href="http://www.webcitation.org/6MWlzUh88">Tropical cyclone report: Hurricane Sandy (AL182012): 22-29 October 2013</a></em>. Miami, FL: National Hurricane Center. Retrieved from http://www.nhc.noaa.gov/data/tcr/AL182012_Sandy.pdf (Archived by WebCite® at http://www.webcitation.org/6MWlzUh88).
</li>
<li id="buck">Buckley, C. (2012, October 29). <a href="http://www.webcitation.org/6MWm9PxLj">Mix of panic and nonchalance for millions in a storm's path.</a> <em>The New York Times</em>. Retrieved from http://www.nytimes.com/2012/10/29/nyregion/panicked-evacuations-mix-with-nonchalance-in-hurricane-sandys-path.html (Archived by WebCite® at http://www.webcitation.org/6MWm9PxLj).
</li>
<li id="bunc">Bunce, S., Partridge, H. and Davis, K. (2012). Exploring information experience using social media during the 2011 Queensland Floods: a pilot study. <em>Australian Library Journal, 6</em>(1), 34-45.
</li>
<li id="chat">Chatman, E.A. (1999). A theory of life in the round. <em>Journal of the American Society for Information Science, 50</em>(3), 207-217.
</li>
<li id="chan">Chang, H. (2008). <em>Autoethnography as method.</em> Walnut Creek, CA: Left Coast Press.
</li>
<li id="dabn">Dabner, N. (2012). Breaking ground in the use of social media: a case study of a university earthquake response to inform educational design with Facebook. <em>Internet and Higher Education, 15</em>(1), 69-78.
</li>
<li id="dela">Delaware. (2012, October 25). <em><a href="http://www.webcitation.org/6MWo4Qd1V">Prepare now for Hurricane Sandy</a></em>. Dover, DE: Delaware.gov. Retrieved from http://news.delaware.gov/2012/10/25/prepare-now-for-hurricane-sandy-2/ (Archived by WebCite® at http://www.webcitation.org/6MWo4Qd1V).
</li>
<li id="dyne">Dynes, R.R. (1970). <em>Organized behavior in disaster.</em> Lexington, MA: Heath-Lexington Books.
</li>
<li id="elli">Ellis, D. (1989). A behavioural approach to information retrieval design. <em>Journal of?</em><em>Documentation, 46</em>, 318-338.
</li>
<li id="evan">Evans, P.B. and Wurster, T.S. (1997). Strategy and the new economics of information. <em>Harvard Business Review, 75</em>(5), 70-82.
</li>
<li id="fema">Federal Emergency Management Agency. (2012). <a href="http://www.webcitation.org/6MWoNMtug"><em>Hurricane Sandy: rumor control</em></a>. Washington, DC: Federal Emergency Management Agency. Retrieved from http://www.fema.gov/hurricane-sandy-rumor-control. (Archived at http://www.webcitation.org/6MWoNMtug)
</li>
<li id="fema-tweet">Federal Emergency Management Agency. (2012, October 26). <a href="http://bit.ly/1hm4Qjo">#Sandy tip for east coast: get to the store today for emerg supplies. Water, nonperishable food, batteries, flashlight, etc.</a> [Tweet] Retrieved from http://bit.ly/1hm4Qjo
</li>
<li id="fu">Fu, L. (2011). The government response to 9/11: communications technology and the media. <em>Library and Archival Security, 24</em>(2), 103-118.
</li>
<li id="glas">Glaser, B. (1992). <em>Basics of grounded theory analysis</em>. Mill Valley, CA: Sociology Press.
</li>
<li id="gros">Gross, D. (2012, October 31). <a href="http://www.webcitation.org/6MWmNPvMs">Man faces fallout for spreading false Sandy reports on Twitter</a>. Retrieved from http://www.cnn.com/2012/10/31/tech/social-media/sandy-twitter-hoax (Archived by WebCite® at http://www.webcitation.org/6MWmNPvMs).
</li>
<li id="gusk">Guskin, E. &amp; Hitlin, P. (2012). <em><a href="http://www.webcitation.org/6MWmVAKMd">Hurricane Sandy and Twitter</a></em>. Washington, DC: Pew Research Center. Retrieved from http://www.journalism.org/index_report/hurricane_sandy_and_twitter (Archived by WebCite® at http://www.webcitation.org/6MWmVAKMd).
</li>
<li id="hag06">Hagar, C. (2006). <em>Using research to aid the design of a crisis information management course</em>. Paper presented at the ALISE SIG Multicultural, Ethnic and Humanistic Concerns (MEH) session on Information Seeking and Service Delivery for Communities in Disaster/Crisis, San Antonio, Texas.
</li>
<li id="hag10">Hagar, C. (2010). Introduction. <em>Bulletin of the American Society for Information Science and Technology, 36</em>(5), 10-12.
</li>
<li id="howe">Howell, Joseph T. (1972). <em>Hard living on Clay Street: portraits of blue collar families</em>. Prospect Heights, IL: Waveland Press, Inc.
</li>
<li id="john">Johnson, J.D., Case, D.O., Andrews, J., Allard, S.A. &amp; Johnson, N.E. (2006). Fields and pathways: contrasting or complementary views of information seeking. <em>Information Processing and Management, 26</em>(2), 569-582.
</li>
<li id="kapl">Kaplan, A.M. &amp; Haenlein, M. (2010) Users of the world, unite! The challenges and opportunities of social media. <em>Business Horizons, 53</em>(1), 59-68.
</li>
<li id="kuhl">Kuhlthau, C.C. (1994). <em>Seeking meaning: a process approach to library and information services</em>. Norwood, NJ., Ablex Publishing.
</li>
<li id="linc">Lincoln, Y.S. &amp; Guba, E. (1985). <em>Naturalistic enquiry.</em> Beverley Hills, CA: Sage
</li>
<li id="masl">Maslow, A.H. (1943). A theory of human motivation. <em>Psychological Review, 50</em>(4), 370-96.
</li>
<li id="medl">Medley, E. (2012, October 31). <a href="http://www.webcitation.org/6MWmgRRB5">Hurricane Sandy: use Twitter to find open gas stations and businesses</a>. <em>NJ.Com</em>. Retrieved from http://www.nj.com/news/index.ssf/2012/10/hurrican_sandy_use_twitter_to.html (Archived by WebCite® at http://www.webcitation.org/6MWmgRRB5).
</li>
<li id="mck">McKechnie, L. (2000). Ethnographic observation of preschool children. <em>Library and information science research, 22</em>(1), 61-76.
</li>
<li id="medi">Medina, S. (2013, October 30). <a href="http://www.webcitation.org/6MWmnHTSB">Meet the heroes of Hurricane Sandy.</a> <em>Huffington Post</em>. Retrieved from http://www.huffingtonpost.com/2012/10/30/the-heroes-of-hurricane-sandy_n_2044253.html#slide=1703775 (Archived by WebCite® at http://www.webcitation.org/6MWmnHTSB).
</li>
<li id="muhr">Muhren, W.J. &amp; Van de Walle, B. (2010). Sense-making and information management in emergency response. <em>Bulletin of the American Society for Information Science and Technology, 36</em>(5), 30-33.
</li>
<li id="nws">National Weather Service. <em>National Hurricane Center.</em> (2012). <em><a href="http://www.webcitation.org/6MWmsyUR1">Hurricane preparedness: be ready</a></em>.?Miami, FL: National Hurricane Center. Retrieved from http://www.nhc.noaa.gov/prepare/ready.php (Archived by WebCite® at http://www.webcitation.org/6MWmsyUR1).
</li>
<li id="otte">Otterman, S. (2012, November 3). <a href="http://www.webcitation.org/6MWmxzgVO">Relief efforts of all sizes and forms spring up across New York.</a> <em>The New York Times</em>. Retrieved from http://www.nytimes.com/2012/11/03/nyregion/relief-efforts-of-every-size-and-form-spring-up-in-new-york-city.html (Archived by WebCite® at http://www.webcitation.org/6MWmxzgVO).
</li>
<li id="pale">Palen, L., Starbird, K., Vieweg, S. &amp; Hughes, A. (2010). Twitter-based information distribution during the 2009 Red River Valley Flood Threat. <em>Bulletin of the American Society for Information Science and Technology, 36</em>(5), 13-17.
</li>
<li id="para">Parashar, A. (2012, November 9). <a href="http://www.webcitation.org/6MWn8CihH">Cleaning up Rockaway, bucket by bucket</a>. <em>National Public Radio</em>. Retrieved from http://www.npr.org/2012/11/09/164779040/cleaning-up-rockaway-bucket-by-bucket (Archived by WebCite® at http://www.webcitation.org/6MWn8CihH).
</li>
<li id="putn">Putnam, L. (2002, November 4). <a href="http://www.webcitation.org/6MYY9GqWx">By choice or by chance: How the internet is used to prepare for, manage, and share information about emergencies.</a> <em>First Monday, 7</em> (11), Retrieved from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1007/928 (Archived by WebCite® at http://www.webcitation.org/6MYY9GqWx).
</li>
<li id="ramp">Rampell, C. (2012, November 5). <a href="http://www.webcitation.org/6MWnH7Y7q">Volunteers and donations flock to areas affected by Hurricane Sandy.</a> <em>The New York Times.</em> Retrieved from http://www.nytimes.com/2012/11/05/nyregion/volunteers-and-donations-flock-to-areas-affected-by-hurricane-sandy.html (Archived by WebCite® at http://www.webcitation.org/6MWnH7Y7q).
</li>
<li id="rob">Robbins, L. (2012, November 19). <a href="http://www.webcitation.org/6MWnMXUpB">For pet owners left homeless by hurricane, a temporary home for the animals.</a> <em>The New York Times</em>. Retrieved from http://www.nytimes.com/2012/11/19/nyregion/temporary-shelter-opens-for-pets-of-owners-displaced-by-hurricane-sandy.html (Archived by WebCite® at http://www.webcitation.org/6MWnMXUpB).
</li>
<li id="rose">Rosenberg, A. (2012, November 4).? <a href="http://www.webcitation.org/6MWnRnLQt">At the Jersey Shore, tears and disbelief in the aftermath of Sandy.</a> <em>Philly.com</em>. Retrieved from http://articles.philly.com/2012-11-04/news/34930471_1_fema-flood-insurance-flood-damage (Archived by WebCite® at http://www.webcitation.org/6MWnRnLQt).
</li>
<li id="rowa">Rowan, T. (2012, November 17). <a href="http://www.webcitation.org/6MWnXnpHK">Lack of power in Superstorm Sandy's wake brings Stewartsville family together.</a> <em>LehighValleyLive.com</em> Retrieved fromhttp://www.lehighvalleylive.com/warren-county/express-times/index.ssf/2012/11/superstorm_sandy_brings_stewar.html (Archived by WebCite® at http://www.webcitation.org/6MWnXnpHK).
</li>
<li id="sant">Santora, M. (2013, February 12). <a href="http://www.webcitation.org/6MWnf318Z">Hurricane Center seeks expanded authority to issue warnings.</a> <em>The New York Times.</em> Retrieved from http://www.nytimes.com/2013/02/13/nyregion/reducing-hurricane-sandy-to-a-tropical-storm-undercut-warnings.html?_r=0 (Archived by WebCite® at http://www.webcitation.org/6MWnf318Z).
</li>
<li id="semp">Semple, K. &amp; Goldstein, J. (2012, November 11). <a href="http://www.webcitation.org/6MWnmAUnv">How a beach community became a deathtrap.</a> <em>The New York Times</em>, Retrieved from http://www.nytimes.com/2012/11/11/nyregion/how-a-staten-island-community-became-a-deathtrap.html (Archived by WebCite® at http://www.webcitation.org/6MWnmAUnv).
</li>
<li id="shah">Shaheen, M.A. (2008). Use of social networks and information seeking behaviour of students during political crises in Pakistan: a case study. <em>International Information and Library Review, 40</em>(3), 142-147.
</li>
<li id="shep">Sheppard, D. &amp; DiSavino, S. (2012, October 31). <a href="http://www.webcitation.org/6MWnsnc7T">Superstorm Sandy cuts power to 8.1 million homes.</a> <em>Reuters</em>. Retrieved from http://www.reuters.com/article/2012/10/30/us-storm-sandy-powercuts-idUSBRE89T10G20121030 (Archived by WebCite® at http://www.webcitation.org/6MWnsnc7T).
</li>
<li id="shkl">Shklovski, I., Palen, L. &amp; Sutton, J. (2008). Finding community through information and communication technology in disaster events. In <em>Proceedings of the 2008 ACM Conference on Computer Supported Cooperative Work</em> (CSCW '08), (pp. 127-136). New York, NY: ACM Press.
</li>
<li id="star">Starr, K. (2012, November 5). <a href="http://www.webcitation.org/6MWnzIAVI">Will Sandy bring a baby boom or baby bust? How disasters change marriage, divorce, and birth rates.</a> <em>Slate</em>. Retrieved from http://slate.me/1gzSO3V (Archived by WebCite® at http://www.webcitation.org/6MWnzIAVI).
</li>
<li id="nbc">
<a href="http://www.webcitation.org/6MYaBOewV">Superstorm Sandy by the numbers</a>. (2012, 29 October). <em>NBC News</em>. Retrieved from http://usnews.nbcnews.com/_news/2012/10/29/14777524-superstorm-sandy-by-the-numbers?lite (Archived by WebCite® at http://www.webcitation.org/6MYaBOewV).
</li>
<li id="turk">Turkle, S. (2011). <em>Alone together: why we expect more from technology and less from each other</em>.? New York, NY: Basic Books.
</li>
<li id="west">Westbrook, L. (1999). <em>Interdisciplinary information seeking in women's studies.</em> Jefferson, NC: McFarland and Company, Inc.
</li>
<li id="wan">Wan, M. (2010). Government information management during four emergencies in China. <em>Journal of Technology Management in China, 5</em>(5), 188-195.
</li>
<li id="wetz">Wetzel, N.A. (2012, November 30). <em><a href="http://www.webcitation.org/6MWoBMKPl">Hurricane Sandy: devastation and strength in families and communities</a></em> [Web log post]<em>.</em> Retrieved from http://www.princetonfamily.com/blog/post.php?s=2012-11-30-hurricane-sandy-devastation-and-strength-in-families-and-communities (Archived by WebCite® at http://www.webcitation.org/6MWoBMKPl).
</li>
<li id="wils">Wilson, T.D. (1999). Models in information behaviour research. <em>Journal of Documentation, 55</em>(3) 249?270.
</li>
<li id="zach">Zach, L. &amp; McKnight, M. (2010). Special services in special times: responding to changed information needs during and after community-based disasters. <em>Public Libraries, 49</em>(20), 37-43.
</li>
<li id="zipf">Zipf, G.K. (1949). <em>Human behavior and the principle of least effort: an introduction to human ecology.</em> Cambridge, MA: Addison-Wesley.
</li>
</ul>

</section>

</article>