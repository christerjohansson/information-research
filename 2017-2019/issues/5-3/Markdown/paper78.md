#### Information Research, Vol. 5 No. 3, April 2000

# The impact of personality and approaches to learning on information behaviour

#### [Jannica Heinström](mailto:jannica.heinstrom@abo.fi)  
Department of Information Studies  
Åbo Akademi University  
Henriksgatan 9, 20500 Åbo, Finland

#### Abstract

> Previous studies have shown how personality influences learning strategies and learning outcome. In this study this will be taken further by combining personality and approaches to learning with information behaviour. The aim of this study is to show how the five traits of the Five-Factor Inventory related to the approaches to learning of the ASSIST-test affect information behaviour. The subjects will be approximately 500 university students writing their dissertation.
> 
> In a pilot study it was shown that personality traits can be related to approaches to learning. Moreover they seem to form distinctive information behaviour.

## Introduction

The tradition of research in information studies has become more user centered since the beginning of the 1980's ([Kuhlthau, 1991](#ref2)). It has been acknowledged that the user studies have important implications on the availability of information and the development of libraries. Psychology gives us essential understanding of search behaviour ([Awaritefe, 1984](#ref1)). This creates a base through which we can develop the search systems and make the information more available for the customer. Instead of, as before, studying the safe and stable information systems the focus is now on the users who often are uncertain and confused ([Kuhlthau, 1991](#ref2)).

Today it is increasingly important to be information literate. An information literate person realizes the need for information, finds, evaluates and effectively uses the information he needs ([American Library Association, 1989](#ref1)). The aim of the library and information skills curriculum is not only to teach how to locate and access information sources but to develop logical and critical thinking in the students. ([Eisenberg & Berkowitz, 1988](#ref1)). Talents like cognitive competence, systematical thinking, energetic information-seeking and an optimistic attitude towards problem solving are important in the information society ([Savolainen, 1995](#ref2))

Understanding of the connection between personality and information seeking can ease the understanding of the different seeking behaviour of the students and help teachers, tutors and librarians support the students in their searches. You must meet each student in the framework of his personal search style, some for instance finds it easier than others to search databases ([Hawk, 1993](#ref1)). No single search strategy is the right one, the crucial criteria is how well it suits the individual in question.

The aim of this study is to compare the relationship between personality on the one hand and approaches to learning and information behaviour on the other. The personality, search behaviour and learning style will also be compared to the students' own estimated study result.

## The Impact of Personality on Information Behaviour

This research is based on [Wilson's](#ref2) (1997) definition of information behaviour, a term including information seeking and the needs of the user. The term includes the need of information, inner processes and environmental factors affecting the individual's way of responding to the information need.

[Palmer](#ref2) (1991) showed that the information behaviour of scientist could be clustered into five groups of information searchers characterized of different personalities. The first group was the _non-seekers_. They were not really motivated for information seeking. The second group was the _lone, wide rangers_. They liked working alone and often found the solution to their problems by chance. They subscribed to more journals than the other groups. They sought information very widely. They also had more personal contacts than the other groups. This could be due to their longer experience in the field. The third group was the _unsettled, self-conscious seekers_. This group consisted of fresh researchers who were still quite new to the field. They visited the library more frequently than the other groups. They also consulted their colleagues perhaps due to insecurity about their role and their information seeking. They had not chosen the subject for their research themselves. The fourth group was the _confident collectors_**.** They didn't put much effort into conscious information seeking but they tried to keep an open mind for new information. They trusted themselves for getting the proper information. They had worked in their field a long time. The last group was the _hunters_**.** These researchers worked in biochemistry, a rapidly changing field. They had developed their own strategies to cope with the information flow. They all had information seeking routines, some even visited the library every day. They had wide contact with colleagues both in the same country and abroad. (Palmer, 1991)

Information-seeking behaviour has also been related to for instance attachment style ([Bowlby, 1969](#ref1)). Secure and anguished persons are more curious than avoiding people. Secure persons have a constructive and positive attitude towards information and seek a lot of information. Anguished persons prefer personal contacts when seeking information whereas avoiding persons tend to avoid social contact also in their search behaviour. Insecure persons have more difficulties in coping with unpredictability, disorder and ambiguity. They have a tendency to finish the search process as soon as possible resulting in decisions based on early-received information. They are less prone to change their views and accept new information. ([Miculincer, 1997](#ref2)). This insecurity could be linked with neuroticism (Schouwenburg, 1995) which in turn can be linked with a surface learning style ([Entwistle, 1988](#ref1)).

## The Five Factor Model of Personality

During the last years conformity about the basic personality traits has emerged. It has been stated that they are extraversion, neuroticism, agreeableness, conscientiousness and openness to experience. These dimensions are stable across the lifespan and directly related to behaviour. They also seem to have a physiological base. ([Revelle & Loftus, 1992](#ref2))

The five factors are the following;

### 1\. Extraversion

The extraverts tend to be more physically and verbally active whereas the introverts are independent, reserved, steady and like being alone. The person in the middle of the dimension likes a mix between social situations and solitude. ([Howard & Howard, 1998](#ref1)). Extraverts are adventurous, assertive, frank, sociable and talkative. Introverts are quiet, reserved, shy and unsociable. ([Boeree, no date](#ref1)).

### 2\. Agreeableness

The agreeableness scale is linked to altruism, nurturance, caring and emotional support versus hostility, indifference, self-centeredness and jealousy. Agreeable people are altruistic, gentle, kind, sympathetic and warm. ([Boeree, no date](#ref1)).

### 3\. Conscientiousness

The conscientious, focused person is concentrating on only a couple of goals and strives hard to perceive them. He is career oriented, while the flexible person is more impulsive and easier to persuade from one task to another. Conscientiousness has been linked to educational achievement and particularly to the will to achieve. ([Howard & Howard, 1998](#ref1)). The more conscientious a person is the more competent, dutiful, orderly, responsible and thorough he is.

### 4\. Neuroticism

The persons with a tendency towards neuroticism are more worried, temperamental and prone to sadness. ([Howard & Howard, 1998](#ref1)). Emotional stability is related to calm, stable and relaxed persons, whereas neuroticism is linked to anger, anxiousness and depression. ([Boeree, no date](#ref1)). The name neuroticism doesn't refer to any psychiatric defect. A more proper term could be negative affectivity or nervousness. ([McCrae & John, 1992](#ref2)).

### 5\. Openness

People with a high openness have broader interests, are liberal and like novelty. This factor relates to intellect, openness to new ideas, cultural interests, educational aptitude and creativity ([Howard & Howard, 1998](#ref1)). These individuals are cultured, esthetic, intellectual and open. ([Boeree, no date](#ref1)). The openness to experience can be connected to activities like writing, science and art ([Wallach & Wing, 1969](#ref2)).

## The Relationship between the Five Factor Model Personality Traits and Learning

Personality traits are expressed in learning styles, which are in turn reflected in learning strategies, which eventually produce a certain learning outcome. ([De Raad & Schouwenburg, 1996](#ref1)). Personality traits serve as directors or blocks for motivation and learning strategies. (Mumford & Gustafson 1988 in [Blickle, 1996](#ref1)).

Conscientiousness and openness to experience are of special educational interest. ([de Fruyt & Mervielde, 1996](#ref1)).[Blickle](#ref1) (1996) has compared the Five Factor Model personality traits with learning strategies and learning outcome. He found that particularly conscientiousness and openness were related to learning style. The student's personality was related to learning outcome mediated by learning strategies.

Conscientiousness is related to work discipline, interest in subject matter, concentration and considering studying as quite easy. ([Schouwenburg, 1995](#ref1)). Different ways of learning in higher education have been described by [Entwistle & Tait](#ref1) (1996). Students using the strategic approach are good at organizing their work, managing their time and work hard in their studies. They care about their working conditions and have clear goals for their studies. ([Entwistle & Tait, 1996](#ref1)). They have an intrinsic motivation and a positive study attitude ([Entwistle, 1988](#ref1))

Openness is linked with questioning and analysing arguments. ([Schouwenburg, 1995](#ref2)). It is further related to critical evaluation, searching literature and making relationships (deep approach) ([Blickle, 1996](#ref1)). The students with a deepapproach want to find out the deeper meaning in the text. They are critical, logical and relate what they learn to their previous knowledge. ([Entwistle & Tait, 1996](#ref1)). Their motivation is intrinsic and they look for a personal comprehension independent of the syllabus. ([Entwistle, 1988](#ref1)).

Neuroticism is linked to lack of concentration, fear of failure and experiencing studying as stressful. Moreover neuroticism is linked with a lack of critical ability and problems in understanding how things relate to each other. ([Schouwenburg, 1995](#ref2)) This can be linked to the surface learning style ([Entwistle, 1988](#ref1)). The student with a surface approach concentrates on memorising without any concern of finding a deeper meaning or understanding of the material. They are most concerned about getting through the exams and are not really interested in the material itself. ([Entwistle & Tait, 1996](#ref1)). Their motivation is extrinsic and they take on a strategic, syllabus-bound approach to studying ([Entwistle, 1988](#ref1))

## The Relationship between Learning Style and Search Behaviour

There seems to be a relationship between learning style and search approach. ([Limberg, 1998](#ref2)). Searchers characterized by different learning styles adopt different search strategies. ([Wood et al, 1996](#ref2)).

Subcritical students seek authoritative sources in order to find the objective truth. They memorise and describe the information rather than value it. They seek only superficial information and the most important sources. The surface-critical students value the information clinically in order to impress their teachers. They need information from many sources. The deep-critical group seeks information from many sources in order to form their own opinion. ([Ford, 1986](#ref1)).

In Fidel's 1984 study search styles were compared to the two different learning styles. It was found that the search style obtained was related to the learning style used. The conceptualist search style can be compared with the wholist learning style ([Pask, 1976](#ref2)) and the second search style corresponds to the analytical learning approach ([Pask, 1976](#ref2)). ([Fidel, 1984](#ref1))

[Limberg](#ref2) (1998) also found relationships between search style and learning style. The first group in her study had an atomistic approach to information seeking, concentrating on finding facts. They wanted information that was easy to find, understand and could be reached by the least effort. The second group wanted to find information to be able to make the right choice. The third group wanted more information to be able to analyse it. Their aim was to understand and critically reflect over the material. They used many different kinds of material and many different search paths. This group could be compared to the wholistic learning style. There were different use of information and for instance relevance judgement in the three groups as a result of different conceptions of information seeking. ([Limberg, 1998](#ref2)).

## The Study

In order to find out if personality and approaches to learning are related and how they effect information behaviour 500 university students will be asked to fill out three questionnaires regarding their information behaviour, personality and approaches to studying. There is likely to be a similarity between learning style and search behaviour as they both spring from the same source, the personality structure. The personality is a filter that influences both learning style, search behaviour as other types of behaviour.

The questionnaires are the following;

### 1\. Questionnaire about information behaviour

The questionnaire regarding information behaviour is based on previous research in the area of personality, learning styles, search behaviour and their relationships. It will consist of questions covering cognitive aspects, resources, search strategies and information sources.

### 2\. The Personality test

The Five-factor inventory (FFI) is a test of five personality factors; neuroticism, extraversion, openness, agreeableness and conscientiousness. It has developed from the Five-Factor Model of personality, which claims that these factors are the most essential in human personality. The Five-Factor Model (FFM) is based in a belief that people are rational beings and can account for their own personality and behaving. According to this theory people generally understand their own way of being and can analyze their own actions and reactions. ([McCrae & Costa, 1996](#ref2)). One of the best proofs for the FFM is the convergence between lay-observer ratings, expert ratings and self-reports ([McCrae & Costa, 1996](#ref2)).

### 3\. The Approaches to Studying Test

The Approaches and Study Skills Inventory for students (ASSIST) ([Entwistle & Tait, 1996](#ref1)) test is a learning approach inventory specially designed for use in higher education studies ([Entwistle & Tait, 1996](#ref1)). The main purpose of the test is to give information about the students' motivation for education and their learning styles. The learning style categories used in this test are deep, strategic and instrumental (surface) approach. Previous versions of the ASSIST test have been used in many studies. A short version of the ASSIST-test will be used in this study in order to easy the students workload in completing the various questionnaires. This short version correlates with the complete version of the test ([Entwistle, personal communication 1999](#ref1)).

## Pilot study

A pilot study was done on 7 doctoral students in Information Studies to test the questionnaires. It is important to notice the group of subjects, as they are supposed to be more or less professional in the field. The results might have been different in other groups. A homogeneous group was on the other hand useful for this purpose as the influence of subject area and methods in the area was reduced and minimized. This was good, as the purpose of the test was to test the influence of personality and approach to learning.

The material was tested statistically mainly by correlation and factor analysis. The most significant result was found in the correlation between a **strategic approach to learning** and a **conscientious personality**. This shows a clear relationship between these variables. In a factor analysis it was shown that conscientiousness linked to the strategic approach to learning formed a search behaviour that was orderly and systematic. These students put much effort into their information seeking; that is they were willing to pay for articles, spend much time on information seeking and go through trouble obtaining information. It has previously been shown that the conscientiousness scale is related to effort ([Blickle, 1996](#ref1)) and this can thus also be related to the effort put into information seeking. These students also used fairly many different sources, including informal sources. They planned their searches in databases in advance and were confident in their ability to judge relevance. This group also had a considerably better study result than the other groups. This result can be related to previous research that has shown that conscientiousness is the most important characteristic related to academic success ([de Fruyt & Mervielde, 1996](#ref1)). It has also been found that strategic approaches to studying and work discipline are predictors of a good study result. ([Schouwenburg & Kossowska, 1999](#ref2)).

Another significant correlation was the one between a **surface approach to learning** and **neuroticism**. These traits also formed a factor in the factor analysis. These students had a low critical ability. For this group it was important that the books they used had a nice physical appearance. If the book was torn or worn out they rejected it. It was also crucial for them that the source was of high scientific quality. These students preferred material which were acknowledged in the field as opposite to new, paradigm-challenging material. This can directly be related to the extrinsic motivation, where the students look for authorities to tell them what to do.

The expected correlation between the **deep approach to learning** and the **openness personality trait** was not proved in the pilot study, at least not by a high correlation. These trait together formed an information behaviour which was characterized by looking for new ideas and paradigm-challenging material. These students had a quite high critical ability. This finding can be confirmed by previous studies which state that openness is linked with questioning and analysing arguments, in other words critical ability ([Schouwenburg,1995](#ref2); [Blickle, 1996](#ref1)).

## Discussion

It is important to remember that the pilot study was performed on only a few subjects, moreover in the field of Information Studies, presumingly good at handling information-related problems. Still it was encouraging to see differences according to personality and approaches to learning even in such a homogenous group. In the real study students of different disciplines will fill out the questionnaire and the different disciplines are likely to affect their information behaviour. In a larger group it is on the other hand easier to find patterns of information behaviour, personality and approach to learning.

The study is basically descriptive. The main aim is to point to the influence of personality also on information behaviour. In previous studies the influence of discipline, working environment, social class etc have been studied. Only a few studies (e.g., [Palmer, 1991](#ref2) and [Miculincer, 1997](#ref2)) have related the variation of information behaviour to personality.

Information behaviour is largely a question of training and tradition, but it is important to have an awareness that personality can influence how we learn. One aim of this study is to increase the understanding of why different persons search information in different ways. An awareness of the personality factor in teaching students how to seek information can be important and might increase the understanding of why different persons approach their search tasks in different ways. It is important to take these differences into account when planning the teaching.

Also the development of search systems should be more flexibly designed so that different ways of approaching the search tasks might be met. To some extent information searching can be learned, but there will always be differences in the ways different people seek information. There can never be only one way to approach the search problem, why then have just one search design, as the consequences will inevitably be shutting out some people. It is important to adjust the systems to the learners instead of just teaching the learners to use the system. They will still to some extent hold their previous behaviour and can never be taught exactly how to search in every situation.

For this study a quantitative approach was chosen in order to be able to generalize more freely from the findings. But there are also obvious drawbacks to this method. When students are forced to choose between a certain set of alternatives when describing their information behaviour, the result is obviously limited. Many types of information behaviour will thus not be described in this study simply because the subjects will not have the opportunity to express them. On the other hand the aim of the study was not qualitative descriptions of the matter but instead finding general patterns.

## References:

<a id="ref1"></a>

*   American Library Association (1989). _Report of the Presidential Committee on Information Literacy_. Available on: Gopheer://ala1.ala.org:70/00/alagophiv/50417007 Site accessed 5.2.1998.
*   Awaritefe, M. (1984). "Psychology applied to librarianship." _International Library Review_. **1**, 27-33.
*   Blickle, G. (1996). "Personality traits, learning strategies, and performance." _European Journal of Personality_, **10**, 337-352.
*   Boeree, G. C. (no date). _Personality theories_. Available on: [http://www.ship.edu/~cgboeree/persintro.html](http://www.ship.edu/~cgboeree/persintro.html). Site accessed 10\. 6\. 1999.
*   Bowlby, J. (1969). _Attachement style and loss_. New York: McGraw-Hill
*   De Fruyt, F. & Mervielde, I. (1996). "Personality and interests as predictors of educational streaming and achievement." _European Journal of Personality_, **10**, 405-425\.
*   De Raad, B. & Schouwenburg, H. C. (1996). "Personality in learning and education: a review." _European Journal of Personality_, **10**, 303-336.
*   Eisenberg, M. B. & Berkowitz, R. E. (1988). _Curriculum initiative: an agenda and strategy for library media programs_. Norwood: Ablex.
*   Entwistle, N. (1988). "Motivational factors in students' approaches to learning", in: _Learning strategies and learning styles_. edited by R. R. Schmeck. New York: Plenum Press. pp. 21-49
*   Entwistle, N & Tait, H (1996). _Approaches and Study Skills Inventory for Students_. Centre for Research on Learning and Instruction. University of Edinburgh.
*   Fidel, R. (1984). "Online searching styles: a case-study-based model of searching behavior." _Journal of the American Society for Information science_, **4**, 211-221.
*   Ford, N. (1986). "Psychological determinants of information needs: a small-scale study of higher education students." _Journal of Librarianship_, **1,** 47-61.
*   Goldsmith, R. (1989). "Creative style and personality theory." _in: Adaptors and innovators_. edited by M. Kirton. New York: Routledge. pp. 37-55
*   Hawk, S. (1993). "The effects of user involvement: some personality determinants." _International Journal of Man-Machine Studies,_ **38,** 839-855.
*   Howard, P. J. & Howard, J. M. (1998). _An introduction to the five-factor model for personality for human resource professionals._ Available on: www. centacs.com/quik-pt3.htm Site accessed 7.6.1999
<a id="ref2"></a>
*   Kuhlthau, C. G. (1991). "Inside the search process: Information seeking from the user's perspective_." Journal of the American Society for Information Science,_ **5**, 361-371.
*   Limberg, L. (1998). _Att söka information för att lära._ Göteborg: Valfrid.
*   McCrae, P. R. & Costa, P. T. (1996). "Toward a new generation of personality theories: theoretical contexts for the Five-Factor model." in _The Five-factor model of personality. Theoretical perspectives,_ edited by J. S. Wiggins. New York: Guilford Press. pp. 51-87
*   McCrae, R & John, O. (1992). "An introduction to the Five-Factor Model and its applications." _Journal of Personality,_ **2**, 174-214.
*   Miculincer, M. (1997). "Adult attachement style and information processing: individual differences in curiosity and cognitive closure." _Journal of Personality and Social Psychology,_ **5**, 1217-1230.
*   Palmer, J. (1991). "Scientists and information: II. Personal factors in information behaviour." _Journal of Documentation_, **3**, 254-275\.
*   Pask, G. (1976). "Styles and strategies of learning." _British Journal of Educational Psychology,_ vol. 46, 128-148.
*   Revelle, W. & Loftus, D. (1992). _The implications of arousal effects for the study of affect and m_emory. Available on: [http://pmc.psych.nwu.education/revelle/publications/r191/rev-loft-ToC.html](http://pmc.psych.nwu.education/revelle/publications/broadbent/broad.html). Site accessed 9.6.1999
*   Savolainen, R. (1995). "Everyday Life Information Seeking: approaching information seeking in the context of "Way of Life". "_Library and Information Science Research_, **17**, 259-294.
*   Schouwenburg, H. C. (1995). _Personality and academic competence_. Poster presented at the seventh meeting of the International Society for Study of Individual Differences, Warsaw, Poland.
*   Schouwenburg, H.C. & Kossowska, M. (1999_). Learning styles: Differential effects of self-control and deep-level information processing on academic achievement._ Available on http://www.rug.nl/rugcis/.bureau/dsz/studond/topics/research/gent1.htm Site accessed on September 7<sup>th</sup>, 1999
*   Wallach, M. A. & Wing, C. W. (1969). _The talented student_. Abstract available on http://home.ku.education.tr/~earik/personality/wallach.html. Site accessed 9.6.1999
*   Wilson, T.D. (1997). "Information behaviour: an inter-disciplinary perspective." in _Information seeking in context_, edited by Vakkari, Savolainen & Dervin. London: Taylor Graham, 39-49
*   Wood, F. et al. (1996). "Information skills, searching behaviour and cognitive styles for student-centered learning: a computer-assisted learning approach." _Journal of Information Science_, **2**, 79-92.