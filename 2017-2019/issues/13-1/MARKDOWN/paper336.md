#### vol. 13 no. 1, March 2008



# Flickr: a first look at user behaviour in the context of photography as serious leisure



#### [A.M.Cox](mailto:a.m.cox@sheffield.ac.uk), [P.D. Clough](mailto:p.d.clough@sheffield.ac.uk) and [J. Marlow](mailto:j.marlow@sheffield.ac.uk)  
Department of Information Studies, University of Sheffield, Regent Court, 211 Portobello Street, Sheffield S1 4DP United Kingdom

#### Abstract

> **Introduction.** The use of **Flickr**, a photo sharing Website, is examined in the context of amateur photography as a 'serious leisure' pursuit.  
> **Method.** Eleven telephone interviews were carried out with users of **Flickr**, using an open-ended interview schedule to explore use of the system within the context of the interviewees' photographic practices.  
> **Analysis.** Practices described are set against theoretical considerations from the literature, specifically the alternate paradigms of the photographic club and the photo magazine. Sontag's cultural critique of photography is an important, challenging reference point.  
> **Results.** The affordances of the system affect the satisfactions of hobby photography. **Flickr** creates moral dilemmas, such as whether to reciprocate comments or tag the photos of others. The system's appeal lies in its moral qualities as much as whether it is easy to use or performs functions efficiently. **Flickr** draws users into the hobby and so, like the camera club or the magazine, can be linked to the interests of industry. Yet it is too pessimistic to see it as simply a vehicle of consumerist culture; users expressed almost unqualified satisfaction with the system for its direct pleasures and learning opportunities.  
> **Conclusions.** The fluid social relations of **Flickr** potentially free the hobby from the rather restrictive codes and ordering of the photographic club.



## Introduction

[Flickr](http://www.Flickr.com) is a popular Website for 'sharing' photographs, invariably cited in discussions of Web 2.0 (e.g., [O'Reilly 2005](#oreilly), [Katz 2006](#katz)). Because it is based on individuals uploading their own content, **Flickr** reflects the move from consumption to mass participation, a supposed feature of Web 2.0\. **Flickr** was also an early implementer of tagging ([Hammond _et al._ 2005](#hammond)). As a result of its open application programming interface (API), **Flickr** can be used in the development of "mash-ups", new services in which the data from several systems is integrated. It also adheres to the Web 2.0 philosophy of continuous development, 'perpetual beta'; the joke is that **Flickr** is now in gamma. Our argument in this paper about **Flickr**, however, is not constructed around Web 2.0 and whether this represents more than a reinvigoration of themes that were always part of the Web (self publishing, online community). Rather the paper endeavours to avoid the typical hype around Web 2.0 applications (and the Internet generally) by locating the analysis firmly in an understanding of photography for itself, as a serious leisure pursuit, drawing on pre-existing literature. It also uses interview material from **Flickr** users to investigate how it is actually being used. In this way we can hope to identify in what ways **Flickr** is novel and interesting.

The paper begins by exploring some of the background in the literature of amateur photography. It explains the method for collecting the interview data. It then presents a description of the features of **Flickr** and the findings of the interviews. It concludes by considering how **Flickr** potentially changes the social ordering of amateur photography.

## Background: the social worlds of amateur photography

Commentators have recognised that there is a marked divide between the casual 'snapshooter' and the social world of the serious amateur photographer ([Porter 1989/90](#porter), [Slater 1999](#slater99)). Using data from 1979, Porter ([1989/90](#porter)) characterised the typical serious amateur as male, class ABC1 and aged 25-44\. They typically used a high number of rolls of film (10+) a year, were a camera club member (through which they reach an audience for their work beyond the family), were a reader or subscriber to a range of photographic magazines and had access to a dark room ([Porter 1989/90](#porter): 407). Serious amateurs like to see themselves as very different from the snapshooter, who takes a few photos a year to mark family events ([Porter 1989/90: 46](#porter)). The subjects photographed, the style of photograph and its uses are all different in the serious hobby. This status differential is played on in advertising to sell more sophisticated, more expensive equipment ([Slater 1999, plate 18.2: 297](#slater99)). Much of the profit of the industry has historically come from this band of sales ([Slater 1999: 298](#slater99)).

Those who take photography as a serious leisure pursuit ([Stebbins 1992](#stebbins92), [2004](#stebbins04)), the serious amateurs, are also to be demarcated from the professionals. Like the professionals, they have a strong psychic investment in the activity, a _career_ in developing their skill and a shared knowledge base, and identify strongly with others engaged in the practice ([Stebbins 1992](#stebbins92)) but they lack the formalised training and paid employment of a professional and a degree of external recognition. The social worlds of amateur photography and its institutions of clubs and magazines are rather separate from those of professional photography.

A useful paper by Schwartz ([1987](#schwartz87)) gives us a deeper insight into the working of one important part of the social world of the serious amateur: the photographic club. Her ethnographic study shows that such clubs are organized through a series of highly ritualised social activities, such as dinners and competitions (themselves linked to a ladder of recognition of achievement). All activities are tightly governed by informal rules: for example, in competitions, about how photographs are presented (e.g., mounting, [Schwartz 1987: 263](#schwartz87)) and how interactions are ordered (e.g., judges' behaviour is codified, [Schwartz 1987: 272](#schwartz87)). Schwartz also agrees with the common critical evaluation of such clubs ([1987: 253](#schwartz87), [Schwartz 1986](#schwartz86)) that they have a conventionalised, even restrictive set of standards of what is a good photograph. This code stresses correct exposure, technical perfection, and representational realism and privileges certain genres ([Schwartz 1987: 260-2, 264](#schwartz87)). Schwartz identifies these values with pictorialism ([1987: 259](#schwartz87)). Yet, she makes the point that this narrow, stable, consensual semiotic code is necessary to the functioning of a club.

> Camera club photographs emanate from a stable pictorial code. The club context for photographic activity produces a symbol-sharing community which nurtures and maintains traditional aesthetic values" ([1987: 279](#schwartz87))

For Schwartz, critics of the club's lack of creativity have failed to recognise the social context in which the code works, its purpose. Without having a common ground in an agreement about what is good, how could the club, with its competitions and career ladder, be maintained? Both competitions and hierarchical order imply some common values. Equally, one could argue that the close and long lasting friendships Schwartz observes in the group ([1987: 257](#schwartz87)) are premised on shared values. Further, the club and its competitions perpetuate pictorialism.

Although not very much detail is given, the club described by Schwartz fits into a wider social world of other clubs (with necessarily a similar set of aesthetic standards) working to a framework provided by a national body (the Photographic Society of America, ([Schwartz 1987: 265](#schwartz87))) and also links to the photographic industry. One such link to business interest is the way the club encourages taking more photographs and therefore greater consumption of film. More importantly, the club has a role in the fetishisation of camera technology, where increasing skill and status are linked to having more sophisticated and expensive equipment. The network of clubs is an essential part of constructing the amateur career ([Stebbins 2004: 69-71](#stebbins04)) and is therefore closely tied to this "ladder" of consumption ([Slater 1999: 299](#slater99)), and thus the interests of the industry.

Schwartz's study dates to the early 1980s. We glimpse through it a picture of a rather stable system of amateur photography in which a network of clubs is tied to the economic interests of the camera industry, through its encouragement of taking more photographs and upgrading equipment ([Slater 1999](#slater1999)). However, it should not be ignored that there is probably considerable enjoyment and satisfaction for the participants in their club, both in photographic practice itself and through the close social ties that develop around it, regardless of a wider critical context that we might wish to uncover. Also, this neat picture linking the institutions of amateur photography and the interests of the industry is not totally convincing. In fact, the situation is probably more complex. Porter ([1989/90](#porter)) identifies the serious amateur as typically male between the age of 25 and 45\. Yet in Schwartz's club typical members were older than 50 and there was a reasonable sex balance. This might be a feature only of the club she investigated, but it could be typical: we do not know. Certainly there may always have been many amateur photographers who conduct their hobby outside the club system. Presumably the forms of influence that discipline their photography are mass forms such as magazines, photo manuals or inscribed into cameras themselves as tools (because certain types of technical option are emphasised in the design of a camera as defining what photography is about). Camera magazines may be a key institution in the way they propagate views to a mass audience about what are the appropriate aesthetics of amateur photography and link the taking of photos to consumption. Yet looking at the dominant discourses current in amateur photography, the technophile strand promoted in magazines is often resisted by others that are dominated by the language of art and personal expression and which are technophobic (e.g., [Lomographic Society International 2006a](#lomoa), [Cohen 2005a](#cohena), [Cohen 2005b](#cohenb)). Precisely how this discourse is maintained institutionally is unclear from existing literature.

Given the spread of ownership of cameras throughout society, photography may be viewed as one of the few mass media that has democratic possibilities. Yet Slater suggests that in reality it is a '_conventionalized, passive, privatized and harmless leisure activity_' ([1999: 289](#slater99)). Underlying this judgement is an expectation that the individual should be politically engaged and a belief that consumerism is false consciousness. The classic statement of the position is Sontag's _On photography_ ([1977](#sontag)). The main thrust of her iconoclasm is that taking photographs is a false and superficial engagement with the world. Thus, the tourist takes photos to deal with the anxiety of being in a strange place, but as a result does not really engage with it ([1997: 9](#sontag)). Although what constitutes good participation in the world is not explicitly defined, four examples suggest aspects of a definition:

*   Proust's difficult struggle to engage with elusive memory contrasts with the too easy generation of fabricated memories through family snapshots ([Sontag 1997: 163-5, 8](#sontag))
*   Lawrence's deep and sensual engagement with life is contrasted with photography's narrow focus on the visual ([Sontag 1997: 97](#sontag))
*   The protestor's activism is contrasted implicitly against the photographer's political non-intervention ([Sontag 1997: 10-11](#sontag))
*   Brecht's point that a photograph of Krupps cannot say anything about how capitalism is organized for Sontag encapsulates photography's inability to produce critical analysis ([Sontag 1997: 23](#sontag))

Thus, the photograph is too easy, fails to take a moral stance or conduct an analysis of things. Rather it tends, on the one hand, to aestheticise ([Sontag 1997: 176](#sontag)), so that a beautiful photograph can be made of anything or, on the other, defamiliarise, so further disconnecting us from the world ([Sontag 1997: 167](#sontag)). Sontag's arguments are fundamentally an extension of Plato's position that the visual does not lead to knowledge but rather creates illusions of knowledge ([Sontag 1997: 3](#sontag)). She also participates in the Marxist critique that capitalism makes us into 'image junkies' ([Sontag 1997: 24](#sontag)), creating illusory entertainments to anaesthetise against experiences of injustice ([Sontag 1997: 178](#sontag)).

Sontag's arguments are interesting and challenging, but only partly, if at all, convincing. Taking photographs can be alienating and enervating, but it can also lead to greater engagement. It draws the tourist out of his/her house to explore the world, even if they seek the relative safety of mediated experience and tend to see in the style of some master photographer. However, most seeing is through convention; few have the courage or imagination to reinvent how to see, which is almost what Sontag seems to demand. Sharing photographs is a locus of engagement, a moment through which common ground is built and shared meanings established. Because they are polysemic, images draw out differences of interpretation. They are natural boundary objects, i.e., artefacts that are understood differently in different social worlds, but connect those worlds by being reference points in all of them ([Leigh Star and Griesmer 1988](#star)). In short, the photograph is not in itself flawed as a means of engaging with the world and others, though it is often used for such evasion.

Sontag's is an extreme position, premised as it is on a Marxist critique of the consumer society. It is also difficult, because of its focus on a moral challenge about how people should act, to encompass within the paradigms of information studies and its focus on how information or information systems are used. Such cultural critics also eschew the empirical validation of their claims. Nevertheless, Sontag does provide a challenging starting point for our consideration of the relation between amateur photography as a hobby and **Flickr**; partly because it is among the most widely cited analyses of photography, but also because it may expand our understanding to acknowledge that **Flickr** is not just a neutral information system but also value laden and has a role within a wider cultural order. The work of Stebbins, Schwartz and others frame questions around how the practices of photography may have been changed by **Flickr** and how the social institutions of amateur photography, and the relation between the hobby and the industry, are reconfigured.

## Features of **Flickr**

It will be useful to give the reader an overview of the main features of **Flickr** as at the time of writing (December 2006) (see also [Garrett 2005](#garrett), [Koman 2005](#koman), [Davies 2006](#davies)). Since the site is in 'perpetual beta' some features will certainly have changed by the time this article is published. Users are also not always aware of all the features of the system so it might appear quite differently to them.

Viewing a particular photo one sees the author-given title, description and tags (keywords), including geotags (grid references for the location where the photo was taken). Descriptive EXIF data embedded automatically by most digital cameras into the picture file can be viewed by clicking on **_More properties_** (unless the author has set their preferences not to display this information). This gives the viewer details of the settings of the camera when the photo was taken. The author can also set licence restrictions on use of the image, using Creative Commons licences. As a user, one can comment on a photo or add a note, add a tag, mark the photograph as a favourite, or blog the photograph. Thus there are several active ways one can react to a photograph.

Viewing a particular photograph one can also see whether the photograph has been added to a group pool. Groups are set up by users. They have a photo pool, discussion area and member listing. They are commonly related to photographing a particular type of thing, at varying degrees of granularity (dogs, Alsatians), or genre of photography. Many groups relate to places. They are primarily buckets for collecting related types of photos rather than social groups. Some groups encourage critiquing the submissions of members (e.g., **_Hit or miss_**) or are games such as identifying the location of a photo or **_Photoshop tennis_**, where users iteratively edit photos.

One can also navigate from a photograph to view more about the author of the photograph, including their other photographs (_photostream_) and profile. The profile is partly created deliberately by the author: filling in details about themselves, providing links to a personal Website and listing personal interests (favourite music and books). This has something of the flavour of a social networking site, e.g., an option is to identify one's availability for dating. Users can also choose a name and a _buddy icon_ (a small, thumbnail image). Other users can add _testimonials_ to the profile praising the photographer in general. The author is also profiled automatically through a listing of their contacts and the public groups of which they are a member. Contacts are people the user has chosen to mark, be that because they are a friend or simply because the user likes their photos. The system gives prominence at log-on to browsing contacts' latest photos, so this is an incentive to add contacts. The user can also navigate to see another user's tag cloud representing the tags they have used, listed in fonts proportionate to the frequency they are used (a quick way to get a feel for the author's interests), photos they have made a favourite and a map showing where photos have been taken (if geotags have been added). Thus from the user's point of view a natural way to explore photographs is through people.

One can search for people, groups and photos (by tags or all descriptive material) and browse through a photo to the photostream of the author, groups, tags, the camera in use; through groups to the pool or member's photostreams; or through contacts to their photostream, groups and their own contacts.

The system provides photographers with a number of tools to manage their photo collection. There are tools to upload photos and organize them in sets. Access can be restricted. One can also view data about the number of times one's photos have been viewed and made a favourite by others. It is also possible to track one's own activity and view subsequent comments on photos where one has commented oneself.

## Method

The empirical data used in this paper is from eleven 30 to 40 minute long telephone interviews of **Flickr** users conducted between July and December 2006\. The full interview schedule is reproduced in the appendix. Questions were developed from the researchers' own experiences of using **Flickr**, and with a concern to discover how exactly the system works and why users enjoyed the experience and how it fitted into why they took photographs and their history in photography. The focus was on exploring users' perspectives on use, rather than specific researcher hypotheses. At the same time within the research the perspective of cultural critics like Sontag demands that broader philosophical questions are asked which go beyond the perspective of users to see cultural practices in a social context which is often taken for granted by users. This is appropriate because **Flickr** seems to fall somewhere between an information system (that might be understood within the paradigms of information studies) and a mass medium (that can be approached by methods prevalent within media or cultural studies).

For interviewees' convenience, interviews were conducted by telephone. It is acknowledged that telephone interviews are generally not as good as face-to-face interviews for richness of data, because rapport is more difficult to establish, more prompting by the interviewer has to occur and there is a significant loss of visual information both about how responses are given and artefacts (such as a camera or physical arrangements of uploading) cannot be used to stimulate discussion as they would in a face-to-face encounter. So in this paper the ambition is to do some initial theory building and to identify key issues, rather than any sort of definitive account of the use of **Flickr**.

The research was cleared through the institutional ethical procedures and guided by ethics guidelines for social research, such as those of the Association of Internet Researchers.

Interviewees were selected from recently active members of the **Flickr** group associated with a large UK city (hereafter the _City group_) i.e., people who had posted photos in the group pool about the time we were conducting the interviews. **Flickr**'s own internal mailing system was used to make the request for an interview. This method of selection was necessitated partly by low response rates to early requests for interviews, but it does place limits on the representativeness of the sample.

To explore the nature of the representativeness of the group - and to explore the basic demographics of the **Flickr** user base - we compared 50 randomly chosen members of the group to a random sample of 50 from all **Flickr** users .

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="BORDER-RIGHT: #99f5fb solid; BORDER-TOP: #99f5fb solid; FONT-SIZE: smaller; BORDER-LEFT: #99f5fb solid; BORDER-BOTTOM: #99f5fb solid; FONT-STYLE: normal; FONT-FAMILY: verdana, geneva, arial, helvetica, sans-serif; BACKGROUND-COLOR: #fdffdd"><caption align="bottom">  
**Table 1\. Characteristics of random sample of 50 of all Flickr users and 50 City group members, compared to interviewees**  
</caption>

<tbody>

<tr>

<th colspan="2">Parameters</th>

<th colspan="2">All Flickr users sample</th>

<th colspan="2">City group random sample</th>

<th colspan="1">Interviewees</th>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>N=50</td>

<td> </td>

<td>N=50</td>

<td> </td>

<td>N=11</td>

</tr>

<tr>

<td>Sex</td>

<td> </td>

<td>(14 n/a)</td>

<td> </td>

<td>2 n/a</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>Female</td>

<td>16</td>

<td>44%</td>

<td>13</td>

<td>27%</td>

<td>2</td>

</tr>

<tr>

<td>Age</td>

<td> </td>

<td>(30 n/a)</td>

<td> </td>

<td>25 (n/a)</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td><21</td>

<td>2</td>

<td>11%</td>

<td>3</td>

<td>12%</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>21-30</td>

<td>11</td>

<td>58%</td>

<td>13</td>

<td>52%</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>31-40</td>

<td>5</td>

<td>26%</td>

<td>9</td>

<td>36%</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>41-50</td>

<td>1</td>

<td>5%</td>

<td>0</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>51+</td>

<td>0</td>

<td> </td>

<td>0</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Mean Age</td>

<td> </td>

<td>26.78</td>

<td> </td>

<td>26.84</td>

<td> </td>

<td>29</td>

</tr>

<tr>

<td>Profile 1</td>

<td> </td>

<td> </td>

<td>67%</td>

<td> </td>

<td>90%</td>

<td> </td>

</tr>

<tr>

<td>Profile 2</td>

<td> </td>

<td> </td>

<td>51%</td>

<td> </td>

<td>72%</td>

<td> </td>

</tr>

<tr>

<td>Website</td>

<td> </td>

<td> </td>

<td>39%</td>

<td> </td>

<td>52%</td>

<td>8</td>

</tr>

<tr>

<td>Mean no. contacts</td>

<td> </td>

<td>41.65</td>

<td> </td>

<td>69.02</td>

<td> </td>

<td>118</td>

</tr>

<tr>

<td>Mean no. photos</td>

<td> </td>

<td>1058</td>

<td> </td>

<td>514</td>

<td> </td>

<td>680</td>

</tr>

<tr>

<td>Occupation</td>

<td> </td>

<td>(32 n/a)</td>

<td> </td>

<td>(23 n/a)</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>Student</td>

<td>2</td>

<td> </td>

<td>10</td>

<td> </td>

<td>3</td>

</tr>

<tr>

<td> </td>

<td>Media or new media</td>

<td>3</td>

<td> </td>

<td>7</td>

<td> </td>

<td>1</td>

</tr>

<tr>

<td> </td>

<td>IT</td>

<td>7</td>

<td> </td>

<td>4</td>

<td> </td>

<td>2</td>

</tr>

<tr>

<td> </td>

<td>Unemployed</td>

<td>0</td>

<td> </td>

<td>0</td>

<td> </td>

<td>3</td>

</tr>

<tr>

<td> </td>

<td>Other</td>

<td>6</td>

<td> </td>

<td>6</td>

<td> </td>

<td>3</td>

</tr>

<tr>

<td> </td>

<td>Total</td>

<td>18</td>

<td> </td>

<td>27</td>

<td> </td>

<td>11</td>

</tr>

<tr>

<td>Pro account</td>

<td> </td>

<td>31</td>

<td> </td>

<td>31</td>

<td>62%</td>

<td>10</td>

</tr>

<tr>

<td colspan="7">Note: The sample was generated by taking four lots of 12 or 13 users from the "recent uploads" section of Flickr, from four time periods spread over two days (a weekday and a weekend day) in July 2006\. This may not be random because we do not know how the recent uploads section is generated.  
Note that profiles 1 and 2 refer to different parts of the Flickr profile.</td>

</tr>

</tbody>

</table>

The salient features of _the City group_ can be summarized as follows. A quarter of them were female. Although there is a lot of missing data about age, approximately 50% of group members were 21-30 years old; another third 31-40\. The mean age was about 27\. This belies the common claim that **Flickr** is used most heavily by very young people, i.e., teenagers. Ninety percent of users filled in something in their profile (of whom five mentioned the make of their camera as a defining attribute). Over 50% linked to another Website. The average number of contacts was around 70 (quite high); with around 500 photos uploaded to Flikr. About half acknowledged their occupation, which was most commonly being a student, with media, new media and technology jobs second in frequency. Ten of the fifteen lived in the city concerned.

Turning to the random sample of fifty of all **Flickr** users (see also [Meyer _et al._2005](#meyer)), whose findings are broadly congruent), far fewer had profiles; indeed, the majority gave very little information about themselves. They also had fewer contacts (the mean was 42, but this was distorted by three individuals with very large numbers of contacts - whereas the deviation across __the City group__ was much smaller). Interestingly, the all-users sample had significantly more photos uploaded, with a mean above 1000, more than twice _the City group_. Many more were also female (40%) and though this might be distorted by low reporting, it is suggestive given the sex-biased character of serious amateur photography. The occupational range is similar, though with fewer students and more _others_. The strongest similarity was in age, where the profiles and averages were almost the same.

Although the sample of _all **Flickr** users_ is very small, we would suggest that the findings above are reliable in suggesting that users who participate in groups like _City group_ are more active users, who tend to use all the features of **Flickr** more. The all-users group may contain far more snap shooters who are primarily sharing photos with friends and family, invest little time in the hobby, use only the storage aspects of the site and participate less in the social aspects of it (joining groups, making contacts or commenting on photos). Additionally, there is a more even sex balance in the all-users group.

Furthermore, and not surprisingly, the users who responded to our request for an interview were also more active than the average City group members. For example, a high proportion had a developed profile, and they had more contacts and photographs than the random sample from the group. They also seemed to be generally more Internet active: all the interviewees had been using the Internet for a long time. Interviewees may also have had more individualistic ways of using **Flickr**. Thus looking at the data for the sample of fifty _City group_ members there was a significant positive correlation (calculated using Spearman's rho) each between the number of photos taken, number of contacts and number of favourites. For the interviewee group the pattern showed no correlation between the activities (though not statistically valid for the small sample of 11). Thus, the person with the most contacts had among the fewest photos. The person with the most photos had the fewest contacts and no favourites. As our interviewees over-represent more intense and committed users of **Flickr**, their responses are reported for interest not for simple typicality.

## Findings

### The satisfactions of amateur photography

From the description of **Flickr** given above it should be clear that **Flickr** has few inherently novel features, it trades on the audience's understanding of how the Web works, built up over a decade ([Goggin 2006: 152](#goggin)), yet it offers its users a rich set of easily understood tools. One way to conceptualise the attraction of **Flickr** is to see how the affordances it offers fit into the satisfactions of amateur photography as a serious leisure pursuit. We propose the following list of activities as representing the activities involved in traditional print photography as a serious hobby:

1.  Buying equipment.
2.  Travelling and planning.
3.  Taking pictures, e.g., setting up camera and waiting for correct lighting.
4.  Developing, printing and presentation (e.g., mounting or framing).
5.  Organizing one's collection.
6.  Showing photos, including in competitions.
7.  Developing relationships through social aspects of these activities (especially showing).
8.  Learning, e.g., through magazines, books, manuals and talks.

Each activity is potentially inherently satisfying, but also could be a chore. As Cohen ([2005a](#cohena)) explains, photo-bloggers often express the wish just to walk and take photos in an unplanned way and blog them immediately, collapsing some potentially inherently satisfying activities into each other. For other individuals, organizing the collection or practical aspects of the hobby might be the main interest. As we have seen, Schwartz ([1987](schwartz87)) reports on the strong friendship ties that are maintained within a camera club.

Digital photography potentially reconfigures some of these processes, particularly by enabling easier and new forms of editing of photos (cf. activity 4 above). Kirk _et al._ ([2006](#kirk)) have coined the term _photowork_ to describe in detail the steps in the typical flow of activities from capture and editing on the camera, through downloading to preparation for sharing. We suggest that **Flickr** further reconfigures the hobby, though it does not add anything entirely new.

Central to **Flickr** are the activities and satisfactions relating to managing one's _photostream_: selecting new photos to add, reviewing them and seeing how others are viewing, commenting or 'favouriting' them. So **Flickr** works mainly in the area defined by activities 6 and 7 above. Our interviewees were quite concerned with generating interest and feedback on their photos, and a key motivation for tagging their own photographs and for being drawn into commenting and group activity was to increase activity on their own photos. One interviewee talked about uploading photos selectively to increase commenting on them, rather than uploading many at once. Thus, users were adopting strategies to generate more interest in their own work, what this interviewee called _exposure_. Another talked about generating _traffic_. So, personal content is managed within a frame of thinking that might be more readily associated with publishing an organizational Website. In this sense the producer-consumer notion within writing about Web 2.0 seems relevant. Since a high proportion of the interviewees had blogs, photoblogs or other personal Websites, or multiple **Flickr** accounts, **Flickr** was part of a wider nexus of self presentation or communication through the Web, with both family and friends, and specific relationships or passing encounters mediated through **Flickr** itself. One interviewee showed concern to manage his image across the whole Internet, looking himself up on Google to check everything was consistent. Although the literature of the Internet seems to increasingly celebrate virtual and weak ties, interviewees tended to stress the continuing importance of communication with friends and family. _'I've got a lot of friends and family all scattered throughout the world and the UK itself so it's a good way of keeping them in touch with what I'm doing [.] and vice versa'_, commented one interviewee. Yet over time the focus does shift to new contacts and primarily photographic rather than personal concerns.

Interviewees managed the bulk of their photos on their own computer (activity 5). Only one interviewee used it as a store for photos. The collection on **Flickr** was usually a selection of the best or most appropriate to be shared. Managing and sifting through one's own collection and maintaining ties might be closely linked, however. One interviewee talked about photography as _'documenting myself'_, that it had _'an archiving feel'_ and of being liberated by the possibility created with the digital camera to take as many photos as he wanted. Some of his photography was about observing the mundane but also he looked back at old photos, reflecting on and coming to understand more clearly an exciting but at the time bewildering experience of travelling and working in the USA. This personal reflection process was also shared with friends and family: _'Every time I go through my photo albums I think I wouldn't mind uploading that to **Flickr** and show my friends that one_'.

Although there were these reflective processes, interviewees generally uploaded photos soon after taking them. **Flickr** was an important motivation and inspiration for taking photos. Most interviewees also logged on frequently, indeed some were continuously logging on during the day because they wanted to look at new activity. One interviewee saw it as '_a release_' while working on a computer all day. So the character of activity 6 is reconfigured as a continuous, low-level, virtual activity, rather than the episodic character of showing for competition in a club.

As an aspect of organizing the collection, tagging was recognised as important but also often seen to be boring or difficult. One interviewee said _'I don't spend a lot of time putting tags on because I don't like them. I find them completely annoying_'. She just put on the first ten terms that came into her head and left it at that. Another found it hard and just put down _'whatever springs into my mind at the time'_. Many acknowledged _laziness_ in adding descriptions. However, another seemed concerned to generate the maximum number of tags. We suggest that tagging, though of interest theoretically for information studies, may not be very important in **Flickr** (at least not to users), because one is not generally searching for an image as such. Purposive searching would imply a specific information need, such as for a photo of a cat on a fence, and **Flickr** is probably more used for browsing. Furthermore, searches can also be on text in titles and descriptions, so tagging is not critical to retrieval.

Most interviewees also saw **Flickr** as a learning experience (cf. 8 above). As one individual commented, "Most of how I do photography: my techniques, coming to find my own style and everything.originated from using **Flickr**." A related use of **Flickr** was as a source for inspiration: "I love seeing what other people are shooting.sometimes I feel like, wow, I wish I was that good, or wow.I want to find out where that is so I can go take photos there." Learning opportunities are expanded through free access to a large pool of photographs, but one can also share one's immediate reactions to the work of others. The scope to form informal groups around particular, often quite narrow, interests is increased because of the global reach of membership. **Flickr** also played a role in respondents' purchase of equipment through direct advice and by enabling researching how others have used a particular camera (cf. 1 above).

It should be noted that users have a lot of choice about how to use the system based upon which affordances offer the most satisfaction, so use of the system is varied. Whereas most interviewees seemed very reluctant to admit editing their photos except in minimal ways (cropping and light correction, for example), even though this could itself be construed as a highly creative process, one interviewee spent a lot of time on this activity. This also seemed to be linked to being less selective about photos to upload to **Flickr**, since anything could be material for editing. Most other users tended to be selective in what they uploaded. Nine of the eleven interviewees had never met anyone face to face whom they had first met through **Flickr**, and the actual taking of photos remained an individual activity. Yet one interviewee clearly saw meeting people as a central pleasure of using **Flickr**. Given the sex ratio in the hobby and among active **Flickr** users, it may be significant that this interviewee was female. She had also been a member of **Flickr** longer than others. Another interviewee emphasised contacting others through **Flickr**Mail, i.e., directly and privately, rather than through public commenting (though he did that too). Another user took particular satisfaction in the efficiency of his method of uploading photos by e-mail. Although the interviewees were more active users, perhaps not surprisingly, only one user was actively using the open application programming interface (API), which, again, is significant at the technical level, but may not be relevant to the vast majority of users.

Methods of navigation in **Flickr** were also quite varied. One interviewee described a pattern of browsing where he started at photos that scored high for _interestingness_, then looking at comments on that photo, and then if someone had made a good comment, navigating to that person's photos. He might go on to look at this person's favourites. _'I will kind of lose myself - woo how did I get here?'_ This is a familiar enough _getting lost in hyperspace_ phenomenon, but none of the interviewees acknowledged any sense of unpleasant disorientation. Another said that he browsed contacts _'because I've added them because I enjoy their photography - so they're the best selection of **Flickr** for me_'. To explore further he would go to the groups and then do searches looking at recently uploaded material which was high on _interestingness_. For him, commenting focused on these users too. _'They're regular visitors to my photostream and I'm regular visitors to theirs'_. Another interviewee navigated primarily by searching or browsing from the latest uploaded photos. Thus, it is characteristic of this sort of richly functional information system that behaviour and use of it is diverse.

### Moral dilemmas

One of the interesting aspects of the system is that as social software its use has strong ethical aspects. Online communities have always required their rules and netiquette, use of **Flickr** trades on these conventions and expands them. There is a strong culture of reciprocity: in return for a comment given, users often felt an obligation to look at the commenter's own photos or say thank you for comments they received. At least one interviewee said frankly that he would not comment on photos of someone who had not commented on his work. That online generosity is premised on reciprocity is well established ([Kollock 1999](#kollock)). We suggest that by now, explicit understanding of this as a principle of behaviour is widespread among net users. All the interviewees were long term net users. However, reciprocation also creates dilemmas and ethical issues. Interviewees acknowledged that some people might "spam" other users by putting on comments in the hope of getting comments in return. When a user chooses to mark another member of **Flickr** as a contact, that other member is automatically notified and asked if they want to reciprocate. This creates the dilemma that one may wish to mark another as a contact (it makes it easier to browse their work), without necessarily wishing them to look at one's own work, which is implied by the invitation to make the reciprocal link.

Interviewees differed about tagging the photos of others, as opposed to tagging one's own. Several saw it as rude, because it was an invasion of one's own space and also because whoever had added the tag was not visible (whereas for most other user behaviour in one's space, e.g., commenting, one can navigate to the profile of the person who did it). Others saw tagging photos of others, e.g., with corrected spellings, as a public good. Issues of access also created moral dilemmas. One interviewee deleted all her private photos rather than delete a former friend from the list of people who could access them. Having said this, use of **Flickr** is not stressful, and this same respondent had the happy experience of being bought a pro account by another user, illustrating the strength of online generosity.

The social order of **Flickr**, though less obvious and restrictive than the photo club identified by Schwartz ([1987](#schwartz87)), is quite specific, embodying values which are summarized by Tik ([2005](#tik)) from users' comments as stressing open entry (Tik's point 42), high activity (44), lack of hierarchy (46), learning (51), diversity (52, 73), creativity (60, 74), and positivity (61). So **Flickr** is enjoyed for the social values which it seems to embody, as much as for whether it has a good interface or is easy to use (though these things are important to users too). We might find these values sympathetic but must still recognise them to be historically and culturally constructed. Unlike the small, ordered universe of the photographic club, **Flickr** is a vast (global), cacophonous moving stream of photographs. There is no aesthetic order or hierarchy imposed by the system itself. There are many access points made available, all of which are valid, and all of which enter into a constantly changing flow of images. Crucially, the designers of the system have chosen to not have any way for collective explicit evaluation of photos, e.g., by voting ([Bausch and Bumgardner 2006: 105](#bausch)), though it does use an unpublished and changing formula to define _interestingness_ (an attribute one can browse by). While different groups within **Flickr** are evolving their own local cultures and conventions, we do want to argue that overall the system has a moral character. It is easy to imagine societies (or social groups) that would find the experience abhorrent as it is organized in **Flickr** for its privileging diversity over order and constant change over expectation, its mixing of expert and novice, and its stress on random encounters. Although this is difficult to encompass within information studies paradigms, it is essential to understanding systems like social software (or online communities).

### **Flickr** and consumption

Schwartz links the institutional order of the camera club to the needs of the industry. At a deeper level, Sontag explores the relation between the use that is made of photography and consumerism. This section explores the link between **Flickr** and consumption. On **Flickr**, individuals' collections are referred to as _photostreams_, implying the need to add new items continuously, rather than, say, maintain a small collection of a limited number of favourites. In group photopools the stress is on the latest photos (not, for example, the best photos) and the navigation to older photos is quite poor, suggesting a deliberate design choice to focus on the new, as a way to stimulate new activity. This seems to chime with the photographic industry's need to stimulate more and more activity. Similarly, photobloggers want to take more photos, the blog is a means to discipline themselves to do so ([Cohen 2005a: 894](#cohena)). It expresses a desire to be addicted. People used to collect cigarette cards (it was closely tied to purchasing a product; the text conveyed patriotic messages; the album _programmed_ the collection, but offered closure). Now people collect images produced by themselves; they collect themselves and there is no end to the collection. One **Flickr** user's ambition is to take 100,000 photos before he dies. It is possible, because he takes 200/300 a day ([Hawk 2006](#hawk)). This seems to be a pervasive discourse in photography, for example lomography (a cult style of photography based on effects originally produced by a Russian company's badly made cameras that leaked light) is also described as addictive ([](bbca)). The conclusion of a history of lomography reads: a "hungry beast with an endless appetite the WorldArchive stands poised under the endless flow of lomographic morsels" ([Lomographic Society International, 2006b](#lomob)). --> It seems possible to link this pervasive discourse of plenitude across amateur photography with the problem of justifying consumerism in a world of limited resources. Industry wants to stimulate consumption. Making consumption visible addresses the guilt of the consumer: it cannot be wrong if everyone is seen doing it, as in the shopping complex. Indeed, it generates guilt about not consuming. The stimulus to taking more photographs that **Flickr** offers seems to be linked to a culture of consumption. Thus, **Flickr** stimulates the taking of photos, which broadly fits the needs of commerce.

There may be some link here to the changed character of the marketplace with digital photography. Digital photography greatly reduces film processing revenue. Yet, if Kodak made money from processing film ([Slater 1999: 294](#slater99)), the figures suggest that this was always a less important source of revenue than the purchase of equipment ([p.298](#slater99)). There has, if anything, been an acceleration of the "permanent technical revolution" in camera design ([Slater 1999: 298](#slater99)) with intense competition in the digital market ([Ang 2005](#ang)). Further, digital photography intensifies the demand for home equipment, though not necessarily just or mainly cameras, requiring the purchase of photo editing software, printers and - always more profitable - paper and toner - plus CDs to store images, an ISP, telephone call costs, a **Flickr** Pro account, one's own domain name. It is often said that with a digital camera one can take as many photos as one likes for "free". But the largely invisible technical infrastructure to support such free self expression is not without cost, nor open to all.

<div align="center">![the ladder of amateur photography](p336fig1.jpg)</div>

<div align="center">  
**Figure 1\. The ladder of amateur photography**  
</div>

Figure 1 represents in a simplified form the typical ladder of upgrading and involvement in amateur photography, with each interviewee presented as an arrow showing their trajectory of involvement. The casual hobbyist may use a mobile camera or point-and-shoot 35mm camera. A key dividing line is use of a single lens reflex camera, which gives the photographer a greater degree of control over exposure, etc. Buying this equipment is an entry point into the realm of the serious amateur. The medium format camera with its high quality lenses is for the serious amateur and semi-professional. Analysing the interviewees' accounts of their history of use of **Flickr**, most told stories of increasing involvement in the hobby, moving from being snap shooters to taking the hobby to be a more serious leisure activity ([Stebbins 2004](#stebbins04)). Indeed, all had recently bought a new camera. Several had or were seeking formal photographic training. **Flickr** encompasses photos of and for the family and friends, photos of touristic travel and more serious expressive uses (often focusing on photographing and re-seeing the mundane). That interviewees were taking the hobby more seriously was marked by a shift of audience away from friends and family to a wider audience of hobby contacts and mass of users; a shift in criteria of evaluation from personal interest in content to "good" photos; and increasing frequency of log-on and uploading. Subtle shifts in behaviour take place. Thus, there was a little evidence to suggest the hypothesis that buddy icons at first, when the stress is on an audience of friends and family, are representations of the self but later, as the intended audience shifts to fellow hobbyists, they are changed to samples of favourite _good_ photos. But interviewees did see quite clear boundaries between different types of photography. For example, one interviewee had three **Flickr** accounts, one for personal travel photos (all of which featured a personal possession, presumably as a stand in for herself), an account where she uploaded all her photos relatively indiscriminately and an account for her best photos within a particular specialist genre. The last was under her own real name. Maintaining the different accounts was a way of marking the boundary between the hobby and snap shooting within her work. We would argue that **Flickr**'s greatest impact was on pulling people into photography as a serious hobby, therefore. Yet only three interviewees were members of photographic clubs and even fewer currently purchased a camera magazine. We suspect, however, that **Flickr** activity will be complementary to existing hobby institutions, rather than a substitute. It is too early to say, as **Flickr** has only existed since 2004, and only two of the interviewees have been members for more than a year.

The way that **Flickr** draws users further into the hobby, partly in a consumerist way, seems to be valid as a broad generalisation, but there were exceptions among interviewees. One interviewee had had considerable media attention to his work, but also prided himself in using very cheap equipment and thus resisting climbing the technical ladder. Another interviewee had had his long standing interest in photography revived by **Flickr**. Yet using **Flickr** had, ironically, brought him back to taking more photos using photographic film, since he still considered the quality far better.

In a recent report to the stock market Yahoo ([2006](#yahoo)) seemed to argue that, in future, full commercialisation of Flickr would be profitable because of its unique content and the ability to target audiences through profiling information.

It is obvious that the attraction of **Flickr** for Yahoo! is the way that by conscious and behaviour-based self-profiling, users are giving away a lot of information about their interests that could have commercial value for targeted advertising. Users tend to try to retain public anonymity by using nicknames for their accounts, displaying distorted images of themselves and not supplying a direct e-mail, yet Yahoo! itself has access to a lot of data about **Flickr** users. In a sense, sites like **Flickr** solve a problem with personalisation: the fact that people tend to lack the motivation to maintain a self-profile, for within **Flickr** there is a strong investment in building a personal profile.

Thirty years ago it would have been convincing to represent the whole area of amateur photography as a mass of passive consumers, devoid of individuality (cf. [Watney 1999: 159](#watney99)), of brands such as Kodak, made universally visible through mass advertising. Potentially. **Flickr** makes the individual the centre of the world and globally visible, while Kodak (and its successors) become invisible. Yet the visibility of the potential consumer is a means by which consumption is maintained. Personalised advertising may be more effective than the broad brushstroke selling of an earlier era of capitalism. The increasingly visible consumers need to discipline themselves to be _creative_, _interesting_, to have _fun_ and be supportive of others in the **Flickr** community. After Foucault, we have to recognise that the desire of users to increase traffic on their own photographs, that is, to be visible, is potentially double-edged and opens them up to surveillance and the self-disciplining pressure that follows.

Clearly, much more data about the connections between the industry and **Flickr** are needed to substantiate this discussion. Furthermore, it is significant that none of the interviewees was concerned with these issues. Nearly all had pro accounts and did not see the unobtrusive advertising associated with some searches for free account users. It remains to be seen whether Yahoo will realize the financial value of **Flickr**, and if they do, whether they can find acceptable ways to do so, which will not damage the culture of freeness and openness. This is, of course, a dilemma that has been seen before with the commercialisation of online communities ([Werry 1999](#werry)). A relatively new feature of the Flikr site is the link from EXIF data to photos taken with the same camera. While this is an obvious channel for advertising, it does provide genuine information. Increasingly, information on the currently most-used cameras on **Flickr** will be commercially valuable. At the moment, all interviewees were very happy with **Flickr**, but the difficulties of extracting one's collection from **Flickr** will create an inertia if commercialisation does begin.

### **Flickr** and the social worlds of amateur photography

It is difficult not to conclude that the critique of Sontag and others is too pessimistic. All the interviewees were very positive about their experience with **Flickr** (though of course it was a sample of active users, who were likely to be positive). One interviewee said:

> Flickr itself feels like a club in a way; I've got everything I need there. I've got the advice, the feedback, the exposure of my photos.

Yet comparing participation in **Flickr** to membership of a photographic club, smaller specialist groups can be viable on **Flickr** because the Internet overcomes problems of geographical dispersal (a classic Internet effect). Whereas for Schwartz a club inevitably has quite a narrow pictorial code, there is much more potential for freedom and choice in the fluid associations of **Flickr** groups and 'me-centred networks' ([Wellman _et al._ 2003](#wellman)). More fluid relations require less conformity of values and therefore a greater likelihood of real creativity. There is genuinely greater scope for individual creativity, releasing the individual to explore their own identity in a way not possible in the narrow world of the photo club, for example (cf. the self actualisation processes identified by Csikszentmihalyi ([1990](#csi90)) and cited by Stebbins ([2004: 7](#stebbins04)) as key to the hobby as central life interest). Davies ([2006](#davies)) argues strongly that using **Flickr** makes available an enriching and social learning experience. Again, Cohen's ([2005a](#cohena), [2005b](#cohenb)) accounts of photoblogging do suggest that it is experienced as a deeply significant activity, not an empty, disconnected one.

We can trace a continuing link between the hobby and commercialisation, but this may not trouble us very much. In the mass scale of its membership and the ability to browse photos by unknown others, in many ways **Flickr** is more like a camera magazine than a club. But unlike a magazine it is free to use, advertising is minimal, and critically, all can participate in adding content in terms of photos and commentary on photos. Whereas a magazine is under editorial control, in **Flickr** the user can effectively construct their own editorial priorities e.g., through whom they make contacts and through browsing strategies. Users can communicate with each other in a way that readers of a magazine cannot. This opens up the potential within groups, for example, to develop radical new practices, including socially engaged ones. In some respects **Flickr** does seem to increase the chance of photography meeting Slater's call for a practice of photographic use offering '_empowerment in the sphere of self representation_' ([Slater 1995: 144](#slater95)).

This paper has argued that it makes most sense to analyse **Flickr** in the context of the social organization of amateur photography as serious leisure. There is much more research to do on **Flickr**: we see the current study as one starting point. We see looking at how particular groups develop new forms of photographic practice as a priority. There is also potential for research looking at how photo sharing reconfigures the uses of snap-shooting photography, especially using camera phones, as part of the maintenance and building of personal networks of relationships, memory, self expression and presentation ([van House _et al._ 2005](#house)). There may also be interesting impacts on how personal collections of memories are stored and used, e.g., how using **Flickr** changes the patterns of control in building photo albums to describe the family.

## Acknowledgements

The authors would like to thank the interviewees for their invaluable contribution to this research. They would also like to thank the anonymous reviewers for their suggestions..

## References

*   <a name="ang">Ang, T.</a> (2005, November, 24). [Seeing the big picture.](http://www.webcitation.org/5UbWtKqdr) _Guardian_. Retrieved 4 January, 2008 from http://technology.guardian.co.uk/weekly/story/0,16376,1648837,00.html (Archived by WebCite® at http://www.webcitation.org/5UbWtKqdr)
*   <a name="bausch">Bausch, P. & Bumgardner, J.</a> (2006). _Flickr hacks: tips and tools for sharing photos online_. Sebastopol, CA: O'Reilly.
*   <a name="bbcb">BBC</a> (2006b). _[In pictures: the future of digital photography](http://www.webcitation.org/5UbX4joit)_, Retrieved 4 January, 2008 from http://tinyurl.com/qjpd2 (Archived by WebCite®: at http://www.webcitation.org/5UbX4joit)
*   <a name="cohena">Cohen, K.R.</a> (2005a). What does the photoblog want? _Media, Culture and Society_, **27**(6), 883-901.
*   <a name="cohenb">Cohen, K.R.</a> (2005, June 11). [Some generalisations, Part 1](http://www.webcitation.org/5UbXKNb8w). _Photos Leave Home_ . Retrieved 4 January, 2008 from http://photosleavehome.blogspot.com/2005_06_01_ photosleavehome_archive.html (Archived by WebCite® at http://www.webcitation.org/5UbXKNb8w)
*   <a name="csi90"></a>Csikszentmihalyi, M. (1990). Flow: the pyschology of optimal experience. New York, NY: Harper Collins.
*   <a name="davies">Davies, J.</a> (2006). [Affinities and beyond! Developing ways of seeing in online spaces.](http://www.webcitation.org/5UbXx5zvg) _E-learning_, **3**(2), 217-234\. Retrieved 4 January, 2008 from http://tinyurl.com/2gutf7 (Archived by WebCite® at http://www.webcitation.org/5UbXx5zvg)
*   <a name="evans">Evans, J.</a> (2006). _The image and visual culture: Block 3_. Milton Keynes, UK: Open University.
*   <a name="garrett">Garrett, J.J.</a> (2005, August 4). [An interview with Flickr's Eric Costello](http://www.webcitation.org/5UbbdyCj3). _Adaptive path_. Retrieved 4 January, 2008 from http://tinyurl.com/ypzzar (Archived by WebCite® at http://www.webcitation.org/5UbbdyCj3)
*   <a name="goggin">Goggin, G.</a> (2006). _Cell phone culture: mobile technology in everyday life_. London: Routledge.
*   <a name="hammond">Hammond, T., Hannay, T., Lund, B. & Scott, J.</a> (2005). [Social bookmarking tools (1): a general review](http://www.webcitation.org/5Ubbmh9yr), _D-lib magazine_, **11**. Retrieved 18 July, 2007 from http://www.dlib.org/dlib/april05/hammond/04hammond.html (Archived by WebCite® at http://www.webcitation.org/5Ubbmh9yr
*   <a name="hawk">Hawk, T.</a> (2006, June 29). [Tom Rafferty interview with Thomas Hawk.](http://www.webcitation.org/5Ubc0Eai8) _Electricnews.net_. Podcast. Retrieved 4 January, 2008 from http://www.enn. ie/frontpage/news-9719655.html (Archived by WebCite® at http://www.webcitation. org/5Ubc0Eai8)
*   <a name="katz">Katz, I.</a> (2006, November 4). [Flickr, Caterina Fake & Stewart Butterfield.](http://www.webcitation.org/5UbcHclyg) _Guardian Unlimited_. Retrieved 4 January, 2008 from http://www.guardian.co.uk/weekend/story/ 0,,1939084,00.html http://www.webcitation.org/ 5UbcHclyg)
*   <a name="kirk">Kirk, D.S., Sellen, A.J., Rother, C. & Wood, K.R.</a> (2006). [Understanding photowork.](http://www.webcitation.org/5UbdBHBNs) In Rebecca Grinter, Thomas Rodden, Paul Aoki, Ed Cutrell, Robin Jeffries & Gary Olson (Eds.). _Proceedings of the SIGCHI conference on Human Factors in computing systems 2006, Montréal, Québec, Canada, April 22 - 27, 2006_. (pp. 761-770). Retrieved July 18, 2007 from http://research.microsoft.com/sds/papers/Kirk_ et_al_CHI06.pdf (Archived by WebCite® at http://www.webcitation.org/5UbdBHBNs)
*   <a name="kollock">Kollock, P.</a> (1999). The economies of online cooperation: Gifts and public goods in cyberspace. In M.A. Smith and P. Kollock (Eds.), _Communities in cyberspace_ (pp.220-239). London: Routledge.
*   <a name="koman">Koman, R.</a> (2005). [Stewart Butterfield on Flickr](http://www.webcitation.org/5UcOE7UXS). _O'Reilly network_. Retrieved 18 July, 2007 from http://www.oreillynet.com/pub/a/network/2005/02/04/sb_flckr.html (Archived by WebCite® at http://www.webcitation.org/5UcOE7UXS)
*   <a name="lomoa">Lomographic Society International</a> (2006a). _[The ten golden rules](http://www.webcitation.org/5UcdVr7vz)_. Retrieved 18 July, 2007 from http://www.lomography.com/about/ index_rules.php (Archived by WebCite® at http://www.webcitation.org/5UcdVr7vz
*   <a name="lomob">Lomographic Society International</a> (2006b). _[The story](http://www.webcitation.org/5UcdrhNZx) _. Retrieved 18 July, 2007 from http://www.lomography.com/about/ index_story.php (Archived by WebCite® at http://www.webcitation.org/5UcdrhNZx)
*   <a name="lom07"></a>[LomoWorldWall. _Trafalgar Square, London, 17 - 23 September 2007._](http://www.webcitation.org/5UcxRv73b) (2007). Retrieved, 5 January, 2008 from http://congress.lomography.com/lomowall (Archived by WebCite® at http://www.webcitation.org/5UcxRv73b)
*   <a name="meyer">Meyer, E.T., & Rosenbaum, H. Hara, N.</a>(2005). _[How photobloggers are framing a new computerization movement](http://www.webcitation.org/5UceJL2OH)_. Paper presented at Internet Research 6.0 - Internet Generations. Retrieved 5 January, 2008 from http://people.oii.ox.ac.uk/meyer/wp-content/uploads/2007/09/meyerrosenbaumhara_photoblog_aoir_paper_v1_5.pdf (Archived by WebCite® at http://www.webcitation.org/5UceJL2OH)
*   <a name="oreilly">O'Reilly, T.</a> (2005). _[What is Web 2.0: design patterns and business models for the next generation of software](http://www.webcitation.org/5UceqnWiU)_. Retrieved 18 July, 2007 from http://www.oreillynet.com/ pub/a/oreilly/tim/news/2005/09/30/what-is-Web-20.html (Archived by WebCite® at http://www.webcitation.org/5UceqnWiU)
*   <a name="porter">Porter, G.</a> (1989/90). Trade and industry. _Ten8_, No. 35, 45-48.
*   <a name="schwartz87">Schwartz, D.</a> (1987). Camera club photo-competitions: an ethnographic approach to the analysis of a visual event. _Research on Language and Social Interaction_, **21**, 251-281.
*   <a name="schwartz86">Schwartz, D.</a> (1986). Camera clubs and fine art photography: the social construction of an elite code. _Urban life_, **15**(2), 165-195.
*   <a name="slate95">Slater, D.</a> (1995). Domestic photography and digital culture. In M. Lister (Ed.) _The photographic image in digital culture_ (pp.129-146). London: Routledge.
*   <a name="slater99">Slater, D.</a> (1999). Marketing mass photography. In J. Evans & S. Hall (Eds.) _Visual culture: the reader_. (pp.289-306). London: Sage.
*   <a name="sontag">Sontag, S</a>. (1977). _On photography_. London: Penguin.
*   <a name="star">Star, S.L. & Griesmer, J.R.</a> (1988). Institutional ecology, "translations" and boundary objects. _Social Studies of Science_, **19**(3), 387-420.
*   <a name="stebbins92">Stebbins, R.A.</a> (1992). _Amateurs, professionals, and serious leisure_. Montreal: McGill-Queens University Press.
*   <a name="stebbins04">Stebbins, R.A.</a> (2004). _Between work and leisure_. Edison, NJ: Transaction.
*   <a name="tik">Tik, J.</a> (2005 May, 20). [Why is Flickr so successful?](http://www.webcitation.org/5Ucg7Oazn) Message posted http://flickr.com/groups/central/discuss/36512/. Retrieved 5 January, 2008 from http://flickr.com/groups/central/discuss/36512/ (Archived by WebCite® at http://www.webcitation.org/5Ucg7Oazn)
*   <a name="house">Van House, N., Davis, M., Ames, M., Finn, M., & Viswanathan, V.</a> (2005). [The uses of personal networked digital imaging: an empirical study of cameraphone photos and sharing.](http://www.webcitation.org/5UchYdU2h) In _CHI '05 extended abstracts on Human factors in computing systems 2005, Portland, OR, USA, April 02 - 07, 2005_. (pp. 1853-1856). New York, NY: Association of Computing Machinery. Retrieved 5 January, 2008 from http://people.ischool.berkeley.edu/~vanhouse/van_house_chi_short.pdf (Archived by WebCite® at http://www.webcitation.org/5UchYdU2h)
*   <a name="watney94">Watney, S.</a> (1994). Photography-education-theory. _Screen_, **25**(1), 67-72
*   <a name="watney99">Watney, S.</a> (1999). On the institutions of photography. In J. Evans & S. Hall (Eds.) _Visual culture: the reader_ (pp.141-161). London: Sage
*   <a name="wellman">Wellman, B., Quan-Haase, A., Boase, J., Chen, W., Hampton, K., de Diaz, I. I. & Miyata, K</a>. (2003). [The social affordances of the Internet for networked individualism.](http://www.webcitation.org/5UchkMd7o) _Journal of Computer Mediated Communication_, **8**(3). Retrieved 18 July, 2007 from http://jcmc.indiana.edu/vol8/issue3/wellman.html (Archived by WebCite® at http://www.webcitation.org/5UchkMd7o)
*   <a name="werry">Werry, C.</a> (1999). [Imagined electronic community: representations of virtual community in contemporary business discourse.](http://www.firstmonday.org/issues/issue4_9/werry/) _First Monday_, **4**(9). Retrieved 18 July, 2007 from http://www.firstmonday.org/issues/issue4_9/werry/
*   <a name="yahoo">Yahoo!</a> (2006). _[Yahoo! Q3 2006 earnings call transcript](http://www.webcitation.org/5UciKbiQU)_. Retrieved 5 January, 2008 from http://seekingalpha.com/article/18639-yahoo-q3-2006-earnings-call-transcript (Archived by WebCite® at http://www.webcitation.org/5UciKbiQU)



## Appendix

## Interview questions

1. When did you start using Flickr?

a. How did you first hear about it?

b. When did you first start using the net?

c. Do you use chat, online communities, blogs?

2. Talk me through what you do when you have some new photos.

a. How do you choose what images to upload?

b. How is your workspace organized to do this?

c. What connection do you have?

d. What day of the week/ time of day would this be?

e. How long would this be after taking photos?

f. Do you edit the photos, e.g., with Photoshop?

g. How do you feel about editing photos?

h. How do you choose descriptions, tags etc?

i. Can you explain the tagging system on Flickr to me?

j. After uploading what do you do? (e.g., browse contacts' photos)

3. How often do you upload photos?

4. How often do you log on to Flickr?

5. Are you a member of any Flickr groups? If so, why did you join them?

6. What goes on in these groups?

7. How do you explore Flickr? (probes: through explore, search) How often do you just randomly look in Flickr, as opposed to uploading?

8. Do you ever explore by tag cloud? What do you think of that?

9. Do you ever browse by interestingness?

10. Are you ever looking for a specific type of photo? Or are you just browsing?

11. Can you think of a successful search you have made: talk me through what happened and why it was successful?

12. Do you ever tag somebody else's photo?

13. How do you feel when you have been browsing Flickr? Do you ever feel disorientated, depressed, anxious?

14. How often do you comment on a photo? Why do you do it?

15. Would it make you more likely to comment if the photographer had commented on one of your photos?

16. Do you see any difficulties in commenting?

17. Who looks at your photos?

18. How did you choose your Flickr ID and thumbnail image? ("buddy icon")

19. What is your most looked at/commented on photo?

20. Why do you use Flickr (probes: to make contact, to keep in contact, to share photos, to make money, to improve photos)?

21. Why do you enjoy using Flickr?

22. Would you say you have learned anything from using Flickr?

23. How would you like Flickr to be improved?

24. Have you met anybody f2f that you first encountered online through Flickr?

25. Have you ever used Flickr for work?

26. Do you want to make money through Flickr?

27. When and why did you go Pro?

28. Do you have any fears about the copyright of your images? (Have you heard of the different types of licence, e.g., creative commons?)

29. Have you ever thought about who runs Flickr and why?

30. Do you ever use the commercial products/services advertised?

31. Have you had any less pleasant experiences with Flickr?

32. What was your stance on the Iraq war? Did you see anything reflected on Flickr?

33. Why do you like Flickr?

34. Where does photography fit into your life?

a. How much time to you spend taking photos?

b. What sort of photographs do you take?

c. Why is photography important to you?

d. What cameras have you got? What was the cost of your latest digital camera?

e. Do you regularly buy a photo magazine?

f. How much do you spend on photography a year?

g. Are you (or have you ever been) a member of a camera club?

h. Have you ever made any money from photography? If not do you see that as a serious possibility?

35. What is the main way Flickr has affected your hobby?

36. Age, occupation, degree

