#### vol. 16 no. 2, June, 2011

# Information behaviour of Australian men experiencing stressful life events: the role of social networks and confidants

#### [Peta Wellstead](#author)  
School of Information and Social Sciences Open Polytechnic - Kuratini Tuwhera, 3 Cleary Street, Waterloo, Private Bag 31914, Lower Hutt, New Zealand 5040

#### Abstract

> **Introduction.** This paper discusses the findings of a qualitative study into the information behaviour of a group of Australian men who had experienced a stressful life event. It examined sources of information and the gaps and barriers experienced in the information search. The information value of social networks and confidants was explored.  
> **Method.** Sense-making interviews were conducted with fifteen men who had experienced a period of significant life stress. A self-report sheet was used to collect additional data from the help-seeking men. A semi-structured interview was held with six professionals who offer information and help to members of the community.  
> **Analysis.** Interview transcripts were examined for themes within the help-seeking episode. A thematic analysis was also undertaken in the interviews with the professionals.  
> **Results.** Results indicate the use of a variety of information behaviour strategies, including avoidance, during stressful life events. A key finding is the important role of women in assisting men's information behaviour both in terms of facilitating help-seeking, and assisting men to stay on task during help-seeking.  
> **Conclusions.** These findings indicate the need for greater understanding of the information behaviour of men experiencing stressful life events. The findings have implications for health policy, particularly the development of support services for men.

## Introduction

Men die in Australia, on average, at 77.8 years, six years younger than the 83.9 years for women (Australian Bureau of Statistics 2010b). Compared with women, men in most age groups have higher mortality rates for stroke, diabetes, cancers, ischaemic heart disease, bronchitis, emphysema, injury, poisoning, accidents and drug dependence ([Verrinder and Denner 2000](#ver00)). Studies both in Australia and overseas have shown that men in countries with a dominant Anglo culture are also more likely than women to have unhealthy lifestyles, drink and smoke excessively, eat a less healthy diet and engage in risk-taking and/or aggressive activities that affect their health outcomes ([Connell 1999](#con99); [Griffiths 1996](#gri96)).

While the statistics for general health outcomes for Australian men are poor, statistics for male suicide, compared to women, are also of considerable concern. The Australian Bureau of Statistics shows that throughout the period 1998 to 2007 the male age-standardised suicide death rate was approximately four times higher than the corresponding female rate: 13.9 per 100,000 standard population compared with 4.0 per 100,000 for women ([Australian Bureau of Statistics 2010a](#aus10a)). Notwithstanding these obvious poor physical and mental heath outcomes, research also shows that Australian men are reluctant to seek help during stressful life events and face particular barriers in accessing information and help at such times, and across the lifespan more generally ([Connell 1999](#con99); [Australia. Department... 2009](#aus09)).

The role of relationship breakdown as a factor in the high rates of suicide for Australian men has been suggested by a number of studies ([Baume _et al._ 1998](#bau98); [Cantor and Slater 1995](#can95)). Cantor and Slater's study found that:

> separated (compared with married) [Australian] males were six times more likely to suicide, and this was greater in younger age groups. Separated female suicide rates were not significantly elevated. [The research suggests that] males may be particularly vulnerable to suicide associated with interpersonal conflict in the separation. ([Cantor and Slater 1995](#can95): 91)

The risk of suicide in men post separation is of particular concern when the issue is considered in light of the occurrence of significantly higher female initiated separation in Australia ([Headley 2006](#hea06)).

There is also considerable community anxiety related to the issue of separated and divorced men who engage in familicide, that is, the murder of children and then subsequent suicide of the father in the context of a dispute or distress over custody and access to children after a relationship has broken down (see, for example, [Harris-Johnson 2005](#har05)).

## Research method and design

This study took a multi-disciplinary study approach to the examination of the help-seeking behaviour of Australian men. An extensive multidisciplinary literature review was conducted which examined the possible historical, sociological, and psychological antecedents to poor health and harming behaviours by Australian men. In particular the impact of social norms ([Cialdini and Trost 1999](#cia99); [Hatch 1989](#hat89)), and construction of masculinity ([Courtenay 2000](#cou00)), were explored as contributors to levels of voluntary help-seeking. Likewise, the role of gender role strain ([Pleck 1995](#ple95)) and the development of personal and cultural masculinity scripts ([Mahalik _et al._ 2003](#mah03)) in the help-seeking experience were also explored.

Help-seeking is a subset of information behaviour. Information needs, seeking and use are a response to the world in which the subject finds himself or herself: there must be a perceived need for information to which there is a consequent response, either to seek out new information, or to ignore that need. By seeking help, individuals are using need as the framework for their information exchange. Using information and in turn increasing knowledge is a transitional process from '_distressing ignorance to becoming informed_' ([Buckland 1988](#buc88): 115)

There is a range of existing theories related to human information behaviour. These include sense-making ([Dervin 1998](#der98); [Dervin and Foreman-Wernet 2003](#der03b)), small world theory ([Chatman 1991](#cha91), [1996](#cha96)) and large group theory ([Schneider and Weinberg 2003](#sch03); [Wilke 2003](#wil03)). There is also a range of graphic models (for example, [Foster 2005](#fos05); [Spink and Cole 2001](#spi01); [Wilson 1999](#wil99)) to demonstrate how these theories operate in the information seeking arena in _real time_. These theories and models inform understanding of men's information seeking for personal decision making and personal change. Case has undertaken an extensive review of information seeking models showing that although sex is the primary focus of a number of information seeking studies '_typically the focus is on women_' ([Case 2007](#cas07): 314), and the few extant studies of the information behaviour of men focus on subgroups such as young homosexuals undertaking the process of coming out (see [Case 2007](#cas07): 307) rather than men dealing with everyday situations. The scholarship contained in this study provides much needed material for the enhanced understanding of the information behaviour of men in everyday settings. This new understanding may be able to be adapted and adopted in a range of contexts.

The multidisciplinary literature review undertaken for this study provided background to the development of a narrative style _sense-making interview_ which was undertaken as the core of the research. Sense-making focuses on how humans make and unmake, develop, maintain, resist, destroy, and change order, structure, culture, organization, relationships, and the self. The sense-making theoretic assumptions are implemented through a core methodological metaphor that pictures the person as moving through time-space, bridging gaps and moving on ([Dervin 2003](#der03): 332).

During the sense-making interview the men were asked to reflect on their help-seeking episode as a serious of incremental steps and to consider their information behaviour in terms of:

1.  the type of information and support used (or avoided) during the help-seeking episode;
2.  the value of this information and support; and
3.  the type of information products and tools they might have used if these had been more readily available.

After the sense-making interview the men were provided with a self-report sheet which could be returned by mail at a later date. This technique was an attempt to gather data that was more reflective and it was considered that the men may have preferred to respond in private. The information related to confidant availability and social network strength. Spencer and Pahl use the work of American epidemiologists ([Cohen _et al_ 1997](#coh97)) to suggest that '_the relative risk from mortality among those with less diverse networks is comparable in magnitude to the relationship between smoking and mortality of all causes_' ([Spencer and Pahl 2006](#spe06): 28). With this in mind examination of confidant availability and social network strength were considered important aspects of the study.

## Participants

The men in the study (n=15) were recruited using the e-mail contact database of a community agency which offers information and support to men. The men who agreed to take part in the study responded voluntarily to the e-mail broadcast. Initially twenty-eight men expressed interest in the research. When provided with further information seventeen agreed to take part. Two of these men cancelled their interview on the day it was to take place, citing pressure of work. Attempts to reinstate these interviews were not successful. Demographic data collected from the fifteen help-seeking men revealed a wide range backgrounds and life experience (see Table 1).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: Verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Demographics of help-seeking men**</caption>

<tbody>

<tr valign="top">

<td>Age</td>

<td>The age range of the men was 32-63</td>

</tr>

<tr valign="top">

<td>Educational level</td>

<td>Varied across a range from completing Year 10 (leaving school age 15) to post-graduate university qualifications;</td>

</tr>

<tr valign="top">

<td>Place of residence</td>

<td>Varied across a range from outer urban/semi-rural to inner city.</td>

</tr>

<tr valign="top">

<td>Employment status</td>

<td>Varied across a range from casual work, home duties, public service, professional, to owning a business</td>

</tr>

<tr valign="top">

<td>Number of children</td>

<td>Only one of the participants did not have children</td>

</tr>

<tr valign="top">

<td>Employment</td>

<td>No men were currently working in blue collar industries, although a number had done so in the past</td>

</tr>

<tr valign="top">

<td>Relationships 1</td>

<td>Only three of the fifteen participants identified having had only one partner during his adult life; two of these relationships were no longer intact (so the men were single again) and the third had separated and reunited</td>

</tr>

<tr valign="top">

<td>Relationships 2</td>

<td>Four of the men had been married or partnered three times or more. [Note: the term partner was used to identify significant relationship bonds; casual 'dating type' relationships were excluded].</td>

</tr>

</tbody>

</table>

The interviews took between 45 and 90 minutes. The interview collected data on the use of formal information and support services, as well as use of informal networks such as family, friends and colleagues during the help-seeking episode. It also sought data on use of information _things_ such as books, pamphlets and websites and awareness of public information campaigns such as television advertising and billboards which seek to alert the community to sources of information and help.

To put the help-seeking behaviour of this group of men into context with help-seeking behaviour of Australian men more generally, the research also canvassed the views of a group of professionals (n=6), public and private, who offer information and help to members of the community facing stressful life events. These professionals were selected to take part because of the profile of the services they were offering, or the focus of their work role in supporting men. Approaches were made to eleven professionals but six of these declined due to pressure of work, a belief that they had nothing to offer to the project, and an unwillingness to take part in an interview process because they didn't feel comfortable in that setting. It was more difficult to recruit private practitioners than those working in community or government agencies.

These participants were recruited by word-of-mouth and through peer networks. The work role of the professionals who took part in the study was varied (see Table 2). Five male professionals were interviewed. Three female practitioners were approached to take part but they declined. The manager of the local government service was a woman.

<table width="50%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: Verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Work role of professionals**</caption>

<tbody>

<tr>

<th>Work role</th>

<th>No.</th>

</tr>

<tr>

<td>Private practice</td>

<td align="center">2</td>

</tr>

<tr>

<td>Private mentoring and support service</td>

<td align="center">2</td>

</tr>

<tr>

<td>Community-based (not for profit) agencies</td>

<td align="center">2</td>

</tr>

<tr>

<td>Local government agencies</td>

<td align="center">1</td>

</tr>

<tr>

<td>State government agencies</td>

<td align="center">1</td>

</tr>

<tr>

<td colspan="2">Note: The number of work roles is greater than the study participants (n=6) as the two private psychologists combined this role with work in a government agency or work in a community based (not-for-profit) agency.</td>

</tr>

</tbody>

</table>

Data were collected by engaging the professionals in a semi-structured interview in which they were asked to consider:

1.  what services were currently being offered to men by them or their agency;
2.  how they market these services; and
3.  if resources (human and financial) were not restrained what type of service would they develop to assist men and how would they market it?

The interviews also gave the professionals an opportunity to reflect on their information provision and support to men, and to discuss matters of general concern about engaging with men and supporting them in times of stress. The interviewers with the professionals took approximately 60 minutes.

## Results

### Phase One: The interview.

The first question in the interview was: _Can you give me an example of a time when you needed information to help you during a significant period of life stress?_ The men indicated that they had previously needed help for to navigate a variety of life situations (see Table 3).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: Verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Issues for which the men needed help**</caption>

<tbody>

<tr>

<td width="112">Participant 1</td>

<td width="445">Relationship breakdown, custody issues</td>

</tr>

<tr>

<td width="112">Participant 2</td>

<td width="445">Relationship breakdown, custody issues</td>

</tr>

<tr>

<td width="112">Participant 3</td>

<td width="445">Relationship breakdown</td>

</tr>

<tr>

<td width="112">Participant 4</td>

<td width="445">Relationship breakdown</td>

</tr>

<tr>

<td width="112">Participant 5</td>

<td width="445">Relationship breakdown, custody issues</td>

</tr>

<tr>

<td width="112">Participant 6</td>

<td width="445">Relationship breakdown, custody issues</td>

</tr>

<tr>

<td width="112">Participant 7</td>

<td width="445">Wife's alcohol addiction</td>

</tr>

<tr>

<td width="112">Participant 8</td>

<td width="445">Custody issues</td>

</tr>

<tr>

<td width="112">Participant 9</td>

<td width="445">Alcohol addiction</td>

</tr>

<tr>

<td width="112">Participant 10</td>

<td width="445">Death of young wife, support with care of 2-year old son</td>

</tr>

<tr>

<td width="112">Participant 11</td>

<td width="445">Relationship issues</td>

</tr>

<tr>

<td width="112">Participant 12</td>

<td width="445">Tried to kill himself twice</td>

</tr>

<tr>

<td width="112">Participant 13</td>

<td width="445">Life threatening illness</td>

</tr>

<tr>

<td width="112">Participant 14</td>

<td width="445">Relocation to Australia to join wife's family and enhance her career</td>

</tr>

<tr>

<td width="112">Participant 15</td>

<td width="445">Relationship breakdown</td>

</tr>

</tbody>

</table>

The second and third questions in the interview asked: _What was the gap you were trying to fill?_ _How did you think the information would help you?_ Often, the responses were inter-changeable between these questions. Responses included:

> I didn't have a clue;  
> I felt like a child crawling in the dark;  
> I was deeply uncomfortable;  
> I wanted more connection

and, significantly:

> I needed to know I was normal.

This sense of normalising the help-seeking experience was spoken of by all the men in the study in various ways.

Other men indicated that their health was the impetus for help-seeking. Responses with this theme included:

> sick of being sick;  
> suicidal thoughts and depression;  
> my wife knew I was ill.

In response to the further question: _Was it difficult to think of places and people who might be able to help you?_ only three of the fifteen men indicated that the task was relatively easy. Two of these seemed to be men with a clear articulation of their needs and with a strong support system, the third had a physical illness and his doctor was the obvious place for help, although he did admit to going to the doctor only after the protracted intervention by his wife.

As part of the interview men were asked to indicate their first major sources of information and help, and then secondary and subsequent sources in response to a question: _What did you do next?_

The primary source of help, or conduits to other help such as doctors and counsellors, were family, friends and colleagues including their spouse (n=11).

Apart from the primacy of family, friends and colleagues in the help-seeking episode other early sources of help included _information things_ such as pamphlets or leaflets, phone books, self-help books and mass media.

Secondary sources of help were often used concurrently with primary sources. For example, men who were talking to friends and family or reading books as a primary source of help did not stop doing those things when they sought help from a secondary source such as counselling.

### Phase Two: Self-completion questionnaire.

The self completion response sheet covered two broad themes: confidant availability and social network strength.

The concepts of confidant availability and social network strength were overlapping and interdependent; the men often included a person named as an available confidant as a person within their social network. This confidant may also have been instrumental in many cases in facilitating a social network and often providing direction to _information things_ such as books or Websites.

#### Confidant availability

These questions asked the participants to identify people who were available to help and support them in times of need and to reportthose to whom they are important in return.

Nine men identified women as their primary confidants and people of emotional importance. In response to the question: _Is there a special person you feel close to?_ all but one man answered _Yes_, with the other answering _Somewhat_. Eleven of these close people were women. Of those men who did not answer _partner or spouse_ (not necessarily live-in) to the question, four identified other females as the person special to them namely, sister, friend or ex-wife.

In response to: _Are you special to someone?_, all but one man answered _Yes_ and many men identified more than one person, with women and children being predominant.

As far as those who have been available to offer support in the last twelve months the primary source of support was women, with several identifying more than one woman (e.g. wife and mother-in-law, partner and sister etc). Other helpers in the last twelve months had been mates, former work colleague, brother and father.

#### Social network strength

In response to the questions enquiring about the strength of their social network the men were asked to identify the number of close relationships they have through a range 1-2; 3-5; 6-10; or more than 10\. The most common response was: 3-5 (n=10).