#### vol. 16 no. 2, June, 2011

# From school to work and from work to school: information environments and transferring information literacy practices

#### [James E. Herring](#author)  
School of Information Studies, Charles Sturt University, Wagga Wagga, Australia

#### Abstract

> **Introduction.** This study, based in a Scottish secondary school, aimed to examine the views of students who were completing work experience, on their information literacy practices, and the differences they found between the school and workplace information environments while on work experience. The views of guidance teachers were also explored.  
> **Method.** The study used a constructivist grounded theory approach to identify areas of exploration. Data were gathered via interviews with students and guidance teachers before and after the work experience period. All interviews were coded.  
> **Analysis.** Constructivist grounded analysis was used to analyse and interpret the data gathered, and to form categories for discussion.  
> **Results.** The findings showed that students' information literacy practices were typical of that found in the literature. Students' observation of the workplace information environment identified key differences between school and the workplace, in terms of the use of technology, sources of information, and the value of information. Students identified areas of transfer from the workplace to school. Teachers were found to lack knowledge of these workplace information environments.  
> **Conclusion.** Focusing on the workplace information environment was a valuable learning experience for students, and could potentially improve their information literacy practices. Teachers could benefit from having a greater focus on workplace information environments.

## Introduction

This study aimed to evaluate the extent to which Year 10 students' information literacy practice in schools was relevant to the workplace information environment. (Year 10 students are aged 14). It also aimed to explore whether some workplace information practices might be transferred back into school. In order to meet these aims, the study sought to explore the views of Year 10 students on their information practices in school and at home, as well as their views on workplace information environments, before they went on a ten day work experience placement. The study then sought the views of these students on school and workplace information environments when they returned from work experience. The views of the students' guidance teachers were also canvassed via pre- and post-placement interviews.

The study took place in Scotland. The school is a state comprehensive secondary school (Years 7-12, ages 11-17) which has 500 students and sixty-four teachers, and is situated in a town with a population of 7000\. Students at the school come from a range of socio-economic backgrounds. They completed their work experience in a variety of workplace environments, including small commercial businesses, large firms, education, healthcare and local government. The aim of the work placement was to give students experience in a workplace environment, but also included experiences such as contacting the placement host, organizing transport to and from the placement, and being in the workplace for seven to eight hours per day.

This paper presents the areas of exploration for the study; a literature review relevant to the topic; the methodology used in the study; the results of the study; a discussion of the results; and a conclusion with recommendations.

## Areas of exploration

As this study used grounded theory techniques (in particular, constructivist grounded analysis), research questions were replaced with areas of exploration. The study did not set out to find answers to particular questions, but to explore relevant areas. The following areas of exploration guided the study:

*   The information literacy practices in which students were engaged in school.
*   The information literacy practices in which students were engaged outside school.
*   Students' knowledge of workplace information environments prior to work experience.
*   Students' knowledge of workplace information environments after work experience.
*   Students' views on transferring information literacy practices from school to work and from work to school.
*   Teachers' views on school and workplace information environments prior to students completing work experience.
*   Teachers' views on school and workplace information environments after students had completed work experience.

## Literature review

This review of the literature focuses on transfer, information literacy practices, information environments, and work experience in schools.

### Transfer

While there has been a debate about the transfer of knowledge and skills in schools [Detterman 1993](#det93), [Haskell 2001](#has01), [Royer _et al._ 2005](#roy05)) since the beginning of the twentieth century, little attention has been paid to the transfer of information literacy skills, abilities or practices in the context of schools or higher education. In the university sector, authors such as Ellis and Salisbury ([2004](#ell04)) argued that students showed little evidence of transferring information literacy skills taught in schools when they came to university. In the school context, Herring and Hurst ([2006](#her06)) examined students' attitudes to the transfer of information literacy in a primary school, and Herring ([2010](#her10)) studied information literacy and transfer in secondary schools. In the workplace context, Smith and Martina ([2004](#smi04)) argued that information skills should be transferable from school to the workplace, but provided no evidence of this happening. Lloyd ([2006](#llo06)) stated that there was little evidence of transfer between school and the workplace, and also argued that many of the information activities that were carried out in the workplace, did not exist in schools. Compared to studies in education, however, little research has been carried out in the information literacy domain.

In education, [Haskell (2001)](#has01) and [Royer](#roy05) _et al._ (2005) discussed many different definitions and types of transfer, as well as environmental, cognitive and sociocultural perspectives on transfer. The benefits of taking a sociocultural perspective on transfer was emphasised by [Volet (1999)](#vol99), and [Royer](#roy05) _et al_. (2005) took a similar view, identifying the need for transfer studies to take account of the influence of the wider environment (e.g. places and people). Haskell ([2001](#has01)) argued that unless a culture of transfer existed in an educational context, then transfer was unlikely to happen.

### Information literacy practices

The term _practice_ is used differently in a range of contexts. In the information seeking domain, there has been recent discussion between Wilson, Savolainen and others ([Behaviour/practice debate 2009](#beh09)) about the uses of the terms _information behaviour_ and _information practice_. Lloyd ([2010](#llo10)) cited the work of Schatzki ([2002](#sch02)) as the key influence on examining the effects of practice on social life, including the educational and workplace contexts. Lloyd argued that, using Schatzki's theories, information literacy can be seen as '_a dispersed practice that hangs together as a bundle of information focused activities that are constituted within larger integrative practices_' ([Lloyd 2010: 249](#llo10)). In this author's view, information literacy practice by school students represents the information related activities in which students are reflectively engaged while at school or outside school. Thus, the term _information literacy practice_, in the lives of school students, does not merely represent students engaging in activities such as Web searching or information evaluation; rather it implies that the students reflect on the use of these activities (for example, by selecting particular search strategies and rejecting others).

The main aspect of information literacy practice examined in the present study was information seeking by students. Kuhlthau's ([2004](#kuh04)) range of research studies focused partly on information seeking and led to the development of the information search process model. Kuhlthau ([2004](#kuh04)) also introduced a new emphasis on the need to consider the affective factors which influenced students' information seeking. [Herring and Tarter (2007](#her07)) reviewed three studies on students' use of Herring's ([1996](#her96)) PLUS (purpose, location, use and self-evaluation) model. These studies showed that while a minority of students had relatively sophisticated information seeking skills, most students had a more casual approach to information seeking. Bilal _et al._ ([2008](#bil08)) examined information seeking by school students in digital libraries, and their conclusions include a reaffirmation of Kuhlthau's ([2004](#kuh04)) findings on uncertainty and satisfaction in relation to information seeking. Alexandersson and Limberg's ([2003](#ale03)) research, which takes a sociocultural perspective, found that students' information seeking often focussed on gathering facts, rather than on deep learning. Limberg _et al._ [2008](#lim08)) argued that in schools there was a need to focus more on learning goals and meaningful learning for students, rather than the more common information skills approach.

Chung and Neuman ([2007](#chu07)) studied the information seeking strategies of high school students, and found that 11<sup>th</sup> grade students' understanding of topics increased during information seeking.Myers _et al._ ( [2006](#mye06)) conducted research into barriers to information seeking in school libraries and identified barriers such as a lack of collaboration between students, and insufficient focus on students' previous information seeking activities. Chelton and Cool ([2004](#che04)) presented a range of research studies, mainly from North America, on how children and adolescents seek information in a variety of contexts. Gross ([2004](#gro04)) examined how primary school children coped with finding information for school assignments. None of the above studies focused on the transfer of information seeking practices.

Students' use of social networking sites was reviewed by Boyd and Ellison ([2007](#boy07)), who also considered a range of studies relating to aspects such as friendship, privacy, offline and online networks used by school students, and impression management.

### Information environments

The term _information environment_ is often used generally. Srinivasan and Pyati ([2007](#sri07)) argued that an information environment relates to the physical or virtual context in which people seek information. Roos _et al._ defined an information environment as

> the entirety of _**information**_ resources, consisting of various _information_ objects, like data in databases, published documents etc., and the tools and services that are needed to retrieve, manage and analyse them. ([Roos _et al._2008](#roo08): 1)

In this study, the information environment is viewed from an information and sociocultural perspective. It is seen as the context in which information needs exist, the sources of information (including people) available, the technologies used to retrieve, organize and manipulate information, the information practices of people, and the culture of information use in that context. The information environments studied here, the school and the workplace, provide two different and contrasting examples.

In the school context, little attention has been paid to the information environment in which students and school staff operate. General works on school libraries byWoolls and Loertscher ( [2005](#woo05)) and Herring ([2004](#her04)) refer to aspects of the school information environment, but this is mainly related to sources of information, the organization of information resources and use of information by students and staff. In the workplace context, the work of Lloyd ([2007](#llo07), [2010](#llo10)) has changed the way in which information literacy and the workplace information environment is perceived. Lloyd's work identifies the importance of people as key aspects of the information environment.

### Work experience in schools

A Scottish Government ([2008](#sco08)) report on work experience argued that students completing work experience in the fourth year of secondary school would benefit from the experience when they entered the workforce at a later date. The report's main points include that schools viewed work experience as providing students with an enhanced knowledge of the workplace; that schools were under pressure to find relevant work experience places; and that state schools could not offer the same level of support to students as private schools because of the pressure of student numbers. The report does not include students' observation of information use in the workplace.

In England, the Department for Children, Schools and Families ([2008](#dep08)) reported that students' work experience consisted of a ten-day placement and outlined standards for work experience. A guide to work experience in schools ([U.K. Department... 2009](#dep09)) outlines expectations for work experience in relation to student learning. As with the Scottish report, there is no reference to students' observations of the workplace information environment.

The Scottish Government ([2008](#sco08)) report states that few countries have compulsory student work experience schemes and cites England, Ireland and Australia as exceptions. In Australia, Lamb and Vickers ([2006](#lam06)) reported on different models of workplace learning in schools and argued that some models promoted better learning within schools, while other models promoted better post-school learning. Hillman's ([2001](#hil01)) report on work experience stated that work experience often encouraged students to stay in school until the end of Year 12 (final year of secondary education). The Australian reports did not cover what students did during their work experience.

The literature relating to transfer, information literacy practices, information environments and work experience demonstrates that while some research has been carried out on aspects relating to the focus of the present study, there has been no comparable research carried out in relation to the information literacy practices of school students completing work experience.

## Methodology

This study took a sociocultural perspective ([Limberg and Sundin 2006](#lim06)) which implies that the researcher views the acquisition of knowledge and students' information literacy practices in the school as being influenced by the school context, including communication between staff and students. The study used a constructivist grounded theory approach (C[harmaz 2006](#cha06)) to identify areas of exploration and to develop interview questions. Constructivist grounded analysis was used to analyse and interpret the data gathered. A constructivist approach implies that the researcher views knowledge, and data collection and analysis, as being constructed by individuals ([Phillips 1995](#phi95)) and that researchers seek to interpret the constructions of reality offered by research participants. Grounded analysis is one element within the grounded theory method. Glaser and Strauss ([1967](#gla67)) were the originators of grounded theory, but it has subsequently been developed by others, including Strauss and Corbin ([1998](#str98)) and Charmaz ( [2006](#cha06)). Charmaz has been particularly influential in the development of constructivist grounded theory. Lloyd ([2007](#llo07), [2010](#llo10)) also used a constructivist grounded theory within a sociocultural perspective for information-literacy-related research.

The participants in the study were Year 10 students and guidance teachers. Ten students were selected from a total of eighty students going on work experience. The four guidance teachers who organized work experience were interviewed. The reason for including both student and teacher perspectives was to examine the extent to which each group had reflected on information literacy practices and the two information environments (school and workplace). The selection of students was done partly through convenience sampling ([Johnson and Christensen 2007](#joh07)) in that students were selected from those who volunteered to be interviewed and were available for both interviews. There was an element of stratified sampling in that teachers selected students with a range of work experience placements, including a small couturier business, a large engineering firm, law courts, a day centre for the elderly, and a veterinary practice. The selection of staff represented all the teachers in the school who were involved in organizing work experience. The data collection technique used in this study was interviews (See [Appendices 1 and 2](#app1) for interview questions). Students and their guidance teachers were interviewed before and after the work experience took place. Both Brenner ([2006](#bre06)) and Johnson and Christensen ([2007](#joh07)) recommend interviews as key tools in gaining significant data from research participants, but also caution that the planning of interviews, the interview questions and the conduct of the interviews must be of a high standard to produce quality results.

Constructivist grounded analysis was used to code and interpret the data. Following Charmaz's ([2006](#cha06)) advice, the researcher used both initial and focused coding to analyse and interpret the data, leading to the establishment of a number of categories that highlight the key findings. The pre-placement interviews with students and teachers were coded and potential categories emerged. After the post-placement interviews were coded, emerging categories were tested in relation to the evidence produced in both the pre- and post-placement interviews ([Pidgeon and Henwood 2004](#pid04)). In a large grounded theory study, categories can lead to the establishment of theory ([Charmaz 2006](#cha06)), but the restricted size of this study does not allow for this. [Appendix 3](#app3) shows the analysis techniques used by the researcher to identify categories from the data.

## Results

The results are presented under the following headings:

*   Pre-placement interviews with students ([Appendix 1](#app1))
*   Pre-placement interviews with guidance teachers ([Appendix 2](#app2))
*   Post-placement interviews with students ([Appendix 1](#app1))
*   Post-placement interviews with guidance teachers ([Appendix 2](#app2))

### Pre-placement interviews with students

In these interviews, students were first asked about aspects of their information literacy practices in schools and, in particular, how they found and used information in the school. One of the categories that emerged from the coded data was sources of information. When students were asked to state how they found out about school events, they noted that they sourced information either from people or from printed sources. Comments included

> If it's something big, it'll usually come through assemblies, there's usually letters at registration for any opportunities you want to take part in.

and

> There's usually stuff up on the notice board, go past in the corridor and you look on the board.

Students distinguished between receiving information directly from teachers such as at the school assembly or in a registration class and printed sources such as posters, and retrieving information in digital form. All students made the point that the school Website did not contain current information about school issues or events, but noted that if it did have relevant information, then they would use it.

Students noted the contrast between people and print sources of information for school-related issues, and the sources they mainly used for their school work. All the students agreed that Google was the starting point for any research, and a typical response was

> I just search on Google, it's pretty easy to find information on the Internet.

Only one of the ten students used Advanced Search in Google although all students knew about this facility. One of the categories identified was that all students were aware of other search engines and named them, but could not see past Google. One student commented

> Yes, we've been told about others like MSN, Dogpile, Yahoo and others, but I stick to Google.

Another student pointed out that Google appeared on the school computers when students logged in. The use of Google by these students revealed reasonably sophisticated use and all students noted that their searches were combinations of keywords and/or phrases. Comments included

> I typed in **Lockerbie bomber arguments** because if you just type in **Lockerbie bomber**, it would usually give you facts, not arguments.

Students used specific sites for school work and four students referred to Websites such as online newspapers, the BBC and Wikipedia.

It was noticeable that this group of students were mostly irregular or reluctant users of the school library. Only three out of the ten students stated that they would regularly come to the school library and only one student referred to asking the school librarian for help. Five out of the ten students referred to using books from the library, but noted that this was unusual, as they believed that they could find more information on the Web. One of the five students commented

> I have used books but I think Websites are going to give me more up to date information, and Google of course gives you a much wider range [of information] than books.

These students appeared to view the library as one of a range of sources of information but not their primary source.

When students were asked about information literacy practices at home, there was a consistency of responses in that all students stated that they used Google as their main source for school work. These students saw no difference in approach to finding information at school or in the home. Only one student book-marked sites which might be useful for school work and a typical response on this issue was

> I know about saving sites and using other search engines but Google, well, it's just so easy.

All students stated that they used a computer at home to find information related to social aspects, as opposed to school work. An interesting difference in approach to school work was that nine out of the ten students identified favourite sites that related to their social interests, which included sport, fashion and music. One student commented

> I look for music on iTunes and I've got my shop Websites I usually go on to see what there is.

All of the students used or had used social networking sites to some extent. The most cited was _[MSN Messenger](http://explore.live.com/windows-live-messenger?os=other)_, with nine out of ten students stating that they used this for talking to their friends, either in video or audio format. For most students, _MSN Messenger_ was used only for social chat but one student commented

> It's quite good if you've got any problems with your homework and you've got friends in the same classroom, then I can ask.

Other sites noted by students were _[Facebook](http://www.facebook.com/)_, _[Bebo](http://www.bebo.com/)_ and _[MySpace](http://www.myspace.com/)_. None of the students mentioned using e-mail either at school or at home as a means of contacting others.

Students were then asked questions about how they thought the people in their work experience organizations would find and use information. Nine out of the ten students had no apparent experience of the workplace, and expressed uncertainty about the information practices of people in different work situations. Students therefore speculated about where people in the workplace would find information, and comments included

> I'm not sure, maybe from customers about the cars being hired? I suppose they'll have a database?

Another student stated:

> I think she'll [the couturier] get it from Websites, from the Internet. I'm not sure, I think that's what she'll use.

Four of the students commented that workers probably received information from other people in the workplace, but there was uncertainty about the nature of such information exchanges. In terms of information formats in the workplace, students also expressed uncertainty and tended to use vague phrases as '_from the computer_' or '_maybe they have books to help them_'.

When asked about how they thought information literacy practices would differ in the workplace from school, students were equally uncertain although they did speculate on some differences, including:

> They might have access to more things that we don't have, the resources, they have more money that allows them to access different things that we can't.

and

> I think the [large engineering] company would have much more sophisticated ways of getting information than just Google for instance.

Overall, there was a distinct lack of knowledge on the part of students about workplace information practices.

### Pre-placement interviews with guidance teachers

The four guidance teachers were firstly asked about the purposes of work experience for their students and were unanimous in their response. Teacher A summed up the group's response, stating:

> I think the main purpose for us is to give the youngsters an experience of something very different to school, working with adults, working in an adult-working environment,

with teacher B adding:

> It introduces students to a new learning environment. They are joining the workplace and we want them to have a good experience. Meeting new people in a secure environment and being open to new ideas.

All teachers stressed that one of the benefits of work experience was that students often returned to school feeling more confident about themselves and their abilities. Teachers C and D indicated that students not only learned about the workplace environment, including ways of working, responsibilities, attitudes, but also about themselves.

When asked about students observing the information environment in the workplace, all teachers agreed that this had not been a focus. Students were given a workbook in which they were expected to observe aspects of the workplace, but information literacy practices were not part of that workbook. Three of the four teachers argued that, in future, a focus on the information environment would be a very useful part of the workbook. Teacher B stated

> Yes, definitely, I think that's something that would be a good addition actually. To focus on what you call the information environment and to report on it when they come back.

It was clear that the teachers had a rather vague idea of what the term _information environment_ implied, and this was reflected in their replies about what students might learn from the workplace information environment. So, when asked about different kinds of information in the workplace, the teachers gave generalised replies, for example referring to legal information or technologies such as computer-aided design. Teacher A was the only teacher to refer to there being more face-to-face information in the workplace.

When asked about what students might learn from the workplace information environment, the teachers also gave rather generalised replies, and appeared to be unsure what students could learn. Two of the teachers referred to information systems, stating that students might learn about systems different from school or might have experience of particular software such as sophisticated databases or advanced engineering software. Teacher A was again the only teacher to refer to how people used information in the workplace, stating:

> I think that they might learn that people think about information differently in the workplace; you know, how people rely on key information to do their jobs.

The teacher interviews carried out before students went on work experience showed that teachers were very supportive of their students and provided the students with relevant information. The teachers clearly had little experience of considering the workplace information environment, and what students might learn about this environment while on placement had not been a focus of attention for teachers. All the teachers agreed that there should be more focus on the information environment in the future.

### Post-placement interviews with students

The coding, analysis and interpretation of the second interviews with students produced categories that reflected what the students learned from their work experience and what differences they saw between the school's information environment and the workplace information environment. The categories identified were: using technology, sources and formats of information, the importance of information, and, applying what was learned in school.

#### Using technology

Some of the students were introduced to new computer systems and used software which was not available in school. Four students who worked in organizations where databases were used commented that using a database was a new experience for them. One student said:

> The company relied almost totally on their database, for staff giving answers to customers or finding the right car for a customer. I was amazed how this database was so important.

This comment summed up the views of all four students, who also commented on the differences between using the Web and using a database. A second student stated:

> Their database was brilliant, so much more organized than the Internet.

Seven out of the ten students said that people in the workplace used e-mail as a key aspect of computer use. All students in the school have e-mail access but few students used it. A student who worked in an engineering company expressed a common view amongst students:

> At first, I couldn't get over how _often_ [student's emphasis] they checked their e-mail. A lot of what they did involved e-mailing.

Students also commented that people in the workplace contacted other companies or organizations mainly by e-mail.

A second technology which all students commented on was the phone, either in the office or out of the office using a mobile phone. All students noted that this was a major difference from their work in school. A student who worked with an engineer who was fitting fire alarms and smoke detectors, commented:

> I reckon the engineer using the phone a lot more than I expected was an eye-opener for me. He had a lot of phone calls from trainee engineers for advice on a job they had. And he got most of his information on the phone.

Most students were surprised that people in the workplace did not use the Web more. Only two of the students, both of whom worked with one person companies, noted extensive use of the Web to find information. The key difference that students noted between school and the workplace was that in the workplace, people relied much more on internal sources of information such as databases and intranets. Only three of the students, including one who worked in a day-care centre, commented that the people with whom they worked could gain more information from the Web.

#### Sources and formats of information

Eight of the ten students expressed surprise that people in the workplace used fellow employees or external contacts as the main source of information to solve problems or to carry out workplace activities. Students argued that in school, exchanges of information did take place, particularly between teachers and students, but these exchanges were mostly person (teacher) to group (students) and rarely person to person. In the workplace, students found that the key source of information was often other people rather than a computer. A student who worked in a veterinary practice expressed a typical view, stating:

> They used other people much more than I thought they would. They had a cat with jaundice so they had to phone around different vets to see what they thought.

In terms of formats of information, some students noted that there were more printed sources of information than they had expected. The two students who worked with one person companies stated that their hosts used much personal information, for example in the form of notes taken from phone conversations or from Web searches. These students commented that this was similar to their own note-taking in school. The student whose placement was in the local law courts was most surprised at the amount of printed material, commenting:

> All the information for the courts, it's all printed out on paper, big stacks of paper.

#### The importance of information

One of the key aspects of the workplace information environment that students noted was the importance of information to the organizations in which they had gained work experience. Students who worked in commercial companies, in engineering, electricity, car hire and veterinary practice, all commented on how they had discovered that information could be viewed as an economic commodity. The engineering company student compared the importance of information in school and in the company, stating

> If there was a fault and they couldn't get the right information to correct it then they would be losing a lot of money. I might lose marks in exams, but they could lose millions of pounds.

Students also commented on the importance of information being immediate, compared with the school situation, where information for school work could be gathered over time. Six students argued that the people with whom they worked needed to find the right information within a very short time: for example, to solve an engineering problem, to get the right component, to find a suitable car for a client, to treat an animal, or to source a dress for a client. This compared with the school information environment where, for students, information could be sourced over a period of days. One student commented that she might ask the librarian for relevant Websites and return the following week for the recommended sites.

Other aspects of the importance of information were related to legal and ethical issues and some students commented that the use of company or organizational databases ensured that information was kept within the organization and therefore confidential. The law court student was the most pertinent example, commenting

> Information is more important, not because it's a job, but because it's dealing with people's lives, so there's a lot of consequences. Information has to be right, it's pretty much crucial. Here [in school] you don't actually have to know it, well you do, but it's not the end of the world if [you] don't. But at the court, you have to know all the information, and everything has to be taken into consideration. If you make a mistake with information there [in court], you could change someone's life.

#### Applying what was learned in school

The students expressed a range of aspects of information literacy practice in explaining what they had learned from their work experience. The most common aspect identified was the need to use a wider range of sources to find information. Comments included

> I have to make sure I have sources that can't be biased, to make sure that I've got the right stuff (hire car student)

Related to using a wider range of sources was the need to view other people as sources of information. The workplace experience had shown these students how there was a greater dependence on people as sources of information than in school. Comments included:

> I get nearly all my information from computers, whereas she uses most of all of hers from other people, by talking to them directly, or on the phone. I think I could use people more. (Couturier, one person company, student)

A third aspect of the workplace information environment on which students commented was the use of e-mail. Six out of the ten students argued that they could use e-mail more, particularly if they had a problem with school work. They argued that, although students had e-mail access, there was not a culture of using e-mail in the school, and this led students not to use e-mail in school. These students stated that they intended to use e-mail more as a way of finding information. The electrical engineering company student summed up the general view:

> We've got e-mail set up, everybody in the school has e-mail, so if you were having a problem you could e-mail all of your friends and they could pick it up and e-mail you back the solution to your problems.

The final element in the category of students applying what they had learned in the workplace was related to the importance of information discussed above. Eight students made comments about having a more serious attitude to finding and using information. The law court student reflected the general view:

> Getting information right is obviously more serious there [in court]. It's not as laid back as in school. In school you're in your comfort zone. At work you feel a lot more grown up, so I think I need to maybe take a more serious look at getting information in school.

The post-placement interviews with students revealed that students had been exposed to a workplace information environment that was, in many ways, different to that in their school. It was clear that students were able to evaluate key aspects of the workplace information environment and might envisage the possibilities of transferring some aspects of what they had learned to their work in school.

### Post-placement interviews with guidance teachers

The categories that emerged from the second interviews with teachers were: the workplace information environment, work experience and student learning, the transfer of skills, and future focus on workplace information.

#### The workplace information environment

After the work experience period, the teachers held debriefing sessions with all students, in class groups where students worked in pairs and then larger groups. From student feedback, the teachers appeared to have a better sense of what might constitute an information environment in the workplace. As in the first interviews, the teachers all noted that, for some students, the computer systems that they encountered on placement would be different from that in school. The teachers gave examples of company databases and specialised software in engineering or design. The teachers also commented that students had found much more face-to-face communication in the workplace than they did in school, particularly in terms of information gathering. Teacher C said that in some debriefing sessions, students had noted that in some workplaces, people used computers much less than they had expected, with more information gathered from other people. A third element commented on was the importance of information in the workplace. Two of the four teachers commented directly on students finding that information in the workplace was taken more seriously than in school. Teacher B stated:

> I think they would realise just how vital it is to be accurate and meticulous about their information gathering

and compared this with the students' more relaxed attitude at school.

#### Work experience and student learning

The teachers were asked what they thought students might have learned about the workplace information environment. All four teachers stated that they hoped that students would have learned more about communication between people and, in addition to the comments above on face-to-face communication, teachers emphasised that learning about interpersonal sharing of information was important. Teacher C also stressed that students learned about how information gathering affected others in the workplace, stating:

> I think they learned that having the right information affected other people, what other people do, what their jobs were. If they don't get the right information for their homework it's only impacting on them.

Teachers A and D noted that students learned that people in the workplace referred to experts to find necessary information, and that some students came back from placement with a new attitude to asking for information from their teachers, the teacher librarian and other students. Teacher D commented:

> I think that some learn about asking the right **person** <span style="font-style: normal">[teacher's emphasis]</span> and they bring this back to school. Some definitely come back with a different attitude to asking for information.

#### Transfer of skills

The teachers were divided in their attitudes to whether students might transfer skills learned in the workplace. They agreed that not all students would transfer skills, as some were not motivated to do so. However, they disagreed about the sustainability of transfer. While Teacher B argued that some students did transfer skills, such as gaining information from people, or attitudes, such as viewing information accuracy as very important, Teacher C questioned whether the immediate transfer would be sustained.

Teachers were more in accord in relation to the transfer of attitudes and confidence from the workplace to the school. Teacher C thought that when many students returned to school, their attitude to finding and using information had changed, stating:

> One thing they bring from the workplace is that when they look round their peers, they realise that they've learned that if they want to get further, they have to do something about it [finding and using information] themselves.

#### Future focus on workplace information

All the teachers agreed that, in future, requiring students to assess their placement's workplace information environment would be beneficial. Students completed a workbook on placement and the teachers thought that including a section on the organization's information environment would enhance student learning. Teacher A argued that students were unlikely to evaluate the information environment unless they were specifically asked to. Teachers C and D stated that it would be very useful students for to compare information environments after placement, for example to compare the information environment in a day-care centre with that of a large company.

The teacher interviews revealed that teachers, possibly as a result of the first interviews, focused more on what students might learn about the information environments in their students' workplaces. It was also clear, however, that while teachers had gained general impressions from student debriefing, they lacked detailed knowledge of external workplace information environments.

## Discussion

The analysis and interpretation of the data from students and guidance teachers led to the identification of key issues. The issues discussed in this section are students' information literacy practices, student and staff views on the school and workplace environments, and student and staff views on transfer of skills.

### Students' information literacy practices

As was noted above, the focus of this study was not on students' overall information practices but mainly on their information seeking and use. The students noted that their main source of information for school activities came from school assemblies, noticeboards and other students (i.e. non-electronic sources). The school Website was not seen as a reliable vehicle for school activities, reflecting the school's information and communications technology culture where there is wide provision of hardware, a school network with high speed Web access, but no emphasis on using the school Website as an effective internal communication tool. The student data relating to students' use of information sources in the school revealed a reluctance to use the school library and a preference for using online information, in particular the Web. There was also evidence of the use of effective search strategies, both at school and at home. This study thus provided similar findings to those of Bilal _et al_. ([2008](#bil08)) and Chelton and Cool ([2004](#che04)), who found that students had a preference for Google above other search engines and that most students rarely used the advanced searching. One aspect of students' use of the Web in this study, which was not found in the reviewed literature, was that students did not book-mark sites for school use, but did for recreational information at home. This may show a lack of emphasis in the school on book-marking, or it could reflect an attitude amongst students that social use of the Web encouraged them to be more innovative.

These findings raise the question of whether these students are being taught how to develop effective search strategies or to use tools such as advanced searching, and have implications for the teaching of information literacy practices in school. It is possible that schools might include not only a more structured approach to teaching information seeking and use, but also include aspects of workplace information literacy practices as examples for students to follow.

### The school and workplace information environments

This study appears to be unique in examining students' and teachers' views of the school and workplace information environments, as the reviewed literature did not reveal similar studies. It was clear from the pre-placement interviews that neither students nor guidance teachers had given much consideration to the workplace information environment. For students this is perhaps understandable, given their age and lack of experience. For the teachers, however, who are active participants in a workplace environment, it may seem strange. On the other hand, it may be that most people in a workplace information environment take the elements of that environment for granted. Only one of the teachers observed that students would find the workplace information environment different because of the emphasis on obtaining information from people as opposed to the Web. Other teachers cited the use of a wider range of technology to provide information and this partly reflects the views of Roos _et al_. ([2008](#roo08)) on information environments. This emphasis by most teachers on technology in the information environment may reflect an information sources-based approach by teachers, rather than a sociocultural approach, which would focus more on the workplace as a learning environment, in which information literacy practices were viewed as essential in meeting organizational aims.

As noted above, there appears to be no comparable literature on students' experiences of, and reflections on, workplace information environments. Work experience studies by the Department for Children, Schools and Families ([2008](#dep08)) and Lamb and Vickers ([2006](#lam06)) did not focus on information environments. The findings of this study revealed some interesting insights by students into workplace information environments, and these insights show that one of the benefits of work experience for students is to heighten their understanding of how information is sought and used. The findings relating to the use of technology in the workplace may have been anticipated, in that some of the organizations into which students were placed had sophisticated information systems to support their workforce, and this was recognised by both staff and students. One new element observed by the students was the limited use of the Web as a key source of information, with workplace staff relying more on technologies such as databases, e-mail and mobile phones. For students, this represented an entry into an information culture different from their own, and one from which they could learn.

In relation to sources and formats of information the use of the workplace phone (both static and mobile) surprised a number of students. Phones were used to obtain information and to exchange information, as well as for general communication. Given that these students were all regular users of mobile phones for recreational purposes, the observation of phone use at work proved to be a valuable learning experience for many of the students. The use of other people as a key source of information was also found to be a revelation to students, and they identified this as a major difference between school and the workplace. These students observed much person to person information exchange in the workplace, and noted the difference between this and the more familiar person to group exchanges in school. These students thus experienced a different culture of communication in most workplaces. This reflects the findings of Lloyd ([](#llo07)2007, [](#llo10)2010) whose studies of workplace information literacy identified much less reliance on electronic sources of information. Students also noted that the use of printed sources was more widespread than they expected (for example, in law courts). Students' preference, in both their social and educational spheres, for digital information sources was therefore not reflected in the workplace.

Information as a key factor, particularly in commercial organizations, was a revelation to some students who had not previously considered how up-to-date information could be vital to the existence of companies. Students also observed that a lack of accurate or up to date information in both the commercial and the public sector could lead to loss of business or a drop in quality of service. Information was seen to be a commodity by some students and this reflects the findings of contributors to Cronin ([2006](#cro06)). This is another example of students experiencing a different information culture from that of the school where a lack of timely or accurate information was not crucial for students.

It can be argued that the students, as opposed to their teachers, took a more sociocultural view of the workplace information environment, as they reflected on a range of personal and group communication that involved the use of technologies such as the computer and the mobile phone, as well as face to face exchanges of information. This may have been stimulated by the questions asked in the pre-placement interviews with the researcher.

### Transfer

While most students were confident that they would transfer aspects of what they learned in the workplace back to school, teachers were less convinced. Some of the guidance teachers questioned the sustainability of the transfer of skills and attitudes. Students argued that they would transfer some attitudes to information (for example, by viewing accurate and up-to-date information as much more important than they had previously). Students may be seen as not only transferring information literacy practices, but also bringing elements of the workplace information culture back to the school. Students stated that they intended to use people more as sources of information, and to communicate with people, such as fellow students, more by e-mail. Students' _intentions_ to transfer reflect the findings of Herring ([2010)](#her10) who observed a majority of students as being prospective rather than actual transferrers of information literacy skills. There did not appear to be a culture of transfer in the school under study, and Haskell ( [2001)](#has01) and Herring ([2010](#her10)) identify the existence of a culture of transfer as being a key factor in determining whether students will transfer knowledge and skills. The work experience of students in this study provided them with a relatively brief entry into different information environments and information cultures. For these students to sustain the transfer of attitudes and practices from the workplace back into the school may require a change in the information culture of the school to one where there is greater emphasis on teaching students to value information more and to reflect on their information literacy practices. It may also require a change in teachers' and school management's attitudes to the school's information culture, including making better use of the school Website and encouraging more use of e-mail for educational purposes.

## Conclusion

This study has shown that work placement for Year 10 students in one Scottish school proved to be a valuable learning experience by expanding the students' knowledge of how workplaces functioned, and in particular, how workplace information environments differed from that of the school. Future research in this area could replicate this study in a number of schools, particularly in countries such as the UK and Australia that have extensive work experience programmes. Possible implications for the library and information sector are that teacher librarians might focus more on developing students' ability to create effective search strategies. Teacher librarians might also be influential in advising guidance teachers of the benefits to students that would arise if students were asked to focus more on the workplace information environment while on work experience. Finally, this study may imply that a greater recognition of the importance of students transferring information literacy practices is needed in schools.

## About the author

Dr James Herring is part of the teacher librarianship teaching team at Charles Sturt University, Australia. James teaches online from his home in Dunbar, Scotland, for all but six weeks of the year. He is the author of ten books on information literacy, ICT in schools, and school libraries. His new book is _[Improving students' Web use and information literacy](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.facetpublishing.co.uk%2Ftitle.php%3Fid%3D743-2%26category_code%3D825&date=2011-04-11)_ He can be contacted at [jherring@csu.edu.au](mailto:jherring@csu.edu.au)

#### References

*   Alexandersson, M. & Limberg, L. (2003). Constructing meaning through information artefacts. _The New Review of Information Behaviour Research: Studies of Information Seeking in Context._ **4**, 17-30.
*   [The behaviour/practice debate: a discussion prompted by Tom Wilson's review of Reijo Savolainen's '_Everyday information practices: a social phenomenological perspective_'. Lanham, MD: Scarecrow Press, 2008.](http://www.webcitation.org/5xfn9nKdF) (2009). _Information Research_, **14**(2) paper 403\. Retrieved 7 December. 2010 from http://InformationR.net/ir/14-2/paper403.html (Archived by WebCite at http://www.webcitation.org/5xfn9nKdF)
*   Bilal, D., Sarangthem, S. & Bachir, I. (2008). Toward a model of children's information seeking behavior in using digital libraries. In P. Borlund, J.W. Schneider, M. Lalmas, A. Tombros, J. Feather, D. Kelly and others (Eds.), _Proceedings of the Second International Symposium on Information Interaction in Context_ _London, United Kingdom_ (pp. 145-151). New York, NY: ACM Press
*   Boyd, D. & Ellison, N. (2007). [Social network sites: definition, history, and scholarship](http://www.webcitation.org/5xiU7ZpkV). _Journal of Computer-Mediated Communication_. **13**(1), article 11\. Retrieved 7 December, 2010 from http://jcmc.indiana.edu/vol13/issue1/boyd.ellison.html (Archived by WebCite at http://www.webcitation.org/5xiU7ZpkV)
*   Brenner, M. (2006). Interviewing in educational research. In J. Green, G. Camilli and P. Elmore (Eds.), _Handbook of complementary methods in educational research_ (pp. 357-370). Mahwah, NJ: Lawrence Erlbaum.
*   Charmaz, K. (2006). _Constructing grounded theory: a practical guide through qualitative analysis_. London: Sage.
*   Chelton, M. & Cool, C. (2004) _Youth information seeking behavior: theories, models, and Issues_. Lanham, MD: Rowan and Littlefield.
*   Chung, J. & Neuman, D. (2007) High school students' information seeking and use for class projects. _Journal of the American Society for Information Science and Technology_, **58** (10), 1503-1517.
*   Cronin, B. (Ed.). (2006) _Annual review of information science and technology_. Medford, NJ: Information Today Inc.
*   Detterman, D. (1993) The case for the prosecution: transfer as an epiphenomenon In: D. Detterman and R. Sternberg (Eds.), _Transfer on trial: intelligence, cognition and instruction_ (pp. 1-24). Norwood, NJ.: Ablex Publishing.
*   Ellis, J. & Salisbury, F. (2004). [Information literacy milestones: building on the prior knowledge of first year students](http://www.webcitation.org/5xiVNrLWX). _Australian Library Journal_, **53**, 4\. Retrieved 7 December 2010 from http://alia.org.au/publishing/alj/53.4/full.text/ellis.salisbury.html (Archived by WebCite at http://www.webcitation.org/5xiVNrLWX)
*   Glaser, B. & Strauss, A. (1967). _The discovery of grounded theory: strategies for qualitative research_. Chicago, IL: Aldine.
*   Gross, M. (2004) Children's information seeking at school: findings from a qualitative study. In M. Chelton and C. Cool (Eds.), _Youth information-seeking behaviour: theories, models and issues_ (pp. 211-240). Lanham, MD: Scarecrow Press.
*   Haskell, R. (2001) _Transfer of learning: cognition, instruction and reasoning_. San Diego, CA: Academic Press
*   Herring, J. (1996) _Teaching information skills in schools_. London: Library Association Publishing.
*   Herring, J. (2004). _The Internet and information skills: a guide for teachers and school librarians_. London: Facet Publishing.
*   Herring, J. (2010). School students, question formulation and issues of transfer: a constructivist grounded analysis. _Libri_ **60**, 218-229
*   Herring, J. & Hurst, J. (2006). [An investigation into the extent to which Year 6 students transfer information literacy across subjects](http://www.webcitation.org/5xiWFZey6). In J. McGregor and L. Hay (Eds.), _Research in Teacher Librarianship: Proceedings of the Centre for Studies in Teacher Librarianship Research Conference, Australian National University, Canberra, 9-10 April 2005._ Retrieved 7 December 2010 from http://www.csu.edu.au/faculty/educat/sis/CIS/epubs/CSTLpapers.htm (Archived by WebCite? at http://www.webcitation.org/5xiWFZey6)
*   Hillman, K. (2001). _Work experience, work placements, and part-time work among Australian secondary school students_ Melbourne, Australia: Australian Council for Educational Research. ( LSAY Briefing No. 3).
*   Johnson, B. & Christensen, L. (2007). _Educational research: quantitative, qualitative, and mixed approaches_. Thousand Oaks, CA: Sage.
*   Kuhlthau, C. (2004). _Seeking meaning: a_ _process approach to library and information services_ (2<sup>nd</sup> ed.). Santa Barbara, CA: Libraries Unlimited.
*   Lamb, S. & Vickers, M. (2006). _Variations in VET provision across Australian schools and their effects on student outcomes_. Melbourne, Australia: Australian Council for Educational Research (LSAY Research Report No 48).
*   Limberg, L. & Sundin, O. (2006). [Teaching information seeking: relating information literacy education to theories of information behaviour](http://www.webcitation.org/5xieJYEDq). _Information Research_, **12**(1) paper 280\. Retrieved 7 December 2010 from http://InformationR.net/ir/12-1/paper280.html (Archived by WebCite at http://www.webcitation.org/5xieJYEDq)
*   Limberg, L., Alexandersson, M., Lantz-Andersson, A. & Folkesson, L. (2008). What matters? Shaping meaningful learning through teaching information literacy (1). _Libri_, **58**, 82-91.
*   Lloyd, A. (2006). Drawing from others: ways of knowing about information literacy performance. In D. Orr, F. Nouwens, C. Macpherson, R. Harreveld, and P. Danaher (Eds.), _Lifelong learning: partners, pathways and pedagogies: keynote and refereed papers from the 4<sup>th</sup> International Lifelong Learning Conference, Yeppoon, Central Queensland, Australia._ Rockhampton, Australia: Central Queensland University Press.
*   Lloyd, A. (2007). [Recasting information literacy as sociocultural practice: implications for library and information science researchers.](http://www.webcitation.org/5xieNtEF6) _Information Research_, **12**(4). Retrieved 7 December from http://informationr.net/ir/12-4/colis/colis34.html (Archived by WebCite at http://www.webcitation.org/5xieNtEF6)
*   Lloyd, A. (2010). Framing information literacy as an information practice: site ontology and practice theory. _Journal of Documentation_ **66**(2), 245-258.
*   Myers, E., Nathan, L. & Saxton, M. (2007). [Barriers to information seeking in school libraries: conflicts in perceptions and practice](http://www.webcitation.org/5xiivFuYX). _Information Research_, **11**(2). Retrieved 7 December from http://informationr.net/ir/12-2/paper295.html (Archived by WebCite? at http://www.webcitation.org/5xiivFuYX)
*   Phillips, D. (1995). The good, the bad and the ugly: the many faces of constructivism. _Educational Researcher_, **24**(7), 5-12.
*   Pidgeon, N. & Henwood, K. (2004). Grounded theory. In M. Hardy and A. Bryman (Eds.), _Handbook of data analysis_ (pp. 625-648). London: Sage.
*   Roos, A., Kumpulainen, S., J?rvelin, K. & Hedlund, T. (2008). [The information environment of researchers in molecular medicine](http://www.webcitation.org/5xieZtodg). _Information Research_, **13**(3). Retrieved 07 December from http://informationr.net/ir/13-3/paper353.html (Archived by WebCite at http://www.webcitation.org/5xieZtodg)
*   Royer, J., Mestre, J. & Dufresne, R. (2005) Introduction: framing the transfer problem In J. Mestre (Ed.), _Transfer of learning from a modern multidisciplinary perspective_ (pp. vii-xxvi). Greenwich, CT: Information Age Publishing.
*   Schatzki, T. (2002) _The site of the social: a philosophical account of the constitution of social life and change_. University Park, PA: Pennsylvania State University Press.
*   Scottish Government (2008). _[Work experience in Scotland](http://www.webcitation.org/5xiih69VC)_. Retrieved 7 December 2010 from http://www.scotland.gov.uk/Publications/2008/11/27092915/0 (Archived by WebCite at http://www.webcitation.org/5xiih69VC)
*   Smith, E. & Martina, C. (2004). Keeping the dough rising: considering information in the workplace with reference to the bakery trade. In P. Danaher, C. Macpherson, F. Nouwens and D. Orr (Eds.), _Lifelong learning: whose responsibility and what is your contribution_: _refereed papers from the 3<sup>rd</sup> international lifelong learning conference, Yeppoon, Central Queensland, Australia_. Rockhampton, Australia: Central Queensland University Press.
*   Srinivasan, R. & Pyati, A. (2007). Diasporic information environments: reframing immigrant-focused information research. _Journal of the American Society for Information Science and Technology_, **58**(12), 1734-1744.
*   Strauss, A. & Corbin, J. (1998). _Basics of qualitative research_ (2<sup>nd</sup> ed.). London: Sage.
*   UK. _Department for Children, Schools and Families_. (2008). _[Quality standard for work experience](http://www.webcitation.org/5yNSQAwaD)_. Retrieved 07 December 2010 from http://www.dcsf.gov.uk/14-19/documents/Quality%20Standard%20for%20Work%20Experience.pdf (Archived by WebCite at http://www.webcitation.org/5yNSQAwaD
*   Volet, S. (1999). Learning across cultures: appropriateness of knowledge transfer. _International Journal of Educational Research_, **31**(7), 625-643.
*   Woolls, B. and Loertscher, D. (2005). _The whole school library handbook_. Chicago, IL: American Library Association.

## Appendix 1: Interview questions for students

**Questions for students - pre-placement**

1.  Where do you find information about what's going on in the school e.g. sports or events?
2.  Where do you find information for your school assignments when you are in school?
3.  Where do you find information for your school assignments when you are not in school?
4.  What kinds of information ? not to do with school ? do you look for when you are at home?
5.  Which websites do you use most often when you are at home?
6.  How did you find out about the placement that you're going to?
7.  Where do you think people at your placement will get the information they need to do their jobs?
8.  How do you think getting the right information will be different at your placement i.e. from getting the right information in school?
9.  How do you think people at your placement will learn new things related to their job?

**Questions for students post-placement**

1.  Tell me about what you did on your placement.
2.  Tell me about how people in your placement found the information they needed to do their jobs.
3.  How good do you think people in your placement were at getting the _right_ information they needed to do their job?
4.  How did people in your placement learn new things about their job?
5.  How different was your placement from school in terms of finding information?
6.  Do you think that people in your placement could be better at finding the information they need? If yes, tell me about how they could be better.
7.  Did you learn anything new about finding information when you were on placement? If yes, tell me about it. If no, tell me about why you didn't learn anything new.

## Appendix 2: Interview questions for teachers

**Questions for guidance teachers, pre-placement**

1.  What is the purpose of sending students out on placement?
2.  What do you expect your students to learn while on placement?
3.  To what extent will students being using different kinds of information when on placement, as opposed to the kinds of information they find and use in school?
4.  To what extent do you ask students to observe the information environment, i.e. how and where people gain information to do their job, that they work in while they are on placement?
5.  What do you think students will learn about the information environments they will work in while on placement?
6.  What types of information/ knowledge do you expect students to have about their placement prior to commencing?

**Questions for guidance teachers, post-placement**

1.  From your students' debriefing and from the students' reports, to what extent do you think that your students found the workplace information environment to be different from the school information environment?
2.  What do you think your students learned about a different information environment while they were on placement?
3.  To what extent do you think that students can transfer information skills they learned on placement to their school work?
4.  To what extent do you think that students can improve their ability to find and use information in school, as a result of their placement experience?
5.  What can students learn about possible future careers from their placement experience?
6.  What have you learned from the current placements undertaken by students?