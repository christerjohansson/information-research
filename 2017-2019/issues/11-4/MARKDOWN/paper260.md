####  Vol. 11 No. 4, July 2006


# A re-examination of information seeking behaviour in the context of activity theory

#### [T.D. Wilson](mailto:wilsontd@gmail.com)  
Visiting Professor, Leeds University Business School, Leeds, UK and  
Swedish School of Library and Information Studies  
Gothenburg University and Högskolan i Borås, Sweden.

#### Abstract

> **Introduction.** Activity theory, developed in the USSR as a Marxist alternative to Western psychology, has been applied widely in educational studies and increasingly in human-computer interaction research.  
> **Argument.** The key elements of activity theory, Motivation, Goal, Activity, Tools, Object, Outcome, Rules, Community and Division of labour are all directly applicable to the conduct of information behaviour research. An activity-theoretical approach to information behaviour research would provide a sound basis for the elaboration of contextual issues, for the discovering of organizational and other contradictions that affect information behaviour. It may be used to aid the design and analysis of investigations.  
> **Elaboration.** The basic ideas of activity theory are outlined and an attempt is made to harmonize different perspectives. A contrast is made between an activity system perspective and an activity process perspective and a diagrammatic representation of the process perspective is offered.  
> **Conclusion.** Activity theory is not a predictive theory but a conceptual framework within which different theoretical perspectives may be employed. Typically, it is suggested that several methods of data collection should be employed and that the time frame for investigation should be long enough for the full range of contextual issues to emerge. Activity theory offers not only a useful conceptual framework, but also a coherent terminology to be shared by researchers, and a rapidly developing body of literature in associated disciplines.


## Introduction

Activity theory, or 'cultural-historical activity theory' as it is also known, has its origins in the search in the Soviet Union for an alternative to the then existing basis for psychology in behaviourism and psychoanalysis. The development of cultural-historical activity theory is associated with the names of Lev Vygotsky (1896-1934), Sergei Leonidovich Rubinshtein (1889-1960), Alexander Luria (1902-1977) and Alexei Leont'ev (1904-1979).

The work of these four founders is related in complex ways: Vygotsky was the founder of cultural-historical psychology, as a reaction against the behavioural psychology of the West and Luria and Leont'ev were, first, his students and then co-workers, who eventually carved out their own careers in different directions. Luria remained closer to Vygotsky's ideas in developing the concept of the cultural-historical context of human behaviour, with particular respect to the use of language as the 'tool of tools' in the mediation of behaviour, but Vygotsky's contribution was cut short by his early death. Luria went on to work on various aspects of language use, and other phenomena, in psychology ([Cole n.d.](#colnd)). Leont'ev, on the other hand, ultimately reacted against Vygotsky's work and is mainly responsible for the development of general activity theory, although always recognizing the earlier collaborative work. Rubinshtein was not one of the circle of Vygotsky but he, too, was primarily interested in the role of language and in activity generally. His key contribution to activity theory is the idea of the development of the human mind through activity and the reciprocal relationship between activity and subject: <span style="font-style: italic;">The leading doctrine in the works of Rubinshtein is that every human act changes not only the world, but the actor as well.</span> ([Häyrynen 1999](#hay99): 120)

Leont'ev acknowledges a debt to Marx in the elaboration of 'activity':

> Introducing the concept of activity into the theory of cognition, Marx gave it a strictly materialistic sense: for Marx, activity in its primary and basic form was sensory, practical activity in which people enter into a practical contact with objects of the surrounding world, test their resistance, and act on them, acknowledging their objective properties. This is the radical difference of Marxist teaching about activity as distinguished from the idealistic teaching that recognizes activity only in its abstract, speculative form. ([Leont'ev 1978](#leo78): Sect. 1.1)

Leont'ev's contribution deserves further attention because virtually all of the key features of activity theory emerged in his work. It must be made clear, however, that all of these writers were working in the field of psychology and that their primary concern was in understanding the fundamental psychological basis of human behaviour through the study of language, learning, cognitive disabilities and other phenomena. Leont'ev, in particular, was concerned with the nature of consciousness, and sought an explanation that did not rely upon postulating the existence of unobservable mental phenomena.

Leont'ev defines 'activity' as those processes 'that realise a person's actual life in the objective world by which he is surrounded, his social being in all the richness and variety of its forms' ([Leont'ev 1977](#leo77)). Thus, society, or 'community' as it is expressed in later activity theory writing, is central to Leont'ev's concept of activity.

## Activity theory: from education to information science

Largely as a consequence of the significance of Vygotsky's work in developmental psychology and the theory of learning, activity theory first made its appearance in the West in the field of educational research. Here, Vygotsky's concept of the Zone of Proximal Development gained attention, partly by the re-presentation of Russian work and subsequently through independent work, (e.g., [Rogoff & Wertsch 1984](#rog84) ; [Exner 1990](#exn90); [Salomon _et al._ 1989](#sal89)).

Education was also the initial field of research of Yrjö Engeström, who is probably the best known of the Western interpreters of activity theory. Engeström's early work was almost entirely in Finnish and concerned to a large degree with ideas on the reform of educational practice, but he has moved from education in the narrow sense to the study of learning in work situations ([Engeström 1994](#eng94), [2000](#eng00), [2001](#eng01)) and in the application of technology ([Mwanza & Engeström 2005](#mwa05)). This move has been accompanied by more publication in English.

More recently (and partly through the influence of Engeström's work), activity theory has found a place in human-computer interaction and information systems research. ([Bødker 1989](#bod89); [Kuutti 1991](#kuu91); [Kaptelinin 1994](#kap94); [Nardi 1996a](#nar96a); [Kaptelinin _et al._ 1999](#kap99); [Ditsa 2003](#dit03)). Here, the dominant model of activity theory has been Engeström's, but an alternative representation of the theory (known as systemic-structural activity theory), by Bedny ([2000](#bed00)), has also gained support. Bedny's conception of activity theory is derived from the work of successors to Vygotsky, and, in particular, from the work of Rubinshtein (e.g., [1957](#rub57)). Before his move to the USA, Bedny was at Kharkov University, an institution at which both Luria and Leont'ev had worked and he may be considered to be in the direct intellectual line of these founders of activity theory.

To date, information science researchers appear to have been slow to consider the usefulness of activity theory in their work. A search for 'activity theory AND information science' in the Web of Science revealed only one paper ([Spasser 1999](#spa99)) and a broader search of LISA revealed eighteen papers, most of which were concerned with communications and information technology applications in education, rather than information science. Spasser's paper is very brief and simply sets out an argument for considering activity theory as an appropriate approach for information science, especially with regard to the interaction of people and computers. He notes:

> …by explicitly recognizing and theorizing three broad classes of complexity-the stratified nature of the social world, the social contextualization and embeddedness of interaction, and the dynamism of development… AT is a promising new direction for the field of information science research. Conceptually, AT registers the shift of focus from the interaction between the isolated user and the stand-alone computer to a larger, more ecologically valid interaction context between human beings with their environment; sensitizes us to the dynamic and evolving nature of human-computer interaction and IS design and evaluation; and highlights the rich, multifaceted, and multidimensional reality of strips of computer-mediated activity in situ… (Spasser 1999: 1137)

Importantly, from the perspective of information behaviour research, he also points to the need, if an activity-theoretical perspective is adopted, of having a time-frame for research that is long enough to understand information users' behaviour and their motivations.

More recently, Spasser has applied an activity-theoretical analysis in a study of the development of content for the Flora of North America digital library, in which activity theory is employed as a middle-range realist theory to initiate, provisionally order, and govern the ongoing conduct of data collection and analysis. ([Spasser 2002](#spa02): 93) He presents activity theory within the broader conceptual framework of social realism (a perspective advocated by [Hjørland 1997](#hjo97)) and concludes that,

> …analysis in the present work has demonstrated the utility of a realist activity theory conceptual framework for ecologically evaluating the socio-organizational and institutional embeddedness of computer-based information systems, such as digital libraries, and explaining the contextual conditioning of our experience of them. ([Spasser 2002](#spa02): 103)

Hjørland (1997) has drawn attention to activity theory in his book _Information seeking and subject representation: an activity-theoretical approach to information science_. In spite of the sub-title, however, the author actually devotes very little space to an exposition of activity theory and his work is more concerned with subject representation, information retrieval and information searching than with information seeking as generally understood in the ISIC series of conferences. However, he makes the important point that:

> Activity theory stresses the development of cognition as a unity of biological development, cultural development, and individual development. It has a strong ecological and functional-historical orientation. It also stresses the activity of the subject and the object orientation of this activity. ([Hjørland 1997](#hjo97): 80)

Further:

> Activity theory also stresses the ecological and social nature of meaning… A person's use of a term may be determined not by his individual usage, but by the usage of some social group to which he semantically defers. Therefore, the content of a person's thoughts are themselves in part a matter of social facts. ([Hjørland 1997](#hjo97): 81)

Clearly, both of these points are of significance for the study of information seeking behaviour and Hjørland, since 1997, has published a number of papers in which he indicates the value of activity theory in underpinning his own philosophical position of 'critical realism' and his theoretical framework of 'domain analysis' (e.g., [Hjørland & Albrechtsen 1999](#hjo99); [Hjørland 2000](#hjo00), [2004](#hjo04)).

## An outline of activity theory

Mention has been made already of the different tendencies in activity theory, with one school of thought stemming from Engeström's research activities and another from Bedny's 'systemic-structural activity theory'. The two are very closely related, but authors from one or the other group use different diagrams to illustrate the components of the theory. The most basic representation and perhaps the closest to the ideas of the founders is Bedny's (Figure 1 below).

<div align="center">![figure_1](p260fig1.jpg)</div>

<div align="center">  
**Figure 1: Bedny's representation of activity theory ( [Bedny 2003](#bed03))**</div>

The figure shows the key features of activity theory: the subjects, that is, a person or group of persons engaged in the activity, acting upon an object in ways directed by a predetermined goal, using tools in the course of the activity, which has some result (not always directly satisfying the intended goal). The result establishes feedback to the subject or subjects engaged in the activity. Although not represented in the diagram, 'goal' is closely associated with the concept of 'motive': as Leont'ev notes:

> …different activities are distinguished by their motives. The concept of activity is necessarily bound up with the concept of motive. There is no such thing as activity without a motive; 'unmotivated' activity is not activity that has no motive, but activity with a subjectively and objectively hidden motive. ( [Leont'ev, 1977](#leo77))

It should be noted that 'tools' may be artefacts or abstract constructs: thus, in working with a computer we use a mouse, an artefact but, in using a computer to perform organizational tasks we may follow certain rules that, for example, forbid personal use of the e-mail system. The rules are abstract constructs that govern the particular activity of computer use.

The identification of two 'subjects' in the diagram signifies that activity may be shared or collaborative; that two or more persons may work together to achieve some predetermined goal. This fact introduces the notion of the division of labour in the performance of activity.

Some of these ideas are made more explicit in Engeström's formulation of activity theory and his diagram is widely used in the literature (Figure 2).

<div align="center">![figure_2](p260fig2.jpg)</div>

<div align="center">  
**Figure 2: Engestrom's activity theory diagram ([Engestrom, 1987](#eng87): 78)**</div>

The differences between Bedny's representation and that of Engeström are readily apparent: the triangle remains the dominant figure, but only one subject is represented and the notion of 'division of labour' is separately represented. Also, the concept of 'community' is introduced as part of the cultural context within which activity is performed and the abstract tool, 'rules', is also separated from artefacts that are here represented as 'instruments'.

Also central to activity theory is the concept of 'contradictions'; that is, the issues, conflicts and tensions that develop in any activity system. The idea stems from the Marxist analysis of society, which Leont'ev expresses in the following way:

> ..the class division of society puts people into unequal, opposed relations to the means of production and the social product; hence their consciousness experiences the influence of this inequality, this opposition. At the same time ideological notions are evolved and enter into the process by which specific individuals become aware of their real life relations. ([Leont'ev 1977](#leo77))

He goes on:

> There thus arises a complex picture of internal connections, interweaving and inter-traffic generated by the development of internal contradictions, which in abstract form become apparent in the analysis of the simplest relations characterising the system of human activity. ([Leont'ev 1977](#leo77))

Engeström and those who follow his formulation of activity theory, have developed the concept of contradictions extensively in their work and Spasser's research on the development of the Flora of North America digital library looks particularly at the fact that,

> Given their differing and inevitably conflicting roles and their equally divergent conception of the objects of their work, the project has been riddled with destabilizing, structurally embedded contradictions from its initial funding. ([Spasser 2002](#spa02): 97)

Thus, activity theory presents us with a framework for the analysis of activity in the socio-cultural-historical context of the community concerned and with regard to the motivations that give rise to goals, the accomplishment of which is the aim of activity. The framework identifies the tools - artefacts and mental constructs - that assist activity, as well as the rules, norms and division of labour that may affect activity. Given the complexity of human activity, we might expect inner contradictions to arise in the performance of activity as well contradictions brought about by the diversity of actors and their expectations.

## Activity, actions and operations

Leont'ev also developed an important distinction among concepts that is of particular value for information seeking behaviour: he distinguished between activity, actions and operations ([Leont'ev 1978](#leo78): para 3.5) and relates these terms to motives, goals and the conditions under which the activity is performed. The relationship is can be expressed diagrammatically:

<div align="center">![figure_3](p260fig3.jpg)</div>

<div align="center">  
**Figure 3: Activity, actions and operations**</div>

In Leont'ev's words:

> Thus the concept activity is necessarily connected with the concept of motive. Activity does not exist without a motive ; "non motivated" activity is not activity without a motive but activity with a subjectively and objectively hidden motive. Basic and "formulating" appear to be the actions that realize separate human activities. We call a process an action if it is subordinated to the representation of the result that must be attained, that is, if it is subordinated to a conscious purpose. Similarly, just as the concept of motive is related to the concept of activity, the concept of purpose is related to the concept of action. ([Leont'ev 1978](#leo78): para 3.5)

and:

> …actions are not special 'units' that are included in the structure of activity. Human activity does not exist except in the form of action or a chain of actions. For example, work activity exists in work actions, school activity in school actions, social activity in actions (acts) of society, etc. If the actions that constitute activity are mentally subtracted from it, then absolutely nothing will be left of activity. ([Leont'ev 1978](#leo78): para 3.5)

and, finally: <span style="font-style: italic;">…the action also has its operational aspect (how, by what means this can be achieved), which is determined not by the goal in itself but by the objective-object conditions of its achievement.</span> ([Leont'ev 1978](#leo78): para 3.5)

These distinctions are explored further, below.

## Activity theory and information behaviour

At this point, the reader is likely already to have a fairly clear perception that activity theory can apply to the study of information behaviour; however, some examples of the way it might be applied may be useful. The [AIMTech Research Group](http://www.aimtech.org/) at Leeds University Business School in the UK has adopted activity theory as a framework for its research and consultancy work. After a number of studies it was felt that the existing framework within which work was being pursued left some things unexplored and activity theory presented itself as potentially useful. AIMTech's work is concerned primarily with the investigation of mobile information systems in the emergency services in the UK - police forces, ambulance services and fire and rescue services. A review of this work suggested that, if activity theory had been adopted as the conceptual framework at the outset, certain aspects of the studies would have been either developed further or would have been included instead of being omitted. As a result, activity theory now provides the framework for evaluating pilot studies and other implementations of mobile information systems and specific questions in the interview process directly address the elements of activity theory. Thus, we will ask about:

*   the history of an innovation in the organization, to uncover previous attempts to implement technological change;
*   the cultural context of the innovation, for example, the extent to which the advocates of innovation are aware of tensions in the organization that may prevent success and the extent to which the end users of the system are committed to the innovation;
*   the rules and conditions under which the technology is to be applied (particularly important in police forces, where legislation guides all activity);
*   the informal norms of behaviour in the affected work team(s) and how this might affect the innovation;
*   the motivation for the innovation: for example, mobile systems may be introduced into a police force to reduce the time officers must spend in the police station, thereby enabling them to be more visible on the street;
*   how the total activity system affects other activity systems in the organization: for example, providing a police officer with information at the point of need has implications for supervision, for the activities of the control room and for the provision of the underlying communications technologies.

In this way a much deeper understanding is gained of the context of the information needs and information uses that are associated with the implementation of the new technology and the evaluation process can bring to the fore aspects of the organizational setting that affect the innovation but might otherwise be missed.

The re-examination of previous research in the light of activity theory can provide insights that may have been missed in the earlier analysis. Consider this example, from research into the information and communication behaviour of social workers, which I reconstructed from a much less rich account:

> The Social Services Department (community) is tasked with the care of children at risk in the wider community (local authority) and a social worker, as a consequence is assigned a 'case' (a child to be evaluated in the family setting). The Object, is the child to be assessed, the Motivation (stimulated by the law) the child's wellbeing; the Goal, to ensure the safety of the child. The action taken is the visit to the child's home, to observe the treatment of the child, to see the child washed or bathed (to observe any bruising, etc.), to observe the relationship between child and parent(s), and, on the basis of this evaluation, to make a recommendation. The Mediating Artefacts, in the initial action, are the manual tools of note-taking - pencil and paper, and the abstract tools of the social worker's practical experience and theoretical knowledge of similar situations.  
>   
> The outcome of this initial action takes the form of feedback from the Subject to the Community (in the form of his/her supervisor(s)), on the basis of which further action may take place, that is, a new activity system is invoked. For example, a case conference (another abstract artefact) is called in which there may be representatives of the Social Services Department, the school or nursery attended by the child, the child's doctor, and, if the parents are 'known to the police', the relevant community officer (clearly an instance of the division of labour).  
>   
> The outcome from that action may be a decision to take the child into care (invoking yet another activity system), placing him or her, initially, in a children's home before further assessment may lead to fostering (another activity system). A further outcome may be a court case of child abuse against the parent(s), which is yet another activity system.

Our interest in information behaviour would lead us to explore the nature of the information available to the different parties in the different activity systems, the nature of the information created during the conduct of the activity (case records, case conference minutes, etc.), the technical systems established to manage the various information resources, the flow of information from one activity system to another, the extent to which divergent interests of the different parties in the different activity systems might inhibit the exchange of information and so on. (We can represent the relationships diagrammatically as in Figure 4.) In the original investigation ([Wilson & Streatfield 1980](#wil80)), very little of this was actually explored, but the adoption of an activity-theoretical frame work immediately directs one's attention to the wide range of contextual issues that influence information behaviour and information exchange.

<div align="center">![Figure-4](p260fig4a.png)</div>

<div align="center">  
**Figure 4: Interacting activity systems**</div>

Another example, concerns a mediated information search, for which the enquirer gave the context:

> Myself and a number of other colleagues have been researching properties of colliery spoils for a number of years now primarily focusing on the construction of landfill liners. Now we have been asked by the Environment Agency to perhaps put a document together on best practice using colliery spoils, not only for landfill liners, but for other earthworks and construction aspects. As part of that we need to draw together all of the literature that is available from all the different sources. ([Wilson 2004](#wil04): Appendix 2)

Here we see, in activity theory terms: the Motivation - an external request from a funding agency for a document on best practice in using colliery spoils; a statement of the Goal (to prepare the document), and the fact of the Division of labour in the overall project: 'Myself and a number of other colleagues…' Later in the interview the person concerned says: 'We have done various searches but we are not sure that we have covered all of the areas.' Again, the plural is used, indicating that the task of search was the subject of division of labour.

The detailed exploration of cases was not the purpose of that research project, but it is easy to see that an activity-theoretical framework would enable one to pursue individual cases over time to explore how the division of labour in information seeking (and information creation) was carried out, what informal norms the group evolved to divide the work and share their results, and the effectiveness of the division of labour.

## Activity, actions and operations in information seeking

Leont'ev's definitions of these terms have clear application in information seeking research and, if applied rigorously, could help in the formulation of theory. Examining again the colliery spoils search, we can see that the motivation, the external request, stimulated the activity of document preparation, and that, to accomplish the goal of producing a 'best practice' document, it was necessary for the team to engage in information seeking actions. The conditions under which they worked enabled them to accomplish some of these actions themselves; thus, they were able to carry out the operations of search the Web of Knowledge databases, other databases accessible online, as well as the World Wide Web, and to search library resources, but the limitations in those conditions (no access to the commercial databases) induced them (or, rather, through the division of labour, one of them) to undertake the further action of seeking the project's assistance in carrying out a further search. The operations of that search were carried out, not by the individual himself, but by the search intermediary. We can note that the search intermediary was part of another activity system, that of the research project in which he was engaged, which had its own motivation and goals.

This analysis of that particular case, brings out the rich contextual framework in which it was set and suggests lines of enquiry that a less-rich analysis would not suggest.

## Activity and process

In exploring the applicability of activity theory in this way, I became somewhat dissatisfied by the rather static character of the diagrammatic representations. Certainly, flows of information, action, influence, etc., are represented in the arrowed lines leading to and from the nodes, but the process is not altogether clear. Initially, I tried to resolve this by combining Bedny's and Engeström's diagrams in Figure 5.

<div align="center">![figure_5](p260fig4.jpg)</div>

<div align="center">  
**Figure 5: The harmonization of Bedny and Engeström**</div>

The use of the 'tools' circle may be thought excessive but, in fact, it serves the useful purpose of drawing attention to questions that one might wish to ask. For example, How are the norms that affect this activity transmitted within the community? What tools are used by subjects to communicate with one another their experience in the activity? How do the subjects organize the division of labour between themselves and to what extent is this affected by decisions of the community to which they belong and by the norms of behaviour as they affect this activity?

Although this harmonized diagram was useful in enabling a deeper understanding of activity theory, I still felt that the process element was lacking. Also, Bedny locates 'Goal' in a position that is at variance with all formulations of activity theory, while Engeström omits it from his diagram. 'Goal' is seen by Leont'ev and others as something that promotes activity, not something that is directly affected by activity. Motivation and Goal establish the preconditions for activity. Once this was established, a process model of activity emerged relatively easily and this is shown in Figure 6:

<div align="center">![figure_5](p260fig5.jpg)</div>

<div align="center">  
**Figure 6: A process model of activity**</div>

## Conclusion

This exploration of activity theory has served to show that it can be quite a powerful analytical tool and conceptual framework for enquiry. It should be noted, however, that, in spite of being termed activity theory, it is not a predictive theory, but rather a framework based upon a particular theory of human consciousness, aiming at explaining the character of human behaviour. The practical implication of this, in theoretical terms, is that no one theoretical position is involved in its use. Human information activity can be explored with the application of many theories, from risk avoidance to self-efficacy and more ([Fisher _et al._ 2005](#fis05)) and there is no reason why a proponent of any theoretical position should not adopt an activity theory framework for his or her work. The same applies to method: both quantitative and qualitative methods can be applied and, indeed, for some work in information behaviour research, it may well be appropriate to use both: for example, carrying out relatively unstructured interviews and collecting computer log data for quantitative analysis.

As to the value of activity theory for information behaviour research, it has already been noted that use of the framework can draw the researcher's attention to aspects of the context of activity that otherwise might be missed. The comments above on the work of the AIMTech Research Group address this issue and we can summarise the point by noting that the cultural-historical setting of the activity and the relationship of the activity to the external environment constitute what might be called the 'macro-context' of the activity, while the goals and motivations of the originators of the activity, together with the artefacts, rules, norms and division of labour in the setting constitute the 'micro-context'.

The extension of Engeström's concept of 'instrument' to include abstract and cultural 'tools' (although note that 'tools' were viewed as both material and abstract by Vygotsky and Leont'ev) also has implications for information research. Thus, in order to use a computer for information searching, the searcher must have a complex set of abstract concepts that constitute the 'tools' for searching: these concepts may be explicitly-learnt ideas, but they may also be the result of the 'sedimented' experience of using information search devices (from the card catalogue and the book index to the search engine), where learning has taken place through experience and without instruction. Exploring the possible research questions that arise out of this idea would fill another paper!

The theory might also help us to formulate interesting research questions, and intimations of this have been made earlier in this paper. Thus research questions might direct attention to the role of organizational goals in determining the information seeking behaviour of the participants in the organization, or to the extent to which prevailing formal and informal norms of behaviour in an organization may help or inhibit information sharing, or the extent to which division of labour in task performance affects the division of labour in information seeking and the extent to which that helps on inhibits information sharing. Once the framework is internalised by the researcher, the research questions will arise almost without prompting in his or her mind

Finally, the distinction between activity, actions and operations is interesting and, at least to my knowledge, has been little explored in information science, if at all. It can be argued, for example, that information seeking is not an 'activity', but a set of 'actions' that support some higher level activity - examples of this have been given earlier. To treat information behaviour as somehow fundamental to human life may be as misleading as the early efforts to demonstrate a fundamental need for information. At the level of searching (an action?), how do we distinguish between 'action' and 'operation'? For the skilled searcher the implementation of Boolean operators becomes an instinctive process, without much conscious decision making (an operation), whereas for the naive searcher it may be considered an 'action' to be learnt and integrated into the emerging battery of actions that are used in the information seeking process: practice, familiarity and experience then reduce it to the level of an 'operation'. Again, the possible research questions are numerous: What is an information 'activity'? Considering 'information behaviour' as the totality of actions and operations employed by a person to discover, manipulate and use information, what different sets of actions and operations are employed by persons in different roles, or of different ages, or of different status in an organization, etc.? How does an action become an operation?

Much information behaviour research is of a 'snapshot' character, exploring a situation at a particular point in time. A further implication of activity theory, however, is that a longer timeframe is necessary, partly to enable the 'triangulation' of data through the application of different methods, and partly through the need to explore the activity system of interest in all its complexity. As Nardi puts it, activity theory requires:

> A research time frame long enough to understand users' objects, including, where appropriate, changes in objects over time and their relation to the objects of others in the setting studied. Kuutti… observes that 'activities are longer-term formations and their objects cannot be transformed into outcomes at once, but through a process consisting often of several steps or phases'. ([Nardi 1996b](#nar96b): 49)

This is particularly important when much information behaviour research involves small-scale studies of a very limited number of individuals: a re-orientation of research to larger-scale investigations with larger numbers of respondents and over a longer time-frame is becoming a matter of some urgency for the progress of understanding in the field.

## Acknowledgements

My thanks to Prof. Elena Macevičiūtė for helpful discussions on the original Russian sources, to colleagues in the AIMTech Research Group for their contributions to the debate and to the referees for their useful comments on the draft paper.

## References

*   <a id="bed03" name="bed03"></a>Bedny, G.Z. (2003). [Systemic structural theory of activity.](http://case.glam.ac.uk/CASE/StaffPages/SteveHarris/GZBPubs/Goal-Formation-England1.ppt) [A PowerPoint presentation] Retrieved 11 September, 2005 from http://case.glam.ac.uk/CASE/StaffPages/SteveHarris/GZBPubs/Goal-Formation-England1.ppt
*   <a id="bed00" name="bed00"></a>Bedny, G.Z., Seglin, M.H., & Meister, D. (2000). Activity theory: history, research and application. _Theoretical Issues in Ergonomic Science_, **1**(2), 168-206.
*   <a id="bod89" name="bod89"></a>Bødker, S. (1989). A human activity approach to user interfaces. _Human Computer Interaction_, **4**(3), 171-195
*   <a id="colnd" name="colnd"></a>Cole, M. (n.d.). [A brief overview of Luria's life and work.](http://marxists.anu.edu.au/archive/luria/comments/bio.htm) In _Marxists Internet Archive. Alexander Luria._ Retrieved 20 November, 2005 from http://marxists.anu.edu.au/archive/luria/comments/bio.htm.
*   <a id="dit03" name="dit03"></a>Ditsa, G. (2003). Activity theory as a theoretical foundation for information systems research, In, G. Ditsa, (Ed.) _Information management: support systems & multimedia technology_. (pp. 192-231). Hershey, PA: Idea Group Publishing.
*   <a id="eng87" name="eng87"></a>Engeström, Y. (1987). _Learning by expanding: an activity-theoretical approach to developmental research._ Helsinki: Orienta-Konsultit.
*   <a id="eng94" name="eng94"></a>Engeström, Y. (1994). _Training for change: new approach to instruction and learning in working life._ Geneva: International Labour Office.
*   <a id="eng99" name="eng99"></a>Engeström, Y. (1999). Activity theory and individual and social transformation. In Yrjö Engeström, Reijo Miettinen and Raija-Leena Punamäki, (Eds.). _Perspectives on activity theory_. (pp. 19-38). Cambridge: Cambridge University Press.
*   <a id="eng00" name="eng00"></a>Engeström, Y. (2000). Activity theory as a framework for analyzing and redesigning work. _Ergonomics_, **43**(7), 960-974.
*   <a id="eng01" name="eng01"></a>Engeström, Y. (2001). Expansive learning at work: toward an activity theoretical reconceptualization. _Journal of Education and Work_, **14**(1), 133-156.
*   <a id="exn90" name="exn90"></a>Exner C.E. (1990). The zone of proximal development in in-hand manipulation skills of nondysfunctional 3-year-old and 4-year-old children, _American Journal of Occupational Therapy_, **44**(10), 884-891
*   <a id="fis05" name="fis05"></a>Fisher, K.E., Erdeles, S. & McKechnie, L. (Eds.) (2005). _Theories of information behavior_. Medford, NJ: Information Today, Inc.
*   <a id="hay99" name="hay99"></a>Häyrynen, Y-P. (1999). Collapse, creation and continuity in Europe: how do people change? In Yrjö Engeström, Reijo Miettinen and Raija-Leena Punamäki, (Eds.). _Perspectives on activity theory._ (pp. 115-132). Cambridge: Cambridge University Press.
*   <a id="hjo97" name="hjo97"></a>Hjørland, B. (1997). _Information seeking and subject representation. An activity-theoretical approach to information science._ Westport, CT; London: Greenwood Press.
*   <a id="hjo00" name="hjo00"></a>Hjørland, B. (2000). Information seeking behaviour: what should a general theory look like? _The New Review of Information Behaviour Research_, **1**, 19-33.
*   <a id="hjo04" name="hjo04"></a>Hjørland, B. (2004). Arguments for philosophical realism in library and information science. _Library Trends_, **52**(3), 488-506
*   <a id="hjo99" name="hjo99"></a>Hjørland, B. & Albrechtsen, H. (1999) Toward a new horizon in information science: domain-analysis. _Journal of the American Society for Information Science_, **46**(6), 400-425.
*   <a id="kap94" name="kap94"></a>Kaptelinin, V. (1994). Activity theory, implications for human-computer interaction. In M.D. Brouwer-Janse and T.L. Harrington (Eds.). _Human machine communication for educational systems design_. (pp. 5-15). Berlin: Springer.
*   <a id="kap99" name="kap99"></a>Kaptelinin, V., Nardi, B. & Macaulay, C. (1999). Methods & tools: the activity checklist: a tool for representing the 'space' of context. _interactions_, **6**(4), 27-39.
*   <a id="kuu91" name="kuu91"></a>Kuutti, K. (1991). Activity theory and its applications in information systems research and design, in H.-E. Nissen, H.K. Klein, and R. Hirschheim, (Eds.). _Information systems research arena of the 90s_. (pp. 529-550). Amsterdam: North-Holland.
*   <a id="leo77" name="leo77"></a>Leont'ev, A.N. (1977). [Activity and consciousness](http://www.marxists.org/archive/leontev/works/1977/leon1977.htm). In, _Philosophy in the USSR: problems of dialectical materialism_. (pp. 180-202) Moscow: Progress Publishers. Retrieved 18 November, 2005 from http://www.marxists.org/archive/leontev/works/1977/leon1977.htm
*   <a id="leo78" name="leo78"></a>Leont'ev, A.N. (1978). _[Activity, consciousness, and personality](http://www.marxists.org/archive/leontev/works/1978/index.htm)_. Translated from Russian by Marie J. Hall. Englewood Cliffs, NJ; London: Prentice-Hall. Retrieved 18 November, 2005 from http://www.marxists.org/archive/leontev/works/1978/index.htm
*   <a id="mwa05" name="mwa05"></a>Mwanza, D. and Engeström, Y. (2005). Managing content in e-learning environments. _British Journal of Educational Technology_, **36**(3), 453-463.
*   <a id="nar96a" name="nar96a"></a>Nardi, B. (Ed.) (1996a). _Context and consciousness: activity theory and human-computer interaction_. Cambridge, MA: MIT Press.
*   <a id="nar96b" name="nar96b"></a>Nardi, B.A. (1996b). Studying context: a comparison of activity theory, situated action models, and distributed cognition. In B.A. Nardi (Ed.), _Context and consciousness: activity theory and human-computer interaction_. (pp. 69-102), Cambridge, MA: MIT Press.
*   <a id="rog84" name="rog84"></a>Rogoff, B. and Wertsch, J. (Eds.) (1984). _Children's learning in the 'Zone of Proximal Development'_. San Francisco: Jossey-Bass Inc.
*   <a id="rub57" name="rub57"></a>Rubinshtein, S.L. (1957). _Existence and consciousness._ Moscow: Academy of Pedagogical Science.
*   <a id="sal89" name="sal89"></a>Salomon, G., Globerson, T. & Guterman, E. (1989). The computer as a zone of proximal development - internalizing reading-related metacognitions from a reading partner. _Journal of Educational Psychology_, **81**(4), 620-627
*   <a id="spa99" name="spa99"></a>Spasser, M.A. (1999). Informing information science: the case for activity theory. _Journal of the American Society for Information Science_, **50**(12), 1136-1138.
*   <a id="spa02" name="spa02"></a>Spasser, M.A. (2002). Realist activity theory for digital library evaluation: conceptual framework and case study. _Computer Supported Cooperative Work_, **11**(1/2), 81-110
*   <a id="wil04" name="wil04"></a>Wilson, T.D. (2004). [Talking about the problem: a content analysis of pre-search interviews.](http://informationr.net/ir/10-1/paper206.html) _Information Research_, **10**(1), paper 206\. Retrieved 31 December, 2005 from http://informationr.net/ir/10-1/paper206.html.
*   <a id="wil80" name="wil80"></a>Wilson, T.D. & Streatfield, D.R. (1980). _['You can observe a lot…' A study of information use in local authority social services departments conducted by Project INISS.](http://informationr.net/tdw/publ/INISS/)_ Sheffield: Sheffield University, Postgraduate School of Librarianship and Information Science. Retrieved 14 January, 2006 from http://informationr.net/tdw/publ/INISS/


