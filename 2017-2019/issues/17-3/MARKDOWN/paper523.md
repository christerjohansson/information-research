#### vol. 17 no. 3, September 2012

# College students' uses and perceptions of social networking sites for health and wellness information

#### [Yan Zhang](#author)  
The University of Texas at Austin, School of Information, Austin, TX, 78701, USA

#### Abstract

> **Introduction.** This study explores college students' use of social networking sites for health and wellness information and their perceptions of this use.  
> **Method.** Thirty-eight college students were interviewed.  
> **Analysis.** The interview transcripts were analysed using the qualitative content analysis method.  
> **Results.** Those who had experience using social networking sites for health information used the platform mainly to check health updates of a loved one, find lifestyle information and ask about treatments for mild conditions. Overall, participants were sceptical about the quality of information, concerned about the lack of medical knowledge of their friends or peers and wary about possible social risks and invasion of privacy. Based on the results, a model of users' acceptance of social networking sites for health and wellness information was proposed and implications for designing social platforms to better support health inquiries were discussed.  
> **Conclusions.** Using social networking sites for health and wellness information is not a popular behaviour among college students in this study and social networking sites seem not to be a well-perceived platform for health and wellness information.

## Introduction

Community-based social media, characterized by its ability to support information sharing and collaboration between individuals, is imposing more and more significant influence on people's daily information seeking and decision making. Unsurprisingly, the impact of social media has extended to the health care domain, as consumers seeking health information online have begun to share their experiences and knowledge ([Scanfeld, _et al._ 2010](#sca10); [Wicks _et al._ 2010](#wic10) ). Numerous studies on consumers' communication and information behaviour in various online communities, mostly listserv, bulletin boards, Web forums and social question and answer sites, suggested that participation in these communities provides effective emotional and informational support to users ([Preece 1998](#pre98); [Wing and Jeffery 1999](#win99)), as well as helping them initiate and sustain behavioural changes ([Bahr, _et al._ 2009](#bah09); [Korda and Itani 2011](#kor11)).

However, few studies explored the ways in which people use social networking sites, one of the most popular forms of social media, for health and wellness information ([Morris, _et al._ 2011](#mor11)). According to a recent Pew report, as of May 2011, 65% of adult Internet users use a social networking site such as [MySpace](http://www.myspace.com/), [Facebook](http://www.myspace.com/), or [LinkedIn](http://www.linkedin.com/), up from 29% in 2008\. Social networking sites were also ranked as the third most used Web applications, second to e-mail and search engines ([Madden & Zickuhr 2011](#mad11)). People's social networks have long been recognized as an important factor in shaping their lifestyle and health related behaviour (e.g., [Christakis and Fowler 2007](#chr07); [Hoffman_et al._ 2006](#hof05)). This network is also a major source of information and care when a person becomes ill (e.g., [Smith and Christakis 2008](#smi08)). As a digital counterpart of the physical social network, social networking sites could offer users opportunities for improving personal health ([Consolvo _et al._ 2006](#con06); [Morris _et al._ 2011](#mor11)).

To gain an understanding of the potential of social networking sites in health promotion, it is necessary to know how users currently use social networking sites for health and wellness information. Human factors and human-computer interaction researchers postulated that users' use and interaction with a system is mediated by their perceptions or mental models of the system ([Norman 1983](#nor83)). These perceptions impact users' understanding of the form, purpose and functions of a system ([Norman 1983](#nor83); [Young 1983](#you83); [Rouse and Morris 1986](#rou86)). Therefore, we also intend to examine users' perceptions of social networking sites in relation to health and wellness information to understand why users use, or do not use, social networking sites for health information.

Because of a lack of studies in this area, this study is exploratory in nature. Two research questions were proposed: (1) how do students use social networking sites for health and wellness related information? (2) What are their perceptions of this use? The participants of this study were a group of undergraduate students. They were chosen because they were major users of social networking sites, with more than 80% having an account with a social networking site ([Madden and Zickuhr 2011](#mad11)). Member of this group are also heavy users (85% of the group) of the Web for health related information ([Zickuhr 2010](#zic10)).

## Related literature

### Social networking sites and health information seeking

A substantial body of research has examined various forms of online health communities and support groups, such as listserv, bulletin boards and Web-based forums ([Eysenbach _et al._ 2004](#eys04)). These communities or groups are often organized around a particular health goal (e.g., weight loss) or a particular disease (e.g., diabetes). Participants, including patients, care-givers and sometimes health care professionals, seek or provide useful tips and offer or receive encouragement and success stories ([Blank _et al._2010](#bla10); [Ginossar 2008](#gin08); [Klemm, _et al._ 1998](#kle98); [Preece 1998](#pre98)). The clinical evidence concerning the effectiveness of the participation in these communities is lacking ([Campbell, _et al._ 2004](#cam04)), but numerous studies provide qualitative accounts that participants often felt better informed, better motivated, better prepared to manage their illness and less lonely ([Hwang _et al._ 2010](#hwa10); [Nambisan 2011](#nam11)).

Social networking sites are gaining popularity with a record number of users in various age groups ([Madden and Zickuhr 2011](#mad11)). Social networking sites are Web-based services that allow individuals to post profile information, construct a list of _friends_ and communicate with others using both synchronous and asynchronous messaging tools ([Boyd and Ellison 2007](#boy07)). Different from more traditional online support groups, social networking sites are built upon a person's existing social ties (e.g., friends, family and acquaintances) and people come to the sites with many different purposes.

With their increasing popularity, social networking sites have influenced various aspects of the society and have been integrated into the daily lives of Web users ([Lampe, _et al._ 2008](#lam08)). The impact of social networking sites is also reaching the health domain. People have begun using social networking sites for various health purposes. The recent Pew study, _The social life of health information_, ([Fox 2011](#fox11)), reported that as of September 2010, about 20% of social networking site users had used the sites to follow friends' personal health experiences or updates; remember or memorialize other people who suffered from a particular health condition; get health information; raise money for or draw attention to a health related issue; post comments, queries, or information about health or medical matters; or start or join a health related group ([Fox 2011](#fox11)). At the same time, many researchers and organizations suggest that online social networking sites, especially when the network includes people who knew each other personally, could be a compelling venue for health promotion and patient support ([Fox 2010](#fox10); [Morris _et al._ 2011](#mor11), [Skeels _et al._ 2010](#ske10)).

Nevertheless, little is known about the phenomenon of using social networking sites for health and wellness purposes, such as when people would use social networking sites for this purpose, what information they look for, how they use this platform, why they choose it and what influence this use has on people's health behaviour. A recent study was performed to address some of these issues. By interviewing fourteen people who were using Facebook for the purpose of losing weight or managing Type II diabetes, Newman _et al._ ([2011](#new11)) found that users posted their exercises on Facebook through wall posts, became fans of particular weight loss online communities and added friends they knew from other online communities to their Facebook as a way to keep in touch and deepen the engagement with them. But overall, Facebook was not an effective venue for interacting for health-related matters. To maintain a positive identity of themselves as a healthy person in their network, users were selective about what to post on Facebook. For example, users post about their runs on Facebook but not their struggles, even though they contended that people who knew them personally as opposed to only through their online identity would provide better emotional support and accountability. Morris _et al._ ([2010](#mor10)) found that less frequent users of Facebook or Twitter were more likely to ask health questions to their social networks and speculated that infrequent users might have a different understanding of the norms and etiquette on social networking sites.

### Everyday life information seeking

Everyday life information seeking refers to the information behaviour of an average person looking for information for non-occupational and non-school-related purposes ([Savolainen 1995](#sav07)). Such information behaviour plays an important role in individuals' daily life and is constituted by two major modes of information seeking: (1) seeking problem-specific information; and (2) seeking orienting information. The former refers to the acquisition of information that is needed to solve individual problems and the latter refers to the acquisition of information concerning current events or keeping up to date ([Savolainen 2007](#sav07)). Generally speaking, the overarching goal of such behaviour is to make sense of the gaps in daily life so as to achieve a sense of coherence ([Dervin 1983](#der83); [Spink and Cole 2001b](#spi01b)). Everyday life information behaviour is culturally-bounded, socially-situated and affected by individuals' '_way of life_' and '_mastery of life_' ([Savolainen 1995: 262](#sav95); [Spink and Cole 2001b](#spi01b)). Searching for health and wellness related information is a typical and popular type of everyday life information seeking ([Bishop _et al._ 1999](#bis99); [McKenzie 2003](#mck03)).

A major focus of the existing research on everyday life information seeking is the channels, sources, or information environments that a particular group of people has when looking for everyday life information. Most studies suggest that human sources dominate everyday life information seeking patterns and are a preferable source in various everyday life information seeking contexts ([Agosto and Hughes-Hassell 2005](#ago05); [Julien and Michels 2004](#jul04) ; [Savolainen and Kari 2004](#sav04); [Savolainen 2007](#sav07)). In the health domain, studies have found different patterns of source usage among different user groups, but their preferences for sources tended to be similar. For example, Johnson and Meischke ([1991](#joh91)) studied middle-class women in a medium-sized mid-western US city who were seeking mammography treatments and found that media was the major source of health information, followed by doctors, organizations and family and friends. However, they ranked doctors as the most preferable source and the media the least. Contrary to Johnson and Meischke's ([1991](#joh91)) finding for middle class subjects, Spink and Cole ([2001a](#spi01a)) conjectured that because the low-income households in their study did not have the middle class option of seeking health information via channels that preserved their privacy, they were compelled to use family and friends for health information.

Several studies went further to explore the criteria of selecting and using a particular source. Savolainen ([1999](#sav99)) found that the major criteria for preferring the Internet for both job-related and non-work-related information, in Finland, were ease of access, savings in time, savings in money, the opportunity to consult several experts with a single result and greater independence from specific times and places for information seeking. In another study, Savolainen and Kari ([2004](#sav04)) found similar results that networked sources were favoured by Internet users for easy accessibility and the opportunities to save time. By studying twenty environmental activists' behaviour when searching for orienting information, Savolainen ([2007](#sav07)) found that they preferred printed media (newspapers), the Internet and broadcast media. The major criteria in selecting the sources were content of information, availability, accessibility and usability of information sources. Connaway, Dickey and Radford ([2011](#con06)) found convenience to be a critical factor for both college teaching staff and students (particularly millennials) in seeking everyday information.

As previously mentioned, as an information source quickly gaining popularity, social networking sites have been touted as having great potential in promoting public health. In this study, using everyday life information seeking as a research framework, we attempt to explore college students' (a group of heavy users of social networking sites) health related information seeking behaviour , on social networking sites, as well as their perceptions of this usage. The results could improve the understanding of social networking sites as a source for health information and inform the design of social networking sites intended to support health information seeking and health promotion.

## Research methods

Thirty-eight undergraduate students from a major research university in Texas were interviewed about their current use of social networking sites for health and wellness related information and their perceptions of this use. The interview method was chosen for two reasons. First, it is difficult to empirically observe users' behaviour whilst using social networking sites due to privacy concerns. Second, unlike for traditional information retrieval systems, whose major components of interest to information retrieval research are well defined ([Wang, Hawk and Tenopir 2000](#wan00)), there is a lack of agreement on the major components of social networking sites that should be investigated to understand such sites as a place for information. The interview technique allowed researchers to follow up on threads brought up by participants in the conversation so as to more effectively probe into aspects of their perceptions of social networking sites. The participants were self-selected volunteers, recruited through the participant pool of the Psychology Department. The students received class credits as compensation for their time spent in the study.

In the existing literature, social networking sites were often defined as Web-based services that allow individuals to construct a profile, develop a list of others with whom they share a connection, and view and traverse their own connections and those made by others within the system ([boyd and Ellison 2007](#boy07)). However, in our pilot studies, we observed that some students were confused about the concept and often asked what was meant by social networking sites. One student asked whether bulletin boards and online support groups are social networks. Thus, a phenomenological approach was taken to discover any way in which users construct this concept, while efforts were made to observe differences and commonalities in the use of this concept during interviews and the data analysis.

The interviews were one-on-one and took place in a private laboratory. All the interviews were conducted in January and February 2011\. At the beginning of the study, all participants completed a demographic questionnaire reporting their age, class status, major field of study, experience with the Web and experience with social networking sites. Then participants were asked to draw their perceptions of social networking sites. Since the drawings were not specific to their perceptions of social networking sites as a place for health related information, these data were excluded from the analysis. After the drawing, participants were asked a few questions about their general Web search behaviour, followed by questions about their current use of social networking sites for health and wellness related information and their perceptions of this usage. The questions were intentionally designed to be open so participants could freely express their thoughts on this subject. During the interviews, prompting questions, such as 'can you elaborate on this?' and 'how do you feel about the experience?', were used to encourage participants to talk, when necessary. At the end of the study, participants were asked to report their years of experience, number of friends and frequency of usage of the social networking sites they had used. Each session lasted approximately thirty to fifty minutes.

Participants' demographic information and their experience with social networking sites were analysed using descriptive statistics. The interviews were transcribed and analysed using qualitative content analysis through an open coding process ([Zhang and Wildemuth 2009](#zha09)). Nvivo 9.0 was employed to assist in the data analysis. The unit of analysis was a theme or issue related to participants' use and perceptions of social networking sites for health information. Categories were generated deductively from the data. In the process of developing categories, the constant comparison method was used, that is, whenever a new text was to be coded into a category, it was compared to those already in the category. This comparison helps researchers elucidate the properties of a particular category, as well as differences between categories ([Glaser and Strauss 1967](#gla67)). A second coder coded 20% of the transcripts and the inter-coder agreement reached 86.6%.

## Results

### Participants' demographics and experience with social networking sites

Of the 38 participants (22 females, 16 males), 23 (61%) were first year students, 11 (29%) were second year students, 2 (5%) were third year students and 2 (5%) were final year students. Their ages ranged from eighteen to twenty-two, with an average of nineteen years old (_SD_ = .99). Three of the participants had not declared a major. The rest were from 20 different major fields of studies, including biology, chemistry, nursing, nutrition, business, engineering, education, history, music and social work. Their experience with the Web ranged from 5 to 17 years (_Mean_ = 9.88; _SD_ = 2.63) and experience with social networking sites ranged from 3 to 10 years (_Mean_ = 5.17; _SD_ = 1.46). The majority of them (31; 82%) were from the state of Texas, 4 (10%) were from other states and 3 (8%) were from other countries.

All the participants used Facebook and 36 (95%) used it on a daily basis. The number of their Facebook friends ranged approximately from 100 to 2000 (_Mean_ = 668; _SD_ = 446). The second most used social networking site was Myspace. 27 participants (71%) had an account, but 16 were no longer using it and 6 described using it rarely. Eighteen participants (47%) reported using Twitter, but none had more than 20 friends and 6 reported that they had stopped using it. The most common activities that this group of students performed on social networking sites included establishing connections with existing friends, sending messages to friends, reading friends' posts, sharing photos and videos, chatting with friends, updating their status, joining a social event, becoming a fan of an organization, and asking questions.

### The current use of social networking sites for health and wellness information

The study took a phenomenological approach in defining social networking sites. When asked whether they had used any social networking sites for health and wellness related information, almost all the participants referred to Facebook and a few mentioned [Twitter](http://twitter.com/), [Tumblr](https://www.tumblr.com/), Myspace and an online forum. They tended not to perceive blogs and online bulletin boards as social networking sites.

Everyday life information seeking behaviour can take different forms. McKenzie ([2003](#mck03)) identified a continuum of four modes: (1) active seeking; (2) active scanning; (3) non-directed monitoring; and (4) by proxy. Active seeking refers to the practice of actively asking questions or performing searches. This mode corresponds to Wilson' s ([1997](#wil97)) active search and Choo _et al._' s ([2000](#cho00)) formal search. Active scanning involves practices of semi-directed browsing or scanning within sources. This mode corresponds to Wilson's ([1997](#wil97)) passive search and Choo _et al._'s ([2000](#cho00)) conditioned viewing. Non-directed monitoring refers to the practice of serendipitously encountering information while monitoring information sources with '_no intent other than to become generally informed_' ([McKenzie 2003: 27](#mck03)). This mode corresponds to Choo _et al._' s ([2000](#cho00)) undirected viewing. By proxy refers to the practice of getting information through an intermediary.

Table 1 summarizes the participants' behaviour using social networking sites, particularly Facebook, for health and wellness information based on McKenzie's model as well as on the data.

<table class="center" style="width: 75%;"><caption>  
**Table 1: The use of social networking sites for health and wellness related information**</caption>

<tbody>

<tr>

<th>Mode</th>

<th>Specific use cases</th>

<th>No.</th>

</tr>

<tr>

<td style="vertical-align:top">Active seeking:  
asking & searching</td>

<td style="vertical-align:top">

*   Asking relatives about a loved one's health condition
*   Asking friends how to get rid of flu and how to stay healthy
*   Searching to find whether other people posted articles about a condition
*   Searching to find whether there was a group page about a condition

</td>

<td style="text-align:center; vertical-align:middle">4</td>

</tr>

<tr style="background-color:#fffff">

<td style="vertical-align:top">Active scanning:  
following</td>

<td style="vertical-align:top">

*   Following friends' diet plan to see what can eat, what cannot and being motivated to stay fit
*   Following information about organic food

</td>

<td style="text-align:center; vertical-align:middle">2</td>

</tr>

<tr>

<td style="vertical-align:top">Non-directed monitoring:  
encountering</td>

<td style="vertical-align:top">

*   Keeping updated with friends' health conditions

</td>

<td style="text-align:center; vertical-align:middle">3</td>

</tr>

<tr>

<td style="vertical-align:top">Active scanning:  
collowing</td>

<td style="vertical-align:top">

*   Following friends' diet plan to see what can eat, what cannot and being motivated to stay fit
*   Following information about organic food

</td>

<td style="text-align:center; vertical-align:middle;">2</td>

</tr>

<tr>

<td style="vertical-align:top">Sharing information</td>

<td style="vertical-align:top">

*   Sharing nutrition articles with friends
*   Sharing lifestyle, particularly diet and exercise routines, with friends
*   Updating a diet page for freshmen
*   Posting recipes from health food stores or health Websites
*   Sharing the news of the death of a pet with cousins overseas
*   Sharing a loved ones' health condition with relatives

</td>

<td style="text-align:center; vertical-align:middle;">5</td>

</tr>

<tr style="background-color:#ffffff;">

<td style="vertical-align:top;">Participating</td>

<td style="vertical-align:top">

*   Joining a group for learning medical vocabulary
*   Participating in support groups for a particular health condition

</td>

<td style="text-align:center; vertical-align:middle;">2</td>

</tr>

</tbody>

</table>

As shown in the table, the first three types of behaviour using social networking sites could be mapped to the first three modes in McKenzie's model. Among the participants, four reported they had been actively seeking information, mainly through two means: asking questions to friends or relatives and searching the existing information on Facebook. An example is:

> I typed [high blood pressure] on Facebook and just try to get information as far as to help keep your blood pressure down and, basically, other stuff like which are more treatment things other than medications… I was kind of searching around to see if other people posted articles or just if there was a group page about it.

Two participants reported that they followed friends' diet plans to learn about what can and cannot be eaten and followed a list concerning organic food. In Facebook, by following a particular information thread, the user recognizes that the thread is of interest and that s/he would like to receive regular updates from it. Often the user will scan the incoming updates expecting to find something interesting. Thus, following a particular information thread is a form of active scanning.

Three participants reported that they kept up with friends' or loved ones' health conditions by receiving information from their status updates. This behaviour is considered as a form of non-directed monitoring because, in Facebook, reading status updates is a behaviour of monitoring an individual's social network to become generally informed of new developments. The user often does not have a specific information-seeking goal in mind when s/he browses the news feeds; instead, s/he serendipitously encounters information. An example is:

> I guess not necessarily my own health but keeping up with other people's health situations, like right now I had one of my friends from high school who I would have no idea she was hospitalized had I not opened on Facebook. And people keeping me up-to-date with her condition, I guess, is the best…

No behaviour corresponding to the 'by proxy' mode in McKenzie's model was found. Nevertheless, two additional modes of behaviour were identified: sharing information with others and participating in group activities. Five participants mentioned that they had shared health and wellness related information with friends and family. Different from the first three modes where the participants acted as an information receiver, by sharing the participants acted as an information provider. For example, one participant commented:

> [A friend created a page] listing the food you can eat and you cannot eat when you want to concentrate on building your body. He also lists the exercise and you are supposed to do per day like each day. This year, it is still a page about that and this time it is more we help our friends who are freshmen and they are pretty interested in this kind of stuff. We keep doing it for them right now.

The other participant mentioned sharing the news of the death of a pet with cousins living overseas using Facebook wall posts. S/he felt that Facebook provided an effective way to share information broadly, as s/he commented _'it was easier to tell them than calling them because I cannot hold them together'_. The same participant observed others sharing information about the health condition of a loved one with relatives.

Participating in group activities potentially involves both receiving and providing information, and the intention of this participation was often to learn about a subject. Two participants reported participating in support groups; one joined a medical terminology group on Facebook because she felt that it could help her learn medical vocabularies and the other joined a support group for a particular condition, asking questions, receiving suggestions, sharing information and responding to others.

The social networking functions that supported these different modes of behaviour included chat, messages, wall posts, searches, notes and picture posting.

### Users' perceptions of social networking sites for health and wellness information

Participants' use of social networking sites for health and wellness information is mediated by their perceptions of such sites in relation to this particular type of information. Thus, we examined their perceptions of this use. Since there is a lack of existing frameworks concerning the major components of a social networking site as a place for information, we asked open questions and allowed themes to emerge from the interview conversations. The analysis of the transcripts suggested that participants perceived and appraised social networking sites as a source for health information mainly from four aspects: the information existing on the network, the people and organizations involved, the social networking technology and the social consequences of the usage.

#### Information

The participants perceived and appraised the existing information on social networking sites in relation to credibility, availability, relevance and currency of the information, as shown in Table 2.

<table class="center" style="width: 75%;"><caption>  
**Table 2\. Participants' perceptions of the health and wellness related information on social networking sites**</caption>

<tbody>

<tr>

<th>Aspect</th>

<th>Perceptions</th>

<th>No.</th>

</tr>

<tr>

<td>Credibility, trustworthiness, validity, reliabilty</td>

<td>**Positive**: reliable, good advice  
**Negative**: not credible, not reliable, not accurate, false, biased</td>

<td style="text-align:center">2  

16</td>

</tr>

<tr>

<td>Availability</td>

<td>Do not see others using it  
Do not know where to look  
Have no health information on social networking sites</td>

<td style="text-align:center">12</td>

</tr>

<tr>

<td>Relevance</td>

<td>**Positive**: personalized, unique  
**Negative**: off-topic, not specific, not systematic</td>

<td style="text-align:center">2  
3</td>

</tr>

<tr>

<td>Currency</td>

<td>**Negative**: not up-to-date</td>

<td style="text-align:center">1</td>

</tr>

</tbody>

</table>

Credibility of the information was the aspect that concerned most of the participants. Specifically, they were concerned about whether the information was credible, reliable, trustworthy, accurate and objective. Overall, as shown in Table 2, the participants expressed a negative view of the credibility of information on social networking sites, with 16 (42%) explicitly stating that the information was _'not reliable', 'false', 'not systematic' and 'biased'_. A comment from one participant illustrates this view:

> Getting information off Facebook would be, it's like getting homework help off Wikipedia. It's like, not accredited, but the information is there, so you don't know what to trust.

Only one participant commented that the information on social networking sites was _'reliable'_ and another mentioned that he did get 'good advice' on how to get rid of the flu.

Availability of the information is another aspect of major concern. Twelve participants (32%) mentioned that health and wellness information was not very visible on social networking sites. They had not seen others using it, did not know where to look, or thought that there was no health related information on social networking sites.

In terms of relevance, participants had conflicting views. One view was that information on social networking sites was personalized (_'I feel like the people you are talking to understand what you are talking about, know what you are going through'_) and unique (_'I do think social network site can help the emotional part of it and some other like things you would probably never be able to find on search engines'_). The other view was that information on social networking sites was _'not specific', 'not systematic'_ and sometimes could be _'off topic'_.

Only one participant commented on the currency of information stating that the information on social networking sites was not up-to-date.

#### People and organizations

Social networking sites, by nature, are networks of connections between friends, acquaintances or peers with common interests. Thus, people are a major contributor of information. When using social networking sites for health and wellness information, many of the participants thought about the trustworthiness of their friends. Specifically, they were mainly concerned about the domain knowledge of their friends and the quality of personal opinions from peers who were not healthcare professionals. For example, one participant commented:

> Most friends are of my age. So, that's what prevents me from asking them so many questions about what should I do because they probably have the same knowledge as I do.

Another commented:

> [Information on social network sites] is not trustworthy, I will take an opinion from a doctors' Website rather than my friends, you know, my buddies they don't know really know much.

If their friends were doctors, physicians or other healthcare professionals, they tended to trust the friends' domain knowledge and would be more likely to use social networking sites for health information. For example,

> If I know somebody is a doctor or something like that I may ask [him/her] questions about medical problems — hey my friend is feeling this way, do you have any idea what might be going on with her or him. May be that's the only sense I will use [social networking sites] for medical purposes but other than that I do not think I will.

> [I would use social networking sites for health and wellness information] if famous or good doctors are part of it, it would definitely be useful because they have so much knowledge.

Several participants also commented that if their friends on the social networking sites had similar conditions or shared similar health needs or interests they would also be more likely to use the sites for health information.

> I guess if I had like maybe a similar injury to my friend. I think that would be the only time I could think of using Facebook. If my friend broke his arm and I feel like I broke my arm then I would be like one 'how bad it feels' and two 'what kind of rehab did you do to help it out'… Maybe [I would also ask friends for] some physical therapy advice because a lot of my friends are pretty athletic and into weight training and stuff.

They would also be more likely to use social networking sites for health and wellness information when sites have an effective rating system in place, so that the reputation of the individual who provides the information could be easily judged:

> [A good social networking site needs to] make sure [a person] is not just putting random stuff, [s/he should] have reputation. [Others] vote stars that how well this person knows based on their knowledge so you have red points like [if a person] has 700 red points and then you [get to know that] this man know what he is talking about and then I will try to read what that person wrote in that forum.

In addition to people, a couple of participants also mentioned organizations or groups. When organizations or groups were mentioned, reputation of these entities was of major concern and the participants expressed a sceptical attitude.

#### Social networking technology

Both information and people are situated in the technological context of social networking sites. The importance of usability for the adoption and use of any tool or technology on a large scale has been shown in many different domains ([Norman 1983](#nor83)). In this study, the participants also brought up usability of the social networking sites as a primary criterion for appraising these sites. The majority thought social networking sites, as a technology, were accessible and useable; this platform was considered _'convenient'_, _'fast'_, _'useful'_, _'effective'_; in broadcasting information to a group of people and _"easy-to-use"_. However, a few also commented that, in the context of health and wellness, because social networking sites are a mediated communication channel, they were not as effective as face-to-face interaction and not good for emergency situations.

#### Social consequences: privacy, security, self-identity and inspiration

A major concern associated with using social networking sites for health and wellness information was privacy. Eight participants (21%) believed that health issues are very personal; they did not want their existing social ties to know about their illness and also felt uncomfortable broadcasting something like medications they were on. Moreover, some were concerned about network security: they did not feel safe to put such personal information on Facebook and were concerned that their health information might be used against them. The following two comments illustrate these privacy and security concerns:

> Whenever I am in pain or anything…, I just go to that WebMD Website and say what is wrong with me, do that little symptom checker or I will go on a forum and ask people like what this is. I wouldn't put it on the social networking site because then people will know what my problem is. In forums people don't know who you really are so you can do it anonymously so I would rather do that sometimes.

> It's not something I feel like it need to be made public, like whether it is an allergy medication or if I was on anti-depression medications, either way, I don't really feel like it's something that need to be broadcasted.

One participant also expressed the desire to manage social impressions and maintain a positive and 'cool' image in her network by pointing out that she is not willing to be viewed as a 'grandma' by posting health related messages.

Several participants spoke of positive social consequences of using social networking sites for health information. They felt that by joining Facebook groups, they could be motivated and inspired to keep exercising and be on healthy diets. As one commented:

> That will be a very good idea if they can add that forum to that because lot of people on FB also talk about exercise that I want to run 3 miles, or I am going to go to work out for the spring break, beach blah blah blah… So those people will go to this site you know and that also inspires you like one of my friends say I will go on a run and then I think I should go and run too and then people will join that forum to know how to stay healthy.

### Perceived use of social networking sites

In order to examine how participants' perceptions could influence their (potential) use of social networking sites for health and wellness information, we asked under what circumstances they would use social networking sites for health purposes. Among the twenty-eight participants who had not used social networking sites for health and wellness information, eight expressed minimum intention to use social networking sites for this purpose in the foreseeable future, as one commented: _'No, never. I will use Google, Yahoo, but not Facebook, for that'_. The other commented: _'[I would use social networking sites] if Wikipedia, WebMD, and Yahoo were completely shut down. I mean it is probably the last place I will turn in for [health and wellness] information'_.

The remaining twenty participants were receptive, in varying degrees, about this use. Three major ways in which social networking sites might be used were identified: to look for health and wellness related information, to communicate with friends on health and wellness related subjects and to join health organizations or groups.

Ten participants pointed out that they would like to use social networking sites for information about healthy lifestyles, such as physical therapy advice and information about losing weight, as well as information about the treatment of _common_ and _mild_ conditions, including back pain, colds and headaches. One participant mentioned that he would use social networking sites for health surveillance purposes:

> If people on their status say they are sick or they have a cold, in a way you think like oh it is just in that area that's having this cold passing around.

Five participants mentioned that they would use social networking sites as a communication tool to get in contact with healthcare professionals, tell friends to get better, ask friends if they want to go exercise together, as well as spread information about a virus or sickness.

> I would use social network sites when I want to ask "when do you guys want to go for a run", I will post that.

The other four participants stated that they would use social networking sites to participate in group activities, including joining a gym, finding and participating in a support group and finding health organizations that they are interested in. For example, one participant said:

> If I had interest in, let's say, cancer organizations, I will use the social network site to find [a group] that matches my interest and join it.

It is worth noting that, during the interviews, twenty-six participants (68%) pointed out their preferred sources for health and wellness information. These sources included, in the order of the frequency of occurrence:

*   Search engines (primarily Google)
*   Doctors
*   Specific Websites (e.g., WebMD, fitness.com, CNN, YouTube)
*   Forums, blogs, Wikipedia
*   University resources: library databases and UT-Health
*   Parents

### A model of users' acceptance of social networking sites for health and wellness information

As reported, the participants perceived the social networking sites as a source for health and wellness information from four aspects: information, people and organizations, technology and social consequences. The technology acceptance model postulates that user acceptance and use of a technology is determined by two factors: perceived usefulness and perceived ease of use ([Davis 1989](#dav89)). Perceived usefulness is defined as '_the degree to which a user believes that using the system will enhance his/her performance_' and perceived ease of use is defined as '_the degree to which the user believes that using the system will be free of effort_' ([Davis 1989: 320](#dav89)). The first two aspects of the participants' perceptions are associated with the usefulness of the social networking sites, as the quality of the information directly determines whether the use of social networking sites would enhance a person's health condition. The third aspect, technology, is associated with ease-of-use, as both usability and accessibility of the social networking sites have great influence on users' perceptions of whether the systems are easy to use.

The theory of reasoned action states that the users' intention to use a technology could be predicted by two factors: attitudes about the behaviour and subjective norms ([Fishbein and Ajzen 1975](#fis75)). Subjective norms are people's perceptions of whether most people who are important to them approve of their behaviour ([Fishbein and Ajzen 1975](#fis75)). The fourth aspect of perception, social consequences, coincides with this factor, as it depicts the participants' perceptions of how other people are going to view them.

Based on the technology acceptance model and theory of reasoned action ([Dillon and Morris, 1996](#dil96)), we are able to suggest that users' intention to use, as well as their actual use of social networking sites for health and wellness information, is directly influenced by the four aspects of their perceptions of social networking sites, as shown in Figure 1.

<figure>![Model of User Acceptance](p523fig1.png)

<figcaption>Figure 1: A model of users' acceptance of social networking sites for health and wellness information</figcaption>

</figure>

Positive views of these aspects, such as a perceived high quality of information on the sites and credible friends, encourage the adoption and use of the social networking sites and negative views, such as potential security breaches and out-of-date information, hinder the use. As suggested by the results reported above, in this study the social networking sites were underused and the participants lacked the intention to use them for serious health and wellness information.

## Discussion

Social networking sites have become prominent in today's diverse digital environment. Only a few studies have explored people's behaviour using social networking sites for health and wellness information, thus little is known about how people make use of such sites for this popular type of everyday life information seeking ([Fox 2011](#fox11); [Newman _et al._ 2011](#new11)). We interviewed thirty-eight college students about their use of social networking sites for health and wellness related purposes, as well as their perceptions of social networking sites as an information resource. Our findings support the results of the recent Pew research that people use social networking sites for health purposes, but this behaviour has not yet become a mainstream activity ([Fox 2011](#fox11);). In this study, only 26% of participants reported that they had used social networking sites for health related information.

Although not popular, the health information behaviour in social networking sites reflects different everyday life information seeking modes. Consistent with Savolainen ([1995](#sav95)), the participants used social networking sites for both orienting (e.g., keeping updated with friends' health) and problem solving (e.g., finding out means to cure flu) purposes. In terms of actions, they were actively searching for information (active seeking), actively following information threads about nutrition and diets (active scanning) and monitoring friends' and relatives' health status updates (non-directed monitoring). These activities are consistent with McKenzie's ([2003](#mck03)) model of everyday-life information seeking practices. This study has identified two additional modes of behaviour: sharing information with others and participating in group activities: both manifest a two-way communication and are supported by the fact that social networking sites are not only an information channel, but also a communication channel. As Web technologies became more and more interactive and social, behavioural models of everyday life information seeking need to be expanded to embrace the emerging new capacities of the technologies.

Savolainen ([2007](#sav07)) pointed out that researchers should dig deeper in everyday life information seeking studies by elaborating source preference criteria. The second contribution of this study is the identification of criteria that the students employed to decide the selection and use of social networking sites as a source for health information. It was found that in considering social networking sites for health related information, users perceived four major factors: information, people and organizations, technology and social consequences of the usage. In perceiving the information, significant weight was placed on the credibility, availability and accessibility, and relevance of the information. In perceiving people and organizations, the weight was placed on the domain knowledge and personal experience of the people and the reputation of the organizations. In perceiving the technology, the usability and accessibility was brought up.

In perceiving the social consequences, the desire to protect privacy, manage social impressions (present a positive image of self) and avoid social risk (avoid presenting an unhealthy image of self and prevent misuse of one's health information) was emphasized. These concerns, to a large degree, are attributable to two features of social networking sites. First, because people's online social networks are often formed based on existing social ties, it is difficult for individuals to stay anonymous. Second, tools on social networking sites are designed to best support sharing information to a broad audience and often default to sharing broadly ([Newman _et al._ 2011](#new11)). This finding also confirmed that seeking serious health information is a sensitive and private matter for most people ([Spink and Cole 2001a](#spi01a)). It is likely that they want to have control over the flow of such information. Where the information goes to is more arbitrary on social networking sites than on search engines or in communications with doctors.

Informed by the technology acceptance model and the theory of reasoned action, we were able to postulate that these four aspects of the participants' perceptions of social networking sites for health and wellness information have a direct impact on their adoption of this technology. In this study, overall, participants had an unfavourable view of social networking sites as a source of health information. They questioned the quality and accessibility of the information on social networking sites, expressed concerns about the lack of medical expertise or shared experiences of their social ties and were wary about the possibility that the openness of the network environment would harm their privacy and personal image. As a result, the social networking sites were underused and the intention to use them for health purposes was lacking. Those who were willing to use them would only seek and share, primarily, information about healthy lifestyles (nutrition, diet plans, organic food, recipes and exercise routines), weight loss, physical therapies, or treating common and mild conditions (e.g., flu, colds and headaches), rather than information concerning their _real_ health. For the latter, other sources, primarily search engines, doctors and reputable health Websites, were preferred.

To support better the use of social networking sites for health and wellness purposes, system designers need to address the concerns that the participants had in their perceptions of social networking sites. Based on the research results, we propose the following design recommendations:

*   First and foremost, social networking sites need to provide clear indications of the quality of the health information on the sites, such as the source of information and the profession or reputation of the contributors. Mechanisms, such as an effective voting system, could be established to assist users in judging the quality of information.
*   Second, social sites need to make health and wellness information visible to users. Functions, such as aggregating various health and wellness related information (e.g., posts, messages, videos and notes), recommending sources in context and integrating users' preferred sources into the network, could be implemented to promote source visibility and accessibility.
*   Third, social sites should capitalize on their knowledge about the user to provide more personalized services. Functions such as finding users with similar experience or health goals could be implemented.
*   Last but not least, social sites should be explicit about how they protect users' privacy. Different functions such as allowing users to define custom groups, allowing users to send information to a targeted group of users and providing a visual view of where messages go and who can view them could be provided. These functions could help users feel more comfortable about seeking and sharing health information. In some cases, social platforms should allow users to stay anonymous.

This study has limitations. First, because of the sensitive nature of health information, the participants might have used social networking sites for such information, but not been willing to talk about it in the interviews. It could be helpful to frame the interview questions to ask for their observation of friends' health information behaviour. Secondly, the sample was a group of undergraduate students. This group was young and comfortable with the new technology. In future studies, it would be worthwhile to examine older groups' use and perceptions of using social networking sites for health purposes, as they might have different kinds of health demands and be less comfortable with the technology.

## Conclusions

This study explores college students' use of social networking sites, particularly Facebook, for health and wellness information. Consistent with recent Pew research findings, the use of social networking sites for health queries is sparing. Those who did use the platform used it mostly for health updates, lifestyle information and treatments of mild conditions, rather than more serious health issues. Exploration of users' perceptions of social networking sites found that users were skeptical about the quality of information, concerned about the lack of medical knowledge of their peers and wary about possible threats to their privacy and potential misuse of their health information. Future social networking sites with an intention to support health related information seeking and communication need to be able to make the information more visible and readily accessible to users; provide explicit cues to convey source credibility, reliability and reputation; and provide effective means to protect user privacy and foster trust on the platform. Future studies could be performed to examine how older generations use social networking sites for health and wellness purposes. Studies are also needed to understand users' perceptions of other social sources and how users' perceptions mediate the selection of these sources in the context of performing everyday life information seeking for health and wellness information.

## Acknowledgements

The author wants to thank all the participants for their contributions to the study and thank Bretagne Abirached for her help with part of the data collection. The research is partially supported by the Research Grant from the Office of the Vice President for Research at the University of Texas at Austin.

## About the author

**Yan Zhang** is an assistant professor at the School of Information at the University of Texas at Austin. Her research focuses on consumer health information seeking behavior, psychological processes involved in people interacting with health information retrieval systems and consumer health information system design. She can be contacted at [yanz@ischool.utexas.edu](mailto:yanz@ischool.utexas.edu)

#### References

*   Agosto, D. & Hughes-Hassell, S. (2005). People, places and questions: an investigation of the everyday life information-seeking behaviors of urban young adults. _Library & Information Science Research_, **27** (2), 141-163.
*   Bahr, D.B., Browning, R.C., Wyatt, H.R. & Hill, J.O. (2009). Exploiting social networks to mitigate the obesity epidemic. _Obesity_, **17**, 723-728.
*   Bishop, A.P., Tidline, T.J., Shoemaker, S. & Salela, P. (1999). Public libraries and networked information services in low-income communities. _Library & Information Science Research_, **21**(3), 361-390.
*   Blank, T.O., Schmidt, S.D., Vangsness, S.A., Monteiro, A. K. & Santagata, P.V. (2010). Differences among breast and prostate cancer online support groups. _Computers in Human Behaviour_, **26**(6), 1400-1404.
*   Boyd, D.M. & Ellison, N. B. (2007). [Social network sites: Definition, history and scholarship. _Journal of Computer-Mediated Communication_](http://www.Webcitation.org/65jNZJgvp), **13** (1), article 11\. Retrieved 12 December, 2011 from http://jcmc.indiana.edu/vol13/issue1/boyd.ellison.html (Archived by WebCite® at http://www.Webcitation.org/65jNZJgvp)
*   Campbell, H., Phaneuf, M. & Deane, K. (2004). Cancer peer support programs—do they work? _Patient Education and Counsel_, **55**, 3-15.
*   Choo, C.W., Detlor, B. & Turnbull, B. (2000). [Information seeking on the Web: an integrated model of browsing and searching](http://www.Webcitation.org/65jMzei1r). _First Monday_, **5** (2). Retrieved 12 December, 2011 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/729/638 (Archived by WebCite® at http://www.Webcitation.org/65jMzei1r)
*   Christakis, N. A. & Fowler, J.H. (2007). The spread of obesity in a large social network over 32 years. _New England Journal of Medicine_, **357**(4), 370-379.
*   Connaway, L. S., Dickey, T. J. & Radford, M. L. (2011). "If it is too inconvenient I'm not going after it": Convenience as a critical factor in information-seeking behaviors. _Library & Information Science Research_, **33**(3), 179-190.
*   Consolvo, S., Everitt, K., Smith, I. & Landay, J. (2006). [Design requirements for technologies that encourage physical activity](http://www.webcitation.org/69FWYWJHF). _CHI '06 Proceedings of the SIGCHI conference on Human Factors in computing systems_, April 22-27, 2006, Montreal, Québec, Canada (pp. 457-466.) New York, NY: ACM Press. Retrieved 18 July, 2012 from http://www.katherineeveritt.com/papers/p457-consolvo.pdf http://www.webcitation.org/69FWYWJHF)
*   Davis, F.D. (1989). Perceived usefulness, perceived ease of use and user acceptance of information technology. _MIS Quarterly_, **13**(3), 319-339.
*   Dervin, B. (1983). Information as user construct: the relevance of perceived information needs to synthesis and interpretation. In S.A. Ward & L.J. Reed (Eds.), _Knowledge structure and use: implications for synthesis and interpretation_, (pp. 153-183). Philadelphia, PA: Temple University Press.
*   Dillon, A., & Morris, M. (1996). User acceptance of new information technology: theories and models. _Annual Review of Information Science and Technology_, **31**, 3-32.
*   Eysenbach, G., Powell, J., Englesakis, M., Rizo, C. & Stern, A. (2004). Health related virtual communities and electronic support groups: systematic review of the effects of online peer to peer interactions. _British Medical Journal_, **328**(7449), 1166-1171.
*   Fishbein, M. & Ajzen, I. (1975). _Belief, attitude, intention and behavior: an introduction to theory and research_. Reading, MA: Addison-Wesley.
*   Fox, S. (2010, 13 Sep.). [_The power of mobile_](http://www.Webcitation.org/65jN2O8pY). Washington, DC: Pew Internet and American Life Project. [Video presentation and text commentary.] Retrieved 22 September, 2010 from http://pewinternet.org/Commentary/2010/September/The-Power-of-Mobile.aspx (Archived by WebCite® at http://www.Webcitation.org/65jN2O8pY)
*   Fox, S. (2011). [_The social life of health information, 2011._](http://www.Webcitation.org/65jNGftnA) Washington, DC: Pew Internet and American Life Project. Retrieved 7 October, 2011 from http://www.pewinternet.org/~/media//Files/Reports/2011/PIP_Social_Life_of_Health_Info.pdf (Archived by WebCite® at http://www.Webcitation.org/65jNGftnA)
*   Ginossar, T. (2008). Online participation: a content analysis of differences in utilization of two online cancer communities by men and women, patients and family members. _Health Communication_, **23**(1), 1-12.
*   Glaser, B.G. & Strauss, A.L. (1967). _The discovery of grounded theory: strategies for qualitative research_. New York, NY: Aldine.
*   Hoffman, B. R., Monge, P., Chou, C.P. & Valente, T.W. (2006). Peer influence on adolescent smoking: a theoretical review of the literature. _Substance Use and Misuse_, **41**(1), 103-155.
*   Hwang, K.O., Ottenbacher, A.J., Green, A.P., Canon-Diehl, M.R., Richardson, O., Bernstam, E.V. & Thomas, E.J. (2010). Social support in an Internet weight loss community. _International Journal of Medical Informatics_, **79**(1), 5-13.
*   Johnson, J.D. & Meischke, H. (1991). Cancer information: women's source and content preferences. _Journal of Health Care Marketing_, **11**(1), 37-44.
*   Julien, H. & Michels, D. (2004). Intra-individual information behaviour in daily life. _Information Processing & Management_, **40**(3), 547-562.
*   Klemm, P., Reppert, K. & Visich, L. (1998). A nontraditional cancer support group: the Internet. _Computers in Nursing_, **16**(1), 31-36.
*   Korda, H. & Itani, Z. (2011). Harnessing social media for health promotion and behaviour change. _Health Promotion Practice_. (Forthcoming).
*   Lampe, C., Ellison, N.B. & Steinfield, C. (2008). [Changes in use and perception of Facebook.](http://www.webcitation.org/69Fe9tcPK) In _Proceedings of CSCW: 2008 ACM Conference on Computer Supported Cooperative Work_, (pp. 721-730) New York, NY: ACM Press. Retrieved 18 July, 2012 from http://gatortracks.pbworks.com/f/facebook%2Bchanges%2Bin%2Buse.pdf (Archived by WebCite® at http://www.webcitation.org/69Fe9tcPK)
*   Madden, M. & Zickuhr, K. (2011). [_65% of online adults use social networking sites_](http://www.Webcitation.org/65jNJ2vU2). Washington, DC: Pew Internet & American Life Project. Retrieved 12 October, 2011 from http://www.pewinternet.org/~/media//Files/Reports/2011/PIP-sns-Update-2011.pdf (Archived by WebCite® at http://www.Webcitation.org/65jNJ2vU2)
*   McKenzie, P. J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of Documentation_, **59**(1), 19-44.
*   Morris, M. R., Teevan, J. & Panovich, K. (2010). [What do people ask their social networks and why? A survey study of status message Q&A behaviour](http://www.webcitation.org/69FePQGaT). In _Proceedings of the 28th international conference on Human factors in computing systems_, (pp. 1739-1748). New York, NY: ACM Press. Retrieved 18 July, 2012 from http://research.microsoft.com/pubs/154559/chi10-social.pdf (Archived by WebCite® at http://www.webcitation.org/69FePQGaT)
*   Morris, E.M., Consolvo, S., Munson, S.A., Kramer, A.D.I., Patrick, K. & Tsai, J. (2011). Facebook for health: opportunities and challenges for driving behavior change. In _Proceedings of the 2011 Annual Conference. Extended Abstracts on Human Factors in Computing Systems_, (pp. 443-446). New York, NY: ACM Press.
*   Nambisan, P. (2011). [Information seeking and social support in online health communities: impact on patients' perceived empathy](http://www.webcitation.org/69FeuaRYt). _Journal of the American Medical Informatics Association_, **18**(3), 298-304\. Retrieved 18 July, 2012 from http://jamia.bmj.com/content/18/3/298.full.pdf+html (Archived by WebCite® at http://www.webcitation.org/69FeuaRYt)
*   Newman, M.W., Lauterbach, D., Munson, S.A., Resnick, P. & Morris, M.E. (2011). ['It's not that I don't have problems, I'm just not putting them on Facebook': challenges and opportunities in using online social networks for health.](http://www.webcitation.org/69FfFy30L) In _Proceedings of the ACM 2011 Conference on Computer Supported Cooperative Work_, (pp. 341-350). New York, NY: ACM Press. Retrieved 18 July, 2012 from http://misc.si.umich.edu/media/papers/OnlineSupportNetworks_Final_CSCW11.pdf (Archived by WebCite® at http://www.webcitation.org/69FfFy30L)
*   Norman, D. A. (1983). Some observations on mental models. In D. Gentner & A. L. Stevens (Eds.), _Mental models_ (pp. 7-14). Hillsdale, NJ: Erlbaum.
*   Preece, J. (1998). Empathic communities: researching out across the Web. _Interactions_, **5**(2), 32-43.
*   Rouse, W. B. & Morris, N. M. (1986). On looking into the black-box: prospects and limits in the search for mental models. _Psychological Bulletin_, **100**(3), 349-363.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of 'way of life'. _Library and Information Science Research_, **17**(3), 259-294.
*   Savolainen, R. (1999). [The role of the internet in information seeking: putting the networked services in context](http://www.webcitation.org/69FfUAtqN). _Information Processing & Management_, **35**(6), 765-782\. Retrieved 18 July, 2012 from http://www.viktoria.se/~dixi/BISON/resources/savolainen%201995.pdf (Archived by WebCite® at http://www.webcitation.org/69FfUAtqN)
*   Savolainen, R. (2007). Information source horizons and source preferences of environmental activists: a social phenomenological approach. _Journal of the American Society for Information Science and Technology_, **58**(12), 1709-1719.
*   Savolainen, R. & Kari, J. (2004). Placing the Internet in information source horizons: a study of information seeking by Internet users in the context of self-development. _Library & Information Science Research_, **26**(4), 415-433.
*   Scanfeld, D., Scanfeld, V. & Larson, E.L. (2010). Dissemination of health information through social networks: Twitter and antibiotics. _American Journal of Infection Control_, **38**(3), 182-188.
*   Skeels, M.M., Unruh, K.T., Powell, C. & Pratt, W. (2010). [Catalyzing social support for breast cancer patients.](http://www.webcitation.org/69Fg6KIy9) In _Proceedings of the 28th International Conference on Human Factors in Computing Systems_, (pp.173-182.) New York, NY: ACM Press. Retrieved 18 July, 2012 from http://faculty.washington.edu/wpratt/Publications/pap1135-Skeels.pdf (Archived by WebCite® at http://www.webcitation.org/69Fg6KIy9)
*   Smith, K.P. & Christakis, N.A. (2008). Social networks and health. _The Annual Review of Sociology_, **34**, 405-429.
*   Spink, A. & Cole, C. (2001a). Information and poverty: information-seeking channels used by African American low-income households. _Library & Information Science Research_, **23**(1), 45-65.
*   Spink, A. & Cole, C. (2001b). Introduction to the special issue: everyday life information-seeking research. _Library & Information Science Research_, **23**(4), 301-304.
*   Wang, P., Hawk, W. B. & Tenopir, C. (2000). Users' interaction with World Wide Web resources: an exploratory study using a holistic approach. _Information Processing and Management_, **36**(2), 229-251.
*   Wicks, P., Massagli, M., Frost, J., Brownstein, C., Okun, S., Vaughan, T., Bradley, R. & Heywood, J. (2010). [Sharing health data for better outcomes on PatientsLikeMe](http://www.Webcitation.org/65jNQISxK). _Journal of Medical Internet Research_, **12**(2), e19\. Accessed 12 December, 2011 from http://www.jmir.org/2010/2/e19/ (Archived by WebCite® at http://www.Webcitation.org/65jNQISxK
*   Wilson, T.D. (1997). Information behaviour: an inter-disciplinary perspective. In P. Vakkari, R. Savolainen & B. Dervin, (Eds.), Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts (pp. 39-50). London: Taylor Graham.
*   Wing, R.R. & Jeffery, R.W. (1999). Benefits of recruiting participants with friends and increasing social support for weight loss and maintenance. _Journal of Consulting and Clinical Psychology_, **67**(1), 132-38.
*   Young, R. M. (1983). Surrogates and mappings: two kinds of conceptual models for interactive devices. In D. Gentner & A. L. Stevens (Eds.), _Mental models_ (pp. 35-52). Hillsdale, NJ: Lawrence Erlbaum Associates.
*   Zhang, Y. & Wildemuth, B. M. (2009). Qualitative analysis of content. In B. Wildemuth (Ed.), _Applications of social research methods to questions in information and library science_ (pp.308-319). Westport, CT: Libraries Unlimited.
*   Zickuhr, K. (2010). [_Generations 2010_](http://www.Webcitation.org/65jNU8YHh). Washington, DC: Pew Internet & American Life Project. Retrieved on 17 October, 2011 from http://www.pewinternet.org/~/media//Files/Reports/2010/ PIP_Generations_and_Tech10.pdf (Archived by WebCite® at http://www.Webcitation.org/65jNU8YHh