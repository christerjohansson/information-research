<header>

#### vol. 19 no. 2, June, 2014

</header>

<article>

# Information creation and the ideological code of managerialism in the work of keeping track

#### [Pamela J. McKenzie](#authors), [Elisabeth Davies](#authors) and [Sherilyn Williams](#authors)  
Faculty of Information and Media Studies, The University of Western Ontario, London, Ontario, Canada N6A 5B7

#### Abstract

> **Introduction**. This paper considers the practices of information creation in personal information management by studying the work of keeping track in everyday life, e.g., creating lists and calendars.  
> **Method**. We interviewed ten participants from two Canadian provinces about how they keep track and we observed and photographed the physical spaces and the documents they created and used. Our data set consists of fourteen hours of interviews, 330 photographs and 500 pages of interview transcripts.  
> **Analysis**. We used the qualitative technique of constant comparison within an abductive framework of relational and discourse analysis to study a) how the domestic work of keeping track hooks into the requirements of organizations such as schools and workplaces, and b) how talk about keeping track relates to participants' presentations of themselves as good workers, parents, citizens, etc.  
> **Results**. The work of keeping track functions in terms of Dorothy Smith's concept of the ideological code. A managerial imperative pervades this work, even in domestic contexts, and participants made use of workplace genres and conventions.  
> **Conclusions**. Even in households, the work of keeping track is embedded within organizational contexts. Managerialism is produced and reproduced as an ideological code that shapes participants' information creation and their talk about it.

<section>

## Introduction

Everyday life, for many individuals and families in the industrialised world, is made up of interwoven threads of home, school, leisure and workplace activity ([DeVault, 2003](#dev03); [Griffith and Smith, 2005](#gri05); [Hochschild and Machung, 2003](#hoc03)). Navigating everyday life therefore increasingly requires the coordination of people and resources in time, and across multiple physical spaces and organizational settings ([Daly, 2002](#dal02); [Darrah, Freeman and English-Lueck, 2007](#dar07)). _Keeping track_ is a process by which people monitor and document this coordination and remind themselves and others of what must be done and when, where, and by whom. People keep track with a range of informational tools and texts ([McKenzie and Davies, 2012](#mck12)), including handwritten entries in appointment diaries, lists and inventories, and smartphone calendaring applications, as well as conversations with, and phone, e-mail, and text messages to family members, and artful physical placement of objects in places where they will be seen again ([Crabtree, 2003](#cra03)).

This paper forms part of an ongoing research project into how people keep track in their everyday lives. It contributes to the research on information behaviour and impacts in two ways. First, we contribute to the scholarship on personal information management by focusing on information creation, a component of document work ([Trace, 2007](#tra07)). We contend that the documentary, inscription and discursive practices of keeping track are examples of information creation; for example, writing notations on the kitchen calendar, making entries in shared electronic calendars and recording details in diaries. Second, we build on studies of socially situated information seeking and use by showing the broader contexts within which individual information creators make their decisions, and the ways in which those contexts may constrain the options available to information creators. We view keeping track as a practice mediated by texts. The information created and inscribed in texts whilst keeping track contain what Smith ([1993](#smi93)) calls an _ideological code_, that is, a consensus vocabulary that is

> woven into political discourse, budgeting, policy implementation, media reports, and so on, in ways that give them purchase with different audiences; as people take up these vocabularies, they take up beliefs on which they rest, which come to be widely accepted ([DeVault, 2008, p. 293](#dev08)).

Our research builds on previous research on personal information management by introducing ideas from critical management studies ([Alvesson and Willmott, 1992](#alv92)), which studies how capitalism is entwined with _other structures of domination_ ([Adler, Forbes and Wilmott, 2007](#adl07)), including those that operate outside of institutional settings. We show that the work of keeping track embeds the ideological code of managerialism and frames everyday life as something to be managed, as institutional in addition to simply domestic.

## Literature review

Studies of personal information management identify the mechanisms people use for keeping, managing and exploiting personal information ([Whittaker, 2011](#whi11)), and show how the work of information management is situated within household and workplace contexts. Several studies (e.g. [Jones and Teevan, 2007](#jon07), [Bruce, Wenning, Jones, Vinson and Jones, 2011](#bru11)) have shown that effective personal information management is challenging for most people as it requires a prediction of how, when and where information will be needed in the future. The management of what Whittaker ([2011](#whi11)) calls _actionable items_, such as calendars, lists and notes, that contain details of tasks the user must do, is further complicated by the need for these items themselves to serve as reminders. Actionable items may then be discarded, or they may be kept as future records of household transactions or as mementos of significant events or relationships ([Marshall, 2007](#mar07); [McKenzie and Davies, 2013](#mck13)).

However, research on personal information management, like information behaviour research more generally, focuses on the seeking, organizing, keeping, sharing and use of information, and often neglects the initial steps of document creation. Trace ([2007, p. 142](#tra07)) argued for a research focus on information creation: _'how and why people are socialized to create information in various contexts (whether in everyday life or in the working world)_' and contended that such a study would shed light on '_the fundamental skills and knowledge that come into play in creating information, and the larger role that genres (or physical forms) of information play in society_'.

Trace’s ([2007](#tra07)) own study documented the information creation work of two classes of American schoolchildren and their teachers over a nine-month period. She worked from an ethnomethodological perspective that attended to the social and cultural _stock of knowledge_ that individuals bring to the practice of information creation. This stock of knowledge both comes from individuals' own experience and is derived from their interactions with others. It is expressed through everyday language and practice. Trace found that information creation related both to the explicit curriculum of the school classroom (i.e., to the subjects under study), and also to a hidden curriculum of social norms and regulations, such as negotiating relationships with others, dealing with authority and completing work in a timely fashion.

Trace ([2007, p. 145-146](#tra07)) argued that information creation formed part of the _hidden curriculum_ of the classroom in two ways. First, the documents that teachers and students created served to communicate and sanction classroom norms. Second, knowledge of how to create and use documents in the classroom was in and of itself part of the hidden curriculum. Document work, including the work of information creation, therefore functioned '_both as an articulation and as an enactment of the hidden curriculum_' ([Trace, 2007, p. 146](#tra07)).

Trace's interactionist perspective enabled her to focus on what actually happens in a social setting, and to identify how the classroom setting is constructed and reconstructed on an ongoing basis. This approach allowed her to understand information creation and the hidden curriculum, not within a structural framework but as arising out of '_the day-to-day negotiations that occur in interactions between and among people in the classroom setting_’ ([Trace, 2007, p. 146](#tra07)). Attention to the interactional particularities and locally-constituted context enabled Trace to show how classroom information creation followed specific forms, genres and conventions, and how knowledge of how to read, write, search, and use documents appropriately was constituted and evaluated through the practices of a specific community. This constitution and evaluation have real implications: identity as a _good_ student is contingent on learning and applying these norms, even when they are not explicitly articulated ([Trace, 2007](#tra07)).

By studying the ongoing development of the local setting, Trace ([2007, p. 157](#tra07)) also interrogated the broader context: '_if we know the explicit norms or stock of knowledge that students learn about documents, we have to ask ourselves in a larger sense why this is so_'. Dorothy Smith's work has similarly focused on explicating how the everyday activities of individuals are both coordinated within a local setting and traceable to extra-local sites of power. Smith’s concept of the ideological code ([1993](#smi93)) provides a useful lens through which to understand the ways that broader sets of values and interests come to inflect individuals’ stock of knowledge and thereby their practices of information creation.

An ideological code is not a definite category or concept; rather, it is a

> constant generator of procedures for selecting syntax, categories, vocabulary in writing and speaking, for interpreting what is written and spoken, and for positioning and relating discursive subjects. It is not as such social organization, but it is a social organizer ([Smith, 1995, p. 26](#smi95)).

Smith's model for the ideological code is the genetic code and in particular the ability of the DNA molecule to replicate.

> An ideological code... operates in a field of public discourse to structure text or talk, and each instance of its functioning is capable of generating new instances. Reproduction occurs, of course, as people "pick up" its organization from reading it or about it or hearing it used, and using it themselves, hence passing it on to readers or hearers... ([Smith, 1995, p. 26-27](#smi95)).

Once established, ideological codes can replicate anywhere and therefore appear to come from nowhere:

> no one seems to be imposing anything on anybody else. People pick up an ideological code from writing, hearing, or watching, and replicate it in their own talk or writing. They pass it along. Once ideological codes are established, they are self-replicating' ([Smith, 1995: 26](#smi95)).

Regardless of where ideological codes first appear, they come to operate as '_a free-floating form of control in the relations of public discourse_', appearing in multiple forms in multiple texts and organizing talk, thinking, and writing ([Smith, 1995, p. 26](#smi95)).

Smith herself has written about two ideological codes. The Standard North American Family (SNAF) consists of a married man and woman who may be parents to children in their household. She showed ([Smith, 1993](#smi93)) how this code has become a powerful organizing frame for census data collection, scholarly research and policy making that effectively erases or marginalises other household and familial forms. In 1995, Smith showed how the ideological code of political correctness has been detached from its original context of providing voice to politically marginalised individuals and groups, and has come to exemplify a challenge to freedom of speech.

Catherine Richards Solomon ([2008](#sol08)) has identified the academic star as an ideological code that organizes the work of American assistant professors. Achieving star status requires continuous attention to one's scholarly record, demonstrated through texts such as CVs and practices such as grant evaluations. Some of Solomon's participants described accommodating the code by organizing their work to maximise their output. Others refused to accept it, valuing family or other priorities over star status. What is significant, however, is that the ideological code organized their apparently individual choices. Even those who positioned themselves as rejecting the code of the academic star framed their resistance as an individual issue: a personal choice or the natural result of personality characteristics. Solomon argues that,

> instead of seeing this ideological code as externally generated by the ruling relations of academe, most of these professors attributed the pressure to be a star to their individual personalities. That is, they presented themselves as internally driven to be stars, exclusive of any demands placed on them by their profession ([Solomon, 2008, p. 198](#sol08)).

We argue that managerialism functions similarly as an ideological code, organizing our participants' document work and inflecting their talk about that work.

## Managerialism as an ideological code

Critical management studies researchers observe that the rise of managerialism in the 20th century is backed by several unexamined commonsense assumptions, for example, that progress is equivalent to humans' ability to control (and manage) the world around us, and that ordinary people need to be managed by others ([Parker, 2011](#par11)). The generality of the concept of management reflects a claim that this form of knowledge can (and should) be widely applicable across a variety of domains. It is now possible to talk about the management of children, careers and sex lives. In this context, managing something is presented as good in relation to not-managing (or managing poorly), both of which are bad ([Parker, 2011](#par11)). Hancock and Tyler ([2009, p. 6](#han09)) show '_how the values of managerialism—as a cultual ideology—permeate and resonate with a host of socio-cultural forms and lived practices that, to a lesser or greater extent, continue to lie outside the formal domain of the workplace_' (See also [Hancock and Tyler, 2004](#han04); [Larsson and Sanne, 2005](#lar05); [McKenzie and Davies, 2010](#mck10)).

Managerial competency is no longer simply equated with administrative skills but now bears a host of additional expectations. Foremost among these are leadership and entrepreneurialism, traits associated with proactiveness, dynamism and vision ([Hancock and Tyler, 2009, p. 5](#han09)). Hancock and Tyler ([2009, p. 6](#han09)) argue that entrepreneurialism brings with it a process of subjectification through the production of

> entrepreneurs of the self [who] come to see their physical and intellectual selves also as projects; projects oriented towards the maximization of personal opportunities, financial, as well as emotional, spiritual and cultural.

A further set of characteristics associated with managerialism are effectiveness, efficiency and control, and the success of managerial activities is determined through the continuous measurement and evaluation of achievement.

The idea that everyday life is amenable to managerial control brings with it the broadly prevalent notion that everyday life should, or must, be planned, managed and documented. The proper use of documentary tools for information creation (planners, calendars, lists, notes) is presented as essential for achieving success in multiple facets of everyday life as well as in the workplace. Four examples illustrate the pervasiveness and reach of this notion in contemporary North America.

The use of planners as a tool for self-management, efficiency and effectiveness, and self-evaluation is taught by example in many Canadian schools. Companies such as Premier Planners and First Class Planners produce customisable day planners for schools to provide to their students from the very youngest kindergarten children (ages 3-4) to those about to graduate from secondary school (ages 17-18). [Premier Planners' site](http://premier.ca/value-planning) provides a page on the value of planning, including a video of educator testimonials on the importance of students’ knowledge of how to use planners, and the requirement for teachers and parents to teach these skills. The site also advertises [a white paper](http://premier.ca/education-resources/knowledge-corner/white-papers/download-why-plan-white-paper) that provides research evidence on '_the role of planning, organization, and time management in determining students' success in our 21st Century world_'. First Class Planners' depiction of their day planner for a [kindergarten audience](http://www.firstclassplanners.ca/kindergarten-planners.php) illustrates how self-management tools and tips appear throughout the book. A sidebar along the left page provides a definition of goal-setting and a cheery story tells how a caribou walks all the way around the island of Newfoundland by walking '_just a little bit every day_': '_By doing little bits at a time, Colin reached his goal and was very proud_'.

For adults, mass-marketed planning guides address an increasing range of situations. Wedding planning guides treat a wedding less as a life event than as a project to be managed. The bride-to-be is framed as both wanting and needing to stage-manage her big day, generally without much involvement from her future spouse ([McKenzie and Davies, 2010](#mck10)). Entrepreneurialism of the self is evident in these guidebooks' focus on the physical appearance of the bride as well as her organizational prowess. One devoted a chapter to invasive cosmetic procedures such as teeth whitening, laser eye surgery and plastic surgery. The guides demonstrate efficiency and effectiveness as an ideal by including numerous charts and checklists on which the bride-to-be can document her progress towards her goals. These guides instruct the bride as to how she ought to behave, what expectations she must meet and the ways that her own recordkeeping practices contribute to her emerging identity as '_fully competent in the socially organized practices associated with planning a wedding_' ([McKenzie and Davies, 2010, p. 802](#mck10)).

Pregnancy organizers (e.g. [Douglas, 2004](#dou04); [Eisenberg, Murkoff and Hathaway, 1995](#eis95)) present even a physiological occurrence as being amenable to management and planning. Although both wedding and pregnancy planning guides may be turned to later as keepsakes ([McKenzie and Davies, 2013](#mck13)), the instructions for their use clearly indicate that this function is secondary to the tool's managerial purpose:

> This section of the organizer helps you gather up all the supplies you need to have on hand after you deliver your baby…. You can keep track of any stores where you and your baby are registered; you're also reminded of who gave you those lovely shower gifts. And when it comes time to start interviewing potential pediatricians, this section will prove to be a real lifesaver, offering both a checklist of questions to ask and spaces for the prospective pediatrician's responses ([Douglas, 2004, p. 4](#dou04)).

In addition, the guides provide spaces for the pregnant woman to make notes about visits with her care providers and to keep track of symptoms and questions to discuss at her next visit, as well as to track her food intake and evaluate her success at adhering to dietary recommendations. A pregnancy and birth, like a wedding, is therefore presented as a personal project that will be improved through active planning and recordkeeping; in this case, to ensure that medical and nutritional guidelines are adhered to, adverse symptoms are appropriately reported, gift-givers are thanked and responsible health decisions are made.

Finally, a Canadian newspaper story presents the use of a planner as evidence of the self-management needed to gain and maintain physical and mental health and good social functioning. The story profiles the experience of a client of an alternative sentencing programme for people with drug addictions who have been convicted of crimes. The client had not used a planner since being a young school child but was given one at the start of his drug treatment sentence. '_For people in drug court, getting a planner means they have to plan. Getting organized means organizing their mind. It means getting a measure of control over a life that was out of control_' ([Richmond, 2013](#ric13)). The planner itself, along with the effective use of it, was presented by the journalist as a necessary component of the self-management and self-surveillance required to recover from addiction and escape a life of crime.

As Hancock and Tyler ([2009](#han09)) have noted, the principles of managerialism are understood to be necessary for even the most intimate areas of everyday life. These four examples show how the use of managerial strategies such as keeping good records and being organized are taken as necessary prerequisites for success at growing up to be a functional adult, getting married, giving birth, and becoming and staying healthy, sober and on the right side of the law. The range and ubiquity of these representations suggests that the notion and practice of keeping track are intimately tied to managerialism. They further indicate that managerialism is being produced and reproduced as an ideological code, '_order[ing] and organiz[ing] texts across discursive sites, concerting discourse focused on different topics and sites, often having divergent audiences, and variously hooked into policy or political practice_' ([Smith, 1993, p. 51](#smi93)).

In this paper we describe the ways this ideological code organizes the talk, thinking and document creation practices of our research participants. We will show how self-management imperatives are played out in the information creation work of keeping track and how the ideological code of managerialism is contained in people's talk about that work. We will thereby contribute a contextual understanding of personal information management and build on Trace's work on information creation, which shows how familiarity with embedded social norms and realities around specific genres of information allows people to '_function within the rules but also outside them_' ([Trace, 2008, p. 1541](#tra08)).

## Our study

Data for this paper come from the first ten participants in an ongoing Canadian study of keeping track. We selected these participants (eight women and two men, from two provinces) purposively to represent a wide variety of household situations (living alone, with partner and/or children, in a university residence with other students), work characteristics (home-based businesses, work that requires travel, shiftwork), and the multiple roles they occupied in their paid and unpaid work (elite athlete, hairdresser, editor, labour activist, musician, producer, salesperson, student, teacher, writer, volunteer, manager of a chronic illness, full-time caregiver).

Trace ([2007](#tra07)) has shown that the characteristics of information creation pose particular challenges for data collection. First, because the work may be both idiosyncratic and invisible to the people who do it, it is necessary to observe the physical environment to identify relevant documents and artefacts. Conversely, because tools may be multiple and physically distributed, and because interpersonal sources may be used in addition to artefacts, researchers must be prepared to go beyond what is immediately visible and ask questions to elicit accounts from participants. Similarly, discovering social norms can be challenging. For this reason, it is beneficial to ask questions that address what is unacceptable and to look for the cues that members provide within social situations, including the situation of a research interview.

We therefore used a combination of in-depth interviews and observation. We asked participants to show us, as well as tell us, how they keep track in their everyday life ([McKenzie _et al._, 2010](#mckw10)). Interviews averaged 61 minutes in length and ranged from 41 to 123 minutes. We also observed and photographed physical household spaces and the documents occupying them ([Hartel and Thomson, 2011](#har11)). Data collection and analysis conform to ethical guidelines on research on human subjects of the Social Sciences and Humanities Research Council of Canada ([Canadian..., 2010](#can10)). To maintain confidentiality, we identify participants by a generic initial and do not associate demographic characteristics with any individual participant. Our data set consists of over fourteen hours of interviews, 330 photographs, and 500 pages of interview transcripts.

Data analysis for this paper has been qualitative. We have used the techniques of constant comparison ([Strauss and Corbin, 1990](#str90)) and Hartel's ([Hartel and Thomson, 2011](#har11)) techniques for analysing photographs within an abductive framework of relational and discourse analysis. An abductive strategy allows the researcher to demonstrate how a particular instance is connected to a broader set of discourses and relations. We undertook this strategy in two ways. Relational analysis allows the researcher to explore how the work done in individual households hooks in to the organizational requirements and contingencies of workplaces, schools, and other institutions and infrastructures ([Campbell and Gregor, 2002](#cam02)), including the taking up of ideological codes that benefit institutions. Discourse analysis ([Potter, 1996](#pot96)) shows how individual participant’s talk about their document work relates to their presentation of themselves as good workers, parents, students, etc. ([Hancock and Tyler, 2004](#han04)). Neither relational nor discourse analysis considers participants’ actions and accounts to be windows into their cognitive processes or subjective states. Relational analysis sees them as clues to the ways that these activities are linked to activities, discourses and ideological codes that may originate elsewhere. Discourse analysis ([Potter, 1996](#pot96)) considers the discursive building blocks participants use to put together an account and the rhetorical purposes to which that account is put (e.g. justifying, explaining). Intercoder reliability is not a feature of either form of analysis. For this paper we have chosen examples from these analyses that best illustrated the themes we identified.

## Findings

We present examples from our data to show how the managerial principles of effectiveness and efficiency, and of entrepreneurialism of the self, shape our participants' information creation work related to keeping track and inflect their talk about that work. The transcription conventions used in the extracts reported below are:  
I:      Conversational turns are prefaced by an initial identifying the speaker (Participant, Interviewer), and a colon.  
//      Marks overlapping talk.  
[ ]     Nonverbal elements such as laughter, physical gestures, changes in tone, or to indicate the removal of identifying details or the editing of the excerpt.  
? !    Punctuation indicates both grammatical sentence-ends and emphatic or interrogative intonation.

### Effectiveness and efficiency

In their interviews, our participants provided evidence that they attended to both efficiency and effectiveness when doing and evaluating their work of information creation. A primary performance measure participants used to evaluate a system for keeping track is how well or how much was accomplished:

Example 1:

> I: Do you feel that you, um, are able to do more things or to do things **better** because you're organized than if you weren't organized?  
> P: Yeah. I can […] I can squish more things into a day […] Just because I know that I'm going to do **this** at this time, **this** at this time, **this** at this time. […] I plan **ahead** of time so that… I know "Ok, **so**. I'm gonna get this done, this done, this done, this done". And I have the **list** of what I'm going to **do**, and I'll make sure that I get **that** done before the day's over.

Two strategies participants used to achieve efficiency and effectiveness in keeping track were to use particular inscription techniques and to place tools for keeping track in places where they would be seen and attended to by themselves and others in their households.

#### Inscription

One participant explained to us an inscription strategy for indicating which items on lists should be accorded highest priority:

Example 2a:

> _P: Things that are more important I put a big star next to it. Sometimes the list is entirely stars_.

Example 2b:

> I: So tell me about your grocery list.  
> P: Such as it is. God, how embarrassing is this going to be […] So there, that's one that I drew up last week. And as you can see, it's sort of a mishmash of… And star is cat food…  
> I: Oh.  
> P: …which is, once again.  
> I: Urgent.

<figure>

![Note with stars](../p614fig1.png)

<figcaption>Figure 1: Photograph of the list under discussion.</figcaption>

</figure>

Other participants also used inscription in ways that made sense to them and to others who needed to make use of the list or calendar. A participant who operated a small home-based business explained to us why she only writes the start and not the end times of her consultations with clients onto the family calendar, even though her partner must arrange to be available to provide childcare during those consultation times.

Example 3:

> I: So you've been presumably doing this long enough that you know that if somebody comes in for [particular kind of client consultation], you know how long it's likely to take and you can kind of plan that?  
> P: Yep. I just… you get so used to doing it. I mean, I used to work in [same business in an institutional setting] for years, and you get used to having to block off your time, so now it's just in my head and I don't worry about it. I think… I don't even have to ask [partner], but I think he's kind of gotten used to the idea like a [kind of interaction] takes me roughly half-an-hour, [another kind] is roughly two hours, so I think he kind of…  
> I: Yeah… so you've just got the start times here…  
> P: Yeah. I think he's just gotten used to it. It never really occurred to me to write the end times on here… I think he's… he's never asked me.  
> I: Well you don't need them, clearly. You know how long it's gonna take.  
> P: Yeah, he must have gotten used to it.

By using this tool together over time, the couple was able to develop more efficient inscription practices while maintaining the effectiveness of their household information creation and management system for coordinating their work and childcare responsibilities. The participant now therefore creates the individual calendar entries with a view to future information sharing with her partner.

### Making use of contextual locations

Research on home-based organizational systems (e.g. [Crabtree, 2003](#cra03)), has shown that individuals and families place artefacts in contextual locations that exploit family routines to ensure that the right person sees the right artifact at the right time ([Eliot _et al._, 2007](#eli07)). Our participants showed and told us of their efforts to maximise efficiency and effectiveness by taking advantage of contextual locations. Several households we observed made use of self-adhesive sticky notes and affixed them to other documents such as a page in a planner and also to surfaces in the household such as countertops and refrigerator doors.

Example 4:

> P: We live and die by the sticky.  
> I: Okay.  
> P: Umm, we have stickies everywhere.

</section>

<figure>

![The sticky note](../p614fig2.png)

<figcaption>Figure 2: Use of sticky notes in two households.</figcaption>

</figure>

<section>

In Figure 2, the first image is of a note that a participant stuck to the bathroom mirror as a reminder to her partner to get blood tests done. The tests required a several-hour fast so she placed the note in a location where he would see it first thing in the morning, before he went to the kitchen to get something to eat or drink. The second sticky note was affixed to the letter of information for our study and was set in the participant's home office.

Wall and desk calendars and planning books were also evident in many participants' homes (Figure 3). Calendars designed to be used by the entire household were placed centrally, often in kitchens, where all household members could see them.

</section>

<figure>

![Participants' calendars](../p614fig3.png)

<figcaption>Figure 3: Participants' calendars</figcaption>

</figure>

<section>

One participant had created a calendar in the form of small chalk boards (Figure 4), one for each day of the week, which she placed in a prominent location on a kitchen wall.

<figure>

![Chalkboard calendars](../p614fig4.png)

<figcaption>Figure 4: Chalkboard calendars</figcaption>

</figure>

Goals of efficiency and effectiveness are evident in her description of the thinking behind that tool and its location:

Example 5:

> _I: What made you decide to start those boards, and when did you start… first put them up?  
> P: Um, we have been in this house probably four-and-a-half years, and it's been there at least three, if not longer. I knew I had to do something because before we just had your regular, you know, little hanging calendars where I was trying to write, you know, different… "okay, dinner with this person and blah blahblah" the different stuff we had to do, and nobody ever looked at it… nobody ever looked it and so I knew I needed something bigger._

This centrally located, large, and visually striking tool was more effective for keeping track within the household as a whole than its smaller and less visible predecessors had been.

### Entrepreneurialism of the self

Participants' practices of information creation and their discussions with us of it also reflected attention to entrepreneurialism of the self. All participants talked about _being organized_ as a positive state, demonstrated by their effectiveness at keeping track.

Example 6:

> P: Like, unorganized for me feels really bad. organized feels good.  
> I: Okay. All right.  
> P: Makes me happy.

Our participants described being organized as an individual characteristic, a trait innately associated with their personalities or a way of being familiar since childhood, learned or perhaps inherited from other family members. Two participants provided quite similar descriptions of list-making in their childhoods.

Example 7:

> P: I like to makes lists. I like the satisfaction of crossing something out.  
> I: Okay, so you're a crosser offer.  
> P: So that might be part of the motivation for the system.  
> I: So, it's a reward, essentially.  
> P: There's absolute… there's satisfaction in it. To go "it is done".  
> I: Yes, I think that's a large reason why people keep those wonderful lists.  
> P: Yes. It just, it feels good.  
> I: Did you… have you always been a list-maker then?  
> P: I think so.  
> I: Even like in high school?  
> P: Oh… probably not to the extent I am now. […] But, I… but I think so. I think so.  
> I: But, were like your parents….  
> P: My mother makes lists.  
> I: …listmakers?  
> P: She makes lists. And she… we have talked about the fact that she likes to make lists also. Or that she relies on her lists.

Example 8:

> P: I've been a list maker forever. Probably started when I was in school, and my mother was a list maker, too.  
> I: Okay, so how did you learn to be a list maker?  
> P: I don't know. My mother had, well, my mother had a grocery list that she kept in the kitchen. That was certainly… And she would go out, or my dad would go out, whoever was buying groceries, and they had **the list**.

Treating being organized as a personal characteristic and the practice of list-making as inherently satisfying or innate meant that participants took personal and individual credit for their success and expressed satisfaction when they were able to describe themselves as living up to a perceived managerial standard:

Example 9:

> P: When I was in school, I always did a lot of stuff -- school work, you know, regular school work you had to do, plus I always had that extra-curricular stuff: [Student Council] President when I was in [one town], [Student Council] Treasurer when I was in [another town]. When I went to university, I was on the council, and then I was [student council executive role] one year, and I was a fulltime student at the same time, so you just had to be organized. And […] I had jobs when I was younger that there was responsibility involved with organization. And if my job wasn't done then it affected other people and I never wanted **that**. Someone not to be able to have enjoyment or not to be able to do their job because mine wasn't done. And that just carried over to now. I make sure the bills are paid on time. I think it's that — [partner] calls it the Protestant upbringing or something like that you know. You get up early, gosh, if you're not in bed early, then your day is half done. And the thought of never paying a bill on time and all this stuff, so I just — So I know when all my credit cards are due, I know when I pay them.

Participants conversely assumed personal blame for their perceived failure to achieve what they perceived to be an adequate degree of being organized, in the same way that Solomon's ([2008](#sol08)) participants took personal responsibility for achieving or failing to achieve academic star status.

One participant described the way that the week's household events were recorded on an online calendar. Over the course of the conversation, it transpired that she had experienced serious health difficulties that week and that nothing listed on the calendar, apart from the interview itself, actually took place as recorded. The participant presented this mismatch between the contents of a calendar and the actual events that took place as an inadequacy in her recordkeeping and organizing processes and therefore as evidence of a personal shortcoming.

Example 10:

> P: And then I will use scraps of paper too. I have one here from before you came and I threw it out [opens drawer] which I should keep out because it still has stuff that has to be done.  
> I: You haven't done it but you threw out the little piece of paper?  
> P: Yes I just threw it out to make myself feel good I guess. Yes.  
> I: Oh my goodness. So, is that a… it's not a sticky note, it's just a scrap?  
> P: No, it's just a piece of scrap paper.  
> I: Okay.  
> P: And then, so that eventually should go on — See really, it should go on here.  
> I: Onto your calendar // on // your computer.  
> P: // Yeah //  
> I: Okay.  
> P: Sometimes I'm hopeful. I think "Well, I'll do it right away it doesn't have to be on the list" but… This is really making me feel like a failure.

Rather than acknowledging the challenges inherent in coordinating multiple recordkeeping systems (scraps of paper and calendar) or that serious illness might legitimately curtail both a person's activities and her recording of them, this participant presented herself as a managerial failure. The promise of becoming/being organized both shaped what this participant and this household did and how the participant felt about and described herself. Treating being organized as an individual characteristic therefore required constant work, constant vigilance and constant assessment to manage and strengthen that characteristic in themselves.

<figure>![Motivational notes](p614fig5.png)

<figcaption>Figure 5: Motivational notes.</figcaption>

</figure>

The motivational notes shown in Figure 5, which two participants posted in prominent places for themselves combine emotional and spiritual support with managerial tips to help them achieve success, e.g.,

> [left image] _If you do not die first you will have time to do it. If you die before it is done, you don't need to do it. Anthony Bloom, Beginning to Pray._  
>   
> [right image] _The minute you quit is the moment you fail. Success is a destination, not a journey. Act as if it were impossible to fail and it will be._

As did Trace's ([2007](#tra07)) students, our participants gave us examples that tied their identity as effective, organized people as contingent on learning and applying the norms of managerialism, even when those norms were not explicitly articulated.

## Discussion

We have shown that managerialism functions as an ideological code, a powerful organizing frame that shapes our participants' practices of and talk about keeping track. We have also shown that this code frames everyday life as something to be managed, as though it were institutional rather than intimate. Managerialism contributed to our participants' stock of knowledge about everyday life and thereby inflected their practices of information creation. Participants used project language, time management techniques and business tools such as day planners to coordinate their everyday lives. They aspired to an ideal of being organized and evaluated their own performance in terms of notions of efficiency and effectiveness. As a result, they took real pleasure when their practices of information creation aligned with the tenets of managerialism. Conversely, they attributed perceived failings in their household management performance to individual characteristics. In Excerpt 2 a general invitation ('_So tell me about your grocery list_') elicited a response ('_Such as it is. God, how embarrassing is this going to be_') that demonstrates this participant's awareness that there may be standards by which such things as shopping lists may be judged and that her prowess as an effective list-maker may be found wanting.

The findings of this study are limited by the small sample size. It is important to note that even within this sample, managerialism was not always taken up in the same way and was sometimes actively resisted. A single participant might produce both descriptions of competence, perhaps in one area of life, and descriptions of failure. Whereas several participants tended toward describing themselves more in negative terms, two participants stood out as describing their work of keeping track in mostly positive terms, and many described individual incidents of success. As Solomon ([2008](#sol08)) notes, however, even these successes referenced the ideological code: participants presented themselves as successful precisely because they had internalised and were able to meet the requirements of managerialism.

It is also important to note that, although managerialism shaped both participants' practices and their talk about those practices, it was not the only shaper. As Trace ([2007](#tra07)) noted, information creation in a classroom served multiple functions related to both an explicit and a hidden curriculum. For our participants, the tools they created for keeping track did multiple kinds of work. For example, the sticky note depicted on the left side of Figure 2 featured a frowning face, and did relational work by demonstrating sympathy for the intended recipient. The meaning of any one of a list or calendar entry also had the potential to evolve over time as participants read and reread the information they created. Texts written with instrumental goals, like a calendar documenting when and where to get on an airplane, were transformed over time into emotionally rich stories of one's own past and the past of one's nearest and dearest ([McKenzie and Davies, 2013](#mck13)).

Using the ideological code as an analytic lens has allowed us to understand '_how and why people are socialized to create information in various contexts_' ([Trace, 2007: 142](#tra07)). As Trace did, we have attended to the interactional particularities and locally-constituted context of our participants' households. Doing so has shown us how information creation for the purposes of keeping track followed managerial genres and conventions, and how knowledge of information creation and management techniques was learned and evaluated with respect to managerial norms and standards. As Trace found, our participants framed their identity as _good_ (organized) people as contingent on learning and successfully applying these norms.

By studying the ongoing development of the local setting, we have built on Trace's work on information creation and on the work on personal information management, to show how that setting, and the practices of people within it, are shaped and constrained by the broader context.

## Acknowledgements

The first author would like to acknowledge the support of an Insight Grant from the Social Sciences and Humanities Research Council of Canada. This paper is based on a presentation at the Information: Interactions and Impact (i<sup>3</sup>) Conference. This biennial international conference is organised by the Department of Information Management and Research Institute for Management, Governance and Society (IMaGeS) at Robert Gordon University. i<sup>3</sup> 2013 was held at Robert Gordon University, Aberdeen, UK on 25-28 June 2013\. Further details are available at http://www.i3conference.org.uk/

## <a id="authors"></a>About the authors

**Pam McKenzie** is an Associate Professor in the Faculty of Information and Media Studies at The University of Western Ontario, London, Ontario, Canada. She can be contacted at: [pam.mckenzie@uwo.ca](mailto:pam.mckenzie@uwo.ca)  
**Elisabeth Davies** is a Lecturer in the Faculty of Information and Media Studies at The University of Western Ontario, London, Ontario, Canada. She can be contacted at: [edavies3@uwo.ca](mailto:edavies3@uwo.ca)  
**Sherilyn Williams** is a doctoral student in the Faculty of Information and Media Studies at The University of Western Ontario, London, Ontario, Canada. Her contact address is: [swilli72@uwo.ca](mailto:swilli72@uwo.ca)

</section>

<section>

<ul>
<li id="adl07">Adler, P. S., Forbes, L.C. &amp; Wilmott, H. (2007). Critical management studies. <em>The Academy of Management Annals, 1</em>(1), 119-179.</li>

<li id="alv92">Alvesson, M. &amp; Willmott, H. (1992). <em>Critical management studies</em>. London: Sage Publications.</li>

<li id="bru11">Bruce, H. W., Wenning, A., Jones, E., Vinson, J. &amp; Jones, W. (2011). <a href="http://www.webcitation.org/6NPdKjLav">Seeking an ideal solution to the management of personal collections</a>. <em>Information Research, 16</em>(1), paper 462. Retrieved from http://InformationR.net/ir/16-1/paper462.html (Archived by WebCite® at http://www.webcitation.org/6NPdKjLav)</li>

<li id="cam02">Campbell, M. &amp; Gregor, F. (2002). <em>Mapping social relations: a primer in doing institutional ethnography</em>. Toronto, ON: Garamond.</li>

<li id="can10">Canadian Institutes of Health Research, Natural Sciences and Engineering Research Council of Canada, &amp; Social Sciences and Humanities Research Council of Canada. (2010). <em>Tri-council policy statement: ethical conduct for research involving humans</em>. Ottawa, ON: Public Works and Government Services Canada.</li>

<li id="cra03">Crabtree, A. (2003). <em>The social organization of communication in the home</em>. Paper presented at the 8th Conference of the International Institute of Ethnomethodology
and Conversation Analysis, August 6-9, Manchester, England.</li>

<li id="dal02">Daly, K. J. (2002). Time, gender, and the negotiation of family schedules. <em>Symbolic Interaction, 25</em>(3), 323-342.</li>

<li id="dar07">Darrah, C. N., Freeman, J. M. &amp; English-Lueck, J. A. (2007). <em>Busier than ever: why American families can't slow down</em>. Stanford, CA: Stanford University Press.</li>

<li id="dev03">DeVault, M. L. (2003). Families and children: together, apart. <em>American Behavioral Scientist, 46</em>(10), 1296-1305.</li>

<li id="dev08">DeVault, M. L. (Ed.). (2008). <em>People at work: life, power and social inclusion in the new economy</em>. New York, NY: New York University Press.</li>

<li id="dou04">Douglas, A. (2004). <em>The mother of all pregnancy organizers: the ultimate guide to your next nine   months</em>. Hoboken, NJ: Wiley.</li>

<li id="eis95">Eisenberg, A., Murkoff, H. E. &amp; Hathaway, S. E. (1995). <em>The what to expect when you're expecting pregnancy organizer</em>. New York, NY: Workman.</li>

<li id="eli07">Eliot, K., Neustaedter, C. &amp; Greenberg, S. (2007). StickySpots: using location to embed technology in the social practices of the home. Tangible and embedded interaction archive. In <em>Proceedings of the 1st International Conference on Tangible and Embedded Interaction</em>, (pp. 79-86). New York, NY: ACM Press.</li>

<li id="gri05">Griffith, A. I. &amp; Smith, D. E. (2005). <em>Mothering for schooling</em>. London: Routledge.</li>

<li id="han04">Hancock, P. &amp; Tyler, M. (2004). MOT your life: critical management studies and the management of everyday life. <em>Human Relations, 57</em>(5), 619-645.</li>

<li id="han09">Hancock, P. &amp; Tyler, M. (Eds.). (2009). <em>The management of everyday life</em>. New York, NY: Palgrave Macmillan.</li>

<li id="har11">Hartel, J. &amp; Thomson, L. (2011). Visual approaches and photography for the study of immediate information space. <em>Journal of the American Society for Information Science and Technology, 62</em>(11), 2214-2224.</li>

<li id="hoc03">Hochschild, A. R. &amp; Machung, A. (2003). <em>The second shift: working parents and the revolution at home</em>. (2nd. ed.). New York, NY: Penguin Books.</li>

<li id="jon07">Jones, W. &amp; Teevan, J. (2007). <em>Personal information management</em>. Seattle, WA: University of Washington Press.</li>

<li id="lar05">Larsson, J. &amp; Sanne, C. (2005). Self-help books on avoiding time shortage. <em>Time &amp; Society, 14</em>(2/3), 213-230.</li>

<li id="mar07">Marshall, C. C. (2007). How people manage personal information over a lifetime. In W. Jones &amp; J. T. Teevan, <em>Personal information management</em> (pp. 57-75). Seattle, WA: University of Washington Press.</li>

<li id="mck10">McKenzie, P. J. &amp; Davies, E. (2010). Documentary tools in everyday life: the wedding planner. <em>Journal of Documentation, 66</em>(6), 788-806.</li>

<li id="mck12">McKenzie, P. J. &amp; Davies, E. (2012). Genre systems and &quot;keeping track&quot; in everyday life. <em>Archival Science, 12</em>(4), 437-460.</li>

<li id="mck13">McKenzie, P.J. &amp; Davies, E. (2013). <em>The once and future self: (re)reading personal lists, notes, and calendars</em>. Paper presented at the Researching the Reading
Experience, Oslo and Akershus University College of Applied Sciences (OAUC), Oslo, Norway, June 11-12, 2013. </li>

<li id="mckw10">McKenzie, P. J., Davies, E. &amp; Wong L. (2010). <a href="http://www.webcitation.org/6Pw3xLtRw"><em>Methodological strategies for studying documentary planning work</em></a>. Paper presented at the Canadian Association for Information Science conference, Montreal, Quebec, June 2-4, 2010. Retrieved from  http://www.cais-acsi.ca/proceedings/2010/CAIS006_McKenzie_Final.pdf (Archived by WebCite® at http://www.webcitation.org/6Pw3xLtRw)</li>

<li id="par11">Parker, M. (2011). Managerialism. In M. Tadajewski, P. MacLaran, E. Parsons &amp; M. Parker, <em>Key concepts in critical management studies</em> (pp. 155-159). Los Angeles,
CA: Sage.</li>

<li id="pot96">Potter, J. (1996). <em>Representing reality: discourse, rhetoric and social construction</em>. Thousand Oaks, CA: Sage.</li>

<li id="ric13">Richmond, R. (2013, March 22). <a href="http://www.webcitation.org/6NPeGBFzl">London. one of a handful of Ontario cities with a drug treatment court</a>. <em>The London Free Press</em>. Retrieved from http://www.lfpress.com/2013/03/22/london-one-of-a-handful-of-ontario-cities-with-drug-treatment-court (Archived by WebCite® at http://www.webcitation.org/6NPeGBFzl)</li>

<li id="smi93">Smith, D. E. (1993). The Standard North American Family: SNAF as an ideological code. <em>Journal of Family Issues, 14</em>(1), 50-65.</li>

<li id="smi95">Smith, D. E. (1995). &quot;Politically correct&quot;: an ideological code. In S. Richer and L. Weir (Eds.), <em>Beyond political correctness: toward the inclusive university</em> (pp. 23-50). Toronto ON: University of Toronto Press.</li>

<li id="sol08">Solomon, C. R. (2008). Personal responsibility in professional work: the academic "star" as ideological code. In M. L. DeVault (Ed.), <em>People at work: life, power and social
inclusion in the new economy</em> (pp. 180-202). New York, NY: New York University Press.</li>

<li id="str90">Strauss, A. &amp; Corbin, J. (1990). <em>Basics of qualitative research: grounded theory procedures and techniques</em>. Newbury Park, CA: Sage.</li>

<li id="tra07">Trace, C. B. (2007). Information creation and the notion of membership. <em>Journal of Documentation, 63</em>(1), 142-164. </li>

<li id="tra08">Trace, C. B. (2008). Resistance and the underlife: informal written literacies and their relationship to human information behavior. <em>Journal of the American Society for   Information Science and Technology, 59</em>(10), 1540-1554.</li>

<li id="whi11">Whittaker, S. (2011). Personal information management: from consumption to curation. <em>Annual Review of Information Science and Technology, 45</em>(1), 1-62.</li>

<li id="zim02">Zimmerman, T. S., Haddock, S. A., Ziemba, S. &amp; Rust, A. (2002). Family organizational labor: who's calling the plays? <em>Journal of Feminist Family Therapy, 13</em>(2/3),
65-90.</li>
</ul>

</section>

</article>