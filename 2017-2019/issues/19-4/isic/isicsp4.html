<!DOCTYPE html>
<html lang="en">

<head>
	<title>Contextualising information practices and personal information management in mobile work</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="dcterms.title"
		content="Contextualising information practices and personal information management in mobile work">
	<meta name="author" content="Leslie Thomson, Mohammad Jarrahi">
	<meta name="description" content="People living in rural areas may face specific
 barriers to finding useful health-related information, and their use of
 information may differ from their urban counterparts.  This random-digit dial telephone survey of 253 people
 (75% female) living in a rural, medically under-serviced area of Ontario,
 Canada, follows-up a previous interview study to examine with a larger sample
 issues related to searching for and using health information.  Descriptive statistics were used to describe the
 sample and the distribution of responses to each question.  Sex differences on
 key questions were analysed using the Chi-squared test.  Respondents were as likely to find information on the
 Internet as from doctors, although several reported that they had no access to
 these resources. Many of those surveyed used the information they found to look
 after themselves or someone else, to decide whether to seek assistance from a
 professional health care provider, and/or to make treatment decisions. Echoing
 results from other studies, a significant proportion of women reported that they
 did not discuss with a doctor the information they found.  These findings have implications for Canadian
 government health policy, particularly the use of e-health strategies.">
	<meta name="keywords" content="mobile work, information management, personal, information behaviour,">
	<meta name="robots" content="all">
	<meta name="dcterms.publisher" content="Professor T.D. Wilson">
	<meta name="dcterms.type" content="text">
	<meta name="dcterms.identifier" content="ISSN-1368-1613">
	<meta name="dcterms.identifier" content="http://InformationR.net/ir/19-4/isic/isicsp4.html">
	<meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/19-4/infres194.html">
	<meta name="dcterms.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dcterms.issued" content="2014-12-15">
	<meta name="geo.placename" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<header>
		<h4 id="vol-19-no-4-december-2014">vol. 19 no. 4, December, 2014</h4>
	</header>
	<article>
		<h1 id="contextualising-information-practices-and-personal-information-management-in-mobile-work">
			Contextualising information practices and personal information management in mobile work</h1>
		<h4 id="leslie-thomson-and-mohammad-jarrahi">Leslie Thomson and Mohammad Jarrahi,<br>
			School of Information and Library Science, University of North Carolina, Chapel Hill, NC 27599, USA</h4>
		<h4 id="abstract">Abstract</h4>
		<blockquote>
			<p><strong>Introduction.</strong> People living in rural areas may face specific barriers to finding useful
				health-related information, and their use of information may differ from their urban counterparts.<br>
				<strong>Method.</strong> This random-digit dial telephone survey of 253 people (75% female) living in a
				rural, medically under-serviced area of Ontario, Canada, follows-up a previous interview study to
				examine with a larger sample issues related to searching for and using health information.<br>
				<strong>Analysis.</strong> Descriptive statistics were used to describe the sample and the distribution
				of responses to each question. Sex differences on key questions were analysed using the Chi-squared
				test.<br>
				<strong>Results.</strong> Respondents were as likely to find information on the Internet as from
				doctors, although several reported that they had no access to these resources. Many of those surveyed
				used the information they found to look after themselves or someone else, to decide whether to seek
				assistance from a professional health care provider, and/or to make treatment decisions. Echoing results
				from other studies, a significant proportion of women reported that they did not discuss with a doctor
				the information they found.<br>
				<strong>Conclusions.</strong> These findings have implications for Canadian government health policy,
				particularly the use of e-health strategies.</p>
		</blockquote>
		<p>Traditional, centralised offices and workspaces have provided a common setting for information practices
			research (<a href="#Cou07">Courtright, 2007</a>; <a href="#Har94">Harris and Dewdney, 1994</a>). One
			specific line of information practices inquiry concerns personal information management, individuals' daily
			activities of '<em>acquiring, organising, maintaining, retrieving, using, and controlling the distribution
				of information items</em>' (<a href="#Jon07">Jones and Teevan, 2007</a>), both physical and digital,
			that they encounter. As personal information management is equally applicable to professional and
			non-professional instances, it might be considered an apt lens for studying mobile work; individuals who
			perform mobile work navigate various boundaries-spatial, temporal, social, informational, and otherwise
			(e.g., <a href="#Kak01">Kakihara and Sorenson, 2001</a>; <a href="#Urr07">Urry, 2007</a>)-so to access, use,
			and store their professional content successfully, and also plan and corral artefacts in order to enable
			their work in and across various settings (<a href="#Per01">Perry, O'Hara, Sellen, Harper, and Brown,
				2001</a>) and configurations (<a href="#Ols14">Olson and Olson, 2014</a>; <a href="#Sor11">Sorensen,
				2011</a>). (Here, the term mobile work is used to denote the professional work tasks of individuals who
			are required or enabled to transit more often than episodically between disparate locations-offices, homes,
			and coffee shops, for example.) Further, mobile work not only depends upon informational considerations like
			these, but is often itself inherently '<em>informational</em>' and knowledge-based (<a href="#Dav02">Davis,
				2002</a>).</p>
		<p>Yet, despite this seeming congruence between the aims of information practices scholarship and the realities
			of mobile work, there is little discussion of the intricacies of information practices and personal
			information management activities in the context of mobile work. This short paper briefly reviews the
			largely socio-technological scope of mobile work research thus far, points to three possible dimensions of a
			more '<em>informationally holistic</em>' view of mobile work-accounting for various interactions beyond the
			technological-and describes a project now underway that is exploring mobile workers' information practices
			and their management activities. What shapes do mobile workers' information practices and activities take?
		</p>
		<p>To date, information-related discussions of mobile work have been approached mainly from technological
			angles, often in the subfields of human-computer interaction and computer-supported collaborative work.
			Recently, Ciolfi and Pinatti de Carvalho (<a href="#Cio14">2014</a>) and Erickson, Jarrahi, Thomson, and
			Sawyer (<a href="#Eri14">2014</a>) have stated that studies of mobile work ought to account for work
			practices, mobility issues, technological concerns, and work-life boundaries as interrelated parts of mobile
			workers' day-to-day realities. Even inclusive of these four elements, however, research will still risk an
			incomplete, potentially abstracted, picture of mobile workers' experiences, presuming one is interested in
			gleaning more holistically informational insights of mobile work, beyond the technological. Three potential
			(equally interrelated) avenues by which information practices approaches could illuminate the growing body
			of mobile work research are outlined below; these suggestions represent only three possible dimensions of
			many within this particular context and should be altered and extended as research in this vein progresses.
		</p>
		<p>First, as existing mobile work research and theorization remains relatively nascent (<a href="#Pic03">Pica
				and Kakihara, 2003</a>), individual workers are not usually considered beyond their immediate work
			situations. Yet no one enters the professional realm an empty vessel, without the same sorts of
			'<em>personal information infrastructure</em>' that Marchionini (<a href="#Mar95">1995</a>) notes affect
			information seeking; mental models, past experiences, and specific abilities are brought to and continually
			shaped through information encounters. Information practices and management preferences and tendencies
			manifest as personality traits (<a href="#Mas14">Massey, TenBrook, Tatum, and Whittaker, 2014</a>) ,
			'<em>styles</em>' (<a href="#Ber93">Berlin, Jeffries, O'Day, Paepcke, and Wharton, 1993</a>), and dynamics
			(<a href="#Hea00">Heath, Knoblauch, and Luff, 2000</a>) that come to bear in social and collaborative
			situations, common features of mobile work. Taken up by information researchers as group information
			management issues, these ideas may serve as a springboard for further investigation of the virtual settings
			of mobile work, or may even find application to considerations of the physical settings of mobile work.</p>
		<p>Second, mobile work research has yet to probe the information activities and strategies of individual mobile
			workers. Instead, information-related discussions occur at higher levels, with a technological purview, not
			often delving into the roles of non-digital artefacts over the long-term or across different settings and
			situations (independent, collaborative, and collocated work, for example). Similarly, individual mobile
			workers' '<em>meta-level</em>' (<a href="#Jon08">Jones, 2008</a>) activities, such as information
			organisation, storage, and discarding, across devices and formats, remain to be examined.</p>
		<p>Lastly, mobile workers encounter and use information across multiple physical locations, and while some
			existing mobile work research accounts for setting (e.g., <a href="#Bro03">Brown and O'Hara, 2003</a>; <a
				href="#Lie14">Liegl, 2014</a>), such discussions are not often detailed in their attention to
			sociospatial contexts, so key to information practices investigations (see <a href="#Mer12">Mervyn and
				Allen, 2012</a>, for a review of such literature). Liegl (<a href="#Lie14">2014</a>) found that settings
			influence work processes like creativity, and Spinuzzi (<a href="#Spi12">2012</a>) showed that different
			individuals perceive of the same setting differently, raising questions of whether and how individuals'
			information practices and activities are nurtured or confined, implicitly or explicitly, depending on the
			'<em>psychological and social ecology</em>' (<a href="#Mar95">Marchionini, 1995</a>) that different
			locations engender. A mobile worker interacts with settings in ways that extend beyond logistics of
			resources and technologies, and '<em>informationally holistic</em>' studies could account for this.</p>
		<p>This short paper has identified three potential crossroads between information practices scholarship and the
			context of mobile work, which are currently being investigated, altered, and refined in an exploratory pilot
			project that involves approximately 35-50 mobile workers in the United States. As mobile work itself and
			mobile work research are relatively new phenomena, this pilot study is gathering data from individuals
			across both diverse professions (consultancy, design, and academia, for example) and locations. At present,
			semi-structured interviews are being conducted that centre on-among other issues-mobile workers'
			professional needs; their salient information tools and artefacts; their information formats, upkeep,
			organisation, and storage; as well as their workspaces and their information activities therein. Interviews
			are expected to conclude in June, and a subset of this data (from 15-20 interviews in one state) will be
			analyzed thereafter, affording findings about the information practices and activities of mobile workers at
			a more encompassing yet finer level of detail than seems to currently exist. A formalised study, including
			interviews, observation, and a diary component with a targeted mobile worker population, will follow. Given
			that a small percentage, if any, of the present workforce consists of true '<em>digital natives</em>,' such
			an informationally inclusive investigation of mobile work seems appropriate.</p>
		<section>
			<h2>References</h2>
			<ul>
				<li id="Ber93">Berlin, L.M., Jeffries, R., O'Day, V.L., Paepcke, A., and Wharton, C. (1993). Where did
					you put it? Issues in the design and use of a group memory.<em> Proceedings of the 1993 conference
						on Computer Human Interaction (CHI '93).</em> New York: ACM Press, 23-30.</li>

				<li id="Bro03">Brown, B., and O'Hara, K. (2003). <em>Place as a practical concern of mobile workers.
						Environment and Planning A, 35</em>, 1565-1587.</li>

				<li id="Cio14">Ciolfi, L., and Pinatti de Carvalho, A.F. (2014). Work practices, nomadicity and the
					mediational role of technology. <em>Computer Supported Collaborative Work Journal (CSCW),
						23</em>(2).</li>

				<li id="Cou07">Courtright, C. (2007). Context in information behavior research. <em>Annual Review of
						Information Science and Technology</em>(ARIST), 41, 273-306.</li>

				<li id="Dav02">Davis, G. B. (2002). Anytime/anyplace computing and the future of knowledge work.
					<em>Communications of the ACM, 45</em>(12), 67-73.</li>

				<li id="Eri14">Erickson, I., Jarrahi, M., Thomson, L., and Sawyer, S. (2014). More than nomads:
					Mobility, knowledge work and infrastructure. Paper presented at the European Group for
					Organizational Studies colloquium, Rotterdam, The Netherlands.</li>

				<li id="Har94">Harris, R., and Dewdney, P. (1994). <em>Barriers to information: How formal help systems
						fail battered women.</em> Westport, CT: Greenwood Press. </li>

				<li id="Hea00">Heath, C., Knoblauch, H., and Luff, P. (2000). Technology and social interaction: The
					emergence of 'workplace studies.' <em>British Journal of Sociology, 51</em>(2), 299-320.</li>

				<li id="Jon08">Jones, W. (2008). <em>Keeping found things found: The study and practice of personal
						information management.</em> San Francisco, CA: Morgan Kaufmann.</li>

				<li id="Jon07">Jones, W., and Teevan, J. (Eds.) (2007). <em>Personal information management.</em>
					Seattle, WA: University of Washington Press.</li>

				<li id="Kak01">Kakihara, M., and Sorenson, C. (2001). Expanding the 'mobility' concept. <em>SIGGROUP
						Bulletin, 22</em>(3), 33-37.</li>

				<li id="Lie14">Liegl, M. (2014). Nomadicity and the care of place-On the aesthetic and affective
					organization of space in freelance creative work. <em>Computer Supported Collaborative Work Journal
						(CSCW), 23</em>(2).</li>

				<li id="Mar95">Marchionini, G. (1995). <em>Information seeking in electronic environments</em>.
					Cambridge: Cambridge University Press.</li>

				<li id="Mas14">Massey, C., TenBrook, S., Tatum, C., and Whittaker, S. (2014). PIM and personality: What
					do our personal file systems say about us? <em>Proceedings of the 2014 conference on Computer Human
						Interaction (CHI '14).</em> New York: ACM Press.</li>

				<li id="Mer12">Mervyn, K., and Allen, D. K. (2012). Sociospatial context and information behaviour:
					Social exclusion and the influence of mobile information technology. <em>Journal of the American
						Society for Information Science and Technology, 63</em>(6), 1125-1141.</li>

				<li id="Ols14">Olson, J.S., and Olson, G.M. (2014). <em>How to make distance work work. ACM
						Interactions, 21</em>(2), 28-35.</li>

				<li id="Per01">Perry, M., O'Hara, K., Sellen, A., Harper, R., and Brown, B.A.T. (2001). Dealing with
					mobility: understanding access anytime, anywhere. <em>ACM Transactions on Computer-Human
						Interaction,4</em>(8), 1-25.</li>

				<li id="Pic03">Pica, D., and Kakihara, M. (2003). The duality of mobility: Understanding fluid
					organizations and stable interaction. <em>European Conference on Information Systems (ECIS)</em>,
					1555-1570.</li>

				<li id="Sor11">Sorensen, C. (2011). <em>Enterprise mobility: Tiny technology with global impact on work
						(Technology, work, and globalization)</em> (1st ed.). London: Palgrave Macmillan.</li>

				<li id="Spi12">Spinuzzi, C. (2012). Working alone together: Coworking as emergent collaborative
					activity. <em>Journal of Business and Technical Communication, 26</em>(4), 399-441.</li>

				<li id="Urr07">Urry, J. (2007). <em>Mobilities.</em> Cambridge: Polity Press.</li>

			</ul>
		</section>
	</article>
</body>

</html>