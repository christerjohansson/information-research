<!DOCTYPE html>
<html lang="en">

<head>
	<title>Information literacy in higher education - empowerment or reproduction? A discourse analysis approach</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="dcterms.title"
		content="Information literacy in Higher Education - empowerment or reproduction? A discourse analysis approach ">
	<meta name="author" content="Geoff Walton, Jamie Cleland">
	<meta name="dcterms.subject" content="Give a brief description of your paper">
	<meta name="description"
		content="This paper presents a qualitative investigation into whether online discourse, produced by first year undergraduate students, can demonstrate their information capabilities as a socially-enacted practice and be used to assess their information literacy. Discourse analysis was used to categorise online discussion board postings produced by first-year UK undergraduate students as part of a formative online peer assessment exercise. Online discourse was the node of analysis which sought to identify patterns of language within the social and cultural contexts in which they occurred. Postings were inductively analysed through manual content analysis. Postings appeared to embody student's discursive competence in information literacy, especially their level of information discernment and what constituted a quality 'reference' for an assignment. However, they also demonstrated that 'references' perform a certain function within this discourse as an agreed construct between tutor, student and librarian. Students were engaged in the process of becoming good scholars by using appropriate online postings to create valid arguments through assessing other's work, but what they did not do was question received meanings regarding the quality of found information they used as evidence.">
	<meta name="keywords" content="Information behaviour, information literacy, discourse analysis, e-learning">
	<meta name="robots" content="all">
	<meta name="dcterms.publisher" content="Professor T.D. Wilson">
	<meta name="dcterms.type" content="text">
	<meta name="dcterms.identifier" content="ISSN-1368-1613">
	<meta name="dcterms.identifier" content="http://InformationR.net/ir/19-4/isic/isicsp3.html">
	<meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/19-4/infres194.html">
	<meta name="dcterms.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dcterms.issued" content="2014-12-15">
	<meta name="geo.placename" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<header>
		<h4 id="vol-19-no-4-december-2014">vol. 19 no. 4, December, 2014</h4>
	</header>
	<article>
		<h1 id="information-literacy-in-higher-education---empowerment-or-reproduction-a-discourse-analysis-approach">
			Information literacy in higher education - empowerment or reproduction? A discourse analysis approach</h1>
		<h4 id="geoff-walton"><a href="#author">Geoff Walton</a><br>
			Department: Mathematics and Information Sciences, Northumbria University, Newcastle-upon-Tyne, NE1 8ST, UK
		</h4>
		<h4 id="jamie-cleland"><a href="#author">Jamie Cleland</a><br>
			Department of Social Sciences, Loughborough University. Leicestershire, LE11 3TU, UK</h4>
		<h4 id="abstract">Abstract</h4>
		<blockquote>
			<p><strong>Introduction.</strong> This paper presents a qualitative investigation into whether online
				discourse, produced by first year undergraduate students, can demonstrate their information capabilities
				as a socially-enacted practice and be used to assess their information literacy.<br>
				<strong>Method.</strong> Discourse analysis was used to categorise online discussion board postings
				produced by first-year UK undergraduate students as part of a formative online peer assessment
				exercise.<br>
				<strong>Analysis.</strong> Online discourse was the <em>node</em> of analysis which sought to identify
				patterns of language within the social and cultural contexts in which they occurred. Postings were
				inductively analysed through manual content analysis.<br>
				<strong>Results.</strong> Postings appeared to embody student's discursive competence in information
				literacy, especially their level of information discernment and what constituted a quality 'reference'
				for an assignment. However, they also demonstrated that 'references' perform a certain function within
				this discourse as an agreed construct between tutor, student and librarian.<br>
				<strong>Conclusions.</strong> Students were engaged in the process of becoming good scholars by using
				appropriate online postings to create valid arguments through assessing other's work, but what they did
				not do was question received meanings regarding the quality of found information they used as evidence.
			</p>
		</blockquote>
		<h2 id="literature-review">Literature review</h2>
		<p>It is argued that information literacy capabilities are context based and social in nature (<a
				href="#Hep09">Hepworth &amp; Walton, 2009</a>). Walton &amp; Hepworth (<a href="#Wal13">2013</a>, p55)
			have further articulated the subset of information literacy capability known as information discernment
			which they define as, '<em>the ability to use higher order thinking skills in order to make sound and
				complex judgements regarding a range of text-based materials</em>'. Five discrete levels of this ability
			are identified, from the lowest ('cut and paste') to the highest (where individuals can express the relative
			value of evaluation criteria for a given source of information). This theory overlaps with Ford's notion of
			'relevance judgements' found in the information seeking process (<a href="#For04">Ford, 2004</a>, p203).</p>
		<p>It is suggested that discourse analysis appears to be a useful methodology for analysing information literacy
			capabilities, yet it does not feature heavily in relation to information literacy research and practice with
			the notable exception of some recent studies (see Lloyd (<a href="#Llo12">2012</a>). However, it has been
			used to a greater extent within information behaviour research (<a href="#Cas12">Case, 2012</a>; <a
				href="#Bat05">Bates, 2005</a>, <a href="#Giv05">Given, 2005</a>; <a href="#Tuo05">Tuominen <em>et
					al</em>., 2005</a>) which is argued provides an evidence base for information literacy (<a
				href="#Hep13">Hepworth &amp; Walton, 2013</a>). Discourse is regarded as more than simply talking or
			writing, it is a complex network of relationships between individuals, texts, ideas and institutions (<a
				href="#Fou72">Foucault, 1972</a>: <a href="#van05">van Leeuwen, 2005</a>). It is from these
			relationships that sharing of meaning arises at specific points (<em>a node</em>) in space and time (<a
				href="#Ols10">Olsson, 2010</a>). Therefore, individual information behaviour is not an isolated
			activity, it is this discursive element which locates the person-in-context (<a href="#Wil99">Wilson,
				1999</a>).</p>
		<h2 id="methodology">Methodology</h2>
		<p>This research seeks to address the following questions:</p>
		<ul>
			<li>RQ1: To what extent does online discourse reveal students' information literacy capabilities (as a
				socio-cultural practice)?</li>
			<li>RQ2: In what ways could this online discourse be used as a basis for summative assessment of their work?
			</li>
			<li>RQ3: in what ways does online peer assessment embody complex and asymmetrical power relations between
				tutors, students and librarians?</li>
			<li>RQ4: how is academic discourse rehearsed, negotiated, reproduced and its meaning shared through online
				discourse?</li>
		</ul>
		<h3 id="discourse-analysis-as-a-method">Discourse analysis as a method</h3>
		<p>The node for analysis in this study is the online postings made by first-year undergraduate student
			participants during the online peer assessment activities within a particular module. The socio-historical
			context of this study is a UK Higher Education institution from 2007-2012. However, it is noted that
			information literacy is relevant beyond this specific context. Data was themed into categories to identify
			'patterns and processes, commonalities and differences' across the student cohort (<a
				href="#Mil94">following Miles and Huberman, 1994</a>, p.9).</p>
		<h2 id="results">Results</h2>
		<p>Online postings sent to Blackboard's Discussion Board centred on the notion of 'references' - artefacts of
			found information such as a book, journal article, website or newspaper article used in the student author's
			draft assignments. Below are typical pieces of online discourse posted by students (commentators) commenting
			on fellow student's draft essays (authors):</p>
		<blockquote>
			<p>SS11: 'It is effective how you focus on one particular idea and you use good examples to back your
				arguments up…. Overall referncing (sic) was done well, however make sure every key point made is backed
				up with a reference. Also try to use academic references instead of autobiographies.'<br>
				SB11: 'you have included a number of points to why sport affects society instead of focusing on one area
				and used references well, the first paragraph has over 5 references that all support your point […]
				which proves there is a lot of evidence for your argument….'<br>
				SF11: 'A very good use of references as well to help back up the points you are making about certain
				health issues….'</p>
		</blockquote>
		<p>These extracts appear to embody the student commentator's discursive competence in information literacy,
			especially in terms of information discernment, in showing that they have made relevance judgements in
			evaluating academic texts (corroborating Ford, <a href="#For04">2004</a>; Walton &amp; Hepworth, <a
				href="#Wal13">2013</a> and Lloyd, <a href="#Llo12">2012</a>). Particularly, for example, what
			constitutes a quality reference ('academic' as opposed to 'autobiographies'), that there should be a number
			of references ('5 reference') and that these should be used specifically to 'make sure every key point is
			backed up by a reference' corroborating Walton &amp; Hepworth (<a href="#Wal13">2013</a>) and addressing
			RQ1. This also appears to offer promising output for assessment purposes in that commentators demonstrate
			their information discernment capability within the discourse (answering RQ2).</p>
		<p>All agree on the subject of the references and what function they should perform in the author's assignment,
			that they should be present and should be used to 'back up' each 'key point' or 'argument'. Hence,
			references perform a certain function within the discourse, not just to support an argument, but as an
			agreed construct between tutor, student and librarian (and therefore within Western scholarship generally) -
			addressing RQ3. In this sense, commentators are exercising their power inductively rather than coercively
			(<a href="#Fou72">Foucault, 1972</a>). The commentators have become 'authoritative speakers' where the
			discourse posted, for example, 'try and use academic references' is accepted as a 'truth statement' by their
			community (the student participants). This is not to say that the commentator is making a statement of
			truth, it is only deemed true by the discursive network, i.e., students agree that it is necessary to write
			an essay that will get a good grade (addressing RQ4).</p>
		<h2 id="conclusion">Conclusion</h2>
		<p>Online peer assessment appears to be a useful way for evidencing information literacy capabilities as a
			socio-cultural practice by revealing relevance judgements and information discernment within the discourse
			itself (addressing RQ 1). The online contributions made by students as commentators are contextually
			appropriate by embodying attributes of information literacy capability, demonstrating discursive competence
			in evaluating information which may lend themselves to summative assessment, which partly addresses RQ 2.
			However, they are operating within a well-structured discourse which reproduce structures that already exist
			and their questioning is bounded and finite - addressing RQ3 and 4. In other words, the outcome of
			information literacy is already decided for the student and s/he merely has to follow the rules of the game
			and is subservient to more powerful discourses. Critical thinking is only engaged in a very narrow range.
		</p>
		<section>
			<h2>References</h2>

			<ul>
				<li id="Bat05">Bates, M. J. (2005). <em>An introduction to metatheories, theories and models</em>. In
					Fisher, K. E., Erdelez, S. & McKechie, L. E. F. (eds.) (2005). <em>Theories of information
						behavior</em>. Assist Monograph Series. Medford: Information Today, Inc. pp1-24.</li>
				<li id="Cas12">Case, D. O. (2012). Looking for information. <em>A survey of research on information
						seeking, needs and behavior</em>. (3rd edn.). Bingley: Emerald Group.</li>
				<li id="For04">Ford, N. (2004). <em>Towards a model of learning for educational informatics. Journal of
						Documentation, 60</em>(2), pp183-225.</li>
				<li id="Fou72">Foucault, M. (1972). <em>Archeology of Knowledge</em>. London: Tavistock.</li>
				<li id="Giv05">Given, L. M. (2005). Social positioning. In Fisher, K. E., Erdelez, S. & McKechie, L. E.
					F. (eds.) (2005). <em>Theories of information behavior</em>. Assist Monograph Series. Medford:
					Information Today, Inc. pp334-338.</li>
				<li id="Hep09">Hepworth, M. & Walton, G. (2009). <em>Teaching information literacy for inquiry-based
						learning</em>. Oxford: Chandos.</li>
				<li id="Hep13">Hepworth, M. & Walton, G. (eds) (2013). Introduction - information literacy and
					information behaviour, complementary approaches for building capability. In Hepworth, M. & Walton,
					G. (eds) <em>Developing people's information capabilities: fostering information literacy in
						educational, workplace and community contexts</em>. Bingley: Emerald Group Publishing Ltd.</li>
				<li id="Llo12">Lloyd, A. (2012). Information literacy as a socially enacted practice: Sensitising themes
					for an emerging perspective of people-in-practice. <em>Journal of Documentation, 68</em>(6),
					pp772-783 [Online] http://www.emeraldinsight.com/journals.htm?articleid=17062472 (accessed 15 August
					2013).</li>
				<li id="Mil94">Miles, M. B. & Huberman, A.M. (1994). <em>Qualitative data analysis: an expanded
						sourcebook. </em>(2nd edn.). Thousand Oakes: Sage.</li>
				<li id="Ols10">Olsson, M. (2010). <em>Michel Foucault: discourse, power/knowledge and the battle for
						truth. In Leckie, G. J., Given, L. M. and Buschamen, J. (eds) Critical theory for library and
						information science: exploring the social from across the disciplines</em>. Santa Barbara:
					Libraries Unlimited, pp63-74.</li>
				<li id="Tuo05">Tuominen, K., Talja, S. & Savolainen, R. (2005). The social constructionist viewpoint of
					information practices. In Fisher, K. E., Erdelez, S. & McKechie, L. E. F. (eds.) (2005).
					<em>Theories of information behavior</em>. Assist Monograph Series. Medford: Information Today, Inc.
					pp328-333.</li>
				<li id="van05">van Leeuwen, T. (2005). <em>Introducing social semiotics</em>. New York: Routledge.</li>
				<li id="Wal13">Walton, G. & Hepworth, M. (2013). Using assignment data to analyse a blended information
					literacy intervention: a quantitative approach. <em>Journal of Librarianship and Information
						Science, 45</em>(1) pp53-63</li>
				<li id="Wil99">Wilson, T. D. (1999). Models in information behaviour research. <em>Journal of
						Documentation, 55</em>(3), pp249-270.</li>
			</ul>
		</section>
	</article>
</body>

</html>