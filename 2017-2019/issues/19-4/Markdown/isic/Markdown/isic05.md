<header>

#### vol. 19 no. 4, December, 2014

Proceedings of ISIC: the information behaviour conference, Leeds, 2-5 September, 2014: Part 1.

* * *

</header>

<article>

# Web 2.0 as a tool for market intelligence acquisition in the Malaysian hotel industry

#### [Ching Seng Yap](#authors)  
Graduate School of Business, Universiti Tunku Abdul Razak, Malaysia  
#### [Boon Liat Cheng](#authors)  
Sunway University Business School, Sunway University, Malaysia  
#### [Kum Lung Choe](#authors)  
Faculty of Business and Finance, Universiti Tunku Abdul Rahman, Malaysia

#### Abstract

> **Introduction.** Web 2.0 applications such as social networks, blogs, content aggregators, online forums, and content communities can serve as powerful marketing communication tools for disseminating product information, obtaining feedback from customers, and creating online communities. This study aims to investigate the current state of market intelligence acquisition from various Web 2.0 applications in hotels and to examine the probable relationship between market intelligence practices, environmental turbulence and firm performance.  
> **Method.** Data were collected from 280 sales and marketing personnel of hotels via self-administered questionnaire survey.  
> **Analysis.** Descriptive statistics were used to describe the sample and the distribution of responses to each question. Partial least squares path modelling was used to test the reliability and validity of the data, and the hypothesised relationships in the study.  
> **Results.** This study revealed that the use of Web 2.0 applications for market intelligence acquisition is still low and found a positive relationship between environmental turbulence, market intelligence acquisition and firm performance.  
> **Conclusions.** This paper concluded that higher perceived environmental turbulence leads to higher extent of market intelligence acquisition, which subsequently leads to enhanced firm performance among the hotels in Malaysia.

<section>

## Introduction

The prevalence of Web 2.0 applications such as social networks, blogs, content aggregators, online forum, and content communities has changed the marketing landscape for businesses. Web 2.0 has caused the shift of loci of activity, value and power from firms to consumers ([Berthos, Pitt, Plangger and Shapiro 2012](#ber12)) which lead to an increasing mistrust and diminishing effect of traditional marketing media. Web 2.0 can be an effective marketing channel to be used in integrated communications and marketing of tourism services ([Sotiriadis and van Zyl 2013](#sot13)). Apart from being a powerful marketing communication tool, these emerging Internet media may also serve as an important tool for market intelligence acquisition. Past studies on the use of Web 2.0 applications by marketers focused mostly on the dissemination of marketing information about their products and services to customers. However, Web 2.0 applications can also be used as the tools for acquiring market intelligence. For example, listening to the online customer's voice enables marketers to learn about customer experience with products and services, identifying trends and new market opportunities through social networks, and receiving early warnings about product problems from online product reviews. These studies showed that increasing number of consumers are relying on opinion of other consumers in their online purchase decisions, ranging from what movie to watch to what stock to buy. Customers also consider blogs as more credible information sources for customers' experience, product reviews and comments than the traditional sources of news media ([Elliot 2006](#ell06)). As such, market intelligence acquired from these media is vital for marketers to devise their marketing strategy according to the changing needs of customers. Compared with the traditional types of marketing media, Web 2.0 applications contain abundant information and resources, are more interactive, and provide customizable and personalized contents. Moreover, blogs and online social networks have become increasingly ubiquitous as exemplary by Facebook which registered 1 billion users as of October 2012 and the Huffington Post's 54 million unique monthly visitors.

Previous empirical studies examine blogs as a means of marketing communications as opposed to the traditional word-of-mouth ([Singh, Veron-Jackson and Cullinane 2008](#sin08)) or social network as a means of direct marketing ([Sharifah Fatimah and Murphy 2010](#sha10)). Culnan, McHugh and Zubillaga ([2010](#cul10)) identified social media as a source for acquiring new knowledge from customers and subsequently exploiting that knowledge for devising marketing strategies. However, almost none of these studies have empirically examined the use of social media as a tool for market intelligence acquisition. Empirical research on the relationship between market intelligence practices, perception of environmental turbulence and market performance is also limited. Therefore, the study attempts to answer the following research questions: (1) what is the extent of the use of Web 2.0 applications as the tools for market intelligence acquisition? (2) does perceived environment turbulence relate positively to market intelligence? and (3) does market intelligence relate positively to firm performance? The tourism industry is chosen as it has been ranked the top industry in Malaysia in terms of online transaction volume from hotels, airlines, tour operators, and destination marketing organisations. In addition, tourism is one of the 12 National Key Economic Areas (NKEAs) under the Economic Transformation Program which is expected to make substantial contributions to the nation's economic performance and will receive prioritised public investment and policy support. By providing empirical evidence from the local context, this research is important for tourism industry players to gain an understanding of the Web 2.0 application as an emerging tool for market intelligence acquisition and the potential relationship between market intelligence practices and firm performance. Data were collected from 280 marketing managers and executives of the hotels via self-administered questionnaire survey.

This paper unfolds as follows. Section two reviews the foundation concepts of information processing literature and its link to current study, and assesses the empirical studies concerning the relationship between environmental turbulence, market intelligence practices and firm performance, and use of Web 2.0 in marketing activities and in hotels. The section ends with the development of two hypotheses. Section three describes sample and sampling procedures, data collection methods, and variables and measurement. Section four reports the data analysis and hypothesis tests. The paper ends with discussing research implications, limitations and recommendations for future research.

## Review of the literature

### Information processing literature

In information processing literature, the activities of gathering, processing, and acting on data from the environment are the main task of a firm ([Daft and Weick 1984](#daf84)). These activities closely relate to strategic planning of a firm concerning environmental scanning and competitor analysis. Information requirements of a firm are contingent on interplay among strategy, structure, decision making processes, and business environments it operates in ([Rogers, Miller and Judge 1999](#rog99)). Environmental uncertainty is one of the major determinants to information processing activities in a firm. When a business environment sector is perceived to be uncertain, more effort is required to scan the environment sector, collect information from the sector and process it to be actionable information for effective decision making ([Choo 2001](#cho01)). The environmental sector can be divided into task (immediate) sector or general (remote) sector. The task environment is considered to be industry specific and consists of all those organisations with which a given organisation must interact to grow and survive. It typically includes customers, suppliers and competitors. These sectors are closer to organisations and therefore information about opportunities and threats is more easily accessible and the timing of developments is more easily understood ([Fahey and Narayanan 1986](#fah86))). On the other hand, the general environment consists of broad forces that influence societal institutions as a whole, including economic conditions, technological development, political and regulatory environments, and socio-cultural and demographic trends. The opportunities and threats posed by the general environment sectors tend to be more abstract and the timing of development is more difficult to estimate ([Priem, Love and Shaffer 2002](#pri02)). Therefore, the task environment is associated with greater degree of strategic uncertainty and is likely to have a direct impact on the firm and is thus considered more important to business level strategy, especially in goal setting and goal attainment. In contrast, the general environment is more likely to have an indirect impact on organisational performance and is often linked to corporate level strategy ([Bourgeois 1980](#bou80)).

Prior empirical studies have examined the perceptions of managers in tourism industry about the business environments and the link with information processing activities. For example, in studying the behaviour on environmental scanning and information utilisation among general managers of hotels, travel agencies, and tour operators in Hong Kong, Jogaratnam and Law ([2006](#jog06)) found that scanning attention was higher for the task environment, specifically the customer, competitor and economic sector for strategic decision making. The study also found that personal sources are preferred to impersonal sources as well as internal sources are utilized more frequently than external sources. Further to the study, Jogaratnam and Wong ([2009](#jog09)) examined the scanning behaviour of 201 top level hotel executives and found that they perceived higher levels of hostility, dynamism, and complexity in the task environment than the remote environment. The respondents scanned the task environment with greater frequency and more broadly in response to increased uncertainty. In Singapore, Zhang, Majid and Foo ([2013](#zha13)) found a positive relationship between perceived strategic uncertainty and scanning frequency among the travel agent companies. Human information sources were used more frequently than electronic sources for collecting environmental information. Oreja-Rodriguez and Yanes-Estevez ([2007](#ore07)) found that 34 tourism firms in Canary Island, Spain rated customer demand and competitors as the main sources of environment uncertainty. The perceived environment uncertainty is explained by both perceived dynamism and complexity.

The prevalence of Web 2.0 applications has provided firms with additional platform and sources for acquiring information about the business environment, specifically about market and customers that have an impact on their strategic direction and operations. However, there exists limited published research investigating the use of Web 2.0 as a source for market intelligence acquisition.

### Market intelligence practices

From the marketing literature, market intelligence is defined as a major dimension of market orientation that covers organisation-wide gathering, sharing and use of exogenous market factors that affect current and future needs of customers ([Kohli and Jaworski 1990](#koh90)). Market intelligence is one of the most important determinants to market success and failure, especially in the literature of new product development ([Droge _et al._ 2008](#dro08); [Mishra _et al._ 1996](#mis96)). From the information science literature, market or marketing intelligence is considered a subset of competitive intelligence which can be defined as a method and a product. As a method, competitive intelligence is the tool used for identifying the intelligence needs, systematically gathering relevant information about competitors and processing the information into actionable knowledge ([Jaworski and Wee 1992](#jaw92)). As a product, competitive intelligence is information about the present and future behavior of competitors, suppliers, customers, technologies, government and other general business environment ([Vedder and Guynes 2000](#ved00)). Market intelligence specifically focuses on market aspects of competitive intelligence such as new markets and creative segmentation opportunities, and major shifts in marketing and distribution ([Rouach and Santi 2001](#rou01)). Market intelligence practice consists of processes involving gathering, analysis, dissemination, and use of actionable information by managers in decision making. Market intelligence practices emphasise the ethical conduct of the processes involved and should be distinguished from industry espionage. Market intelligence can be gathered through various publicly available sources such as industry reports, government reports, customers, suppliers, and competitors. As such, Web 2.0 serves as one of the market intelligence sources, which is ubiquitous, easily obtained and inexpensive.

Various consulting firms such as Global Intelligence Alliance (GIA), McKinsey, and Frost & Sollivan have reported their survey findings on market intelligence practices worldwide. In 2011, drawing from 989 responses representing large- and mid-sized organisations worldwide, GIA reported that slightly over three quarters of the surveyed firms have systematic market intelligence in place. A majority of the firms agreed that they have gained benefits from market intelligence practices and their investment have paid off. Firms with a systematic market intelligence program consistently make decision more efficiently than those without. On average, the surveyed firms are considered the intermediates adopters. The firms with world class market intelligence are substantially more efficient in decision making than other firms in general ([GIA 2011](#gia11)). Nevertheless, very little academic empirical research can be found on market intelligence practices in business organisations, largely lacking in theoretical perspective.

### The use of Web 2.0 applications in marketing functions

Research on the use of Web 2.0 applications for gaining business value has been common, ranging from sales and marketing to customer relationship management and after-sales services. For example, using blogs to gain real information and valuable market research, supporting brand loyalty, and targeting customer segments ([Singh _et al._ 2008](#sin08)); using social networking as a marketing communication channel ([Sharifah Fatimah and Murphy 2010](#sha10)), using social media for promoting social endorsement of brands and products and monitoring customer feedback and complaints ([Schaffer 2010](#sch10)), and using online customer product reviews for increasing sales volume ([Chevalier and Mayzlin 2006](#che06)).

Web 2.0 applications have the potential to provide not only the traditional e-commerce activities such as disseminating product information and driving sales but also forming online customer communities. Li and Bernoff ([2008](#li08)) refer this formation of online communities as virtual customer environments. Virtual customer environments facilitate branding, sales, customer service and support, and product development, as well as gain business value from each of these areas. Online community site such as TripAdvisor encourages travelers to review the services and share their experience with other potential travelers who consider the purchases. The peer reviews are seen as more trustworthy than traditional promotional materials provided by the company itself ([Harris and Rae 2009](#har09)).

Culnan, McHugh and Zubillaga ([2010](#cul10)) surveyed the use of four social media platforms to engage with customers among the Fortune 500 companies. On average, each company adopted slightly more than a social media platform, with Twitter being the most frequently used, followed by Facebook, blogs, and client-hosted forums. Based on case studies conducted at three of the US largest companies (Walmart, HP, and Coca-Cola), the study identified three elements of effective social media implementation - making mindful decision for initial adoption, building customer communities, and developing absorptive capacity to learn from the content generated by the customers. The last element serves as the ultimate goal of using social media platform in organisations which is to acquire new knowledge provided by their customers and subsequently exploit that knowledge. Each interaction in the social media platform is viewed as a transaction which requires organisations to manually process or respond to.

Berkman ([2008](#ber08)) explained how market intelligence can be acquired via forums, multimedia, social bookmarking, social networking sites, alerts, RSS, and buzz surfacing tools. Through strategic listening, companies are able to tap into and obtain valuable near real-time market intelligence to serve the current customer better, identify potential future customers, and better spot emerging consumer and social trends. Singh _et al._ ([2008](#sin08)) reported the use of blogs by companies as tools to better engage their customers in the creation, delivery, and dissemination of marketing messages. Blogging is viewed as a viral marketing as it uses social networks, user generated content, and interactivity to spread the messages. Companies such as General Motors, IBM, Sun Microsystems, Microsoft, among others, are adopting blogging as a communication platform with customers and other stakeholders. Blogging is also used in soliciting market information from customers and responding to feedback provided by customers in a fast and effective manner.

### Web 2.0 applications as tools for market intelligence acquisition

Puhringer and Taylor ([2008](#puh08)) viewed blogs and social networks as potential sources of strategic market knowledge for destination marketing organisations as both platforms allow maintenance of existing social ties and formation of new connection in the virtual environments. Specifically, market intelligence that can be obtained from the Web 2.0 applications include trends in traveler movements, reviews of products and services, product or infrastructure gap, reviews of the performance of associated products (airlines) or collaborators (partnering ventures), and competitors' market strategies.

Chen and Xie ([2008](#che08)) posit that online consumer review as an emerging source of product information. Unlike seller-created product information that describes products in terms of technical specifications, online consumer reviews are more user-oriented. Online sellers may capitalize on the information provided by consumers in making decisions concerning the response to consumer reviews, the creation and dissemination of consumer reviews, and the best timing for introducing consumer reviews for a given product.

### The use of Web 2.0 applications in hotels

The use of Web 2.0 in tourism organisations varies between different countries. For instance, Milano, Baggio and Piattelli ([2011](#mil11)) found a limited usage of Web 2.0 functionalities by tourism websites in Italy. Schegg, Liebrich, Scaglione and Syed Ahmad ([2008](#sch08)) also reported that the presence of the Web 2.0 technologies and applications is still low. Hamill, Attard and Stevenson ([2009](#ham09)) found that national destination marketing organisations from the leading tourist destination in Europe have underutilised Web 2.0 in spite of exhibiting a high level of awareness and understanding of the potential of Web 2.0 concerning global marketing and customer engagement. However, according to Milwood ([2013](#mil13)), the destination marketing organisations have begun to introduce various social media tools to their websites in the USA as compared to the Swiss counterparts.

From an Asian perspective, Chan and Guillet ([2008](#cha11)) assessed the marketing performance of 67 hotels in Hong Kong on 23 social media sites under five phases of online marketing - attracting, engaging, retaining, learning, and relating. The study found that hotels in Hong Kong generally performed poorly in using social media to learn about customers. The study also reported that some hotels lack commitment to sustain social marketing efforts as return on investment in social media marketing is uncertain.

### Market intelligence practice and firm performance

In December 2010, McKinsey reported the use of Web 2.0 applications from a survey of 3,249 executives from various backgrounds. A large majority of the respondents who used Web 2.0 applications reported that they were receiving measurable business benefits for not only customer-related purposes, but also for internal purposes and working with external partners or suppliers. Specific marketing-related benefits include increasing customer satisfaction, reducing marketing, support and travel costs, and reducing time to market for products and services. The use of Web 2.0 applications is also significantly correlated with corporate performance measured by market share gains, higher operating margins, and market leadership ([Bughin and Chui 2010](#bug10)).

Ye, Law and Gu ([2009](#ye09)) found that online consumer-generated review has a positive impact on hotel business performance. Based on the data obtained from the largest travel site in China, Ctrip, the number of review, the average rating of reviews, and hotel room rates are positively related to hotel room reservation. Soh ([2003](#soh03)) reported that firms which acquired more information about other firms through networking alliances would gain information advantage and lead to improved new product performance. On the other hand, firms with more information about the product tend to have higher level of new product performance in terms of profit margin and return on investment ([Brockman and Morgan 2003](#bro03)). While information acquisition is vital, Diamantopoulos and Souchon ([1999](#dia99)) argued that any information acquired had little impact on firm performance unless it is utilized fully in decision-making. Prior empirical studies revealed the important role of information utilisation in new product development ([Moorman 1995](#moo95); [Lybaert 1998](#lyb98)), in assessing alliance formation ([Ottum and Moore 1997](#ott97)), and in marketing program planning effectiveness ([Conant and White 2000](#con00)), which subsequently leads to enhanced financial performance.

Keh, Nguyen and Ng ([2007](#keh07)) found that acquisition and utilisation of market information in marketing mix decisions are positively related to firm performance among SMEs in Singapore. Song, Di Benedetto and Parry ([2009](#son09)) revealed that formal market information acquisition and use have direct and positive impact on Chinese new ventures, specifically the use of formal processes for market information acquisition is higher among new ventures serving emerging market while formal processes for information use has a higher impact on new ventures serving established markets. The latter finding was also consistently found to have higher impact on technological-driven firms than market-driven firms ([Parry and Song 2010](#par10)). Among new ventures in the USA, Song, Wang and Parry ([2010](#son10)) reported that formal processes of market information acquisition is positively linked to formal processes of market information use, which in turn is positively associated with new venture performance.

From the perspective of an emerging economy, Yap and Md Zabid ([2011](#yap11)) found that competitive intelligence related positively to firm performance among Malaysian public listed companies. Companies with a formal competitive intelligence unit performed better than those without such formal unit. Adidam, Banerjee and Shukla ([2012](#adi12)) also found a positive and significant relationship between competitive intelligence activities and firm's financial performance in India. Both studies revealed that among the competitive intelligence sectors, respondents ranked acquisition of customer intelligence the top sector. From the above literature, the acquisition about market intelligence from various business environment sectors and its utilisation in marketing decision-making are seen as crucial in today's businesses.

Based on the earlier review on information processing literature and prior related empirical studies, this study argues that when the perception of environment turbulence increases, marketing managers of hotels tend to acquire a higher extent of market intelligence from various sources. The market intelligence will be used in a greater extent in making marketing decisions which subsequently leads to better firm performance. Therefore,

Hypothesis 1: Perceived environmental turbulence relates positively to the extent of market intelligence acquisition.

Hypothesis 2: The extent of market intelligence acquisition relates positively to firm performance.

## Method

### Sample and sampling procedures

The unit of analysis for this study is organisation in the tourism industry, specifically hotels. Hotels are the major tourism industry players and interact directly with customers via online interface. The list of hotels was obtained from the [Malaysian Association of Hotels](http://www.hotels.org.my). For questionnaire survey, quota sampling technique was used to ensure adequate representation of sample characteristics in terms of location and star rating. For location, the study selects 100 hotels each from Klang Valley, East Coast, Northern, and Southern regions of Peninsular Malaysia, totaling 400 hotels. Of 400 hotels, 280 hotels returned usable questionnaires, a response rate of 70%. Almost half (46%) of the respondents hold junior management position, while one third (34%) are middle management and the remaining one fifth are senior management. Majority of the hotel are locally owned (88%) and have been in operation for less than 10 years (84%). Slightly over half (52%) of the respondents are 3-star hotels, and the remaining are 4-star and 5-star hotels respectively, with both constituting about a quarter of the total respondents. The major customer acquisition channel is online agency (37%), followed by directory (25%) and internal sales team (16%). In terms of hotel size, it is almost equally distributed between large and small hotels measured by number of rooms and employees. The major types of customers consist of corporate (35%), government (24%) and family (18%). The remaining quarter (24%) comprises groups, couples and solo travelers. Refer Table 1 for more detailed demographic information about the sample.

<table><caption>Table 1: Sample profile (n=280)</caption>

<tbody>

<tr>

<th>Variable</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td colspan="3">

**Sales and marketing position**</td>

</tr>

<tr>

<td>Junior management</td>

<td>128</td>

<td>45.7</td>

</tr>

<tr>

<td>Middle management</td>

<td>95</td>

<td>33.9</td>

</tr>

<tr>

<td>Senior management</td>

<td>57</td>

<td>20.4</td>

</tr>

<tr>

<td colspan="3">

**Type of organisation**</td>

</tr>

<tr>

<td>Local</td>

<td>246</td>

<td>87.9</td>

</tr>

<tr>

<td>Local majority</td>

<td>24</td>

<td>8.6</td>

</tr>

<tr>

<td>Foreign</td>

<td>6</td>

<td>2.1</td>

</tr>

<tr>

<td>Foreign majority</td>

<td>4</td>

<td>1.4</td>

</tr>

<tr>

<td colspan="3">

**Star rating**</td>

</tr>

<tr>

<td>3-star</td>

<td>144</td>

<td>51.5</td>

</tr>

<tr>

<td>4-star</td>

<td>67</td>

<td>23.9</td>

</tr>

<tr>

<td>5-star</td>

<td>69</td>

<td>24.6</td>

</tr>

<tr>

<td colspan="3">

**Major customer acquistion channel**</td>

</tr>

<tr>

<td>Online agency</td>

<td>104</td>

<td>37.1</td>

</tr>

<tr>

<td>Directory</td>

<td>70</td>

<td>25.0</td>

</tr>

<tr>

<td>Own sales team</td>

<td>44</td>

<td>15.7</td>

</tr>

<tr>

<td>Tour agency</td>

<td>31</td>

<td>11.1</td>

</tr>

<tr>

<td>Walk-in</td>

<td>16</td>

<td>5.7</td>

</tr>

<tr>

<td>Hotel website</td>

<td>11</td>

<td>3.9</td>

</tr>

<tr>

<td>Others</td>

<td>4</td>

<td>1.4</td>

</tr>

<tr>

<td colspan="3">

**Years in operation**</td>

</tr>

<tr>

<td>Less than 5 years</td>

<td>90</td>

<td>32.1</td>

</tr>

<tr>

<td>5 to 10 years</td>

<td>144</td>

<td>51.4</td>

</tr>

<tr>

<td>More than 10 years</td>

<td>53</td>

<td>18.9</td>

</tr>

<tr>

<td colspan="3">

**Number of employees**</td>

</tr>

<tr>

<td>Less than 20</td>

<td>38</td>

<td>13.6</td>

</tr>

<tr>

<td>20 to 60</td>

<td>106</td>

<td>37.9</td>

</tr>

<tr>

<td>61 to 100</td>

<td>38</td>

<td>29.6</td>

</tr>

<tr>

<td>More than 100</td>

<td>53</td>

<td>18.9</td>

</tr>

<tr>

<td colspan="3">

**Number of rooms**</td>

</tr>

<tr>

<td>100 and below</td>

<td>146</td>

<td>52.1</td>

</tr>

<tr>

<td>More than 100</td>

<td>134</td>

<td>47.9</td>

</tr>

<tr>

<td colspan="3">

**Major type of customers**</td>

</tr>

<tr>

<td>Corporate</td>

<td>99</td>

<td>35.4</td>

</tr>

<tr>

<td>Government</td>

<td>66</td>

<td>23.6</td>

</tr>

<tr>

<td>Family</td>

<td>49</td>

<td>17.5</td>

</tr>

<tr>

<td>Group</td>

<td>28</td>

<td>10.0</td>

</tr>

<tr>

<td>Couple</td>

<td>25</td>

<td>8.9</td>

</tr>

<tr>

<td>Solo</td>

<td>8</td>

<td>2.9</td>

</tr>

<tr>

<td>Others</td>

<td>5</td>

<td>1.8</td>

</tr>

</tbody>

</table>

### Data collection method

Primary data collection method is most appropriate to obtain responses directly from sample about their perceptions of environment turbulence, market intelligence practices, use of Web 2.0 in market intelligence acquisition, and perceived firm performance. Even though survey instrument is adopted from prior related studies, a validation check from marketing professors was conducted to ensure the face validity of the questions. Similarly, a pretesting to 20 hotels was conducted to ensure the reliability of the survey instrument.

### Variables and Measurement

_Perceived environmental turbulence._ The construct is defined as the hostile environments that are characterised by intense competition, frequent technological advancements, and changes in customer preferences. It is operationalised as having three items adopted from Droge, Calantone and Harmancioglu (2008). A sample item includes "The actions of our competitors are unpredictable". All 3 items are measured by 5-point Likert scale from 1 - strongly disagree to 5 - strongly agree.

_Market intelligence acquisition._ Market intelligence acquisition is measured by 5 items adapted from Moorman ([1995](#moo95)). A sample item includes "Our organisation has formal processes for continuously collecting intelligence about competitors' activities". All 5 items are measured by 5-point Likert scale from 1 - strongly disagree to 5 - strongly agree.

_Firm performance._ Firm performance is measured in terms of customer relationship performance and is operationalized as having four items - loyalty, satisfaction, lifetime value, and customer retention ([Griffin and Page 1993](#gri93); [Moorman and Rust, 1999](#moo99); [Wang, Chen and Chen, 2012](#wan12)). Respondents are asked to rate the customer relationship performance of the hotel along 5-point Likert scale from 1 - strongly disagree to 5 - strongly agree.

_Web 2.0 applications._ The study also collected information on sources of market intelligence acquisition from 12 Web 2.0 applications - personal blogs, corporate blogs, personal social networks, professional social networks, discussion groups/forums, mailing lists/RSS/newsgroups, podcasts, videocasts/vodcasts, webinars, Google Alert/Yahoo Alerts, Wikipedia/Wikimedia, and online consumer product reviews. The frequency of market intelligence acquisition from these sources is measured using 5-point Likert scale ranging from 0 - very low to 4 - very high.

## Analysis and results

Means, standard deviations, minimum, maximum and the ranking for the use of Web 2.0 applications in market intelligence acquisition are shown in Table 2; correlation coefficients between variables, means, standard deviations and reliability and validity tests in Table 3; and Table 4 presents factor loadings and cross-loadings. Wikis such as Wikipedia and Wikimedia are ranked the top among all Web 2.0 applications where sales and marketing personnel acquire market intelligence, followed by online consumer reviews and personal blogs. The next important sources of market intelligence are Google Alert/Yahoo Alert, personal social networks, corporate blogs, and forums/discussion groups. All the above applications received a high mean value of greater than 3 on the scale of 0-4\. On the other hand, five applications receive a low mean value of less than 1, including professional social networks, mailing lists/RSS/news groups, webinars, videocasts/vodcasts, and podcasts. On average, the respondents perceived environmental turbulence highly (M=4.13), conducted market intelligence acquisition in a moderate to high manner (M=3.94) and demonstrated moderately high firm performance (M=3.98).

<table><caption>Table 2: Frequency of market intelligence acquisition from Web 2.0 applications</caption>

<tbody>

<tr>

<th>Application</th>

<th>Min.</th>

<th>Max.</th>

<th>M</th>

<th>SD</th>

</tr>

<tr>

<td>Wikis</td>

<td>3</td>

<td>4</td>

<td>3.63</td>

<td>0.48</td>

</tr>

<tr>

<td>Online consumer reviews</td>

<td>3</td>

<td>4</td>

<td>3.57</td>

<td>0.50</td>

</tr>

<tr>

<td>Personal blogs</td>

<td>1</td>

<td>4</td>

<td>3.51</td>

<td>0.71</td>

</tr>

<tr>

<td>Google Alert/Yahoo Alert</td>

<td>2</td>

<td>4</td>

<td>3.44</td>

<td>0.71</td>

</tr>

<tr>

<td>Personal social networks</td>

<td>2</td>

<td>4</td>

<td>3.37</td>

<td>0.74</td>

</tr>

<tr>

<td>Corporate blogs</td>

<td>1</td>

<td>4</td>

<td>3.33</td>

<td>0.84</td>

</tr>

<tr>

<td>Forum/Discussion groups</td>

<td>0</td>

<td>4</td>

<td>3.18</td>

<td>1.04</td>

</tr>

<tr>

<td>Professional social networks</td>

<td>0</td>

<td>4</td>

<td>0.89</td>

<td>1.09</td>

</tr>

<tr>

<td>Mailing lists/RSS</td>

<td>0</td>

<td>4</td>

<td>0.83</td>

<td>1.27</td>

</tr>

<tr>

<td>Webinars</td>

<td>0</td>

<td>4</td>

<td>0.63</td>

<td>0.98</td>

</tr>

<tr>

<td>Videocasts/Vodcasts</td>

<td>0</td>

<td>1</td>

<td>0.35</td>

<td>0.48</td>

</tr>

<tr>

<td>Podcasts</td>

<td>0</td>

<td>1</td>

<td>0.26</td>

<td>0.44</td>

</tr>

</tbody>

</table>

_Note_. Scale ranges from 0 - very low to 4 - very high.

<table><caption>Table 3: Factor loadings and cross loadings</caption>

<tbody>

<tr>

<th>Item</th>

<th>Perceived Environmental Turbulence</th>

<th>Market Intelligence Acquisition</th>

<th>Firm Performance</th>

</tr>

<tr>

<td>PET1</td>

<td>

**0.90**</td>

<td>0.25</td>

<td>0.14</td>

</tr>

<tr>

<td>PET2</td>

<td>

**0.55**</td>

<td>0.10</td>

<td>0.10</td>

</tr>

<tr>

<td>PET3</td>

<td>

**0.62**</td>

<td>0.10</td>

<td>0.16</td>

</tr>

<tr>

<td>MIA1</td>

<td>0.16</td>

<td>

**0.74**</td>

<td>0.19</td>

</tr>

<tr>

<td>MIA2</td>

<td>0.21</td>

<td>

**0.71**</td>

<td>0.12</td>

</tr>

<tr>

<td>MIA3</td>

<td>0.18</td>

<td>

**0.73**</td>

<td>0.20</td>

</tr>

<tr>

<td>MIA4</td>

<td>0.20</td>

<td>

**0.76**</td>

<td>0.12</td>

</tr>

<tr>

<td>MIA5</td>

<td>0.16</td>

<td>

**0.76**</td>

<td>0.18</td>

</tr>

<tr>

<td>FP1</td>

<td>0.04</td>

<td>0.13</td>

<td>

**0.67**</td>

</tr>

<tr>

<td>FP2</td>

<td>0.11</td>

<td>0.12</td>

<td>

**0.71**</td>

</tr>

<tr>

<td>FP3</td>

<td>0.15</td>

<td>0.17</td>

<td>

**0.74**</td>

</tr>

<tr>

<td>FP4</td>

<td>0.18</td>

<td>0.18</td>

<td>

**0.73**</td>

</tr>

</tbody>

</table>

<table><caption>Table 4: Correlation matrix</caption>

<tbody>

<tr>

<th>Variable</th>

<th>M</th>

<th>SD</th>

<th>CR</th>

<th>AVE</th>

<th>1.</th>

<th>2.</th>

<th>3.</th>

</tr>

<tr>

<td>1. Perceived environmental turbulence</td>

<td>4.13</td>

<td>0.53</td>

<td>0.74</td>

<td>0.499</td>

<td>0.706</td>

<td></td>

<td></td>

</tr>

<tr>

<td>2. Market intelligence acquisition</td>

<td>3.94</td>

<td>0.61</td>

<td>0.86</td>

<td>0.548</td>

<td>0.244</td>

<td>0.740</td>

<td></td>

</tr>

<tr>

<td>3. Firm performance</td>

<td>3.98</td>

<td>0.57</td>

<td>0.81</td>

<td>0.513</td>

<td>0.182</td>

<td>0.219</td>

<td>0.716</td>

</tr>

</tbody>

</table>

_Note_. CR - composite reliability; AVE - average variance extracted; diagonal values are squared roots of AVE; all correlation coefficients are significant at _p_ <0.05\.

Partial least squares path modelling (PLS-PM) was used to test the two hypotheses of the study. PLS-PM is also called the component-based structural equation modelling or the soft modelling which places less minimal restrictions on measurement scales, sample size and residual distribution ([Chin _et al._ 1998](#chi98)). In addition, this study serves as a preliminary examination of the hypothesised relationships which suits the exploratory research contexts of PLS-PM ([Chin 2000](#chi00)). The analysis of PLS-PM consists of two stages: (1) the assessment of the measurement model, involving the reliability, convergent and discriminate validity of the items, and (2) the assessment of the structural model, involving the magnitude, direction and significance of the hypothesised relationships.

To ensure the reliability of all items in this study, composite reliability test is applied to all measures. As shown in Table 4, all the items exhibit acceptable reliability scores that exceed 0.70 ([Nunnally 1978](#nun78)). Besides, convergent validity and discriminant validity test are also conducted. To test the convergent validity, factor loadings of all items are presented in Table 3 and they all exceed 0.50, indicating adequate convergent validity. As for the discriminant validity, the square root of average variance extracted (AVE) for each variable is obtained and compared it with the correlation with other variables. As shown in Table 4, all square roots of AVE of each variable are higher than the correlation with other variables, indicating sufficient discriminant validity for the variables. At the item level, the factor loadings of each variable are used to compare with the cross factor loadings. As presented in Table 3, the analysis shows that the loading of each item is higher than its cross-loadings, signifying adequate discriminant validity at the item level.

Hypothesis tests reveal that both H1 and H2 are supported. The relationship between perceived environmental turbulence and marketing intelligence acquisition is positive and significant (_B_ = 0.244; _t_ = 3.714; _p_ <0.001). This finding is consistent with prior studies which perception of environment uncertainty is positively related to information processing activities. ([Daft _et al._ 1988](#daf88); [Sawyerr 1993](#saw93); [Lozada and Calantone 1996](#loz96); [Ebrahimi 2000](#ebr00); [May _et al._ 2000](#may00); [Ngamkroeckjoti and Speece 2008](#nga08)). However, the effect size and variance explained in this study is lower than that of prior studies. This can be explained by the fact that there exist other important determinants to information processing activities. In addition, perceived environmental turbulence per se could not explain well the information processing activities especially when most players in the industry perceived the similar level of environment turbulence (M = 4.13; SD = 0.53). The relationship between market intelligence acquisition and firm performance is also positive and significant (_B_ = .219; _t_ = 3.411; _p_ <0.001). The finding is consistent with the prior studies which found that information processing activities are positively related to firm performance ([Daft _et al._ 1988](#daf88); [Miller 1989](#mil89); [Jaworski and Wee 1992](#jaw92); [Cappel and Boone 1995](#cap95); [Beal 2000](#bea00); [Analoui and Karami 2002](#ana02); [Yap and Md Zabid 2011](#yap11)). Similar to the first hypothesis, the effect size and variance explained is lower than prior studies. It is commonly accepted that firm performance is contingent on many contextual factors which vary across industry sectors.

## Conclusion

The preliminary findings of this study offer managerial implications to players in the tourism industry, specifically the sales and marketing personnel in Malaysian hotels. The findings indicate that perception of environmental turbulence influences the acquisition of market intelligence, which then influences firm performance. The Web 2.0 applications frequently used as the source for acquiring market intelligence include wikis, online consumer reviews, personal blogs, alerts, personal social networks, corporate blogs, and forums/discussion groups. This study emphasised the need of marketing personnel in hotels to capitalise on the Web 2.0 applications in acquiring market intelligence to support business decision making and strategic planning. The findings also further confirm and extend the previous empirical studies about the positive relationship between market intelligence practices and firm performance.

There are several limitations in this study. First, the study collected only the acquisition process of market intelligence practices which is unable to capture the complete domain of market intelligence practices. Second, the variance explained in the analysis is considered low as it only focused on one antecedent to market intelligence acquisition. Third, even though the study attempted to include a representative sample in terms of hotel star rating and location, it was done using a convenience sampling which limits the generalisability of the findings across the hotel industry in Malaysia. Forth, the study adopted the perceptual measures of firm performance and limited to only the customer relationship perspective.

Future studies may include other activities of market intelligence practices such as dissemination, sharing, and use of market intelligence in specific decision making in order to better understand the concept of market intelligence practices. Besides, future research may examine the use of Web 2.0 applications in acquiring specific market intelligence and relate it to a particular marketing decision making. More antecedents to market intelligence practices can be identified and tested in future studies, including IT infrastructure, organisational setting and specific environmental variables. Objective firm performance in terms of financial measures such as profitability, return on investment and revenue growth may be examined in future research. The similar study may be extended to other players in the tourism industry such as tour operators or other industry sectors such as pharmaceutical and financial services where the use of market intelligence is prevalent.

To conclude, this study suggested that the use of Web 2.0 applications is not limited to providing product information to customers, but also serving as the tools for hotels to acquire market intelligence about customers' needs and preferences. It was evident in seven out of the twelve Web 2.0 applications tested in the study. This study also confirmed the interrelationships between market intelligence practices, perceived environmental turbulence and firm performance among hotels in Malaysia.

## Acknowledgements

This research is funded by Fundamental Research Grant Scheme, Ministry of Education, Malaysia (FRGS/1/11/SS/UNITAR/03/2).

## <a id="authors"></a>About the authors

**Ching Seng Yap** is an assistant professor in the Graduate School of Business, Universiti Tun Abdul Razak, Malaysia. He received his Bachelor's degree in Agribusiness and Master of Information Technology from Universiti Putra Malaysia, and his PhD in Management from Universiti Tun Abdul Razak, Malaysia. He can be contacted at:[chingseng@unirazak.edu.my](mailto:chingseng@unirazak.edu.my)  
**Boon Liat Cheng** is a senior lecturer in Sunway University Business School, Sunway University, Malaysia. He received his Bachelor of Marketing from Coventry University, England, his Master's in Marketing from The Robert Gordon University, Scotland, and his PhD in Marketing from Universiti Tun Abdul Razak, Malaysia. He can be contacted at: [boonliatc@sunway.edu.my](mailto:boonliatc@sunway.edu.my)  
**Kum Lung Choe** is a senior lecturer in the Faculty of Business and Finance, Universiti Tunku Abdul Rahman, Malaysia. He received his Bachelor of Arts in Business Administration from University of Western Ontario, Canada and his Master of Business Administration from University of Manchester, England. He can be contacted at: [choekl@utar.edu.my](mailto:choekl@utar.edu.my)

</section>

<section>

## References

*   Adidam, P. T., Banerjee, M. & Shukla, P. (2012). Competitive intelligence and firm's performance in emerging markets: an exploratory study in India. _Journal of Business & Industrial Marketing, 27_(3), 242-254.
*   Aguilar, F. J. (1967). _Scanning the business environment_. New York: Macmillan.
*   Analoui, F. & Karami, A. (2002). How chief executives' perception of the environment impacts on company performance. _Journal of Management Development, 21_(4), 290-305.
*   Beal, R. M. (2000). Competing effectively: environmental scanning, competitive strategy, and organizational performance in small manufacturing firms. _Journal of Small Business Management, 38_, 27-47.
*   Berkman, R. (2008). _The art of strategic listening: finding market intelligence through blogs and other social media_. Ithaca, NY: Paramount Market Publishing.
*   Berthon, P. R., Pitt, L. F., Plangger, K. & Shapiro, D. (2012). Marketing meets Web 2.0, social media, and creative consumers: implications for international marketing strategy. _Business Horizon, 55_(3), 261-271.
*   Booms, B. H. & Bitner, M. J. (1981). Marketing strategies and organizational structures for service firms. In J. H. Donnelly and W. R. George (Eds.). _Marketing of Services_ (pp. 47-51). Chicago: American Marketing Association.
*   Bourgeois, L. J. (1980). Strategy and environment: A conceptual integration. _Academy of Management Review, 5_, 25-39.
*   Brockman, B. K. & Morgan, R. M. (2003). The role of existing knowledge in new product innovativeness and performance. _Decision Sciences, 34_(2), 385-419.
*   Bughin, J. & Chui, M. (2010). The rise of the networked enterprise: Web 2.0 finds its payday. _McKinsey Quarterly, 2010_(4).
*   Cappel, J. J. & Boone, J. P. (1995). A look at the link between competitive intelligence and performance. _Competitive Intelligence Review, 6_(2), 15-23.
*   Chan, N. L. & Guillet, B. D. (2011). Investigation of social media marketing: how does the hotel industry in Hong Kong perform in marketing on social media websites? _Journal of Travel & Tourism Marketing, 28_(4), 345-368.
*   Chen, Y. & Xie, J. (2008). Online consumer review: word-of-mouth as a new element of marketing communication mix. _Management Science, 54_, 477-491.
*   Chevalier, J. & Mayzlin, D. (2006). The effect of word of mouth on sales: online book reviews. _Journal of Marketing Research, 43_, 345-354.
*   Chin, W. W. (2000). Partial least squares for IS researchers: an overview and presentation of recent advances using the PLS approach. In _Proceedings of the International Conference on Information Systems, 2000_. (Paper 88). Atlanta, GA: Association for Information Systems.
*   Chin, W. W. (1998). The partial least squares approach for structural equation modelling. In G. A. Marcoulides (Ed.). _Modern methods for business research: methodology for business and management_ (pp. 295-336). Mahwah, NJ: Lawrence Erlbaum.
*   Choo, C. W. (2001). Environmental scanning as information seeking and organizational learning. _Information Research, 7_(1), paper 112\. Retrieved from [http://www.informationr.net/ir/7-1/paper112.html](http://www.informationr.net/ir/7-1/paper112.html)
*   Conant, J. S. & White, J. C. (2000). Marketing program planning, process benefits, and store performance: an initial study among small retail firms. _Journal of Retailing, 75_(4), 525-541.
*   Culnan, M. J., McHugh, P. J. & Zubillaga, J. I. (2010). How large U.S. companies can use Twitter and other social media to gain business value. _MIS Quarterly Executive, 9_(4), 243-259.
*   Daft, R. L., Sormunen, J. & Parks, D. (1988). Chief executive scanning, environmental characteristics and company performance: an empirical study. _Strategic Management Journal, 9_, 123-139\.
*   Daft, R. L. & Weick, K. E. (1984).Toward a model of organizations as interpretation systems. _Academy of Management Review, 9_, 284-295.
*   Diamantopoulos, A. & Souchon, A. L. (1999). Measuring export information use: scale development and validation. _Journal of Business Research, 46_(1), 1-14.
*   Droge, C., Calantone, R. & Harmancioglu, N. (2008). New product success: is it really controlled by managers in high turbulence environments? _Journal of Product Innovation Management, 25_, 272-286.
*   Duncan, R. B. (1972). Characteristics of organizational environments and perceived environmental uncertainty. _Administrative Science Quarterly, 17_(3), 313-327.
*   Ebrahimi, B. P. (2000). Perceived strategic uncertainty and environmental scanning behaviour of Hong Kong executives. _Journal of Business Research, 49_(1), 67-77.
*   Elliot, S. (2006). [Nowadays, it is all yours, mine and ours](http://www.nytimes.com/2006/05/02/business/media/02adco.html?_r=0). Retrieved from http://www.nytimes.com/2006/05/02/business/media/02adco.html?_r=0
*   Fahey, L., King, W. R. & Narayanan, V. R. (1981). Environmental scanning and forecasting in strategic planning - the state of the art. _Long Range Planning, 14_, 32-39.
*   Fahey, L. & Narayanan, V. K. (1986). _Macroenvironmental analysis for strategic management_. St Paul, MN: West Publishing.
*   Global Intelligence Alliance (2011). Market intelligence in global organizations: survey findings in 2011\. _GIA White Paper 2/2011_.
*   Griffin, A. & Page, A. L. (1993). An interim report of measuring product development success and failure. _Journal of Product Innovation Management, 10_(4), 291-308.
*   Hamill, J., Attard, D. & Stevenson, A. (2009). National destination marketing organisations and Web 2.0\. _Market and Competitiveness, 1_, 71-94.
*   Harris, L. & Rae, A. (2009). Social networks: the future of marketing for small business. _Journal of Business Strategy, 30_(5), 24-31.
*   Jaworski, N. & Wee, L. C. (1992/1993). Competitive intelligence and bottom-line performance. _Competitive Intelligence Review, 3_(3), 23-27.
*   Jogaratnam, G. & Law, R. (2006). Environmental scanning and information source utilization: exploring the behaviour of Hong Kong hotel and tourism executives. _Journal of Hospitality & Tourism Research, 30_, 170-190.
*   Jogaratnam, G. & Wong, K. K. F. (2009). Environmental uncertainty and scanning behavior: an assessment of top-level hotel executives. _International Journal of Hospitality & Tourism Administration, 10_, 44-67.
*   Keh, H. T., Nguyen, T. T. M. & Ng, H. P. (2007).The effects of entrepreneurial orientation and marketing information on the performance of SMEs. _Journal of Business Venturing, 22_(4), 592-611.
*   Kohli, A. J. & Jaworski, B. J. (1990). Market orientation: The construct, research propositions, and managerial implications. _Journal of Marketing, 54_, 1-18.
*   Kotler, P. (1976). Marketing management: analysis, planning, and control. New Jersey: Prentice Hall.
*   Li, C. & Bernoff, J. (2008). _Groundswell: winning in a world transformed by social technologies_. Boston: Harvard Business School Press.
*   Lybaert, N. (1998). The information use in a SME: its importance and some elements of influence. _Small Business Economics, 10_(2), 171-191.
*   Lozada, H. R. & Calantone, R. J. (1996). Scanning behaviour and environmental variation in the formulation of strategic responses to change. _Journal of Business & Industrial Marketing, 11_(1), 17-41.
*   May, R. C., Stewart, W. H. & Sweo, R. (2000). Environmental scanning behaviour in a transitional economy: Eevidence from Russia. _Academy of Management Journal, 43_(3), 403-427.
*   Milano, R., Baggio, R. & Piattelli, R. (2011). _[The effects of online social media on tourism websites](http://www.webcitation.org/6TgR0zWle)_. Paper presented at the 18th International Conference on Information Technology and Travel & Tourism, Innsbruck, Austria, January 26-28, 2011\. Retrieved from http://www.iby.it/turismo/papers/baggio_socialmedia.pdf (Archived by WebCite® at http://www.webcitation.org/6TgR0zWle)
*   Miller, D. (1989). Matching strategies and strategy making: process, content and performance. _Human Relations, 42_(3), 241-260.
*   Milwood, P., Marchiori, E. & Zach, F. (2013). A comparison of social media adoption and use in different countries: the case of the United States and Switzerland. _Journal of Travel & Tourism Marketing, 30_, 165-168.
*   Mishra, S., Kim, D. & Lee, D. H. (1996). Factors affecting new product success: cross-country comparisons. _Journal of Product Innovation Management, 13_, 530-550.
*   Moorman, C. (1995). Organizational market information processes: Cultural antecedents and new product outcomes. _Journal of Marketing Research, 32_(3), 318-335.
*   Moorman, C. & Rust, R. (1999). The role of marketing. _Journal of Marketing, 63_, 180-197\.
*   Ngamkroeckjoti, C. & Speece, M. (2008). Technology turbulence and environmental scanning in Thai food new product development. _Asia Pacific Journal of Marketing and Logistics, 20_(4), 413-432.
*   Nunally, J. (1978). _Psychometric methods_. New York: McGraw-Hill.
*   Oreja-Rodriguez, J. R. & Yanes-Estevez, V. (2007). Perceived environmental uncertainty in tourism: a new approach using the Rasch model. _Tourism Management, 28_(6), 1450-1463.
*   Ottum, B. D. & Moore, W. L. (1997). The role of market information in new product success/failure. _Journal of Product Innovation Management, 14_, 258-73.
*   Parry, M. E. and Song, M. (2010). Market information acquisition, use , and new venture performance. _Journal of Product Innovation Management, 27_(7), 1112-1126.
*   Priem, R. L., Love, L. G. & Shaffer, M. A. (2002). Executives' perceptions of uncertainty sources: a numerical taxonomy and underlying dimensions. _Journal of Management, 28_, 725-746.
*   Puhringer, S. & Taylor, A. (2008). A practitioner's report on blogs as a potential source of destination marketing intelligence. _Journal of Vacation Marketing, 14_(2), 177-187.
*   Rogers, P. R., Miller, A. & Judge, W. Q. (1999). Using information-processing theory to understand planning/performance relationship in the context of strategy. _Strategic Management Journal, 20_, 567-577.
*   Rouach, D. & Santi, P. (2001). Competitive intelligence adds value: five intelligence attitudes. _European Management Journal, 19_, 552-559.
*   Sawyerr, O. O. (1993). Environmental uncertainty and environmental scanning activities of Nigerian manufacturing executives: A comparative analysis. _Strategic Management Journal, 14_(14), 287-299.
*   Schaffer, N. (2010). Is your customer service ready for social media? The Virgin America Twitter campaign case study. Customer Think. Retrieved from [http://maximizesocialbusiness.com/is-your-customer-service-ready-for-social-media-virgin-america-twitter-campaign-case-study-2335/](http://maximizesocialbusiness.com/is-your-customer-service-ready-for-social-media-virgin-america-twitter-campaign-case-study-2335/)
*   Schegg, R., Liebrich, A., Scaglione M. & Syed Ahmad, S. F. (2008). An exploratory field study of Web 2.0 in tourism. _Information and Communications Technologies in Tourism 2008_, 152-163.
*   Sharifah Fatimah, S. A. & Murphy, J. (2010). Social networking as a marketing tool: the case of a small Australian company. _Journal of Hospitality Marketing & Management, 19_(7), 700-716.
*   Singh, T., Veron-Jackson, L. & Cullinane, J. (2008). Blogging: a new play in your marketing plan. _Business Horizon, 51_, 281-292\.
*   Soh, P. H. (2003). The role of networking alliances in information acquisition and its implications for new product performance. _Journal of Business Venturing, 18_(6), 727-744.
*   Song, M., Di Benedetto, A. & Parry, M. E. (2009). The impact of formal processes for market information acquisition and utilization on the performance of Chinese new ventures. _International Journal of Research in Marketing, 26_, 314-323.
*   Song, M., Tang, W. & Parry, M. E. (2010). Do market information processes improve new venture performance? _Journal of Business Venturing, 25_, 556-568.
*   Sotiriadis, M. D. & van Zyl, C. (2013). Electronic word-of-mouth and online reviews in tourism services: the case of twitter by tourists. _Electronic Commerce Research_, **13**, 103-124.
*   Vedder, R. G. & Guynes, C. S. (2000). A study of competitive intelligence practices in organizations. _The Journal of Computer Information Systems, 41_(2), 36-39.
*   Wang, C.-H., Chen, K.-Y. & Chen, S. C. (2012). Total quality management, market orientation and hotel performance: the moderating effects of external environmental factors. _International Journal of Hospitality Management, 31_, 119-129\.
*   Yap, C. S. & Md Zabid, A. R. (2011). Competitive intelligence practices and firm performance. _Libri, 61_(3), 175-189.
*   Ye, Q, Law, R. & Gu, B. (2009). The impact of online user review on hotel room sales. _International Journal of Hospitality Management, 28_, 180-182.
*   Zhang, X., Majid, S. & Foo, S. (2013). Environmental scanning practices of travel agent companies in Singapore. _Asia Pacific Journal of Tourism Research, 18_(8), 823-848.

</section>

</article>