<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# Information literacy in higher education - empowerment or reproduction? A discourse analysis approach

#### [Geoff Walton](#author)  
Department: Mathematics and Information Sciences, Northumbria University, Newcastle-upon-Tyne, NE1 8ST, UK

#### [Jamie Cleland](#author)  
Department of Social Sciences, Loughborough University. Leicestershire, LE11 3TU, UK

#### Abstract

> **Introduction.** This paper presents a qualitative investigation into whether online discourse, produced by first year undergraduate students, can demonstrate their information capabilities as a socially-enacted practice and be used to assess their information literacy.  
> **Method.** Discourse analysis was used to categorise online discussion board postings produced by first-year UK undergraduate students as part of a formative online peer assessment exercise.  
> **Analysis.** Online discourse was the _node_ of analysis which sought to identify patterns of language within the social and cultural contexts in which they occurred. Postings were inductively analysed through manual content analysis.  
> **Results.** Postings appeared to embody student's discursive competence in information literacy, especially their level of information discernment and what constituted a quality 'reference' for an assignment. However, they also demonstrated that 'references' perform a certain function within this discourse as an agreed construct between tutor, student and librarian.  
> **Conclusions.** Students were engaged in the process of becoming good scholars by using appropriate online postings to create valid arguments through assessing other's work, but what they did not do was question received meanings regarding the quality of found information they used as evidence.

## Literature review

It is argued that information literacy capabilities are context based and social in nature ([Hepworth & Walton, 2009](#Hep09)). Walton & Hepworth ([2013](#Wal13), p55) have further articulated the subset of information literacy capability known as information discernment which they define as, '_the ability to use higher order thinking skills in order to make sound and complex judgements regarding a range of text-based materials_'. Five discrete levels of this ability are identified, from the lowest ('cut and paste') to the highest (where individuals can express the relative value of evaluation criteria for a given source of information). This theory overlaps with Ford's notion of 'relevance judgements' found in the information seeking process ([Ford, 2004](#For04), p203).

It is suggested that discourse analysis appears to be a useful methodology for analysing information literacy capabilities, yet it does not feature heavily in relation to information literacy research and practice with the notable exception of some recent studies (see Lloyd ([2012](#Llo12)). However, it has been used to a greater extent within information behaviour research ([Case, 2012](#Cas12); [Bates, 2005](#Bat05), [Given, 2005](#Giv05); [Tuominen _et al_., 2005](#Tuo05)) which is argued provides an evidence base for information literacy ([Hepworth & Walton, 2013](#Hep13)). Discourse is regarded as more than simply talking or writing, it is a complex network of relationships between individuals, texts, ideas and institutions ([Foucault, 1972](#Fou72): [van Leeuwen, 2005](#van05)). It is from these relationships that sharing of meaning arises at specific points (_a node_) in space and time ([Olsson, 2010](#Ols10)). Therefore, individual information behaviour is not an isolated activity, it is this discursive element which locates the person-in-context ([Wilson, 1999](#Wil99)).

## Methodology

This research seeks to address the following questions:

*   RQ1: To what extent does online discourse reveal students' information literacy capabilities (as a socio-cultural practice)?
*   RQ2: In what ways could this online discourse be used as a basis for summative assessment of their work?
*   RQ3: in what ways does online peer assessment embody complex and asymmetrical power relations between tutors, students and librarians?
*   RQ4: how is academic discourse rehearsed, negotiated, reproduced and its meaning shared through online discourse?

### Discourse analysis as a method

The node for analysis in this study is the online postings made by first-year undergraduate student participants during the online peer assessment activities within a particular module. The socio-historical context of this study is a UK Higher Education institution from 2007-2012\. However, it is noted that information literacy is relevant beyond this specific context. Data was themed into categories to identify 'patterns and processes, commonalities and differences' across the student cohort ([following Miles and Huberman, 1994](#Mil94), p.9).

## Results

Online postings sent to Blackboard's Discussion Board centred on the notion of 'references' - artefacts of found information such as a book, journal article, website or newspaper article used in the student author's draft assignments. Below are typical pieces of online discourse posted by students (commentators) commenting on fellow student's draft essays (authors):

> SS11: 'It is effective how you focus on one particular idea and you use good examples to back your arguments up…. Overall referncing (sic) was done well, however make sure every key point made is backed up with a reference. Also try to use academic references instead of autobiographies.'  
> SB11: 'you have included a number of points to why sport affects society instead of focusing on one area and used references well, the first paragraph has over 5 references that all support your point […] which proves there is a lot of evidence for your argument….'  
> SF11: 'A very good use of references as well to help back up the points you are making about certain health issues….'

These extracts appear to embody the student commentator's discursive competence in information literacy, especially in terms of information discernment, in showing that they have made relevance judgements in evaluating academic texts (corroborating Ford, [2004](#For04); Walton & Hepworth, [2013](#Wal13) and Lloyd, [2012](#Llo12)). Particularly, for example, what constitutes a quality reference ('academic' as opposed to 'autobiographies'), that there should be a number of references ('5 reference') and that these should be used specifically to 'make sure every key point is backed up by a reference' corroborating Walton & Hepworth ([2013](#Wal13)) and addressing RQ1\. This also appears to offer promising output for assessment purposes in that commentators demonstrate their information discernment capability within the discourse (answering RQ2).

All agree on the subject of the references and what function they should perform in the author's assignment, that they should be present and should be used to 'back up' each 'key point' or 'argument'. Hence, references perform a certain function within the discourse, not just to support an argument, but as an agreed construct between tutor, student and librarian (and therefore within Western scholarship generally) - addressing RQ3\. In this sense, commentators are exercising their power inductively rather than coercively ([Foucault, 1972](#Fou72)). The commentators have become 'authoritative speakers' where the discourse posted, for example, 'try and use academic references' is accepted as a 'truth statement' by their community (the student participants). This is not to say that the commentator is making a statement of truth, it is only deemed true by the discursive network, i.e., students agree that it is necessary to write an essay that will get a good grade (addressing RQ4).

## Conclusion

Online peer assessment appears to be a useful way for evidencing information literacy capabilities as a socio-cultural practice by revealing relevance judgements and information discernment within the discourse itself (addressing RQ 1). The online contributions made by students as commentators are contextually appropriate by embodying attributes of information literacy capability, demonstrating discursive competence in evaluating information which may lend themselves to summative assessment, which partly addresses RQ 2\. However, they are operating within a well-structured discourse which reproduce structures that already exist and their questioning is bounded and finite - addressing RQ3 and 4\. In other words, the outcome of information literacy is already decided for the student and s/he merely has to follow the rules of the game and is subservient to more powerful discourses. Critical thinking is only engaged in a very narrow range.

<section>

## References

<ul> 
<li id="Bat05">Bates, M. J. (2005). <em>An introduction to metatheories, theories and models</em>. In Fisher, K. E., Erdelez, S. & McKechie, L. E. F. (eds.) (2005).  <em>Theories of information behavior</em>.  Assist Monograph Series.  Medford: Information Today, Inc. pp1-24.</li>
<li id="Cas12">Case, D. O. (2012).  Looking for information. <em>A survey of research on information seeking, needs and behavior</em>. (3rd edn.).  Bingley: Emerald Group.</li>
<li id="For04">Ford, N. (2004). <em>Towards a model of learning for educational informatics.  Journal of Documentation, 60</em>(2), pp183-225.</li>
<li id="Fou72">Foucault, M. (1972). <em>Archeology of Knowledge</em>. London: Tavistock.</li>
<li id="Giv05">Given, L. M. (2005). Social positioning. In Fisher, K. E., Erdelez, S. & McKechie, L. E. F. (eds.) (2005). <em>Theories of information behavior</em>.  Assist Monograph Series. Medford: Information Today, Inc. pp334-338.</li>
<li id="Hep09">Hepworth, M. & Walton, G. (2009). <em>Teaching information literacy for inquiry-based learning</em>. Oxford: Chandos.</li>
<li id="Hep13">Hepworth, M. & Walton, G. (eds) (2013).  Introduction - information literacy and information behaviour, complementary approaches for building capability.  In Hepworth, M. & Walton, G. (eds) <em>Developing people's information capabilities: fostering information literacy in educational, workplace and community contexts</em>. Bingley: Emerald Group Publishing Ltd.</li>
<li id="Llo12">Lloyd, A. (2012). Information literacy as a socially enacted practice: Sensitising themes for an emerging perspective of people-in-practice. <em>Journal of Documentation, 68</em>(6), pp772-783  [Online]  http://www.emeraldinsight.com/journals.htm?articleid=17062472 (accessed 15 August 2013).</li>
<li id="Mil94">Miles, M. B. & Huberman, A.M. (1994). <em>Qualitative data analysis: an expanded sourcebook. </em>(2nd edn.). Thousand Oakes: Sage.</li>
<li id="Ols10">Olsson, M. (2010). <em>Michel Foucault: discourse, power/knowledge and the battle for truth. In Leckie, G. J., Given, L. M. and Buschamen, J. (eds) Critical theory for library and information science: exploring the social from across the disciplines</em>.  Santa Barbara: Libraries Unlimited, pp63-74.</li>
<li id="Tuo05">Tuominen, K., Talja, S. & Savolainen, R. (2005). The social constructionist viewpoint of information practices. In Fisher, K. E., Erdelez, S. & McKechie, L. E. F. (eds.) (2005). <em>Theories of information behavior</em>. Assist Monograph Series.  Medford: Information Today, Inc. pp328-333.</li>
<li id="van05">van Leeuwen, T. (2005). <em>Introducing social semiotics</em>. New York: Routledge.</li>
<li id="Wal13">Walton, G. & Hepworth, M. (2013). Using assignment data to analyse a blended information literacy intervention: a quantitative approach. <em>Journal of Librarianship and Information Science, 45</em>(1) pp53-63</li>
<li id="Wil99">Wilson, T. D. (1999). Models in information behaviour research. <em>Journal of Documentation, 55</em>(3), pp249-270.</li>
</ul>

</section>

</article>