<header>

#### vol. 19 no. 4, December, 2014

</header>

<article>

# Information literacy standards and the World Wide Web: results from a student survey on evaluation of Internet information sources

#### [Arthur Taylor](#author)  
Information Systems and Supply Chain Management, College of Business Administration, Rider University, Lawrenceville, New Jersey, USA  
#### [Heather A. Dalal](#author)  
Moore Library, Rider University, Lawrenceville, New Jersey, USA

#### Abstract

> **Introduction**. This paper aims to determine how appropriate information literacy instruction is for preparing students for these unmediated searches using commercial search engines and the Web.  
> **Method**. A survey was designed using the 2000 Association of College and Research Libraries literacy competency standards for higher education. Survey questions examined how subjects perceived the source evaluation criteria of reliability, validity, accuracy, authority, timeliness and point of view or bias.  
> **Analysis**. Quantitative analysis was carried out on the data collected from 389 survey respondents who answered twenty-seven multiple choice questions concerning their information search practice and their evaluations of information sources.  
> **Results**. Subjects primarily use Google as a research source for academic work and appear to be confused about how to determine the author of a source and how to determine the qualifications of the author. About half of the subjects indicated they may not be able to determine the author of an Internet source yet consider it possible to determine the objectivity of the source.  
> **Conclusions**. Information literacy instruction on source evaluation criteria may need to be reexamined in relation to the various information sources available today. More effective information literacy instruction methods which address the issues identified in this study may need to be implemented.

<section>

## Introduction

It is known that some significant portion of college research is done using commercial search engines on the Web. In this process, traditional gatekeepers of research have been replaced by search engine applications with unknown algorithms which return content of dubious quality. That college students today use the Internet for research is not unexpected. Most have had constant exposure to the Web and its vast sea of fragmented and unstructured information. Information literacy instruction attempts to address some of these issues. Librarians, however, often have difficulties scheduling any information literacy instruction and when they can perform the instruction, they have a limited amount of time to discuss evaluating sources.

Librarians and instructors strive to teach college students to evaluate sources of information for all their research projects. Critically evaluating information sources is a life skill and considered a core competency of information literacy highlighted by the Association of College and Research Libraries Information Literacy Competency Standard 3 ([Association of College and Research Libraries, 2000](#acl200)). The standards have given librarians the support needed to advocate for information literacy, but librarians with just a one-time guest instructional session can do little more than suggest students evaluate Web sources and highlight the criteria to use, often in a checklist form.

It is unclear how effective information literacy instruction has been in teaching students to specifically evaluate Web information sources. A better understanding of how students perceive information sources on the Web and how they evaluate the suitability of those sources for their research could help improve information literacy instruction.

The intention of the research reported here was to evaluate the student perceptions of Web information sources, specifically their evaluation of various properties of those information sources in relation to traditional information literacy instruction. The study investigated student understanding of the evaluative criteria recommended in the Association of College and Research Libraries Information Literacy Competency Standard Performance Indicator 3.2a concerning "reliability, validity, accuracy, authority, timeliness, and point of view or bias" of sources ([Association of College and Research Libraries, 2000](#acl200)). (See [Appendix B](#appb) for the Association of College and Research Libraries standards, performance indicator and outcomes that informed this research).

## Literature review

Traditionally, it has been the librarian's role to ensure students have access to quality material. While most librarians and faculty direct students to quality information sources, students easily bypass the traditional method and use familiar search engines. A gap exists between what sources professors expect students to cite in their academic research papers and the sources students actually use. Grimes and Boening ([2001](#grimes-2001)) identify two reasons why the gap might exist. In their study they report that faculty provided instruction which directed students to use reliable resources. Results of their study indicated that faculty felt their advice on evaluating information sources was ignored, and students indicated that they did not know what was meant by a _reliable_ resource. More confusion exists about what is acceptable to the instructor versus what librarians recommend, yet it is the librarians who are commonly tasked with ensuring students meet information literacy outcomes ([Weiner, 2014](#weiner-2014)).

Librarians lament when their information literacy efforts go to waste. Students show they have not retained knowledge when they cite unacceptable sources in their academic research papers. In attempts to improve information literacy instruction, librarians often get creative. Librarians might have an entire hour or just ten minutes to devote to teaching the evaluation of sources and strive to deliver effective instruction to leave students prepared to evaluate any information source they encounter. Evaluation of sources as an information literacy competency standard has become increasingly important as information on the Web continues to expand dramatically.

To address students' gaps in information literacy skills, many different approaches to Website evaluation have been taught by librarians. Librarians often teach to a checklist model with various acronyms and mnemonic devices such as RADCAB (relevancy, appropriateness, detail, currency, authority, bias), and CRAAP (currency, relevance, authority, accuracy, purpose) ([Christennson, 2013](#christennson-2013); [Blakeslee, 2004](#blakesleee-2004); [Grimes and Boening, 2001](#grimes-2001); [Shanahan, 2008](#shanahan-2008)). The goal of the checklists is to address the criteria defined in Association of College and Research Libraries information literacy competency performance outcome 3.2a, which stresses the importance of evaluating source research documents ([Meola, 2004](#meola-2004); [Metzger, 2007](#metzger-2007)). Librarians' information literacy instruction may also provide examples showcasing pre-selected Websites that might be good, bad, extremist or a hoax ([Mathson and Lorezen, 2008](#mathson-2008)). Other librarians' information literacy instruction may vary from the standard approach to information literacy instruction to teaching using various learning styles ([Dahlen, 2012](#dahlen-2012)), teaching to various stages in cognitive development ([Jackson, 2007](#jackson-2007); [Benjes-Small, Archer, Tucker, Vassady and Resor , 2013](#benjes-small-2013)), teaching using a contextual approach ([Meola, 2004](#meola-2004)) or a constructivist approach ([O'Connor _et al._, 2010](#oconnor-2010)), and engaging in new strategies such as active learning and redesigning learning spaces ([Detlor, Booker, Serenko and Julien, 2013](#detlor-2013); [Hsieh, Hofmann, Dawson and Titus, 2014](#hsieh-2014); [Julian, 2013](#julian-2013)).

Existing literature also discusses partnerships between librarians and faculty. Writing instructors and librarians partner in teaching writing information literacy as both have similar goals ([O'Connor _et al_., 2010](#oconnor-2010); [Norgaard, 2004](#norgard-2004)). There are many successful librarian and discipline faculty collaborations which can be found in the literature, for example, in engineering ([Wang, 2011](#wang-2011)), nursing ([Dee and Reynolds, 2013](#dee-2013)), business ([Atwong and Heichman, 2008](#atwong-2008)) and other domains ([Weiner, 2014](#weiner-2014)).

Ultimately it is the student who must apply the criteria and perform the evaluation of information sources. Studies however have reported a discrepancy between what students say they can do and how they actually perform. Students can confidently articulate what to do or what to look for in evaluating sources, but when they have to perform research, they either do not know how to do it, do not have the skills or just do not perform the work needed to evaluate sources correctly. Thus, the criteria students say they will use are not the same criteria they actually use ([Currie _et al_., 2010](#currie-2010); [Grimes and Boening, 2001](#grimes-2001); [Walraven,](#walraven-2009) [Brand-Gruwel and Boshuizen, 2009](#walraven-2009)). A part of this discrepancy could be linked to students not really grasping how to apply the evaluative criteria or not understanding the need to apply the criteria while examining sources.

Other problems in the use of evaluative criteria are also evident. In Grimes and Boening ([2001](#grimes-2001)) some students assigned credibility based on how a page looked and students ranked the author as the most important feature of a quality Website yet failed to consider the author's qualifications (_ibid_, p. 18). Studies have also provided some indication that students claim to evaluate their sources much more than they do in reality ([Flanagin and Metzger, 2008](#flanagin-2008); [Gross and Latham, 2011](#gro11); [Metzger, Flanagin and Zwarun, 2003](#metzger-2003)). Gross and Latham ([2011](#gro11)) recognise that this is part of a greater pedagogical problem in which students often believe they have skills to find, evaluate and use information, but teaching students to be more self-reflective of their need to learn without damaging their self-efficacy is challenging.

Previous research has also provided some indication that college-age searchers tend to skim sources and use just one or two pages of content to make a determination about the quality or suitability of a potential research source ([Rowlands _et al_., 2008](#rowlands-2008)). Students also admit to selecting a source because their search keywords were in the title or in one of the lead paragraphs of a document ([Currie _et al_., 2010](#currie-2010)). When probed, students disclose that their focus is on finding an answer and that the source of the information is of little concern ([Van Deursen and Van Diepen, 2013](#van-deursen-2013)). Research suggests that this light review of content may be related to disinterest or lack of concern for the evaluation of various characteristics of content among college-age searchers and Internet users in general ([Williams and Rowlands, 2007](#williams-2007); [Hirsh, 1999](#hirsh-1999); [Grimes and Boening, 2001](#grimes-2001); [Lorenzen, 2001](#lorenzen-2001); [Carr, 2010](#carr-2010)). It may be that in viewing the search process as just another task, a consumerist approach may prevail whereby searchers simply view information as just another commodity to be consumed at the lowest possible cost, identifying cost in this case as ease-of-access with limited consideration for quality ([Buczynski, 2005](#buczynski-2005); [Thompson, 2003](#thompson-2003); [Young and Von Segern, 2001](#young-2001)).

Some research has also focused specifically on the Web as a research source. Studies have suggested that students often prefer Google as their first choice for research ([Head and Eisenberg, 2010](#head-2010)) and that Web materials are easier for students to find ([Biddix, Chung and Park, 2011](#biddix-20111); [Currie, Devlin, Emde and Graves, 2010](#currie-2010)). Studies have also examined how students evaluate documents found through various information retrieval sources. In Taylor (2012), subjects appeared to have limited interest in source quality and appeared to procrastinate in their effort to find information as the deadline for the research project approached.

Other studies have identified additional information literacy problems specific to Web sources. Thirty-eight percent of students in a study reported by Tillotson ([2002](#tillotson-2002)) did not recognise the need for evaluating resources, indicating that they rarely find incorrect information on the Web. Students are aware that not all information on the Web is reliable but appear to forget this while searching. Students report trusting institutional Web pages, Web pages free from spelling errors, Web pages with a list of good references, a professional layout, easy navigation, and no commercial purpose and/or a lot of detail ([Lorenzen, 2001](#lorenzen-2001); [Hilligoss and Rieh, 2008](#hilligoss-2008); [Head and Eisenberg, 2010](#head-2010)).

To understand how students evaluate sources, a clearer understanding of the criteria they use to evaluate those sources is needed. In a study conducted by Project Information Literacy, students rated how often evaluative criteria (currency, author's credentials and different viewpoints expressed) affected their selection of research sources. Questions on the survey instrument used inquired about which parts of the course-related research process were difficult. The survey included question categories for filtering irrelevant results, 'determining credibility', and 'evaluating sources', but questions did not specifically enquire about difficulties in determining page authorship or other evaluative criteria ([Head and Eisenberg, 2010](#head-2010), p.25). Other surveys have examined social cues, social networks and students' use of instructors, classmates, friends/family, librarians and others for help with evaluation of information sources, but specific criteria used for evaluations were not examined ([Metzger, Flanagin and Medders, 2010](#metzger-2010); [Head and Eisenberg, 2010](#head-2010), pp. 13, 40).

In order the better to teach information literacy, librarians need to improve their understanding of students' decision processes when evaluating and selecting a source for their research assignments. If, as some studies suggest, the Web has become a primary source of information for student researchers, then a better understanding of how students specifically evaluate Web information sources is needed. Previous research has not specifically examined the criteria for source evaluation, such as source authorship, authority, accuracy, credibility, objectivity and quality, by the student information seeker. Research which examines these aspects of site evaluation for Web sources is needed. Such research will help clarify educators' understanding of these aspects of student information gathering and provide suggestions for improving information literacy instruction.

## Research design

To understand better how searchers evaluate information sources and to examine this evaluation process in relation to current information literacy instructional strategies, the following research questions were examined in this study.

In relation to the source evaluation criteria identified by the information literacy competency standards (reliability, validity, accuracy, authority, timeliness and point of view or bias):

*   Do searchers consider it possible to determine these criteria for documents being evaluated?
*   How do searchers attempt to evaluate these criteria when evaluating information sources?

To gather information on the evaluation of sources during the information search process, a survey instrument was developed (see [Appendix A](#appa)). The survey questions were based on the Association of College and Research Libraries information literacy competency standards for higher education published in 2000\. Questions were based primarily on standard three from this publication which indicates that '_the information literate student evaluates information and its sources critically and incorporates selected information into his or her knowledge base and value system_' (Association of College and Research Libraries, 2000). A small number of questions were also used to gather demographic and other information about the subject. (See [Appendix B](#appb) for a specific description of how questions were mapped to the Association of College and Research Libraries information literacy competency standards.)

The survey instrument was designed to be delivered online, through the Web. Students were not monitored as they completed the survey and were allowed to take the survey at their convenience. Subjects took the survey before receiving information literacy instruction from library faculty at the university. Survey results were stored anonymously in a database for later analysis.

Subjects taking the survey were presented with a screen which indicated that the research was voluntary and that they could stop at any time. They were then presented with instructions on how to complete the survey (see Figure 1). Subjects were directed to answer questions about how they conducted and evaluated sources of information for their college research across all college courses.

Most subjects completed the twenty-seven questions in the survey in between five and ten minutes. The data collected was anonymous. Subjects were not directly compensated for participation, but some subjects were allowed to optionally be entered into a pool to win a $50 gift certificate.

<figure>

[![Survey System](../p645fig1tn.png)](../p645fig1.png)

<figcaption>

[Figure 1: Online survey instrument](../p645fig1.png)</figcaption>

</figure>

The survey taken by the subjects contained twenty-seven questions (see Appendix A). (Note that question twenty-eight on the survey allowed subjects to optionally enter their email to be entered in a pool for a gift certificate and therefore was not analysed.) Respondents provided demographic and background information for questions one to eight. Respondents provided answers relating to how they searched for information and evaluated information sources for questions nine to twenty-seven. Survey questions nine to twenty-seven were related to Association of College and Research Libraries information literacy standards (2000) as detailed in Appendix B.

## Results

The 389 subjects who participated were undergraduate students at an American university. Most were business majors (38%) with the remainder being a mix of liberal arts, education and communication majors. Most subjects were female (66%). Most subjects indicated they had a grade point average (GPA) of greater than 3.0 (74%).

(Note that for all survey questions presented in the tables that follow, the percentage column is calculated as the number of respondents who selected the item divided by the total number of respondents for the survey. Using this approach for single select questions, the sum of the percentages for a question response will be equal to or close to 100% with some deviations due to rounding errors. Using this same approach for multiple select questions, the sum of the percentages for a question response will not total 100% for a question.)

### Information search habits

When asked about their search habits, approximately 90% of the subjects indicated they searched for information on the Web several times a day. Subjects also indicated they were most likely to search using Google, but suggested that other search engines and tools were also used in their research (see Table 1). Twenty-five percent of the subjects indicated they rarely use a search engine other than Google, 5% indicate they never use a search engine beyond Google, and 23% indicated they infrequently (less than 25% of the time) use a search engine beyond Google, so for 53% (25+23+5) of our sample, Google is the most commonly used information search tool for academic research.

<table><caption>Table 1: Question 10 - Consider the papers that you have written in the last year. How often have you used research tools beyond Google or Ask.com to do your research?</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>Never</td>

<td>6%</td>

</tr>

<tr>

<td>Infrequently - less than 25 per cent of the time.</td>

<td>23%</td>

</tr>

<tr>

<td>Usually - about 75 per cent of the time.</td>

<td>20%</td>

</tr>

<tr>

<td>Almost always.</td>

<td>>26%</td>

</tr>

</tbody>

</table>

When asked how they identify whether or not to use a source in their research, subjects provided a number of different answers from a set of criteria as shown in Table 2\. The most common selection was that the Website was understandable and current, suggesting that Websites which were difficult to understand or did not appear to have current information were not used. It is interesting to note that the criteria for _credentials of the author_ and the _amount of information_ were selected less commonly than the criteria of _understandability_ and _currency_ of the information, suggesting that the credentials of the author were less of a concern for some of the subjects.

<table><caption>Table 2: Question 9 - When a search engine returns a list of pages, I select a page from the list based on the following criteria. Check all that apply.</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>the format of the site</td>

<td>57%</td>

</tr>

<tr>

<td>whether or not the site has video</td>

<td>5%</td>

</tr>

<tr>

<td>the site has pictures</td>

<td>12%</td>

</tr>

<tr>

<td>the site has a lot of information</td>

<td>54%</td>

</tr>

<tr>

<td>the site is understandable</td>

<td>70%</td>

</tr>

<tr>

<td>I am familiar with the author of the site</td>

<td>22%</td>

</tr>

<tr>

<td>I can verify the information on the site</td>

<td>62%</td>

</tr>

<tr>

<td>the credentials (qualifications) of the author are good</td>

<td>54%</td>

</tr>

<tr>

<td>the site is current</td>

<td>70%</td>

</tr>

<tr>

<td>the quality of the writing on the site is good</td>

<td>60%</td>

</tr>

<tr>

<td>there are reviews and comments on the site</td>

<td>22%</td>

</tr>

</tbody>

</table>

### Evaluation of author qualifications and site authority

Subjects provided a range of answers concerning the authority of a Web page and the qualifications of the author. When asked how they determine the author of a Web page (see Table 3), almost half of the respondents indicated they could not make such a determination (45%) with a little over half indicating they could (55%). A similar conundrum exists with evaluating the qualifications of the author of the Web page. When asked if they could determine the qualifications of the author of a Web page, only 30% indicated they could, 63% of the respondents indicated they could not, and 7% indicated they did not understand what was meant by qualifications in the question. These results provide some indication of confusion concerning this information literacy instruction requirement and perhaps an issue with Web content in general.

A similar question asked subjects about evaluating whether or not a site is authoritative. A little over half indicated they could determine whether or not a site was authoritative (53%), almost a quarter (24%) indicated they specifically could not determine the authoritativeness of a site, and almost a quarter (23%) indicated they did not understand (perhaps hedging because they do not check or do not know how to check).

<table><caption>Table 3: TRUE/FALSE questions on information source characteristics</caption>

<tbody>

<tr>

<th>Question</th>

<th>True</th>

<th>False</th>

<th>Don't understand</th>

</tr>

<tr>

<td>It is usually possible to determine the author of a Web page.</td>

<td>55%</td>

<td>45%</td>

<td> </td>

</tr>

<tr>

<td>

It is usually possible to determine the _qualifications_ of the author of a Web page.</td>

<td>30%</td>

<td>63%</td>

<td>7%</td>

</tr>

<tr>

<td>

It is usually possible to determine whether or not a site is _authoritative_.</td>

<td>53%</td>

<td>24%</td>

<td>23%</td>

</tr>

<tr>

<td>

It is usually possible to determine whether or not a Web page is _objective_ and _unbiased._</td>

<td>74%</td>

<td>25%</td>

<td>1%</td>

</tr>

<tr>

<td>

It is usually possible to determine the _credibility_ of a Web page.</td>

<td>80%</td>

<td>20%</td>

<td> </td>

</tr>

<tr>

<td>

I believe the pages listed by a search engine results usually contain **accurate** information.</td>

<td>60%</td>

<td>39%</td>

<td> </td>

</tr>

</tbody>

</table>

In examining how the subjects evaluated the authority of a site (see Table 4), subjects appeared to be confused by the general concept of authority, with 17% of respondents indicating they did not understand what was meant by that term in relation to Websites. Ten percent of respondents do not evaluate the authority of a site and 6% do not believe it is possible to examine the authority of a site. The total of these three selections represent one third (33%) of respondents who do not evaluate the authority of a site for some reason, whether it is lack of understanding or the belief that it is not possible to examine the authority of a site. Thirty-nine percent of respondents use the URL of the site to determine authority (perhaps not the best indicator of authority). Twelve percent confer with someone knowledgeable in the subject domain and 43% check to see if the author has published other content.

<table><caption>Table 4: Question 24: How do you evaluate the authority of a page and determine the ability to comment or write about a particular topic? Check all that apply.</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>

I do not understand what is meant by **authority** in this question.</td>

<td>17%</td>

</tr>

<tr>

<td>I do not believe it is possible to determine the authority of a page returned by a search engine.</td>

<td>6%</td>

</tr>

<tr>

<td>I check to see if the author of the page has published other pages, articles or books about the topic.</td>

<td>43%</td>

</tr>

<tr>

<td>I look for information about the author of the page - their qualifications, their degrees or certifications, their profession, their background, other pages/documents they have authored</td>

<td>43%</td>

</tr>

<tr>

<td>I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) and use that information to help me determine whether or not the site has authority.</td>

<td>39%</td>

</tr>

<tr>

<td>I do not examine the authority of a site</td>

<td>10%</td>

</tr>

<tr>

<td>I check with someone with knowledge of the site or topic, for example, library staff or a professor</td>

<td>12%</td>

</tr>

</tbody>

</table>

### Evaluating the objectiveness of content

When asked about the objectiveness of a Website in a TRUE/FALSE question (see Table 3), most respondents indicated they believed it was possible to evaluate the objectiveness of a Web page (74%), while a quarter (25%) indicated they believed it was not possible, and 1% did not understand the concept of objectiveness in relation to a Web page.

Table 5 provides the results of a question concerning the evaluation of the objectiveness of an Internet source and reveals a more nuanced perspective on the part of the subjects. For this multiple-select question, 68% of respondents indicated that a fair and balanced discussion of the topic indicated the site was objective, a standard definition of the term, but an almost equal percentage (62%) indicated that they used the URL of the site and the domain to determine whether or not the source was objective. Seven percent of respondents do not check the objectiveness of a site and an almost equal number (5%) do not believe it is possible to check the objectiveness of a site. Interestingly, 8% of the respondents felt that all pages returned by a search engine are objective.

<table><caption>Table 5: Question 20 - How do you decide whether or not a source retrieved from the Internet is objective and provides fair and equal treatment of all sides of a topic? Check all that apply.</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>

I do not understand what is meant by **objective** in this question</td>

<td>2%</td>

</tr>

<tr>

<td>I believe that pages returned by my search engine are objective</td>

<td>8%</td>

</tr>

<tr>

<td>I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) and use that information to help me determine whether or not the site is objective.</td>

<td>62%</td>

</tr>

<tr>

<td>I check with someone who may know, for example library staff or a professor</td>

<td>25%</td>

</tr>

<tr>

<td>I ask a friend if they think the site is objective.</td>

<td>8%</td>

</tr>

<tr>

<td>If the document provides a fair discussion of all sides of a topic or issue and acknowledges other viewpoints, then I consider it objective</td>

<td>68%</td>

</tr>

<tr>

<td>I do not evaluate the objectiveness of a site</td>

<td>7%</td>

</tr>

<tr>

<td>I do not believe it is possible to determine the objectiveness of a page returned by a search engine</td>

<td>5%</td>

</tr>

</tbody>

</table>

### Evaluation of content credibility

When asked a TRUE/FALSE question about source credibility, approximately 80% of the subjects indicated that they felt it was possible to determine the credibility of a Web page (see Table 3).

When asked a multiple select question about how they determined the credibility of a document and determined whether or not the document was trustworthy, subjects responded as shown in Table 6\. Most subjects (57%) reported that they looked at the URL of the site and an equal percentage (58%) looked at who published the site. Approximately 9% believed all pages returned by a search engine are credible, 5% do not evaluate the credibility of a site and another 2% do not understand what is meant by credibility, so a total of 16% (9+5+2) of subjects in this sample appear to not be evaluating credibility of Web content.

<table><caption>Table 6: Question 22 - How would you evaluate the credibility of a site or document and determine that the information is truthful and trustworthy? Check all that apply.</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>I do not understand what is meant by credibility in this question.</td>

<td>2%</td>

</tr>

<tr>

<td>I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) and use that information to help me determine whether or not the site is credible.</td>

<td>57%</td>

</tr>

<tr>

<td>I look at the background of the author of the page - their professional affiliations, their college degrees, what else they have written.</td>

<td>55%</td>

</tr>

<tr>

<td>I check to see who published the information on the site</td>

<td>58%</td>

</tr>

<tr>

<td>I believe that pages returned by my search engine are credible.</td>

<td>9%</td>

</tr>

<tr>

<td>I do not evaluate the credibility of Web sites.</td>

<td>5%</td>

</tr>

<tr>

<td>I do not believe it is possible to evaluate the credibility of pages returned by a search engine</td>

<td>5%</td>

</tr>

<tr>

<td>I evaluate the information on the site against what I know about the topic</td>

<td>36%</td>

</tr>

</tbody>

</table>

### Evaluation of content accuracy

When asked a TRUE/FALSE question about the evaluation of the accuracy of Web content, over half (60 %) of the subjects indicated that they believed pages listed or returned by search engines usually contain accurate information (see Table 3).

When asked a multiple select question about how they evaluate the accuracy of information on a page, subjects appeared to take a more conservative and nuanced view of search engine accuracy (see Table 7). Only 6% of the subjects indicated that search engine results are accurate (the word 'usually' was not used here, implying either pages were accurate or were not accurate). Over half of the respondents (60%) look at the URL to evaluate accuracy and an almost equal percentage (64%) evaluate the content on the site against their own prior store of knowledge (what they know about the topic).

<table><caption>Table 7: Question 21 - How do you decide whether or not information on a page is accurate and contains truthful and correct information? Check all that apply.</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>

I do not understand what is meant by **accurate** in this question</td>

<td>2%</td>

</tr>

<tr>

<td>I believe that pages returned by my search engine are accurate</td>

<td>6%</td>

</tr>

<tr>

<td>I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) and use that information to help me determine whether or not the site is accurate</td>

<td>60%</td>

</tr>

<tr>

<td>I check with someone with knowledge of the site or topic, for example, library staff or a professor</td>

<td>34%</td>

</tr>

<tr>

<td>I believe that sites with a more current date are more accurate</td>

<td>30%</td>

</tr>

<tr>

<td>I do not check the accuracy of information on a web site</td>

<td>5%</td>

</tr>

<tr>

<td>I evaluate the information on the page in relation to what I know about the topic</td>

<td>64%</td>

</tr>

<tr>

<td>I do not believe it is possible to determine the accuracy of pages returned by a search engine</td>

<td>5%</td>

</tr>

<tr>

<td>I ask a friend if they think the page is objective</td>

<td>4%</td>

</tr>

</tbody>

</table>

### Evaluation of page quality

When asked about how they evaluate the quality of a Website, subjects responded as shown in Table 8\. Over half of the respondents (51%) appear to consider spelling and grammar to be part of the evaluation, and an equal percentage (51%) feel the URL of the site has some bearing on quality. Most (66%) of the subjects based quality on a determination of whether the page is comprehensive and covers the topic in considerable depth and over a third (38%) chose Web pages which were interesting and gave clear and well-reasoned explanations.

<table><caption>Table 8: Question 23 - How do you evaluate the quality of a page returned by a search engine? Check all that apply.</caption>

<tbody>

<tr>

<th>Response text</th>

<th>Percentage</th>

</tr>

<tr>

<td>

I do not understand what is meant by **quality** in this question.</td>

<td>3%</td>

</tr>

<tr>

<td>If the page includes pictures and charts it is a quality site</td>

<td>11%</td>

</tr>

<tr>

<td>If the page is free from spelling, typographical errors and the author uses good grammar, I consider it a quality site.</td>

<td>51%</td>

</tr>

<tr>

<td>If the information presented in the site is comprehensive and covers the topic in considerable depth, I consider it a quality site.</td>

<td>66%</td>

</tr>

<tr>

<td>I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) and use that information to help me determine whether or not the site is a quality site.</td>

<td>51%</td>

</tr>

<tr>

<td>I do not evaluate the quality of pages returned by a search engine.</td>

<td>3%</td>

</tr>

<tr>

<td>If the information on the page is interesting, and presents a clear, well-reasoned explanation of the topic, I consider it a quality page.</td>

<td>38%</td>

</tr>

<tr>

<td>I do not believe it is possible to determine the quality of a page returned by a search engine.</td>

<td>3%</td>

</tr>

<tr>

<td>I check with someone with knowledge of the site or topic, for example, library staff or a professor</td>

<td>16%</td>

</tr>

</tbody>

</table>

### Evaluation of information sources in relation to years in college

Evaluation of survey responses in relation to year in college provides some additional insights into the success or failure of current information literacy training efforts. Table 9 details the TRUE responses to a set of questions for early years college students (freshmen and sophomore year) and later years college students (junior and senior year). These results indicate a statistically significant change in students' perception of various properties of Internet sources in later years of college (?-squared = 19.3077, df = 11, p-value = 0.056). Subjects appeared to be more circumspect in their evaluation of Web sources, with more subjects in later years indicating they could not identify the author of a Web page and more subjects indicating they could not determine the credibility of a Web page.

<table><caption>Table 9: Year in college and evaluation of sources questions. Percentage of TRUE responses (in-group percentages)></caption>

<tbody>

<tr>

<th>Question</th>

<th>Question text</th>

<th>Early</th>

<th>Later</th>

</tr>

<tr>

<td>14</td>

<td>It is usually possible to determine the author of a Web page</td>

<td>18%</td>

<td>14%</td>

</tr>

<tr>

<td>15</td>

<td>It is usually possible to determine the qualifications of the author</td>

<td>9%</td>

<td>8%</td>

</tr>

<tr>

<td>16</td>

<td>It is usually possible to determine whether or not a site is authoritative</td>

<td>15%</td>

<td>14%</td>

</tr>

<tr>

<td>17</td>

<td>It is usually possible to determine whether or not a page is objective</td>

<td>20%</td>

<td>21%</td>

</tr>

<tr>

<td>18</td>

<td>It is usually possible to determine the credibility of a Web page</td>

<td>20%</td>

<td>24%</td>

</tr>

<tr>

<td>19</td>

<td>Web pages listed by a search engine usually contain accurate information.</td>

<td>17%</td>

<td>19%</td>

</tr>

</tbody>

</table>

A question concerning the choice of information search tool also provides some indication of a change in the student's perception of sources in relation to year in college as indicated in Table 10\. When asked whether they use a search engine or library database, survey respondents in later years of college indicated a slightly greater preference for library databases and a slightly smaller preference for Google. These results are significant at the p <= 0.10 level (?-squared = 11.97, df = 7, p-value = 0.10).

<table><caption>Table 10: Question 13 - Which search engines or library databases do you use for your research? Check all that apply. (in-group percentages)</caption>

<tbody>

<tr>

<th>Search engine/database</th>

<th>Early (freshman-sophomore)</th>

<th>Late (junior-senior)</th>

</tr>

<tr>

<td>Yahoo</td>

<td>6%</td>

<td>4%</td>

</tr>

<tr>

<td>Bing</td>

<td>3%</td>

<td>3%</td>

</tr>

<tr>

<td>Google</td>

<td>39%</td>

<td>34%</td>

</tr>

<tr>

<td>Other (Blekko/Lycos etc.)</td>

<td>0%</td>

<td>3%</td>

</tr>

<tr>

<td>Cross-search engine aggregator</td>

<td>2%</td>

<td>1%</td>

</tr>

<tr>

<td>General library</td>

<td>27%</td>

<td>29%</td>

</tr>

<tr>

<td>Subject specific library</td>

<td>13%</td>

<td>17%</td>

</tr>

<tr>

<td>Book catalogue</td>

<td>9%</td>

<td>8%</td>

</tr>

</tbody>

</table>

## Discussion

The major findings from this study can be summarised as follows.

*   the Google search engine appears to be the most widely used source for academic research for subjects, though subjects did report using other sources
*   subjects had a high degree of confidence in search engine results, with the majority (60%) believing that pages returned by search engines usually contain accurate information
*   identification of the author of a Web page appeared to be difficult for subjects, with almost half (45%) indicating they could not determine the author of a Web page
*   evaluation of the authoritative characteristics of a source was difficult for subjects, with almost a quarter reporting they could not determine the authoritative characteristics of a site and almost a quarter reporting they did not understand the concept in that context
*   identification of the qualifications of the author were difficult for subjects, with a majority (63%) indicating they did not believe it was possible to evaluate author qualifications
*   a quarter (25%) of respondents did not consider it possible to determine the objectiveness of a Web page and over half (62%) used the URL and domain of the site to determine objectivity
*   subjects in later years of college (junior-senior) appear to be more discerning about Internet sources, less likely to use Google and more likely to use library databases

These findings and others are discussed in more detail in the following paragraphs.

### Information search habits

Google was the most common information source for this search sample, though respondents did provide some indication that they also commonly use other information sources. Nevertheless, this provides some quantitative confirmation of other findings which indicate that the Web and specifically Web content delivered through the Google commercial search engine continues to be a primary source for college level research ([Head and Eisenberg, 2010](#head-2010); [Biddix, Chung and Park, 2011](#biddix-20111)).

### Evaluation of author qualifications and site authority

Information literacy instruction implores students to ask questions about the authority of content sources, specifically to learn about the authors of the source. Students are asked to determine the author's credentials, reputation, knowledge about the subject, education and affiliation (Doyle and Hammond, 2006). For this sample, almost half (45%) of the subjects indicated they could not identify the author of a Web page and thus would not be able to meet that information literacy competency standard performance outcome. This finding provides evidence to support Doyle and Hammond's ([2006](#doyle-2005)) proposal that identifying the source and determining authorship be supported through specific criteria (pp. 63-64).

Similarly, when asked about the qualifications of the author of a Web page, a majority of respondents (63%) indicated they could not evaluate the qualifications of an author, providing indications of further issues concerning the evaluation of Web content authorship. Furthermore, in evaluating the authority of a site, a third of the respondents indicated they do not evaluate site authority for some reason, and 39% of respondents use the URL of the site to determine authority (perhaps not the best indicator of authority). Twelve percent confer with someone knowledgeable in the subject domain and 43% check to see if the author has published other content. These results are an indication that subjects struggle with the identification of site authority using a variety of methods which may or may not provide good results.

### Evaluating objectiveness of content

When teaching about objectivity, librarians expect that students will be able to identify if a source provides a fair discussion of all viewpoints. The majority of subjects in this study had confidence that they could evaluate the objectiveness of a Web page (74%). A majority of subjects (68%) attempted to determine if a site provides a fair discussion of all sides of a topic, an indication that they are making a valid effort to determine the objectiveness of content. Over half (62%) indicated they use the URL of the site to determine the objectivity of the source and a quarter of the subjects check with someone with more knowledge concerning a source. While a URL alone is not a good indicator of objectiveness, subjects were considering other criteria. That only 25% seek advice is potentially an indication that expediency may be more important to students than taking the time to perform a deeper evaluation of objectivity for a source.

### Evaluation of content credibility

Most subjects indicated they could evaluate content credibility (80%), but 57% use the URL and 55% look at the author of the site, which almost half of the subjects indicated they could not identify. Five percent of subjects did not appear to be considering the credibility of a site at all. Additionally, about a third (36%) reviewed the site's content against what they already knew about the subject. This suggests that many students are not thinking critically about the information presented to them and evaluate credibility using a superficial and easy measure such as the site URL. This provides additional confirmation of a finding reported by other researchers that 'students value credibility but prefer efficiency' ([Biddix, _et al._, 2011](#biddix-20111), p.180).

### Evaluation of content accuracy

In response to a question about page selection, subjects indicated they used a somewhat balanced approach to selecting pages for their research, with a slight preference for understandability and currency of pages relative to other criteria. A majority of respondents believe the pages returned by search engines usually contain accurate information. The survey question asked if the results returned were usually accurate, so it is possible subjects allowed for inaccurate information sometimes being returned from search engines. Still this result appears to provide some indication that subjects were very confident in the accuracy of search engine results. This finding may be a disappointment to librarians who would prefer students not simply assume pages or articles are accurate. If students trust a search engine's results, giving credit to a source just because it appears in the first or second page of the results, as Head and Eisenberg ([2010](#head-2010)) also found, then there is little motivation to evaluate these sources.

An additional question provided more in-depth examination of how subjects evaluate the accuracy of Internet sources. Survey responses indicated that approximately 60% of the subjects look at the URL to evaluate accuracy and 64% evaluate the content on the site against their own prior store of knowledge. Only about a third of the subjects look for help in evaluating the accuracy of a source, indicating that they were fairly confident they could evaluate the accuracy of the source through the URL or their own knowledge.

### Evaluation of content quality

Survey respondents provided a somewhat balanced set of responses when asked about the evaluation of content quality. Half (51%) of the respondents considered the URL and domain to be an indicator of quality and an equal percentage considered the spelling and grammar on the site to be an indicator of quality. Most (66%) considered the depth and comprehensiveness of the site to be an indicator of quality and 38% considered the well-reasoned presentation of the topic to be the standard for site quality. This finding provides some indication that subjects are evaluating quality and using a number of different criteria, though it is debatable whether or not the URL and domain alone provide a clear indication of Web page quality.

### Evaluation of information sources in relation to years in college

Survey respondents in later years of college (junior-senior) indicated a slight but statistically significant change in evaluation of Internet sources. Questions concerning the examination of various properties of a Web source revealed that subjects in later years of college were more aware that it may not be possible to determine the author of a Web page and the qualifications of the author. Subjects also indicated that they were less certain about the objectivity, authoritativeness and credibility of Web sources in later years of college. Conversely, these same subjects also appeared to be more certain of the accuracy of Web sources.

This finding provides some indication that the junior and senior class students in this sample have learned some information literacy skills and report applying them in the evaluation of Web sources. However, there still appears to be a belief amongst these same students that Web sources are usually accurate sources. This certainty may reflect a personal confidence that they can determine the accuracy of a Web page or that they have been desensitised and falsely assume that this constant source of information (the Web) is truly accurate information.

## Limitations of study

Evaluation of the demographic questions in the survey revealed a slight bias towards women and students with a high grade point average (above 3.0). Additionally, the survey sample was from a single U. S. university. A slightly more diversified sample from more universities, domestic and international, might produce different results.

Though subjects in this sample received information literacy instruction after taking the survey, it is possible that the subjects had previously received information literacy instruction in a previous year at the university or before coming to the university.

Subjects may not have clearly understood the terms and concepts addressed in the questions in relation to information seeking. Many questions provided a response which could be used to indicate confusion with the concept, but not all questions provided this response.

## Conclusion

The findings in this study add quantitative evidence to the assertion that the Web has become the primary source for research for college students. Though subjects gave some indication that other sources were used, the Web, through the lens of the commercial Google search engine, appears to be their primary research source.

The goal of the study was to examine two research questions in relation to information literacy standards for evaluation of Web sources. Evaluation of the sources was based on the criteria of authority, reliability, validity, accuracy, timeliness and point of view or bias. The first research question was whether searchers considered it possible to examine these criteria for documents found on the Web. Survey questions fourteen to nineteen addressed this research question. Our findings indicate that for the evaluative criteria of author, authoritativeness, objectivity, credibility and accuracy, the majority of subjects felt they could determine these criteria for Web information sources. However, a majority of subjects felt they could not determine the qualifications of the author of a Web page. The second research question was to determine how searchers evaluate these criteria when examining information sources. Survey questions twenty to twenty-seven addressed this research question. Our findings indicate that most subjects reported using a number of different strategies to evaluate these criteria. While many of these strategies are logical and expected, careful examination of our results provides some indication of confusion amongst the subjects about how to perform these evaluations. These findings are discussed in more detail in the following paragraphs.

While most subjects (60%) responding to a TRUE/FALSE question concerning search engine accuracy indicated they believed search engine results are usually accurate, responses to more detailed questions concerning search engine results were more nuanced. The results of a series of questions asking survey respondents about credibility, quality and authority of search engine results indicated students examine a number of different criteria in evaluating sources. This could be an indication that a decade of information literacy instruction and expressions of concern by university faculty have had an impact on students' perceptions of search engine results. Still, the indication that most subjects considered search engine results usually accurate should be a concern. Given that students and instructors understand little of the unpublished algorithms which drive commercial search engines, it cannot be assumed that results returned by these applications meets any of the Association of College and Research Libraries information literacy competence evaluative criteria, including the criteria of accuracy. Though Google's search engine algorithm is proprietary and not publicly available, evidence available indicates that Google's search engine results are ordered largely by number of links and user selection counts, with no measure of accuracy or quality ([Brin and Page, 1998](#brin-1998)). It is important that students understand this and evaluate the documents returned by Google accordingly.

Information literacy instruction emphasises identification of the author. This is crucial in understanding the authority or ability of the author to write about a particular topic. There is perhaps no other area where the distinction between traditional publishing and the Web creates difficulties for researchers using Web sources. The nature of the free-form publishing of the Web makes it very difficult to determine the true authorship of a page. The technology of the Web makes the development of collaborative documents extremely easy, leading to sites such as Wikipedia. While debate on the quality of the content of a collaborative encyclopaedic site such as Wikipedia is beyond the scope of this paper, the authorship of the content and the qualifications of the authors are clearly difficult to ascertain. Information literacy instruction and course instructors should address this issue.

While subjects provided various answers in response to the question about evaluating the objectiveness of a Web page, the indication that subjects use the URL or site domain to determine objectivity is problematic. Students should be more discerning and information literacy instruction should encourage more careful evaluation of objectivity of Web page sources.

Subjects in this study used a variety of techniques to evaluate Web content. In examining their evaluation in relation to Association of College and Research Libraries information literacy standards, subjects appeared to follow some standards but were confused about how to actually implement them. Specifically, identifying content authors, assigning authority for content and determining accuracy and objectivity appeared difficult for subjects. While subjects did indicate that they were attempting to perform these actions and did appear to have confidence in their ability to perform these actions, their selection of methods was inconsistent and problematic. These could be indications of problems with information literacy instruction standards or broader problems with assigning authorship and determining authority and accuracy of Web content in general.

The results of this study provide some indication of flaws in a Website evaluation checklist instructional model. Additionally, the broad proposals on evaluating information sources in the Association of College and Research Libraries information literacy standards may require context-sensitive strategies which are specific to various fields. Librarians alone cannot remedy this, since librarians cannot be experts in all fields. Faculty and librarian partnerships in course instruction may provide a solution. This can be accomplished using an embedded librarian within the course to accomplish more in helping students to identify authors, authority, accuracy, objectivity, point of view and other important criteria to determine which sources are really the most appropriate to use in the context of the course and assignment.

## Future directions

The Association of College and Research Libraries has recognised that the Web and college students have changed since the release of the original information literacy competency standards in 2000 and they have been '_rethinking the standards_' ([Bell, 2013](#bell-2013)). Kuhlthau ([2013](#kuhlthau-2013)) highlights how the current standards are not holistic and do not adequately prepare students to research reflectively in the '_dynamic global information environment_' (p. 97).

The Association of College and Research Libraries Information Literacy Standards Task Force has proposed a Framework for Information Literacy. The framework explores information literacy criteria with threshold concepts (core understandings), a set of abilities or knowledge practices, and related metaliteracy objectives. These objectives expand evaluation criteria definitions and encourage evaluation of context referred to as the 'information need of the moment' ([Gibson and Jacobson, 2014](#gibson-2014)).

The results of this study demonstrate the need for course instructors to engage with information literacy objectives through their semester long courses. The new Association of College and Research Libraries information literacy framework provides a structure for doing this. Librarians need to partner with course instructors to design assignments that encourage students to think critically about the selection of information sources relating to course content.

Subject domains cannot be ignored given the nature of the information available on the Web, the lack of access mediation and the relative lack of peer or expert review of much of the content available. The evaluation of the results from these distinct domains may require a different set of standards and rules for each. Some subject-specific information literacy standards have been compiled by the American Library Association, but not all disciplines are currently represented (see [Information Literacy Standards Committee, 2013](#ala13) for the list of disciplines with subject specific standards).

## About the Authors

**Arthur Taylor** is a Professor of Computer Information Systems at Rider University. Before joining Rider University, he worked for over 17 years in the field of information systems. His research interests include information literacy, relevance, and the information search process. He can be contacted at [ataylor@rider.edu](mailto:ataylor@rider.edu).  
**Heather Dalal** is an Assistant Professor/Emerging Technologies Librarian at Rider University. Her research interests include instructional design and technologies, student research behaviour, and promotion and marketing of library tools and services. Heather serves on the Association of College & Research Libraries (ACRL) Distance Learning Section's Standards Committee, the New Jersey Library Association's (NJLA) Emerging Technologies Section, and the New Jersey Virtual Academic Library Environment's Shared Information Literacy Committee. Heather is the co-Chair of the ACRL-NJ/NJLA College and University Section User Education Committee. She can be contacted at [hdalal@rider.edu](mailto:hdalal@rider.edu).

</section>

<section>

## References

*   American Library Association. _Information Literacy Standards Committee_. (2013). _[Standards and guidelines.](http://www.webcitation.org/6Qcd0GBQ5)_ Chicago, IL: American Library Association. Retrieved from: http://www.ala.org/acrl/issues/infolit/ilcc/ilcc-standards (Archived by WebCite® at http://www.webcitation.org/6Qcd0GBQ5)
*   Association for College and Research Libraries. (2000). _Information literacy competency standards for higher education_. Chicago, IL: American Library Association.
*   Atwong, C. T. & Heichman Taylor, L. J. (2008). Integrating information literacy into business education: a successful case of faculty-librarian collaboration. _Journal of Business & Finance Librarianship, 13_(4), 433-449.
*   Bell, S.J. (2013, June 4). [Rethinking ACRL's information literacy standards: the process begins](http://www.webcitation.org/6QbSubYZp). Retrieved from: http://www.acrl.ala.org/acrlinsider/archives/7329/ (Archived by WebCite® at http://www.webcitation.org/6QbSubYZp)
*   Benjes-Small, C., Archer, A., Tucker, K., Vassady, L. & Resor, J. (2013). Teaching Web evaluation. _Communications in Information Literacy, 7_(1), 39-49.
*   Biddix, J., Chung, C. & Park, H. (2011). Convenience or credibility? A study of college student online research behaviors. _Internet and Higher Education, 14_(3), 175-182.
*   Blakeslee, S. (2004). The CRAAP test. _LOEX Quarterly, 31_(3), 4.
*   Brin, S. & Page, L. (1998). The anatomy of a large-scale hypertextual Web search engine. _Computer Networks and ISDN Systems, 30_ (1-7), 107-117.
*   Buczynski, J. A. (2005). Satisficing digital library users. _Internet Reference Services Quarterly, 10_(1), 99-102.
*   Carr, N. (2010). _What the Internet is doing to our brains: the shallows_. New York, NY: W. W. Norton and Company.
*   Christennson, K.M. (2013). [RADCAB - your vehicle for information evaluation](http://www.webcitation.org/6QcaDNU2e). Retrieved from http://radcab.com. (Archived by WebCite® at http://www.webcitation.org/6QcaDNU2e)
*   Currie, L., Devlin, F., Emde, J. & Graves, K. (2010). Undergraduate search strategies and evaluation criteria: searching for credible sources. _New Library World, 111_(3/4), 113-124.
*   Dahlen, S. (2012). Seeing college students as adults: learner-centered strategies for information literacy instruction. _Endnotes: the Journal of the New Members Round Table, 3_(1), 1-18.
*   Dee, C. R. & Reynolds, P. (2013). Lifelong learning for nurses–building a strong future. _Medical Reference Services Quarterly, 32_(4), 451-458.
*   Detlor, B., Booker, L., Serenko, A. & Julien, H. (2013). Student perceptions of information literacy instruction: the importance of active learning. _Education for Information, 29_(2), 147-161.
*   Doyle, T. & Hammond, J. L. (2006). Net cred: evaluating the Internet as a research source. _Reference Services Review, 34_(1), 56-70.
*   Flanagin, A. J. & Metzger, M. J. (2008). Digital media and youth: unparalleled opportunity and unprecedented responsibility. In M.J. Metzger & A.J. Flanagin (Eds.), _Digital media, youth, and credibility_ (pp. 5-27). Cambridge, MA: MIT Press.
*   Gibson, C. & Jacobsen, T. (2014). [Draft framework for information literacy for higher education: part 1](http://www.webcitation.org/6QcaWuEzx). Chicago, IL: American Library Association. Retrieved from http://acrl.ala.org/ilstandards/wp-content/uploads/2014/02/Framework-for-IL-for-HE-Draft-1-Part-1.pdf (Archived by WebCite® at http://www.webcitation.org/6QcaWuEzx)
*   Grimes, D. & Boening, C. (2001). Worries with the Web: a look at student use of Web resources. _College and Research Libraries, 62_(1), 11-22.
*   Gross, M. & Latham, D. (2011). Experiences with and perceptions of information: a phenomenographic study of first-year college students. _Library Quarterly, 81_(2), 161-186
*   Head, A. J. & Eisenberg, M. B. (2010). [Truth be told: how college students evaluate and use information in the digital age](http://www.webcitation.org/6QccRbXgo). Seattle, WA: University of Washington, Project Information Literacy. Retrieved from http://projectinfolit.org/images/pdfs/pil_fall2010_survey_fullreport1.pdf (Archived by WebCite® at http://www.webcitation.org/6QccRbXgo)
*   Hilligoss, B. & Rieh, S. (2008). Developing a unifying framework of credibility assessment: construct, heuristics, and interaction in context. _Information Processing & Management, 44_(4), 1467-1484.
*   Hirsh, S. G. (1999). Children's relevance criteria and information seeking on electronic resources. _Journal of the American Society for Information Science, 50_(14), 1265-1283.
*   Hsieh, M.L., Dawson, P.H., Hofmann, M.A., Titus, M.L. & Carlin, M.T. (2014). Four pedagogical approaches in helping students learn information literacy skills. _Journal of Academic Librarianship, 40_(3/4), 234-246.
*   Jackson, R. (2007). Cognitive development: the missing link in teaching information literacy skills. _Reference & User Services Quarterly, 46_(4), 28-32.
*   Julian, S. (2013). Reinventing classroom space to re-energise information literacy instruction. _Journal of Information Literacy, 7_(1), 69-82.
*   Kuhlthau, C. (2013). Rethinking the 2000 ACRL standards. _Communications in Information Literacy, 7_(2), 92-97.
*   Lorenzen, M. (2001). Land of confusion? High school students and their use of the World Wide Web for research. _Research Strategies, 18_(2), 151-163.
*   Mathson, S. M. & Lorenzen, M. G. (2008). We won't be fooled again: teaching critical thinking via evaluation of hoax and historical revisionist Websites in a library credit course. _College & Undergraduate Libraries, 15_(1/2), 211-230.
*   Meola, M. (2004). Chucking the checklist: a contextual approach to teaching undergraduates Web-site evaluation. _Portal: Libraries & the Academy, 4_(3), 331.
*   Metzger, M.J. (2007). Making sense of credibility on the Web: models for evaluating online information and recommendations for future research. _Journal of the American Society for Information Science and Technology, 58_ (13), 2078-2091.
*   Metzger, M.J., Flanagin, A. J. & Medders, R. B. (2010). Social and heuristic approaches to credibility evaluation online. _Journal of Communication, 60_(3), 413-439.
*   Metzger, M.J., Flanagin, A.J. & Zwarun, L. (2003). College student Web use, perceptions of information credibility, and verification behavior. _Computers & Education, 41_(3), 271-290.
*   Norgaard, R. (2004). Writing information literacy in the classroom: pedagogical enactments and implications. _Reference & User Services Quarterly, 43_(3) 220-226.
*   O'Connor, L., Bowles-Terry, M., Davis, E. & Holliday, W. (2010). "Writing information literacy" revisited application of theory to practice in the classroom. _Reference & User Services Quarterly, 49_(3), 225-230.
*   Rowlands, I., Nicholas, D., Williams, P., Huntington, P., Fieldhouse, M., Gunter, B., Withey, B., Jamali, H. R., Dobrowolski, T. & Tenopir, C. (2008). The Google generation: the information behaviour of the researcher of the future. _Aslib Proceedings, 60_(4), 290 - 310.
*   Shanahan, M.C. (2008). Transforming information search and evaluation practices of undergraduate students. _International Journal of Medical Information, 77_(8), 518-526.
*   Taylor, A. (2012). [A study of the information search behaviour of the millennial generation.](http://www.webcitation.org/6UDr5AkZB) _Information Research, 17_(1), paper 508\. Retrieved from http://www.informationr.net/ir/17-1/paper508.html (Archived by WebCite® at http://www.webcitation.org/6UDr5AkZB)
*   Thompson, C. (2003). Information illiterate or lazy: how college students use the Web for research. _Libraries and the Academy, 3_(2), 259-268.
*   Tillotson, J. (2002). Web site evaluation: a survey of undergraduates. _Online Information Review, 26_(6), 392-403.
*   Walraven, A., Brand-Gruwel, S., & Boshuizen, H. (2009). How students evaluate information and sources when searching the World Wide Web for information. _Computers & Education, 52_(1), 234-246.
*   Wang, L. (2011). An information literacy integration model and its application in higher education. _Reference Services Review, 39_(4), 703-720.
*   Weiner, S. (2014). Who teaches information literacy competencies? Report of a study of faculty. _College Teaching, 62_(1), 5-12.
*   Williams, P. & Rowlands, I. (2007). [Information behaviour of the researcher of the future: the literature on young people and their information behavior: work package II](http://www.webcitation.org/6QcdDpZWG). Bristol, UK: Joint Information Systems Committee. Retrieved from http://www.jisc.ac.uk/media/documents/programmes/reppres/ggworkpackageii.pdf. (Archived by WebCite® at http://www.webcitation.org/6QcdDpZWG)
*   Van Deursen, A.J.A.M. & Van Diepen, S. (2013). Information and strategic Internet skills of secondary students: a performance test. _Computers & Education, 63_, 218-226.
*   Young, N.J. & Von Segern, M. (2001). General information seeking in changing times: a focus group study. _Reference and User Services Quarterly, 41_(2), 159-169.

</section>

</article>

* * *

<section>

# Appendices

## Appendix A – Survey questions

1.  If you have declared a major or completed work for a college undergraduate degree, what is or was the _area of study_ for your declared undergraduate major? If you have multiple declared majors or multiple degrees, check all that apply.
    1.  I have not declared a major
    2.  business
    3.  education
    4.  sciences
    5.  communication
    6.  arts
    7.  music
    8.  math
    9.  oreign language
    10.  English language
    11.  journalism
    12.  theatre
    13.  nursing
    14.  engineering
    15.  my major is not listed
2.  What is your gender?
    1.  male
    2.  female
3.  What is your age?
    1.  number range from 18 to 85
4.  Is English your first language?
    1.  yes
    2.  no
5.  How often do you use the Web to find information?
    1.  several times a day
    2.  about once a day
    3.  about once a week
    4.  about once a month
6.  How many years of college have you completed?
    1.  How many years of college have you completed?
    2.  Sophomore
    3.  Junior
    4.  Senior
    5.  Graduate
    6.  Masters
    7.  Graduate - Ph.D.
7.  How many papers have you written in the last year that required research, evaluation and citation of sources?
    1.  0
    2.  1
    3.  2
    4.  3
    5.  4 or more
8.  What is your current GPA?
    1.  below 2.0
    2.  above 2.0 and below 2.5
    3.  2.5 and above and below 3.0
    4.  3.0 and above and below 3.5
    5.  3.5 and above
    6.  not applicable or do not know
9.  When a search engine returns a list of pages, I select a page from the list based on the following criteria. Check all that apply.
    1.  the format of the site
    2.  whether or not the site has video
    3.  the site has pictures
    4.  the site has a lot of information
    5.  the site is understandable
    6.  I am familiar with the author of the site
    7.  I can verify the information on the site
    8.  the credentials (qualifications) of the author are good
    9.  the site is current
    10.  the quality of the writing on the site is good
    11.  there are reviews and comments on the site
10. Consider the papers that you have written in the last year. How often have you used research tools **beyond** Google or Ask.com to do your research?
    1.  Never
    2.  Infrequently - less than 25 per cent of the time.
    3.  Sometimes - about 50 per cent of the time.
    4.  Usually - about 75 per cent of the time.
    5.  Almost always
11. How would you know when you have enough sources for a paper?
    1.  I don't worry about the number of sources for a paper.
    2.  5 sources is enough for any paper
    3.  10 sources is enough for any paper
    4.  20 sources is enough for any paper
    5.  I try to find enough quality sources to support the information in my paper.
12. Consider the papers that you have written in the last year. How many pages of search engine (Google, Yahoo, Bing) results do you usually view?
    1.  1 page
    2.  2 pages
    3.  more than 2 pages
    4.  more than 5 pages
13. Which search engines or library databases do you use for your research? Check all that apply.
    1.  Yahoo!
    2.  Bing
    3.  Google
    4.  Other Search engine such as: Blekko/Lycos/AOL
    5.  Metasearch engine (one that cross searches other search engines) such as Ixquick/Dogpile/Clusty/Webcrawler/Mywebsearch
    6.  General Library databases such as Academic Search Premier/CQ Researcher/Credo Reference/LexisNexis/JSTOR/Omnifile/Gale Virtual ReferenceEbscohost
    7.  Subject specific library databases such as: ABI-Infom, ArtStor, Business Source Premier, Communication
    8.  Library OneSearch (cross-searches the library catalog and most of our databases)
    9.  Library Book Catalog
14. It is usually possible to determine the author of a web page.
    1.  TRUE
    2.  FALSE
15. It is usually possible to determine the _qualifications_ of the author of a web page.
    1.  TRUE
    2.  FALSE
    3.  I do not understand what is meant by _qualifications_ in this question
16. It is usually possible to determine whether or not a site is _authoritative_.
    1.  TRUE
    2.  FALSE
    3.  I do not understand what is mean by _authoritative_ in this question
17. It is usually possible to determine whether or not a web page is _objective_ and _unbiased._
    1.  TRUE
    2.  FALSE
    3.  I do not understand what is meant by _objective_ and _unbiased_
18. It is usually possible to determine the _credibility_ of a web page.
    1.  TRUE
    2.  FALSE
    3.  I do not understand the meaning of _credibility_ in this question
19. I believe the pages listed by a search engine results usually contain _accurate_ information.
    1.  TRUE
    2.  FALSE
20. How do you decide whether or not a source retrieved from the Internet is _objective_ and provides fair and equal treatment of all sides of a topic? Check all that apply.
    1.  I do not understand what is meant by _objective_ in this question
    2.  I believe that pages returned by my search engine are objective
    3.  I look at the URL of the site, and based on the domain (.com,., .edu, .org, .net, etc.) and use that information to help me determine whether or not the site is objective.
    4.  I check with someone who may know, for example library staff or a professor
    5.  I ask a friend if they think the site is objective.
    6.  If the document provides a fair discussion of all sides of a topic or issue and acknowledges other viewpoints, then I consider it objective
    7.  I do not evaluate the objectiveness of a site
    8.  I do not believe it is possible to determine the objectiveness of a page returned by a search engine
21. How do you decide whether or not information on a page is _accurate_ and contains truthful and correct information? Check all that apply.
    1.  I do not understand what is meant by _accurate_ in this question
    2.  I believe that pages returned by my search engine are accurate
    3.  I look at the URL of the site, and based on the domain (.com,.edu, .org, .net, etc.) and use that information to help me determine whether or not the site is accurate
    4.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
    5.  I believe that sites with a more current date are more accurate
    6.  I do not check the accuracy of information on a web site
    7.  I evaluate the information on the page in relation to what I know about the topic
    8.  I do not believe it is possible to determine the accuracy of pages returned by a search engine
    9.  I ask a friend if they think the page is objective
22. How would you evaluate the _credibility_ of a site or document and determine that the information is truthful and trustworthy? Check all that apply.
    1.  I do not understand what is meant by _credibility_ in this question.
    2.  I look at the URL of the site, and based on the domain (.com,.edu, .org, .net, etc.) and use that information to help me determine whether or not the site is credible.
    3.  I look at the background of the author of the page - their professional affiliations, their college degrees, what else they have written.
    4.  I check to see who published the information on the site
    5.  I believe that pages returned by my search engine are credible.
    6.  I do not evaluate the credibility of web sites.
    7.  I do not believe it is possible to evaluate the credibility of pages returned by a search engine
    8.  I evaluate the information on the site against what I know about the topic
23. How do you evaluate the _quality_ of a page returned by a search engine? Check all that apply.
    1.  I do not understand what is meant by _quality_ in this question.
    2.  If the page includes pictures and charts it is a quality site
    3.  If the page is free from spelling, typographical errors and the author uses good grammar, I consider it a quality site.
    4.  If the information presented in the site is comprehensive and covers the topic is considerable depth, I consider it a quality site.
    5.  I look at the URL of the site, and based on the domain (.com,.edu, .org, .net, etc.) and use that information to help me determine whether or not the site is a quality site.
    6.  I do not evaluate the quality of pages returned by a search engine.
    7.  If the information on the page is interesting, and presents a clear, well-reasoned explanation of the topic, I consider it a quality page.
    8.  I do not believe it is possible to determine the quality of a page returned by a search engine.
    9.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
24. How do you evaluate the _authority_ of a page and determine the ability to comment or write about a particular topic? Check all that apply.
    1.  I do not understand what is meant by _authority_ in this question.
    2.  I do not believe it is possible to determine the authority of a page returned by a search engine.
    3.  I check to see if the author of the page has published other pages, articles, or books about the topic.
    4.  I look for information about the author of the page - their qualifications, their degrees or certifications, the profession, their background, other pages/documents they have authored
    5.  I look at the URL of the site, and based on the domain (.com,.edu, .org, .net, etc.) and use that information to help me determine whether or not the site has authority.
    6.  I do not examine the authority of a site
    7.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
25. How do you evaluate the _currency_ of a page and determine how current the information on the site is? Check all that apply.
    1.  I do not understand what is meant by _currency_ in this question.
    2.  I examine how frequently the site has been updated.
    3.  I look for a date on the site
    4.  I do not believe it is possible to determine if the information returned by a search engine is current.
    5.  I do not evaluate the currency of a site
    6.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
    7.  I look for a copyright date
    8.  I look at the dates of the references
    9.  I check to see if all the links are still active (broken links will lead me to believe the site is not current)
26. How do you evaluate the _purpose_ or bias of a web site? Check all that apply.
    1.  I do not understand what is meant by _purpose_ in this question.
    2.  I determine whether or not the author of the page or the owner of the URL is trying to sell something.
    3.  I examine whether or not the purpose of the site is to promote a particular opinion or point of view
    4.  I examine whether or not the site is a spam, hoax or joke
    5.  I do not evaluate the purpose of a site.
    6.  I do not believe it is possible to determine the purpose of a page returned by a search engine.
    7.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
27. How do you evaluate the _relevance_ of a page returned by a search engine? Check all that apply.
    1.  I do not understand what is meant by _relevance_ in this question
    2.  I determine whether the page is useful for the research I am doing
    3.  I can understand the information on the page
    4.  I determine whether or not the information on the page adds to my knowledge on the topic
    5.  I do not believe it is possible to determine the _relevance_ of a page returned by a search engine.

### Appendix B – Survey questions and Association of College and Research Libraries information literacy competency standards for higher education (2000)

Questions 1 - 8 are demographic and not mapped to standards.

<table>

<tbody>

<tr>

<td>

**Question in survey instrument**</td>

<td>

**ACRL IL competency standard/performance indicator outcomes from** (Association of College and Research Libraries, 2000)</td>

</tr>

<tr>

<td>

**Question 9:** When a search engine returns a list of pages, I select a page from the list based on the following criteria. Check all that apply.

1.  the format of the site
2.  whether or not the site has video
3.  the site has pictures
4.  the site has a lot of information
5.  the site is understandable
6.  I am familiar with the author of the site
7.  I can verify the information on the site
8.  the credentials (qualifications) of the author are good
9.  the site is current
10.  the quality of the writing on the site is good
11.  k.) there are reviews and comments on the site

</td>

<td>

1.2c: The information literate student identifies the value and differences of potential resources in a variety of formats (e.g., multimedia, database, website, data set, audio/visual, book)

2.4a: The information literate student assesses the quantity, quality, and relevance of the search results to determine whether alternative information retrieval systems or investigative methods should be utilised

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias

</td>

</tr>

<tr>

<td>

**Question 10:** Consider the papers that you have written in the last year. How often have you used research tools beyond Google or Ask.com to do your research?</td>

<td>

2.2e: The information literate student implements the search strategy in various information retrieval systems using different user interfaces and search engines, with different command languages, protocols, and search parameters.

2.3: The information literate student retrieves information online or in person using a variety of methods.

2.3a: The information literate student uses various search systems to retrieve information in a variety of formats.

</td>

</tr>

<tr>

<td>

**Question 11:** How would you know when you have enough sources for a paper?</td>

<td>

1: The information literate student determines the nature and extent of the information needed.

3.4 a: The information literate student determines whether information satisfies the research or other information needed.

</td>

</tr>

<tr>

<td>

**Question 12:** Consider the papers that you have written in the last year. How many pages of search engine (Google, Yahoo, Bing) results do you usually view?</td>

<td>

1: The information literate student determines the nature and extent of the information needed.

3.4 a: The information literate student determines whether information satisfies the research or other information need.

3.7c: The information literate student reviews information retrieval sources used and expands to include others as needed.

</td>

</tr>

<tr>

<td>

**Question 13:** Which search engines or library databases do you use for your research? Check all that apply.

1.  Yahoo
2.  Bing
3.  Google
4.  Other search engine
5.  Metasearch engine
6.  General library database
7.  Subject specific library database
8.  Library one search
9.  Library book catalogue

</td>

<td>

2.1d: The information literate student selects efficient and effective approaches for accessing the information needed from the investigative method or information retrieval system.

2.2e: The information literate student implements the search strategy in various information retrieval systems using different user interfaces and search engines, with different command languages, protocols, and search parameters.

2.3: The information literate student retrieves information online or in person using a variety of methods.

</td>

</tr>

<tr>

<td>

**Question 14:** It is usually possible to determine the author of a Web page.</td>

<td>

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4e: The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions.

</td>

</tr>

<tr>

<td>

**Question 15:** It is usually possible to determine the _qualifications_ of the author of a Web page.</td>

<td>3.4e : The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions</td>

</tr>

<tr>

<td>

**Question 16:** It is usually possible to determine whether or not a site is _authoritative_.</td>

<td>3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.</td>

</tr>

<tr>

<td>

**Question 17:** It is usually possible to determine whether or not a Web page is _objective_ and _unbiased._</td>

<td>

1.2d: The information literate student identifies the purpose and audience of potential resources.

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

</td>

</tr>

<tr>

<td>

**Question 18:** It is usually possible to determine the _credibility_of a Web page.</td>

<td>

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

3.4e: The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions.

</td>

</tr>

<tr>

<td>

**Question 19:** I believe the pages listed in search engine results usually contain accurate information.</td>

<td>

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

3.4e: The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions.

</td>

</tr>

<tr>

<td>

**Question 20:** How do you decide whether or not a source retrieved from the Internet is **objective** and provides fair and equal treatment of all sides of a topic? Check all that apply.

1.  I do not understand what is meant by objective in this question
2.  I believe that pages returned by my search engine are objective
3.  I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) use that information to help me determine whether or not the site is objective
4.  I check with someone who may know, for example library staff or a professor
5.  I ask a friend if they think the site is objective.
6.  If the document provides a fair discussion of all sides of a topic or issue and acknowledges other viewpoints, then I consider it objective
7.  I do not evaluate the objectiveness of a site
8.  I do not believe it is possible to determine the objectiveness of a page returned by a search engine

</td>

<td>

3.2: The information literate student articulates and applies initial criteria for evaluating both the information and its sources.

Outcomes include:

1.  Examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias
2.  analyses the structure and logic of supporting arguments or methods
3.  Recognises prejudice, deception, or manipulation
4.  Recognises the cultural, physical, or other context within which the information was created and understands the impact of context on interpreting the information

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

3.4e The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions.

3.6: The information literate student validates understanding and interpretation of the information through discourse with other individuals, subject-area experts, and/or practitioners.

3.6c: The information literate student seeks expert opinion through a variety of mechanisms (e.g., interviews, email, listservs).

</td>

</tr>

<tr>

<td>

**Question 21:** How do you decide whether or not information on a page is **accurate** and contains truthful and correct information? Check all that apply.

1.  I do not understand what is meant by **accurate** in this question
2.  I believe that pages returned by my search engine are accurate
3.  I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) use that information to help me determine whether or not the site is accurate
4.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
5.  I believe that sites with a more current date are more accurate
6.  I do not check the accuracy of information on a Website
7.  I evaluate the information on the page in relation to what I know about the topic
8.  I do not believe it is possible to determine the accuracy of pages returned by a search engine
9.  I ask a friend if they think the page is **accurate**

</td>

<td>

3.2 The information literate student articulates and applies initial criteria for evaluating both the information and its sources.

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4: The information literate student compares new knowledge with prior knowledge to determine the value added, contradictions, or other unique characteristics of the information.

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

3.4g: The information literate student selects information that provides evidence for the topic.

3.6c: The information literate student seeks expert opinion through a variety of mechanisms (e.g., interviews, email, listservs).

</td>

</tr>

<tr>

<td>

**Question 22:** How would you evaluate the **credibility** of a site or document and determine that the information is truthful and trustworthy? Check all that apply.

1.  I do not understand what is meant by **credibility** in this question.
2.  I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) use that information to help me determine whether or not the site is credible.
3.  I look at the background of the author of the page - their professional affiliations, their college degrees, what else they have written.
4.  I check to see who published the information on the site
5.  I believe that pages returned by my search engine are credible.
6.  I do not evaluate the credibility of Websites.
7.  I do not believe it is possible to evaluate the credibility of pages returned by a search engine
8.  I evaluate the information on the site against what I know about the topic

</td>

<td>

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

3.4e: The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions.

</td>

</tr>

<tr>

<td>

**Question 23:** How do you evaluate the **quality** of a page returned by a search engine? Check all that apply.

1.  I do not understand what is meant by **quality** in this question.
2.  If the page includes pictures and charts it is a quality site
3.  If the page is free from spelling, typographical errors and the author uses good grammar, I consider it a quality site.
4.  If the information presented in the site is comprehensive and covers the topic in considerable depth, I consider it a quality site.
5.  I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) use that information to help me determine whether or not the site is a quality site.
6.  I do not evaluate the quality of pages returned by a search engine.
7.  If the information on the page is interesting, and presents a clear, well-reasoned explanation of the topic, I consider it a quality page.
8.  I do not believe it is possible to determine the quality of a page returned by a search engine.
9.  I check with someone with knowledge of the site or topic, for example, library staff or a professor

</td>

<td>

3.2: The information literate student articulates and applies initial criteria for evaluating both the information and its sources.

3.4: The information literate student compares new knowledge with prior knowledge to determine the value added, contradictions, or other unique characteristics of the information.

3.4b: The information literate student uses consciously selected criteria to determine whether the information contradicts or verifies information used from other sources.

3.4e: The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information gathering tools or strategies, and the reasonableness of the conclusions.

</td>

</tr>

<tr>

<td>

**Question 24:** How do you evaluate the **authority** of a page and determine the ability to comment or write about a particular topic? Check all that apply.

1.  I do not understand what is meant by **authority** in this question.
2.  I do not believe it is possible to determine the authority of a page returned by a search engine.
3.  I check to see if the author of the page has published other pages, articles or books about the topic.
4.  I look for information about the author of the page - their qualifications, their degrees or certifications, their profession, their background, other pages/documents they have authored
5.  I look at the URL of the site, and based on the domain (.com, .edu, .org, .net, etc.) use that information to help me determine whether or not the site has authority.
6.  I do not examine the authority of a site
7.  I check with someone with knowledge of the site or topic, for example, library staff or a professor

</td>

<td>

3.2: The information literate student articulates and applies initial criteria for evaluating both the information and its sources.

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.4e The information literate student determines probable accuracy by questioning the source of the data, the limitations of the information-gathering tools or strategies, and the reasonableness of the conclusions.

3.6c: The information literate student seeks expert opinion through a variety of mechanisms (e.g., interviews, email, listservs).

</td>

</tr>

<tr>

<td>

**Question 25:** How do you evaluate the **currency** of a page and determine how current the information on the site is? Check all that apply

1.  I do not understand what is meant by **currency** in this question.
2.  I examine how frequently the site has been updated.
3.  I look for a date on the site
4.  I do not believe it is possible to determine if the information returned by a search engine is current
5.  I do not evaluate the currency of a site
6.  I check with someone with knowledge of the site or topic, for example, library staff or a professor
7.  I look for a copyright date
8.  I look at the dates of the references
9.  I check to see if all the links are still active (broken links will lead me to believe the site is not current)

</td>

<td>

3.2: The information literate student articulates and applies initial criteria for evaluating both the information and its sources.

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.6c: The information literate student seeks expert opinion through a variety of mechanisms (e.g., interviews, email, listservs).

</td>

</tr>

<tr>

<td>

How do you evaluate the **purpose** or bias of a Website? Check all that apply.

1.  I do not understand what is meant by **purpose** in this question.
2.  I determine whether or not the author of the page or the owner of the URL is trying to sell something.
3.  I examine whether or not the purpose of the site is to promote a particular opinion or point of view
4.  I examine whether or not the site is a spam, hoax or joke
5.  I do not evaluate the purpose of a site.
6.  I do not believe it is possible to determine the purpose of a page returned by a search engine.
7.  I check with someone with knowledge of the site or topic, for example, library staff or a professor

</td>

<td>

1.2d: Identifies the purpose and audience of potential resources (e.g., popular vs. scholarly, current vs. historical).

3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.

3.2c: The information literate student recognises prejudice, deception, or manipulation.

</td>

</tr>

<tr>

<td>

**Question 27:** How do you evaluate the **relevance** of the documents you retrieve from the Web?

1.  I do not understand what is meant by **relevance** in this question
2.  I determine whether the page is useful for the research I am doing
3.  I can understand the information on the page
4.  I determine whether or not the information on the page adds to my knowledge on the topic
5.  I do not believe it is possible to determine the **relevance** of a page returned by a search engine.

</td>

<td>3.2a: The information literate student examines and compares information from various sources in order to evaluate reliability, validity, accuracy, authority, timeliness, and point of view or bias.</td>

</tr>

</tbody>

</table>

</section>