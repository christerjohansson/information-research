<header>

#### vol. 18 no. 4, December, 2013

</header>

<article>

# A practice theoretical exploration of information sharing and trust in a dispersed community of design scholars

#### [Ola Pilerot](#author)  
Swedish School of Library and Information Science, University of Borås, SE-501 90 Borås, Sweden

#### Abstract

> **Introduction.** This paper presents an exploration of information sharing and trust in a geographically dispersed network of design scholars.  
> **Method.** The study used a practice theory approach to identify aspects of trust in relation to information sharing. The empirical material consists of 15 in-depth interviews with design scholars from four Nordic countries and field notes from workplace visits.  
> **Analysis.** The interview transcripts and field notes were categorised in accordance with three themes derived in synergy from practice theory and the empirical material.  
> **Results.** A number of strategies for assessing and creating trust in relation to information sharing were identified. Depending on the dimension of practice in analytical focus, different aspects of trust emerge.  
> **Conclusions.** Trust issues connected to information sharing appear in relation to the information to be shared, the people involved, the tools used for sharing, and the place where information sharing occurs. The practice-theoretical perspective has proven effective in order to identify and capture the elusive phenomenon of trust in connection to information sharing.

<section>

## Introduction

Information sharing activities are difficult to clearly demarcate from other activities, such as writing, reading, and the seeking and use of information. There is an abundance of studies of information seeking in various groups and contexts (e.g., [Case 2007](#cas07)). There are also several studies of collaborative information seeking in academe (e.g., [Foster 2006](#fos06)). Regarding contributions that primarily focus on information sharing in scholarly work practices there is, however, a relative dearth of research. During the last decade a mere handful of papers that solely focuses on this theme have been published (e.g., [Talja 2002](#tal02); [Haeussler _et al._ 2010](#hae10); [Pilerot and Limberg 2011](#pil11); [Tabak and Willson 2012](#tab12)). It is often claimed that progress in research depends on researchers' willingness to share information. Merton [(1942/1973: 270-277](#mer73)) for example, portrays the results of scientific production as a '_common heritage_' which should be accessible for all participants in the research community; whether that is the case is a question that can be related to the issue of trust ([Shapin 1994](#sha94)). This paper deals with information sharing in relation to trust in a geographically dispersed network of design scholars.

On a general level, scholarly work ([Palmer and Cragin 2008](#pal08)) can be described as a collective enterprise that aims to develop credible knowledge and novel insights about the world. Empirically- and theoretically-based explorations of certain facets and aspects of the world is at the core of the activities carried out within a research community. The creation of legitimate knowledge presupposes information to be shared between the members of the community. Through formal and informal communication; for instance in the shape of dissemination of scholarly literature and participation in conferences, through workplace meetings and corridor talk, researchers obtain and furnish others with information which they deem necessary for their work. Research thus constitutes a practice in which:

> [d]ifferent members of a community hold knowledge that individuals may need to draw upon in order to perform practical actions: to maneuver in the material world, to confirm the status of their knowledge, to make new knowledge. Even to be sceptical about existing items of knowledge ([Shapin 1994: 7](#sha94)).

Seventy years ago (1942) Robert Merton presented his highly influential account of the norms underpinning research work. The conception of the researcher as an altruistic knowledge seeker concerned with '_benefit to humanity_' ([Merton 1942/1973](#mer73): 270-77) has since then been disputed. Ian Mitroff ([1974](#mit74)) put forth a set of '_counternorms_' in which he claims that researchers are expected by their colleagues '_to achieve the self-interest they have in work-satisfaction and in prestige through serving their special communities of interest_' ([Mitroff 1974: 592](#mit74)).

No matter what set of norms we take to be reasonable, both Merton's and Mitroff's contributions clearly indicate that research work is a social practice infused with moral issues. Since researchers must assent to, or question, their peers' assertions about the world, communicated for instance by means of shared documents and oral statements, members of a research community are morally bound to each other. As stated by Cronin: '_The conventions for evaluating research may have changed in the last few centuries, but trust, a manifestation of the "normative ghost in the scientific machine",... remain central to the conduct of science in general_' ([Cronin 2003: 12](#cro03)). For information sharing to take place, people in a research community need to trust each other. However, as Shapin ([1994: 16](#sha94)) points out, '_the role of trust and authority in the constitution and maintenance of systems of valued knowledge has been practically invisible_'.

The theoretical point of departure for this study is rooted in practice theory (e.g., [Schatzki 2002](#sch02); [Kemmis 2011](#kem11)). A practice theoretical approach to the study of information sharing entails a focus not only on the site where activities take place but also on the nexus of the stuff and people involved, and their doings and sayings ([Schatzki 2002](#sch02)). This paper introduces the idea of investigating trust in relation to information sharing in accordance with the practice theoretical notions of _cultural-discursive_, _social_, and _material-economical_ dimensions of practice, identified by Kemmis ([2011](#kem11)), an idea that corresponds with previous studies of information sharing in academia. It has, for example, been suggested that '_for future research it would be of value, but also a challenge, to try to elaborate on an integrative theoretical basis for a potential framework for the study of the many aspects of information sharing activities; a framework that would include people, places, and information_' ([Pilerot 2012](#pil12), cf. [Talja 2002](#tal02)).

Thus, the study aims at a theoretical contribution through the introduction of practice theory into the field of information sharing research. The purpose is to identify and elucidate trust in relation to information sharing within a complex and heterogeneous community of design scholars. The objectives will be met by exploring the following research questions, with regard to the three dimensions of practice:

1.  Where do trust issues emerge in relation to information sharing in this complex and heterogeneous network?
2.  What are the scholars' strategies for assessing and creating trust?
3.  How can practice theory contribute to elucidate information sharing?

The paper will add to the literature on information sharing and trust by presenting a study which is located in a multifaceted environment inhabited by a geographically dispersed network of design scholars, the [Nordcode](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.nordcode.net&date=2013-10-06) network. The research questions will be explored through a close study of empirical material primarily consisting of transcripts from interviews with scholars within an organized network in the field of communicative design research.

The subsequent section contains a brief account of the field of design research and a presentation of the research network under study. This is followed by a literature review on trust in general and in information science in particular. Before the results are presented, the theoretical perspective and the applied method are explained. The article ends by discussing the theoretical contribution, the empirical findings, and by contrasting the findings with previous literature. The discussion is rounded off by a conclusion.

## Research context

In its interdisciplinary activities design studies seeks to develop historical, theoretical and critical knowledge about design itself and design practice ([Boradkar 2010: 279](#bor10)). Art, science and technology have historically been seen as the three '_primary dimensions of design_' ([Boradkar 2010: 284](#bor10)), but these dimensions have gradually, over the years, expanded to also encompass other disciplines ([Friedman 2003](#fri03)). The perception of design research as a fragmented field is emphasized by Margolin ([2010](#mar10): 77), who calls for the identification of a group of core texts which he claims is missing: '_The purpose of such texts within a research community is to constitute **a common heritage** to reinforce the idea that design researchers are engaged in a shared enterprise, no matter how diverse their interests_' (my emphasis; cf. [Merton 1942/1973](#mer73)). Design research houses communities with both central and peripheral participants. Communities where the participants are active in an environment that is characterized by heterogeneity regarding disciplinary background, nationality, language, geographical location, research group size, research focus, publication strategy, academic experience and merit, gender, and age.

### Nordcode

In this multifaceted and heterogeneous design-research field (e.g., [Cross 2007](#cro07)) the Nordcode network builds upon a shared interest in communicative aspects of artefacts and the aesthetic qualities of physical products and objects. The network was initiated in about 2000 by a small group of scholars with common research interests. Today, the network comprises approximately 100 members located at eight universities in four Nordic countries. The main discussion forum is the annual Nordcode seminar. In addition to these seminars, workshops as well as courses for doctoral students are arranged. The extent to which people are active in the network varies. As can be seen on the Nordcode website, there is an explicit interest within the network to develop and strengthen information sharing activities. It is stated that the network is '_characterized by activities based on research cooperation, doctoral education, **information exchange**, workshops, seminars, and publications_' ([Nordcode, n.d.](#nornd), my emphasis). Even though the members of Nordcode belong to the same network, their disciplinary backgrounds differ. While they see themselves as design researchers they might also identify with disciplines such as, for instance, engineering, art history, sociology, or semiotics. The group of people studied is of a particular interest in this respect, as it is a group that makes up an epistemologically and socio-culturally amalgamated network of scholars, in which there might exist a variety of historically and socio-culturally shaped traditions and conventions about how information is searched for, used and shared.

## Literature review

The notion of trust is widely discussed within several research areas (e.g., [Berry 2011](#ber11); [Lewis and Weigert 2012](#lew12)). It is an issue which is dealt with in the literatures of management and organizational science, (e.g., [Bachmann 2011](#bac11); [McAllister 1995](#mca95); [Dirks and Ferrin 2001](#dir01), [Levin and Cross 2004](#lev04)), information systems (e.g., [Jarvenpaa, _et al._ 2004](#jar04)), social psychology (e.g., [Tanis and Postmes 2005](#tan05)), and sociology (e.g., [Luhmann 1979](#luh79); [Giddens 1990](#gid90); [Ward and Meyer 2009](#war09)).

Trust has been described as a '_lubricant for social and economic transactions_' ([Ermish _et al._ 2009: 749](#erm09)). It has been presented as crucial for work organization (e.g., [Paliszkiewicz 2011](#pal11)) and for sustaining political relationships (e.g., [Hooghe and Zmerli 2011](#hoo11)). Although it is generally agreed that trust is beneficial, the ways in which it is beneficial is not always apparent. Dirks and Ferrin ([2001](#dir01)) review research contributions that propose two different models. One, which is the most common, suggests that trust results in positive attitudes, higher levels of cooperation, and more effective performance; it is thus directly beneficial. The other model describes indirect benefits and suggests that trust in an organization facilitates the conditions under which certain outcomes (for example cooperation) are likely to occur.

In the trust literature, a prominent and profound distinction can be identified between, on the one hand, those authors who mainly regard trust as a phenomenon that should be understood and investigated as a micro-level phenomenon, and, on the other hand, those who advocate an institutional, macro-level perspective on trust ([Bachmann 2011](#bac11)). The former perspective tends to focus on trust as a psychological, calculative or moral issue important for the relationship between two individuals, whereas the latter, macro-perspective, conceptualises trust as '_a decision that embedded social actors make in the light of specific institutional arrangements_' ([Bachmann 2011: 207](#bac11)). This is a distinction that can be traced back to one of the most influential authors in trust research, Luhmann ([1979](#luh79)) who separates personal trust from system trust. System trust is a kind of trust that does not depend on the existence of an inter-personal relationship, but rather as accorded to '_abstract capacities thought to reside in certain institutions_' ([Shapin 1994: 411](#sha94)). Regarding inter-personal trust, McAllister ([1995](#mca95)) contrasts cognitive and affective foundations for trust (cf. [Swift and Hwang 2013](#swi13): 25) where the former is related to '_individual beliefs about peer reliability and dependability_' and the latter to '_reciprocated inter-personal care and concern_'. An assertion which is widespread in the literature is that trust is '_the willingness of a party to be vulnerable to the actions of another party_' ([Mayer, Davies and Schoorman 1995: 712](#may95)). Several authors also emphasize that trust enables people to take risks (e.g., [Luhmann 1979](#luh79); [Möllering 2001](#mol01)). Wittel ([2001](#wit01): 67) introduces the concept of network sociality. This includes the '_erosion of enduring friendships, responsibility and trust_', which in turn leads to a possible reconfiguration of trust as '_inscribed in informational social bonds based less in hierarchical relations and more in the complex, reciprocal intricacies of the transverse networks of information exchange_'. In such a setting, where the borders between work and play are blurred, trust is likely to be based on brief encounters rather than on prolonged work relations.

### The concept of trust in information science

Even though it has been claimed that there is a lack of studies on trust in the field of information science (e.g., [Kelton _et al._ 2008](#kel08)), it has been established in previous studies that trust plays a central role in cooperative work (e.g., [Hertzum 2002](#her02)) and in relation to knowledge management (e.g., [Iivonen and Houtari 2000](#iiv00)). He _et al._ discuss '_knowledge seeking_' through knowledge management systems in relation to the issue of trust. They are mainly occupied with trust on a collective level, which '_unlike the traditional dyadic form, does not depend on the existence of an interpersonal relationship and may occur between many unfamiliar, even anonymous, users_' ([He _et al._ 2009](#he09): 529); a perception of trust that resembles Luhmann's idea of system trust ([1979](#luh79)). Kling and McKim ([2000](#kli00)) conclude that trust plays a central role in relation to the use of electronic media in scientific communication.

Wilson ([2010](#wil10)), in his review of information sharing research, concludes that trust is a recurrent feature in this literature. Within information science, however, there are a limited number of studies that specifically focus on information sharing and trust ([Ibrahim and Allen 2012](#ibr12)). Among the few is the study by Chai and Kim ([2010](#cha10)) which focuses on the blogosphere and the relationship between trust and the information sharing that takes place there. Ibrahim and Allen report an empirical study of information sharing and trust during major incidents within the oil industry. The last-mentioned propose a '_counterintuitive relationship between information sharing and trust_' ([2012](#ibr12): 1921), which means that information sharing can instill trust. This contrasts the more common (in previous literature) standpoint, which assumes that trust is a ground for information sharing. Kelton _et al._ are concerned with trust in relation to digital information. Trust is positioned as a key mediator between the quality of information and information use. The authors suggest a '_change in focus from attributes of the information itself to the perceptions of the person who is using the information_' ([2008](#kel08): 371). The study by Van House in which she examines the '_role of the knowledge community in the identification of trustworthy others_' ([2002](#van02): 103) is closely related to this paper. She draws upon Davenport and Cronin's ([2000](#dav00)) notion of '_epistemological trust_' and addresses the question: what are the criteria for determining credible people? With reference to the literature she points out competence and honesty. Supported by her own empirical findings she adds '_shared orientations and values_'. Van House concludes that with regard to epistemic trust, a key method to assess trust is to verify membership in an epistemic community.

## Theoretical perspective

The overarching theoretical framework guiding the study is grounded in practice theory. Studies of practices are a topical area within information science (e.g., [Cox 2012](#cox12); [Haider 2012](#hai12); [Huizing and Cavanagh 2011](#hui11); [Lloyd 2010](#llo10); [Talja 2010](#tal10)). There is not one practice theory, but rather several ways to conceptualize and study practices (e.g., [Schatzki _et al._ 2001](#sch01); [Gherardi 2006](#ghe06); [Kemmis 2011](#kem11); [Feldman and Orlikowski 2011](#fel11)). Nevertheless, it is possible to discern a field of practice based theorizing where several more or less related theoretical frameworks can be traced. For instance, the idea of knowing and learning in practice as a collective enterprise, which is central for situated learning theory (e.g., [Lave and Wenger 1991](#lav91)), is encompassed by practice theory as well. One of the key ideas of activity theory (e.g., [Nardi 2007](#nar07)) is that an object of work is simultaneously given and emergent, which reveals activities' tentative nature. Such a stance can also be subsumed in a practice-based inquiry. Similar to actor-network theory (e.g., [Latour 2005](#lat05)), which strives towards a reconfiguration of agency, '_theories of practice assume an ecological model in which agency is distributed between humans and non-humans_' ([Gherardi 2009](#ghe09): 115). Practice-based theorizing can also be related to the field of workplace studies (e.g., [Heath _et al._ 2000](#hea00)) in which conversation and technology-mediated interaction is in focus. All the individual theoretical strands that have been mentioned here are important contributions to the study of peoples' doings and sayings. However, the unified perspective offered by the practice-theoretical stance has been valued as having a greater potential to reveal important aspects of the multifaceted concept of trust in relation to scholarly information sharing.

By exploring various relevant literatures with bearing on practice theory, Kemmis ([2011](#kem11): 139) presents a '_"map" of a conversation-space_' which covers a number of dimensions that are crucial in order to understand and explore practices. With a persistent focus on trust in relation to information sharing, three dimensions of practice will be elucidated in the analysis:

1.  The cultural-discursive dimension of practice comprises norms and conventions and the actions prescribed to them. It also relates to how people are expected to talk and use language in the practice. Drawing upon cultural resources entails acting in accordance with certain socially and historically shaped ways of how to go about doing things. Researchers are, for instance, expected to acknowledge their use of other researchers' ideas. This is mainly done through the cultural act of citing. When writing researchers draw on discursive resources by conforming to how articles should be written according to the norms and conventions in the practice.
2.  The social dimension of practice refers to how people interact, form relations, experience identity, and perceive a sense of belonging.
3.  The material and economic dimension of practice comprises for example information and communication technologies, furnishing and lay-outs of buildings. These may offer opportunities or obstacles for information sharing, and also be included in what constitutes the ground for the assessment of trust in peers and informative objects. Researchers are also expected to act in accordance with certain economic arrangements which are expressed, for instance, through funding systems, conference fees, travel costs, and also through the publishing system of academia.

On the basis of the theoretical assumptions that have guided this study, it should be pointed out, however, that the analysis of a social practice is a delicate matter. The notion of practice entails an understanding of the activities and the material entities under study as a dynamic and diffuse whole. It is inevitable, therefore, that the identified practice dimensions tend to merge even if they are scrutinized individually.

An additional theoretical notion, finally, which is of importance for this study is intersubjectivity. Although rarely used in practice-theoretical accounts, here it is useful in the discussion of shared understanding, an issue central to the matter of information sharing. People occupy '_interworlds_' ([Crossley 1996](#cro96)), i.e., intersubjective spaces where resources such as language, ideas and interests, are shared. However, different actors are ascribed different kinds of legitimacy depending on inter-subjective agreements based upon conventions, norms and traditions historically rooted in systems of trust, credibility and rank. A prerequisite for people to establish inter-subjectivity, according to Mortimer and Wertsch ([2003](#mor03)), is that interlocutors are able to fathom the same speech genre ([Bakhtin 1986](#bak86)). A perceived sense of a common epistemic culture ([Knorr-Cetina 1999](#kno99)) also facilitates inter-subjectivity, or as eloquently elaborated by Bazerman: '[e]ach person entering the discursive complexes of a scientific field must learn to cope with those communicative means and processes that mediate participation with others' ([1997](#baz97): 305).

Although it is reasonable to assume that people can never fully achieve perfect inter-subjectivity, even if they are in the same research community, there is always the anticipation that communication is possible. It is only then that at least some overlapping can be accomplished.

## Research methods

A methodological concern, which is particularly pertinent in relation to the practice theoretical lens that has guided this study, is the issue of how to explore and study practices. Schatzki proposes a blended strategy for this purpose. He claims that language is a crucial access point to activities and practices: '_[u]nderstanding people's words for activities and practices thus provides access to the activities and practices that make up their practice-arrangement bundles_' ([2012](#sch12): 24). He also strongly recommends '_hanging out with, joining in with, talking to and watching, and getting together the people concerned_' (p. 25). The study of relevant documents, about the practice in question, is also suggested. On the basis of these recommendations, it has been deemed appropriate to emphasize the interview as a prime means of gathering empirical material. In addition, numerous visits to several of the researchers' workplaces have been of importance for developing a sense for and knowledge about the practice under study. Relevant documents, such as the network Website, personal Websites, and documentation from seminars and meetings, have also been consulted.

Of crucial importance for the selection of study participants was that those recruited had recent experiences of participation in Nordcode activities. The participants were selected with the aim of forming a critical mass; i.e. ,a large enough number of people to talk to in order to produce a material rich enough to make it possible, on a sophisticated level, analyse that which is under study. Variation regarding features such as academic rank, epistemological background, and geographical location, has been striven for in order to ensure a thick description of the object under study. That a sufficient number of participants had been consulted was indicated when the investigation resulted in a satisfactory '_saturation_' (cf. [Saumure and Given 2008](#sau08)). Saturation was signified when the empirical material was deemed rich enough to carry out a meaningful analysis.

The interviewed researchers include six senior researchers, six PhD students, and three recently graduated PhDs. Nine are women, six are men. All fifteen study participants are design researchers connected to Nordcode, which means that they have participated in at least one of the annual Nordcode seminars. Being a design researcher means that they have carried out research work, in connection to doctoral studies or to post graduate projects, and published in scholarly journals and/or conferences. The participants are located in seven universities in four Nordic countries. Among the participants there is a group of six from one of the universities. There are also representatives in the sample from departments with only one Nordcode member. Some participants are more centrally located in the network than others, who have peripheral positions. There is, for instance, a steering group consisting of country representatives, a leader, and two network co-managers. These people, of which five were interviewed, are considered to be centrally located in the network, whereas people that have only occasionally visited one or a couple of seminars are more peripheral.

Apart from four interviews that where conducted over the telephone (including two Skype interviews), all of the interviews took place in the participants' workplaces, face to face. The interviews, which were recorded, lasted for approximately one and a half hours. The semi-structured interviews were carried out with the help of a thematically ordered interview guide, which also included background and round-up questions. Three overlapping themes were addressed: 1) situation and context in focus (tools for information sharing and the location or site where the sharing activities take place); 2) information in focus (that which is shared); and 3) people in focus (those who are sharing).

All interviews were fully transcribed and analysed in accordance with qualitative content analysis ([Zhang and Wildemuth 2009](#zha09)). The unit of analysis was themes derived in synergy from practice theory and the analysed data. Initially, open coding was applied in order to identify salient themes in the material. The analytical process then continued through constant comparative method ([Corbin and Strauss 2008](#cor08)) involving a systematic comparison of each passage assigned to a specific theme with those already assigned to that theme. In this way, an analytical process moving from descriptive to more theoretical levels unfolded (cf. [Kvale and Brinkmann 2009](#kva09): 202). In short, the analysis can be described as based on recurrent close reading of the empirical material with a focus on instances related to trust and information sharing. Excerpts from the empirical material are frequently used to illustrate and support the analysis. The author's theoretical pre-understanding and engagement with the setting under study, including the empirical material, has resulted in analytical themes in harmony with the interpretations guided by practice theory. These themes where then considered in relation to previous literature.

## Results

The following account strives for an identification of salient characteristics of the aforementioned practice dimensions. The thematic presentation is interposed with illustrative excerpts from the empirical material. (Five of the interviews were carried out in English, the other ten in Swedish. The participants are numbered from P1 to P15\. All quotations are reported anonymously and verbatim, those originally in Swedish have been translated into English by the author.)

### Cultural-discursive dimension of practice

An example of how people conform to the cultural-discursive dimension of practice is illuminated by the following excerpt in which a PhD student accounts for how to go about assessing an article that someone has given him:

> If I take an example, you start by reading... it's usually the title that appears first... so you read the title, the abstract and then you go straight down to the reference list in order to find out if it is an article that has a philosophical ground similar to mine, if there are any no-no names in the list or if there are good names that suits your own approach. And then you begin to understand, ok, this might be of interest and then you carry on reading the article (P5).

To assess recommended texts, the participant refers to the issue of philosophical ground, which is partly represented here by the reference to no-no names. The notion of no-no names is used to indicate philosophical or theoretical stances that are deemed to be more or less aligned with what is assumed to be the “right” (“yes-yes”) philosophical or theoretical stance for this practice. Interesting to note is that the PhD student cited above is possibly still a novice and in the process of developing a communicative strategy that make participation with others possible (cf. [Bazerman 1997](#baz97)). As can be seen in the subsequent quote, there are other, more experienced, network members who have developed other approaches to assessing texts:

> the only thing that counts for me, [is] what kind of argument are they presenting, how are they presenting it, and if they are not presenting this seriously, that would then probably be a no-no (P7).

Even though the more experienced researcher reacted against the assessment strategy presented by the PhD student, it is important to note that both of them rely on what can be described as a cultural-discursive dimension of practice when assessing texts. In the latter case it is explicitly the conventions for argumentation that are in focus, whereas the PhD student refers to implicit philosophical grounds that are deemed appropriate.

The fragmented character of the area of design research seems to challenge the establishment of trustworthiness and the assessment of credibility, as can be noted in the following excerpt:

> it's such a conglomerate of areas that differ greatly from each other, such a great diffusion, very complex. There are areas that you find very credible with high scientific quality, but there are also areas that you can really question with regard to quality... (P13).

In the following example the participant continues and fully captures how the cultural-discursive dimension of practice is crucial to the assessment of trustworthiness:

> it has very much to do with the language they use, I think... if it is scientific information, what form... you know, what sort of knowledge are they representing, to what school do they belong, who are they speaking for, or which scientific philosophy do they adhere to... (P13).

These quotations have focused on trust and credibility in relation to cultural-discursive conventions and norms on a general level, but there is also a multitude of passages in the empirical material where trust is discussed in a more explicit manner. The subsequent quote is illuminating in two ways. At the same time as it indicates a problematic, culture-related aspect of research work, which it does by describing what is perceived as a negative ground for information sharing, it also communicates an ideal motive for information sharing:

> ...well firstly he/she would be talking with, not talking at me, like telling how things are but with an open mind, having information but also willing to discuss it and take in information so that it would become a discussion and not a lecture..., genuine interest in the topic, of course and... and not sharing information just to show off or just to look smart but really to... to have some ideas that are thought through (P3).

A fruitful ground for information sharing seems rather to be based in mutual engagement characterised by openness and a willingness to participate in a discussion about the information at hand. A somewhat milder version of boasting, but still perceived as counter to the creation of an inter-subjective space is that of name dropping, which is frequently described negatively in the empirical material.

A participant (P4) reflecting upon what it might be that makes a person seem trustworthy noted that an overly extrovert person might be '_shut off_', whereas the comments of a more unassuming person might be valued.

Both of these situations reveal a norm that is valued in the network being ignored, i.e., that which prescribes a certain degree of humility. There are several instances in the empirical material that relate to a cultural-discursive dimension and indicate what is not considered beneficial to information sharing. One participant (P5) responded to the question about what might contribute to not wanting to share information by referring to '_some self-important person_' and a '_kind of cockfighting_' noting that, '_for being able to share I need a free atmosphere with open attitudes, yes, where you kind of can say what you want_'.

This '_free atmosphere with open attitudes_' is something that seems generally appreciated throughout the network. In contrast to what, according to the empirical material, quite often seems to be the case at some of the bigger conferences in the field, where '_they are discussing... if they should have_ [the conference] _in Columbia_ [University] _because it is so prestigious or if they should have it in Holland, which is not so prestigious_. In the case of the Nordcode conference:

> We never had discussions like that... it didn't matter and that was very relaxing and if you feel relaxed in such a professional situation you are much more likely to contribute something yourself and also to share with the participants. (P7)

As can be seen in the excerpt, there is a direct connection made between a laid-back atmosphere and the willingness to share information.

The creation of trust is presented as a somewhat easier in face to face-meetings rather than digital encounters. There are, however, strategies for accomplishing an inter-subjective space also in digital interactions. For example, participant P12 noted that in an e-mail communication he assessed the correspondent as '_a worthy case_' because he was approached in a '_well-prepared_' and '_solid_' manner.

In a digital interaction focus seems to shift from the person's appearance, their potential charm and, perhaps most importantly, from if what is said makes sense in accordance with the cultural-discursive dimension of practice, and towards more interest in the way a request or an offer is presented. For example, participant P15, appreciated an informal style of approach rather than '_those kinds of very dry and very official e-mails that some people send_'.

To conclude this section on the cultural-discursive dimension of practice it can be asserted that trust can be established through reference to a theoretical stance which is deemed suitable in the practice. A potential sharer can appear trustworthy through the way arguments are presented. In order to be accepted as trustworthy communications are expected to proceed from an interest in the creation of a laid back atmosphere rather than in self-acclaimed merits. In digital interactions the style of writing contributes to the establishment of trust.

### Social dimension of practice

A recurring theme has to do with the creation of an inter-subjective space, which is needed in order for two or more people to meet and share ideas and information. There are a number of accounts in the empirical material about how such a space can come about. As can be seen in the subsequent excerpt, the creation of a space where trust can be established is not solely accomplished by judging the people involved in the interaction. It may well also be grounded in the perception of that which is to be shared:

> ... communication, as you probably know, at least from my point of view, is two... double folded, there is one which goes for the content of the knowledge that you want and the other thing is very much the politeness or getting to know each other,... that's why e-mail correspondence can be very difficult because you only get the factual bit of it, you don't get the contact person to person where you get the trust, find out whether you have a serious person in front of you, you can get that via e-mail but it's more difficult... (P12).

This account indicates that trust is established with a basis both in the '_content of the knowledge_' and in the way it is communicated. Separating the sharer from that which is shared would seem to be problematical. This is also why it seems to be more difficult to establish trust in a digital space, for example via e-mail, because then '_you only get the factual bit of it_'. When interaction is limited to an e-mail correspondence important aspects needed for the creation of an inter-subjective space are missing.

A range of criteria may be applied to the assessment of people's trustworthiness. A widespread approach is to rely on friendship or on impressions from previous interactions. Criteria such as rank and experience are referred to in a number of cases, for example:

> If I hear something from someone who has published a lot in the journals that I am aiming at I will probably listen very carefully to what they have to say or if they are reviewers in those journals that I am aiming at, like professors, reviewers and... in, like, my particular field I definitely listen to more carefully than to others (P6).

Belonging or the sense of being accepted as a member of a collective is also a prominent feature in the foundation of trust. Even if two people have not met before, they can conclude from the sole fact that they are in the same network that they do not need to start from scratch in their co-production of an inter-subjectivity space. In this respect Nordcode functions as a _'stamp of quality on design researchers…it's not as if we are starting from the beginning… we have something in common, we don't have to build up everything from scratch…'_ (P10).

The common identity in this case stems from being in the same network and the issue of creating trust is not a complicated matter; it is already established through the acknowledgment of membership in the network. Nordcode functions as a guarantor. Hence both parties seem to know that it is meaningful and beneficial to enter into an information sharing relationship. However, in relation to the above quote which indicates that a sense of belonging is beneficial for information sharing, the empirical material also reveals that this is not always a straightforward matter. With reference to the discipline of design research in general, the participant asserts that:

> [i]t is not that easy for an outsider to that society of mutual admiration, you know, how do you go about getting inside... you have to be quite cocky...(P5).

There are also passages in the empirical material where positive information sharing interactions are described, but without any explicit explanations of why they are perceived as positive. In the following account it is of particular interest to note how the participant talks about a kind of unvoiced or tacit assumption of mutual trust:

> You don't [share information] with everyone, it's got to do with trust, hasn't it… and integrity… you know, it's like you say in English 'give someone a finger, and they take a hand', so that doesn't happen, or that someone hijacks my ideas and sell them as theirs', kind of, that there is some kind of unvoiced… agreement saying that 'you get this and together with that it might become larger', but the large part is not owned by anyone but us, all of us… (P10).

The interaction described in the quote is evidently based upon a relation built upon trust but it is difficult to pinpoint what constitutes the ground for trust in this case. The participant goes on and tries to answer the question about how trust is created

> It probably takes some time to create and you kind of see how, and if, there is a compliance in this giving and receiving or if it is a one-way street, you know,… but it doesn't have to be mutual, so it has probably more to do with some kind of approach or attitude… if you are a bird, a fish, or in between… (P10).

Apparently, the way it is discussed in this passage, trust is conceived as something that has been developed over time, and it is related to prior information sharing interactions. At the same time, though, it is linked to a certain '_approach or attitude_' which in turn seems to depend on what kind of personality someone has.

As noted, the empirical material strongly indicates that it is almost impossible to separate the sharer from that which is shared. However, there are passages that suggest trust is at least partly established, either by judging that which is given or by judging the giver. For example, here a specific quality of the giver contributes in creating an interest in that which is given:

> Sometimes you get stuff from people that aren't your buddies or you get material from... [people that you know well] but I think I am more inclined to give the material a second chance if I think the person who gave it to me is exciting and interesting (P10).

A recurring theme is the appraisal of conferences as opportunities for information sharing. With regard to the creation of inter-subjectivity, however, conferences may seem challenging, since people who do not necessarily know each other gather at conferences. Whether one perceives that there is a potential for inter-subjectivity depends on a multitude of aspects. A shared identity might contribute to the establishment of inter-subjectivity, but it is not necessarily enough. It also seems to be important that a sense of epistemic kinship is experienced, but not even that might be a strong enough ground to stand upon:

> I suppose I felt in this particular case that my contribution probably would be greater than his, because he was so early in his process (P5).

This statement indicates that it might be of importance for a person who is considering sharing something that the receiver is on a similar level (e.g., knowledge level or stage in the PhD process), otherwise there is a risk giving away something without getting anything back. The lack of trust in the shape of reluctance to take risks (e.g., [Luhmann 1979](#luh79)) emerges in the extract above.

The prime aspect of trust in relation to the social dimension of practice is associated with how relations are formed, where person-to-person contacts are perceived as important for the creation of trust. Moreover, the awareness that the sharer belongs to the same network is put forth as a reason for trusting the sharer, i.e., a shared identity and sense of belonging.

### Material and economic dimension of practice

New technologies are generally perceived as tools that contribute in making work easier and more effective, but as can be seen in the following quotation, they may also bring to the fore issues of trust in relation to information sharing. One participant talked about the use of collective reference management software (such as Zotero, EndNote, and Delicious (for social bookmarking)). Such tools are presented as a support for the collection, organization and sharing of references and documents, but the participant (P4) perceived them as problematic in relation to trust and information sharing, '_you don't want to give away a ready-made package [or] parts that hang together too well. Though I could give away a big lump to someone or single pieces_'. Trust in the system is not apparent here and the system does not offer any '_access points_' (Giddens 1990) that can facilitate the establishment of inter-personal trust.

It is not only the interplay with technology that may contribute to shape the ways information is shared (or not shared), however. There are frequent statements in the interviews about the importance of attending, e.g., conferences '_are nice events [where] you can exchange information and exchange ideas_' (P15) and there are also more detailed accounts of how information sharing can happen in a conference setting. Another participant actually refers to the premises as affording information sharing.

In the following excerpt the participant describes how encounters beneficial for information sharing take shape quite serendipitously, according to how the material surroundings shape activities:

> Sometimes these [encounters] are formed in the lunch queue at a conference… and you are provided with something, like "you should read this, and this"... and this happens with people that you don't know but there is so often something around the encounter which makes it easy, kind of putting a premium on it and this is what the conference is for when it works well (P10)

Like several of the participants, P10 points out how important the informal conference activities are. What is illustrated, however, is that the conference setting offers physical affordances: '_something around the encounter_' for example, as in this case, shaped as a lunch queue, that are generally comprehended by the attendees.

At the same time as conferences are regarded as important for information sharing since they are places where one can become visible and thus susceptible for sharing; they are also related to an economic aspect of practice:

> I only go to conferences where I can present something myself. Two reasons: one is, I need money to go there, that is a prerequisite both for the department and for the foundations that I need to apply for money from, and the second thing is that it gives a quite different visibility. If I just go there anonymously people don't know me, I don't get in touch with people, if I have a paper and give a presentation, I get contact, I find people that I could collaborate with or have discussions with in a quite different way (P12).

It is also apparent how the physical layout of a conference landscape is of importance for the creation of information sharing opportunities. Small conferences differ from big ones, and more specialized events are seen as offering more fruitful sharing encounters:

> it totally depends on the conference, what the theme is and what topic and the size of it… the useful stuff for your own research is… the breaks and talking to people and quite often [at small conferences] it's people that you already know so you can continue with sort of that line but there are also a lot of new people (P15)

Several participants point out the coffee breaks as important. They offer situations that are not restricted by material arrangements to the same extent as lectures and presentations, which are strictly formalized in accordance with historically developed traditions concerning speech turns and seating arrangements. The latter is often conceived as an obstacle to information sharing since sitting in rows does not facilitate interaction to the same extent as a coffee table discussion.

This positive sense of belonging that seems to be evoked by gathering with peers who share the same specialized interest can also be discerned in statements about social media. The examples mentioned in the following extract are seen as disturbing because they offer the (potential) user a sense of belonging to a crowd which is perceived as far too big:

> For example like this LinkedIn or I don't know this Google+ but I just saw that I have to submit my rights for what I publish into Google and I didn't like it so I said no, I don't want to be part of this… and I don't want to be linked with everybody, so I just keep it in a certain frame that is manageable for me and the way I have connections to people (P7).

The excerpt also refers to a legal aspect that appears as an obstacle for participation. The system is not trusted because of a perceived insecurity regarding legal rights. There are also other examples from the interviews of how systems that _could_ be beneficial for information sharing instead turn into obstacles. For instance. how social media such as Facebook can function in an excluding way because not everyone has joined Facebook. As a result some people are left outside of the information sharing community.

From an economic perspective, information sharing can be viewed to a certain extent as the trading of documents on the basis of tacit assumptions about favours and returns of favours, as can be seen in the following statement:

> [O]f course I would like to, maybe sort of see, if the other person could be interesting for me as well, if he or she is doing something that I am interested in, maybe I can also get some information from that person... (P15).

According to the interview transcripts, the material and economical dimensions of practice predominantly bring to the fore issues of trust and information sharing in relation to the use of information and communication technologies and in relation to the physical layout of premises.

When extending the analysis to encompass workplace visits and field notes it can be asserted that in most of the workplaces where the scholars are active, there are material manifestations of the subject area of design research, i.e., design. These manifestations appear in the shape of artefacts that clearly relate to design work. Examples are student exhibitions of design work, such as drawings, prototypes and other objects that relate to the process of designing things and services. There are also examples of collections of specific design objects, such as furniture. In one university the art and design department is located in a building where there are also design galleries and shops. These features are likely to contribute to the creation of a certain atmosphere characterized by design and design work, which may also add to the creation of an inter-subjective space beneficial for information sharing.

## Discussion

This section is structured as follows: it starts with a discussion of the theoretical contribution which is then related to the empirical findings. Before a brief conclusion, the study is contrasted with previous literature.

The principal findings of the present study concern, on the one hand, theory and, on the other, an increased understanding regarding trust in relation to design scholars' information sharing activities. Some of the individual findings generated in relation to the three practice dimensions are not new. It is for example known from previous research that a potential sharer can appear trustworthy through the ways arguments are presented. Even though Wilson ([2010](#wil10)), on good grounds, has concluded that the formulation of a general theory of information sharing is hardly possible, the present study's theoretical stance offers a holistic perspective in which individual findings can be seen and understood in conjunction with one another. The practice lens has permitted an oscillating analytical mode that encompasses both individual sharers and the network in which they are active, the material arrangements, and the interplay in-between. The present study has striven to consider all of these aspects, and also to include the notion of inter-subjectivity. By doing this, it has accomplished the intention of introducing the idea of investigating trust and information sharing in accordance with the notions of cultural-discursive, social, and material-economic dimensions of practice.

Using different dimensions of practice as points of departure it can be asserted that there are a number of strategies applied for assessing trust within the network. Depending on the dimension in analytical focus different aspects of trust emerge.

Trust in relation to the cultural-discursive dimension of practice entails a number of instances that require a specific set of trust assessment strategies. For example, when shared information is assessed in relation to a theoretical stance that is socially approved within the practice, focus is on that which is shared. Scrutinizing reference lists in search of '_no no-names_' or evaluating the way an argument in a text is presented are examples of this type of assessment in the empirical material. However, focus can also be on the sharer. If it is a person who appears reasonable and who conforms to the dominating discourse of the practice, which is manifested in the use of language, there are reasons to assume that the sharer is trustworthy.

In relation to the social dimension of practice, instances that bring trust issues to the fore are the formation of relations, which are facilitated by physical person-to-person contacts and which in turn can establish trust. A sense of belonging and a perceived shared identity also contribute to the creation of trust in relation to information sharing. Shared identity and a sense of belonging can be constituted, for example, by membership in the same research network. Strategies for assessing trust in relation to the social dimension comprise the effort of getting to know people, establishing friendships, seeking out people, and taking previous interactions into account. The identification of a person's position in a hierarchical structure with reference to academic merits may also constitute grounds for trust assessment. It is also implied in the empirical material that trust is dependent on personality; some people simply appear to be more trustworthy than others. In relation to both the cultural-discursive and the social dimensions of practice, the strategy of assessing intrinsic plausibility ([Wilson 1983](#wil83)) emerges, i.e., the identification of a correlation between a new instance and previously developed views upon what signifies a trustworthy person or document.

Features of practice that can be related to a material-economic dimension and that come to the fore in connection to trust assessment with a bearing on information sharing are information technologies, for instance, in the shape of reference management software and social media tools, but also the physical layout of premises. Strategies for managing trust problems in relation to such technologies can be, for example, to consider whom to invite to a reference management system or whatever social media tool to use. It is also indicated that small conferences, rather than big ones, gather audiences that are more likely to conform to one's own interests. The conference venue in itself can also afford opportunities for trusted information sharing depending on the physical layout of premises, where some places afford information sharing more than others do.

It is possible to relate the various instances and strategies to certain strands in the literature on trust. Luhmann's ([1979](#luh79)) distinction between personal and system trust is clearly visible in the present study. System trust is visible in connection to trust issues related to materiality, such as in the example of reference management software, whereas the issue of personal trust dominates the social dimension of practice. It is also possible, in some cases, to apply the distinction made between cognition-based and affect-based trust ([McAllister 1995](#mca95); [Swift and Hwang 2013](#swi13)). The latter is particularly relevant to some aspects of the social dimension of practice. Epistemological trust ([Davenport and Cronin 2000](#dav00)) prevails mostly in relation to the cultural-discursive dimension of practice. Van House ([2002](#van02)) identified competence, honesty, shared orientations and values as criteria for trust assessment. This study can extend this list by adding shared interests since participants emphasize its significance in the establishment of trust. In their study of trust in relation to information, Kelton _et al._ ([2008](#kel08)) propose a shift in focus from trust seen as attributes to information to a focus on the user of the information. This proposal can be enhanced by the findings in the present study where it emerges that both information and the sharer are assessed in terms of trust.

Both the models proposed by Dirks and Ferrin ([2001](#dir01)) seem applicable with regard to how trust is perceived in Nordcode. It is described as directly beneficial since it is taken to lay the ground for opportunities to cooperate, but it is also conceived as indirectly beneficial when it is portrayed as a prerequisite for a relaxed and '_free atmosphere with an open attitude_' (P5), i.e. as something that affords the conditions under which information sharing is likely to occur. The findings can also be related to Wittel's notion of a network sociality in which '_the ability to acknowledge the rules and conventions by which the creation, distribution and protection of information occur is crucial_' ([2001: 68](#wit01)). The analysis indicates that these abilities are important also in the Nordcode network, and that they are especially pertinent in relation to the cultural-discursive and the social dimensions of the practice under study.

The present study clearly adheres to the assertion that research work is a morally infused practice. However, when relating the findings in this study to the norms ([Merton 1942/1973](#mer73)) and counter-norms ([Mitroff 1974](#mit74)) discussed in the introduction, it is not possible to claim that one or the other is more suitable when it comes to describing the practice under study. The empirical material provides examples of how participants chose not to share information, which would support Mitroff's counter-norms. On the other hand, there are also several passages where it is possible to find support for Merton's ethos of science.

## Conclusion

Trust issues related to information sharing emerge on both micro (personal trust) and macro (system trust) levels and should not necessarily be perceived as solely tied to interaction between individuals. The present study demonstrates that strategies for creating and assessing trust encompass conscious collective efforts to establish an open and permissive atmosphere within the network, including careful selection of suitable locations for seminars and conferences and the shaping of the material dimensions of workplaces. When trust issues are connected to information sharing and strategies for dealing with these, they emerge in relation to the shared information, the people involved, the tools used for sharing, and the place where information sharing occurs. The practice-theoretical perspective has proven effective in order to identify and capture the elusive phenomenon of trust in a community of design scholars. It is therefore reasonable to assume that a practice-theoretical approach also may be beneficial for other researchers within the field of information sharing.

## Acknowledgements

This study was conducted within the frame of the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) at the University of Gothenburg and the University of Borås, Sweden. The author would also like to acknowledge the anonymous reviewers and the Editor for their useful comments.

## <a id="author"></a>About the author

**Ola Pilerot**, lecturer and doctoral candidate at the Swedish School of Library and Information Science, University of Borås, teaches within the field of information practices and information literacies. He is a member of the Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS) at the University of Gothenburg and the University of Borås. His main area of research interest concerns information literacies in academia and scholarly information practices. In his dissertation project he is exploring the information practices of design researchers and in particular their information sharing activities. He can be contacted at: [ola.pilerot@hb.se](ola.pilerot@hb.se)

</section>

<section>

## References
<h2>References</h2>  
            
<ul> 

<li id="bac11">Bachmann, R. (2011). At the crossroads: future directions in trust research. <em>Journal of Trust Research</em>, <strong>1</strong>(2), 203-213.</li> 

<li id="bak86">Bakhtin, M. (1986). <em>Speech genres and other late essays</em>. Austin, TX: University of Texas Press.</li> 

<li id="baz97">Bazerman, C. (1997). Discursively structured activities. <em>Mind, Culture, and Activity</em>, <strong>4</strong>(4), 296-308.</li> 

<li id="ber11">Berry, G.R. (2011). A cross-disciplinary literature review: examining trust on virtual teams. <em>Performance Improvement Quarterly</em>,  <strong>24</strong>(3), 9-28.</li> 

<li id="bor10">Boradkar, P. (2010). Design as problem solving. In R. Frodeman (Ed.), <em>The Oxford handbook of interdisciplinarity </em> (pp. 273-287). Oxford: Oxford University Press.</li> 

<li id="cas07">Case, D.O. (2007). <em>Looking for information: a survey of research on information seeking, needs, and behavior</em>. (2nd. ed.) Amsterdam: Elsevier/Academic Press.</li> 

<li id="cha10">Chai, S. &amp; Kim, M. (2010). What makes bloggers share knowledge? An investigation on the role of trust. <em>International Journal of Information Management</em>. <strong>30</strong>(5), 408-415. </li> 

<li id="cor08">Corbin, J. &amp; Strauss, A. (2008). <em>Basics of qualitative research: techniques and procedures for developing grounded theory</em>. (3rd. ed.) Thousand Oaks, CA: SAGE Publications</li> 

<li id="cox12">Cox, A. (2012). An exploration of the practice approach and its place in information science. <em>Journal of Information Science</em>, <strong>38</strong>(2), 176-188.</li> 

<li id="cro03">Cronin, B. (2003). Scholarly communication and epistemic cultures. <em>New Review of Academic Librarianship</em>, <strong>9</strong>(1), 1-24.</li> 

<li id="cro07">Cross, N. (2006/2007). <em>Designerly ways of knowing</em>. Basel, Switzerland: Birkh&auml;user.</li> 

<li id="cro96">Crossley, N. (1996). <em>Intersubjectivity: the fabric of social becoming</em>. London: SAGE Publications.</li> 

<li id="dav00">Davenport, E. &amp; Cronin, B. (2000). The citation network as a prototype for representing trust in virtual environments.  In B. Cronin &amp; H.B. Atkins (eds.), <em>The Web of knowledge: a festschrift in honor of Eugene Garfield</em> (pp. 517-534). Medford, NJ: Information Today Inc.</li> 

<li id="dir01">Dirks, K.T. &amp; Ferrin, D.L. (2001).The role of trust in organizational setting. <em>Organization Science</em>. <strong>12</strong>(4), 450-467.</li> 

<li id="erm09">Ermisch, J., Gambetta, D., Laurie, H., Siedler, T. &amp; Uhrig, N. (2009). Measuring people's trust. <em>Journal of the Royal Statistical Society. Series A. Statistics in Society</em>, <strong>172</strong>(4), 749-769.</li> 

<li id="fel11">Feldman, M. S., &amp; Orlikowski W. J.(2011). Theorizing practice and practicing theory. <em>Organization Science</em>, <strong>22</strong>(5), 1240-1253.</li>

<li id="fos06">Foster, J. (2006). Collaborative information seeking and retrieval. <em>Annual Review of Information Science and Technology</em>, <strong>40</strong>(1), 329-356.</li> 

<li id="fri03">Friedman, K. (2003). Theory construction in design research: criteria: approaches, and methods. <em>Design Studies</em>, <strong>24</strong>(6), 507-522.</li> 

<li id="ghe06">Gherardi, S. (2006). <em>Organizational knowledge: the texture of workplace learning</em>. Malden, MA: Blackwell. </li> 

<li id="ghe09">Gherardi, S. (2009). Introduction: the critical power of the 'practice lens'. <em>Management Learning</em>, <strong>40</strong>(2), 115-128.</li> 

<li id="gid90">Giddens, A. (1990). <em>The consequences of modernity</em>. Cambridge: Polity in association with Blackwell.</li> 

<li id="hae10">Haeussler, C. Jiang, L., Thursby, J. &amp; Thursby, M. (2010).  <a href="http://www.webcitation.org/6JloRmsln">Specific and general information sharing among academic scientists</a>. Paper presented at the <em>DRUID Society Summer Conference 2010 on Opening Up Innovation: Strategy, Organization and Technology</em>, Imperial College London Business School. Retrieved from http://www2.druid.dk/conferences/viewpaper.php?id=501044&amp;cf=43 (Archived by WebCite&reg; at http://www.webcitation.org/6JloRmsln)</li> 

<li id="hai12">Haider, J. (2012). Interrupting practices that want to matter. The making, shaping and reproduction of environmental information online. <em>Journal of Documentation</em>, <strong>68</strong>(5), 639-658.</li> 

<li id="he09">He, W., Fang, Y. &amp; Wei, K.-K. (2009). The role of trust in promoting organizational knowledge seeking using knowledge management systems: an empirical investigation. <em>Journal of the American Society of Information Science and Technology</em>, <strong>60</strong>(3), 526–537.</li> 

<li id="hea00">Heath, C., Knoblauch, H. &amp; Luff, P. (2000). Technology and social interaction: the emergence of 'workplace studies', <em>British Journal of Sociology</em>, <strong>51</strong>(2), 299-320.</li> 

<li id="her02">Hertzum, M. (2002). The importance of trust in software engineers' assessment and choice of information sources. <em>Information and Organization</em>, <strong>12</strong>(1), 1-18.</li> 

<li id="hoo11">Hooghe, M. &amp; Zmerli, S. (2011). Introduction: the context of political trust. In M. Hooghe &amp; S. Zmerli (eds.), <em>Political trust: why context matters</em> (pp. 1-12). Colchester, UK: ECPR Press. </li> 

<li id="hui11">Huizing, A. &amp; Cavanagh, M. (2011). <a href="http://www.webcitation.org/6JlortA57">Planting contemporary practice theory in the garden of information science</a>. <em>Information Research</em>, <strong>16</strong>(4) paper 497. Retrieved from http://InformationR.net/ir/16-4/paper497.html (Archived by WebCite&reg; at http://www.webcitation.org/6JlortA57) </li> 

<li id="ibr12">Ibrahim, N.H. &amp; Allen, D. (2012). Information sharing and trust during major incidents: findings from the oil industry. <em>Journal of the American Society for Information Science and Technology</em>, <strong>63</strong>(10), 1916-1928. </li> 

<li id="iiv00">Iivonen, M. &amp; Huotari, M-L. (2000). The impact of trust on the practice of knowledge management. <em>Proceedings of the ASIS Annual Meeting</em>, <strong>37</strong> 421-429.</li> 

<li id="jar04">Jarvenpaa, S.L., Shaw, T.R. &amp; Staples, D.S. (2004). Toward contextualized theories of trust: the role of trust in global virtual teams. <em>Information Systems Research</em>, <strong>15</strong>(3), 250-267.</li> 

<li id="kel08">Kelton, K., Fleischmann, K.R., &amp; Wallace, W.A. (2008). Trust in digital information. <em>Journal of the American Society for Information Science and Technology</em>, <strong>59</strong>(3), 363-374.</li> 

<li id="kem11">Kemmis, S. (2011). What is professional practice? In C. Kanes (ed.), <em>Elaborating professionalism: studies in practice and theory</em> (pp. 139-166). New York, NY: Springer.</li> 

<li id="kli00">Kling, R. &amp; McKim, G. (2000). Not just a matter of time: field differences and the shaping of electronic media in supporting scientific communication. <em>Journal of the American Society for Information Science and Technology</em>, <strong>51</strong>(14), 1306-1320.</li>

<li id="kno99">Knorr-Cetina, K. (1999). <em>Epistemic cultures: how the sciences make knowledge</em>. Cambridge, MA: Harvard University Press.</li> 

<li id="kva09">Kvale, S. &amp; Brinkmann, S. (2009). <em>InterViews: learning the craft of qualitative research interviewing</em>. (2nd. ed.). Los Angeles, CA: Sage Publications.</li> 

<li id="lat05">Latour, B. (2005). <em>Reassembling the social</em>. Oxford: Oxford University Press.</li> 

<li id="lav91">Lave, J. &amp; Wenger, E. (1991). <em>Situated learning: legitimate peripheral participation</em>. New York, NY: Cambridge University Press.</li> 

<li id="lev04">Levin, D.Z. &amp; Cross, R. (2004). The strength of weak ties you can trust: the mediating role of trust in effective knowledge transfer. <em>Management Science</em>, <strong>50</strong>(1), 1477-1490.</li> 

<li id="lew12">Lewis, J.D. &amp; Weigert, A.J. (2012). The social dynamics of trust: theoretical and empirical research, 1985-2012. <em>Social Forces</em>, <strong>91</strong>(1), 25-31.</li> 

<li id="llo10">Lloyd, A. (2010). Framing information literacy as information practice: site ontology and practice theory. <em>Journal of Documentation</em>, <strong>66</strong>(2), 245-258.</li> 

<li id="luh79">Luhmann, N. (1979). <em>Trust and power: two works</em>. Chichester, UK: J. Wiley.</li> 

<li id="mar10">Margolin, V. (2010). Doctoral education in design: problems and prospects. <em>Design Issues</em>, <strong>26</strong>(3), 70-78.</li> 

<li id="may95">Mayer, R. C., Davis, J. H., &amp; Schoorman, F. D. (1995). An integrative model of organizational trust. <em>Academy of Management Review</em>, <strong>20</strong>(3), 709-734.</li> 

<li id="mca95">McAllister, D.J. (1995). Affect- and cognition-based trust as foundations for interpersonal cooperation in organizations. <em>Academy of Management Journal</em>, <strong>38</strong>(1), 24-59.</li> 

<li id="mer73">Merton, R. (1942/1973). The normative structure of science. In N.W. Storer (Ed.), <em>The sociology of science: theoretical and empirical investigations</em> (pp. 267-278). Chicago, IL: University of Chicago Press.</li> 

<li id="mit74">Mitroff, I. (1974). Norms and counter-norms in a select group of the Apollo moon scientists: a case study of the ambivalence of scientists. <em>American Sociological Review</em>, <strong>39</strong>(4), 579-595.</li>  

<li id="mol01">M&ouml;llering, G. (2001). The nature of trust: from Georg Simmel to a theory of expectation, interpretation and suspension. <em>Sociology</em>, <strong>35</strong>(2), 403-420.</li>

<li id="mor03">Mortimer, E. F.  &amp; Wertsch, J.V. (2003). The architecture and dynamics of intersubjectivity in science classrooms. <em>Mind, Culture, and Activity</em>, <strong>10</strong>(3), 230-244.</li>

<li id="nar07">Nardi, B. (2007). Placeless organizations: collaborating for transformation. <em>Mind, Culture, and Activity</em>, <strong>14</strong>(1-2), 5-22.</li>

<li id="nornd">Nordcode (n.d.), <a href="http://www.webcitation.org/6Jlp2Vv7h">Nordcode; background</a>. Retrieved from http://www.nordcode.net/?page_id=128 (Archived by WebCite&reg; at http://www.webcitation.org/6Jlp2Vv7h) </li>

<li id="pal11">Paliszkiewicz, J.O. (2011). Trust management: literature review. <em>Management</em>, <strong>6</strong>(4), 315-331.</li>

<li id="pal08">Palmer, C. L., &amp; Cragin, M. H. (2008). Scholarly information work and disciplinary practices. <em>Annual Review of Information Science and Technology</em>, <strong>42</strong> 165-211.</li>

<li id="pil12">Pilerot, O. (2012). LIS research on information sharing activities:  people, places, or information. <em>Journal of Documentation</em>, <strong>68</strong>(4) 559-581.</li>

<li id="pil11">Pilerot, O. &amp; Limberg, L. (2011). Information sharing as a means to reach collective understanding: a study of design scholars&rsquo; information practices. <em>Journal of Documentation</em>, <strong>67</strong>(2), 312-333.</li>

<li id="sau08">Saumure, K. &amp; Given, L.M. (2008). Data saturation. In Given L.M. (Ed.), <em>The SAGE encyclopedia of qualitative research methods</em> (pp. 195-196). Thousand Oaks, CA: Sage Publications.</li>

<li id="sch02">Schatzki, T.R. (2002). <em>The site of the social: a philosophical account of the constitution of social life and change</em>. University Park, PA: Pennsylvania State University Press.</li>

<li id="sch12">Schatzki, T.R. (2012). A primer on practices: theory and research. In J. Higgs (Ed.), <em>Practice-based education: perspectives and strategies</em> (pp. 13-26).  Rotterdam, the Netherlands: Sense Publishers. </li>

<li id="sch01">Schatzki, T. R., Knorr-Cetina, K. &amp; von Savigny, E. (eds.) (2001). <em>The practice turn in contemporary theory</em>. London: Routledge.</li>

<li id="sha94">Shapin, S. (1994). <em>A social history of truth: civility and science in seventeenth-century England</em>. Chicago, IL: University of Chicago Press.</li>

<li id="swi13">Swift, P.E. &amp; Hwang, A. (2013). The impact of affective and cognitive trust on knowledge sharing and organizational learning. <em>The Learning Organization</em>, <strong>20</strong>(1), 20-37.</li>

<li id="tab12">Tabak, E. &amp; Willson, M. (2012). A non-linear model of information sharing practices in academic communities. <em>Library &amp; Information Science Research</em>, <strong>34</strong>(2), 110-116.</li>

<li id="tal02">Talja, S. (2002). Information sharing in academic communities: types and levels of collaboration in information seeking and use. <em>New Review of Information Behavior Research</em>, <strong>3</strong> 143-160.</li>

<li id="tal10">Talja, S. (2010). Jean Lave&rsquo;s practice theory. In G.J. Leckie, L.M. Given &amp; J.E. Buschman (eds.), <em>Critical theory for library and information science: exploring the social from across the disciplines</em> (pp. 205-220). Santa Barbara, CA: Libraries Unlimited. </li>

<li id="tan05">Tanis, M. &amp; Postmes, T. (2005). A social identity approach to trust: interpersonal perception, group membership and trusting behaviour. <em>European Journal of Social Psychology</em>, <strong>35</strong>(3), 413-424.</li>

<li id="van02">Van House, N. (2002). Digital libraries and practices of trust: networked biodiversity information. <em>Social Epistemology</em>, <strong>16</strong>(1), 99-114.</li>

<li id="war09">Ward, P. &amp; Meyer, S. (2009). Trust, social quality and wellbeing: a sociological exegesis. <em>Development and Society</em>, <strong>38</strong>(2), 339-363.</li>

<li id="wil83">Wilson, P. (1983). <em>Second-hand knowledge: an inquiry into cognitive authority</em>. Westport, CT: Greenwood P.</li>

<li id="wil10">Wilson, T. (2010). <a href="http://www.webcitation.org/6JlpDu4as">Information sharing: an exploration of the literature and some propositions</a>. <em>Information Research</em>, <strong>15</strong>(4) paper 440. Retrieved from http://InformationR.net/ir/15-4/paper440.html (Archived by WebCite&reg; at http://www.webcitation.org/6JlpDu4as) </li>

<li id="wit01">Wittel, A. (2001). Toward a network sociality. <em>Theory, Culture &amp; Society</em>, <strong>18</strong>(6), 51-76.</li>

<li id="zha09">Zhang, Y. &amp; Wildemuth, B. M. (2009). Qualitative analysis of content. In B. Wildemuth (Ed.), <em>Applications of social research methods to questions in information and library science</em> (pp. 308-319). Westport, CT: Libraries Unlimited.</li>

</ul>

</section>

</article>