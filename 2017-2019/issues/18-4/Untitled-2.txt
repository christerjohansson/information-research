rticle>

    <article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" locale="en_US" date_submitted="1990-01-01" stage="submission" date_published="1990-01-01" section_ref="ART" seq="1" access_status="0">
      <id type="internal" advice="ignore">731</id>
      <title locale="en_US">Title of the second paper in the issue</title>
      <abstract locale="en_US">Abstract from the second paper</abstract>
      <!--List all authors metadata individually-->
      <authors xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
        <author include_in_browse="true" user_group_ref="Author">
          <firstname>First name of author</firstname>
          <lastname>Last name of author</lastname>
          <affiliation locale="en_US">Department of Information Studies, University of Somewhere</affiliation>
          <country>UK</country>
          <email>genere@email.co.uk</email>
        </author>
      </authors>
      <!-- SUBMISSION FILES PAPERS HTML PDF ETC -->
      <submission_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="submission" id="2344" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
      <revision number="1" genre="Article Text" filename="Paper6.html" viewable="false" date_uploaded="2019-02-26" date_modified="2019-02-26" filetype="text/html" uploader="christerjohansson">
          <name locale="en_US">Business use of the World-Wide Web</name>
          <href src="https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/1-2/paper3.html"></href>
        </revision>        
      </submission_file>
      <!--SUPPLEMENTARY FILES IMAGES PDF ETC-->
      <supplementary_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="proof" id="2345" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
        <revision number="1" genre="Other" filename="model2.gif" viewable="false" date_uploaded="2019-03-11" date_modified="2019-02-26" filetype="image/gif" uploader="christerjohansson">
          <name locale="en_US">model2.gif</name>
          <href src="https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/1-2/model2.gif"></href>
        </revision>
        <publisher locale="en_US">Tom Wilson</publisher>
        <date_created>2019-02-26</date_created>
        <source locale="en_US">University of Bor&#xE5;s</source>
      </supplementary_file>
      <!--GALLEYS-->
      <article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="true" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
        <id type="internal" advice="ignore">2332</id>
        <name locale="en_US">paper556.html</name>
        <seq>3</seq>
        <submission_file_ref id="2344" revision="1"/>
      </article_galley>
    </article>

     <article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="true" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
        <id type="internal" advice="ignore">2333</id>
        <name locale="en_US">p556fig1.png</name>
        <seq>4</seq>
        <submission_file_ref id="2344" revision="1"/>
      </article_galley>
    </article>