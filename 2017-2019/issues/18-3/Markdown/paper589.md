<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

# Incorporating nonparametric statistics into Delphi studies in library and information science

#### [Boryung Ju](#author) and [Tao Jin](#author)  
School of Library and Information Science, Louisiana State University,  
267 Coates Hall, Baton Rouge, LA 70803, USA

#### Abstract

> **Introduction.** The Delphi technique is widely used in library and information science research. However, many researchers in the field fail to employ standard statistical tests when using this technique. This makes the technique vulnerable to criticisms of its reliability and validity. The general goal of this article is to explore how nonparametric statistical techniques could mitigate this drawback and be incorporated into Delphi studies in library and information science.  
> **Method.** To reach that goal we provide an example study which used a Delphi method for data collection and nonparametric statistics for data analysis. The example study investigated the barriers and challenges encountered by scientists when using various information and communication technologies for their distributed collaboration activities. Twenty-four participants were recruited into two domain groups (social and behavioural sciences vs. science and engineering).  
> **Results.** After three rounds of data collection and analysis systematically using nonparametric statistical measurements, fourteen items were identified as the most important. Different rankings of the items were observed between the groups involved, including Kendall's _W_s.  
> **Conclusions.** The incorporation of the nonparametric statistics into the Delphi technique may enhance its rigour as a research method and provide evidence for its reliability and validity.

## Introduction

The Delphi method is one of the best known and most frequently used qualitative research methods throughout library and information science (LIS) and other social science disciplines. As a way to forecast, identify, or prioritize issues in a given field, it is commonly used to gather opinions from a panel of experts via iterative rounds of elicitation sessions to eventually produce a collective agreement among the experts. This method enables researchers to observe how experts discuss a complex problem through a structured communication process, as well as collate divergent responses into a convergent overview ([Linstone and Turoff 1975](#lin75); [Müller _et al._ 2010](#mul10)). One drawback of the Delphi technique, however, is the lack of standard statistical tests, which may potentially entail issues of reliability and validity ([Fischer 1978](#fis78); [Weingand 1989](#wei89)).

The general goal of this article is to explore how nonparametric statistical techniques could diminish this drawback and be incorporated into LIS Delphi studies. To achieve that goal, this article contains three parts: first, a synopsis of the Delphi method and its applications in librarianship and information science; second, some of the critical issues found when employing this methodology, specifically with the integration of the nonparametric statistical approach as a remedy to the aforementioned drawback; and finally, an example study illustrating how some nonparametric statistical techniques can be incorporated into the Delphi method. We argue that such incorporation may potentially enhance the rigour of the Delphi technique and add a new tool in the arsenal of researchers.

## Characteristics of the Delphi method

The Delphi method was developed in 1953 at Rand Corporation by Dalkey and Helmer ([Weingand 1989](#wei89)). It was initially devised to '_obtain the most reliable opinion consensus of a group of experts by subjecting them to a series of questionnaires in depth interspersed with controlled opinion feedback_' ([Dalkey and Helmer 1963](#dah63): 458). Since then, the method has been extensively used in innumerable areas; therefore, many variants exist ([Linstone and Turoff 1975](#lin75); [Schmidt 1997](#sch97)). Nonetheless, they all reduce to the administration of questionnaires either in-person, by mail, by phone, or in an electronic format. Most Delphi studies also share a few distinct characteristics, such as:

*   **Expert input.** Delphi researchers traditionally use experts as the source of information, instead of a random sample of a specified population. Experts are defined as individuals with special skills or knowledge representing mastery of a particular subject. Hence, this approach often leads to a small sample size.
*   **Anonymity.** The Delphi technique emphasizes the gathering and generating of collective intelligence by an indirect mode of communication. The participating experts would respond to a series of questionnaires individually. While they might be known to one another, their specific responses in the process would be treated anonymously.
*   **Iteration with controlled feedback.** The Delphi technique usually requires several rounds of systematic solicitation of expert opinions until a group census is achieved ([Agha and Akhta 1992](#agh92); [Eldredge _et al._ 2009](#eld09); [Fischer 1978](#fis78); [Goodman 1987](#goo87); [Weingand 1989](#wei89)). Between rounds, participating experts receive feedback regarding the outcome of each round. Orchestrating a Delphi process requires a modest commitment by all participating experts, but a more significant obligation falls to the researchers ([Yarnal _et al_. 2009](#yar2009)).
*   **Statistical group response.** Many Delphi questionnaires ask respondents to rate or rank items on Likert-type scales. Thus, the research data are mostly ordinal by nature. After each round of data collection a few basic statistics for each item, such as the median and percentage scores, can be calculated as a statistical group response. This allows respondents to see where their opinions fall in the context of the group ([Goodman 1987](#goo87); [Reilly 1970](#rei70)).

## Previous Delphi studies in library and information science research

In the library and informaton research field, the earliest published Delphi study can be traced back to Borko ([1970](#bor70)) and since then the use of this technique has drastically increased. Using _Library, Information Science &Technology Abstracts_, we identified eighty-seven empirical Delphi research papers published between 1971 and 2011\. As shown in Table 1, nearly half (49.4%) of the papers dealt with the general topic of libraries and/or librarianship. This general cluster includes subjects such as books and reading ([Du 2009](#Duy09)), cataloguing ([Smiraglia 2010](#smi10)), electronic information resources ([Kochtanek and Hein 1999](#koc99); [Nicholson 2003](#nic03)), information literacy ([Saunders 2009](#sau09)), library administration ([Maceviciute and Wilson 2009](#mac09)), and reference services ([Pomerantaz _et al_. 2003](#pom03)). These studies involve almost all types of libraries and archives, from academic ([Harer and Cole 2005](#har05)) and public ([Lukenbill and Immroth 2009](#luk09)) to medical ([Eldredge _et al_. 2009](#eld09)) and law ([Missingham 2011](#mis11)).

Following libraries and/or librarianship, the second largest cluster was management information systems, which includes business intelligence ([Müller _et al._ 2010](#mul10)), supply chains ([Daniel and White 2005](#dan05)), information resources ([McFadzean _et al._ 2011](#mcf11)), system security ([Gonzales _et al._ 2006](#gon06)), electronic commerce ([Addison 2003](#add03)) and computer software development ([Dhaliwal and Tung 2000](#dha00)). The third biggest cluster is medical informatics, where the Delphi method has been used to explore health behaviour ([Brouwer _et al_. 2008](#bro08)), to appraise certain medical systems ([Ambrosiadou and Goulis 1999](#amb99)), and to establish a controlled vocabulary for nursing practice and research ([Saranto and Tallberg 2003](#sar03)). It should be noted that most of the authors of the papers mentioned above are from the fields of management and business, medicine and nursing, or computing and engineering. Additionally, the Delphi method has been used extensively in studies on information science ([Zins 2007](#zin07)), knowledge management ([Holsapple and Joshi 2000](#hol00)), information policy ([Meijer 2002](#mei02)), and library education and professionalism ([Baruchson-Arbib and Brostein 2002](#bar02); [Weingand 1989](#wei89)).

Through previous Delphi literature, we also attempted to examine the motivations behind the researchers who selected the Delphi approach as their research method. The broad purposes identified in previous research suggest that this technique can be applied in a variety of research situations. After we examined the abstracts of all these papers and extracted their purpose statements, an analysis about these statements was conducted. Five broad purposes emerged, though they are not mutually exclusive: identification; prediction; evaluation; plan or policy development and refinement; and system conceptualization, development, and implementation (Table 1). We found over one third (37%) of the papers stated that the Delphi technique was used to identify something. The objects identified could be risks, factors, attributes, indicators, needs, challenges, opportunities, limitations, uncertainties, vagueness, or any issues relevant to the research problem. The second most common purpose specified was prediction. Almost 20% of the papers aimed to forecast some kind of trend in a given domain, which echoes the tradition of the method. Baruchson-Arbib and Bronstein ([2002](#bar02)) remarked that the method is particularly suited to research areas '_for which hard data are either insufficient or inconclusive_' ([Baruchson-Arbib and Brostein 2002](#bar02): 399). The third major identified application of the Delphi method in the eighty-seven studies was evaluation. About 18% of the studies intended to assess certain toolkits, standards, impacts, systems, frameworks, performances, or models. The word _criteria_ was one of the most frequently seen terms in such papers. In addition, we found the Delphi method was often adopted in various system development projects, policy analyses and planning activities.

<table><caption>Table 1: Some statistics on 87 Delphi studies in library and information science (1971-2011)</caption>

<tbody>

<tr>

<th rowspan="1" colspan="3">Publication date</th>

<th colspan="3">Main subjects involved</th>

<th colspan="3">Main purposes of the studies</th>

</tr>

<tr>

<th>Date</th>

<th>Freq.</th>

<th>%</th>

<th>Subject term</th>

<th>Freq.</th>

<th>%</th>

<th>Purpose</th>

<th>Freq.</th>

<th>%</th>

</tr>

<tr>

<td>1970s</td>

<td>1</td>

<td>1.15%</td>

<td>Libraries and/or librarianship</td>

<td>43</td>

<td>49.43%</td>

<td>Identification</td>

<td>32</td>

<td>36.78%</td>

</tr>

<tr>

<td>1980s</td>

<td>3</td>

<td>3.45%</td>

<td>Management information systems</td>

<td>15</td>

<td>17.24%</td>

<td>Prediction</td>

<td>17</td>

<td>19.54%</td>

</tr>

<tr>

<td>1990s</td>

<td>8</td>

<td>9.20%</td>

<td>Medical informatics</td>

<td>9</td>

<td>10.34%</td>

<td>Evaluation</td>

<td>16</td>

<td>18.39%</td>

</tr>

<tr>

<td>2000s</td>

<td>57</td>

<td>65.52%</td>

<td>Information science</td>

<td>6</td>

<td>6.90%</td>

<td>Plan or policy development and refinement</td>

<td>13</td>

<td>14.94%</td>

</tr>

<tr>

<td>2010s</td>

<td>18</td>

<td>20.69%</td>

<td>Information policy</td>

<td>4</td>

<td>4.60%</td>

<td>System conceptualization, development, and implementation</td>

<td>11</td>

<td>12.64%</td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td>Library education and professionalism</td>

<td>4</td>

<td>4.60%</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td>Knowledge management</td>

<td>2</td>

<td>2.30%</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td>Others</td>

<td>4</td>

<td>4.60%</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

If we examine the methods used to conduct these Delphi studies more closely, several salient observations can be summarized. To begin, most of the studies only used a small sample of experts for the survey. Depending on the research questions, the total number of invited panellists or opinion leaders (N) ranged between 10 and 50, although a few studies initially distributed their questionnaires on a larger scale (e.g., [Eldredge _et al._ 2009](#eld09), N=1,064; [Maceviciute and Wilson 2009](#mac09), N=125; [Baruchson-Arbib and Bronstein 2002](#bar02), N=120). Second, most of the studies used only two or three rounds of iteration to obtain useful information. Only a few studies conducted the iteration process to four or more rounds (e.g., [Weingand 1989](#wei89)). However, the studies' methodology rarely provides any descriptions or justifications for determining when to move to the next round and/or stop polling. Third, very few studies systematically used statistical techniques to present and explain how the group consensus was reached. Focusing on those works written by authors affiliated with libraries or informaton schools reveals that many of studies solely employed a qualitative approach to report their results related to the consensus mechanism and did not include any standard statistical analyses (e.g., [Agha and Akhtar 1992](#agh92); [Kochtanek and Hein 1999](#koc99); [Saunders 2009](#sau09); [Smiraglia 2010](#smi10); [Weingand 1989](#wei89); [Westbrook 1997](#wes97); [Zins 2007](#zin07)). Even those few studies which provided statistical analyses were, quite often, limited to group means, medians, standard deviations, or simply percentages of distribution (e.g., [Baruchson-Arbib and Bronstein 2002](#bar02); [Du 2009](#Duy09); [Feret and Marcinek 1999](#fer99); [Harer and Cole 2005](#har05); [Zhang and Salaba 2009](#zha09)). This huge gap has left nonparametric statistical techniques a crucial issue to cover. It merits mentioning that Maceviciute and Wilson ([2009](#mac09)) employed the Spearman rank correlation test in their data analysis; which may suggest that some researchers have recognized this problem, and attempted to address it.

## Limitations of Delphi

There are two core limitations of the Delphi method: the vagueness of the concepts of expert and expertise; and the lack of elementary statistical analyses of data ([Baruchson-Arbib and Bronstein 2002](#bar02); [Fischer 1978](#fis78); [Goodman 1987](#goo87)). The first limitation is directly related to the sampling procedures. Because the method relies heavily on the use of experts in gathering information, the qualifications of the experts and the quality of their input are essential in validating a given study. The researchers must develop a set of objective criteria to select experts and assess their expertise. Nevertheless, the actual selection process must still be somewhat arbitrary. The researchers are responsible for proving and justifying that the selected experts are competent and knowledgeable enough to 'yield significantly better and substantially different responses from non-experts' ([Goodman 1987](#goo87): 731).

The second limitation is the lack of standard statistical analyses, which arguably makes the method vulnerable to criticisms of its reliability and validity. Quite often some standard tests for statistical significance, such as standard errors of estimate and systematic correlations of items, are intentionally, or unintentionally, ignored by some Delphi researchers ([Fischer 1978](#fis78)). Furthermore, due to its extensive applications, the Delphi method has evolved over time to include many variants. Some researchers, who justified their method as a Delphi variant, actually failed to make a sufficiently rigorous analysis, resulting in a more dramatic convergence than a strict statistical employment of the method ([Goodman 1987](#goo87)). However, Fischer ([1978](#fis78)) argued that '_[if] Delphi is to be considered seriously as a reliable and valid method, then it must be evaluated by the same standards as other social science methodology_' ([Fischer 1978](#fis78): 68).

## Critical issues in Delphi procedures

Despite these limitations, the Delphi method is still a popular tool. Those interested in using this method in the future should consider incorporating techniques that can specifically compensate for the limitations. Moreover, these limitations raise some critical issues to consider. Correspondingly, we propose three critical aspects that need particular attention from the researchers: recruitment of experts; iteration of rounds for experts' input, and the range of identified issues and prioritization of those issues. Along with a series of techniques that researchers can use, we will elaborate each of the aspects below.

### Recruitment of experts

One of the most important steps for conducting a Delphi study is to identify and solicit qualified individuals for valuable input. In their study, Okoli and Pawlowski ([2004](#oko04)) detailed a procedure for selecting experts as follows:

*   **Step 1:** prepare a worksheet that defines categories and knowledge levels of relevant disciplines, skills, individuals/organizations
*   **Step 2:** populate the worksheet with names
*   **Step 3:** contact experts and/or ask them to nominate other experts
*   **Step 4:** rank and categorize experts based on their availability and qualification
*   **Step 5:** invite experts according to the target panel size

The size and number of expert groups (or panels) are totally dependent on the nature of the study. Like other qualitative approaches, the Delphi method does not need to ensure the representativeness of statistic samples because this is a group decision mechanism for reaching a group consensus ([Okoli and Pawlowski 2004](#oko04); [Ju and Pawlowski 2011](#Jub11)). For this reason a relatively small sample size, twenty to forty-five participants per study, is justifiable, as has been shown in many previous studies (e.g., [Keller 2001](#kel01); [Ludwig and Starr 2005](#lud05); [Neuman 1995](#neu95); [Westbrook 1997](#wes97)).

### Iteration of rounds for experts' input

As mentioned earlier, iteration with controlled feedback is the most distinguished feature of the Delphi method. But how many rounds of solicitations would be sufficient? As Schmidt ([1997](#sch97)) pointed out, one of the possible challenges faced by the researchers is the question of when to stop polling. Too few rounds may lead to an immature outcome, while too many rounds would require a burdensome commitment from the participating experts and the researchers conducting a Delphi study. Although no rules dictate how many rounds should be undertaken, it seems that most of the previous Delphi studies ran through two to three rounds of solicitations without additional justifications. The main issue for researchers is obtaining the most rigorous data while being considerate of the participating experts' time and efforts.

### Range of identified issues and prioritization

Given the natural tendency of the Delphi to centralize opinions, an important question is how to prioritize those identified issues appropriately in each round, and, likewise, 'how many items to carry over to the subsequent rounds' ([Schmidt 1997](#sch97): 764). To address these questions, researchers must make many decisions. The first set of decisions should include: the number of items study participants should respond to in the initial round; if the initial questionnaire has too many items, how many of them should be carried over to the next rounds; and how can the researchers eliminate less meaningful items from the list?

As suggested previously, the primary role of iteration in the Delphi process is to achieve a high degree of consensus. This leads to yet another important decision: how to operationally define the concept of 'degree of consensus'. Few studies provided a clear definition of this notion and, in most cases, the boundaries between _high_ and _adequate_ agreements among the study participants were vague; different researchers used different statistical elements to represent the boundaries. Some studies used rankings and percentages of agreement or disagreement ([Ludwig and Starr 2005](#lud05)), while others used mean ratings and standard deviations ([Neuman 1995](#neu95); [Zhang and Salaba 2009](#zha09); [Westbrook 1997](#wes97)). Notably, the rated or ranked items in these studies represent the ordinal scale data, measurement of order (rank order), not a relative size of data. Hence, the application of standard deviation to ordinal level data does not produce a valid interpretation ([Schmidt 1997](#sch97); [Stevens 1946](#ste46)). Instead, the rates of agreement using statistics such as Kendall's coefficient of concordance (_W_) or the standard Pearson correlation coefficient (_r_) could be considered to ensure the most rigorous assessment of ratings. Since most of the Delphi studies employ a small sample size, with no assumptions regarding the nature of probability distribution, using a nonparametric statistic such as Kendall's _W_, is a preferred method for result interpretation at each round of a Delphi study.

In order to demonstrate how the above three aspects can be addressed, a study using the ranking-type Delphi method is presented in the following section. This example will illustrate how data were collected and analysed by using nonparametric statistics, as well as the general design of the study. We hope it will provide some insights for the future application of this method.

## An example study

The aim of the example study is to investigate the barriers and challenges encountered by scientists when they use information and communication technologies (information and communication technologies) (e.g. collaboration software, video conferencing, and instant messaging tools) for their distributed research collaboration activities ([Ju and Pawlowski 2011](#Jub11)). A step-by-step procedure is demonstrated.

This study addresses two research questions:

*   What are the types of barriers and challenges to the use of information and communication technologies by scientists in the distributed environment of collaboration over the cyber infrastructure?
*   Are there any major differences or commonalities in the barriers and challenges to the use of information and communication technologies by research domain (science and engineering vs. social and behavioural sciences)?

### Selection of panel members

The identification and selection of qualified individuals is one of the most fundamental processes when conducting a Delphi study. For this study, the concept of 'expert panellists is defined as researchers who have extensive experience in communicating and collaborating with colleagues at other universities (geographically distributed) for research. Each panellist also uses at least one communication technology, such as email, telephone/Skype, Wikis, video conferencing, Listservs, instant messaging, blogs, or collaboration software suites such as SharePoint (see [Appendix](#app)).

Initial panel members were identified by the investigators' personal contacts, as well as by recommendations from the investigators' colleagues in other departments at a national, research-oriented university in the United States. In order to solicit initial panel members to participate for the study, investigators explained the purpose of the study, eligibility of participants, time frame for each round, and the basic structure of the Delphi survey (Figure 1). Once experts agreed to participate, invitations went out to their collaborators at other universities or research centres. Twenty-four researchers from five research-oriented universities in four states (Florida, Georgia, Louisiana, and Ohio) were invited to participate in the Delphi survey. This was a purposeful sample using a snowball sampling method due to the availability of eligible participants.

<figure>

![Figure 1\. Basic structure of the example study](../p589fig1.jpg)

<figcaption>Figure 1: Basic structure of the example study  

<small>(Variation for the current study: Rounds 1 and 2 - Combined science and engioneering/social and behavioural sciences panel; Round 3 - Separate ranking by the science and engioneering panel and the social and behavioural sciences panel)</small></figcaption>

</figure>

<section>

To address the research question of whether or not major differences exist in barriers and challenges experienced by scientists using information and communication technologies, two panels were structured and members were recruited. The science and engineering panel consisted of researchers from the fields of mathematics, computer science, engineering and medicine, while the social and behavioural sciences panel consisted of researchers from psychology, political science, business and geography. In total, twenty-four panellists participated in the study. All of them hold PhD degrees or higher (post-doctoral) and have experience in conducting one or more multi-site research projects by using at least one of the information and communication technologies listed above.

### Data collection and analysis for each round

The online survey (e-Delphi) was administered to elicit opinions from the expert panellists throughout three rounds: Round 1 was the identification of issues by brainstorming among the panellists; Round 2 was ranking identified issues and assessing consensus among all panel members; and Round 3 was re-ranking the shortlisted issues by two different panels. The data analysis was done by following the step-wise method developed by Schmidt ([1997](#sch97)) for the attempt to mitigate the weaknesses seen in other ranking-type Delphi approaches.

#### Round 1

**Goal** The goal of Round 1 was to identify and elicit the key issues regarding the use of information and communication technologies from the panel members.

**Data collection** The survey was delivered using online survey software to all twenty-four panel members. The survey asked the panellists to list as many major issues (e.g., challenges, constraints or conflicts, barriers) as possible in the use of information and communication technologies for distributed research collaboration. The issues could be of any type, such as technical, social, procedural, financial, organizational or cultural. Panellists were allowed to list issues in a bullet format or list with elaborate explanations and/or examples in order to help the investigators' understanding. Other questions in the survey included the kind of information and communication technologies they used, the size of collaboration (number of collaborators), the multi-disciplinary aspects of the research projects they engaged in, the panellists' research domain, and their name and email address for the following surveys. Additional comments were also allowed. After gathering the participation agreements from the twenty-four panellists (twelve from the science and engioneering panel and twelve from the social and behavioural sciences panel), the online survey was delivered and participants were given two weeks to return their answers. The expected time for any given respondent to complete the form was fifteen minutes. E-mail reminders were distributed once in order to boost the response rate.

**Analysing responses** The goal of Round 1 was to identify and elicit the key issues regarding the use of information and communication technologies from the panel members.

<table><caption>Table 2: Example issues and example input items collected in Round 1</caption>

<tbody>

<tr>

<th> </th>

<th>Issues identified</th>

<th>Example input</th>

</tr>

<tr>

<td>1</td>

<td>Annoyances caused by some features of tools or tool use</td>

<td>Annoying aspects of Skype, for example, one cannot turn off the notification that appears on the screen every time a contact comes online</td>

</tr>

<tr>

<td>2</td>

<td>Confidentiality concerns</td>

<td>Potential break of confidentiality</td>

</tr>

<tr>

<td>3</td>

<td>Cost of software, equipment or services</td>

<td>Financial or who will foot the bill</td>

</tr>

<tr>

<td>4</td>

<td>Data management issues - lack of centralized control</td>

<td>Version control for collaborators outside university or department</td>

</tr>

<tr>

<td>5</td>

<td>Differences in participants' preferences and choices of collaboration tools</td>

<td>Using the same type of collaboration technologies (e.g. some use Skype, WebEx, Yahoo)</td>

</tr>

<tr>

<td>6</td>

<td>Differential levels of competence or experience with technologies</td>

<td>Varying levels of technological savvy among collaborators</td>

</tr>

<tr>

<td>7</td>

<td>Difficulty capturing and maintaining online meeting proceedings and other data about collaborative activities</td>

<td>Ability to record, playback or summarize</td>

</tr>

<tr>

<td>8</td>

<td>Difficulties facilitating and making contributions to online group discussion</td>

<td>Facilitating the raising and expression of questions by remote audiences remains tough</td>

</tr>

<tr>

<td>9</td>

<td>Discomfort of some individuals related to mediated communication (affective states, styles of work, etc.)</td>

<td>

'_I don't think everyone is comfortable with viewing people or having people view oneself on screen (e.g. videoconferencing, Skype)...creating an unbalanced group from the standpoint of affective state._'</td>

</tr>

<tr>

<td>10</td>

<td>Technology-mediated communication is less effective or productive (vs. in-person, face-to-face communication)</td>

<td>'Very few information and communication technologies can replace face-to-face communication, even if we see each other. I don't believe we behave the same way when communicating through information and communication technologies as we would face-to-face.'</td>

</tr>

</tbody>

</table>

#### Round 2

**Goal** The goal of Round 2 was to rank issues and assess consensus from all panel members.

**Data collection** The survey questionnaire presented to the panel members consisted of the twenty-seven-issue list with a short definition or explanation of each item, a glossary of terminologies used in the study, and a copy of their responses to the first round questionnaire. According to Schmidt ([1997](#sch97)), ranking around twenty items is considered reasonable at an early stage of Delphi. In this round, each panellist was asked to rank the listed items by importance, where one equalled the most important and twenty-seven the least. No two items were allowed the same ranking. A time frame of two weeks was given, and fourteen out of twenty-four panellists returned their responses.

**Analysing responses** The rankings provided by the fourteen panellists were recorded, and the mean ranks were calculated for each item. Table 3 shows the list of items with mean rank, variance of rank (_D_<sup>2</sup>), Kendall's _W_, and chi-squared (?<sup>2</sup>). From this table, items on the top in the mean rank column are rated _important_ by panellists. As seen in the table, there is no obvious top group of items in terms of mean ranking, and the investigators had a difficult time setting a cut-off point. To assess the group consensus, Kendall's _W_ was calculated and a low level of agreement (_W_ = 0.103) was found. In the nonparametric statistical approach, a strong agreement or consensus exists for _W_>= 0.7; a moderate agreement for _W_= 0.5; and a weak agreement for _W_< 0.3 ([Garcia-Crespo _et al._ 2010](#gar10)). This was somewhat expected considering that individual panellists come from a variety of research domains, and used different tasks and tools. This assumption prompted the investigators to consider the association, if any, between the two research domains (science and engioneering vs. social and behavioural sciences). As indicated in the previous studies ([Kendall and Gibbons, 1990](#ken90); [Schmidt 1997](#sch97)) Kendall's _T_ (rank order correlation coefficient) can be used to find the agreement between two groups. In this case, our calculation for _T_ resulted in -0.009 (approximately zero), which means that there was no association between the two groups' rankings. Consequently, the investigators decided to separate the full panel into two independent groups for the next round of data collection. By calculating the differences between the mean rank and the grand means, variance of rank was measured to assess how each item was dispersed. Chi-squared values were also calculated to demonstrate the level of association between the issues and ranks; this relationship was found statistically significant.

<table><caption>

Table 3\. Example of ranking of issues by expert panellists</caption>

<tbody>

<tr>

<th rowspan="1">Issue number</th>

<th>Mean rank</th>

<th>

_D_<sup>2</sup></th>

</tr>

<tr>

<td>26</td>

<td>7.64</td>

<td>40.45</td>

</tr>

<tr>

<td>25</td>

<td>10.57</td>

<td>11.76</td>

</tr>

<tr>

<td>10</td>

<td>11.18</td>

<td>7.95</td>

</tr>

<tr>

<td>27</td>

<td>11.64</td>

<td>5.57</td>

</tr>

<tr>

<td>20</td>

<td>11.71</td>

<td>5.24</td>

</tr>

<tr>

<td>3</td>

<td>11.93</td>

<td>4.28</td>

</tr>

<tr>

<td>23</td>

<td>12.14</td>

<td>3.46</td>

</tr>

<tr>

<td>8</td>

<td>12.36</td>

<td>2.69</td>

</tr>

<tr>

<td>1</td>

<td>12.71</td>

<td>1.66</td>

</tr>

<tr>

<td>6</td>

<td>12.86</td>

<td>1.3</td>

</tr>

<tr>

<td>24</td>

<td>12.89</td>

<td>1.23</td>

</tr>

<tr>

<td>13</td>

<td>13.25</td>

<td>0.56</td>

</tr>

<tr>

<td>9</td>

<td>13.36</td>

<td>0.41</td>

</tr>

<tr>

<td>5</td>

<td>13.57</td>

<td>0.18</td>

</tr>

<tr>

<td>11</td>

<td>13.86</td>

<td>0.02</td>

</tr>

<tr>

<td>14</td>

<td>14.82</td>

<td>0.67</td>

</tr>

<tr>

<td>21</td>

<td>15.57</td>

<td>2.46</td>

</tr>

<tr>

<td>19</td>

<td>15.64</td>

<td>2.69</td>

</tr>

<tr>

<td>12</td>

<td>15.68</td>

<td>2.82</td>

</tr>

<tr>

<td>18</td>

<td>15.89</td>

<td>3.57</td>

</tr>

<tr>

<td>17</td>

<td>15.93</td>

<td>3.72</td>

</tr>

<tr>

<td>22</td>

<td>15.93</td>

<td>3.72</td>

</tr>

<tr>

<td>16</td>

<td>16.04</td>

<td>4.16</td>

</tr>

<tr>

<td>15</td>

<td>16.5</td>

<td>6.25</td>

</tr>

<tr>

<td>7</td>

<td>16.96</td>

<td>8.76</td>

</tr>

<tr>

<td>4</td>

<td>18.5</td>

<td>20.25</td>

</tr>

<tr>

<td>2</td>

<td>18.86</td>

<td>23.62</td>

</tr>

<tr>

<td>

**Totals**</td>

<td>377.99</td>

<td>169.5</td>

</tr>

<tr>

<td>

**Grand means**</td>

<td>

**14.00**</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td>

_**W**_</td>

<td>

_**?<sup>2</sup>**_</td>

</tr>

<tr>

<td> </td>

<td>

**0.103**</td>

<td>

**37.657**</td>

</tr>

</tbody>

</table>

#### Round 3

**[Goal]** The goal of Round 3 was to re-rank issues and examine the differences by research domains.

**[Data collection]** In this round, the investigators decided to reduce the number of items to be ranked from twenty-seven to fourteen. Based on the mean ranks for each item we produced a list of fourteen items with mean ranks for each and item descriptions in the order of importance, rather than a random or alphabetical order. As a result, two different lists of items were sent to the two groups/panels (science and engioneering and social and behavioural sciences groups) separately. The panellists were again asked to rank these issues by importance. Seventeen panellists (nine from the science and engioneering panel and eight from social and behavioural sciences panel) returned their responses.

**[Analysing responses]** The most important fourteen items were decided based on the mean ranks (mean rank <13.5). The decision of the cut-off point was made by the investigators considering the wide gaps in mean ranking. This was a natural 'breaking point' which halved the original number of twenty-seven. Then Kendall's _W_ was calculated separately for each panel. As a result, Kendall's _W_ for the science and engioneering panel was 0.207, and 0.120 for the social and behavioural sciences panel. They were slightly higher than the previous round but still in the lower range.

### Presentation of findings

The findings of a Delphi study can be presented in any format: list of items, categories or tables with related statistics or narratives. Table 4 lists the final findings of the example study, which includes fourteen top issues that were identified by the science and engioneering and social and behavioural sciences panels respectively. The items are ranked in descending order by their final mean ranks.

<table><caption>

Table 4\. Top fourteen issues ranked by panels for each research domain, categorized by issue type (Ju and Pawlowski 2011)  
(Note: *=Top 14 issue for both groups)</caption>

<tbody>

<tr>

<th rowspan="1" colspan="3">

**science and engioneering GROUP**</th>

<th colspan="3">

**social and behavioural sciences GROUP**</th>

</tr>

<tr>

<th>Issue #</th>

<th>Issue</th>

<th>Issue Type</th>

<th>Issue #</th>

<th>Issue</th>

<th>Issue Type</th>

</tr>

<tr>

<td>20</td>

<td>Quality issues (audio/video)</td>

<td>Tool or technology</td>

<td>24</td>

<td>Time or effort to learn</td>

<td>Individual</td>

</tr>

<tr>

<td>25*</td>

<td>Unreliability of technology</td>

<td>Tool or technology</td>

<td>6*</td>

<td>Differential competence or experience</td>

<td>Individual</td>

</tr>

<tr>

<td>26*</td>

<td>Usability issues</td>

<td>Tool or technology</td>

<td>27*</td>

<td>Variance in tool availability</td>

<td>Resources</td>

</tr>

<tr>

<td>23</td>

<td>Technology infrastructure limitations</td>

<td>Tool or technology</td>

<td>25*</td>

<td>Unreliability of technology</td>

<td>Tool or technology</td>

</tr>

<tr>

<td>16</td>

<td>Limitations of collaboration tools</td>

<td>Tool or technology</td>

<td>8</td>

<td>Difficulties facilitating or contributing</td>

<td>Collaboration process</td>

</tr>

<tr>

<td>11</td>

<td>Incompatibility of technologies</td>

<td>Tool or echnology</td>

<td>26*</td>

<td>Usability issues</td>

<td>Tool or technology</td>

</tr>

<tr>

<td>6*</td>

<td>Differential competence or experience</td>

<td>Individual</td>

<td>10*</td>

<td>Information technology-mediated communication less effective</td>

<td>Collaboration process</td>

</tr>

<tr>

<td>1</td>

<td>Annoyances - tools</td>

<td>Tool or technology</td>

<td>3*</td>

<td>Cost</td>

<td>Resources</td>

</tr>

<tr>

<td>27*</td>

<td>Variance in tool availability</td>

<td>Resources</td>

<td>19</td>

<td>Negative impact on motivation</td>

<td>Collaboration process</td>

</tr>

<tr>

<td>10*</td>

<td>Information technology-mediated communication less effective</td>

<td>Collaboration process</td>

<td>21</td>

<td>Scheduling or time zone issues</td>

<td>Logistics</td>

</tr>

<tr>

<td>12</td>

<td>Information overload</td>

<td>Collaboration process</td>

<td>14</td>

<td>Lack of willingness to adopt or learn</td>

<td>Individual</td>

</tr>

<tr>

<td>3*</td>

<td>Cost</td>

<td>Resources</td>

<td>5</td>

<td>Differences in tool preferences or choices</td>

<td>Individual</td>

</tr>

<tr>

<td>22</td>

<td>Security and security policy</td>

<td>Tool or technology</td>

<td>9</td>

<td>Discomfort with mediated communication</td>

<td>Individual</td>

</tr>

<tr>

<td>13</td>

<td>Insufficient technical support</td>

<td>Resources</td>

<td>17</td>

<td>More misunderstandings</td>

<td>Collaboration process</td>

</tr>

</tbody>

</table>

As mentioned previously, the Delphi technique is highly conducive to producing a consensus among group members through several rounds of opinion solicitations. Hence, the presentation of Delphi results should reflect this convergence process. As illustrated in the above example study, Tables 2, 3 and 4 profile how the group consensus for this given topic was eventually achieved.

It is also recommended that the demographics of experts be provided so that the readers have a glance of the panel members' qualification and skill levels, which supports the relevance and reliability of respondents to a specific topic ([Brancheau and Wetherbe 1987](#bra87); [Schmidt 1997](#sch97)).

## Discussion: concerns and limitations

While the strengths of the Delphi method are evident, its challenges are also well perceived. A couple of concerns and limitations encountered in the course of conducting this study drew our attention.

_Number and range of identified issues and homogeneity of panel members:_ Twenty-four study participants were recruited into two different domain groups (social and behavioural sciences vs. science and engioneering) in order to examine whether or not differences exist in the barriers/challenges perceived by them using information and communication technologies. Each panellist was asked to generate at least ten major issues. The collected inputs were then collated and categorized by the investigators into a list of identified issues. It turned out that the spectrum and numbers of the identified issues were as broad as the diversity of the participants' domain areas in this study. The types of identified issues ranged from tool/technology, collaboration process, logistics and resources to individual experience or perception (see Table 4 for the identified issues in detail). The wide-ranging of the issues might have led to the lower agreement among the participants.

_Agreement rate among experts (panel members) for reliability:_Given the characteristics of the Delphi method, applying nonparametric techniques is considered to be preferred for several reasons. First, in practice, we may often encounter data that comes from a non-normal distribution which skewed, highly peaked, highly flat, etc., and is not covered by parametric methods. In such situations, nonparametric techniques should be used, because they do not assume a particular population probability distribution. In contrast, parametric statistical approaches, 'hypothesis testing and the confidence interval rely on the assumption that the population distribution function is known, or a few unknown parameters' ([Conover 1999](#con99): 115). Most parametric methods are based on the normality assumption, in which data are normally distributed and the 'parameters,' such as means, standard deviations and proportions, provide important clues to understanding the shape or spread of the probability distribution. Secondly, thanks to their distribution-free nature, nonparametric techniques 'are valid for data from any population with any probability distribution' ([Conover 1999](#con99): 116). Therefore, the data from a biased, non-random sample, like the Delphi's sample of experts, would still be valid using nonparametric methods with a good statistical power to data analysis. Third and most important, nonparametric techniques are equipped to handle ordinal data. They are 'often the only [methods] available for data that simply specify order (ranks)' ([Sprent and Smeeton 2000](#spr00): 5). As described previously, the Delphi methods depend heavily on iterative elicitations from respondents who are repeatedly asked to identify and rank items. Nonparametric methods include a family of statistical procedures used to analyse the ranked data, such as the Friedman test and Kendall's _W_; these can be used to test for evidence of consistency between the respondents who assign the ranking. Delphi researchers could consider these procedures useful to track consensus in a rigorous, statistical manner. Finally, when the study sample size is very small ([Siegel and Castellan 1988](#sie88)) nonparametric statistics are more appropriate, in order to retain the same statistical power to test null hypotheses.

With all above-mentioned characteristics of nonparametric statistics, some specific methods were considered for application in our example study. One of the focal points of using the Delphi method is to elicit and shape a group consensus. The degree of agreement achieved among raters (experts/panel members) must be substantial for researchers to make their decisions regarding the optimal number of iterations. Ensuring a certain degree of agreement is directly related to the reliability and validity of the data collected and consequently analysed from each round of inquiries. Moreover, as mentioned earlier, few of the previous Delphi studies specified their adequate level of agreement. The majority of Delphi studies used descriptive statistics, such as average, percentile or standard deviation, with ill-defined cut-off points for the agreement rates. To remedy this, our sample study sought a more sound and consistent way to demonstrate degree of agreement rates by calculating Kendall's W. An analysis of participant rankings indicated that the mean ranks are relatively flat and widely dispersed among the identified issues. This could be due to the different issues (challenges and conflicts) emphasized by the individual panellists, and could be considered for future endeavour.

## Conclusions

The Delphi method is a popular method used by researchers to investigate a variety of research questions in the field of library and information science. Often the Delphi method is not considered as rigorous as other research methods. This is primarily due to a lack of standard statistical tests which would ensure the validity and reliability of the research. Without such statistical tests, it would then be hard to accurately define the optimal number of iterative rounds for data collection. It also would be not statistically rigorous to designate consistent cut-off points for the level of agreement among the panel members. In order to address these critical issues, the authors attempted to explore the applicability of nonparametric statistics as one of the possible solutions, via an illustrative study. More specifically, in our sample study, research attempts were made to identify the source of the challenges encountered by collaborative researchers, and to understand the interrelations among the identified issues.

There are two advantages to our nonparametric statistics approach. One is that, when using a small sample size, our approach provides rigorous and reliable statistics regarding the degree of consensus among the panellists. Another advantage is the justification for setting the cut-off point for each iteration. In most of the previously conducted LIS Delphi studies, explanations about the levels of consensus (or agreement) among panellists at each round are under-emphasized, creating a lack of support for the underlying logic. For example, using percentages or the standard deviation of agreement among panellists, or collecting only the descriptive characteristics of data from each round, does not ensure a level of consensus has been achieved, and results are less predictable. Using our approach could respond to the criticism of this method being 'not a statistically rigorous methodology for predicting the future' ([Ludwig and Starr 2005](#lud05): 316).

We propose our approach with one proviso. As discussed, when the homogeneity of the panel of experts is not guaranteed and Delphi researchers have to deal with a variety of broad and complex topics, which need to be identified in their study, then the approach presented in our example study would not be well served. Therefore, the choice of the method should be made carefully with consideration for the many tenets that define good, basic research. That having been said, the Delphi method with our nonparametric statistical approach can, potentially, be an excellent tool for LIS researchers.

## Acknowledgements

Authors would like to thank referees, the editor (North American editor), and the copyeditor for valuable feedback and constructive suggestions to improve this manuscript. We also appreciate Rachel Gifford for her time and effort to read and edit our manuscript.

## About the author

Boryung Ju is an Associate Professor in the School of Library and Information Science, Louisiana State University, USA. She received her Master's degree in Library and Information Science from Indiana University and her PhD in Information Studies from the Florida State University, USA. She can be reached at: [bju1@lsu.edu](mailto:bju1@lsu.edu).  
Tao Jin is an Assistant Professor in the School of Library and Information Science, Louisiana State University, USA. He received his Master's degree in Library and Information Science, and PhD in Information Studies from McGill University, Canada. He can be reached at: [taojin@lsu.edu](mailto:taojin@lsu.edu).

</section>

<section>

## References

*   Addison, T. (2003). E-commerce project development risks: evidence from a Delphi survey. _International Journal of Information Management_, **23**(1),25-40.
*   Agha, S. S., & Akhtar, S. (1992). The responsibility and the response: sustaining information systems in developing countries. _Journal of Information Science_, **18**(4),283-292.
*   Ambrosiadou, B. V., & Goulis, D. G. (1999). The Delphi method as a consensus and knowledge acquisition tool for the evaluation of the diabetes system for insulin administration. _Medical Informatics and the Internet in Medicine_, **24**(4), 257-268.
*   Baruchson-Arbib, S., & Bronstein, J. (2002). A view to the future of the library and information science profession: a Delphi study. _Journal of the American Society for Information Science and Technology_, **53**(5), 397-408.
*   Borko, H. (1970). _A study of the needs for research in library and information science education_. Washington, D.C.: U.S. Office of Education, Bureau of Research.
*   Brancheau, C., & Wetherbe, C. (1987). Key issues in information systems management. _MIS Quarterly_, **11**(1),23-45.
*   Brouwer, W., Oenema, A., Crutzen, R., de Nooijer, J., de Vries, N. K. & Brug, J. (2008). [An exploration of factors related to internet-delivered behavior change interventions aimed at adults: a Delphi study approach.](http://www.webcitation.org/6ITddU8Fw) _Journal of Medical Internet Research_**10**(2), Retrieved 10 July, 2012 from http://www.jmir.org/2008/2/e10/ (Archived by WebCite?athttp://www.webcitation.org/6ITddU8Fw)
*   Conover, W.J. (1999). _Practicalnonparametric statistics_. (3rd. ed.). New York: John Wiley & Sons.
*   Dalkey, N.(1968) _Predicting the future_. Santa Monica, CA: Rand Corporation
*   Dalkey, N. & Helmer, O. (1963). An experimental application of the Delphi method to the use of experts. _Management Science_, **9**, 458-467.
*   Daniel, E. M. & White, A. (2005). The future of inter-organisational system linkages: findings of an international Delphi study. _European Journal of Information Systems_, **14**(2),188-203.
*   Dhaliwal, J. S. & Tung, L. (2000). Using group support systems for developing a knowledge-based explanation facility. _International Journal of Information Management_, **20**(2), 131.
*   Du, Y. (2009). Librarians' responses to 'reading at risk': a Delphi study. _Library and Information Science Research_, **31**(1), 46-53.
*   Eldredge, J. D., Harris, M. R., & Ascher, M. T. (2009). Defining the Medical Library Association research agenda: methodology and final results from a consensus process. _Journal of the Medical Library Association_, **97**(3), 178-185.
*   Feret, B., & Marcinek, M. (1999). The future of the academic library and the academic librarian: a Delphi study. _Librarian Career Development_, **7**(10), 91-107.
*   Fischer, R. G. (1978). The Delphi method: a description, review and criticism. _The Journal of Academic Librarianship_, **4**(2), 67-70.
*   Garcia-Crespo, A., Colomo-Palacios, R., Soto-Acosta, P., & Ruano-Mayoral, M. (2010). A qualitative study of hard decision making in managing global software development teams. _Information Systems Management_,?**27**(3), 247-252.
*   Gonzalez, R., Gasco, J., & Liopis, J. (2006). Information systems managers' view about outsourcing in Spain. _Information Management and Computer Security_, **14**(4), 312-326.
*   Goodman, C. M. (1987). The Delphi technique: a critique. _Journal of Advanced Nursing_, **12**, 729-734
*   Harer, J. B., & Cole, B. R. (2005). The importance of the stakeholder in performance measurement: critical processes and performance measures for assessing and improving academic library services and programs. _College and Research Libraries_, **66**(2), 149-170.
*   Holsapple, C., & Joshi, K. (2000). An investigation of factors that influence the management of knowledge in organizations. _Journal of Strategic Information Systems_, **9**(2/3), 235-261.
*   Ju, B., & Pawlowski, S. (2011). Exploring barriers and challenges of information and communication technology use in distributed research today: a ranking-type Delphi study. _Proceedings of the American Society for Information Science and Technology_, **48**, 1-9.
*   Keller, A. (2001). Future development of electronic journals: a Delphi study. _The Electronic Library_, **19**(6), 383-396.
*   Kendall, G., & Gibbons, D. (1990). _Rank correlation methods_. London: Edward Arnold.
*   Kochtanek, T. R., & Hein, K. K. (1999). Delphi study of digital libraries. _Information Processing and Management_, **35**(3), 245.
*   Linstone, H., & Turoff, M. (1975). _The Delphi method: techniques and applications_. London: Addison-Wesley Publishing Company, Inc.
*   Ludwig, L., & Starr, S. (2005). Library as place: results of a Delphi study. _Journal of Medical Library Association_, **93**(3), 315-326.
*   Lukenbill, B., & Immroth, B. (2009). School and public youth librarians as health information gatekeepers: research from the Lower Rio Grande Valley of Texas. _School Library Media Research_, **12.**
*   Maceviciute, E., & Wilson, T.D. (2009). [A Delphi investigation into the research needs in Swedish librarianship](http://informationr.net/ir/14-4/paper419.html). _Information Research_, **14**(4), paper 419\. Retrieved 20 May, 2012 from http://InformationR.net/ir/14-4/paper419.html (Archived by WebCite? at http://informationr.net/ir/14-4/paper419.html)
*   McFadzean, E., Ezingeard, J., & Birchall, D. (2011). Information assurance and corporate strategy: a Delphi study of choices, challenges, and developments for the future. _Information Systems Management_, **28**(2), 102-129.
*   Meijer, A. A. (2002). Geographical information systems and public accountability. _Information Polity: The International Journal of Government and Democracy in the Information Age_, **7**(1), 39-47.
*   Missingham, R. (2011). Parliamentary library and research services in the 21st century: a Delphi study._IFLA Journal_, **37**(1), 52-61.
*   Müller, R. M., Linders, S., & Pires, L.F. (2010). Business intelligence and service-oriented architecture: a Delphi study. _Information Systems Management_, **27**, 168-187.
*   Neuman, D. (1995). High school students? use of databases: results of a national Delphi study. _Journal of the American Society for Information Science_, **46**(4), 284-298.
*   Nicholson, S. (2003). Bibliomining for automated collection development in a digital library setting: using data mining to discover web-based scholarly research works. _Journal of the American Society for Information Science and Technology_, **54**(12), 1081-1090.
*   Okoli, C., & Pawlowski, S. (2004). The Delphi method as a research tool: an example, design considerations and applications. _Information and Management_, **42**, 15-29.
*   Pomerantz, J., Nicholson, S. & Lankes, R. (2003). Digital reference triage: factors influencing question routing and assignment. _Library Quarterly_, **73**(2), 103.
*   Reilly, K. (1970). The Delphi technique: fundamentals and applications. In H. Borko (Ed.), _A study of the needs for research in library and information science education_ Washington, D.C.: U.S. Office of Education, Bureau of Research. 252-267.
*   Saranto, K., & Tallberg, M. (2003). Enhancing evidence-based practice ? a controlled vocabulary for nursing practice and research. _International Journal of Medical Informatics_, **70**(2/3), 249-253.
*   Saunders, L. (2009). The future of information literacy in academic libraries: a Delphi study. _Portal: Libraries and the Academy_, **9**(1), 99-114.
*   Siegel, S. & Castellan, N. (1988). _Nonparametric statistics for the behavioral sciences_. (2nd. ed.). New York: McGraw-Hill.
*   Schmidt, R. (1997). Managing Delphi surveys using nonparametric statistical techniques. _Decision Sciences_, **28**(3), 763-774.
*   Smiraglia, R. P. (2010). A research agenda for cataloging: the CCQ editorial board responds to the year of cataloging research. _Cataloging and Classification Quarterly_, **48**(8), 645-651.
*   Sprent, P., & Smeeton, N.C. (2000). _Applied nonparametric statistical methods_. (3rd. ed.). Boca Raton, FL: Chapman and Hall/CRC.
*   Stevens,S. (1946). On the theory of scales of measurement. _Science,_, **103**(2684), 677?680.
*   Westbrook, L. (1997). Information access issued for interdisciplinary scholars: results of a Delphi study on women?s studies research. _The Journal of Academic Librarianship_, **23**(3), 211-216.
*   Weingand, D. (1989). Wisconsin continuing education profile: a Delphi projection of needs, 1986\. _Journal of Education for Library and Information Science_, **30**(1), 39-50.
*   Yarnal, B., Harrington, J., Comrie, A., Polsky, C., & Ahlqvist, O. (2009). Lessons learned from the HERO project. In B. Yarnal, C. Polsky, & J. O'Brien (Eds.), _Sustainable Communities on a Sustainable Planet: The Human-Environment Regional Observatory Project._ Cambridge, UK: Cambridge University Press. 317-338.
*   Zhang, Y., & Salaba, A. (2009). What is next for functional requirements for bibliographic records? A Delphi study. _Library Quarterly_, **79**(2), 233-255.
*   Zins, C. (2007). Conceptions of information science. _Journal of the American Society for Information Science and Technology_, **58**(3), 335-350.

</section>

</article>

* * *

## Appendices

## Appendix: Sample instrument

<figure>

![Figure 2](../p589fig2.jpg)</figure>

<figure>

![Figure 3](../p589fig3.jpg)</figure>

<figure>

![Figure 4](../p589fig4.jpg)</figure>