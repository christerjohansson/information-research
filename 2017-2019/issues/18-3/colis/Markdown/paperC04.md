<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# The public library user and the charter tourist: two travellers, one analogy

#### [Catarina A.M Eriksson,](mailto:catarina.eriksson@hb.se) [Katarina E. Michnik](mailto:katarina.michnik@hb.se); and [Yoshiko Nordeborg](mailto:yoshiko.nordeborg@hb.se)  
Bibliotekshögskolan, University of Borås, Swedish School of Library and Information Science, Allégatan 1, 501 90 Borås, Sweden

#### Abstract

> **Introduction**. A new theoretical model, relevant to library and information science, is implemented in this paper. The aim of this study is to contribute to the theoretical concepts of library and information science by introducing an ethnological model developed for investigating charter tourist styles thereby increasing our knowledge of users' approaches to public libraries.  
> **Method**. This is an empirically grounded study. One hundred and eighty-two library visitors were interviewed at Swedish public libraries during 2011.  
> **Analysis**. Both quantitative and qualitative content analysis were used in the treatment of the empirical data.  
> Results. The paper is divided into three parts. The first part introduces the ethnological model. The second part presents the application of the model on the empirical data. The third part identifies possible research questions concerning public library user approaches.  
> **Conclusions**. This study shows that it is fruitful to apply a travel analogy for mapping user approaches to public libraries. Five public library user approaches are identified. The application can be used as a tool in order to discuss priorities in public library activities.

<section>

## Introduction

The origin of this study is a research project concerning the social functions and values of libraries. As a part of this project, short structured interviews were conducted with public library users. When analysing the interviews, we recognized a metaphorical similarity between how informants talk about library use and how human travelling is described, in a general sense. The similarity was recognized on a high, abstract level – no informant compared the use of the public library to travelling – but there were enough similarities to awaken a question: could an analogy between library use and travelling contribute to the formulation of new, fruitful questions concerning public library activities?

An analogy between library use and travelling is not new. It has formerly been used in traditional discussions on the nature of education as well as in terms of the classic Grand Tour for young aristocrats. In this sense, the travel analogy leans on great traditions.

The aim of this paper is to contribute to the theoretical concepts of library and information science by introducing an ethnological model developed for investigating charter tourist styles, as a means of understanding users approaches to public libraries. Ethnological theories and methods have been used earlier in LIS (Enmark, 1990). We explore users’ approaches to the public library and relate the results to the public library’s possible outcomes.

**Our research questions are:**

*   Is it fruitful to analyse user’s statements concerning their public library use according to a charter tourist style model? If so, what would this model render visible concerning public libraries and public library use?
*   What new questions concerning public libraries activities emerge from this experiment?

## Background

The public library is often described as a product of modern society ([Vestheim, 1997](#vestheim97)). Change such as technological development, the evolution of information and communication methods as well as globalization and the emergence of a so-called multicultural society in western society today impact on the role of the public library. The traditional norms of the public library institution are being challenged as the library struggles to prove its relevance to society. Several research studies focus on how ongoing transitions affect the public library and how it is assigned new roles. ([Audunson, 1996](#audunson96); [Vestheim, 1997](#vestheim97); [Kann-Christensen and Pors, 2004](#kann04); [Audunson, 2005](#audunson05); [Hvenegaard Rasmussen and Jochumsen, 2007](#hvenegaard07); [Evjen and Audunson, 2009](#evjen09); [Hansson, 2010](#hansson10); [Jochumsen, Hvenegaard Rasmussen and Skot-Hansen, 2012](#jochumsen12)). In Nordic LIS research today, the public library is often positioned as a meeting-place. The public library as a meeting-place has been investigated, both as a high-intensive meeting-place and as a low-intensive meeting-place ([Audunson, 2005](#audunson05); [Aabø, Audunson and Vårheim, 2010](#aabo10); [Aabø and Audunson 2012](#aabo12)).

In their classical model, Andersson and Skot-Hansen ([1994](#andersson94)) present the different roles a public library may have in a local community. The public library is described as a cultural center, a knowledge center, an information center and as a social center. Recently this model has been transformed. In the new model, the public library is depicted as an inspiration space, a learning space, a meeting space and a performative space ([Jochumsen, Hvenegaard Rasmussen and Skot-Hansen, 2012](#jochumsen12)).

There is a tradition of analysing and describing public library users in different ways. Four different user discourses have been identified in Swedish library journals; the general education discourse, the pedagogical discourse, the information technology discourse and the information management discourse ([Hedemark, Hedman and Sundin, 2002](#hedemark02)). Johannsen ([2009](#johannsen09)) observes four different conceptions of users in the LIS research undertaken 1960 – 2009: the citizen, the victim, the customer and the co-creator. These understandings of the users are library context driven. In our study user categorizations are derived from the users’ statements on the public library.

Some Swedish statistical surveys pay attention to the role of the public library and to user´s attitudes towards public libraries. There are studies which describe the causes of the decrease in public library use and studies about what users do at the library ([Höglund and Wahlström, 2005](#hoglund05); [Höglund, 2010](#hoglund10)). In general, users are satisfied with the public library services and their demands are often framed within traditional library services ([Svensk Biblioteksförening, 2011](#svensk11)).

## Methodology

This paper is based on data from a larger research project. In this project 182 structured interviews with public library users were conducted. These interviews took place at 14 different public libraries in western Sweden. The locations of the libraries vary; major cities, suburbs, towns and villages. The interviews were conducted by three persons in June 2011 and were recorded and transcribed. The participation rate was high and only a few people declined. A consequence of data collecting during the summer is that the users visited the library during their spare time and probably not for educational or professional purposes. If the data had been collected during term time the statements might possibly have been different.

The transcribed interviews were read several times in order to code and analyse the statements, in accordance with qualitative and quantitative content analysis. Four persons worked on the analysis. During the readings we discovered that the statements on public library use and functions resembled descriptions of travelling on an abstract level. This gave birth to the idea of a comparison between public library use and travelling as a means of eliciting new aspects on statements concerning public library use, functions and value. We were curious if such an analogy could contribute to the formulation of new, fruitful questions concerning public library activities and the development of the LIS field. This approach would make it possible to say something about the rhetoric of libraries, although not about its practices.

Among other possible ways of travelling, we chose to make a comparison between the public library and charter tourism. This is a consequence of the following circumstances: The first is the result of the collected data. The data do not include any references to school or work tasks and therefore presuppose a kind of leisure travel, rather than business travel. The second is that the analogy must include the user, the librarian and the library and this can be implied through the charter tourism concept. The third circumstance is the existence of a theoretical charter tourist styles model ([Wolf, 2001](#wolf01)) which was found through information searching at a research library. We found these circumstances valid and robust enough for making an attempt on the subject.

In her doctoral thesis _By charter to Estoril_ Eva Wolf ([2001](#wolf01)) argues that social patterns are reflected in the practices of individual tourists. When investigating individual tourist motives and expectations, she identifies five collective tourist patterns; recreation tourists, cultural tourists, action tourists, compromisers and individualists. We use the identified patterns as an analytical tool in the work at hand.

Wolf ([2001](#wolf01)) defines charter tourism as journeys planned and arranged by someone other than the traveller, where the traveller has no or limited means of changing or adjusting the plan. Some facilitating arrangements are especially made for the charter tourist group by the travel organizer and most of the travellers are not acquainted with each other when the journey begins. Charter tourism is seen as a reasonably accessible way of travelling.

Implementing the model requires entering some agreements and we have been able to identify some similarities to the public library concept. For instance, the user knows what he/she gets when using the public library as it uses are rather established. Some arrangements at the library are for the convenience of the users and most users are not known to each other. A fundamental condition for the analogy is the setting - who is who in this picture. We dispose the setting as following:

**The public library user as a charter tourist**. This is the visitor role in this setting. The user is the visitor. The visitor accepts the implicit conditions of being charter tourist and public library user.

**The public library as the charter destination**. There is a selection of offers designed for the user/tourist at the library/destination. In the analogy the public library takes on the role of both location and resort.

**The librarian as staff or natives at the destination**. The destination is populated with persons who are familiar with local circumstances and know how to manage visitors. It is important for the librarians/staff/natives to make the library/destination accessible, essential and relevant to the users.

It is important to note that charter tourism is a commercial activity while public library activities are financed through taxes and are therefore free for individuals. We ignore this difference as we do not see that it affects the analogy, according to our aim.

Now we will pay attention to the different five charter tourist styles identified by Wolf ([2001](#wolf01)), recreation tourists, cultural tourists, action tourists, compromisers and individualists. All styles contain streaks of the other styles but there are also some distinct features. We point out that we do not describe individuals in this study but approaches.

**Recreation tourists** long for relaxation and distraction. Recreation tourists seek a calm and pleasant retreat far from everyday life. Recreation tourists avoid intensive environments and they do not take part in activities. Although they do not avoid socializing during the holiday, socializing is not a goal. Recreation tourists are mostly satisfied and seldom complain; they usually point out benefits. However, they tend not to tolerate breaches when the staff fails to meet requirements which recreation tourists assume are reasonable.

**Cultural tourists** are characterized by their cultivation and knowledge motives. They are interested in the cultural selection offered at the destination. Cultural tourists know what they want to do at the destination; they are well prepared and know their business. They seldom take part in the activities offered by the travel organizer and prefer to do things on their own. They search for formal perfection, authenticity and originality. They dislike being identified as mass tourists and identify themselves as individual travellers who value their independence.

**Action tourists** aim for “fun” and “excitement”. They travel by charter in order to do something unusual and unexpected; to have new experiences. They want to do something exciting that does not affect their daily lives.

**Compromisers want** a balance between relaxation and cultivation, action and adventures. They want to have a little of everything but in moderation.

**Individualists** search for a maximum of variations – the more the better. They take the opportunity to give vent to their individual sport interests and, compared to the other tourist styles, they are characterized by making no or very little difference between everyday life and vacation.

There are some weaknesses in the chosen perspective. The travelling analogy is limited by its focus on the charter tourist concept. A comparison to other ways of travelling, for example, business travellers, back-packers, hikers, or campers would probably uncover other aspects of public library activities. Another limitation is that the model describes people in a restricted context: leisure time. No attention is paid to provision of education, which is one of the fundamental affordances of the public library. A further limitation is that not all tourists are expected to return to the same resort while public library users are expected to visit the library repeatedly.

Despite this, we consider that there are reasons to experiment with this analogy as some fruitful, new questions may be formulated. The model is, for instance, not normative; by using a model not based in a library context or in LIS theory, we have no “library filter” when confronting our empirical data. This enables us to investigate public library users approaches without regard to the idea of individual and societal development which is usually embedded in public library ideology.

It is important to emphasize that we examine public library users' _statements_ on their library use and expectations as well as their _statements_ on the library’s functions and value. In other words, we do not examine user practices.

## Findings

In this section we apply the five tourist styles ([Wolf, 2001](#wolf01)) on public library use. We exemplify and illustrate this application with quotes from our empirical material. The quotes are translated from Swedish into English, a few interviews were conducted in English.

### The recreation tourist

Recreation tourists have their counterparts in users who visit the library for relaxing. Previous studies have pointed out that public library users value and appreciate the library as a calm, quiet and peaceful place ([Svensk biblioteksförening, 2011](#svensk11)). In our empirical data it is clear that the library is used and seen as a place in which the user can relax. In other words, the library becomes an oasis, a place visited by the user in order to take a break from everyday life and have time for themselves for a while:

> I went to the library for relaxing. I feel better afterwards. (I:2-17)

For the recreation tourist, the purpose of the journey is not to meet new people but possibly to spend time with those you already know (Wolf, 2001). Our material does not clearly support the meeting-place function pointed out by other researchers ([Audunson, 2005](#audunson05): [Aabø, Audunson and Vårheim, 2010](#aabo10): [Aabø and Audunson, 2012](#aabo12)). 19% of the informants describe the library as an important meeting-place where people can come into contact with each other or with new ideas but not, however, for themselves; according to the statements only 3% of the informants use the library as a meeting-place. We want to emphasize that there is a difference between the informants’ expectations of the library concerning themselves compared to what the library can do for other visitors. This is exemplified by statements where the informants say that it is important for other inhabitants, i.e. children and immigrants, for getting in touch with other people but, at the same time, the informants do not speak about the public library as meeting place for themselves. When our informants describe the public library as a meeting-place for themselves, the meetings are between acquaintances:

> Where should we go otherwise? We [a couple of friends] can sit here and get in touch with the rest of the world... (I:3-2)

Wolf ([2001](#wolf01)) found that recreation tourists were not interested in the attractions, events, guided trips or excursions organized by the travel organizer and seldom participated in them. We can also discern this behaviour among our informants – a majority (70.3%) of the informants say that they don't participate in any cultural activities or other events arranged by the public library:

> No, never. Once I almost went to a lecture by Sven-Eric Liedman. (I:7-10)

Earlier studies have shown that participation in cultural events at public library activities is low ([Höglund, 2010](#hoglund10)). Our empirical data reveals the same tendency: 94.5% of the informants in our study used the library to borrow and/or to read while 29.7% of the informants said that they sometimes participated in cultural events organized by the library.

Recreation tourists are usually tolerant if something doesn't work ([Wolf, 2001](#wolf01)). Poor library service is, according to Höglund and Wahlström ([2005](#hoglund05)), not a major reason for decreasing library use. The library visitors in our empirical data are, like recreational tourists generally, tolerant and satisfied with how things work at the library. They are particularly appreciative of the services that the library staff provides:

> They are always helpful, once when I asked about a lesbian, Norwegian author they knew who I meant, so they read a lot themselves. (I:3-12)

If something does not work, the user is usually lenient:

> Last week the system was messed up so they couldn’t lend out stuff but they solved it another way, an alternative way. (I:2-20)

But even if there are several statements expressing satisfaction with the library and librarians in recreation tourist style, there are also statements concerning staff/librarian failures to meet requirements found reasonable by the user and which result in user protests. Compared to the percent of satisfied statements, 95.1%, the proportion of dissatisfied respondents is low, 2.7%.

> The CD was missing [in the cover] when I returned it and at the desk she said that I should go the (X) department myself and tell them. But I thought that she could do that, she works here, it’s her job. So I didn't go, and I'm pleased... and as we’re on the subject, she was generally unpleasant. (I:8-10)

### Cultural tourists

Cultural tourists are characterized by their motivation to pursue their particular interests. They are well prepared for the trip and clearly know what they want to do on their vacation. They do not want to be seen as mass tourists but as individual travellers on their own ([Wolf, 2001](#wolf01)). If we apply this description to our empirical data, we can discern cultural tourists. Firstly, the cultural tourist has an analogical counterpart in the user whose aim is to learn. He/she assumes that the library’s purpose is to cultivate the individual and society:

> Because, it’s very important in a society based on knowledge, everybody can come here and look for anything they like. And it raises knowledge levels and the education of the people because children come here too and they find (…), without paying, lots of books about everything. (I:2-19)

Secondly, the aim of the library visit is clearly specified. This user prepares for the visit and visits it purposively, i.e. the user comes to the library, picks up ordered books and then leaves the library immediately.

The third approach is identified by statements according to which the library user believes that he/she can manage him-/herself and does not need to be assisted by the library staff:

> No, I never ask for help, I do everything myself. (I:2-3)

### Compromisers

Compromisers have an easy-going approach to information/culture and relaxation, they want both but with moderation ([Wolf, 2001](#wolf01)). Based on this analogy, we can identify attitudes towards the public library which involve making the most of what the public library offers...

> Everything from reading newspapers to borrowing books, films, using the computers, printing, scanning, I use the whole library. (I:3-15)

… and where the library is seen as a place offering many different opportunities. In short, the public library is considered to be a place where you can do different things - everything from finding information to meeting other people:

> Well, I think that 1\. we have a cultural heritage here, 2\. people meet and 3\. you can... you can learn about a lot of things and take the opportunity to find information that you wouldn’t do otherwise... (I:3-18).

### Action tourists

The key word in the vocabulary of an action tourist is “fun”. These tourists travel in order to experience something new, exciting and exceptional ([Wolf, 2001](#wolf01)). We have not found any clear examples of this sort of approach in our empirical data. The informants tend not to associate the library as something “fun”, “challenging” or “exciting”. Although not all informants have a clearly defined goal in visiting the library, the informants in our study seem to have a clear view of what they expect from the library and what the library can do for them. A broader interpretation of the action tourist approach among public library visitors could be that the users take the opportunity to borrow or read documents at the library which they would not otherwise have tried. In our empirical data, we cannot find any answers that clearly support this, at the same time we have not asked any questions about this specific kind of approach. But this could be an interesting question to investigate in future studies.

### Individualists

Characteristic for individualists is a constant and restless quest for variation - individualists want to experience and do as much as possible within a specific timeframe ([Wolf, 2001](#wolf01)). When we apply this style to a public library context, the library is not just seen as a culture, knowledge, information and/or social center ([Andersson and Skot-Hansen, 1994](#andersson94)) but is also expected to fulfill other desires and needs among library users. It concerns services expected of the library because there is no other provider of the desired service. We have not been able to clearly identify this approach in our empirical data but we have found certain statements which could be interpreted as the library considered as a place used simply because the option is offered. In these cases it is less important that it is the library which provides this option:

> Well, it’s that you can go there with the kids. Especially in the winter months, when it’s not much fun to be outside in playgrounds, they [at the library] usually have other areas where they [the children] can play a bit. (I:9-10)

> Used the xerox... This is the only place where you can do that. (I:3-4)

## Discussion

When we look at our results, we note that the analogy with tourism is fruitful and has given new perspectives on the social functions and values of library services. This comparison has also created new questions. Our understanding of library users’ approaches has been broadened when we have used a model with five individual requirements not limited to the library context. Thereby, potential conflicts between those individual requirements are possible to identify. This will be developed below.

We can observe that the attitudes and values of public library users in our study are most similar to those of recreational tourists, cultural tourists and compromisers. One explanation for this could be that our informants came to the library in their spare time and were not visiting the library in connection with a specific organized (cultural) activity at the library. The approaches of action tourists and individualists are not prominent in our empirical data although traces of these styles can be found and create grounds for further studies of the approaches of the actions tourists and individualists at the public library. As we can see, the main difference between the user approach as an action tourist and as an individualist is that the former visits the library to experience something exceptional or for fun while the latter is interested in a particular service which he/she is in need of, no matter of who is providing the service in question.

On one hand, we can see parallels between the public library described as a cultural center, knowledge center, information center and/or as a social center ([Andersson and Skot-Hansen, 1994](#andersson94)) and the user approaches which can be compared to the recreational tourists, cultural tourists and compromisers. On the other hand, we can see similarities between the public library described as an arena for inspiration, learning, meetings and performances ([Jochumsen, Hvenegaard Rasmussen and Skot-Hansen, 2012](#jochumsen12)) and the user approaches which resemble action tourists and individualists. In recent years, an intense discussion has taken place in Sweden regarding the development of public libraries as so-called centers of experience where the public library provides more than just document-based resources. The discussion has, among other things, focused on whether this development is a useful means of reaching non-users or if such a development could change the public library dramatically and negatively. We see this as a part of the struggle for relevance in society, and see it as a cultural policy issue ([Vestheim, 1997](#vestheim97); [Kann-Christenson and Pors 2004](#kann04); [Evjen and Audunson 2009](#evjen09)). During the course of this study, we began to ask if an argument for such a development could be to encourage the action tourists and individualists to visit the public library. This could be done by attracting action tourists with exciting events in the public library and the individualists by promising services that are not traditionally included in the range of public library services. Public libraries in Sweden offer many examples of non-traditional library services: organized dance, lending of bicycles and Nordic walking poles as well as concerts arranged during regular opening hours, etc. The consequences for the public library, the library profession, for traditionally oriented library users and for society have yet to be explored.

Other questions can be posed within the framework of the analogy. If we imagine library staff as natives/staff at the destination, which approaches can be applied? Two possible approaches are preservers and renewers. At its extreme, the renewer wants to adapt services in order to satisfy everyone and the preserver wants to maintain traditional content. Applied to a public library context, will conflicts emerge between different tourist styles/library user approaches at the public library? Is there room for all the different tourist styles/library user approaches at the library? How will libraries deal with emerging tensions? A consequence of our chosen analogy is to inquire if tendencies recognized in the charter tourist context can be translated into the public library context? A possible example is illustrated by hotels with a “no children” policy, which means that one group of people is excluded in order to please another group. If we apply this to a library context, is it possible to think that “no children” periods could be introduced at specified times?

From a theoretical point of view, one way to understand the transformation of the classical model of the public library ([Andersson and Skot-Hansen, 1994](#andersson94)) into the new model ([Jochumsen, Hvenegaard Rasmussen and Skot-Hansen, 2012](#jochumsen12)) is to see it as a requirement that libraries adapt their activities to the desires of action tourists at the expense of recreation tourists and perhaps even of cultural tourists. Whose interests, in this case, come into conflict in this development – those of library staff, politicians, cultural actors, commercial actors, users etc? Perhaps public libraries provide services requested by individualists and action tourists with the idea that these users will eventually transform into recreation tourists, cultural tourists and compromisers after visiting the library and discovering its traditional qualities? What happens if this transformation never occurs? What happens if non-users attracted by the offer concerning bicycle loans or Nordic walking poles, never discover traditional services at the library and are not interested in using the library for other things than borrowing bicycles and Nordic walking poles? How is this kind of situation managed in the charter tourist context? One solution is to provide tailor-made charter journeys. Back to a public library context, this could mean a development of public libraries with different profiles. In such scenario, what are the consequences for potential library users who do not fit the adopted profile?

Finally, some words on using a travel analogy on public libraries. If we consider different kinds of travellers in the public library landscape, for example, explorers, backpackers and business travellers, what other discoveries might be made?

</section>

<section>

## References

<ul>
<li id="aabo12">Aabø, Svanhild and Audunson, Ragnar (2012). Use of Library Space and the Library as Place. <em>Library and Information Science Research</em>, <strong>34</strong>, 138-149
</li>
<li id="aabo10">Aabø, Svanhild, Audunson, Ragnar and Vårheim, Andreas (2010). How do public libraries function as meeting places?, <em>Library &amp; Information Science Research</em>, <strong>32</strong>(1), 16-26
</li>
<li id="andersson94">Andersson, Marianne and Skot-Hansen, Dorte (1994). Det lokale bibliotek: afvikling eller udvikling. <em>Copenhagen: Danmarks Biblioteksskole</em>
</li>
<li id="audunson96">Audunson, Ragnar (1996). Change processes in public libraries: a comparative project within an institutionalist perspective. <em>Oslo: Høgskolen i Oslo</em>
</li>
<li id="audunson05">Audunson, Ragnar (2005). "The public library as a meeting-place in a multicultural and digital context: The necessity of low-intensive meeting-places" <em>Journal of Documentation</em>, <strong>61</strong>(3), 429-441
</li>
<li id="enmark90">Enmark, Romulo (1990). Defining the library's activities. <em>Göteborg: Centre for Library Research</em>
</li>
<li id="evjen09">Evjen, Sunniva and Audunson, Ragnar (2009). ”The complex library: Do the public’s attitudes represent a barrier to institutional change in public libraries?”, <em>New Library World</em>, <strong>110</strong>(3-4), 61-174
</li>
<li id="hansson10">Hansson, Joacim (2010). Libraries and identity: the role of institutional self-image and identity in the emergence of new types of libraries. <em>Oxford: Chandos</em>
</li>
<li id="hvenegaard07">Hvenegaard Rasmussen, Casper and Jochumsen, Henrik (2007). Problems and Possibilities: The Public Library in the Borderline between Modernity and Late Modernity. <em>Library quarterly</em>, <strong>77</strong>(1), 45-60
</li>
<li id="hoglund10">Höglund, Lars (2010). Bibliotek och demokratiska möten. In Sören Holmberg and Lennart Weibull (red.): <em>Nordiskt ljus. SOM-undersökningen 2009</em>. Göteborg: SOM-institutet
</li>
<li id="hoglund05">Höglund, Lars and Wahlström, Eva (2005). Varför minskar biblioteksbesöken?. In Sören Holmberg and Lennart Weibull (red.): <em>Lyckan kommer, lyckan går, SOM-undersökningen 2004</em>. Göteborg: SOM-institutet
</li>
<li id="jochumsen12">Jochumsen, Henrik, Hvenegaard Rasmussen, Casper and Skot-Hansen, Dorte (2012). ”The four spaces – a new model for the public library”, <em>New Library World</em>, <strong>113</strong>(11/12), 586-597
</li>
<li id="johannsen09">Johannsen, Carl Gustav (2009). ”Folkebibliotekernes brugerbilleder. Fire forestillinger om brugen”. <em>Dansk Biblioteksforskning</em>, <strong>5</strong>(1), 5-16
</li>
<li id="kann04">Kann-Christensen, Nanna and Pors, Niels Ole (2004). ”The legitimacy of public libraries: Cross-pressures and change processes”. <em>New Library World</em>, <strong>105</strong>(04/05), 330-336
</li>
<li id="svensk11">Svensk biblioteksförening (2011). Olika syn på saken: folkbiblioteket bland användare, ickeanvändare och personal. <em>Stockholm: Svensk biblioteksförening</em>
</li>
<li id="vestheim97">Vestheim, Geir (1997). Fornuft, kultur og velferd: ein historisk-sosiologisk studie av norsk folkebibliotekpolitikk. <em>Göteborg: Univ. Diss</em>
</li>
<li id="wolf01">Wolf, Eva (2001). Med charter till Estoril: en etnologisk studie av kulturell mångfald inom modern svensk turism. <em>Göteborg: Univ. Diss</em>
</li>
</ul>

</section>

</article>