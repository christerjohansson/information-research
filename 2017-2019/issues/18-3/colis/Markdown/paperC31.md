<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# Castles and inverted castles: the work of Marcia J. Bates

#### [Jenna Hartel](mailto:jenna.hartel@utoronto.ca)  
Faculty of Information, University of Toronto,  
140 St George St, Toronto, ON M5S 3G6, Canada

#### Abstract

> **Introduction**. Theorist Howard D. White has made a call to action for ‘scientist-poets' to synthesize concepts and oeuvres in the centrifugal literature of information science. In response, this paper interprets the work of canonical information scientist Marcia J. Bates through the original metaphor of 'castles and inverted castles'.  
> **Methods**. An intellectual biography and close reading is performed on a sample of eight major papers by Bates.  
> **Analysis**. Bates's career is sketched and the eight landmark papers are summarized; then, the papers are analysed in terms of the architectural metaphor and located at different conceptual levels in the discipline.  
> **Results**. Castles (elaborately built seats of power that bridge borderlands) express many of the unique qualities of Bates's work. Inverted castles (an invention of the author) reflect an additional utilitarian and pedagogical aspect of her thinking.  
> **Conclusion**. A conclusion offers critical remarks on Bates's contribution. The outcome is a vivid interpretation of the work of one notable information scientist.

<section>

## Introduction: scientist-poets wanted

White and McCain's '_Visualizing a discipline: an author co-citation analysis of information science, 1972-1999_' (1998) won the Best JASIS Paper of the Year Award for its thoroughgoing account of the intellectual history and organization of the field. Among other things, the study identifies canonical authors and eleven research specialties.[[1]](#footnote1) Plotting these points on a graph displays information science as a diffuse discipline without a significant central body of scholarship (Figure 1). Correspondingly, it lacks an all encompassing theoretical framework or textbook; and it has few authors who write about information science in general. White and McCain call attention to the fact that the conceptual landscape of information science is populated like Australia: 'heavily coastal in its development, with a sparsely settled interior' (p. 342), as in Figure 2.

<figure>

![Figure 1\. An INDSCAL map shows seventy-five canonical information science authors](../pC31fig1.png)

<figcaption>

Figure 1\. An INDSCAL map shows seventy-five canonical information science authors (five of eleven research specialties were added to the original map in [White and McCain 1998: 350](#white98)).</figcaption>

</figure>

<figure>

![Figure 2\. The heavily coastal organization of information science resembles the population distribution of Australia](../pC31fig2.png)

<figcaption>

Figure 2\. The heavily coastal organization of information science resembles the population distribution of Australia.</figcaption>

</figure>

A year later, White revisits his observation of a highly centrifugal discipline in an unusual short statement published in the 50th anniversary issue of JASIS. The piece is a spoof on a classified job advertisement and is entitled '_Scientist-poets wanted_' ([1999](#white99)). Ardently, he makes a call to action to populate the remote center of the discipline with unifying works. He encourages scholars to apply their effort to '_creative integration and criticism of ideas already embodied in the literature_' rather than staking new claims. He argues that the discipline would be bettered by books that attempt to organize whole segments of the field '_through some single powerful metaphor or thematic statement_'. Or, in the manner of intellectual biography he invites '_critical explications of noted individual authors…by someone who reads them in full_'. Acknowledging the diverse demands of such intellectual effort, he says, '_I suppose I am calling for persons who add the skills of a poet to whatever training we can give them as scholars or scientists – scientist-poets, if you will_' (p. 1052).

## Research method

Following White's directive, this paper is an intellectual biography and analysis of the oeuvre of information scientist Marcia J. Bates. It is based upon careful and reflective reading of a sample of her writings as well as any published commentaries on her work. The study also draws from my experience as Bates's doctoral student, advisee, and teaching assistant at the Department of Information Studies at the University of California, Los Angeles from 2001-2007, roles which entailed hundreds of hours of discussion with our subject about her ideas.

Thoroughly executed, the critical analysis proposed by White ([1999](#white99)) would entail reading an author's work in full—an ambitious undertaking not realized. Alternatively, from hundreds of presentations, chapters, articles, and reports by Bates, eight items will serve as a sample for the study. The criteria to generate the sample from Bates's formidable curriculum vitae was both rigorous and intuitive, tapping the dual sensibilities of a scientist-poet.

To establish validity, all selected items were solo-authored and published in peer-reviewed scholarly journals of library and information science. Major works by Bates were favoured; four ranked among her ten most-cited in Web of Knowledge. To reflect the span of Bates's career, representative works were selected at three to eight year intervals, between 1976 and 2010\. Alongside such legitimate criteria, the chosen writings simply struck me as quintessential Bates and numbered among the personal favourites of myself and my colleagues. The eight publications that constitute the sample are listed below in chronological order, with a shorthand title in italics (to be used for discussion), publication date, and complete title.

1.  _Rigorous systematic bibliography_ ([1976](#bates76)), 'Rigorous systematic bibliography'
2.  _Search tactics_ ([1979](#bates79)), 'Information search tactics'
3.  _What is a reference book?_ ([1986](#bates86)), 'What is a reference book? A theoretical and empirical analysis'
4.  _Berrypicking_ ([1989](#bates89)), 'The design of browsing and berrypicking techniques for the online search interface'
5.  The Getty online searching project ([1996](#bates96)), 'The Getty end-user online searching project, Report No. 6: Overview and conclusions'
6.  Invisible substrate ([1999](#bates99)), 'The invisible substrate of information science'
7.  Cascade ([2002](#bates02)), 'The cascade of interactions in the digital library interface'
8.  Introduction to ELIS ([2010a](#bates10a)), 'Introduction to the encyclopedia of library and information science"

In the role of scientist-poet, I will interpret Bates's work, as represented in the sample, through the metaphor of '_castles and inverted castles_'. According to Lakoff, metaphor is an indispensable and common way people conceptualize the world ([1993, p. 204](#lakoff93)). In metaphor, the characteristics of a familiar source domain are used to render understanding in a less comprehensible target domain ([Lakoff, 1987: 206-207](#lakoff87)). In this study, the source domain is the castle, a large ornate building and stronghold (more will be said about an inverted castle, later). The target domain is Bates's oeuvre, the sum of her life's work. While singular ideas of Bates's, such as berrypicking ([1989](#bates89)), have already been analysed and digested to become the accepted wisdom of the field, the nature of her collective accomplishment has not been addressed. Metaphor is an ideal means to reach and broadcast a deeper understanding of her work as a whole, as similarly applied by Savolainen ([2000](#savolainen00)) to the ideas of Brookes and Dervin.

Of note, White ([1999](#white99)) dreams of thematizing segments of the information science literature through more conventional metaphors, such as _information overload_ (p. 1052). Here, poetic license is invoked to employ a fresh architectural metaphor instead. In my opinion, the chosen metaphor is apropos. It reflects Bates's lifelong preoccupation with the '_underlying structure_' of information ([Bates, 1999: 1044](#bates99)); what is more, the novelty of castles and inverted castles may better capture the imagination of readers.

## Background on Marcia J. Bates

After Peace Corps service in Thailand in the mid 1960s, Bates enrolled in Berkeley's library programme because it offered a rare opportunity to women of her generation to pursue an intellectually stimulating career. She arrived at an exciting and formative moment for the school and the field. The faculty was aiming to institutionalize information science as a new interdiscipline drawn from the sciences, social sciences, and engineering. Bates's post-graduate education covered systems theory, game theory, cybernetics, linguistic information theory, and other multidisciplinary ideas that orient to the dynamic patterns underlying phenomena.

In this context, she studied information retrieval with attention to access vocabularies from both the system and user perspective. A mentor and lasting influence in the latter area was William Paisley of Stanford University, a pioneer in person-centred, context-sensitive approaches to the emerging specialty of user studies. Her dissertation, _Factors affecting subject catalog search success_ ([Bates, 1972](#bates72)), melded these interests. In a memoir of student days ([Bates, 2004](#bates04)), she explains that her conception of information science did not spring from library scholarship or Documentation, but it came from the breakthrough social scientific ideas of the 1950s and 1960s, resulting in a primary focus on information and its unique variables ([Bates, 1987](#bates87)).

In a 30-year academic career, Bates has been an acclaimed theorist, empirical researcher, editor, writer, consultant, and academic administrator. She is a canonical author per White's and McCain's ([1998](#white98)) author co-citation analysis. She is one of only three scholars to have received the JASIS&T Best Paper Award twice, and she has three articles among the twenty most highly cited library and information science papers of all time. Bates has won the ASIS&T Research Award (1998) and Award of Merit (2005) and the ALISE Award for Professional Contribution to Library and Information Science (2005). In 2005, she was the first inductee to the Academy of Fellows of ASIS&T's Special Interest Group on Information Needs, Seeking, and Use (SIG-USE).

Bates's principal achievement can be located at the crossroads of information retrieval systems and user behavior. There, she has shed light on the vocabulary problem, interface design, search strategies and tactics, and the information seeking habits of different groups. After retiring from teaching in 2008, she published philosophical statements about the nature of information ([Bates, 2010b](#bates10b)) and as a magnum opus, she edited the 3rd edition of the _Encyclopedia of Library and Information Sciences_ (2010). In a nomination statement for the ASIS&T Award of Merit, peers testify that her work stands out as '_creative_' and '_innovative_' with great '_originality_' and '_imagination_' ([Furner, 2005](#furner05)).

Bates's inventiveness may be partially associated with a benign peculiarity of her mind: she is synesthetic. According to the _Encyclopedia of Psychology_ (1994), synesthesia (from the Greek '_senses coming together_') is a relatively rare condition in which sensory stimuli cause unusual additional experiences, often perceived as a mismatching of the senses. For instance, to a synesthete, the note F sharp may invoke the smell of smoke; or the sight of green can generate the taste of pepper. Any of the senses can be crossed, and the outcomes vary in expression and intensity per synesthete. The disorder causes a psychedelic impression of everyday life which can only be reproduced in non-synesthetes via hallucinogenic drugs ([Hubbard and Ramachandran, 2005](#hubbard05)).

Text is a locus for synesthesia and most synesthetes see black and white letters and numbers in colors that remain stable over a lifetime (called grapheme-color synesthesia); Bates's synesthesia is limited to this type. Remarkably, into young adulthood Bates was unconscious of her unusual perspective. She assumed everyone saw colored letters until an acquaintance in her college dormitory, also a synesthete, explained the condition.

As a synesthete, Bates is in the good company of some of the 21st century's creative geniuses. American composer George Gershwin's Rhapsody in Blue fuses classical, jazz, and pop traditions and reflects his synesthetic predilection to experience a sound (rhapsody) as a color (blue). Russian-American author and synesthete Vladimir Nabokov, a celebrated savant and wordsmith, asserted in his biography that the English letter a had 'the tint of weathered wood' ([Nabokov, 1947: 21](#nabokov47)). As a child Russian painter, Vassily Kandinsky articulated a particular smell and sound for each color ([Kandinsky, 1947](#kandinsky47)), and he later attempted to merge music and color in his paintings and writings ([Ione and Tyler, 2004](#ione04)).

Does the uncommon condition of synesthesia impact Bates's scholarly work? Although in experiments synesthetes test as more creative than control groups ([Dailey et al., 1997](#dailey97)), its correlation to artistic or scientific output is presently not understood. It bears consideration that Bates sees I-N-F-O-R-M-A-T-I-O-N S-C-I-E-N-C-E as a rainbow. For sure, she has an unusually kaleidoscopic perception of letters, numbers, and text – the building blocks of information. It is time to turn attention from Bates's life and mind, to her work. Next, the eight publications by Bates from the sample will be analysed and summarized, chronologically.

## Eight by Bates

### 1\. _Rigorous systematic bibliography_ ([1976](#bates76))

_Rigorous systematic bibliography_ focuses on bibliographies that serve as guides to subjects or literatures. To start, Bates acknowledges that bibliography has a macroscopic role in structuring the entire graphic universe and a microscopic function as a tool to help individuals search ([Egan and Shera, 1952](#egan52)). She describes five specifications that a bibliographer must make explicit in order to generate a bibliography of maximum utility on both levels. This notion was explored philosophically by her professor Patrick Wilson in _Two kinds of power_ ([1968](#wilson68)); here Bates reworks it for practicing librarians and fledgling bibliographers.

The five specifications are to be stated at the introduction of any bibliography, as follows. The scope explains what the bibliography is about. The domain reports the literature searched. The organization identifies the access points and ordering principle(s) of the references. The information fields notes the citation format. When a bibliography is selective (rather than systematic), the selection principles describe additional criteria used to determine inclusion. Bates demonstrates the specifications in her own ARIST literature review (a type of bibliography) on search techniques ([1981: 139-142](#bates81)).

### 2\. _Information search tactics_ ([1979](#bates79))

_Search tactics_ was written long before the mainstreaming of the Internet, when online searching was the exclusive province of information professionals and little studied from a user's perspective. Through tapping into the literature of human problem solving, the article identifies twenty-nine search tactics; ways for a seeker to further a search. The tactics are grouped to address four dimensions of any inquiry: formulating the search, monitoring the search, navigating the resource, and selecting or revising terms. Each tactic is expressed as a striking word, for example:

Parallel - to broaden a search by using synonymous or conceptually related terms.

Scaffold - to design an indirect route through a file or resource.

Bibble (a neologism) - to look for a bibliography already prepared, instead of starting from scratch.

Interestingly, Bates presents the search tactics as a facilitation model for searching. The model aligns potential inquiring behaviour with the fundamental properties of organized information. A spatial sensibility underlies her thinking, as the tactics help searchers to _move sideways_ or _follow trails_ or pursue _indirect routes_ through structured information (p. 208). Bates recounts that the referees' responses to this manuscript polarized between outright dismissal and bemused praise. The editor of JASIS sided favorably with the latter, as did many readers, for the article won the Best JASIS Paper of the Year Award for 1980.

### 3\. _What is a reference book? A theoretical and empirical analysis_ ([1986](#bates86))

_What is a reference book?_ launches with the critical observation that existing definitions for reference book are inadequate. Prevailing renditions list examples of reference books, explain their customary placement in the library, or state typical applications. Bates asserts that a descriptive definition of a reference book's intrinsic characteristics is needed. She believes that information professionals intuitively know that reference books are unique on account of their structure and organization.

Borrowing terminology from computer science, she names three essential structural sub-elements of reference books: record, an individual unit of information; field, a piece of information within a record; and file, a set of two or more records with an ordering principle. Relying upon these terms, her definition reads: '_Reference books are books substantially or entirely composed of files_' ([Bates, 1986: 41](#bates86)).

She explains further that the file structure of reference books enables a typical manner of use, look-ups and scanning, in contrast to the reading that marks the narrative exposition in non-reference materials, like novels. To validate her proposed definition, Bates performs a content analysis on a sample of 343 books within three libraries; the findings confirm the distinct file composition of the vast majority of books in reference areas.

### 4\. _The design of browsing and berrypicking techniques for the online search interface_ ([1989](#bates89))

At this time, the classical model of information retrieval posited a situation in which a document representation is matched to a query in a singular static event. Bates points out how developments in understanding and technology across this scenario require a new conceptualization. Berrypicking proposes a more organic and nuanced model of information seeking that differs in several fundamental ways from the classical paradigm.

<figure>

![The Berrypicking Model](../pC31fig3.png)

<figcaption>

Figure 3: The Berrypicking Model ([Bates, 1989: 410](#bates89)).</figcaption>

</figure>

The curvy spine of this model represents an information seeker's path of experience through the universe of knowledge. Their initial query [Q0, lower left ] evolves and is re-initiated in a new form [Q1, Q2, Q3…] as documents or information [X] are discovered and subjected to thought [T]. Like berries picked in the woods, information is gathered 'a bit at a time,' not in one grand set. Drawing on information seeking studies, Bates elaborates that a variety of seeking techniques are employed along the way, for instance: footnote chasing, citation searching, journal runs, area scanning, and author searching. Likewise, a plethora of information sources figure in the process.

The dynamic and labyrinthine berrypicking model carries significant implications for system design. From the interface, users would need access to large bodies of text in many formats, reached quickly and with potential to retreat. Modules should enable any of the six searching behaviors. With freedom to browse or search in many directions, the interface would benefit from an organizing physical metaphor at the interface; she suggests a library. Since 1989, many of these propositions have been realized and are now taken for granted in OPACs, search engines, or database portals.

### 5\. _The Getty end-user online searching project, Report No. 6: Overview and conclusions_ ([1996](#bates96))

In the mid-1990s, Bates participated in a major two-year study of end-user online searching by humanities scholars based at the Getty Information Institute (now the Getty Research Institute). Twenty seven visiting research fellows were trained in DIALOG® and given unlimited free access for one year. Complete logs of their online transactions were captured and the subjects were interviewed in-depth about their searching. Bates played a lead role in analysing the data and writing up the findings.

Results were published as six reports that describe online searching in the context of knowledge production in the humanities, sharpened through contrast to the sciences. The work is too broad and rich to sum up here. In general, it documented the nature of the relationship between humanities scholars, their literature, and online information systems. Since DIALOG® was developed for the sciences, its limitations for humanities applications were highlighted. The reports contain numerous recommendations to improve information systems design and information provision to this population.

### 6\. _Invisible substrate of information science_ ([1999](#bates99))

In 1999, The American Society for Information Science celebrated its 50th anniversary with a two part special publication of its journal. The collection was guest co-edited by Bates (with Howard D. White) and she also wrote the lead article in the issue. Her contribution aimed to pin down and communicate the defining characteristics of the discipline that are unarticulated and '_below the water line_', what she calls the field's '_invisible substrate_' (p. 1044).

To start, Bates locates information science in academic space as a metadiscipline that runs orthogonal (at a 90 degree angle) to all other fields. Our distinguishing function is the representation, storage, and retrieval of any discipline's subject matter as packaged in its recorded knowledge. To deliver on that task, we focus on the form and structure of information, not its content and meaning. Metaphorically speaking, Bates posits that information scientists are like actors in their desire and commitment to represent the critical identifying features of their subjects for others to utilize and enjoy.

The methodological substrate to information science, according to Bates, is necessarily diverse. We face problems and questions about humans, literatures, and machines and, therefore, draw from and integrate social science and engineering research approaches. Our value system blends the neutrality of the sciences with the service ethic of librarianship, rounded out with a great sense of humour. Weaving together a lifetime of insights, _Invisible substrate_ won the Best JASIS Paper of the Year Award for 1999\. (For a creative performance of this paper, see [Hartel, 2012](#hartel12).)

### 7\. _The cascade of interactions in the digital library interface_ ([2002](#bates02))

_Cascade_ is a model of the complex dependencies within a digital library. The diagram (Figure 4) represents a digital library pulled apart, like an accordion, to show four components: technical infrastructure, information or content and its structuring metadata, information retrieval system, and user searching and understanding. Bates's point is that the layers influence each other in a synergistic, neutral, or conflicting manner. Design flaws in any layer cascade through the system and impact the user experience at the interface.

A large portion of the paper details four case studies of digital libraries that failed to synchronize these components. In the most blatant example, a database producer strategically added hyphens to its subject terms while simultaneously the database vendor geared its retrieval mechanism to strip hyphens, this resulted in a conflicted and sub-optimal outcome. The cascade model helps design teams coordinate the various arms of a digital library project to deliver the best possible performance for the user.

<figure>

![The cascade model](../pC31fig4.png)

<figcaption>

Figure 4: The cascade model, showing sample layers of a digital library. Design decisions at each layer have a cascading effect on later layers ([Bates, 2002, p. 384](#bates02)).</figcaption>

</figure>

### 8\. 'Introduction to the Encyclopedia of Library and Information Sciences, Third Edition' ([2010](#bates10)>)

As a co-editor (with Mary Niles Maack) of the Encyclopedia of Library and Information Sciences, 3rd Edition, Bates expressed her view of the overall structure and organization of the library and information science domain. She builds upon the conception of 'metadiscipline' introduced in Invisible Substrate and names eleven information-related specialties: museum studies, bibliography, archives, library science, document and genre studies, social studies of information, records management, information science, knowledge management informatics, and information systems. These specialties altogether aim to 'collect, organize, store, preserve, retrieve, transfer, display and make available the cultural record in all its manifestations' ([Bates, 2010, p. xii](#bates10)); as shown in Figure 5, they cut across the traditional content disciplines of the arts and humanities, social and behavioral sciences, and natural sciences.

Compared to the traditional content disciplines, the information sciences are relative newcomers to academe, still in a formative stage. Since the library and information sciences are inextricably linked to the work of the entire academic spectrum, we reflect a range of ideographic and nomothetic approaches that account for our metatheoretical and methodological diversity and interdisciplinarity. While Bates's conception is centred on academe, it is a structure that she extends to include trades, avocations, and hobbies, as well.

The review of 8 papers in the sample is now complete. Next, Bates's work is cast in the metaphor of castles and inverted castles.

<figure>

![Bates's conception of the eleven specialties within the library and information sciences](pC31fig5.png)

<figcaption>

Figure 5: Bates's conception of the eleven specialties within the library and information sciences, shown in relation to traditional disciplines.</figcaption>

</figure>

## Castles

A castle is the fortified home of a monarch or ruler. Castles appeared as an architectural form in western Europe in the late AD 900s until the 1400s, and they played a central role in the political system of the time, feudalism. In feudalism, kings granted land and authority to nobles to manage and defend from the stronghold of the castle. The 2006 Online World Reference Center states that castles served as residences for the nobility and as governance and social centers, prison, armory, and barracks for knights and soldiers. The Oxford Companion to Military History ([2001](#Oxford01)) notes that castles were often located upon hilltops that command a view of the countryside and in strategic positions between unfriendly states.

<figure>

![The main components of the medieval castle.](../pC31fig6.png)

<figcaption>Figure 6: The main components of the medieval castle.</figcaption>

</figure>

The hallmarks of a castle (Figure 6) are an inviolable tower called the keep, which served as a final retreat in case of attack or siege. Open space called the bailey flanked the keep, and the entire site was encircled by a thick wall topped with a battlemented walkway where archers could take aim, fire, and then safely step aside to rearm. Most castles were further protected by water-filled moats breached only via a drawbridge. As centralized governments replaced the feudal system, and military technology advanced, castles became relics of the Middle Ages. However, their distinct appearance lingered in the fanciful castellar style, often applied to palaces or summer retreats, such as Schloss Neuschwanstein (Figure 7) of King Ludwig II of Bavaria.

<figure>

![Schloss Neuschwanstein](../pC31fig7.png)

<figcaption>Figure 7: Schloss Neuschwanstein, a spectacular castle built by King Ludwig II of Bavaria in 1868 shows the castellar style that has no military purposes. For many people this image is a quintessential castle.</figcaption>

</figure>

Castles feature differentiated components that work together as a system. It is possible to see them as military or political _machines_, moreso than buildings. Their mechanical quality becomes clearer in contrast to modern skyscrapers, sleek monoliths that hide any interior dynamics. Bates's ideas resemble castles in the explicit display of components and their activity. _Berrypicking_, for instance, identifies the actions and resources of information seeking and then chronicles how they unfold altogether. _Cascade_, as well, names the conceptual and material apparatus of a digital library and their interdependencies during use. The _Introduction to ELinformation science_ likewise displays information science in terms of the vertical and horizontal machinery of academe. Bates's clockwork sensibility may come through the cybernetic influences during her Berkeley education and from the favoritism shown in information science since the 1980s for mechanistic models.

Typically, castles were strategically located at geographical crossroads or on the borders of divided states. Similarly, Bates's ideas are centred on intersections. To illustrate, _What is a Reference Book?_ unites two research areas that are typically treated independently. The first part of this article describes the special structure of reference texts, then the latter half draws connections to habits of use. The same juncture of information system and person is explored in _Search Tactics_, which relates the properties of structured information to potential searching techniques. In a similar manner, Invisible Substrate identifies two diametric conceptual territories in information science. That which is "above the water line" is expressed in Borko's classic definition[[2]](#footnote2) of the field. That which is "below the waterline" -- her "invisible substrate" -- is the largely unarticulated culture of information science.

Castles played communicative roles in their era. News and ideas were disseminated via the public events within their walls. Also, information was broadcast from castle rooftops using communication "technologies" of flags, bells, smoke, and pigeons. If follows that the dissemination functions of castles brought diverse stakeholders together in understanding or debate. Like castles, Bates's writings enhance communication and strengthen relationships among diverse participants of information science. _Rigorous Systematic Bibliography_ reworks Wilson's ([1968](#wilson68)) theory of bibliographic control (geared to the philosophically-minded) into an accessible statement for practitioners, bridging the gap between thinkers and doers. Likewise, the _Getty Online Searching Project_ and _Cascade_ establish a common landscape and language where information system designers and their users together improve information access.

As political strongholds, castles were sites of accessions, protests, and the occasional coup d'etat. In a parallel manner, Bates's works can overthrow underperforming or outdated ideas. For instance, inadquate definitions for "reference book" were excoriated by Bates ([1986](#bates86)). Also, berrypicking critiques and unseats the longstanding model of information retrieval, calling it inadequate and limiting to the creativity of retrieval research. While scholars had already contested some parts of the traditional model, Bates more aggressively challenges the model as a whole and then offers a more nuanced and multidimensional alternative in the form of the berrypicking model. The success of this coup can be measured in the citation counts to Berrypicking (309 to date) and a proliferation of similarly organic information behavior models.

Within feudalism, castles were the nexus of a leader's identity and authority, established by controlling the historical narrative and the geographic boundaries. No doubt castles were home to historians and mapmakers under the ruler's thumb. Correspondingly, in two papers of the sample Bates makes impactful statements on the origination and present identity of information science itself, and also its boundraies and neighbors. Invisible Substrate chronicles the emergence of information science alongside a cluster of related fields, (namely, psycholinguistics, information theory, cybernetics, and game theory), and illuminates its present day affinity with education and journalism, disciplines that share an interest in the dissemination of knowledge. Later in that paper, Bates unequivocally declares that information science has a unique focus on the form and structure of information. From an even broader vantage point that includes all academic disciplines, Introduction to ELinformation science locates information science as cutting across and above the humanities, social sciences, and sciences.

Bates has been a life-long civil rights champion and feminist[[3]](#footnote3) who participated in historic protests at Berkeley in the late 1960s. Her tendency to occupy crossroads, overthrow tired ideas, and assert disciplinary identity in information science extends naturally from an activist personality.

## Inverted castles

An inverted castle is a product of my imagination, invented to capture a particular quality in some of Bates's writings. This fantasy edifice has the identical structure and intricacies of a castle. But instead of rising to the sky, an inverted castle is upside-down and descends into the ground as an elaborate cellar (Figure 8). A scientist-poet might conjure that inverted castles create depth and stability in a landscape, resembling the roots of a tree. Like many underground or behind-the-scenes environments, inverted castles store materials or contain kitchens and workshops where goods are produced. There, too, master crafters pass on skills to apprentices. The inverted castle is an appropriate trope for Bates's ideas that supply definitions, templates, and best practices for others to take up.

<figure>

![The author's original conception of an inverted castle](../pC31fig8.png)

<figcaption>Figure 8: The author's original conception of an "inverted castle", a metaphor for some of Bates's writings. This fantasy edifice shares all the features of a castle, except that it descends (upside down) into the ground.</figcaption>

</figure>

Definitions by Bates abound in these inverted castles. Her most elaborate and recent definitions, not included in the sample, are for "information" ([Bates, 2010b](#bates10b)) and "information behaviour" ([Bates, 2010c](#bates10c)). An example from this study is the definition from _What is a reference book?_ Bates thoughtful definition borrows terminology from computer science and supersedes purely administrative definitions. Today, her definition anchors popular textbooks on reference services, such as the classic by Bopp and Smith ([2001: 309](#bopp01)).

Templates also reside in the underground workshops of an inverted castle. Rigorous Systematic Bibliography establishes a template that information specialists can employ while making subject bibliographies. Following Bates's guidelines produces consistent, transparent and technically sophisticated bibliographies that can accumulate into greater overall control of the graphic universe.

Numerous papers by Bates in the sample contain instructions for best practices. She variously offers guidance to bibliographers, librarians, searchers, systems designers, and content developers among others. In fact, the main objective of Search Tactics is pedagogical. The 29 terms are meant to provides newcomers with a vocabulary to search better. The Getty Study also recommends best practices for the designers of information systems in the humanities. Bates's success to that end was pronounced when she won the American Society for Information Science's prestigious Research Award. The Getty Online Searching Project was singled out for special commendation as a landmark study that 'forced a re-examination of the way in which information systems are designed for their client groups' ([Furner, 2005: 16](#furner05)).

Whether producing definitions, frameworks, or best practices, it is easy (for this scientist-poet, anyways) to imagine Bates as the master of a late Renaissance information workshop, located far underground in an inverted castle. To apprentices on hand she says:

> Verily it is so: bibliographies best fulfilleth their function whence the principles of scope, domain, organization, information fields, and selection principles are by ye articulated. (Rigorous systematic bibliography)

> Yea, methinks an information system that reflects the needs and habits of a user community tis most splendid! (The Getty Online Searching Project)

> By my troth! Every layer of a digital library toucheth upon the experience of the searcher. What say you? Collaborate I say to thee. (Cascade)

While Bates's castles help information science to know itself in relation to other fields and to soar, inverted castles lend stability and consistency to do good work, especially in practice. As a teacher of information studies I can attest that What is a Reference Book? and Rigorous Systematic Bibliography pass fundamental ideas and practices of good information science to new generations.

## Thesis

The metaphor at the heart of this paper can be extended to a complete thesis statement: Bates has created castles and inverted castles across the field of information science that clarify, structuralize, and popularize key notions about information. This thesis is illustrated in Figure 9\. To visualize "the field" per the thesis, a spectrum by Glazier and Grover ([2002](#glazier02)) is used to show nine conceptual levels of scholarship, from the sweeping tenets of a paradigm, down to the narrowest specifications of a definition; the nine levels are listed along a y-axis. A timeline of Bates's career serves as an x-axis (covering her most active years of 1975-2010). The eight works in the sample can be plotted on this plane at their level of conceptual generality (per Glazier and Grover) and their publication date.

To add further texture to the thesis and illustration, each item in the sample is represented as a castle [ ] or inverted castle [ ] based upon various qualities of the paper's content, function, form, and style. An item from the sample that theorizes and expresses the identity of information science (in a commanding and soaring rhetorical style) is a castle. An item that shares many qualities with a castle but works harder to define, standardize, and instruct (in a teacherly style) is an inverted castle:

<figure>

![The sample of works by Bates is shown as castles or inverted castles](../pC31fig9.png)

<figcaption>Figure 9: The sample of works by Bates is shown as castles or inverted castles at different conceptual levels in the field of information science (y-axis); the works are located at their publication date upon a timeline of Bates's career (x-axis).</figcaption>

</figure>

Stepping back to view Figure 9 as Bates's career timeline, it appears that her earlier papers (within the sample) were inverted castles at relatively low scopes. Understandably, as a junior scholar in information science and a female minority to boot, she focused on new approaches to relatively limited concepts. These writings respectfully use existing literature as points of departure and then argued for more sophisticated alternatives. Throughout she speaks in the tones of a patient educator. In retrospect, this part of her scholarly career is the warm-up of an uncommonly talented player. An appropriate quip for this period of Bates's development is, "we must learn to walk before we can run'.

Halfway through her career, during the early 1990s, Bates's writings change in nature to castles pitched at higher scope. On Figure 9 this happens around Berrypicking. She begins to generate theories about information behavior and ultimately attempts to articulate the paradigm of information science. The tone in the later period is less instructive and more evangelical and authoritative. Her papers do not launch as responses to existing notions, but begin with her original conceptions and ideas. Naturally, Bates has gained confidence, experience, and knowledge over the decades; she is a keynoter and award-winner; Marcia J. Bates has arrived!

## A critique

According to White ([1999: 1052](#white99)) the work of a scientist-poet is "critical" in nature, that is, a judicious evaluation of both strengths and weaknesses. Three shortcomings to Bates's enterprise are noted next. Firstly, some of her ideas are now obsolete due to new information technologies or practices. Secondly, in ways her later writings are out-of-synch with contemporary theorizing. Thirdly, compared to contemporaries of equal stature, Bates's contributions are relatively autonomous and therefore may have fallen short of greater impact.

The most obvious example of technological obsolescence is the instruction model of Search Tactics. The paper assumes the reader is an information professional with a thorough understanding of an information retrieval system's file structure and Boolean command searching. In the past 30 years, there has been a sea change in information retrieval. Today, the typical searcher is not an information professional and uses web-based search engines such as Google that require little training or expertise. Following the architectural metaphor, Bates's ornate and mechanical constructions have been superseded by an entirely different type of user-friendly architecture.

The last decade in information science has also seen the emergence of metatheory, 'the often unarticulated premises upon which empirical research and theorization is based' ([Talja, Tuominen, and Savolainen, 2005](#talja05)). Embracing a social constructionist philosophy, this movement holds that all ideas in information science are rooted in metatheoretical perspectives and there are no universal tenets. Mainly writing prior to the metatheoretical renaissance most of Bates's ideas are universalist declarations. A metatheoretically sophisticated reader of today may find her definition of a reference book ([Bates, 1986](#bates86)) or characterization of information science itself (Bates, 1999) to be positivist and naïve. However, Bates ([2005](#bates05)) entered the conversation on metatheory with a chapter that outlines eleven metatheoreties in information science that is a go-to resource on the topic.

Finally, Bates's output in the form of castles and inverted castles is a style that may have fallen short of greater potentials. Her imposing, aristocratic, sometimes spectacular idea formations have generated few collaborators or extensions. This isolation can be seen in even greater relief when compared to her contemporaries Brenda Dervin and Carol Kuhlthau, whose Sense-Making ([1983](#dervin83)) and the Information Search Process ([1988](#kuhlthau88)) projects, respectively, have blossomed into research programmes and favored analytical tools of many followers. Bates may have had broader reach and impact, and a more enduring legacy, if her ideas welcomed habitation and renovation by fellow travelers.

## Conclusion

While a doctoral student in information science at the University of Berkeley, Bates adopted a multidisciplinary approach to the study of the form and structure of information. Through a prolific academic career she explored information access and use problems from many perspectives, perhaps animated and colored by synesthesia. This paper has reviewed a sample of eight works by Bates and as a scientist-poet has sought to crystallize her oeuvre in a vivid metaphor. Castles – elaborately built seats of power that bridge borderlands – express many of the unique qualities of her work. Inverted castles reflect an additional utilitarian and pedagogical aspect of her thinking. Altogether, the work of Marcia J. Bates has contributed clarity and structure to the fundamentals of information science, and generated some of the most striking ideas in the field.

[1] <small>They are: experimental retrieval, citation analysis, online retrieval, bibliometrics, general library systems, science communication, user theory, OPACs, imported ideas, indexing theory, citation theory, communication theory ([White and McCain, 1998, p. 335](#white98)).</small>

[2] <small>That is, "Information science is that discipline that investigates the properties and behavior of information, the forces governing the flow of information, and the means of processing information for optimum accessibility and usability. It is concerned with that body of knowledge relating to the origination, collection, organization, storage, retrieval, interpretation, transmission, transformation, and utilization of information'. ([Borko, 1968, p. 3](#borko68), as cited in [Bates, 1999](#bates99)).</small>

[3] <small>Bates's radical tendencies bubbled up in a controversial acceptance speech for the 2005 ASinformation science&T Award of Merit. The spoken statement chronicled her experience of gender discrimination and its prevalence across academe ([Bates, 2005](#bates05)). The speech was received by the assembled ASinformation science&T members with nervous silence and hesitant applause.</small>

</section>

<section>

## References

<ul>
<li id="bates72">Bates, M.J. (1972). Factors affecting subject catalog search success. <em>Doctoral Dissertation, University of California, Berkeley</em>
</li>
<li id="bates76">Bates, M.J. (1976). Rigorous systematic bibliography. <em>RQ</em>, <strong>16</strong>, 7-26
</li>
<li id="bates79">Bates, M.J. (1979). Information search tactics. <em>Journal of the American Society for Information Science</em> <strong>30</strong>, 205-214
</li>
<li id="bates81">Bates, M.J. (1981). Search techniques. <em>Annual Review of Information Science and Technology</em>, <strong>16</strong>, 139-169
</li>
<li id="bates86">Bates, M.J. (1986). What is a reference book?: A Theoretical and empirical analysis. <em>RQ</em>, <strong>26</strong>, 37-57
</li>
<li id="bates87">Bates, M.J. (1987). Information - the last variable. <em>Proceedings of the ASinformation science Annual Meeting</em>, <strong>24</strong>, 6-10
</li>
<li id="bates89">Bates, M.J. (1989). The design of browsing and berrypicking techniques for the online search interface. <em>Online Review</em>, <strong>13</strong>, 407-424
</li>
<li id="bates96">Bates, M.J. (1996). The Getty end-user online searching project in the humanities: Report no. 6: Overview and conclusions. <em>College &amp; Research Libraries</em>, <strong>57</strong>, 514-523
</li>
<li id="bates99">Bates, M.J. (1999). The Invisible substrate of information science. <em>Journal of the American Society for Information Science</em> <strong>50</strong>(12), 1043-1050
</li>
<li id="bates02">Bates, M.J. (2002). The Cascade of interactions in the digital library interface. <em>Information Processing &amp; Management</em>, <strong>38</strong>, 381-400
</li>
<li id="bates04">Bates, M.J. (2004). Information science at the University of California at Berkeley in the 1960s: a Memoir of student days. <em>Library Trends</em>, <strong>52</strong>(4), 683-702
</li>
<li id="bates05">Bates, M.J. (2005). Acceptance speech for the ASinformation scienceT Award of Merit. <em>Annual Meeting of the American Society for Information Science and Technology</em>, November 1, 2005, Charleston, South Carolina, USA. Available online at http://www.gseis.ucla.edu/faculty/bates/articles/Acceptance.html
</li>
<li>Bates, M.J. (2005). An introduction to theories, metatheories, and models. In K. E. Fisher, S. Erdelez, and L. McKechnie (Eds.), <em>Theories of information behaviour</em> (pp. 1-24). Medford, NJ: Information Today
</li>
<li id="bates06">Bates, M.J. (2006). Fundamental forms of information. <em>Journal of the American Society for Information Science and Technology</em>, <strong>57</strong>, 1033-1045
</li>
<li id="bates10a">Bates, M.J. (2010a). Introduction. In M. J. Bates and M. N. Maack (Eds.). <em>Encyclopedia of library and information science.</em> (3rd ed.). (pp. xiii-xx) New York, NY: Taylor and Frances
</li>
<li id="bates10b">Bates, M.J. (2010b). Information. In M. J. Bates and M. Niles-Maack (Eds.), <em>Encyclopedia of Library and Information Sciences</em> (3rd ed., pp. 2347-2360)
</li>
<li id="bates10c">Bates, M.J. (2010c). Information behavior. In M. J. Bates and M. Niles-Maack (Eds.), <em>Encyclopedia of Library and Information Sciences</em> (3rd ed., pp. 2381-2391)
</li>
<li id="bauin92">Bauin, S. &amp; Rothman, H. (1992). "Impact" of journals as proxies for citation counts. In P. Weingart, R. Sehringer, and M. Winterhager (Eds.), <em>Representations of science and technology</em> (pp. 225-239). Leiden: DSWO Press
</li>
<li id="borgman90">Borgman, C.L. (Ed.). (1990). Scholarly communication and bibliometrics. <em>London: Sage</em>
</li>
<li id="buckland94">Buckland, M. &amp; Gey, F. (1994). The relationship between recall and precision. <em>Journal of the American Society for Information Science</em>, <strong>45</strong>, 12-19
</li>
<li id="budd02">Budd, J. (2002). Jesse Shera, sociologist of knowledge? <em>The Library Quarterly</em>, <strong>72</strong>(4), 423-440
</li>
<li id="bulletin06">Bulletin of the American Society for Information Science and Technology. Award of merit. February/March 2006
</li>
<li id="dailey97">Dailey, A., Martindale, C. &amp; Bunkum, J. (1997). Creativity, synaesthesia, and physiognomic perception. <em>Creativity Research Journal</em>, <strong>10</strong>(1), 1-8
</li>
<li id="dervin83">Dervin, B. (1983). An overview of sense-making research: Concepts, methods, and results to date. Paper presented at the <em>Annual Meeting of International Communication Association</em>, Dallas, TX
</li>
<li id="egan52">Egan, M.E. &amp; Shera, J.H. (1952). Foundations of a theory of bibliography. <em>Library Quarterly</em>, <strong>22</strong>, 125-137
</li>
<li id="furner02">Furner, J. (2002). Shera's social epistemology recast as psychological bibliology. <em>Social Epistemology</em>, <strong>16</strong>(1), 5-22
</li>
<li id="furner05">Furner, J. (2005). Nomination statement for Marcia J. Bates, ASinformation scienceT Award of Merit. Unpublished document
</li>
<li id="glazier02">Glazier, J.D. &amp; R. Grover. (2002). A Multidisciplinary framework for theory building. <em>Library Trends</em>, <strong>50</strong>(3), 317-329
</li>
<li id="hartel12">Hartel, J. (2012). Welcome to library and information science. <em>Journal of Education for Library and Information Science</em>, <strong>53</strong>(3), 165-175
</li>
<li id="hoppe90">Hoppe, K., Ammersbach, K., Lutes-Schaab, B. &amp; Zinssmeister, G. (1990). EXPRESS: An experimental interface for factual information retrieval. In J.-L. Vidick (Ed.), <em>Proceedings of the 13th International Conference on Research and Development in Information Retrieva</em>l (ACM SIGIR '91) (pp. 63-81). Brussels: ACM
</li>
<li id="hubbard05">Hubbard, E. M. &amp; Ramachandran, V.D. (2005). Neurocognitive mechanisms of synesthesia. <em>Neuron</em>, <strong>48</strong>, 509-520
</li>
<li id="ione03">Ione, A. &amp; Tyler, C. (2003). Neurohistory and the arts: Was Kandinsky a synesthete? <em>Journal of the History of the Neurosciences</em>, <strong>12</strong>(2), 223-226
</li>
<li id="ione04">Ione, A. &amp; Tyler, C. (2004). Synesthesia: Is F-Sharp colored violet? <em>Journal of the History of the Neurosciences</em>, <strong>13</strong>(1), 58-65
</li>
<li id="kandinsky47">Kandinsky, N. (1947). Some notes on the development of Kandinsky's painting. In: Kandinsky, V. (Ed). Concerning the spiritual in art and on painting pp. 9-11. <em>New York: Wittenborn Art Books</em>
</li>
<li id="kling94">Kling, R. &amp; Elliott, M. (1994). Digital library design for usability. Retrieved December 7, 2001 from http://www.csdl.tamu.edu/DL94/paper/kling.html
</li>
<li id="kuhlthau88">Kuhlthau. C.C. (1988). Developing a model of the library search process: cognitive and affective aspects. <em>RQ</em>, <strong>28</strong>(2), 232-42
</li>
<li id="lakoff87">Lakoff, G. (1987). Women, fire and dangerous things. What categories reveal about the mind. <em>Chicago: University of Chicago Press</em>
</li>
<li id="lakoff93">Lakoff, G. (1993). Contemporary theory of metaphor. In: Ortony, A. (Ed). Metaphor and thought, (2nd ed) pp202-251. <em>Cambridge: Cambridge University Press</em>
</li>
<li id="nabokov47">Nabokov, V. (1947). Speak Memory. <em>New York: Knopf</em>
</li>
<li id="savolainen00">Savolainen, R. (2000). Incorporating small parts and gap-bridging: Two metaphorical approaches to information use. <em>The New Review of Information Behavior Research</em>, <strong>1</strong>, 35-49
</li>
<li id="talja05">Talja, S., Tuominen, K. &amp; Savolainen, R. (2005). "information scienceMS" in information science: Constructivism, collectivism and constructionism. <em>Journal of Documentation</em>, <strong>61</strong>(1), 79-101
</li>
<li id="white99">White, H.D. (1999). Scientist-poets wanted. <em>Journal of the American Society for Information Science</em>, <strong>50</strong>(12), 1052-1053
</li>
<li id="white98">White, H.D. &amp; McCain, K.W. (1998). Visualizing a discipline: An author co-citation analysis of information science, 1972-1995. <em>Journal of the American Society for Information Science</em>, <strong>49</strong>(4), 327-355
</li>
<li id="wilson68">Wilson, P. (1968). Two kinds of power: An essay on bibliographical control. <em>Berkeley, CA: University of California Press</em>
</li>
</ul>

</section>

</article>