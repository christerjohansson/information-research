<header>

#### vol. 18 no. 3, September, 2013

</header>

<article>

## Proceedings of the Eighth International Conference on Conceptions of Library and Information Science, Copenhagen, Denmark, 19-22 August, 2013

# An integrated model of information literacy, based upon domain learning

#### [Gary B. Thompson](#author) and Jonathan W. Lathey  
Siena College, Standish Library, 515 Loudon Rd, Loudonville , NY 12211, USA

#### Abstract

> **Introduction**. Grounded in Alexander’s model of domain learning, this study presents an integrated micro-model of information literacy. It is predicated upon the central importance of domain learning for the development of the requisite research skills by students.  
> **Method**. The authors reviewed previous models of information literacy and inquiry-based learning to create a new model. A systematic review of research studies from cognitive development, learning theory and information science literature was conducted to demonstrate how domain learning provides the requisite knowledge and interest for students to apply the concepts and skills of information literacy.  
> **Results**. Novices and experts exhibit marked differences in their information gathering and online searching capabilities and strategies based on their levels of domain knowledge and interest.  
> **Conclusions**. The integrated information literacy model and research findings show how librarians and college faculty can collaborate in new partnerships to strengthen disciplinary information literacy in undergraduate curricula. Specific recommendations are given for how teaching techniques can be improved to enhance the development of information literacy skills for online searching and information evaluation and to foster greater domain learning.

<section>

## Introduction

While library orientations and one-shot lectures may reduce library anxiety and acquaint students with resources, they suggest that library research requires minimal instruction and minimal learning. The librarian’s task is to educate students and faculty that information literacy is not something taught in an orientation or one or two classes, but requires developmental learning with practice over a period of years to master the methods of inquiry and investigation required for the highest forms of scholarly research.

Another issue within the information literacy community regards whether library research is part of general education with skills readily transferrable to any discipline or whether library research methods must correspond with the discourse of specific disciplines. Those who think that information literacy fits best into general education focus on first year programs and introductions to library research methods. This means collaboration with introductory English instructors to teach students how to do research about an author, a literary work or other aspects of literature. An alternative is to offer a credit course which introduces students to information-gathering and gives students practice researching topics and creating bibliographies. No one would deny the value of such beginning information literacy instruction. However, this paper presents theoretical and experimental studies from cognitive learning and information science supporting the claim that domain learning and information literacy are intrinsic partners in student inquiry-based learning and investigative research. Students cannot adequately research a subject without an understanding of the related domain or discipline to assist them in information-gathering, information evaluation and synthesis. The more topical knowledge they possess, the greater their ability to do scholarly research on a subject and vice-versa; thus, the need for collaboration in teaching disciplinary subjects and information literacy.

## Models of domain learning, inquiry-based learning, and information literacy development

One of the premises of this paper is that interest in and knowledge of a domain (a specific body of knowledge) is instrumental to higher-level inquiry-based learning and problem-solving. Tobias ([1994: 38](#tobias94)) evinces how domain knowledge and student interest positively affect learning, especially when the interest is enduring. Thompson and Zamboanga ([2004](#thompson04)) demonstrate that prior knowledge facilitates new learning, even if the knowledge is modest and the sources are inferior, and that growth in knowledge leads to greater critical thinking skills. Patricia Alexander’s research shows that as subject matter knowledge increases, interest in content and recall of information increases, resulting in enhanced learning ([Alexander and Murphy 1997](#alexander97): 134-35). Christine Bruce states: '_Learning is about relations between the learner and the subject matter; the focus is not on the student or the teacher or the information, but the relation between these elements_'. ([Bruce _et al._ 2006](#bruce06): 6)

A related premise is that information literacy is a complex form of inquiry-based learning requiring critical thinking, problem-solving and domain knowledge. Two Cambridge University librarians, Seeker and Coonan, put forth a developmental vision of information literacy '_as a continuum that encompasses a broad range of abilities, from functional skills through high-level cognitive processes, culminating in the individual’s capacity to manage his or her own learning_'. ([Coonan 2011](#coonan11): 3) Scholars from the Netherlands Educational Technology Expertise Centre conclude: '_Just making information sources available is not sufficient to foster learning or to solve information-based problems. To succeed, learners are rather required to apply numerous cognitive skills, from searching and evaluating, to integrating information_'. ([Brand-Gruwel and Stadtler 2011](#brand11): 178). An overarching learning objective for information literacy according to Montiel-Overall ([2007](#montiel07): 58) is to enable learners to build on their personal knowledge through '_opportunities to use information to make connections, solve problems, and create innovative ideas that will lead to further information and knowledge_'. Lupton and Bruce ([2010](#lupton10)) push librarians to go beyond generic information literacy skill-building to a more contextual perspective emphasizing personal transformation, disciplinary and professional applications, and the social impact of information and knowledge.

As information literacy became a more regular part of education, librarians investigated how students conduct library research and achieve the competencies to acquire knowledge. As a trailblazer in research into student information-gathering behaviour, Carol Kulthau recognized student anxiety towards library research and identified processes involved in selecting a topic, searching for information, and starting to write. Michael Eisenberg and R. E. Berkowitz’s '_Big6-skills model_' emphasizes information problem-solving through task identification, investigation, and information evaluation and synthesis. These authors and others identified in a review article by Erdelez _et al._ ([2011](#erdelez11)) have developed '_micro-models of information literacy and inquiry-based learning_', which examine cognitive processes involved in library research and assembling research papers. These models have common elements: (1) they analyze student library research processes; (2) their unit of analysis typically is the student research project; (3) they identify student difficulties with determining the information need and selecting the topic; (4) they include the critical steps of information-gathering, evaluation, and synthesis.

Contrasted with those micro-models are macro-models of information literacy, with a more longitudinal approach to student information literacy development, with these common elements: (1) they put information literacy into a broader educational and social context; (2) the unit of analysis is the learner over a longer period of time; (3) knowledge building, knowledge management and knowledge creation receive greater emphasis. Let us turn to two important contributors to what we call the '_macro-models of information literacy and inquiry-based learning_'.

Building upon studies of cognitive development, motivation, and information processing related to learning theory, Alexander created a learning model outlining stages in the acquisition of domain knowledge. She postulated that the level of domain knowledge and interest fundamentally determines the student’s success in learning strategies and the development of critical thinking for any discipline. Alexander studied how a person’s knowledge base and motivation to learn influences his ability to process information and to integrate domain knowledge. More particular for this paper, levels of disciplinary knowledge are related to students’ strategic abilities in information-gathering, information-processing and information-utilization.

Alexander’s model of domain learning provides a framework for disciplinary education by proposing three developmental stages of academic learning ([Alexander 2003](#alexander03)). The novice or acclimated learner possesses low knowledge and low interest in the academic discipline. Domain knowledge is fragmented and disjointed. The student has trouble distinguishing which information is important and she is easily distracted by '_more tangential, albeit more enticing, information_' ([Alexander 1997](#alexander97): 222). In the second stage, the competent learner achieves greater conceptual understanding, entailing a higher level of thinking and more sustained interest in the subject-matter. With a more cohesive knowledge base centered on key concepts and principles, her knowledge base serves as a scaffold for subsequent domain learning ([Alexander 1997](#alexander97): 228). '_By using this conceptual framework, competent learners should be better able to differentiate domain-relevant and domain-irrelevant content and more important from less important information_'. ([Alexander 1997](#alexander97): 228) The more informed student is able to resist distractions and finds interest in unembellished text. In the third stage, the proficient or expert learner develops the greater interest, conceptual knowledge, cognitive skills and advanced learning strategies necessary for the highest order of critical thinking. She has the abilities and motivation to formulate problems and conduct original research in her academic discipline. '_This means that proficient or expert learners must have a role in reshaping the very landscape of the domain… by perceiving aspects of the domain in novel and insightful ways, or creating or redefining the very base of knowledge or core principles that mark a domain as unique_'. ([Alexander 1997](#alexander97): 237)

Bruce ([1997](#bruce97)) developed a parallel model of '_informed learning_' focusing upon the relationship between the learner and information use. From the model of domain learning perspective, Bruce’s first four conceptions facilitate students’ knowledge acquisition: (1) experience with the technology for information-gathering; (2) knowledge of the most important domain information sources; (3) knowledge of how to do effective searching; and (4) skills at organizing information.

In the final three conceptions in Bruce’s schema, students have competence and proficiency similar to those described in Alexander’s domain learning model, and the orientation shifts to greater creativity and utilization of information. Consistent with a disciplinary model of information literacy, students are equipped with '_both knowledge about the subject-specific content and research practices of particular disciplines, as well as the broader, process-based principles of research and information retrieval that apply generally across disciplines_'. ([Grafstein 2002: 197](#grafstein02)) The students become competent to search strategically for information, interpret information and construct meaning from a particular perspective. The persons are transformed by information, leading to insights for creating new ideas and utilizing information in a wide range of applications ([Bruce 1997](#bruce97)).

## An integrated model of domain learning and information literacy development

The authors of this paper propose a model of information literacy development emphasizing the central importance of domain knowledge and domain learning. Domain learning scholars have focused attention upon the effects of prior knowledge and student interest upon information inquiry, reading comprehension, knowledge acquisition, and knowledge organization and extension. The premise is that the learner is transformed on the road from low to high domain knowledge and interest with the ultimate goal of becoming competent and possibly an expert in a discipline.

Reference librarians encounter these stages of cognitive development as they work with students on research papers and searching. Novices (e.g. high school and first year college students) tend to exhibit low knowledge and low interest in the discipline as they are acclimating to academia in general and academic disciplines in particular. In conducting library research, they experience difficulty choosing topics and selecting terms to research the topics. They lack the knowledge bases to comprehend and critique research reports that they read or synthesize the readings in the field. Thus, they have difficulty integrating what they are reading into coherent thesis statements and sustained discussions.

Alexander’s model of domain learning states that as their domain knowledge and interest grow, learners are transformed by developing learning strategies and information-gathering strategies to enhance competence in their disciplines. Students use domain knowledge to select topics, to choose appropriate search terms, to evaluate content, and then to integrate the new knowledge with their existing bank of knowledge. Further in their academic careers, the expert learner evinces higher interest in their discipline and a broader and deeper knowledge base, enabling them to utilize a more sophisticated strategy for learning and knowledge acquisition. This leads to a new level of information literacy development, in which the student is eager to research new topics, is conversant with disciplinary concepts and methods and thus conducts comprehensive searches of the topics with maximum success. Furthermore, students take those assembled research findings, evaluate them from a deeper understanding of the topic, and not only expand their personal knowledge, but possibly extend the disciplinary knowledge on a specialized topic.

The left column in the chart below outlines characteristics of learners in the three stages of domain learning in Alexander’s model. High school and undergraduate students go through these stages of development at different rates of speed depending upon their education and experience. Novices begin with little or no knowledge of the discipline. As they progress through courses in their major, students begin to acquire disciplinary knowledge. They become competent and finally proficient with the subject matter and discourse of their chosen domain. The right column describes how the levels of domain knowledge and interest affect students’ approaches to and strategic knowledge of information literacy processes. Students start with little motivation and rather primitive information-gathering techniques. As knowledge and interest grow, students develop more targeted and tactical approaches to information gathering, leading to larger personal knowledge bases, which in turn enhance their abilities to investigate, evaluate, and integrate research findings in a synergetic relationship between domain learning and information literacy development.

<figure>

![Chart 1: Integrated model of domain learning and information literacy development](../pC02table1.png)

<figcaption>Chart 1: Integrated model of domain learning and information literacy development</figcaption>

</figure>

## Supporting literature for the link between domain learning and information literacy development

According to Alexander’s domain learning model there are two related kinds of knowledge: domain or topical knowledge and system or strategic knowledge. Information literacy is a type of strategic knowledge involving inquiry-based learning, problem-solving and critical thinking. The integrated domain learning-information literacy model states that the goal of information literacy is not only to teach students to be effective information-gatherers, but to teach students how successful information inquiries both depend upon their prior knowledge and extend their knowledge of the subject. Domain learning and information literacy development have a synergetic relationship emphasizing cognitive development and transformation. Other learning theorists and information scientists support this idea. Lazonder _et al._ ([2008](#lazonder08)) conclude that domain knowledge enables first year students to develop higher-levels of inquiry-based learning than those without subject knowledge. Waniek and Schafer ([2009](#waniek09)) studied the interaction between domain and system knowledge, and they concluded that system knowledge produced advanced navigation skills while domain knowledge improved information processing. Diekema _et al._ ([2011](#diekema11): 263) says that problem-based learning '_integrates information literacy with disciplinary content, enabling students to learn subject matter, information seeking, evaluation and synthesis skills and critical thinking skills all at the same time_'. Schrader _et al._ ([2008](#schrader08)) provide an overview of '_The model of domain learning as a Framework for Understanding Internet Navigation_'.

The interplay between domain learning and information literacy development has been studied most extensively in the arena of Internet and database searching. The studies reported come from many nations: Australia, Canada, Finland, France, Great Britain, Italy, Netherlands, Norway, Sweden and the United States. Our literature review below focuses specifically upon differences in ability and strategy between novices and experts in executing effective Internet searching depending upon levels of domain knowledge and levels of strategic knowledge:

*   Novices have difficulty getting started with framing their research topic ([Diekema _et al._ 2011: 262](#diekema11); [Limberg 2000](#limberg00): 200).

> One student… knew what topic she was to write about, but didn’t know how the field viewed the topic, which questions were considered interesting and valuable by practitioners in the field. ([Fister 1992](#fister92): 165)

> Another student said: I would have been dead in the water if I hadn’t been reading on the subject more or less…so I came with some ideas. ([Fister 1992](#fister92): 164)

*   Novices evince disorientation, information overload, and uncertainty in conducting information searches ([Desjarlais and Willoughby 2007: 6](#desjarlais07); [Desjarlais _et al._ 2008](#desjarlais08): 255).

> Novices tend to have a trial-and-error, wandering search strategy, thus taking more time to do the search ([Willoughby _et al._ 2009](#willoughby09): 646; [Tatatabai and Shore 2005](#tatatabai05): 226, 238).

> Impatience led them [novices] to navigate more, and to execute before spending enough time exploring or planning. ([Tatatabai and Shore 2005](#tatatabai05): 238)

*   Novices have trouble identifying effective search terms ([Desjarlais _et al._ 2008](#desjarlais08)).
*   Novices are easily distracted by technology features and irrelevant information ([Desjarlais _et al._ 2008](#desjarlais08): 258-260; [Cromley and Azevedo 2009](#cromley09): 290).
*   Novices must focus on examining the sources of information and understanding the text rather than evaluating informational content ([Braten _et al._ 2011](#braten11): 188).
*   Novices have difficulty discerning relevant and important findings from the rest ([Alexander _et al._ 1997](#alexander97): 224; [Braten _et al._ 2011](#braten11); [Desjarlais _et al._ 2008](#desjarlais08): 255).
*   Novices tend not to see connections between concepts and ideas and thus have trouble integrating the information discovered into a coherent discourse ([Desjarlais _et al._ 2008](#desjarlais08)).

**Experts manifest higher level information literacy development than novices**

*   Experts have high domain knowledge and specialized knowledge of topics which help them to choose domain-related search terms and get more useful results

> So, domain experts seem to be in advantage, because they can easily link prior knowledge to task requirements and to information found on the web. ([Brand-Gruwell and Stadtler 2011](#brand11): 176).

> Thus discovery depends upon a fit between an entrepreneur’s idiosyncratic prior knowledge and a particular venture idea, which may be discovered through systematic search. ([Norton and Hale 2011](#norton11): 827-828).

*   Experts manifest a targeted and flexible search strategy ([Waniek and Schafer 2009](#waniek09): 235; [Willoughby _et al._ 2009](#willoughby09); [Tatatabai and Shore 2005](#tatatabai05)).
*   Experts conduct searches in less time and with less navigational steps ([Willoughby _et al._ 2009](#willoughby09); [Cromley and Azevedo 2009](#cromley09): 300; [Park and Black 2007](#park07)).
*   Experts are self-regulating and adjust their search strategies based upon the information discovered and how it fits into their knowledge base ([Willoughby _et al._ 2009](#willoughby09): 646; [Stromso and Braten 2010](#stromso10)).

> Research is cyclical, with new questions and issues emerging throughout the research process as a result of reading and learning more about a topic. ([Diekema _et al._ 2011](#diekema11): 265)

> Student quote: Good research means reading information critically not only for answers, but for questions and new angles. ([Diekema _et al._ 2011](#diekema11): 265)

*   Experts focus upon a deep processing of the content of the sources, using critical reading comprehension and well defined evaluation criteria to put findings into the context of domain knowledge ([Braten _et al._ 2011](#braten11): 189; [Cromley _et al._ 2010](#cromley10);. [Rouet _et al._ 1997](#rouet97); [Tatatabai and Shore 2005](#tatatabai05)).

> Undergraduates engaged in fewer text-based strategies (e.g. rereading) and focused more on deep-processing strategies that helped them determine the main ideas of domain-related passages and build mental models of what they read. ([Alexander _et al._ 1997](#alexander97): 142)

> The more students become experts on the topic, the more they were able to evaluate the quality of Web information in terms of credibility of the source, as well as the accuracy and stability of its content. ([Mason _et al._ 2011](#mason11): 148)

*   Experts know the discourse of the domain and how to use the information found to extend the knowledge in the field through effective reporting.

> Norton identifies what an expert systematic information search strategy can do for innovation in business: “it identifies domain of competence; selects a set of information channels; crafts search strategies; develops decision criteria for signals; conducts active, constrained searches; and uses framework to evaluate the wealth-generating potential of ideas. ([Norton and Hale 2011](#norton11)).

> Differences between students’ understanding of subject content influenced how they searched for and used information. Differences in students’ experience of information seeking and use influenced both how they searched for, and used, information and what they learned about content. ([Limberg 2000](#limberg00): 199)

## Discussion and conclusion

The linking of domain learning to information literacy directly impacts our approach to instruction. First, teachers and librarians must adopt a longer-term developmental approach which incorporates growth in both domain learning and information literacy. According to Alexander _et al._ ([1997](#alexander97): 142): '_We did not produce (and could not produce) proficient or expert learners in the domain of educational psychology in a single semester. We did, however, contribute to their emerging competence_'. Secondly, teachers and librarians must emphasize active learning which incorporates information-seeking and information-gathering activities. Domain learning and strategic learning must go hand-in hand. Effective teaching of information searching involves '_the careful coordination of knowledge, flexible use of multiple search functionalities, monitoring of progress towards these goals, and efficient use of search time_'. ([Cromley and Azevedo 2009](#cromley09): 308) Thirdly, professors and librarians must recognize their complementary roles in imparting domain knowledge and information literacy. '_It is throughout the project, as students interact with information and its sources, consult with their professor, the library faculty member, and each other that they begin to develop competence_'. ([Ruediger 2007](#ruediger07): 86)

Information literacy instruction must take into account Alexander’s stages of development from acclimation through competence to expertise in a field of study. In the first stage the students strive to understand the nature of academic discourse and inquiry in general. Librarians must introduce students to the standards of academic research. Librarians also serve as mediators '_between the non-academic discourse of entering undergraduates and the specialized discourse of disciplinary faculty_'. ([Simmons 2005](#simmons05): 298). In the second stage, students undergo disciplinary socialization, during which they learn the core content, methods and discourse for their fields of study. '_Students gradually learn through reading and practicing the production of scholarly texts, the norms for what represents credible research output, what is considered good research, and what counts as a contribution to the field_'. ([Kautto and Talja 2007](#Kautto07): 55) Several studies show how students’ capabilities to search for relevant sources, read and evaluate content vary depending upon the match with their disciplinary majors ([Talja and Maula 2003](#talja03); [Park and Black 2007](#park07); [Rouet _et al._ 1997](#rouet97)). Librarians play a vital role in teaching students particular approaches to information-gathering, evaluation and reporting depending upon the discipline. This is especially important for topics crossing disciplines. Interdisciplinary research does not mean that it is '_non-disciplinary_'; rather it means that multiple disciplines may be doing research on this topic using differing perspectives ([Jones 2012](#jones12)). In the third stage, students assume more of the role of the topical expert whereas librarians assume the role of expert research advisors to ensure that students are highly proficient and agile at information-gathering and utilization to meet their specialized needs.

<figure>

![Chart 2: Implication of integrated model of domain learning/information literacy development for instruction](../pC02table2.png)

<figcaption>Chart 2: Implication of integrated model of domain learning/information literacy development for instruction</figcaption>

</figure>

Here are some specific recommendations for teaching information literacy based upon the integrated information literacy model and the supporting literature:

1.  Give novice students a reading related to the domain and the topic for the research paper just prior to library instruction and practice searching; the students will be more highly motivated and navigate with a greater grasp of the topic from the point of view of that domain ([Lawless _et al._ 2007](#lawless07)). Otherwise, give the students an introductory guide to the subject or let them bring notes on the topic to use during the search session to improve their chances of success ([Lazonder _et al._ 2010](#lazonder10); [Mitchell _et al._ 2005](#mitchell05)).
2.  Use concept maps, visual topical browsers and thesauri to help students with low domain knowledge to define and narrow their research topic and to decipher the relationships between search terms. This reduces disorientation and information overload and leads to improved search strategies because of an increased understanding of domain concepts ([Gordon 2002](#gordon02); [Li and Chen 2010](#li10)).
3.  Increases the confidence of students to conduct searches by offering interactive tutorials and video clips on how to do searching, preferably related to the specific database being used. ([Cromley and Azevedo 2009](#cromley09); [Mitchell _et al._ 2005](#mitchell05)).
4.  Give students more direction in terms of which databases fit the topic and show them through examples how to proceed step-by-step with the search to prevent them from being confused or overwhelmed ([Lawless _et al._ 2007](#lawless07)). Thinking aloud is a good survival technique to help them develop a search strategy and perform the necessary steps. ([Tatatabai and Shore 2005](#tatatabai05)) This is a developmental approach to strategic information literacy to scaffold with expansion of the students’ domain or topical knowledge.
5.  Give novice students more time to complete their searching. ([Desjarlais and Willougby 2007](#desjarlais07)) If you cannot, pair a novice student with a more advanced student with higher domain knowledge. '_There is a marked difference between the performance of acclimated learners alone and with the assistance of more competent or proficient individuals_". ([Alexander 1997](#alexander97): 226)
6.  Librarians and faculty can collaborate to teach students techniques to get the most out of scholarly articles through deep reading and analysis of the content ([MacMillan and MacKenzie 2012](#macmillan12)).
7.  Use more team teaching stressing the necessity for both domain learning and strategic information literacy. Ruediger ([2007](#ruediger07)) gives an extended example for advertising students.

Research questions which need further exploration:

*   How does this learning model work in other cultures and other educational contexts?
*   How do we operationalize for students the intrinsic relationship of domain learning and information-gathering?
*   How do students progress from novices to intermediate learners to experts in a field?
*   How do we assess how many of our students become systematic expert searchers?
*   Which types of visualizations would help students best with searching?
*   How do librarians and professors collaborate to teach students about the nature of disciplinary discourse? How do we embed disciplinary information literacy in disciplinary curricula?
*   Need studies about how to motivate students to do better searching and information-gathering.
*   Need more longitudinal ethnographic studies of student research habits.
*   Need studies of domain learning effects on searching using search engines versus databases.

## About the authors

**Gary B. Thompson** (MLS, MA) is Director of Library and Audiovisual Services at Siena College in Loudonville, New York. He has over forty years of academic library experience including instructing students about information literacy at the college and university levels. His mentors were Evan Farber and Hannelore Rader, two pioneers in library instruction. He can be contacted at [thompson@siena.edu](mailto:thompson@siena.edu)  
**Jonathan W. Lathey** (PhD) is a retired school psychologist and a retired adjunct reference librarian. He has wide interests including cognitive development, public history, writing, and information literacy.

</section>

<section>

## References

<ul>
<li id="alexander91">Alexander, P.A., Schalbert, D.L. &amp;Hare, V.C. (1991). Coming to terms: how
    researchers in learning and literacy talk about knowledge. <em>Review of Educational Research</em>,
    <strong>61</strong>, 315-343
</li>
<li id="alexander97">Alexander, P.A. (1997). Mapping the multidimensional nature of domain learning: the
    interplay of cognitive, motivational, and strategic forces. <em>Advances in Motivation and
        Achievement</em>, <strong>10</strong>, 213-250
</li>
<li>Alexander, P.A., Murphy, P.K., Woods, B.A., Duhon, K.E. &amp;Parker, D. (1997). College instruction
    and concomitant changes in students’ knowledge, interest and strategy use: a study of domain
    learning. <em>Contemporary Educational Psychology</em> <strong>22</strong>, 125-146
</li>
<li id="alexander03">Alexander, P.A. (2003). The development of expertise: The journey from acclimation
    to proficiency. <em>Educational Researcher</em>, <strong>32</strong>(8), 10-14.
</li>
<li id="association00">Association of College and Research Libraries (2000). Information literacy in the
    disciplines. Retrieved 25 July, 2013 from
    http://wikis.ala.org/acrl/index.php/information_literacy_in_the_disciplines (Archived by WebCite® at
    http://www.webcitation.org/6INRMxmkt)
</li>
<li id="brand11">Brand-Gruwel, S. &amp;Stadtler, M. (2011). Soliciting information-based problems:
    evaluating sources and information. <em>Learning and Instruction</em>, 21, 175-179
</li>
<li id="braten11">Braten, I., Stromso, H.I. &amp; Salmeron, L. (2011). Trust and mistrust when students
    read multiple information sources about climate change. <em>Learning and Instruction</em>,
    <strong>21</strong>, 180-192
</li>
<li id="bruce97">Bruce, C. (1997). The Seven faces of information literacy. <em>Adelaide: Auslib
        Press</em>
</li>
<li id="bruce06">Bruce, C., Edwards, S. &amp; Lupton, M. (2006). Six frames for information literacy: a
    conceptual framework for interpreting the relationships between theory and practice. Retrieved 25
    July, 2013 from www.ics.heacademy.ac.uk/italics/vol5-1/pdf/sixframes_final%20_1_.pdf (Archived by
    WebCite® at http://www.webcitation.org/6INSK8Y9u)
</li>
<li id="coonan11">Coonan, E. (2011). Developing a new curriculum for information literacy: Arcadia
    Project, July 2011. LTS News (University of Cambridge) 22, 2-3. Retrieved 25 July, 2013 from
    http://www.admin.cam.ac.uk/offices/education/lts/news/ltsn22.pdf (Archived by WebCite® at
    http://www.webcitation.org/6INSRmcq9)
</li>
<li id="cromley09">Cromley, J.G. &amp; Azevedo, R. (2009). Locating information within extended
    hypermedia. <em>Educational Technology and Research Development</em>, <strong>57</strong>, 287-313
</li>
<li id="cromley10">Cromley, J.G., Snyder-Hogan, L.E. &amp; Luciw-Dubas, U.A. (2010). Reading
    comprehension of scientific text: a domain-specific test of the direct and inferential mediation
    model of reading comprehension. <em>Journal of Educational Psychology</em>, <strong>102</strong>(3),
    687-700.
</li>
<li id="desjarlais07">Desjarlais, M. &amp; Willougby, T. (2007). Supporting learners with low domain
    knowledge when using the Internet. <em>Journal of Educational Computing Research</em>
    <strong>37</strong>(1), 1-17
</li>
<li id="desjarlais08">Desjarlais, M., Willougby, T. &amp; Wood, E. (2008). Domain knowledge and learning
    from the internet. In T. Willoughby &amp; E. Wood (Eds.), <em>Children’s learning in a digital
        world</em> (pp. 249-271). London: Wiley-Blackwell
</li>
<li id="diekema11">Diekema, A.R., Holliday, W. &amp; Leary, H. (2011). Re-framing information literacy:
    problem-based learning as informed learning. <em>Library &amp; Information Science Research</em>,
    <strong>33</strong>, 261-268
</li>
<li id="erdelez11">Erdelez, S., Basic, J. &amp; Levitov, D.D. (2011). Potential for inclusion of
    information encountering within information literacy models. <em>Information Research</em>,
    <strong>16</strong>(3).
</li>
<li id="fister92">Fister, B. (1992).The research process of undergraduate students. <em>Journal of
        Academic Librarianship</em> <strong>18</strong>(3): 163-69
</li>
<li id="gordon02">Gordon, C. A. (2002). Methods for measuring the influence of concept mapping on
    student information literacy. <em>School Library Media Research</em> 5.
</li>
<li id="grafstein02">Grafstein, A. (2002). A discipline-based approach to information literacy. <em>The
        Journal of Academic Librarianship</em>, <strong>28</strong>(4), 197-204
</li>
<li id="jones12">Jones, M. (2012) Teaching research across disciplines: interdisciplinarity and
    information literacy. In C. Gibson &amp; D. Mack (Eds.), <em>Interdisiplinarity and Academic
        Libraries</em> (pp. 167-181). University of California at Berkeley Library.
</li>
<li id="kautto07">Kautto, V. &amp; Talja, S. (2007). Disciplinary socialization: learning to evaluate
    the quality of scholarly literature. <em>Advances in Library Administration and Organization</em>,
    <strong>25</strong>, 33-59
</li>
<li id="lawless07">Lawless, K. A., Schrader, P.G. &amp; Mayall, H. J. (2007). Acquisition of information
    online: knowledge, navigation and learning outcomes. <em>Journal of Literacy Research</em>,
    <strong>39</strong>(3), 289-306
</li>
<li id="lazonder08">Lazonder, A.W., Wilhelm, P. &amp; Hagemans, M.G. (2008). The influence of domain
    knowledge on strategy use during simulation-based inquiry learning. <em>Learning and
        Instruction</em>, <strong>18</strong>, 580-592
</li>
<li id="lazonder10">Lazonder, A.W., Hagemans, M.G. &amp; de Jong, T. (2010). Offering and discovering
    domain information in simulation-based inquiry learning. <em>Learning and Instruction</em>,
    <strong>20</strong>, 511-520
</li>
<li id="li10">Li, L.Y. &amp; Chen, G.-D. (2010). A Web browser interface to manage the searching and
    organizing of information on the Web by learners. <em>Educational Technology and Society</em>,
    <strong>13</strong>(4), 86-97
</li>
<li id="limberg00">Limberg, L.(2000). Is there a relationship between information seeking and learning
    outcomes? In <em>Information literacy around the world: advances in programs and research</em> (pp.
    193-218). Wagga Wagga, N.S.W.:Centre for Information Studies, Charles Stuart University
</li>
<li id="lupton10">Lupton, M. &amp; Bruce, C. (2010). Windows on information literacy worlds: generic,
    situated and transformative perspectives. In <em>Practicing Information Literacy</em> (pp. 3-27).
    Wagga Wagga: N.S.W: Centre for Information Studies., Charles Stuart University
</li>
<li id="macmillan12">MacMillan, M. &amp; Mackenzie, A. (2012). Strategies for integrating information
    literacy and academic literacy. <em>Library Management</em>, <strong>33</strong>(8/9), 525-535
</li>
<li id="mason11">Mason, L., Ariasi, N. &amp; Boldrin, A. (2011). Epistemic beliefs in action:
    spontaneous reflections about knowledge and knowing during online information searching and their
    influence on learning. <em>Learning and Instruction</em>, <strong>21</strong>, 137-151
</li>
<li id="mason10">Mason, L., Boldrin, A. &amp; Ariasi, N. (2010). Epistemic metacognition in context:
    evaluating and learning online information. <em>Metacognition Learning</em>, <strong>5</strong>,
    67-80
</li>
<li id="mitchell05">Mitchell, T.J.F., Chen, S.Y. &amp; Macredie, R.D. (2005). Hypermedia learning and
    prior knowledge: domain expertise vs. system experience. <em>Journal of Computer Assisted
        Learning</em>, <strong>21</strong>, 53-64
</li>
<li id="montiel07">Montiel-Overall, P. (2007). Information literacy: towards a cultural model.
    <em>Canadian Journal of Information and Library Science</em>, <strong>31</strong>(1), 43-68.
</li>
<li id="norton11">Norton, W.I. &amp; Hale, D.H. (2011). Protocols for teaching students: how to search,
    to discover, and evaluate innovations. <em>Journal of Management Education</em>,
    <strong>35</strong>(6), 808-835
</li>
<li id="park07">Park, Y. &amp; Black, J.B. (2007). Identifying the impact of domain knowledge and
    cognitive style on web-based information search behavior. <em>Journal of Educational Computing
        Research</em>, <strong>36</strong>(1), 15-37
</li>
<li id="rouet97">Rouet, J., Favart, M., Britt, M.A. &amp; Perfetti, C.A. (1997). Studying and using
    multiple documents in history: effects of discipline expertise. <em>Cognition and Instruction</em>,
    <strong>15</strong>(1), 85-106
</li>
<li id="ruediger07">Ruediger, C. (2007). When it all comes together: integrating information literacy
    and discipline-based accreditation standards. <em>College &amp; Undergraduate Libraries</em>,
    <strong>14</strong> (1), 79-87
</li>
<li id="schrader08">Schrader, P.G., Lawless, K. &amp; Mayall, H. (2008). The model of domain learning as
    a framework for understanding Internet navigation. <em>Journal of Educational Multimedia and
        Hypermedia</em>, <strong>7</strong>(2), 235-258
</li>
<li id="simmons05">Simmons, M. H. (2005). Librarians as disciplinary mediators: using genre theory to
    move toward critical information literacy. <em>Libraries and the Academy</em>,
    <strong>5</strong>(3), 297-311
</li>
<li id="stromso10">Stromso, H.I. &amp; Braten, I. (2010). The role of personal epistemology in the
    self-regulation of Internet-based learning. <em>Metacogniton Learning</em>, <strong>5</strong>,
    91-111
</li>
<li id="talja03">Talja, S. &amp; Maula, H. (2003). Reasons for the use and non-use of electronic
    journals and databases: a domain analytic study in four scholarly disciplines. <em>Journal of
        Documentation</em>, <strong>59</strong>(6), 673-691
</li>
<li id="tatatabai05">Tatatabai, D. &amp; Shore, B.M. (2005). How experts and novices search the Web.
    <em>Library &amp; Information Science Research</em>, 27, 222-248
</li>
<li id="thompson04">Thompson, R.A. &amp; Zamboanga, B.L. (2004). Academic aptitude and prior knowledge
    as predictors of student achievement in introduction to psychology. <em>Journal of Educational
        Psychology</em>, <strong>94</strong>(4), 778-784
</li>
<li id="tobias94">Tobias, S. (1994). Interest, prior knowledge and learning. <em>Review of Educational
        Research</em>, <strong>64</strong>(1), 7-54
</li>
<li id="waniek09">Waniek, J. &amp; Schafer, T. (2009). The role of domain and system knowledge on text
    comprehension and information search in hypermedia. <em>Journal of Educational Multimedia and
        Hypermedia</em>, <strong>18</strong>(2), 221-240
</li>
<li id="willoughby09">Willoughby, T., Anderson, S.A., Wood, E., Mueller, J. &amp; Ross, C. (2009). Fast
    searching for information on the internet to use in a learning context: the impact of domain
    knowledge. <em>Computers &amp; Education</em>, <strong>52</strong>(3), 640-648
</li>
</ul>

</section>

</article>