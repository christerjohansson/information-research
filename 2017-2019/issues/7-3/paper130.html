<!DOCTYPE html>
<html lang="en">

<head>
	<title>What is the title of a Web page? A study of Webography practice</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="keywords" content="bibliographies, files, html, internet, research, world wide web">
	<meta name="description"
		content="Few style guides recommend a specific source for citing the title of a Web page that is not a duplicate of a printed format. Sixteen Web bibliographies were analyzed for uses of two different recommended sources: (1) the tagged title; (2) the title as it would appear to be from viewing the beginning of the page in the browser (apparent title). In all sixteen, the proportion of tagged titles was much less than that of apparent titles, and only rarely did the bibliography title match the tagged title and not the apparent title. Convenience of copying may partly explain the preference for the apparent title. Contrary to expectation, correlation between proportion of valid links in a bibliography and proportion of accurately reproduced apparent titles was slightly negative.">
	<meta name="rating" content="Mature">
	<meta name="VW96.objecttype" content="Document">
	<meta name="ROBOTS" content="ALL">
	<meta name="DC.Title" content="What is the title of a Web page? A study of Webography practice">
	<meta name="DC.Creator" content="Timothy C. Craven">
	<meta name="DC.Subject" content="bibliographies, files, html, internet, research, world wide web">
	<meta name="DC.Description"
		content="Few style guides recommend a specific source for citing the title of a Web page that is not a duplicate of a printed format. Sixteen Web bibliographies were analyzed for uses of two different recommended sources: (1) the tagged title; (2) the title as it would appear to be from viewing the beginning of the page in the browser (apparent title). In all sixteen, the proportion of tagged titles was much less than that of apparent titles, and only rarely did the bibliography title match the tagged title and not the apparent title. Convenience of copying may partly explain the preference for the apparent title. Contrary to expectation, correlation between proportion of valid links in a bibliography and proportion of accurately reproduced apparent titles was slightly negative.">
	<meta name="DC.Publisher" content="Professor T.D. Wilson">
	<meta name="DC.Coverage.PlaceName" content="Global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h4 id="information-research-vol-7-no-3-april-2002">Information Research, Vol. 7 No. 3, April 2002,</h4>
	<h1 id="what-is-the-title-of-a-web-page-a-study-of-webography-practice">What is the title of a Web page? A study of
		Webography practice</h1>
	<h4 id="timothy-c-craven"><a href="mailto:craven@uwo.ca">Timothy C. Craven</a></h4>
	<p>Faculty of Information and Media Studies<br>
		University of Western Ontario<br>
		London, Ontario N6A 5B7<br>
		Canada</p>
	<h4 id="abstract"><strong>Abstract</strong></h4>
	<blockquote>
		<p>Few style guides recommend a specific source for citing the title of a Web page that is not a duplicate of a
			printed format. Sixteen Web bibliographies were analyzed for uses of two different recommended sources: (1)
			the tagged title; (2) the title as it would appear to be from viewing the beginning of the page in the
			browser (apparent title). In all sixteen, the proportion of tagged titles was much less than that of
			apparent titles, and only rarely did the bibliography title match the tagged title and not the apparent
			title. Convenience of copying may partly explain the preference for the apparent title. Contrary to
			expectation, correlation between proportion of valid links in a bibliography and proportion of accurately
			reproduced apparent titles was slightly negative.</p>
	</blockquote>
	<h2 id="introduction">Introduction</h2>
	<p>It is by now well known that the growth of the World Wide Web since its inception in 1993 has created the need
		for new forms of citation in bibliographies, lists of references, and similar texts. The problems are not simply
		ones of citation format, but also ones of citation content. That is, it is not simply a question of what data
		elements are to be included, in what order, and with what punctuation, font, and capitalization, but also of how
		the values of those data elements are to be derived.</p>
	<p>This paper is concerned with what may be considered a key data element in most bibliographic references: the
		title of the item cited. Specifically, it addresses in a preliminary way the question of what people making
		references to Web pages think the titles of those pages are.</p>
	<p>Although there are a number of style guides that cover citation of Web pages to some degree, few give any
		indication from where the title of a cited item is to be taken, unless the item exactly duplicates a printed
		publication, in which case the title of the printed form may be used.</p>
	<p>By contrast, Land (<a href="#lan01">2001</a>) is very specific about using the title marked with the TITLE tag,
		with the option of adding the contents of a heading (marked, for example, with H1) as a subtitle, if
		substantially different.</p>
	<p>Ivey's (<a href="#ive97">1997</a>) observation that &quot;a properly constructed HTML document must have a
		title&quot; likewise suggests that the TITLE element should be the preferred source. A similar observation is
		found in Fletcher and Greenhill (<a href="#fle97">1997</a>). Ivey (<a href="#ive97">1997</a>) also notes that
		&quot;it may be appropriate to include the title of the larger publication&quot; as well, if this is known.</p>
	<p>The opposing view, that preference should be given to the title as it appears in the main display, is found in
		Estivill and Urbano (<a href="#est97">1997</a>), who recommend that the TITLE element be used as the title only
		if a main display title is absent (they also allow the TITLE element to be included in a note if it differs from
		the main display title and if it is considered significant).</p>
	<p>In contrast to the usual vagueness about the source of the title for Web pages, two commonly used style guides
		provide much more specific advice regarding e-mail messages and postings to news and discussion groups. For this
		type of document, the <em>MLA Handbook</em> (Gibaldi, <a href="#gib99">1999</a>, p. 199) states that the title
		should be taken from the <em>subject</em> line; the APA manual (American Psychological Association, <a
			href="#apa01">2001</a>) says, differing slightly, that the subject line should be used in the citation
		<em>instead of</em> the title.</p>
	<p>A minority of authors of Web bibliographies may be familiar at least with the existence of the <em>Anglo-American
			Cataloguing Rules</em> (Joint Steering Committee for Revision of AACR, <a href="#jsc98">1998</a>), but the
		advice given there seems not particularly helpful for deciding about Web page titles. The general AACR rule for
		the information source for titles is the following: &quot;Take information recorded in this area from the chief
		source of information for the material to which the item being described belongs&quot; (p. 17). As applied to
		computer files, the chief source of information is the <em>title screen</em> (p. 222), defined (p. 624) as
		&quot;a display of data that includes the title proper and usually, though not necessarily, the statement of
		responsibility and the data relating to publication&quot;. Strictly, the definitions are circular: the title is
		defined as text found on the title screen, and the title screen is a display that contains the title. The
		reference to the optional presence of information on responsibility and publication, however, does suggest that
		preference should be given to the more complete display in deciding what is to be viewed as the title screen.
		Preference for the more complete information source is certainly stipulated in cases where nothing can be
		identified as a title screen:</p>
	<blockquote>
		<p>If there is no title screen, take the information from other formally presented internal evidence (e.g., main
			menus, program statements, first display of information, the header to the file including
			&quot;Subject:&quot; lines, information at the end of the file. In case of variation in fullness of
			information found in these sources, prefer the source with the most complete information. (p. 222)</p>
	</blockquote>
	<p>Advice given by Olson (<a href="#ols01">2001</a>) on the OCLC Web site as to what constitutes the title of a Web
		page for cataloging purposes is still rather vague. &quot;The title of an Internet resource is taken from the
		chief source of information.... The chief source of information for computer files available by remote access is
		the title screen or similar display from the terminal or a printout of that information. If there is no special
		display, information may be taken from the home page, web page, or file itself: 'readme file,' 'about' screen,
		TEI (Text Encoding Initiative) header, HTML tagging, documentation file, internal menus, labels, subject line,
		program statements, etc.&quot; Failing these sources, &quot;the cataloger may use a title from any published
		description of, or citation to, the file&quot; or the file name &quot;if there is no other title given&quot;,
		or, failing these, &quot;must supply a title.&quot; The idea of using the file name if no title is in evidence
		is also mentioned by Rudolph (<a href="#rud01">2001</a>).</p>
	<p>Other aspects of the content of Web pages have been studied by various researchers; for example, page layout of
		home pages (King, <a href="#kin98">1998</a>); characteristics of anchors (Haas &amp; Grams, <a
			href="#haa00">2000</a>); informetric measures (Almind &amp; Ingwersen, <a href="#alm97">1997</a>); links to
		e-journals and their articles (Harter and Ford, <a href="#har00">2000</a>); effects of meta-tags on
		retrievability (Turner and Brackbill, <a href="#tur98">1998</a>; Henshaw &amp; Valauskas, <a
			href="#hen01">2001</a>). Another article by the author (Craven, <a href="#cra01">2001</a>) has reviewed
		advice given in both printed and Web-based sources on the function, content, structure, and style of meta-tag
		descriptions.</p>
	<p>Related more specifically to the study of how others describe or identify Web pages is the work of Amitay (<a
			href="#ami01">2001</a>), who developed a tool called SnipIt to extract descriptive passages with URLs from
		Web pages and another tool called InCommonSense to select from among these the &quot;best&quot; descriptive
		passage for each URL.</p>
	<h2 id="methodology">Methodology</h2>
	<p>A sample of bibliographies was identified by searching on Web search engines (chiefly Google and Webcrawler, with
		one bibliography being found with Yahoo!) on the query &quot;bibliography web&quot; (or &quot;bibliography of
		web bibliographies&quot; or &quot;bibliography online web&quot;). To be included in this study, a bibliography
		had to satisfy the following criteria.</p>
	<ul>
		<li>It was in HTML format (not Word, RTF, etc.).</li>
		<li>Most items in the bibliography appeared to be in some standard bibliographic format, with each listed in a
			separate paragraph, list item, or the like. Bibliographic elements, such as title, author(s), and date were
			clearly distinguishable.</li>
		<li>When duplicates were eliminated, at least 30 items in the bibliography had valid links to HTML versions of
			the items to which they refer.</li>
		<li>It was not a bibliographic search engine interface, though it could be spread over several Web pages.</li>
	</ul>
	<p>For each bibliography chosen, the following data were recorded.</p>
	<ul>
		<li>URL.</li>
		<li>Bibliographic data (title, author, date, publisher, etc.) to the extent these could be determined.</li>
		<li>Total number of items.</li>
		<li>Number of items with links.</li>
		<li>Number of links attempted in order to reach 300 valid links (or all links in the bibliography, whichever was
			less).</li>
		<li>Number of valid links followed..</li>
	</ul>
	<p>For each valid link in the bibliography to an HTML page (up to the maximum of 300 per bibliography), the
		following were recorded.</p>
	<ol>
		<li>The title in the bibliography;</li>
		<li>The subtitle in the bibliography (if any).</li>
		<li>The title tagged with the TITLE tag in the item linked to.</li>
		<li>The title as it would appear to be from viewing the beginning of the page in the browser (it might be
			centered, in larger type, in a distinctive font or color, immediately before the author's name, or otherwise
			identifiable).</li>
		<li>The subtitle as it would appear to be from viewing the beginning of the page in the browser.</li>
		<li>Match categories (as described below) for the tagged title.</li>
		<li>Match categories for the apparent title.</li>
	</ol>
	<p>The following codes were used to categorize the degree of matching between a title in the page itself and the
		title as given in the bibliography:</p>
	<p><strong>e</strong> - an exact match of the bibliography title formed all or part of the title to be matched
		(ignoring capitalization, punctuation, and initial &quot;the&quot; and &quot;a&quot;);
		<strong>p</strong> - the title to be matched contained a rewording or abbreviated version the bibliography
		title;
		<strong>n</strong> - there was no match with the bibliography title in the title to be matched;
		<strong>a</strong> - the title to be matched adds an author name or initials;
		<strong>w</strong> - the title to be matched adds a Web site name;
		<strong>x</strong>- the title to be matched adds other information, such as the name of another larger
		containing unit (series, journal, etc.) or body (publisher, etc.)</p>
	<p>The codes <strong>e</strong>, <strong>p</strong>, and <strong>n</strong> were mutually exclusive; each of them
		could stand alone or be combined with any combination of <strong>a</strong>, <strong>w</strong>, and
		<strong>x</strong>.</p>
	<h2 id="results">Results</h2>
	<p>Details of the 16 Web bibliographies examined are given in the Appendix. The proportion of bibliography items
		with links ranged from about 12% (for bibliography 8) to 69% (for bibliography 13). In only one case
		(bibliography 3) were any of the links in the bibliography disregarded because the upper limit had been reach.
		The proportion of attempted links that proved valid at the time of examination ranged from 19% (for bibliography
		12) to 96% (for bibliography 1).</p>
	<p>To simplify the analysis of results for Figure 1, the match categories have been collapsed into the following:
	</p>
	<p><strong>exact</strong>: code <strong>e</strong> alone, with no codes for additional information;
		<strong>additions</strong>: code <strong>e</strong> with any combination of codes for additional information;
		<strong>partial</strong>: any coding involving code <strong>p</strong>;
		<strong>no match</strong>: any other codings.</p>
	<figure>
		<img src="p130fig1.gif" alt="Figure 1">
	</figure>
	<p>For each bibliography, the percentages are shown first for categories of match to the tagged title and then for
		categories of match to the apparent title.</p>
	<p>The proportion of exact matches for tagged titles varies from 13% (for bibliography 9) to 58% (for bibliography
		15); that for apparent titles varies from 51% (for bibliography 14) to 95% (for bibliographies 15 and 16). In
		all cases, the proportion for tagged titles is much less than that for apparent titles, with the smallest
		difference being 23% (for bibliography 14).</p>
	<p>The gap is narrowed somewhat if exact matches with additions are included (with a minimum of about 11%, for
		bibliography 7), but the apparent titles remain ahead in all cases.</p>
	<p>As can be seen from Figure 1, matches with additions are much more unusual for apparent titles than for tagged
		titles; this is no doubt true in large part to the judgment exercised by the research assistant in recording
		apparent titles.</p>
	<p>Only rarely did the bibliography title match the tagged title and not the apparent title: there were two clear
		examples in bibliography 1, three in bibliography 2 (in one of which the apparent subtitle did, however, match
		the bibliography title), three in bibliography 3 (in one of which the apparent title combined with the apparent
		subtitle did match), one in bibliography 4, one in bibliography 5, one in bibliography 6 (with a match to the
		apparent subtitle), one in bibliography 7 (with a match to the apparent subtitle), none in bibliography 8, one
		in bibliography 9 (with a match on the combination of apparent title and subtitle), four in bibliography 10
		(with one match to the apparent subtitle and one to the combined apparent title and subtitle), one in
		bibliography 11, none in bibliography 12, none in bibliography 13, three in bibliography 14, none in
		bibliography 15, and none in bibliography 16.</p>
	<h2 id="discussion">Discussion</h2>
	<p>One might expect that one factor affecting the accuracy with which bibliography entries reflect the titles on the
		original pages might be the currency of the information in the bibliography: recent changes to titles would not
		be reflected if the entries had not been checked recently for continued accuracy. Since currency would also be
		indicated by the proportion of valid links, one might expect some positive correlation between proportion of
		valid links and the proportion of accurate titles. This seems, however, not to be the case: the correlation
		between proportion of valid links and the proportion of apparent titles that were exact matches to the
		bibliography titles or matches with additional information was in fact slightly negative (-0.156225).</p>
	<p>The convenience factor should probably not be ignored as a possible explanation for at least some part of the
		preference for the apparent title. Popular Web browsers, including Netscape Navigator, Internet Explorer, and
		Opera, all make it fairly easy to copy text from the main display. To copy a tagged title, however, is more
		involved. Typically, one must call up a view of the source code, find the tagged title within that source, and
		only then copy the title. To the beginner, it may even not be clear (as in the case of some versions of
		Navigator) how text can be copied from the source. Addition of features to browsers to allow easier capture of
		obscured or hidden page elements, such as tagged titles and meta-tags, might have a substantial effect on future
		preferences.</p>
	<p>Given the continuation of present browser models, compilers of Web bibliographies are probably best advised to
		use apparent page titles rather than tagged titles, on the grounds both of their own convenience and of
		consistency with more common usage.</p>
	<p>This advice is, of course, based on a sample of only sixteen bibliographies. Although there is currently no
		reason to believe that substantially different results would be found with a larger sample, especially given the
		universality of the preference shown by all sixteen, further investigation might possibly be of value. For
		example, the study might be extended to the more common type of listing in which only very brief citations are
		given for most pages, typically a title with a link to the corresponding URL.</p>
	<p>Some possibility of bias existed in the present study, since the research assistant who extracted the apparent
		title and subtitle from a Web page had already seen the bibliographic entry. A more rigorous methodology might
		have involved two assistants, one collecting the bibliographic items and links and the other subsequently
		examining the pages referenced. Having another research assistant revisit the pages at a later date might in any
		case be an interesting followup.</p>
	<p>In the area of followup, future work might address the relative stability of tagged and apparent titles. If
		tagged titles turned out to be substantially more constant over time, that might be an argument in favour of
		employing them in citations, in spite of their other disadvantages in comparison with apparent titles.</p>
	<p>Given the vagueness of the standard cataloguing rules with regard to the chief source of information for the
		title of a computer files, a study of actual cataloguing practice might be of interest. Do cataloguers of Web
		pages in fact tend to take titles from the main display window? In the few cases where information in the main
		display window is less complete than in the tagged title, is the tagged title preferred, or is some other
		information source employed?</p>
	<h2 id="acknowledgments">Acknowledgments</h2>
	<p>Research reported in this article was supported in part by the University of Western Ontario Office of Research
		Services with funds provided by the Natural Sciences and Engineering Research Council of Canada.<br>
		The extensive assistance of research assistant Emmett Macfarlane in data gathering is also acknowledged.</p>
	<h2 id="references">References</h2>
	<ul>
		<li><a id="alm97"></a>Almind, T.C. &amp; Ingwersen, P. (1997) &quot;Informetric analyses on the World Wide Web:
			methodological approaches to 'Webmetrics'.&quot; <em>Journal of Documentation</em> <strong>53</strong> (4),
			404-426.</li>
		<li><a id="apa01"></a>American Psychological Association (2001) <em>Publication Manual of the American
				Psychological Association</em>. 5th ed. Washington: American Psychological Association.</li>
		<li><a id="ami01"></a>Amitay, E. (2001) <em>What lays in the layout</em>. <a
				href="http://www.ics.mq.edu.au/~einat/thesis/">http://www.ics.mq.edu.au/~einat/thesis/</a>.</li>
		<li><a id="cra01"></a>Craven, T.C. (2001) &quot;'DESCRIPTION' META tags in locally linked Web pages.&quot;
			<em>Aslib Proceedings</em> <strong>53</strong> (6), 203-216.</li>
		<li><a id="gib99"></a>Gibaldi, J. (1999) <em>MLA handbook for writers of research papers</em>. 5th ed. New York:
			Modern Language Association.</li>
		<li><a id="est97"></a>Estivill, A.; Urbano, B. (1997) &quot;Cómo citar recursos electrónicos.&quot; <a
				href="http://www.ub.es/biblio/citae-e.htm">http://www.ub.es/biblio/citae-e.htm</a>.</li>
		<li><a id="fle97"></a>Fletcher, G. &amp; Greenhill, A. (1997) <em>Academic referencing of Internet-based
				resources</em>. <a
				href="http://www.spaceless.com/WWWVL/refs.html">http://www.spaceless.com/WWWVL/refs.html</a>.</li>
		<li><a id="haa00"></a>Haas, S.W. &amp; Grams, E.S. (2000) &quot;Readers, authors, and page structure: a
			discussion of four questions arising from a content analysis of Web pages.&quot; <em>Journal of the American
				Society for Information Science</em> <strong>51</strong> (2), 181-192.</li>
		<li><a id="har00"></a>Harter, S.P. &amp; Ford, C.E. (2000) &quot;Web-based analyses of e-journal impact:
			approaches, problems, and issues. <em>Journal of the American Society for Information Science</em>
			<strong>51</strong> (13), 1159-1176.</li>
		<li><a id="hen01"></a>Henshaw, R. &amp; Valauskas, E.J. (2001) &quot;Metadata as a catalyst: experiments with
			metadata and search engines in the Internet journal, First Monday.&quot; <em>Libri</em> <strong>51</strong>
			(2), 86-101.</li>
		<li><a id="ive97"></a>Ivey, K.C. (1997). <em>Citing Internet sources</em>. EEI Press. <a
				href="http://www.eeicommunications.com/eye/utw/96aug.html">http://www.eeicommunications.com/eye/utw/96aug.html</a>.
		</li>
		<li><a id="jsc98"></a>Joint Steering Committee for Revision of AACR (1998) <em>Anglo-American cataloguing
				rules</em>. 2<sup>nd</sup> ed., 1998 revision. Ottawa; London; Chicago: Canadian Library Association;
			Library Association Publishing; American Library Association.</li>
		<li><a id="lan01"></a>Land, B. (2001) <em>Web Extension to American Psychological Association Style</em>. <a
				href="http://www.beadsland.com/NN/ARC/1996/beadsland/ROOT/weapas/html/index/">http://www.beadsland.com/NN/ARC/1996/beadsland/ROOT/weapas/html/index/</a>.
		</li>
		<li><a id="kin98"></a>King, D.L. (1998) &quot;Library home page design: a comparison of page layout for
			front-ends to ARL library Web sites.&quot; <em>College and Research Libraries</em> <strong>59</strong> (5),
			458-465.</li>
		<li><a id="ols01"></a>Olson, N.B. (2001) <em>Cataloging Internet resources</em>. OCLC Online Computer Library
			Center. <a
				href="http://www.oclc.org/oclc/man/9256cat/toc.htm">http://www.oclc.org/oclc/man/9256cat/toc.htm</a>.
		</li>
		<li><a id="rud01"></a>Rudolph, J. (2001) <em>Style manuals</em>. <a
				href="http://www.lib.memphis.edu/instr/style.htm">http://www.lib.memphis.edu/instr/style.htm</a>.</li>
		<li><a id="tur98"></a>Turner, T.P. &amp; Brackbill, L. (1998) &quot;Rising to the top: evaluating the use of the
			HTML meta tag to improve retrieval of World Wide Web documents through Internet search engines.&quot;
			<em>Library Resources and Technical Services</em> <strong>42</strong> (4), 258-271.</li>
	</ul>
	<h2 id="appendix-list-of-bibliographies">Appendix: List of Bibliographies</h2>
	<p><strong>Bibliography 1 (Google Directory search &quot;bibliography web&quot;)</strong><br>
		http://www.lib.vt.edu/research/libinst/evalbiblio.html<br>
		<a href="http://www.lib.vt.edu/research/libinst/evalbiblio.html">Bibliography on Evaluating Internet
			Resources</a>
		Virginia Tech University Libraries
		Last updated: Feb. 26/01<br>
		total items 118<br>
		items with links 58<br>
		links attempted 58<br>
		valid links followed 48</p>
	<p><strong>Bibliography 2 (Google Directory search &quot;bibliography web&quot;)</strong><br>
		http://www.archiveimpact.com/bibliography/index.html<br>
		<a href="http://www.archiveimpact.com/bibliography/index.html">Archive Impact Bibliography</a><br>
		total items 367<br>
		items with links 130<br>
		links attempted 130<br>
		valid links followed 71</p>
	<p><strong>Bibliography 3 (Google Directory search &quot;bibliography web&quot;)</strong><br>
		http://info.lib.uh.edu/sepb/toc.htm<br>
		Bailey, Charles W., Jr.<br>
		<a href="http://info.lib.uh.edu/sepb/toc.htm">Scholarly Electronic Publishing Bibliography</a>.<br>
		Houston: University of Houston Libraries 1996-2001.
		total items 1301<br>
		items with links 354<br>
		links attempted 329<br>
		valid links followed 308</p>
	<p><strong>Bibliography 4 (Yahoo! Search &quot;bibliography web&quot;)</strong><br>
		http://arts.ucsc.edu/gdead/agdl/powers.html<br>
		<a href="http://arts.ucsc.edu/gdead/agdl/powers.html">Richard Powers: A Bibliography</a><br>
		By David G Dodd<br>
		copyright 1997-2001<br>
		Last Revised July 30/2001<br>
		total items 291<br>
		items with links 61<br>
		links attempted 61<br>
		valid links followed 37</p>
	<p><strong>Bibliography 5 (Google web search &quot;bibliography of web bibliographies&quot;)</strong><br>
		http://www-sor.inria.fr/projects/relais/biblio/<br>
		<a href="http://www-sor.inria.fr/projects/relais/biblio/">Web caching bibliography</a><br>
		Guillaume, Pierre<br>
		total items 344<br>
		items with links 108<br>
		links attempted 108<br>
		valid links followed 75</p>
	<p><strong>Bibliography 6 (Google web search &quot;bibliography of web bibliographies&quot;)</strong><br>
		http://www.oasis-open.org/cover/biblio.html<br>
		<a href="http://www.oasis-open.org/cover/biblio.html">SGML/XML Bibliography</a><br>
		By: Robin Cover<br>
		Last modified: April 19, 2001<br>
		total items 2188<br>
		items with links 320<br>
		links attempted 320<br>
		valid links followed 221</p>
	<p><strong>Bibliography 7 (Webcrawler search &quot;bibliography web&quot;, following list from &quot;Contemporary
			Philosophy of Mind&quot;:</strong>
		http://www.u.arizona.edu/~chalmers/biblio.html)<br>
		http://www.cogs.susx.ac.uk/users/ezequiel/alife-page/alife.html<br>
		<a href="http://www.cogs.susx.ac.uk/users/ezequiel/alife-page/alife.html">A Life Bibliography</a><br>
		Compiled by Ezequiel A. Di Paolo (c) 2000<br>
		total items 684<br>
		items with links 226<br>
		links attempted 226<br>
		valid links followed 94</p>
	<p><strong>Bibliography 8 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://library.usask.ca/~dworacze/BIBLIO.HTM<br>
		<a href="http://library.usask.ca/~dworacze/BIBLIO.HTM">Electronic Sources of Information: A Bibliography</a><br>
		Marian Dworaczek<br>
		Last Revised: June 15/2001<br>
		total items 1362<br>
		items with links 157<br>
		links attempted 157<br>
		valid links followed 121</p>
	<p><strong>Bibliography 9 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://library.usask.ca/~dworacze/CENS.HTM<br>
		<a href="http://library.usask.ca/~dworacze/CENS.HTM">Censorship on the Internet: A Bibliography</a><br>
		Marian Dworaczek<br>
		Revised: Oct 25/00<br>
		total items 101<br>
		items with links 56<br>
		links attemptsed 56<br>
		valid links followed 40</p>
	<p><strong>Bibliography 10 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://www.gseis.ucla.edu/iclp/bib.html<br>
		<a href="http://www.gseis.ucla.edu/iclp/bib.html">Cyberspace Law Bibliography</a><br>
		by The UCLA Online Institute for Cyberspace Law and Policy<br>
		19 Aug. 2001<br>
		total items 509<br>
		items with links 212<br>
		links attempted 212<br>
		valid links followed 174</p>
	<p><strong>Bibliography 11 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://www.kcl.ac.uk/humanities/cch/bib/<br>
		<a href="http://www.kcl.ac.uk/humanities/cch/bib/">Bibliography of Humanities Computing</a><br>
		Willard McCarty<br>
		5/96<br>
		total items 417<br>
		items with links 90<br>
		links attempted 90<br>
		valid links followed 36</p>
	<p><strong>Bibliography 12 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://www.counterpane.com/biblio/all-by-author.html<br>
		<a href="http://www.counterpane.com/biblio/all-by-author.html">Crypto Bibliography</a><br>
		Copyright Counterpane Internet Security, Inc., 2001<br>
		total items 1498<br>
		items with links 292<br>
		links attempted 292<br>
		valid links followed 56</p>
	<p><strong>Bibliography 13 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://www.markus-enzenberger.de/compgo_biblio/compgo_biblio.html<br>
		<a href="http://www.markus-enzenberger.de/compgo_biblio/compgo_biblio.html">Computer Go Bibliography</a><br>
		Markus Enzenberger<br>
		July 21/01<br>
		total items 144<br>
		items with links 100<br>
		links attempted 100<br>
		valid links followed 30</p>
	<p><strong>Bibliography 14 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://www.ala.org/alcts/publications/netresources/bib_main.html<br>
		<a href="http://www.ala.org/alcts/publications/netresources/bib_main.html">Standardized Handling of Digital
			Resources Bibliography</a><br>
		Preston, Ahronheim et al.<br>
		American Library Association, 2000<br>
		total items 170<br>
		items with links 79<br>
		links attempted 79<br>
		valid links followed 43</p>
	<p><strong>Bibliography 15 (Webcrawler search &quot;bibliography web&quot;)</strong><br>
		http://www.cs.wpi.edu/~webbib<br>
		<a href="http://www.cs.wpi.edu/~webbib">Webbib Online Bibliography - WWW as a distributed system</a><br>
		Note: items retrieved from:&quot;Core set of bibliography references&quot; section<br>
		Craig E. Wills<br>
		total items 805<br>
		items with links 274<br>
		links attempted 274<br>
		valid links followed 122</p>
	<p><strong>Bibliography 16 (Google Search: &quot;bibliography online web&quot;)</strong><br>
		http://www.enolagaia.com/Bib.html<br>
		<a href="http://www.enolagaia.com/Bib.html">BIBLIOGRAPHY - Autopoiesis and Enaction</a><br>
		Dr. Randall Whitaker<br>
		total items 525<br>
		items with links 70<br>
		links attempted 70<br>
		valid links followed 42</p>

</body>

</html>