#### vol. 21 no. 4, December, 2016

# Information literacy in the tension between school's discursive practice and students' self-directed learning

## [Cecilia Gordon](#author)

#### Abstract

> **Introduction.** Self-guided learning has had a major impact on adult education, where information seeking and use are key aspects of learning. With their lack of experience in study contexts, the students are nevertheless assumed to develop information literacy.  
> **Method.** The paper aims to create an understanding of how information literacy can be recognised in the tension between the schools' practice and the students' self-directed learning. This is done through a qualitative case study including forty-three interviews, thirty observations and seventeen documents, which gave in-depth knowledge of information activities in relation to a complex school assignment.  
> **Analysis.** Using a socio-cultural perspective, the analysis led to the identification of several critical aspects of information literacy.  
> **Results.** The aspects included the distinction between quantitative and qualitative information seeking, critical approaches towards information, knowledge of genres, the ability to identify and use various tools, and the ability to communicate conceptually about information seeking and use.  
> **Conclusions.** The findings reveal an absence of interaction about information seeking and use in the educational context, as well as a lack of common references in the form of tools and support, leading to difficulties for the students in achieving the results that were expected according to learning objectives.

## Introduction and problem statement

Self-directed learning in education is often emphasised as an ideal, as opposed to teacher-led instruction. Directing one's own learning is seen as an important ability in a knowledge and information society, with its constant demands for change and flexibility. This approach is reflected in official government reports, syllabi and grading criteria, as well as in practical school work. The approach has implications for various actors in education, such as students, teachers and librarians. Students at different educational levels are often expected to develop their ability to seek and select information, access, critically examine and understand different texts, their relations to other texts, and to produce their own texts in different contexts. Information seeking and use are key aspects of school work and learning, and students are assumed to develop information literacy during the course of their studies.

Self-guided learning has had a major impact on adult education in Sweden and has reshaped conditions for learning. Curricula for municipal adult education provide several examples in which abilities that can be described as information literacy are stressed. Many of those who study in municipal adult education at upper secondary level in Sweden return to formal, organized learning after several years' absence from educational contexts. Students participating in municipal secondary adult education have experiences of elementary school, working life, family, hobbies and everyday learning, in Sweden or elsewhere, but in terms of information seeking and use in an educational context they could be described as novices. The tension between life experience and lack of experience in study contexts represents a starting point for this study. The paper is based on a doctoral thesis.

Previous research concerning information literacy in educational contexts mostly concern children (e.g. [Lundh, 2011](#lun11); [Spink, Danby, Mallan and Butler, 2010](#spi10)), young people, and college or university students (e.g. [Francke, Sundin and Limberg, 2011](#fra11); [Gross and Latham, 2007](#gro07), [2009](#gro09); [Head and Eisenberg, 2009](#hea09), [2010](#hea10); [Julien and Barker, 2009](#jul09); [Limberg, 1999](#lim99); [Lupton, 2008](#lup08)). Research with adult novice learners seeking and using information in educational contexts, therefore emerges as an important area of investigation.

This paper aims to create an analytical understanding of information literacy in students' work on a complex school assignment. The aim is met by exploring how adult students, teachers and librarians interact in terms of information seeking, the tools and support used, and how information is used by adult students to construct knowledge and make sense, in the practice of working with an assignment.

## Information literacy in school

The research problem implies that it is imperative to explore information seeking and use, related to learning, to better understand what constitutes information literacy in a specific information practice. The paper uses a definition of information literacy as learning to communicate appropriately within a specific practice ([Limberg, Sundin Talja, 2012](#lim12)), using abilities connected to different ways of seeking, finding, choosing, evaluating and compiling information for creating new knowledge.

The understanding of information literacy as highly connected to specific contexts, and the perception that learning takes place through interaction, is connected to a socio-cultural perspective as described by [Soljo (2000](#sal00), [2005](#sal05)). Socio-culturally oriented researchers have shown that being information literate varies over different practices (e.g. [Lloyd, 2006](#llo06)). From a Vygotskian perspective of learning as social, situated and tool-based ([Vygotsky, 1978](#vyg78)), information literacy is understood as something that is shaped in interaction in a social practice. The view of linking information literacy to specific contexts can be associated with a trend in user studies to move towards more communicative or socially oriented theoretical approaches (cf. [Sundin and Johannisson, 2005](#sun05); [Tuominen, Talja and Savolainen, 2005](#tuo05)), especially in Scandinavia (cf. [Hedman and Lundh, 2009](#hed09), p. 268). In recent years, sociocultural perspectives have been employed in several studies of information seeking and information literacies (e.g. [Lloyd, 2012](#llo12); [Lundh, 2011](#lun11); [Rivano Eckerdal, 2012](#riv12); [Wang, Bruce and Hughes, 2011)](#wan11).

Information literacy in this paper is understood in relation to a practice that is normative by nature. Schools are institutions characterised by certain norms, explicit and implicit, that students have to relate to ([Limberg, 2007](#lim07)). Elementary school pupils, college students and students in municipal adult education all have in common that their actions, products and learning are assessed. Syllabi and grading criteria form the basis for assessment in the school's discursive practice. To be assessed is part of the practice of the school and what is understood as knowledge is in a way "given" in the sense that it is based on syllabi and the objectives of the assignment (cf. [Rasmussen, Krange and Ludvigsen, 2004](#ras04), p. 840). Also, the school context makes it impossible to stop seeking information as quickly as people often do in everyday life, since the assignments and curriculum make it obligatory ([Shenton and Hay-Gibson, 2011](#she11), p. 65). Many of the questions are imposed ([Gross 1995](#gro95), [2001](#gro01)), but due to self-directed learning approaches, assignments are also guided by the students' perceived information needs. Students working with complex research-based assignments are expected to work independently and are supposed to make choices of their own, while the results are graded according to specific criteria. It is therefore important to stress that there are special requirements for information seeking and use, and therefore for information literacy, in educational contexts. This means that when students learn to seek and use information in school, several dimensions are involved. They have to coordinate cognitive, material, and discursive actions in ways that make sense to them and are also sanctioned in the school context (see [Lankshear and Knobel, 2011](#lan11), p. 33-50).

## Empirical framework and material

The empirical study was pursued as a qualitative, ethnographically oriented, case study. The research method provided in-depth knowledge about situated school activities. Qualitative case studies are typically empirical, concentrated on everyday settings, descriptive, extensive and aiming at improving the readers' understanding of the research object ([Stake, 1994](#sta94); [Yin 2003](#yin03)).

The selection of a school class that would be suitable for the study included setting up criteria such as self-directed schoolwork, a complex research assignment, enough students for providing a rich empirical material, an accessible library and, of course, students, a librarian and a teacher willing to participate in the study. The selection can be described as qualitative, strategic or purposeful ([Bryman, 2008](#bry08); [Stake 1994](#sta94)). A class in a town in Sweden was approached via the school librarian and its members agreed to take part in the study.

The study was conducted at a municipal school for adult education. Municipal adult education gives adults the chance to acquire formal educational qualifications at the upper secondary level. In the selected school there were classrooms, computer labs, a library, group activity rooms and a cafeteria available to the students. The school was open from morning until late at night. The library was staffed by a librarian and an assistant. The students that participated in the study were studying to become certified nursing assistants on the Health Care Programme at upper secondary level. They all took the same social studies course and were all given the same assignment. The assignment stretched over nearly three months, although the students took other courses at the same time. The assignment required students to show an understanding of society from historical and contemporary perspectives, understand how different values afford different ways of understanding society, see consequences of different options for individuals and society and, finally, be able to use different sources and methods in their work. They had to select a topic, write a statement of purpose, formulate research questions, seek, select and use different sources and analyse their materials. Their work was to be presented in a report which included an introduction and background, research questions and methods, results, conclusions and discussion, and a list of references. Titles of student reports included _How to motivate a drug addict_, _Female immigrants and unemployment_, _Stress and children_, _Television in Sweden_ and _Rehabilitation and animals_.

A major part of the student group regularly used the school library, mainly as a meeting place and for using course literature. The course participants had little experience from similar assignments. They found it hard to pick a topic, to delineate an area, to formulate questions and to understand what was expected of them. They had little previous experience from independent information seeking concerning questions that did not have a given answer. During their work they had one session of library instruction and the librarian urged them to contact her. Tutorials with the teacher were mandatory during the assignment.

The case study included different kinds of data: interviews, observations and documents. In line with a socio-cultural perspective, the empirical focus of the study was on the mediated practices of the students. Fourteen students were interviewed on three separate occasions over a three-month period, since it was pertinent to follow their information seeking and use activities throughout the process of the assignment. The first interview took place after the course introduction, in the initial stage of their work. The second interview was conducted a few weeks later, when they had had time to search for material and started writing. The third interview was performed after they had presented their reports to the teacher and the rest of the class. The students were asked to bring notes, books, text extracts and other artefacts to the interviews so that conversations about information seeking and use were easier to relate to their assignments. The teacher and the librarian were interviewed separately on two different occasions, before the course introduction and after the students' presentations. In all, the case study included forty-three interviews, which lasted between thirty minutes and two hours. All interviews were recorded and transcribed. (Due to illness, not all students were interviewed three times.) Observations were conducted during all five class sessions, as well as of twelve tutorials between the teacher and the students and during thirteen presentations. During the observations, I was not part of the conversations. Using a document with two columns, I was active in taking field notes and also writing down reflections. The students' written reports were added to the data collection to gain further knowledge about information seeking and use.

Much of the time, I was present in the school to follow the interaction between students, the teacher and the librarian. I was given access to the school, to the lectures and to the tutorials by the principal, the teacher, the librarian and all the students in the study. They were all well informed of the purpose of my study and they generously shared their experiences with me. Due to my presence and questions about information seeking and use, it is likely that the participants paid more attention to this topic than they would have done otherwise. The study followed the research ethics principles in humanistic-social scientific research developed by The Swedish Council for Research in the Humanities and Social Sciences.When excerpts and citations from the data appear in the text, a translation has been made from Swedish into English. Students are referred to as fictive proper names, teacher or librarian and numbers (1-3) are used to mark which interview is referred to.

The socio-cultural perspective guided the analysis, by focusing on interaction between teacher, librarian and students. The analysis was conducted in several stages, including a descriptive level where the students' progress was described chronologically and the material was thematically coded in relation to the theoretical framework as well to previous research (cf. [Merriam 1988](#mer88)). Broad themes such as information seeking, information use, interaction, tools, and learning were generated. Through openness to, and careful, repeated readings of the empirical material, new facets and angles emerged. Following this a scheme was created with categories and sub categories, for example school library, sources, support, selection, credibility and text production, using Atlas.ti for administrating the large amount of material. Finally, the findings were interpreted using the theoretical framework and previous research.

## Findings: aspects of information literacy

In this section findings are presented and discussed. By exploring how adult students, teachers and librarians interact around information seeking and use, what tools are used, and how information is used by adult students to construct knowledge and make sense in the practice of working with an assignment, several aspects of information literacy were identified in the tension between the school's practice and the students' self-directed learning. In this context self-directed learning meant that students to a great extent were responsible for their own learning, within the frame of a specific assignment. They had to take initiatives, set goals, formulate problems, identify relevant resources, choose appropriate methods, work with analysis and present their results in written form. The findings show that self-directed learning entails a number of critical aspects of information literacy in connection to students' opportunities for learning.

### Quantitative or qualitative information seeking

Information seeking was generally perceived by the students as searching through Internet search engines. The study revealed that what may be called _quantitatively oriented information seeking_, to quickly search through large entities of information, preferably through a search engine, did not constitute a major problem for the students. Efficient search engines yield numerous pages of results, just through entering one or two keywords. Most of the students searched for information in this way at one or two occasions during first two weeks of the project period. The major part of the students found their material during the library instruction session in the computer lab, where the librarian demonstrated databases and presented different search techniques. The students did not repeat the search for information on further occasions, but were satisfied with the information they had found, and instead devoted much time considering how they could make use of found information. This information significantly influenced how the reports were finally designed. Much time was spent choosing extracts and examples from an extensive material and in different ways trying to get the information to be consistent with the chosen topic. Few examples were found of _qualitatively oriented information seeking_, i.e. searching for information in a more focused, critical way in order to obtain information of a certain quality, character or genre which is an important part of self-directed school work. This was related to the fact that few of the students were explicit concerning a conscious information strategy, and they did not analyse how they would go about seeking for information.

The study further shows that interaction during work on the assignment was rather limited, especially concerning information seeking and use. The fact that information seeking and use did not lead to conversations suggests that these activities were not visible parts of the task, even though the teacher and the librarian had emphasised seeking and use in different ways in the introduction to the assignment. Previous research has shown that in order for information seeking to be noticed in educational practices, the assignment needs to be designed in a particular way where the teacher explicitly regards information seeking as an integral part of the task ([Limberg and Folkesson, 2006](#lim06)). In this case, information seeking was rendered invisible, despite the design of the assignment, and despite the teacher's ambition to raise it as essential. The conversations that were observed treated information in a way that, from a library and information science perspective, could be described as superficial. In most cases, information seeking was only perceived as searching on the Internet. One reason for this was that information was not seen as something distinct in the work, but was such an integral part of the process that it disappeared. Both the students and the teacher had an everyday understanding, as opposed to a more theoretical or critical understanding, of what information might be in this context.

### Open or closed information seeking

One aspect of information literacy that was identified was the lack of understanding of the complexity of information seeking and use concerning the interplay between openness and responsiveness versus the ability to define, interpret and place information in different contexts (cf. [Kuhlthau, 1993](#kuh93), invitational mood/indicative mood and [Heinström, 2002](#hei02), broad scanners/deep divers). During the investigation it became clear how difficult it was for the students to seek and use information that, on the one hand, satisfied the need for information, but on the other hand, did not involve information overload. Concerns about getting too much information or not finding enough information were expressed during the work, as well as concerns about finding information that was rich enough but not too advanced.

Conversations about information needs during the students' work did not take place for the most part. In traditional school work the teacher asks questions that have given answers, and it is relatively easy to identify what information needs exist. In self-directed, research-based school work, there are no simple answers and, as a consequence, information needs are more complex and unclear. They may also be dynamic, changing as the task transforms. How information needs are perceived by the students is related to their understanding of the task and its complexity. An assignment of this nature often requires a reassessment of information needs, as the understanding of the task increases. Information needs can be expected to be negotiated and renegotiated during information seeking and use. But such an attitude was not prevalent in the student group that was part of the case study. During interviews, several students (e.g. Diana, Eve and Louise) described how they typically went about seeking information and gathering material, going on to read-throughs and starting to write. Most did not repeat the search for information, but sought information on one or two occasions after the course introduction and were satisfied with what they had found. The students' information seeking can consequently be described as linear. An issue central to information needs is about knowing when to be satisfied, when enough information has been gathered. Determining sufficiency was not obvious to the students and what was perceived as enough depended on the student's own opinions. Working with research-based school assignments, the work should ideally be driven forward by a question. In practice, it was instead the information found that determined how the question was asked:

> Often, I leave the question open, and then I twist the question so that it fits with what I have actually done. I find it more difficult to decide on a question first, and then proceed. I often come up with a question in the middle or the end of my work. (Interview Karen: 3)

It could, on the basis of the collected data, seem that the students did not take command of their work and that coincidences decided the outcome. This aspect of open or closed information seeking highlights the close interaction between information seeking and information use.

### Interaction and communication about information

Dialogue between the different actors was quite limited during the work. The assignment was rarely discussed among the students and conversations with the teacher or librarian occurred primarily at scheduled appointments. School practice is often based on a tradition of asking and answering questions. During the work with the assignment, it was mainly the teacher and librarian who posed the questions, rather than the students, in spite of the character of the assignment. Dialogue could be characterised as traditional, as the teacher and the librarian asking questions, while the students replied.

The findings show that the students' understanding of the role and significance of interaction varied depending on who they were talking to. Conversations among classmates mainly revolved around the delimitations of the assignment or submission dates. Interaction with the teacher was seen by most students as important for the progression of the assignment. The teacher's statements were accorded authority and were unchallenged. Conversations with the teacher were deemed necessary by most students so that work could continue in the right direction. However, a few students did not perceive this interaction as essential to their work, but prioritised time to work on their own. Students' views on the function of conversations with the librarian differed from the findings of some previous research studies. According to previous research, interactions between librarians and students are often focused on the technical aspects of information seeking ([Limberg and Folkesson, 2006](#lim06), p. 25; [Sundin 2008](#sun08)). Interaction in this context focused mainly on how a topic and research questions could be formulated.

> Librarian: What are you going to work with?  
> Jenny: Stress and health.  
> Librarian: What about stress and health?  
> Jenny: Why everyone is in such a hurry.  
> Librarian: Do you mean that society creates stress? What is it in society today that makes us feel stressed? What is society really?  
> Jenny: It's like you have to be busy all the time. It's like you have to do lots of things, or everyone will think you're lazy [...].  
> Librarian: Is it about lifestyle?[...] (Observation protocol 2)

It should, however, be added that in most cases the librarian initiated these conversations. The teacher maintained that the librarian had an important role to play in the students' work since she was an expert in information seeking, but due to time constraints, they did not talk much about the students' progress. According to the librarian, interaction between the librarian and the teacher mostly concerned materials.

One area that did give rise to interaction was the selection of topics. Students who chose topics they found important on a personal level were, unlike others, very engaged in the assignment. The topic itself also played a major role in information seeking. Another area that prompted interaction was research questions. In this context, three dilemmas were identified. Students who were highly engaged in their topics found it difficult to specify one or more questions in the way that was required by the teacher because they had difficulties in formulating research questions and distancing themselves from the topic, as shown in the example below.

> Teacher: What are you going to write about?  
> Fatima: Work and unemployment in Sweden. What are workers' rights and obligations and what are the rights and obligations of unemployed people.  
> Teacher: How do you mean? Are you going to compare?  
> Fatima: Maybe. What do I have to do to get a job?  
> Teacher: What do you need to find out?  
> Fatima: I heard of a woman who went to the Employment office and they told her off for not going there ten years ago. But what was she supposed to do? So, how are you expected to inform unemployed people of the rules?  
> Teacher: Is it just immigrants?  
> Fatima: No, everyone. But mostly immigrants. I'm concerned they don't know the language. (Observation protocol 7)

The second dilemma concerned formulating questions in the form that was expected according to school norms. The third dilemma covered the problem of formulating research questions that went beyond simple factual answers, as shown in the first interview with Caroline.

> Interviewer: You mean that you don't have a research question yet?  
> Caroline: No. Or, what is right and what is wrong and why. That's it.  
> Interviewer: Why people pirate copy?  
> Caroline: Yes, but I know why. It's because it's cheaper. (Interview Caroline:1)

The study showed that the absence of interaction had several reasons. The assignment was not prioritised by the students as much as courses in nursing were and information seeking was not understood as an important part of the assignment. Some did not want to spend time asking questions about information seeking and use, and a few students found the ever-increasing questions from the teacher and librarian difficult and avoided conversation. Some students thought they did well enough on their own.

The findings show that information literacy can be understood as a communicative skill in which actions, both physical and verbal, are an expression of what we have learned (cf. [Sundin and Johannisson, 2005b](#suj05)). Information literacy in this context is thus not only an individual skill, but also a communicative activity. One conclusion from the study is that conversation, or rather the lack of conversation, led to difficulties for the students in developing knowledge of information seeking and use. The fact that teachers, librarians and students communicated so little, verbally or in writing, relating to various aspects of information literacy, resulted in a failure to detect major changes in information seeking and information use as tools for learning. The results reveal that if language, our most important tool, is not used to understand these aspects of complex assignments it will be difficult to develop knowledge about them.

### Identifying and using tools

Instructions for the assignment, grading criteria, teaching and individual tutoring, worked as tools and support for the assignment which in various ways helped the students to structure their work. The degree to which these tools and support were identified and used by the students varied in the group. Some students actively sought support, especially support from the teacher and the librarian. Other students preferred to work on their own. This stems from different interpretations of the tools relevant to the assignment. Another explanation of the differences in the use of tools was that they were not sufficiently visible. Several reasons for rarely seeking support, or using various tools for information seeking and use, were expressed by the students: They did not identify information seeking and use as essential parts of the assignment, they had difficulty expressing their needs to the librarian and the teacher, they were uncertain about what was acceptable to ask questions about, they did not dare to ask for help because of a fear of information overload or they did not ask for help because of difficulties in responding to the teacher's or librarian's questions. Others did not understand how talking to a librarian could benefit their work.

Reasons why some scaffolds and tools were used by the students while others were not can be linked to different perceptions of the needs for tools and support. As stated earlier, the students' views on information seeking were relatively limited, so it was difficult for them to picture how the librarian and other people might help. When information seeking was perceived as entering one or two keywords in a search engine, it was unlikely that the students could appreciate what the librarian might be able to contribute with. When it was difficult to see how the librarian, who actively encouraged students to ask her for help, could help, it became even more difficult to see how other people could support their information seeking and use.

The findings suggest that information literacy also can be linked to the efficient use of tools and support. The way in which students searched for and used information was formed in interaction with the tools and support that were identified during their work. Support and tools offered by the library were not used to any great extent. It was partly because the students did not identify the support and tools, and partly because they had not learned how to use them. It was difficult for the students to understand what kind of support the librarian and the teacher could offer, other than to point to specific sources, when the school assignment seemed so unstructured and unfocused. When the students did not see information seeking and use as essential to the assignment, they did not actively seek for support. Instead information seeking and use was perceived as peripheral, and the students did not appreciate any direct usefulness, other than that it was required to pass the assignment. Difficulties with using computers and the Internet made qualitative information seeking demanding. It was challenging for them to reflect on their work and information seeking, as the tools that could have helped them, were not really available in both intellectual and physical terms.

### Critical approaches and credibility

In a previous study, [Nilsson (2002)](#nil02) examined sixty pupil texts and the results reveal that most of the texts could be described as reproduced texts, where the pupils copied, sampled, repeated and duplicated text from different sources. Consequently large parts of the pupil texts did not originate from the pupils themselves. The pupils reproduced facts, often in a fragmentary manner and only in a few cases was it clear that the pupils drew conclusions. An analysis of how the students in this case study produced text based on categories created by [Nilsson (2002)](#nil02) showed that several categories were used. In the study at hand the students' written reports reveal copying, sampling, referential writing, re-creation of information, extracts from interviews and investigating style. An additional category was also discovered in my case study which is described as author positioning. The boundaries between different forms of text production were not obvious, and all reports contained several different forms. As a whole, the reports contained both arguments and descriptions from various sources and personal viewpoints and opinions, but these different representational forms were mostly separated from each other in the reports. The major part of the reports were descriptive, where each source was described separately, while the report's last page contained a discussion section where the author suddenly became visible, and formulated a number of opinions. The majority of the reports consisted mostly of reproduced texts, without discussions of credibility or the authority of information.

<table class="center" style="width:90%;"><caption>  
Table 1: Overview of students' forms of text production</caption>

<tbody>

<tr>

<th rowspan="2" style="width:40%;">Form of text  
production</th>

<th colspan="14">Student</th>

</tr>

<tr>

<th>A</th>

<th>B</th>

<th>C</th>

<th>D</th>

<th>E</th>

<th>F</th>

<th>G</th>

<th>H</th>

<th>I</th>

<th>J</th>

<th>K</th>

<th>L</th>

<th>M</th>

<th>N</th>

</tr>

<tr>

<td>Copying</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td>Sampling</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td>Referential writing</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

</tr>

<tr>

<td>Re-creation of information</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td>Interview extracts</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td>Investigating</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

<td style="text-align:center;">X</td>

</tr>

<tr>

<td>Author positioning</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;"> </td>

<td style="text-align:center;">X</td>

</tr>

</tbody>

</table>

Few of the students indicated what page in a book they had taken text from, even though long text extracts or even entire pages was copied directly.

> Interviewer: It seems like you used sources in the text that aren't included in the reference list.  
> Maria: Yes. Most of it we took from the Internet. (Interview Maria:3)

Previous research shows that students who plagiarise substantially are relatively uninvolved in their tasks, have not learned much about their subject over time, are not as interested in learning and understanding as other students and find it difficult to recognise plagiarised text ([McGregor and Williamson, 2005)](#mcg05). In this context, copying could be interpreted as due to lack of experience or unawareness of the procedure rather than plagiarism. In this case it proved difficult for the students to write text that was not only based on personal experience or opinions, and not only copying other texts, but something in between. Few of the students succeeded in what was a large part of the assignment, namely to create their own arguments and positions through writing by using different sources.

In educational contexts students are not only expected to refer to the topical content of different information sources, they are also expected to react and respond to information in various ways. The interviews conducted with the students revealed few reflections and arguments on the evaluation or authority of information. Credibility assessments and critical thinking played no major part in this information practice. Principally, information where the Swedish government was the author was preferred to information from private, individual sources. Printed information, particularly course books, was favored over information from the Internet.

> I think it's easier to get information from books. To find things you want to find. On the net you open up a page and there's a lot of ... with all kinds of things. In books you find knowledge.[...] There's no nonsense, and that's good. (Interview Helen:1)

In common with many adult education students at upper secondary level, this group of adult students was not very familiar with ICT tools for seeking and using information in the school context, and most of them found it difficult to use a PC. This is a feature that often distinguishes adult learners from younger students. In [Sormunen and Lehti's study (2011)](#sor11), young students preferred Web sources to printed materials, as they thought that textbooks were too general to be useful for writing about the specific topics of the assignments.

From the students' perspective, evaluating information in this case meant making a choice, where one option was considered good or right and the other option bad or wrong. Comparing different information sources in order to highlight nuances or different perspectives was not an approach commonly used in the group, although there were a few exceptions. It was revealed that the students did not lack awareness of critical thinking, but they did not possess knowledge of how the tools could be applied to critically analyse a text, to compare information sources, or to consider originality in a document they had found. The fact that the students' own ideas, opinions and experiences were not discussed to any greater extent contributed to making it harder for them to develop a critical approach.

### The character of sources and genre knowledge

Closely associated with critical approaches and credibility is what might be called awareness of the characteristics of a source, or the ability to understand different genres. Oral or written discussions of texts' origins or objectives (see [Andersen, 2006)](#and06) were sporadic in the study. It proved difficult for the students to see differences between information from newspapers, textbooks, encyclopedias, or research papers. In most cases, sources were valued as equal and rather than considering genre, information was judged as fact or not fact (cf. [Alexandersson and Limberg, 2004](#ale04); [Gordon _et al._ 2014](#gar14); [Sundin and Francke, 2009)](#sun09).

> Caroline: It would have been easier not to use newspaper articles perhaps.  
> Interviewer: And used what instead?  
> Caroline: More facts, I mean books. (Interview Caroline:3)

The focus on facts was still vivid, even though the assignment was research based and no correct answers existed. Facts and correct answers are a heritage of traditional schooling and it seems to be difficult to appropriate other approaches ([Francke _et al._ 2011](#fra11), p. 691).

> Jenny: It isn't exactly facts I've got, it's what different people think and believe.  
> Interviewer: What do you mean by facts?  
> Jenny: Well, that something is decided, definite, in a certain way.  
> Interviewer: And you don't think this is decided?  
> Jenny: No, it's more about how people experience stress. But in a way that might be a fact. (Interview Jenny:2)

As described above, most of the students had two distinctly separated sections in their reports. In the first section, they used the sources they had found, in the second, they expressed their own opinions about the chosen topic. The latter sections were placed at the end of the reports. In some cases, these sections were based on thoughts, ideas and beliefs without bearing on the previously presented text. One reason for separating the report into two sections was that students were unable to use information in the way that learning objectives and grading criteria stipulated. When writing reports at upper secondary level, students are expected to seek and use information from different sources, critically work through different texts and use this knowledge to formulate well substantiated arguments and lines of reasoning. When it comes to writing in school contexts traditionally, deviations from the original text are generally not allowed, while today's school appreciates writing that stresses what is new, as well as the use of texts for new purposes (see [Soljo, 2005](#sal05), p. 240). The study exemplified this tension between previous school experiences and the new requirements of learning.

The reason that the students found it difficult to integrate various forms of texts and sources probably also has to do with the culture of adult education, where both rhetoric and the work conducted emphasise that the students' own experiences are important and highly valued. Individual thinking and reflection, rather than critical thinking in student work, were acknowledged by both students and the teacher. The notion that adult students' own experiences should be respected had the effect that critical approaches to information use received little attention.

### The situatedness of information seeking and use

The understanding of the contextual nature of information is another aspect of information literacy. The study confirmed that being able to identify what is meant by information or knowledge in specific contexts is one of the most important features of self-directed school work.

In the school context, it is not uncommon for knowledge to become so closely associated with a particular context that it is difficult for those involved to discern the same or similar patterns, rules or structures in another context. For example, some of the students said that they had worked with this type of assignment before (albeit not as extensively) and others argued that they had not done so, although they had all taken the same courses. Another example is that previous course literature was not recognised as important for this task, even if it addressed their chosen topic.

The study showed that there was no discussion about how the students worked when they used information from various sources in their reports. Collecting information here and there from various texts decontextualises information and takes it out of context (see [Pawley, 2003](#paw03)). The students had the opportunity to seek, select and use information and it gave them confidence, but it also put great demands on their ability to manage information. The way in which they sought and used the information for other school assignments (e.g. courses in nursing) was not suitable for this assignment. The medical courses included in the program were more traditionally oriented in the sense that information could be described as correct or incorrect. This way of categorizing information was transferred to the social studies course, in which other approaches would have been more appropriate, thus illustrating how information literacy can be understood as socially situated.

The seven critical aspects that were found to be particularly prominent in the tension between the school's discursive practice and the students' self-directed work are summarised in Figure 1.

<figure class="centre">![Critical aspects of information literacy in the tension between schools' discursive practice and students' self-directed learning](p734fig2.jpg)

<figcaption>  
Figure 1: Critical aspects of information literacy in the tension between schools' discursive practice and students' self-directed learning</figcaption>

</figure>

## Conclusions

From a socio-cultural perspective, students develop certain aspects of information literacy in order to function in an environment, such as a school environment with its stated and tacit requirements. Other aspects of information literacy are developed in other contexts, where other standards and rules are valid. To understand more about adult students' information literacy in relation to self-directed learning, it was essential to follow the work with an assignment from beginning to end, within a specific practice. The case study demonstrates that information literacy in this context can be characterised as communicative, creative, contextual and critical competences.

In spite of favourable preconditions the students in the case study did not actively seek support or tools for information seeking, and the teacher and the librarian did not contribute to any great extent. One reason was that the stakeholders did not share a common frame of reference with regard to the assignment. To understand the requirements of the problem or assignment is part of learning. In order to analyse a research problem, the students need to seek and use information. Information seeking and use can therefore be described as both objects and as tools for learning in self-directed learning.

The findings reveal that the absence of interaction around information seeking and use in the educational context resulted in difficulties for the students to achieve the expected results as they were formulated in the learning objectives. There was, in fact, no difference in the way the actors discussed information seeking and use after the assignment, although all students passed the assignment. When the adult students were working with a school assignment which was research based, they negotiated meaning through discussions with various stakeholders, and through writing text that the teacher gave feedback on. The findings show that although the assignment was described as free and open, the students had to communicate in a particular way for the task to be approved according to school norms. The student's adaptation to, or interest in, the school's rules, the stated requirements and tacit understandings were tested when they presented their reports. Although the room for interpretation could be described as wide compared to the leeway found in more traditional teaching, it was the teacher who was the examiner and the students were in a dependent position vis-a-vis the teacher.

The school's discursive practice, such as the choice of specific assignments and methods, shape the students' information seeking and use, as well as the use of tools and scaffolds. The analysis reveals the arduousness of reshaping traditional practice, despite considerable efforts. Hopefully, research in library and information science has the potential to contribute to the transformation of traditional school practice through in-depth knowledge of information literacy.

## Acknowledgements

This paper is based on the thesis _Tools for Learning: Information Seeking and Use in Municipal Adult Education_ (Verktyg for lärande: Informationssokning och informationsanvondning i kommunal vuxenutbildning, Gordon 2010). The Linnaeus Centre for Research on Learning, Interaction and Mediated Communication in Contemporary Society (LinCS; 349-2006-146), at University of Gothenburg and University of Boras, made it possible to write the paper. I would like to thank the study participants for sharing their experiences, Helena Francke for reading and commenting on the text and Frances Hultgren for providing language help. I would also like to thank the two anonymous reviewers for their constructive comments and suggestions.

## About the author

Dr **Cecilia Gordon** is a Senior Lecturer at the Swedish School of Library and Information Science, University of Borus, SE-501 90 Borus, Sweden. Her research interests concern information seeking and use practices, information literacy, libraries as learning organisations and interactive research approaches. She received her PhD from University of Gothenburg. She can be contacted at [cecilia.garden@hb.se](mailto:cecilia.garden@hb.se)

#### References

*   Alexandersson, M. & Limberg, L. (2004). _Textflytt och sukslump: Informationssokning via skolbibliotek_ [Copy paste and serendipity: information seeking via the school library]. Stockholm: Myndigheten for skolutveckling.
*   Andersen, J. (2006). The public sphere and discursive activities: information literacy as sociopolitical skills. _Journal of Documentation, 62_(2) 213-228.
*   Bryman, A. (2008). _Social research methods_. Oxford. Oxford University Press.
*   Francke, H., Sundin, O. & Limberg, L. (2011). Debating credibility: the shaping of information literacies in upper secondary school. _Journal of Documentation, 67_(4) 675-694.
*   Gross, M. (1995). The imposed query. _RQ, 35_(2) 236-243.
*   Gross, M. (2001). [Imposed information seeking in public libraries and school library media centers: a common behaviour?](http://www.webcitation.org/5uRBaqRrB) _Information Research, 6_(2) (paper 100). Retrieved from http://informationr.net/ir/6-2/paper100.html (Archived by WebCiteo at http://www.webcitation.org/5uRBaqRrB)
*   Gross, M. & Latham, D. (2007). Attaining information literacy: an investigation of the relationship between skill level, self-estimates of skill, and library anxiety. _Library & Information Science Research, 29_(3) 332-353.
*   Gross, M. & Latham, D. (2009). Undergraduate perceptions of information literacy: defining, attaining, and self-assessing skills. _College and Research Libraries, 70_(4) 336-350.
*   Gordon, C., Francke, H., Lundh, A.H. & Limberg, L. (2014). [A matter of facts? Linguistic tools in the context of information seeking and use in schools](http://www.webcitation.org/6cT5btwtv). In: _Proceedings of ISIC, the Information Behaviour Conference, Leeds, 2-5 September, 2014: Part 1_, (paper isic07). Retrieved from http://InformationR.net/ir/19-4/isic/isic07.html (Archived by WebCiteo at http://www.webcitation.org/6cT5btwtv)
*   Head, A.J. & Eisenberg, M.B. (2009). _Lessons learned: how college students seek information in the digital age._. Seattle, WA: The Information School, University of Washington. (Project Information Literacy Progress Report)
*   Head, A.J. & Eisenberg, M.B. (2010). _Truth be told: how college students evaluate and use information in the digital age._. Seattle, WA: The Information School, University of Washington. (Project Information Literacy Progress Report)
*   Hedman, J. & Lundh, A. (2009). Informationskompetenser: Reflektioner kring teman i antologin [Information literacies: Reflections about themes in the anthology]. In: J. Hedman, & A. Lundh (Eds.) _Informationskompetenser: Om lärande i informationspraktiker och informationssokning i lärandepraktiker_ [Information literacies: learning in information practices and information seeking in learning practices] (pp. 268-285). Stockholm: Carlsson.
*   Heinström, J. (2002). _Fast surfers, broad scanners and deep divers: personality and information seeking behaviour_. obo, Finland: Abo Akademi University Press. (Doctoral dissertation).
*   Julien, H. & Barker, S. (2009). How high-school students find and evaluate scientific information: a basis for information literacy skills development. _Library & Information Science Research, 31_(1) 12-17.
*   Kuhlthau, C. (1993). _Seeking meaning: a process approach to library and information services_. Norwood NJ: Ablex Publishing Corporation.
*   Lankshear, C. & Knobel, M. (2003). _New literacies: changing knowledge and classroom practice_. Buckingham, UK: Open University Press, McGraw-Hill Education
*   Lankshear, C. & Knobel, M. (2011). _New literacies: everyday practices and social learning_. (3rd ed.). Maidenhead, U.K: Open University Press, McGraw-Hill Education.
*   Limberg, L. (1999). Three conceptions of information seeking and use. In T. D. Wilson & D.K. Allen (Eds.), _Exploring the contexts of information behaviour. Proceedings of the second international conference on research on information needs, seeking and use in different contexts_ (pp. 116-133). London: Taylor Graham.
*   Limberg, L. (2007). [Learning assignment as task in information seeking research](http://www.webcitation.org/6RxnySIPQ). _Information Research, 12_(1) (paper colis28). Retrieved from http://www.informationr.net/ir/12-4/colis28.html (Archived by WebCite at http://www.webcitation.org/6RxnySIPQ)
*   Limberg, L. & Folkesson, L. (2006). _Undervisning i informationssokning: Slutrapport fran projektet Informationssokning, didaktik och lärande (IDOL)_ [Teaching information literacy: Report from the project Information seeking, didactics and learning]. Boras: Valfrid.
*   Limberg, L., Sundin, O. & Talja S. (2012). Three theoretical perspectives on information literacy. _Human IT, 11_(2) 93-130.
*   Lloyd, A. (2006). Information literacy landscapes: an emerging picture. _Journal of Documentation, 62_(5) 570-583.
*   Lloyd, A. (2012). Information literacy as a socially enacted practice: Sensitising themes for an emerging people in practice perspective. _Journal of Documentation, 68_(6) 772-223.
*   Lundh, A. (2011). _Doing research in primary school: information activities in project-based learning_. Boros, Sweden: Valfrid. (Doctoral dissertation).
*   Lupton, M. (2008). _Information literacy and learning_. Adelaide, SA: Auslib Press.
*   McGregor, J.H. & Williamson, K. (2005). Appropriate use of information at the secondary school level: Understanding and avoiding plagiarism. _Library & Information Science Research, 27_(3) 496-512.
*   Merriam, S.B. (1988). _Case study research in education: a qualitative approach_. San Francisco, CA: Jossey-Bass.
*   Nilsson, N.E. (2002). _Skriv med egna ord: En studie av loroprocesser nor elever i grundskolans senare or skriver "forskningsrapporter"_ [Write in your own words: a study of learning processes in research report writing in school]. Malmo, Sweden: Malmo Hogskola. (Doctoral dissertation).
*   Pawley, C. (2003). Information literacy: a contradictory coupling. _Library Quarterly, 73_(4) 422-452.
*   Rasmussen, I., Krange, I. & Ludvigsen, S.R.. (2004). The process of understanding the task: how is agency distributed between students, teachers and representations in technology-rich learning environments? _International Journal of Educational Research, 39_(8) 839-849.
*   Rivano Eckerdal, J. (2012). _Information, identitet, medborgarskap: Unga kvinnor berottar om val av preventivmedel_ [Information, identity, citizenship: young women on choices of contraceptives]. Lund, Sweden: Lund University, Institutionen for kulturvetenskaper. (Doctoral dissertation).
*   Shenton, A.K. & Hay-Gibson, N.V. (2011). Modelling the information-seeking behaviour of children and young people: inspiration from beyond. _Aslib Proceedings, 63_(1) 57-75.
*   Sormunen, E. & Lehti , L. (2011). [Authoring Wikipedia articles as an information literacy assignment - copy-pasting or expressing new understanding in one's own words?](http://www.webcitation.org/6BSo3VJdn) _Information Research, 16_(4) (paper 503). Retrieved from http://InformationR.net/ir/16-4/paper503.html (Archived by WebCite at http://www.webcitation.org/6BSo3VJdn)
*   Spink, A., Danby, S., Mallan, K. & Butler, C. (2010). Exploring young children's web searching and technoliteracy. _Journal of Documentation, 66_(2) 191-206.
*   Stake, R.E. (1994). Case studies. In: N.K. Denzin & Y.S. Lincoln (Eds.), _Handbook of qualitative research_ (pp. 236-247). London: Sage Publications.
*   Sundin, O. (2008). Negotiations on information-seeking expertise: a study of web-based tutorials for information literacy. _Journal of Documentation, 64_(1) 24-44.
*   Sundin, O. & Francke, H. (2009). [In search of credibility: Pupils' information practices in learning environments](http://www.webcitation.org/5tzj2fj1h) _Information Research, 14_(4) (paper 418). Retrieved from http://InformationR.net/ir/14-4/paper418.html (Archived by WebCite at http://www.webcitation.org/5tzj2fj1h)
*   Sundin, O. & Johannisson, J. (2005a). The instrumentality of information needs and relevance. In: F. Crestani & I. Ruthven (Eds.), _Context: Nature, Impact, and Role: 5th International Conference on Conceptions of Library and Information Sciences_ (pp. 107-118). New York, NY: Springer-Verlag.
*   Sundin, O. & Johannisson, J. (2005b). Pragmatism, neo-pragmatism and sociocultural theory: Communicative participation as a perspective in LIS. _Journal of Documentation, 61_(1) 23-43.
*   Soljo, R. (2000). _lärande i praktiken: Ett sociokulturellt perspektiv_ [Learning in practice: a sociocultural perspective]. Stockholm: Prisma.
*   Soljo, R. (2005). _lärande och kulturella redskap. Om lorprocesser och det kollektiva minnet_ [Learning and cultural tools: on learning processes and the collective memory]. Stockholm: Norstedts Akademiska Forlag.
*   Tuominen, K., Talja, S. & Savolainen, R. (2005). The social constructionist viewpoint on information practices. In: K. Fisher, S. Erdelez & L. McKechnie (Eds.), _Theories of information behavior: a researcher's guide_ (pp. 328-333). Medford, NJ: Information Today.
*   Vygotsky, L.S. (1978). _Mind in society: The development of higher psychological processes_. Cambridge, MA: Harvard University Press.
*   Wang, L., Bruce, C.S., & Hughes, H.E. (2011). Sociocultural theories and their application in information literacy research and education. _Australian Academic & Research Libraries, 42_(4) 296-308.
*   Williamson, K. & McGregor, J. (2006). [Information use and secondary school students: a model for understanding plagiarism](http://www.webcitation.org/63UYwJxtf) _Information Research. 12_(1) (paper 288). Retrieved from http://InformationR.net/ir/12-1/paper288.html (Archived by WebCite at http://www.webcitation.org/63UYwJxtf)
*   Yin, R.K. (2003). _Case study research: s\design and methods_. Thousand Oaks, CA: Sage Publications.