####  vol. 13 no. 2, June 2008


# Information needs and information competencies: a case study of the off-site supervision of financial institutions in Brazil.



#### [Silvania V. Miranda](mailto:silvania.miranda@bcb.gov.br) and [Kira M.A. Tarapanoff](mailto:kat309@unb.br)  
Information Science Department  
University of Brasília, Brazil

#### Abstract

> **Introduction.** The paper deals with the identification of the information needs and information competencies of a professional group.  
> **Theoretical basis.** A theoretical relationship between information needs and information competencies as subjects is proposed. Three dimensions are examine: cognitive, affective and situational. The recognition of an information need was linked to the development of competencies to analyse the problem in focus.  
> **Methodology.** Qualitative epistemology was used and the research strategy was a case study. The research techniques were document analysis, interviews, participant observation, work process analysis and focus groups.  
> **Analysis and results.** The analysis of critical success factors and the work processes mapping brought understanding of the relationship between the identified information needs and the information competencies developed to satisfy them.  
> **Conclusion.** Information needs were closely linked to the needs of the work processes and that the competencies developed to attend these needs were closely related to the success factors. The proposed research framework offered a new perspective that had positive results in understanding the main characteristics of the activity.



## Introduction

The research problem tried to identify information needs and information competencies for off-site supervisors of financial institutions located in Brazil. The specific purpose was to study how information needs and information competencies of this professional group could be identified applying an informational point of view. Organizational activities are ordinarily studied from an organizational (managerial) point of view, even when it concerns information. Thus, the main issue of the research was to consider informational parameters instead of organizational parameters to address the problem and to study the activity with information science parameters. A combined theoretical and methodological approach was adopted to identify both information needs and information competencies, establishing a possible link between these two, usually separated, approaches.

It is important to stress that no scientific research was found on the subject of information needs and/or information competencies for off-site supervision professionals in the literature review. From the literature it was observed, first, that this activity is frequently attributed to public organizations as a regulatory activity; secondly, information needs, as a theme, has received special attention in information science, in the user studies area; and, finally, competency, as a theme, is still in its consolidation phase in academic and organizational contexts and is centred in complex studies of cognition and behaviour.

Therefore, the intention of this article was not to discuss all the concepts involved or the historical and theoretical origins of the themes. The main objective was rather to present basic elements to supply a minimum theoretical framework to discuss the research findings.

## Theoretical basis: subject development.

### Definitions.

Information need is defined as a state or process started when one perceives that there is a gap between the information and knowledge available to solve a problem and the actual solution of the problem. Information competencies are defined as the capabilities developed to reach the solution of a problem by searching for new information or knowledge that could fill the perceived gap.  

In this article we assume that information need and information competency can be identified on three dimensions: cognitive (knowledge); affective (attitudes); and situational (abilities). They correspond to _knowing_, _know how_ and _know how to act_ in work situations when one is dealing with information problems that need solving on a daily basis.  

It is defended that information parameters which information science researchers can use are: the information cycle and technology and contexts of information communication. The information cycle encompasses the phases of information work that include:  collection, storage, distribution, retrieval and use. Information technology can be defined as the technology that influences the architecture of recorded knowledge (media, format, content and type). In the information age this technology involves telecommunication systems, computers and software. They help to organize, transmit, store and use great volumes of data and information at low cost and high-speed, making possible several applications. Information contexts are contexts in which the information cycle takes place. By influencing the information flow and its characteristics, one can differentiate types of information such as technological information, businesses information and scientific information.

### The off-site supervision of financial institutions in Brazil.

The research problem addressed here is to study the activity of off-site supervision, a governmental regulation activity, as an information activity. Regulation is about control management by public agencies. It is based on economic or social aspects related to provision of: goods and services; rules or control forms or social influence; and processes that direct one or more economic and/or social sectors ([Moura 2002](#moura)). Supervision, according to Lacombe ([2004](#lacombe)), means to guide and to control the activities of some organ or some person. The supervision of financial institutions is a specific niche in regulatory activity. Its purpose, according to Rocha ([1999](#rocha)), is to guarantee the stability of the financial institutions and the confidence of the public in these institutions. Regulation and supervision of financial systems are internationally coordinated by the World Bank, but other organizations such as the International Monetary Fund and the International Organization of Securities Commissions also deal with this subject.

The World Bank draws attention to general principles of supervision trying to assure that the procedures have an international pattern for comparison. It is recommended that all countries follow these principles. The supervision of financial institutions is sometimes characterized by a verifying focus: punishing wrong doing and checking legal issues. The globalization of financial activities addresses the supervision of more complex issues such as surveillance of solidity and financial solvency. In other words, attention has turned to preventative issues such as previous alert indicators, high professionalism in risk control, inspection systems with integrated objectives and accountability regarding the credibility of the financial information supplied to the public and to the supervisors ([Arias 1993](#arias)).

In the preventative supervision path, two different activities can be considered: off-site supervision or the monitoring of the institution's behaviour using information processes; and on-site supervision, the verification _in loco_ of what happens in financial institutions day-by-day. The Basle Committee, in its principles, implicitly recommends an appropriate structure for banking supervision. It is emphasized that monitoring is a minimum procedure that assures the existence of a continuous supervision effort and supports on-site supervision planning. In Italy, for instance, on-site supervision starts with an annual work plan that takes into consideration the identified priorities of off-site supervision( [International Monetary Fund 2004](#imf); [Basle Committee... 1999](#basle)).

In Brazil, the Central Bank is responsible for the regulation and supervision of the Brazilian financial system. It is a public institution that has the legal function of taking care of the safety and solidity of the Brazilian financial system. Supervision is accomplished using two organizational units: one supervises the financial institutions directly, while the other collects and organizes the data and the information available and monitors the financial market ([Brasil 1993](#brasil93), 2003).

The off-site supervision in Brazil monitors the financial institutions and analyses occurrences that could influence the economic and financial situation of these institutions. They make efforts to detect situations that represent (or could come to represent) expressive losses or provoke instability in the market and try to foresee trends and adopt preventative and protective measures [(Brasil 2003)](#brasil03). Off-site supervision can be defined as an activity that works with financial information, producing information services and products that help decision makers to analyse financial markets. The study of this activity from the information science point of view constitutes a novelty that may result in contributions for several research areas, as well as for public organizations responsible for supervising financial institutions.

### Theoretical Framework: connecting information needs and information competencies.

This study considers information needs and information competencies using a combined theoretical approach. The idea was to connect information needs to information competencies in a way that could allow a united methodological path for the identification of both, in practical work situations.

Information search and use processes define information needs. They are built cognitively and emotionally and also on situational and dynamic forms. Information use depends on the individual evaluation of the cognitive and emotional relevance of the information received. It also depends on the appropriated information to solve a certain problematic situation. The construction of information is linked to the social and organizational world in which the user exists. These structures form the conditions that give rise to the need for information. Information needs is determined by: 1) the perception of knowledge gaps (and/or capability gaps) to make sense of the problems to solve or the tasks to perform; 2) the inherent emotional factors linked to the uncertainty, stress level and existing difficulty when noticing those gaps; and 3) the situational factors linked to particular contexts and specific experiences ([Wilson 1981](#wilson81), [2000](#wilson00), [2002](#wilson00) and [Choo 2003](#choo03), [2006](#choo06)).

Organizations can be seen as interpreter systems of environmental information and/or as complex systems to process information. Information, in an organizational environment, is an individual and social product. It is part of all activities, adding value to them with or without the use of information systems and formal tools. It is a value-added process of: organizing, analysing, evaluating and making decisions. Understanding the information as a representation of reality (translation and interpretation), or as a value creation, one can try to evaluate the importance of the invisible information work that composes each person's main activity inside an organization [(Daft & Lengel 1996; Guyot 2000; Taylor 1986; Weick 2001)](#daft). The information needs of different groups of people and organizations are determined the same way as those of the individuals: when a knowledge and meaning gap is perceived and when facing emotional and situational factors.

Competency, on an individual basis, refers to resources possessed and acquired by a person. Most importantly, it refers to the resources that can be put into action in a practical situation. It presupposes the mobilization of personal internal resources (acquired and developed by the individuals in given situations) and of collective resources (brought and made available by the organization). The logic of the competency is to integrate knowledge, know-how and behaviour according to the demands of a work situation. Professional competency resides in the mobilization of the resources and not in the resources themselves. Going from knowledge to action is a re-construction process, a process of value aggregation. Several scholars who study competency at an individual level consider it in three dimensions: knowledge (to know), abilities (to know how to do) and attitudes (to know how to act) ([Brandão 1999](#brandao99); [Fleury & Fleury 2001](#fleury01); [Dutra 2001](#dutra); [Le Boterf 2003](#leboterf03); [Zarifian 2001](#zarifian01), [2004](#zarifian04); [Carbone _et al_. 2005](#carbone)).

Organizational competencies are established starting from the combination of resources and multiple individual competencies in a way that the final result is larger than the sum of all individual competencies. When a company defines its competitive strategy, it also defines the core competencies of the business and the necessary competencies of each function. Core competencies are developed in processes of learning-by-doing, in specific contexts within companies or in contexts of cooperation among companies. The characteristics of a core competency are: specificity, uniqueness, difficulty to imitate and contribution to customer value. Because of these distinctive qualities the core competencies can provide competitive advantages to the organization ([Prahalad & Hamel 1990](#prahalad); [Fleury & Fleury 2001](#fleury01) [2004](#fleury04); [Brandão & Guimarães 2001](#brandao01)).

Information competency is a group of competencies put into action when one is working with information. It could be expressed by the expertise forged when someone works with the information cycle and technologies and with different information contexts. This competency, put into operation in work situations, can be seen as one of the requirements for the necessary professional profile to work with information. It crosses different business processes (managerial and technical) and different parts of an organization or activity. It can be compared to basic competencies that are acquired in educational situations and formalized in cognitive and behaviour conquests. It is necessary to face problematic situations where information work has a primary role ([Miranda 2004](#miranda04)).

In our theoretical definition, we consider that information need and information competency share similar dimensions. Both can be understood in three main dimensions: cognitive (knowledge), affective (attitudes, knowing how to act) and situational (abilities or know-how). in other words, they have similarities that can be shared by the same theoretical approach, opening the possibility for a united framework to study both themes. A proposed theoretical correspondence is represented in Figure 1\. It defines and links the dimensions of information need and information competency in the cognitive (knowledge), situational (ability) and affective (attitude) dimension. These dimensions and their similarities are explored in the following section.

<div align="center">![Figure 1](p344fig1.gif)</div>

<div align="center">  
**Figure 1: Relating information needs and information competencies** (Source, Miranda 2007)</div>

### Exploring the dimensions of information needs and information competencies.

**Knowledge**, on an individual basis, is located in the relationship between the subject and the object. It also corresponds to a set of information that is recognized and integrated by an individual in his or her memory, causing impact on behaviour and composing the knowledge accumulated throughout life. Knowledge, besides individual, also results from actions and collective reflections produced by the information practices of people's groups. This process can create shared interpretations about the environment, new products and new capacities. It also makes it possible to select courses of action to reach objectives and commit resources and capacities. Organizational knowledge is based on the individuals' professional knowledge and on the knowledge of the groups that compose the organization. It is also composed of three parts: an explicit part composed by norms, rules, manuals etc.; a tacit part that people make an effort to express and a cultural part usually included in the current behavioural system ([Varela 1991](#varela); [Hessen 2003](#hessen03); [Carbone _et al._ 2005](#carbone); and [Choo 2006](#choo06)). The nature of the needs and the structure of the knowledge states are variables that should be investigated to identify information competencies.

A **cognitive dimension** refers to both individual points of view and groups' (or organizations') point of view, expressing: history, structures and cognitive styles of information users. The cognitive approach, suggests that people are mediated, in their interactions, by states of knowledge about themselves, about those whom they interact with and/or about the problematic situations they face. Cognitive styles and individual preferences can influence the searching, processing and using of information. Cognitive consensus can be developed inside organizations. This makes possible a reasonable degree of common understanding and collective action, although individualized behaviour continues to exist. Organizational culture (a group of shared premises) can provide a structure for cognitive, behavioural and emotional answers. Organizations can also provide guidelines for individuals to deal with their cognitive limitations and directions for the attention flow that fills out gaps of information and removes discrepancies in one's understanding (stereotypes; typologies of attitudes, sets of intentions and consequences; characterizations; scripts; memories; communication outlines). This framework provides sense making and an increased possibility of satisfactory actions ([Belkin 1980](#belkin80), [1982](#belkin82); [March 1994](#march); [Dervin 1998](#dervin) and [Choo 2006](#choo06)).

**Abilities** can be related to the productive application of knowledge or, in other words, to the capability to search for the information gained in a previous experience to examine and to solve a problem (or to use stored knowledge in actions). They are the know-how: experiences acquired in practice, empirical procedures (recipes and work tricks) that cannot be standardized. Professional contexts and professional situations determine the development of abilities ([Stroobants 2004](#stroobants); [Carbone _et al._ 2005](#carbone)).

The **situational dimension** refers to the beliefs and suppositions that are part of the individual's culture and of his or her environment. It is a set of characteristics, opportunities and difficulties caused by the environment, structures of the existing problems and beliefs about what constitutes the solution of a problem. Among the factors that can influence the perception of in are: the roles played by the individual in the work environment; and the complexity of the problems, tasks and the environment itself. The more complex one's task is the larger the amount of information used to deal with the exceptions and with the media richness. Furthermore, the more complex the situation is, the greater the uncertainty of being able to solve a problem. The information value, in a certain environment, depends on the requirements, norms and expectations of the user's work and the organizational context. This can influence the nature and intensity of information needs, the construction of information and access to it and the capability to analyse the environment. The information use environment (geographical, organizational, social, intellectual and cultural) is a set of elements that affects the flow and the use of the messages, defines entities or groups of customers and determines the criteria by which the value of the information messages will be judged. The analysis of information use inside the environment needs to be translated into information terms; especially in organizations, where it is necessary to monitor, to interpret and to adapt the external information ([Taylor 1986](#taylor); [Choo 2006](#choo)). The value added to the information remains exact in this translation process, which is situational.

**Attitudes** refer to social and emotional aspects, to preferences and interests, to the effort and the demand control conditions to express or to adopt some kind of behaviour. They are related to feelings or predispositions that determine the behaviour of one person in relation to other persons and also to the work or to the situation that people face. It is the person, his or her biography and socialization that determine how to act. The culture supplies the professional with a 'box of symbolic tools' with which s/he can model the outlines of adaptive behaviour. Knowing how to act includes personal qualities, social knowledge and common sense: these appear in cases where a problem is not explicit. Attitudes set a group of information that may influence individuals' and groups' actions ([Brandão 1999](#brandao99); [Le Boterf 2003](#leboterf03); [Bastos 2004](#bastos); [Stroobants 2004](#stroobants); [Carbone _et al_. 2005](#carbone)).

The **affective dimension** is related to the individual and organizational history, in terms of the progression of thoughts, feelings and perceptions experienced in moments of confusion, uncertainty, anxiety, expectation, accessibility and objectivity. This history drives the strategies and the decisions to search and use information. Although the emotional states are felt at an intra-personal level, it is in the socialization process that one learns in which contexts some feelings should be expressed or inhibited. The relationship between reason and emotion is that of interdependence. The emotional factors give quality to human relationships in the work environment. Besides the fact that feelings are a subjective experience, implicit and explicit emotional rules are established and transmitted within our ethnic groups, families, religions and workplaces. They express, in a certain way, the value system and the language that a person carries with them. The information process is systemic and even behaviour based on rules is threatened by uncertainty, ambiguity and inconsistency. This can generate stress, inducing feelings that can interfere in the decision to search for information. The process that transforms information into meanings involves not only thought and action, but also feelings ranging from confusion to frustration, from clarity to optimism. To search for information to formulate meanings in any subject, inside of a referential structure, is bound to be uncertain. Everything depends on the result of the assimilation of new information to the reference chart used to extend the knowledge on a subject ([Kuhlthau 1991](#kuhlthau91) [1993](#kuhlthau93); [Wilson 1996](#wilson96) [1999](#wilson99); [Fineman 2001](#fineman) and [Gondim & Siqueira 2004](#gondim)).

In short, accumulated knowledge is linked to the previous structure of one individual mind as the basis for the evolution of thoughts, which are his or her cognitive domain. The connection between know-how and the situational conditions exists in the measure that the work situation establishes forms to develop the search for and use of information and influences competency construction to deal with information. To know-how-to-act and the affective conditions are linked because the environment engenders the elements that influence the action of the professionals and the formation of their competencies. Attitudes connect _emotional memories_ to paths once chosen when solving a problem. The theoretical proposition to analyse the research problem is: one's visualized solutions to attend one's information needs probably corresponds to one's information competencies development to solve the problem, or to deal with knowledge gaps and/or with the sense of emptiness experienced when the environment changes.

## Research method and techniques

It was the researchers' intention to understand off-site supervision as an information intensive activity that has specific needs and that requires, as assumed, the development of specific information competencies when performing this job. As a theoretical contribution, the present work fills a gap on the theme of off-site supervision and its entire complex environment. The adopted approach linking information needs and information competency themes, was also innovative as a proposal.

When developing their argument the researchers considered that a previous theoretical construction could be a useful guide to develop initial understanding of how the information needs could be perceived and identified in the studied activity and how the competencies related to them could be developed.

On the other hand, there was the need for an approach that could provide a constant relationship between theoretical and practical moments to obtain a larger understanding of the research problem. González Rey's point of view was chosen as an approach that could fulfil the research need for a constructive and interactive path.

González Rey ([2005](#gonzalez)) proposed a subjective-interpretative approach, whose essential analytical units are _subjective senses_ that might express objective aspects of the social life. To that, the authors associated a qualitative epistemology based on a set of general principles to construct knowledge. The constructive, interpretative and interactive characters of knowledge and the role of singular cases can be outlined as characteristics. in this approach, the central point of the research would be to build comprehensive theoretical models that have explanatory value for complex systems. Considered from González Rey's point of view, the theoretical constructs formulated for the present research could be a guide for the data collection and for interpreting the information generated during the research. Information was generated and ideas were formulated during the whole research process seeking to understand the research problem. The goal was to obtain a final construction that could offer the best possible understanding when confronted with the empiric reality observed.

A case study was conducted to study information needs and competencies of the off-site supervision of financial institutions professionals in Brazil, using a united theoretical basis: the research construct expressed mainly by Figure 1\. Information parameters (or information science parameters) were also used.

The object of study was the Central Bank of Brazil. The focus of the study was the activity of off-site supervision, responsible for the monitoring of the Brazilian financial system. The professional group considered in the research was composed of the decision-making group (eight people) and other professionals linked to the off-site supervision tasks (forty-seven people).

The data collection was conducted according to the following group of procedures: information needs mapping followed Le Coadic's ([1998](#lecoadics) )propositions; and information competencies mapping followed the procedures suggested by Zarifian ([2003](#zarifian03)). A combination of research techniques was used, making possible a slowly deepening understanding of the supervisors' sense-making as information users and of the development of their competencies. The research was conducted in four successive and interactive phases, ending up in a final theoretical construction:

1.  in the first phase, document research was used. The objective was to define the activity to be studied; to characterize the organization and the organizational unit that executed the tasks and to define the population and the professional group of the research;
2.  in the second phase, interviewing was the chosen technique. People in decision-making positions were consulted to identify the Critical Success Factors of the activity. They were those people whose position would allow them to have information about the organization, its strategy and the essential factors that have influence on its operational aspects. The method tries to identify the characteristics, conditions, or variables that should be managed and properly monitored. These factors could be used to define information needs and the essential characteristics of an activity or organization. The technique, proposed by Rockart ([1979](#rockart79)), contemplates interviews in two or three separate sessions: in the first session, the executives' goals are listed and the success factors identified are discussed; a second session is held to review the information generated and a third session is held to obtain a final result;
3.  in the third phase of the research, participant observation was used to map processes, tasks, problems and problematic situations, documents and information frequently used, information products and services, information technology used in the environment, factors that would drive people to search for information, knowledge and abilities developed in daily tasks and professionals' attitudes to the workplace. The influence of the organizational structure and hierarchy was also observed in the professionals' daily work life;
4.  in the last phase, for evaluation of all the information obtained and final data triangulation, focus groups were used. It was possible to compare all the data and information gathered during the entire research process and to confirm the researchers' analyses _vis-à-vis_ the opinion of the professionals studied.

The data recording techniques were the researcher's field notes and a tape recorder. The instruments of data collection were six question discussion tables, prepared in different phases of the research and the instrument 'Completing the sentences'. Content analysis was used to analyse the interviews and the field notes (thematic analysis by category) and discourse analysis was used to analyse the instrument 'completing the sentences'.

## Data analysis, results and discussion.

### General results.

Data collection has generated several sets of information besides the identification of the critical success factors and work processes developed: the information process, cycle and contexts; the sense-making process, decision processes and knowledge construction processes. All this information was analysed to describe how the professionals identify their information needs and develop their competencies in the work environment.

Off-site supervision of financial institutions is related to the collection, organization and distribution of value-added information about the Brazilian financial system. Today, the chosen operational strategy is to monitor the risks incurred by the financial institutions through specific work processes, with specific groups of highly specialized professionals, with very advanced technological tools. There is the belief that, in this way, the safety and soundness of the financial system will be assured. Thus, each type of risk is detailed and specified. The complexity of the problems is reduced, in a way and the safety and extension of the monitoring process is increased. Extensive use of information technology makes it possible for analysts to concentrate on the problems detected by the tools for each type of risk. Automated processes transfer information and basic knowledge of _supervision technology_ to tools based on computer programs. The phases of the information cycle (collection, selection, treatment and storage) are automated and common explicit knowledge in each process is provided as a basis for analysis.

The main products of the activity are reports that express the analysis of the information collected and information products for decision-making. Most of the products and services are destined for internal customers. The customers of the off-site supervision are on-site supervision (who can have previous reports before driving to an institution to inspect it), the decision making people (who can lean on consolidated and timely information to make decisions and formulate policies), other organizational units interested in the available information products and services, and the external public interested in value-added information about the Brazilian financial system. The tools used by off-site supervision are information systems designed to answer to necessary data collection and analysis on the formats and types needed. The methodologies, the criteria and the indicators used in the supervisors' analyses are built specifically for the monitoring they perform.

### Critical success factors of off-site supervision.

The identified success factors for off-site supervision are listed in Table 1, grouped under three main identified themes: activity, information and people.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">**  
Table 1 - The Critical Success Factors of off-site supervision in Brazil.** (Source, [Miranda 2007](#miranda07))</caption>

<tbody>

<tr>

<th>Themes</th>

<th>Critical Success Factors</th>

</tr>

<tr>

<td rowspan="6">**Activity**</td>

<td>Capacity for aggregation, integration and comparison.</td>

</tr>

<tr>

<td>Capacity for permanent and concomitant monitoring of the financial system as a whole.</td>

</tr>

<tr>

<td>Capacity for optimization and rationalization.</td>

</tr>

<tr>

<td>Capacity for synergy and communication with other areas of the organization.</td>

</tr>

<tr>

<td>To possess legal and regulated bases for action.</td>

</tr>

<tr>

<td>Capacity to retain trained people (human resources).</td>

</tr>

<tr>

<td> </td>

<td> </td>

</tr>

<tr>

<td rowspan="4">**Information**</td>

<td>To acquire and supply quality, timely information.</td>

</tr>

<tr>

<td>Integrated, accessible information, from autonomous sources and available by security levels.</td>

</tr>

<tr>

<td>Appropriate technological basis.</td>

</tr>

<tr>

<td>Visible products and services.</td>

</tr>

<tr>

<td> </td>

<td> </td>

</tr>

<tr>

<td rowspan="4">**People**</td>

<td>Right qualifications and profile and adapted training.</td>

</tr>

<tr>

<td>Motivation.</td>

</tr>

<tr>

<td>Capacity for creativity and innovation.</td>

</tr>

<tr>

<td>Capacity for communication and cooperation.</td>

</tr>

</tbody>

</table>

The critical resources to accomplish the activity goals involve the capacity to generate value added information (aggregated, integrated and comparative) to monitor the financial institutions constantly and soundly. The optimization of resources (technology and personnel) and rationalization of work processes are also important. Off-site supervision effectiveness depends on the synergy, cooperation and constant communication with other areas of the organization and with their information suppliers. The information resources coming from external sources need to be verified for truthfulness and reliability. An appropriate technological basis is essential because they have to work with huge amounts of information, diverse in format, technology and contents.

It is operationally critical to have an appropriate technological basis to work with the information in the necessary format and quality. Without the appropriate technology it would be impossible to accomplish the monitoring in a satisfactory way. The activity imposes certain situational factors that have a strong influence on professional needs. Given the profile of the activity and the intensive use of information and information technology, the professional profile is marked by the diversity and specificity of the necessary information to accomplish the tasks and for the use of specialized information systems in every work process.

### Work processes and information management process.

One of the first findings of the research was the perception that the activity would be better understood from the point of view of its work processes. Participant observation was used to consider the work processes. Five different processes were observed. They were developed for the monitoring of different types of risks faced by the financial system. Each process presented specific information needs, related to the type of the monitoring accomplished, indicating also some specific competencies to satisfy those needs. The activity was new to the organizational environment and in the area of supervision and it was still being structured and improved. Each process had a different level of development and also a different level of information technology use. The work processes were analysed with regard to the information technology and the information context. A general and schematic representation of an off-site supervision work process is presented in Figure 2.

<div align="center">![Figure 2](p344fig2.gif)</div>

<div align="center">  
**Figure 2: Information cycle, contexts and information technology in work processes.** (Source, [Miranda 2007](#miranda07))</div>

The first process followed (Process 1) examines the financial institution's balance sheets to monitor accounting information systematically and continuously. The outputs of this process are complemented by the outputs of Process 2 and Process 3\. They check the consistency between accounting information and other sources of economical and financial data to verify the situation of each institution regarding the risks involved. The other two work processes, Process 4 and Process 5, constantly monitor financial institutions' operations drawing scenarios and stress curves. The processes use the information supplied by financial institutions and by alternative sources, trying to verify the consistency of all the existent sets of information. This allows the supervisor to draw behaviour scenarios based on market parameters.

The elaboration of tools aims to include business rules in computer programs and to automate tasks, bringing speed, safety and less mechanical work to people. In this way, the supervisor can concentrate him/herself on the very heart of risk analysis and on a proper solution to the detected problems. Nonetheless, the knowledge, the methodologies, the indicators and the criteria included or inserted into the tools continue to be necessary to the analyst. Knowledge of the tools, their possibilities and limits is an important part of the job profile.

The definition of information needs in off-site supervision is established from top to bottom. Starting from the interpretation of the external environment, an outside line of sense-making is generated throughout the organization and the activity. Based on the institutional mission, an internal interpretation is also generated, leading to commitments to a determined direction. The work processes represent the action. This path allows the supervisors to make sense of their activities. The current meaning is that by controlling several types of risk to financial activity, one can assure the stability of the financial system. The intensive assistance of the new information technologies was another side of the path chosen for action. This sense making construction leads to the critical factors the supervisors choose to transform the strategy into operational processes. A representation of the three arenas of information use in off-site supervision is presented in Figure 3\.

<div align="center">![Figure 3](p344fig3.gif)</div>

<div align="center">**Figure 3: Sense-making, knowledge construction and decision-making** (Adapted from [Choo 2003](#choo03))</div>

The information is managed in the environment of off-site supervision in relation to the supervisors' mental model and the belief in a certain meaning of risk control on the financial activity. From the process of identifying the necessary information to its use, the process begins with the reduction of environmental ambiguity and uncertainty through the design of information systems. Cognitive gaps are reduced, to a degree, through the form in which the information is organized. In other words, cognitive gaps are narrowed in agreement with the intellectual supervision technology (rules, criteria etc.). The specific information needs of the supervisors emerge from the problems linked to the risk monitoring. The intellectual technology of supervision is used to drive and to specify the situation or problem that needs solving. In this way, specialized knowledge is created for a certain purpose, making possible more concentrated, concrete and directed decision-making. A representation of the information management model for off-site supervision is presented in Figure 4.

<div align="center">![Figure 4](p344fig4.gif)</div>

<div align="center">  
**Figure 4: Process model of information management** (Adapted from [Choo 2003](#choo03))</div>

### The information needs identified.

To identify information needs, one must discover how the users choose, formulate and express their basic questions (problems or subjects) regarding their activity. This is important to describe how their knowledge systems work (cognitively, emotionally and socially).

Off-site supervision activity is divided according to the types of risks to monitor. The supervisors' main issues are linked to the analysis of the problems identified with the assistance of their tools. The main point is the risk that each situation identified represents for the financial institution and for the financial system. The tools are endowed with methodologies, criteria and indicators for treating the available information and present a problem to the supervisor. To answer each question in each process, the supervisors search for additional information in: organizational information systems, media reports (internet, intranet, newspapers, specialized news etc.) and perhaps try to communicate with other colleagues or other areas of the organization to clarify problems and exchange ideas. The goal is to solve the problem and decide on the alert sign to be sent to the head of the unit, managing director and/or to the on-site supervision, according to the case.

The supervisor's information needs were identified in three dimensions (situational, emotional, and cognitive factors) and with the help of three parameters (cycle and technology of information and information contexts). As for the **cognitive factors** related to the information cycle, it was found that part of the information necessary to the supervisors was transferred to the work tools. The strategy was to automate everything that was possible. Methodologies, indicators, criteria and previous knowledge for the final analysis were included in computer programs. The information cycle was automated from collecting and organizing to storing. The retrieval and analysis was the analyst's main job. Distribution of the analysis results was also an automated step. This does not mean that the supervisors do not need diversified knowledge and, at the same time, a specialized profile. The automation saves time regarding searching and organizing the basic material for analysis, offers agility and makes timeliness possible. In spite of this, the supervisors are not excused from the basic knowledge about the business. The more experienced and knowledgeable the analyst, the better the analysis. As for the use of information technology, the supervisors are intensively linked to the electronic channels and the use of the several resources offered by information and communication technologies. The intense use of technology brings, in addition, new knowledge requirements related to the very use of the technology. As for the use of the informational contexts, the supervisors are usually within the context of business information.

As for the **situational factors** linked to the information cycle, the analysts receive defined information according to established rules relating to needs, access levels and functions. Actually, the analysts receive previous indications from work tools. This way, they can analyse the extension and importance of the problems, in agreement with established criteria for the work processes. These criteria are based on previous knowledge and experience accumulated from and by the supervisory activity itself. Drawing from this knowledge background, the analyst adds the information available on the systems and decides whether to alert the on-site supervisor or other customers.

As for the use of the information contexts, the supervisor decides on the use of different types of information according to the problem to solve. Usually, the analyst uses the information supplied by the systems and joins what is available on the intranet or internet. He looks for additional information about the businesses of the financial institutions. Technological and organizational information are usually already included in tools. The technology includes workstations (computers), automated information systems and communication systems integrated with the information systems. Most of the work is accomplished with computer assistance, telephones and electronic communication systems. The documents used are mostly organizational documents (texts and tables) such as reports, work notes, regulations and balance sheets. The information medium is almost always electronic (computer files) containing text or numeric tables.

As for the **emotional factors** linked to the work with the information cycle, it was found that the automation of a great part of the cycle has reduced, in a way, the uncertainty and the stress. The current technology brings the possibility of using several information contexts simply by making them available. It was an up-grade from the previous situation, since before the modern technologies this possibility was quite small. With the use of intranet and Internet, for instance, the use of several information contexts became possible. The preferences for electronic media can be explained because they are faster and more diversified, reducing the stress of the tasks. The problem is that they also increase the standardization and repetition of certain tasks, affecting the motivation to work.

Considering all the factors together—speed, greater diversity of information, increased analysis resources and greater safety in the processes—the affective dimension seems to have its importance softened (at least within the factors considered in the research model). Due to the intervening variables and the activating mechanisms in the environment, the financial system and, therefore, the environment of the supervision of financial institutions is characteristically dynamic. So, the activity as a whole is marked by uncertainty and stress. Fortunately, that factor is manageable since most of the variables were already considered in the methodologies, tools and indicators that capture the environmental information. It is necessary to have an appropriate professional profile for the job. However, there are some occasional problems regarding the recruitment, selection and retention of personnel. This can occasionally be a stress factor.

As for the role played by the professionals, the history of the off-site supervision activity adds one stress factor to the management problem. The fight for space in the organizational arena adds one emotional factor to the off-site supervisors' situation. The interviews revealed that this activity was not considered a supervisory activity in a strict way within the organization. This situation could be explained, partially, because the division between off-site and on-site supervisors happened only in the year 2000, when the two activities were separated. The off-site supervisors' organizational situation and the recognition of their importance and role, did not seem to have yet been perfectly established. This variable could be controlled if off-site supervision was better defined as a specific function and had its role clearly established inside the organization.

As activation mechanisms, the risk/reward perception differentiates the choice of additional sources of information to provide the decision to send (or not) an alert on possible problems. The concept of effectiveness was based on the return obtained by the off-site supervisors through the results of their work. This could be a stress factor if the return was not offered and the supervisor did not get the evaluation of his/her effectiveness. Moreover, the evaluation obtained could be used to improve future job performance, if the information sent was used to address future analysis and decisions.

From the point of view of the work groups joined by work processes, it can be added that differences of concepts and perceptions existed among groups. They were characterized by differences in the tasks and by the development and automation levels of the work processes. The information need is clearer and more specific in more developed processes (more stabilized in terms of technologies, tools and methodologies). In processes that are still in development, most appropriate technologies, methodologies and tools are still also in development. In consequence, the information need was masked for different kinds of needs like those related to the development of the work process itself.

Some important points could be stressed regarding the chosen operational strategy for the activity, concerning the intensive use of the information technology and the predominance of internal information. The choice of a technological path of development should perhaps be part of a shorter period plan. Future plans for renewal and continuous development should be included to attend to work processes and staff development. In this way, an excessive use of automation would not make the work uninteresting and/or demotivating. As for the predominant use of internal information sources, this is a factor that can generate hermetic information behaviour, following the analysis presented by [Chatman (1999)](#chatman). This kind of behaviour could perpetuate accepted practices and sources and/or establish barriers to change.

### The competencies identified.

What differentiates off-site supervision from other similar activities is the use of large amounts of diversified information on an integrated basis and an ongoing monitoring of the financial system as a whole. On-site supervision, for instance, is a similar and complementary activity that monitors the financial institutions individually, deeply and in a programmed and punctual timeline. Therefore, the core competency of off-site supervision was described as the capacity to use aggregated and integrated information to accomplish ongoing systematic monitoring of the financial system. Together, these two activities accomplish a larger goal, that is, supervision of the Brazilian financial system. Table 2 presents the competencies for each mapped work process of off-site supervision, described as the expected behaviour in the workplace.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2 - The competencies that attend the information needs of off-site supervisors in Brazil.** (Source, [Miranda 2007](#miranda07)).</caption>

<tbody>

<tr>

<th>Work Process</th>

<th>Problems that drive the information needs  
in each work process</th>

<th>Description of the competency  
that could address the information needs</th>

</tr>

<tr>

<td>Work Process 1</td>

<td>What is the significance and relevance of an asset or liability variation on a specific financial institution?</td>

<td>Capacity to analyse economic and financial situation of financial institution to detect significant and relevant variations in the evolution of their assets and liabilities.</td>

</tr>

<tr>

<td>Work Process 2</td>

<td>What is the coherence between the accounts records of a financial institution and the operations reported by the market?</td>

<td>Capacity to analyse the consistency of assets and liabilities of one financial institution and the record of its operations in the market.</td>

</tr>

<tr>

<td>Work Process 3</td>

<td>What is the associated risk to the estimated need of liquidity of a financial institution (for itself and as a future tendency)?</td>

<td>Capacity to evaluate the liquidity situation of a financial institution to pinpoint irregularities and future tendencies.</td>

</tr>

<tr>

<td>Work Process 4</td>

<td>Why a given operation accomplished by some financial institution (identified for the work tool) was not executed obeying the negotiation parameters of the market?</td>

<td>Capacity to analyse the atypical operations of one financial institution on the market of Government Bonds to point irregularities and future tendencies.</td>

</tr>

<tr>

<td>Work Process 5</td>

<td>What is the risk situation of a given financial institution regarding the whole risk of its market?</td>

<td>Capacity to analyse and attribute quantitative notes that classify a financial institution regarding the risks in its market.</td>

</tr>

</tbody>

</table>

The analysis of the competency dimensions was made starting from the work processes considering the information parameters. In the knowledge dimension of the information cycle, it was found that some automated tasks allowed the supervisors to concentrate their knowledge on their main task: risk monitoring. As the cycle was largely automated, the knowledge related to the information sources, for instance, can be concentrated in complementary information sources. The basic sources are already included on the work tools. On the other hand, the knowledge that encompasses the supervision technology includes, nowadays, the learning of information technology tools used in the work processes. As for information contexts, the experience in the activity provides knowledge of a larger and diversified number of sources and usable types of information to solve problems. Previous knowledge of the very existence of these contexts and/or their possibilities is what influences most of the behaviour of supervisors. As for information technology, the supervisor should acknowledge that the tools, methodologies, indicators and criteria used in the activity must be well known. It does not matter if they are included in the automated systems or are related to the final risk analysis, which is a human activity. It is all part of the job profile.

In the know-how (abilities) dimension of the information cycle, it is important for off-site supervisors to possess the ability to use the functionalities included in their tools and to evaluate the effectiveness of their results. Those abilities can only be acquired with the experience of using the tools, methodologies, indicators and criteria. In this way, the know-how would be part of the daily work life. The supervisor should understand the information as raw material and learn to work with the media, formats, contents, types, sources and technologies. As for the use of the information contexts, the know-how is linked to the activating mechanisms and the intervening variables. They drive the information use from certain contexts. The information types used to solve problems are linked to the situational requirements of each work process: for example, analysis type, type and size of financial institution and type of information product to be offered. As for the use of information technology, the supervisor's ability is acquired in agreement with the work process in which s/he works. Each work process has its own computerized tools, methodologies, criteria and specific indicators related to the needs of the process.

In the know-how-to-act (attitudes) dimension, the work with the information cycle is linked to the supervisors' preferences and to the level of development of the work process. The usual preference is electronic media. If the process is not sufficiently developed and/or automated, the preferences are driven to printed information sources not available in the information systems, to information held by people and to the choice of new methodologies. This can reduce the use of certain types of information. The more automated the process, the more diverse are the supervisor's options for additional sources. The work experience, the knowledge of the importance of the job and the atmosphere of cooperation and friendship of the group are also influences on supervisors' attitudes. The use of different information contexts is part of the daily practice in the measure of: the supervisors' previous knowledge about them and the need for different contexts to solve specific problems. Each work process demands the use of certain information that can be considered according to preferences and professional experiences. As for the use of information technology, there is an understanding that the off-site supervisors should adapt themselves to the intensive use of information technology. The activity is intrinsically linked to the technology. The competencies linked specifically to the information process are described in Table 3.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3 - Information competencies of the off-site supervisors in Brazil and their dimensions.** (Source, Miranda 2007).</caption>

<tbody>

<tr>

<th>Behaviour</th>

<th>Knowledge</th>

<th>Abilities</th>

<th>Attitudes</th>

</tr>

<tr>

<td valign="top">It is recognized that the information is a raw material for work and the information analysis must be in agreement with a specific technology.</td>

<td valign="top">Knowledge about information contexts and their peculiarities and about the possibilities of application for the available information technology.</td>

<td valign="top">Development of the capacity to deal with the existent mass of documents and to identify new information needs.</td>

<td valign="top">Availability to work with the information in the extensive model of communication.</td>

</tr>

<tr>

<td valign="top">The information is organized according to the objectives of the tasks, which are elaborate information products and services.</td>

<td valign="top">Knowledge about the technology of off-site supervision and of its connection with the information technology.</td>

<td valign="top">Development of the ability to transform the technology of the off-site supervision with the use of information technology tools. </td>

<td valign="top">Disposition to learn and to diversify knowledge and to intermediate between the amount of documents and their usage possibilities.</td>

</tr>

<tr>

<td valign="top">Several information contexts are used to complement the preliminary information analysis accomplished by the work tool.</td>

<td valign="top">Knowledge about the available information systems and of their possibilities.</td>

<td valign="top">Development of the ability to recognize new information needs on new contexts.</td>

<td valign="top">Disposition to exchange information and knowledge.</td>

</tr>

<tr>

<td valign="top">Information technology is used to gain effectiveness in daily information work.</td>

<td valign="top">Knowledge about the possibilities to use information technology for supervision.  </td>

<td valign="top">Development of the ability to negotiate spaces of information technology use at the environment.</td>

<td valign="top">Disposition to be inquisitive and open to the possibilities of change and improvement of the existent technological level.</td>

</tr>

</tbody>

</table>

The observation of the work processes allowed the evaluation of information competencies in daily practice, given that those competencies are important regarding to the very success of the daily tasks. It is of fundamental importance for the off-site supervisor's profile: the conscious use of information as raw material and the certainty that appropriate and efficient organization of the information leads to efficiency and effectiveness of work processes. The belief that the use of information technology is indispensable for the effectiveness of the tasks on the scale requested by the Brazilian financial system is also a basic aspect of that profile.

An important point regarding the competencies concerns information orientation. Assuming a perspective of the predominance of the technological path, maladjustment in the harmonic development of the competencies dimensions can occur. The competency is related to people and their development as a whole. The emphasis on technology and automation can provoke deviations (distortions) in the individual competencies and, maybe, a future rigidity regarding the development and maintenance of the core competence of the activity. According to Leonard-Barton ([1995](#leonard)), a competency can be transformed into a rigidity and establish as a barrier to the future development of the organization. Another interesting point to be stressed is the contradiction between the need for a diversified professional profile and the _industrial_ use of information technologies (leading only to the automation of processes). This can contribute not only to a lack of work motivation but also, as emphasized by Ehrlinger and Dunning ([2003](#ehrlinger)), hinder the development of necessary meta-cognitive abilities for self-evaluation regarding the competency.

## Conclusions.

The purpose of this research was to study how information needs and information competencies of off-site supervisors of financial institutions in Brazil could be identified using information science parameters. The data collection provided information to identify how these professionals see their work needs and how they search and use information to solve problems. It was also possible to identify how they develop the competencies related to their need for information in the work environment.

The analysis of critical success factors of off-site supervision and the observation of the work processes performed by its professionals confirmed that the activity is information intensive and is based on information technologies and on the information cycle (collecting, processing, distributing and using information).

The importance of analysing the activity from the point of view of information parameters was stressed. As used in the research models, information parameters provided important insights to the better understanding of the research subject and problem. As a result of this approach, the suggested connection that could be established between the critical success factors of off-site supervision, the dimensions of the supervisors' information needs and their competencies is expressed in Figure 5\.

<div align="center">![Figure 5](p344fig5.gif)</div>

<div align="center">  
**Figure 5: Critical success factors (CSF) of off-site supervision (OSS) and their link to  
information needs (IN) and competencies (IC)** (Source, [Miranda 2007](#miranda07))</div>

Information, one of the success factors, is marked by cognitive aspects that depend on the available information and on the capacity to work with the available mass of documents. The cognitive aspect of information need is linked to the information aspects of the activity, or to the knowledge necessary to accomplish information tasks. This suggests that the development of the competencies is related to the use of the information and information technology. The axis established by the information theme could be considered the information orientation axis. It associates the efforts to manage information and information technology at the same time.

Activity, as a success factors theme, means the level of the technological development and it is related to situational aspects of information needs. The abilities developed by off-site supervisors are linked to their work situations (the work processes, the type of information they use and the information technology). The Activity axis has a structural power over the off-site supervision professional's life. Situational characteristics cause stress in the activity and in the professional life of those involved. They guide the perception of the information needs and the development of competencies at the workplace.

People, as a critical success factor theme, are related to the affective dimension of information needs (know-how-to-act) as a way to define the information culture that should be part of the off-site supervision profile. The development of information competency, however, is linked to people's interaction and common tasks that depend on the background of each professional engaged in the activity. The people axis could have professional awareness as building material. This indicates that off-site supervisors could have information professionals as a model. Off-site supervisors and information professionals have similar characteristics that could be outlined to help their development. For instance, an information professional task is the intermediation between a great mass of data and information and its use.

In conclusion, off-site supervisors' information needs and competencies are closely linked to the specific needs of their work processes, situational factors, tasks, routines and technologies. In addition, the role played at work defines a group of needs, as proposed by [Wilson (1981)](#wilson81). These needs could only be attended through a specific professional profile, developed in daily work practice. In this profile, the competencies to deal with information are important items. As [Le Boterf (2006)](#leboterf06) remarked, the study of competencies must not rest only on its mapping. It has to offer some referential for future development. Figure 6 presents some referential guidelines for the perception of the off-site supervisors' needs as much as for the development of their competencies, especially information competencies.

<div align="center">![Figure 6](p344fig6.gif)</div>

<div align="center">  
**Figure 6: Information needs and competencies** (Source, [Miranda 2007](#miranda07))</div>

## Acknowledgements

This paper is based on doctoral research funded by the Central Bank of Brazil. The research was conducted at the University of Brasília-Brazil and supervised by Prof. Dr. Kira Tarapanoff. The results, analysis and conclusions do not represent an official position of the funding institution.

## References

*   <a id="arias" name="arias"></a>Arias, R.D. (1993). _Aspectos de la supervisión financiera_. [Aspects of financial supervision]. Ciudad de Mexico: Centro de Estudios Monetarios Lationamericanos.
*   <a id="brasil03" name="brasil03"></a>Banco Central do Brasil. _Diretoria de Fiscalização._ (2003). _[Relatório de atividades. 1995-2002.](http://www.webcitation.org/5YY8EQjLm)_. [Report on activities 1995-2002]. Brasília: Banco Central do Brasil, January. Retrieved 13 June, 2008 from http://www.bcb.gov.br/ftp/defis/RelAtiv8/Defis_Relatorio_Atividades.pdf (Archived by WebCite® at http://www.webcitation.org/5YY8EQjLm)
*   <a id="brasil93" name="brasil93"></a>Banco Central do Brasil. (1993) _O Banco Central do Brasil e a fiscalização de instituições financeiras_. [The Central Bank of Brazil and the supervision of financial institutions]. Brasília: Banco Central do Brasil.
*   <a id="basle" name="basle"></a>Basle Committee on Banking Supervision. (1999). _[Core principles methodology](http://www.webcitation.org/5YYGBAb8n)_. Retrieved 13 June, 2008 from http://www.bis.org/publ/bcbs61.pdf (Archived by WebCite® at http://www.webcitation.org/5YYGBAb8n)
*   <a id="bastos" name="bastos"></a>Bastos, A.V.B. (2004). Cognição nas organizações de Trabalho. [Cognition in organizational work]. In José C. Zanelli, Jairo E.B. Andreade and Antonio V.B. Bastos (Eds.), _Psicologia, organizações e trabalho no brasil_ (pp.177-206). Porto Alegre, Brazil: Artmed.
*   <a id="belkin80" name="belkin80"></a>Belkin, N.J. (1980). Anomalous states of knowledge as a basis for information retrieval. _The Canadian Journal of Information Science_, **5**(1), 133-143.
*   <a id="belkin82" name="belkin82"></a>Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982). ASK for information retrieval: Part I. Background and theory. _Journal of Documentation_, **38**(2), 61-71.
*   <a id="brandao99" name="brandao99"></a>Brandão, H.P. (1999). _Gestão baseada nas competências: um estudo sobre competências profissionais na indústria bancária._ [Competency based management: a study of professional competency in the banking industry]. Unpublished Master's dissertation, Universidade de Brasília, Brazil.
*   <a id="brandao01" name="brandao01"></a>Brandão, H.P. & Guimarães, T.A. (2001). Gestão de competências e gestão de desempenho: tecnologias distintas ou instrumentos de um mesmo construto? [Competency management and performance management: distinct technologies or similarly constructed instruments?]. _RAE - Revista de Administração de Empresas_, **41**(1), 8-12.
*   <a id="carbone" name="carbone"></a>Carbone, P.P. _et al._ (2005). _Gestão por competências e gestão do conhecimento_. [Competency management and knowledge management]. Rio de Janeiro, Brazil: Editora FGV.
*   <a id="chatman" name="chatman"></a>Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Society for Information Science_, **50**(4) 207-217.
*   <a id="choo03" name="choo03"></a>Choo, C.W. (2003). _A Organização do conhecimento_. [The knowing organization]. Trans. Eliana Rocha. São Paulo, Brazil: Editora Senac São Paulo.
*   <a id="choo06" name="choo06"></a>Choo, C.W. (2006). _The knowing organization_. New York, NY: Oxford University Press.
*   <a id="daft" name="daft"></a>Daft, R.L. & Lengel, R.H. (1996). Information richness: a new approach to managerial behavior and organization design. In Ethel Auster & Chun Wei Choo,. (Eds.) _Managing information for the competitive edge._ (pp.167-216). New York, NY: Neal-Schuman
*   <a id="dervin" name="dervin"></a>Dervin, B. (1998). Sense-making theory and practice: an overview of user interests in knowledge seeking and use. _Journal of Knowledge Management_, **2**(2), 36-46.
*   <a id="dutra" name="dutra"></a>Dutra, J.S. (Ed.) (2001). _Gestão por competências_. [Competency management]. São Paulo, Brazil: Editora Gente.
*   <a id="ehrlinger" name="ehrlinger"></a>Ehrlinger, J. & Dunning, D. (2003). How chronic self-views influence (and potentially mislead) estimates of performance. _Journal of Personality and Social Psychology_, **84**(1), 5-17.
*   <a id="fineman" name="fineman"></a>Fineman, S. (2001). A Emoção e o processo de organizar. [Emotion in organizational processes]. In Stewart R. Clegg, Cynthia Hardy & Walter R. Nord (Eds.) _Handbook de Estudos Organizacionais_. _Vol. 2_ (pp. 157-189). São Paulo, Brazil: Atlas.
*   <a id="fleury01" name="fleury01"></a>Fleury, M.L.T. & Fleury, A.C. (2001). _Estratégias empresariais e formação de competências: um quebra-cabeça caleidoscópico da indústria brasileira_. [Business strategies and competency development: a kaleidoscopic puzzle of Brazilian industry]. (2nd ed.). São Paulo, Brazil: Atlas.
*   <a id="fleury04" name="fleury04"></a>Fleury, M.L.T. & Fleury, A.C. (2004). Alinhando estratégia e competências. [Aligning strategy and competency]. _Rae - Revista de Administração de Empresas,_ **44**(1), 44-57.
*   <a id="gondim" name="gondim"></a>Gondim, S.M.G. & Siqueira, M.M.M. (2004). Emoções e afetos no trabalho. [Emotion and affection at work]. In José C. Zanelli, Jairo E.B. Andreade and Antonio V.B. Bastos (Eds.), _Psicologia, organizações e trabalho no brasil_ (pp.177-206). Porto Alegre, Brazil: Artmed.
*   <a id="gonzalez" name="gonzalez"></a>González Rey, F.L. (2005). _Pesquisa qualitativa e subjetividade._ [Qualitative research and subjectivity]. São Paulo, Brazil: Pioneira Thomson Learning.
*   <a id="guyot" name="guyot"></a>Guyot, B. (2000). [Quelques problématiques pour éclairer l'étude de l'information dans les organizations.](http://www.webcitation.org/5YYHZcG4C) [Some questions to clarify the study of information in organizations]. _Sciences de la Société_, (50-51), 129-148\. Retrieved 13 June, 2008 from http://archivesic.ccsd.cnrs.fr/docs/00/06/20/78/PDF/sic_00000097.pdf (Archived by WebCite® at http://www.webcitation.org/5YYHZcG4C)
*   <a id="hessen03" name="hessen03"></a>Hessen, J. (2003). _Teoria do conhecimento_. [Theory of knowledge]. (2nd ed.) São Paulo, Brazil: Martins Fontes.
*   <a id="imf" name="imf"></a>International Monetary Fund. (1998). _[Toward a framework for financial stability](http://www.webcitation.org/5YYI0q2P2)_. Washington, DC: International Monetary Fund. Retrieved 13 June, 2008 from http://www.imf.org/external/pubs/ft/wefs/toward/. (Archived by WebCite® at http://www.webcitation.org/5YYI0q2P2)
*   <a id="kuhlthau91" name="kuhlthau91"></a>Kuhlthau, C.C. (1991). Inside the search process. _Journal of the American Society for Information Science_, **42**(5), 361-371\.
*   <a id="kuhlthau93" name="kuhlthau93"></a>Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation_, **49**(4), 339-355.
*   <a id="lacombe" name="lacombe"></a>Lacombe, F. (2004). _Dicionário de administração._ [Dictionary of management]. São Paulo, Brazil: Saraiva.
*   <a id="leboterf03" name="leboterf03"></a>Le Boterf, G. (2003). _Desenvolvendo a competência dos pProfissionais_. [Developing professional competency]. (3rd ed.) Porto Alegre, Brazil: Artmed.
*   <a id="leboterf06" name="leboterf06"></a>Le Boterf, G. (2006). _Construire le compétences individuelles et collectives._ [The construction of individual and collective competencies.] 4th ed. Paris: Éditions d'Organizations.
*   <a id="lecoadics" name="lecoadics"></a>Le Coadic, Y.F. (1998). _Le besoin d'information._ [Information need]. Paris: ADBS Editions.
*   <a id="leonard" name="leonard"></a>Leonard-Barton, D. (1995). _Wellsprings of knowledge: building and sustaining the sources of innovation_. Boston, MA: Harvard Business School Press.
*   <a id="march" name="march"></a>March, J.G. (1994). _A primer on decision making: how decisions happen_. New York, NY: The Free Press.
*   <a id="miranda04" name="miranda04"></a>Miranda, S.V. (2004). Identificando competências informacionais. [Identifying information competencies]. _Ciência da Informação_, **33**(2) 112-122.
*   <a id="miranda07" name="miranda07"></a>Miranda, S.V. (2007). Identificação de necessidades de informação e sua relação com competências informacionais; o caso da supervisão indireta de instituições financeiras no Brasil. [Information needs and their relationship to information competencies: the off-site supervision of financial institutions in Brazil case study]. Unpublished doctoral thesis, Universidade de Brasília, Brazil.
*   <a id="moura" name="moura"></a>Moura, M.G. (2002). Agências regulatórias no Brasil: os casos dos setores de tele-comunicações, eletricidade e petróleo/gás natural. [Regulatory agencies in Brazil: the cases of the telecommunications, electricity and petroleum/natural gas sectors]. _Revista do Serviço Público_, **53**(2), 81-115.
*   <a id="prahalad" name="prahalad"></a>Prahalad, J.K. & Hamel, G. (1990). The core competence of the corporation. _Harvard Business Review,_ **68**(3), 79-91.
*   <a id="rocha" name="rocha"></a>Rocha, M.E.A. (1999). _Discursos de abertura. Seminário sobre a função da supervisão bancária_. [Opening speeches, Seminar on the function of banking supervision.] Praia, Cape Verde: Banco Central de Cabo Verde.
*   <a id="rockart79" name="rockart79"></a>Rockart, J.F. (1979). Chief executives define their own data needs. _Harvard Business Review_, **57**(2), 81-93.
*   <a id="stroobants" name="stroobants"></a>Stroobants, M. (2004). A visibilidade das competências. [The visibility of competencies]. In F. Ropé & L. Tanguy, (Eds.) _Saberes e competências: o uso de tais noções na escola e na empresa._ (5th ed.).(pp. 135-166). Campinas, Brazil: Papirus.
*   <a id="taylor" name="taylor"></a>Taylor, R.S. (1986). _Value-added processes in information systems_. Norwood: Ablex Publishing.
*   <a id="varela" name="varela"></a>Varela, F. (1991). _Conhecer: as ciências cognitivas, tendências e perspectivas_. [To know: the cognitive sciences, trends and perspectives]. Lisbon: Instituto Piaget.
*   <a id="weick" name="weick"></a>Weick, K.E. (2001). _Making sense of the organization_. Oxford: Blackwell.
*   <a id="wilson81" name="wilson81"></a>Wilson, T.D. (1981). [On user studies and information needs.](http://www.webcitation.org/5YYKG0B5A) _Journal of Librarianship_, **37**(1), 3-15\. Retrieved 13 June, 2008 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite® at http://www.webcitation.org/5YYKG0B5A)
*   <a id="wilson99" name="wilson99"></a>Wilson, T.D. (1999). Models in information behavior research. _Journal of Documentation_, **55**(3), 249-270\. Retrieved 13 June, 2008 from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite® at http://www.webcitation.org/5YYJxV1LO)
*   <a id="wilson00" name="wilson00"></a>Wilson, T.D. (2000). [Human information behavior.](http://www.webcitation.org/5YYJpSSLE) _Informing Science_, **3**(2), 49-55\. Retrieved 13 June, 2008 from http://inform.nu/Articles/Vol3/v3n2p49-56.pdf. (Archived by WebCite® at (Archived by WebCite® at http://www.webcitation.org/5YYJpSSLE)
*   <a id="wilson02" name="wilson02"></a>Wilson, T.D. (2002) _[Alfred Schutz, phenomenology and research methodology for information behavior research](http://www.webcitation.org/5YYJHrLbz)_. _New Review of Information Behaviour Research_, **3**, 71-81\. Retrieved 13 June, 2008 from http://informationr.net/tdw/publ/papers/schutz02.html (Archived by WebCite® at http://www.webcitation.org/5YYJHrLbz)
*   <a id="wilson96" name="wilson96"></a>Wilson, T.D. & Walsh, C. (1996). _[Information behaviour: an inter-disciplinary perspective. Report to British Library Research and Innovation Centre.](http://www.webcitation.org/5YYK9sZSE)_ London: British Library Board. (Report 10) Retrieved 13 June, 2008 from http://informationr.net/tdw/publ/infbehav/ (Archived by WebCite® at http://www.webcitation.org/5YYK9sZSE).
*   <a id="zarifian01" name="zarifian01"></a>Zarifian, P. (2001) _Objetivo competência: por uma nova lógica_. [Objective competency: towards a new logic]. São Paulo, Brazil: Atlas.
*   <a id="zarifian03" name="zarifian03"></a>Zarifian, P. (2003). _O modelo da competência: trajetória histórica, desafios atuais e propostas._ [The competency model: historical trajectory, current challenges and proposals.] São Paulo, Brazil: Editora Senac São Paulo.
*   <a id="zarifian04" name="zarifian04"></a>Zarifian, P. (2004). _Le modèle de la compétence_. [Competency model] (2nd ed.) Paris: Éditions Liaisons.

