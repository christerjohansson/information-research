<header>

#### vol. 18 no. 1, March, 2013

</header>

<article>

# Multi-dimensional analysis of dynamic human information interaction

#### [Minsoo Park](#author)  
Korea Institute of Science and Technology Information, Seoul, Korea  

#### Abstract

> **Introduction.** This study aims to understand the interactions of perception, effort, emotion, time and performance during the performance of multiple information tasks using Web information technologies.  
> **Method.** Twenty volunteers from a university participated in this study. Questionnaires were used to obtain general background information and examine the research problem.  
> **Analysis.** The quantitative data were analysed using the Statistical Package for the Social Sciences (SPSS).  
> **Results.** The results of this study revealed that those with higher cognitive effort experienced more stress and frustration and higher temporal demand, resulting in lower performance, even though they spent more time finishing the information task. A high degree of temporal demand and negative emotional state led people to perform the task poorly even though they invested more mental effort on the task. Our feelings and emotions play a key role in our ability to deal with a situation where multiple information tasks need to be done within a certain time limit.  
> **Conclusions.** The results of this study can be employed as a theoretical foundation for designing human-friendly, adaptive user interfaces, which function as intelligent and affective central mechanisms and help users prioritise, monitor and coordinate their needs/tasks/goals effectively and efficiently. This study introduces the emotional factor, which is a newly emerging dimension, in dynamic information seeking and retrieval contexts and enlightens the existing areas of human information interaction.

<section>

## Introduction

Our adaptation occurs from our ability to process information and modify our information behaviour accordingly ([Morgan 2002](#mor2002)). Since the advent of the Web, humans have lived in dynamic, volatile digital information environments. Due to the dynamism and complexity of the Web information environments, people are getting more involved in multiple information task behaviours ([Waller 1997](#wal1997)). Multiple task performance is an important human behaviour that allows people to manage complex situations by handling more than one task in an effective, coordinated way ([Burgess 2000](#bur2000); [Carlson and Sohn 2000](#car2000); [Lee and Taatgen 2002](#lee2002)).

The Compact Oxford English Dictionary (2005) defines multiple task performance as the ability to perform concurrent tasks by interleaving. In cognitive science, the concept of multiple task performance is more complicated than _interleaving_ tasks in a multiple task sense. The complex situations people face often demand further mental activities, such as prioritisations and planning ([Burgess 2000](#bur2000)).

Multiple task performance has been important in the research of cognitive science, engineering psychology, human computer interaction, and human factors (e.g., [Damos 1991](#dam1991); [Treisman 1960](#tre1960); [Wickens 1989](#wic1989)). Early studies in the literature of multiple task performance in cognitive science focused mainly on dichotic listening skills (e.g., [Broadbent 1958](#bro1958); [Treisman 1960](#tre1960)), during which people were asked to listen to two simultaneous messages and shadow one. Later research has focused on both cognitive and motor task performance (e.g., [Wickens 1989](#wic1989)). According to Wickens researchers in multiple task performance include both task characteristics and coordination processes.

While some tasks can be easily performed concurrently, others compete for cognitive capacities ([Wickens 2002](#wic2002); [Wickens _et al_. 2003](#wic2003)) and as a consequence, people need to efficiently prioritise and coordinate their tasks with appropriate efforts to accomplish each task successfully. The way in which task prioritisations, task characteristics, and mental effort (cognitive resources) are connected has not yet been explored in information seeking and retrieval contexts. Research on multiple information task interaction in humans in such contexts is necessary for a greater understanding of human information behaviour in dynamic and complex information environments.

Recent studies show that people often perform multiple information tasks while using Web information retrieval (IR) technologies and looking for information through more than one information task over multiple search episodes ([Spink 2004](#spi2004); [Spink _et al._ 2002](#spi2002)). In an exploratory study of human multiple information task behaviour, Spink _et al._ ([2006](#spikos2006)) found that information task prioritising processes were influenced by the following factors: level of interest, level of knowledge, perceived level of information available on the Web, level of difficulty, level of importance, and information seeking from general information problems to specific ones.

Multiple task performance has been an important area of study in cognitive science, engineering psychology, human computer interaction, and human factors, but human multiple information task interaction during information seeking and retrieval processes in the Web environment is under-explored.

The overall goal of this study is to explore the relationships of task demand, mental effort, affective state, temporal demand, and performance in the processes of prioritising and coordinating multiple information tasks in the contexts of seeking and retrieving information on the Web.

## Literature review

Different theoretical approaches to attention suggest that psychologists are far from agreeing on how to explain the attentional phenomena. Just _et al_. ([2001](#jus2001)) defined attention as limited cognitive capacity that can be distributed over tasks, such as in divided attention tasks. Kahneman ([1973](#kah1973)) viewed attention as a set of cognitive processes for categorizing and recognizing stimuli; the more complex the stimulus, the harder the processing, and therefore the more resources are engaged. Despite the different theoretical approaches, attention has been shown to a flexible aspect of cognition ([Kahneman 1973](#kah1973)). We see that attention, rather than being rigidly and mechanically limited, is instead a more flexible system, affected by factors such as the complexities of tasks and the person’s intention.

In Treisman’s ([1960](#tre1960), [1964](#tre1964)) attenuation theory, irrelevant messages (or less important messages) are turned-down through the multi-stage processing (physical/sensory processing, linguistic processing, and semantic/meaning processing), leaving more attention for important information. The turned-down messages are still partially accessible and monitored occasionally. Some messages have low thresholds and are easily processed, e.g., names, danger signals. Unattended information is still available, just less accessible. It requires little constraint on what gets through.

Work such as that by Johnston and Heinz ([1978](#joh1978)) has led many to use new metaphors when explaining attention. For instance, some compare attention to a spotlight that highlights whatever information the system is currently focused on ([Johnson and Dark 1986](#joh1986)). Accordingly, psychologists are now interested less in determining what information cannot be processed than exploring what kinds of information people choose to focus on.

In multimode theory ([Johnston and Heinz 1978](#joh1978)), attention is assumed to be flexible in that attended and non-attended information can be differentiated at different depths of perceptual analysis. Multimode theory assumes that the more processing needed, the greater the capacity and mental effort required, and the later the selection, the harder the task. Free attentional capacity decreases the later the selection occurs.

Kahneman ([1973](#kah1973)) presented a slightly different model to explain attention. He viewed attention as a set of cognitive processes for categorising and recognising stimuli. The more complex the stimulus, the harder the processing, and therefore the more resources are engaged. However, according to Kahneman, people have some control over where they direct their mental resources: They can often choose what to focus on and devote their mental effort to. Kahneman’s ([1973](#kah1973)) model of attention indicates how attention can be considered as a flexible system affected by several factors but has a limitation in explaining how an individual’s enduring dispositions and momentary intentions affect the processes of resource allocation. Essentially, this model suggests that we pay more attention to things we are interested in or have judged important.

Everyday observations tell us that the more one concentrates, the better one performs. Many researchers have employed the notion of mental effort (cognitive resources) as a hypothetical construct to explain performance differences ([Bandura 1982](#ban1982); [Salomon 1981](#sal1981), [1983](#sal1983); [Salomon and Leigh 1984](#sallei1984)).

Bobrow and Collins ([1975: 145](#bob1975)) pointed out:

> Data which were either deemed as important or could not easily be accounted for would receive sufficient processing effort and as a result, they would probably be remembered later. Moreover, we suspect that they would receive conscious attention at the time of their arrival and processing. Thus, data which are expected or otherwise, readily accounted for will be remembered.

To explain the essence of what is meant by constructs such as depth of processing and cognitive capacity, Salomon ([1981](#sal1981), [1983](#sal1983)) used the construct of amount of invested mental effort, which is defined as '_the number of non-automatic mental elaborations applied to a unit of material_'. ([Salomon 1984: 648](#sal1984)) This concept is based on Kahneman’s ([1973](#kah1973)) attention theory, which assumes that one has a pool of available mental effort (cognitive capacity) that can be allocated to tasks and conscious information processes demand mental effort and therefore tap the pool of cognitive resources.

According to Salomon, the amount of invested mental effort indicates cognitive and motivational attributes:

> It is cognitive in the sense that it pertains to mental elaborations of information material. But as these elaborations are controlled, rather than automatic, their employment implies a measure of choice, as all controlled activities do ([Steiner, 1979](#ste1979)). The exercise of choice, the preference of one alternative course of action over another, implies in turn the existence of motivation ([Brigham, 1979](#bri1979)). Non-automatic effort demanding elaborations are at one’s disposal; their actual employment is a matter of choice and motivation. ([Salomon 1983: 44](#sal1983))

Motivation, curiosity, anxiety, or arousal may all be necessary factors in producing greater invested mental effort ([Salomon 1981](#sal1981)). But according to Salomon ([1981](#sal1981)) the invested mental effort ultimately produces learning. High motivation, even when accompanied by comprehensive knowledge or skill, is not sufficient to produce high performance unless one actually invests mental effort in processing information ([Clark 1980](#cla1980)).

Langer, Blank, and Chanowitz ([1978](#lan1978)) showed that when information or stimuli are considered as highly familiar, people tend to respond to them mindlessly, thereby resulting in low performance. The perception toward task difficulty is also thought to influence the amount of effort expended ([Weiner 1985](#wei1985)). Weiner found that individuals seem to perform best at tasks of intermediate difficulty. When faced with tasks of intermediate difficulty, people invest more effort since these individuals believe that the best performance strategy for high achievement in such situations is to try harder. These studies suggest that invested mental effort may depend on the perceived demand characteristics of the stimulus, the task, or the context.

More demanding, difficult or novel stimuli are generally expected to evoke more effort investment than simple stimuli ([Salomon 1983](#sal1983)). But according to Salomon, the nature of stimuli, their complexity, novelty, and the like, in interaction with learners’ abilities, affect performance or learning outcomes only to some extent. Perceptions, in the sense of predispositions, preconceptions, attitudes, or attributions, also play an important role in the way one processes information.

In a series of studies ([Salomon 1983](#sal1983), [1984](#sal1984); [Salomon and Leigh 1984](#sallei1984)), Salomon addressed the question of how individuals’ perceptions of information categories relate to the amount of invested mental effort and performance. The findings of these studies indicate that differential perceptions of tasks are related to the amount of invested mental effort, which in turn is related to performance and learning. The results suggest that individuals’ performance may depend on what they perceive the tasks to be.

Taken as a whole, this line of research suggests that individuals’ perceived demand characteristics of tasks or contexts affect AIME. The more demanding PDC is, the more AIME would be expended. When people face a task they perceive to be easy relative to their abilities, they may invest less mental effort in its processing or performance. For example, a skilled driver or Web surfer may perceive the respective task of driving or Web surfing to be easy and thus rely more on automatic processes. This is due to knowing that no additional effort investments are needed to perform the task that is perceived to be well mastered ([Salomon 1984](#sal1984)).

Research in human information behaviour, shows that people often face multiple task situations in information contexts. When they have multiple information tasks or problems, they tend to batch those problems or tasks and try to solve them at once, often using information retrieval systems ([Spink 2004](#spi2004)). Studies indicate that users’ searches may have multiple goals or topics in the contexts of human information interaction ([Miwa 2001](#miw2001); [Spink 2004](#spi2004)).

The concept of _human information coordinating behaviour_ has been introduced in the studies of human multitasking information behaviour ([Spink _et al._ 2006](#spi2006); [Spink _et al._ 2002](#spi2002); [Spink _et al._ 2006](#spi2006)). The general meaning of coordinating is bringing the different elements of a complex activity into a relationship that will ensure efficiency (Compact Oxford English Dictionary 2005). Spink, Park, and Cole emphasise that:

> the concept of HICB [human information coordinating behaviour] is an important linking and sustaining process for a science of information that binds together the many HIB [human information behaviour] processes. The development of HIB necessitates a theoretical and empirical explication of the important nature and role of HIB’s, including HICB. In HICB, humans coordinate a number of elements, including their cognitive state, level of domain knowledge, and their understanding of their information problem, into a coherent series of activities that may include seeking, searching, interactive browsing, retrieving, and constructing information. A key process for HICB is to sustain these activities toward completion of some information goal or object. ([Spink _et al._ 2006: 150](#spi2006))

In the context of information access and use, task attributes have been investigated in numerous studies (e.g., [Byström and Järvelin 1995](#bys1995); [Culnan 1983](#cul1983); [Hart and Rice 1991](#har1991); [Tiamiyu 1992](#tia1992)). These studies show that task characteristics (e.g., task complexity) influence the way people seek and use information. Task complexity is multidimensional ([Campbell 1988](#cam1988)) and there are systematic interplays among task complexity, information types, information / communication channels, and sources ([Bystrom and Jarvelin 1995](#bys1995)).

### Theoretical model

From the analysis of the studies reviewed, a theoretical model of human prioritising and coordinating information behaviour in the contexts of information seeking and retrieval emerged.

Figure 1 depicts the process of prioritising and coordinating multiple information tasks in the contexts of information seeking and retrieval. The model is fundamentally based on the global single-channel hypothesis, which is all of the mechanisms between stimulus input and response output (stimulus perception, response selection, movement initiation) together constitute a single-channel and can be used by only one task at a time ([Craik 1948](#cra1948)).

In this model, differential perceptions of tasks initiate the process by influencing the dimensions of task prioritisations, cognitive effort, affective state, and temporal demand. All these components are in turn related to the level of task performance. Performance is then followed by evaluation. Interplay plays an important role in the processes of human prioritising and coordinating behaviour over time in the context of multiple information tasks.

In this study, the notion of multiple task performance is coming from an interdisciplinary perspective of human information interaction. The conceptual foundation of multiple task performance has been built on the studies of cognitive psychology, human factors, and educational psychology and it has been extended in the contexts of information seeking and retrieval in human information interaction. This exploratory study seeks a multi-dimensional understanding of human prioritising and coordinating information behaviour by investigating, in particular, the relationships of task demands, cognitive effort, affective state, temporal demand, and performance.

<figure>

![Theoretical Model of Human Prioritizing and Coordinating Information Behaviour ](../p566fig1.jpg)

<figcaption>

Figure 1\. Theoretical model of human prioritizing and coordinating information behaviour</figcaption>

</figure>

The next section details the methods used in this study.

## Research design

This study aims to understand the relationships of perception, effort, emotion, time, and performance when people manage multiple information tasks using Web information technologies.

### Data collection instrument

Data were collected from questionnaires completed before and after the task completion exercise. They were used to obtain general background information and examine the research problem. The pre-task questionnaire contained closed and open-ended questions. The post-task questionnaire was mainly made up of rating scales, applied to collect data on the levels of each of the four dimensions associated with assessment of the following variables: task demand (task dimension), mental effort (cognitive dimension), performance (behavioural dimension), temporal demand (temporal dimension) and psychological or affective level (psychological/affective dimension). Each dimension was defined as follows:

*   Task demand: the perceived levels of difficulty, importance, interest, familiarity or complexity of each task.
*   Mental effort: the experienced levels of cognitive demand required to accomplish each task.
*   Affective state: the experienced levels of emotion or feeling during the task.
*   Temporal demand: the experienced levels of time pressure because of the rate at which the tasks occurred.
*   Performance: the experienced levels of success or satisfaction

The post-task questionnaire was designed based on the _subjective workload assessment technique_ ([Reid and Nygren 1988](#rei1988)) with substantial modifications for the purpose of this study. Each scale in the pre and post questionnaires was presented as a 12-cm line with bipolar descriptors at each end (e.g., high or low, excellent or poor). Numerical values were not displayed on the rating scales. A questionnaire was chosen because it has the potential to collect cognitive data quickly and easily. Another advantage of a questionnaire is that the data may be both qualitative and quantitative, allowing them to play a part in both quantitative and qualitative studies ([Su 1991](#su1991)).

### Subject sample

The first step in choosing a sample is to decide who should be included in the population of interest ([Krathwohl 2004](#kra2004)). For this study, students and faculty members were the sampled population. The reason for choosing this population was that it was assumed that they regularly interacted with the Web for information or knowledge. Web information technologies have played an important role in electronic learning environments. Students and faculty regularly interact with information and information retrieval systems (e.g., the Web, databases, digital libraries) to solve their information problems and/or broaden their knowledge horizons in electronic learning environments.

Twenty volunteers with diverse academic backgrounds, including telecommunications, library and information science, environmental studies, health and community systems, sociology, nursing, and health information management, participated in this study. All students and faculty members at a university were asked to take part in the study. A notice of recruitment was distributed through emailing lists and notice boards. No demographic and disciplinary limits were set in order to minimise bias in the sampling process.

### Data collection

#### The tasks

After giving general instructions, the subjects were asked to create one information problem for the non-assigned (additional) task. They were then given general descriptions of three different information tasks in random order. It was the participants who decided on the entire process of their searching sessions, e.g., which task they were going to begin with. They had a maximum of one hour to finish their sessions regardless of the state of completion of all tasks. The subjects conducted one task at a time. The three assigned tasks were related to medicine, travel, and research. The descriptions of the assigned tasks were as follows:

*   Medicine: One of your family members has just been diagnosed with skin cancer, and you want to learn about the disease and the medical treatments available (e.g., currently existing and newly developed). You are also interested in how to protect yourself from the disease.
*   Travel: You and your best friend are planning to travel to a place, where you can enjoy one of your favorite sports. You are trying to figure out how to prepare for this adventure and what kinds of information you need.
*   Research: You are currently working on a term project. You still have enough time to finish it but you want to work hard on this one and get a good grade because you are really interested in the topic. Now, you are trying to find some good materials, which can provide some background information of the topic area you chose for the term project.

A task in this study was not simply a topic or an information search task. Each task entailed several activities such as problem solving and planning to pursue certain goals.

#### Research setting

The researcher conducted the study in a controlled environment. The research took place in a laboratory, which was equipped with Dell 3.06GHz Pentium PCs with 512 MB RAM and a 80 GB hard disk running on Windows XP. They also had Microsoft Office 2003 packages and several different kinds of Web browsers including the latest versions of Safari, Firefox, Netscape, and Internet Explorer.

### Data analysis

The analysed data included pre- and post-task questionnaires. The questionnaires were tabulated, compiled and analysed with computer program SPSS. Cross-tabulated tables provide '_information on the variation of responses with various demographic and other independent variables that can throw considerable light on the respondents’ underlying characteristics, value structures and thinking processes_' ([Krathwohl 2004: 372](#kra2004)).

## Results

### Demographic information

<table><caption>

Table 1\. Demographic data for the research subjects</caption>

<tbody>

<tr>

<th>Category</th>

<th>Sub-category</th>

<th>Frequency</th>

<th>Percentage (%)</th>

</tr>

<tr>

<td>Age</td>

<td>18-21</td>

<td>3</td>

<td>15%</td>

</tr>

<tr>

<td> </td>

<td>22-29</td>

<td>9</td>

<td>45%</td>

</tr>

<tr>

<td> </td>

<td>30-39</td>

<td>6</td>

<td>30%</td>

</tr>

<tr>

<td> </td>

<td>40+</td>

<td>2</td>

<td>10%</td>

</tr>

<tr>

<th> </th>

<th>Total</th>

<th>20</th>

<th>100%</th>

</tr>

<tr>

<td>Sex</td>

<td>Female</td>

<td>15</td>

<td>75%</td>

</tr>

<tr>

<td> </td>

<td>Male</td>

<td>5</td>

<td>25%</td>

</tr>

<tr>

<td> </td>

<th>Total</th>

<th>20</th>

<th>100%</th>

</tr>

<tr>

<td>Academic Status</td>

<td>Undergraduate</td>

<td>3</td>

<td>15%</td>

</tr>

<tr>

<td> </td>

<td>Master's</td>

<td>13</td>

<td>65%</td>

</tr>

<tr>

<td> </td>

<td>Doctoral</td>

<td>2</td>

<td>10%</td>

</tr>

<tr>

<td> </td>

<td>Professor</td>

<td>2</td>

<td>10%</td>

</tr>

<tr>

<th> </th>

<th>Total</th>

<th>20</th>

<th>100%</th>

</tr>

<tr>

<td>Academic Discipline</td>

<td>Social Sciences</td>

<td>12</td>

<td>60%</td>

</tr>

<tr>

<td> </td>

<td>Engineering</td>

<td>3</td>

<td>15%</td>

</tr>

<tr>

<td> </td>

<td>Health Sciences</td>

<td>4</td>

<td>20%</td>

</tr>

<tr>

<td> </td>

<td>Natural Sciences</td>

<td>1</td>

<td>5%</td>

</tr>

<tr>

<th> </th>

<th>Total</th>

<th>20</th>

<th>100%</th>

</tr>

</tbody>

</table>

The subjects in this study had varying backgrounds (see Table 1), including different sexes, academic status (e.g., faculty, doctoral students, master’s students, and undergraduate students), ages, and academic discipline areas (e.g., engineering, social sciences, natural sciences, and health sciences). All participants were regular and experienced Web users.

### Multidimensional measures on the tasks

This study was designed to gain a multi-dimensional understanding of the multiple information task behaviour in humans by analysing the dynamic interplays of perceived task attributes, mental efforts, affective states, temporal demands, and performances.

Table 2 describes the average scores of the multiple aspects associated with the multiple information task behaviour in humans in the context of information seeking and retrieval.

<table><caption>

Table 2\. Multidimensional measures on individual information tasks</caption>

<tbody>

<tr>

<th rowspan="2">Measured  
variable</th>

<th colspan="2">Medicine</th>

<th colspan="2">Travel</th>

<th colspan="2">Research</th>

<th colspan="2">Additional</th>

</tr>

<tr>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

<th>SD</th>

<th>Mean</th>

<th>SD</th>

</tr>

<tr>

<td>Difficulty</td>

<td>3.870</td>

<td>2.1258</td>

<td>4.465</td>

<td>1.9680</td>

<td>5.690</td>

<td>2.3657</td>

<td>3.870</td>

<td>1.8388</td>

</tr>

<tr>

<td>Importance</td>

<td>8.585</td>

<td>2.4692</td>

<td>5.365</td>

<td>2.6039</td>

<td>8.550</td>

<td>2.0075</td>

<td>5.965</td>

<td>3.3137</td>

</tr>

<tr>

<td>Interest</td>

<td>7.540</td>

<td>3.2047</td>

<td>6.585</td>

<td>3.3611</td>

<td>6.150</td>

<td>3.1081</td>

<td>8.580</td>

<td>2.6333</td>

</tr>

<tr>

<td>Knowledge or familiarity</td>

<td>4.815</td>

<td>3.1179</td>

<td>5.800</td>

<td>2.5381</td>

<td>6.455</td>

<td>2.6518</td>

<td>7.090</td>

<td>2.0619</td>

</tr>

<tr>

<td>Complexity</td>

<td>5.830</td>

<td>2.6474</td>

<td>5.315</td>

<td>2.5646</td>

<td>7.775</td>

<td>2.2148</td>

<td>4.950</td>

<td>3.0626</td>

</tr>

<tr>

<td>Mental effort</td>

<td>4.685</td>

<td>2.5236</td>

<td>4.715</td>

<td>2.2115</td>

<td>5.940</td>

<td>3.2419</td>

<td>4.090</td>

<td>2.8011</td>

</tr>

<tr>

<td>Affective state</td>

<td>3.025</td>

<td>2.6248</td>

<td>3.765</td>

<td>2.3647</td>

<td>4.925</td>

<td>3.2973</td>

<td>3.275</td>

<td>2.8988</td>

</tr>

<tr>

<td>Temporal demand</td>

<td>2.780</td>

<td>1.8171</td>

<td>4.185</td>

<td>2.7130</td>

<td>4.560</td>

<td>2.8840</td>

<td>3.205</td>

<td>2.3141</td>

</tr>

<tr>

<td>Performance (success)</td>

<td>9.010</td>

<td>2.0047</td>

<td>7.265</td>

<td>3.1174</td>

<td>7.900</td>

<td>2.7719</td>

<td>8.950</td>

<td>2.3332</td>

</tr>

<tr>

<td>Performance (satisfaction)</td>

<td>8.740</td>

<td>2.5124</td>

<td>7.275</td>

<td>3.4127</td>

<td>8.030</td>

<td>2.8603</td>

<td>8.700</td>

<td>2.6921</td>

</tr>

<tr>

<td>Duration</td>

<td>572.20</td>

<td>339.995</td>

<td>632.35</td>

<td>436.576</td>

<td>833.05</td>

<td>532.620</td>

<td>519.25</td>

<td>318.951</td>

</tr>

</tbody>

</table>

The results in Table 2 show that the medicine task was considered the least difficult and familiar with the mean scores with 3.8 and 4.8, respectively, and most important with a mean of 8.5\. The subjects felt the least emotional and temporal demands while working on the medicine task resulting in the highest levels of success and satisfaction regarding their performances.

The travel task was considered the least important (<span style="text-decoration:overline;">X</span>=5.3) with the lowest levels of overall success (<span style="text-decoration:overline;">X</span>=7.2) and satisfaction (<span style="text-decoration:overline;">X</span>=7.2). The additional task was evaluated as the least difficult (<span style="text-decoration:overline;">X</span>=3.8) and complex (<span style="text-decoration:overline;">X</span>=4.9). The subjects also thought that the additional task was most interesting (<span style="text-decoration:overline;">X</span>=8.5) and familiar (<span style="text-decoration:overline;">X</span>=7.0), requiring the least level (<span style="text-decoration:overline;">X</span>=4.0) of cognitive processing to perform the task. The subjects spent the least amount of time in conducting the additional task with a mean duration of 519 seconds.

The research task was considered most difficult (<span style="text-decoration:overline;">X</span>=5.6) and complex (<span style="text-decoration:overline;">X</span>=7.7), and the least interesting (<span style="text-decoration:overline;">X</span>=6.1). It seemed that the subjects invested a high level of mental effort (<span style="text-decoration:overline;">X</span>=5.9). It was noticed that the levels of emotional state and temporal demand were highly evaluated while the subjects were working on the research task. They also seemed to spend the longest amount of time in finishing the research task (833 sec.).

The additional task was considered most interesting (<span style="text-decoration:overline;">X</span>=8.5) and familiar (<span style="text-decoration:overline;">X</span>=7.0) and the least difficult (<span style="text-decoration:overline;">X</span>=3.8) and complex (<span style="text-decoration:overline;">X</span>=4.9). It was noticed that the subjects invested less time (519 sec.) and mental effort (<span style="text-decoration:overline;">X</span>=4.0) on the additional task than the other tasks.

### Correlation measures on the tasks

These data were analysed further to see if there were any statistically significant correlations among the variables. The results of this analysis are discussed in the following sections.

#### Correlation measures: medicine task

</section>

<table><caption>

Table 3\. Medicine task correlation matrix</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th>Difficulty</th>

<th>Importance</th>

<th>Interest</th>

<th>Knowledge or  
Familiarity</th>

<th>Complexity</th>

<th>Mental  
effort</th>

<th>Affective  
state</th>

<th>Temporal  
demand</th>

<th>Performance  
(success)</th>

<th>Performance  
(satisfaction)</th>

<th>Duration</th>

</tr>

<tr>

<td>Difficulty</td>

<td>Pearson 'r'</td>

<td>1</td>

<td>-0.267</td>

<td>-0.078</td>

<td>-0.446*</td>

<td>0.400</td>

<td>0.517*</td>

<td>0.456*</td>

<td>0.211</td>

<td>-0.300</td>

<td>-0.230</td>

<td>-0.059</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.255</td>

<td>0.743</td>

<td>0.049</td>

<td>0.081</td>

<td>0.020</td>

<td>0.043</td>

<td>0.371</td>

<td>0.199</td>

<td>0.329</td>

<td>0.806</td>

</tr>

<tr>

<td>Importance</td>

<td>Pearson 'r'</td>

<td> </td>

<td>1</td>

<td>0.876**</td>

<td>0.280</td>

<td>0.236</td>

<td>-0.265</td>

<td>-0.394</td>

<td>-0.226</td>

<td>0.529*</td>

<td>0.334</td>

<td>0.097</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td>.000</td>

<td>.231</td>

<td>.317</td>

<td>.259</td>

<td>.086</td>

<td>.337</td>

<td>.017</td>

<td>.150</td>

<td>.683</td>

</tr>

<tr>

<td>Interest</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td>1</td>

<td>.336</td>

<td>.300</td>

<td>-.095</td>

<td>-.088</td>

<td>.118</td>

<td>.524*</td>

<td>.365</td>

<td>.231</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td>.147</td>

<td>.198</td>

<td>.689</td>

<td>.711</td>

<td>.621</td>

<td>.018</td>

<td>.114</td>

<td>.326</td>

</tr>

<tr>

<td>Knowledge / Familiarity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>.157</td>

<td>-.412</td>

<td>-.113</td>

<td>-.060</td>

<td>.642**</td>

<td>.558*</td>

<td>.112</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>.509</td>

<td>.071</td>

<td>.635</td>

<td>.803</td>

<td>.002</td>

<td>.011</td>

<td>.637</td>

</tr>

<tr>

<td>Complexity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.149</td>

<td>0.046</td>

<td>-0.054</td>

<td>0.070</td>

<td>0.063</td>

<td>-0.043</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.531</td>

<td>0.848</td>

<td>0.821</td>

<td>0.771</td>

<td>0.792</td>

<td>0.858</td>

</tr>

<tr>

<td>Mental effort</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.639**</td>

<td>0.449*</td>

<td>-0.446*</td>

<td>-0.532*</td>

<td>0.360</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.002</td>

<td>0.047</td>

<td>0.049</td>

<td>0.016</td>

<td>0.119</td>

</tr>

<tr>

<td>Affective State</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.532*</td>

<td>-0.472*</td>

<td>-0.473*</td>

<td>0.331</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.016</td>

<td>0.035</td>

<td>0.035</td>

<td>0.154</td>

</tr>

<tr>

<td>Temporal demand</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.119</td>

<td>-0.070</td>

<td>0.315</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.617</td>

<td>0.769</td>

<td>0.176</td>

</tr>

<tr>

<td>Performance (success)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.925**</td>

<td>0.020</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.935</td>

</tr>

<tr>

<td>Performance (satisfaction)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.147</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.536</td>

</tr>

<tr>

<td>Duration</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

</tbody>

</table>

<section>

Table 3 presents the results regarding the correlations of the variables on the medicine task. It was found that among the task attributes, difficulty was negatively associated with knowledge or familiarity (r = -0.446) and importance was positively correlated to interest (r=0.876). Task difficulty had a positive correlation with the degree of mental effort (r=0.517) and affective state (r=0.456). In terms of the behavioural dimension, the degree of success was positively associated with several task attributes including task importance (r=0.529), interest (r=0.524), and knowledge or familiarity (r=0.642). The level of satisfaction was correlated with one task attribute, knowledge or familiarity (r=0.558). In the cognitive dimension, it was noticed that several meaningful relationships existed between mental effort and other variables, including affective state (r=0.639) and temporal demand (r=0.449). Interestingly, the degrees of success (r=-0.446) and satisfaction (r=-0.532) were negatively associated with the level of cognitive processing. It was found that a positive relationship existed between affective state and temporal demand (r=0.532). The emotional degree was negatively associated with the levels of success (r=-0.472) and satisfaction (r=-0.473). A very strong relationship (r=0.925) existed between success and satisfaction.

#### Correlation measures: travel task

</section>

<table><caption>

Table 4\. Travel task correlation matrix</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th>Difficulty</th>

<th>Importance</th>

<th>Interest</th>

<th>Knowledge or  
Familiarity</th>

<th>Complexity</th>

<th>Mental  
effort</th>

<th>Affective  
state</th>

<th>Temporal  
demand</th>

<th>Performance  
(success)</th>

<th>Performance  
(satisfaction)</th>

<th>Duration</th>

</tr>

<tr>

<td>Difficulty</td>

<td>Pearson 'r'†</td>

<td>1</td>

<td>0.151</td>

<td>-0.021</td>

<td>0.089</td>

<td>0.261</td>

<td>-0.026</td>

<td>0.233</td>

<td>-0.116</td>

<td>-0.160</td>

<td>-0.177</td>

<td>0.290</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.525</td>

<td>0.930</td>

<td>0.710</td>

<td>0.266</td>

<td>0.912</td>

<td>0.323</td>

<td>0.626</td>

<td>0.501</td>

<td>0.455</td>

<td>0.215</td>

</tr>

<tr>

<td>Importance</td>

<td>Pearson 'r'</td>

<td> </td>

<td>1</td>

<td>0.612**</td>

<td>0.444*</td>

<td>0.310</td>

<td>-0.037</td>

<td>-0.246</td>

<td>-0.162</td>

<td>0.337</td>

<td>0.324</td>

<td>-0.027</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td>0.004</td>

<td>0.050</td>

<td>0.183</td>

<td>0.875</td>

<td>0.295</td>

<td>0.496</td>

<td>0.146</td>

<td>0.164</td>

<td>0.909</td>

</tr>

<tr>

<td>Interest</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.627**</td>

<td>0.394</td>

<td>0.216</td>

<td>-0.157</td>

<td>-0.168</td>

<td>0.148</td>

<td>0.231</td>

<td>-0.131</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.003</td>

<td>0.086</td>

<td>0.361</td>

<td>0.509</td>

<td>0.480</td>

<td>0.532</td>

<td>0.326</td>

<td>0.581</td>

</tr>

<tr>

<td>Knowledge / Familiarity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.121</td>

<td>0.162</td>

<td>-0.258</td>

<td>-0.315</td>

<td>0.185</td>

<td>0.277</td>

<td>-0.222</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.611</td>

<td>0.495</td>

<td>0.272</td>

<td>0.177</td>

<td>0.434</td>

<td>0.236</td>

<td>0.347</td>

</tr>

<tr>

<td>Complexity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.393</td>

<td>-0.281</td>

<td>-0.238</td>

<td>0.161</td>

<td>0.174</td>

<td>0.232</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.087</td>

<td>0.230</td>

<td>0.312</td>

<td>0.498</td>

<td>0.464</td>

<td>0.324</td>

</tr>

<tr>

<td>Mental effort</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.641**</td>

<td>0.458*</td>

<td>-0.501*</td>

<td>-0.347</td>

<td>0.115</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.002</td>

<td>0.042</td>

<td>0.024</td>

<td>0.134</td>

<td>0.631</td>

</tr>

<tr>

<td>Affective state</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.604**</td>

<td>-0.752**</td>

<td>-0.719**</td>

<td>0.292</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.005</td>

<td>0.000</td>

<td>0.000</td>

<td>0.211</td>

</tr>

<tr>

<td>Temporal demand</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.718**</td>

<td>-0.700**</td>

<td>0.122</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.001</td>

<td>0.609</td>

</tr>

<tr>

<td>Performance (success)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.967**</td>

<td>-0.397</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.083</td>

</tr>

<tr>

<td>Performance (satisfaction)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.371</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.107</td>

</tr>

<tr>

<td>Duration</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="13">

\* Correlation is significant at the 0.05 level (2-tailed)  
** Correlation is significant at the 0.01 level (2-tailed).</td>

</tr>

</tbody>

</table>

<section>

The results in Table 4 indicate that among the task attributes, task importance was positively associated with interest (r=0.612) and knowledge or familiarity (r=0.444). A positive relationship (r=0.627) was found between interest and knowledge or familiarity. It was noticed that mental effort was related to affective state (r=0.641), temporal demand (r=0.458), both positively, and success negatively (r=-0.501). It was found that there was a relationship (r=0.604) between affective state and temporal demand. The degree of emotional state was also negatively associated with overall performance, including success (r=-0.752) and satisfaction (r=-0.719). There was a strong relationship between temporal demand and performance: success (r=-0.718) and satisfaction (r=-0.700). In the travel task, it was also found that the level of success was strongly related to the level of satisfaction.

#### Correlation measures: research task

</section>

<table><caption>

Table 5\. Research task correlation matrix</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th>Difficulty</th>

<th>Importance</th>

<th>Interest</th>

<th>Knowledge or  
familiarity</th>

<th>Complexity</th>

<th>Mental  
effort</th>

<th>Affective  
state</th>

<th>Temporal  
demand</th>

<th>Performance  
(success)</th>

<th>Performance  
(satisfaction)</th>

<th>Duration</th>

</tr>

<tr>

<td>Difficulty</td>

<td>Pearson 'r'</td>

<td>1</td>

<td>-0.039</td>

<td>-0.284</td>

<td>-0.268</td>

<td>0.432</td>

<td>0.437</td>

<td>0.372</td>

<td>0.230</td>

<td>-0.414</td>

<td>-0.416</td>

<td>0.062</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.872</td>

<td>0.225</td>

<td>0.253</td>

<td>0.057</td>

<td>0.054</td>

<td>0.107</td>

<td>0.329</td>

<td>0.069</td>

<td>0.068</td>

<td>0.797</td>

</tr>

<tr>

<td>Importance</td>

<td>Pearson 'r'</td>

<td> </td>

<td>1</td>

<td>0.280</td>

<td>0.300</td>

<td>0.449*</td>

<td>0.005</td>

<td>-0.048</td>

<td>0.087</td>

<td>0.089</td>

<td>0.092</td>

<td>0.568**</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td>0.231</td>

<td>0.199</td>

<td>0.047</td>

<td>0.982</td>

<td>0.840</td>

<td>0.715</td>

<td>0.709</td>

<td>0.700</td>

<td>0.009</td>

</tr>

<tr>

<td>Interest</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.374</td>

<td>-0.112</td>

<td>-0.160</td>

<td>-0.273</td>

<td>-0.259</td>

<td>0.533*</td>

<td>0.595**</td>

<td>-0.078</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.104</td>

<td>0.638</td>

<td>0.500</td>

<td>0.245</td>

<td>0.270</td>

<td>0.015</td>

<td>0.006</td>

<td>0.743</td>

</tr>

<tr>

<td>Knowledge or familiarity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.023</td>

<td>0.046</td>

<td>-0.050</td>

<td>-0.294</td>

<td>0.238</td>

<td>0.284</td>

<td>-0.163</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.922</td>

<td>0.847</td>

<td>0.835</td>

<td>0.208</td>

<td>0.312</td>

<td>0.225</td>

<td>0.492</td>

</tr>

<tr>

<td>Complexity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.181</td>

<td>0.206</td>

<td>0.315</td>

<td>-0.294</td>

<td>-0.284</td>

<td>0.428</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.446</td>

<td>0.383</td>

<td>0.176</td>

<td>0.208</td>

<td>0.225</td>

<td>0.060</td>

</tr>

<tr>

<td>Mental effort</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.680**</td>

<td>0.669**</td>

<td>-0.638**</td>

<td>-0.615**</td>

<td>0.197</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.001</td>

<td>0.001</td>

<td>0.002</td>

<td>0.004</td>

<td>0.405</td>

</tr>

<tr>

<td>Affective state</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.509*</td>

<td>-0.751**</td>

<td>-0.682**</td>

<td>0.259</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.022</td>

<td>0.000</td>

<td>0.001</td>

<td>0.270</td>

</tr>

<tr>

<td>Temporal demand</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.563**</td>

<td>-0.596**</td>

<td>0.483*</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.010</td>

<td>0.006</td>

<td>0.031</td>

</tr>

<tr>

<td>Performance (success)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.941**</td>

<td>-0.233</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.323</td>

</tr>

<tr>

<td>Performance (satisfaction)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>-0.279</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.233</td>

</tr>

<tr>

<td>Duration</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="13">

\* Correlation is significant at the 0.05 level (2-tailed).  
** Correlation is significant at the 0.01 level (2-tailed)</td>

</tr>

</tbody>

</table>

<section>

Table 5 shows that among the task attributes, importance was correlated to complexity (r=0.449) and duration (r=0.568). There was a relationship between interest and performance: success (r=0.533) and satisfaction (r=0.595). It was noticed that mental effort was associated with several variables including affective state (r=0.680), temporal demand (r=0.669), success (r=-0.638), and satisfaction (r=-0.615). Affective state was related to temporal demand positively (r=0.509) and performance negatively (success, r=-0.751; satisfaction, r=-0.682). The degree of temporal demand was positively associated with task duration (r=0.483) and negatively related to both success (r=-0.563) and satisfaction (r=-0.596). A noticeable relationship (r=0.941) existed between success and satisfaction.

#### Correlation measures: additional task

</section>

<table><caption>

Table 6\. Additional task correlation matrix</caption>

<tbody>

<tr>

<th colspan="2"> </th>

<th>Difficulty</th>

<th>Importance</th>

<th>Interest</th>

<th>Knowledge or  
familiarity</th>

<th>Complexity</th>

<th>Mental  
effort</th>

<th>Affective  
state</th>

<th>Temporal  
demand</th>

<th>Performance  
(success)</th>

<th>Performance  
(satisfaction)</th>

<th>Duration</th>

</tr>

<tr>

<td>Difficulty</td>

<td>Pearson 'r'</td>

<td>1</td>

<td>0.441</td>

<td>0.257</td>

<td>0.111</td>

<td>0.562**</td>

<td>0.332</td>

<td>0.288</td>

<td>0.398</td>

<td>-0.061</td>

<td>0.095</td>

<td>0.318</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td>0.051</td>

<td>0.275</td>

<td>0.641</td>

<td>0.010</td>

<td>0.152</td>

<td>0.218</td>

<td>0.082</td>

<td>0.798</td>

<td>0.691</td>

<td>0.171</td>

</tr>

<tr>

<td>Importance</td>

<td>Pearson 'r'</td>

<td> </td>

<td>1</td>

<td>0.465*</td>

<td>0.399</td>

<td>0.241</td>

<td>0.241</td>

<td>0.267</td>

<td>0.412</td>

<td>-0.020</td>

<td>0.116</td>

<td>0.306</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td>0.039</td>

<td>0.082</td>

<td>0.306</td>

<td>0.305</td>

<td>0.256</td>

<td>0.071</td>

<td>0.934</td>

<td>0.626</td>

<td>0.190</td>

</tr>

<tr>

<td>Interest</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.012</td>

<td>0.473*</td>

<td>0.263</td>

<td>0.279</td>

<td>0.152</td>

<td>-0.146</td>

<td>-0.108</td>

<td>0.482*</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td>0.960</td>

<td>0.035</td>

<td>0.262</td>

<td>0.233</td>

<td>0.523</td>

<td>0.539</td>

<td>0.650</td>

<td>0.031</td>

</tr>

<tr>

<td>Knowledge or familiarity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.052</td>

<td>0.025</td>

<td>0.124</td>

<td>0.139</td>

<td>0.279</td>

<td>0.363</td>

<td>0.086</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.828</td>

<td>0.918</td>

<td>0.602</td>

<td>0.559</td>

<td>0.234</td>

<td>0.116</td>

<td>0.720</td>

</tr>

<tr>

<td>Complexity</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.014</td>

<td>-0.075</td>

<td>0.234</td>

<td>0.123</td>

<td>0.260</td>

<td>0.116</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.952</td>

<td>0.753</td>

<td>0.321</td>

<td>0.605</td>

<td>0.268</td>

<td>0.625</td>

</tr>

<tr>

<td>Mental effort</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.737**</td>

<td>0.388</td>

<td>-0.548*</td>

<td>-0.448*</td>

<td>0.580**</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.091</td>

<td>0.012</td>

<td>0.048</td>

<td>0.007</td>

</tr>

<tr>

<td>Affective state</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.153</td>

<td>-0.611**</td>

<td>-0.530*</td>

<td>0.240</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.521</td>

<td>0.004</td>

<td>0.016</td>

<td>0.309</td>

</tr>

<tr>

<td>Temporal demand</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.041</td>

<td>0.142</td>

<td>0.464*</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.864</td>

<td>0.552</td>

<td>0.039</td>

</tr>

<tr>

<td>Performance (success)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.943**</td>

<td>-0.070</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.000</td>

<td>0.770</td>

</tr>

<tr>

<td>Performance (satisfaction)</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

<td>0.058</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>0.809</td>

</tr>

<tr>

<td>Duration</td>

<td>Pearson 'r'</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1</td>

</tr>

<tr>

<td> </td>

<td>Sig. (2-tailed)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="13">

\* Correlation is significant at the 0.05 level (2-tailed).  
** Correlation is significant at the 0.01 level (2-tailed)</td>

</tr>

</tbody>

</table>

<section>

In Table 6, for the additional task, task complexity was associated with both difficulty (r=0.562) and interest (r=0.473). It was found that perceived task interest was related to importance (r=0.465) and time spent (r=0.482). It was noticed that the degree of mental effort was associated with several variables including affective state (r=0.737), performance (success, r=-0.548; satisfaction, r=-0.448), and duration (r=0.580). Affective state was also negatively related to performance (success, r=-0.611; satisfaction, r=-0.530). A relationship existed between the level of temporal demand and the time spent on the task (r=0.464). In the additional task, the level of success was also strongly related to the level of satisfaction (r=0.943).

## Discussion and conclusion

Multiple information task behaviour in humans consists of multiple components. The success of such activities depends on how people bring those different elements into a relationship that will ensure efficiency and harmony.

The correlations between the measures were examined at the significant levels of .01 and .05\. Table 7 shows the dynamic interplays of the components of human multiple information task interaction on the Web, employing a matrix to represent/visualise the numeric data in a meaningful way.

Statistically significant relationships exist among the perceived attributes of the information tasks. Interest is related with importance (r=0.88, medicine task; r=0.61, travel task; r=0.47, additional task). knowledge or familiarity is associated with both importance (r=0.44, travel task) and interest (r=0.63, travel task). At the same time, knowledge or familiarity is negatively correlated with difficulty (r=-0.45, medicine task). Complexity is associated with multiple task attributes, including difficulty (r=0.56, additional task), importance (r=0.45, research task), and interest (r=0.47, additional task). Interesting information tasks are considered important. When people determine that they have some experience or are familiar with a certain information task, they think the task is more important and interesting and less challenging. A complex information task is perceived as more difficult, important, and interesting.

Both mental effort (r=0.52, medicine task) and affective state (r=0.46, medicine task) are correlated with the perceived degree of task difficulty. Negative perceptions toward an information task (e.g., higher task difficulty) led people to try harder but this feeling counteracts and increases stress and frustration. This might be caused by low self-confidence in performing the task.

In terms of performance, task importance (r=0.53, medicine task), interest (r=0.52, medicine task; r=0.53, research task), and knowledge or familiarity (r=0.64, medicine task) are all significantly correlated with success. Among them, only task interest (r=0.60, research task) and knowledge or familiarity (r=0.56, medicine task) are significantly related to satisfaction. There is a significant correlation between duration and task importance (r=0.57, research task). Duration is also correlated with task interest (r=0.48, additional task). It seems that people perform better with higher satisfaction when faced with more important, interesting, and familiar information tasks. In addition, they tend to spend more time on interesting and important tasks to complete them.

It should be noticed that there are dynamic correlations among other measures, including mental effort, affective state, temporal demand, performance, and duration, for most of the information tasks.

Mental effort is positively associated with multiple measures, such as affective state (r=0.64, medicine task; r=0.64, travel task; r=0.68, research task; r=0.74, additional task), temporal demand (r=0.45, medicine task; r=0.46, travel task; r=0.67, research task), and duration (r=0.58, additional task). At the same time, the cognitive measure is negatively correlated with performance in terms of success (r=-0.45, medicine task; r=-0.50, travel task; r=-0.64, research task; r=-0.55, additional task) and satisfaction (r=-0.53, medicine task; r=-0.62, research task; r=-0.45, additional task).

Another measure, which is related to multiple dimensions, is affective state. It was found that a statistically significant relationship exists between affective state and temporal demand (r=0.53, medicine task; r=0.60, travel task; r=0.51, research task). Affective state is also negatively correlated with performance, with respect to success (r=-0.47, medicine task; r=-0.75, travel task; r=-0.75, research task; r=-0.61, additional task) and satisfaction (r=-0.47, medicine task; r=-0.72, travel task; r=-0.68, research task; r=-0.53, additional task).

A negative correlation exists between temporal demand and performance in terms of success (r=-0.72, travel task; r=-0.56, research task) and satisfaction (r=-0.70, travel task; r=-0.60, research task). In addition, temporal demand is positively correlated with duration (r=0.48, research task; r=0.46, additional task). There is a highly significant correlation between success and satisfaction (r=0.93, medicine task; r=0.97, travel task; r=0.94, research task; r=0.94, additional task).

<figure>

![Model revisited](../p566fig2.jpg)

<figcaption>Figure 2: Model revisited</figcaption>

</figure>

With the resulting data, the theoretical model of human prioritising and coordinating information behaviour was revisited. Figure 2 indicates those with higher cognitive effort experienced more stress and frustration and higher temporal demand, resulting in lower levels of performance, even though they spent more time finishing the information task. It seems that cognitive, emotional, temporal, and behavioural aspects are closely related to each other across the tasks. When people consider a task to be demanding (e.g., higher difficulty or complexity), they often become frustrated. In this case, even though they try harder to solve the problem for a longer time period, their negative emotional state prevents them from performing the task successfully. In other words, high degrees of temporal demand and a negative emotional state led people to perform poorly even though they invest more mental effort on the task. Our feelings and emotions play a key role in our ability to deal with a situation where multiple information tasks need to be done within time limits.

Humans have inhabited dynamic, volatile information environments since the advent of the Web. As Web information environments become more complex and dynamic, people often find themselves faced with multiple information tasks or goals while interacting with Web information technologies. Our adaptation occurs from our ability to process information effectively and adapt our information behaviour accordingly. Despite the values, current studies of human-information interaction have limitations in explaining our adaptive information behaviour in dynamic and complex Web information environments.

</section>

<table><caption>

Table 7\. Interactions of the components of human multiple information task behaviour</caption>

<tbody>

<tr>

<th> </th>

<th>Interest</th>

<th>Knowledge or  
familiarity</th>

<th>Complexity</th>

<th>Mental  
effort</th>

<th>Affective  
state</th>

<th>Temporal  
demand</th>

<th>Performance  
(success)</th>

<th>Performance  
(satisfaction)</th>

<th>Duration</th>

</tr>

<tr>

<td>Difficulty</td>

<td> </td>

<td>-.446*[MT]</td>

<td>.562*[AT]</td>

<td>.517*[MT]</td>

<td>.456*[MT]</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Importance</td>

<td>.876***[MT]  
.612**[TT]  
.465*[AT]  
</td>

<td>.444*[TT]</td>

<td>.449*[RT]</td>

<td> </td>

<td> </td>

<td> </td>

<td>.529*[MT]</td>

<td> </td>

<td>.568*[RT]</td>

</tr>

<tr>

<td>Interest</td>

<td> </td>

<td>.627**[TT]</td>

<td>.473*[AT]</td>

<td> </td>

<td> </td>

<td> </td>

<td>.524*[MT]  
.533*[RT]  
</td>

<td>.595*[RT]</td>

<td>.482*[AT]</td>

</tr>

<tr>

<td>Knowledge or familiarity</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>.642**[MT]</td>

<td>.558*[MT]</td>

<td> </td>

</tr>

<tr>

<td>Mental effort</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>.639**[MT]  
.641**[TT]  
.680**[RT]  
.737**[AT]</td>

<td>.449*[MT]  
.458*[TT]  
.669**[RT]</td>

<td>-.446*[MT]  
-.501*[TT]  
-.638**[RT]  
-.548*[AT]</td>

<td>-.532*[MT]  
-.615**[RT]  
-.448*[AT]</td>

<td>.580*[AT]</td>

</tr>

<tr>

<td>Affective state</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>.532*[MT]  
.604**[TT]  
.509*[RT]  
</td>

<td>-.472*[MT]  
-.752**[TT]  
-.751**[RT]  
-.611**[AT]</td>

<td>-.473*[MT]  
-.719**[TT]  
-.682**[RT]  
-.530*[AT]</td>

<td> </td>

</tr>

<tr>

<td>Temporal demand</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>-.718**[TT]  
-.563*[RT]</td>

<td>-.700**[TT]  
-.596*[RT]</td>

<td>.483*[RT]  
.464*[AT]</td>

</tr>

<tr>

<td>Performance (success)</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>.925***[MT]  
.967***[TT]  
.941***[RT]  
.943***[AT]</td>

<td> </td>

</tr>

<tr>

<td colspan="10">

Note 1: * means moderate correlation; ** strong correlation; *** very strong correlation  
Note 2: MT stands for Medicine task; TT, Travel task; RT, Research task; AT, Additional task</td>

</tr>

</tbody>

</table>

<section>

The understanding of human multiple information task behaviour can be utilized as a conceptual base to design information systems which support efficient and effective human-information and human-computer interactions in complex and dynamic information seeking and retrieval environments.

From the system design point of view, it would be advantageous for systems to support a user’s individuality to close the gaps in performance differences. Adaptive (also called personalised) information systems are designed to deal with the fact that users are individuals, taking into account individual features such as goals/tasks, knowledge, background, Web experience, preferences and interests ([Brusilovsky, 2001](#bru2001)). To build such systems to be human-friendly, system designers first need to understand the dynamic interplays of humans, information, tasks, and systems.

Researchers in the field of human-information interaction and human-computer interaction can incorporate the concept of human multiple task performance to design adaptive user interfaces. Creating effective user interfaces requires knowledge of how people actually deal with multiple information task situations physically, cognitively, and emotionally under dynamic and complex circumstances. The results of this study can be employed as a theoretical foundation for designing human-friendly, adaptive user interfaces, which function as intelligent and affective central mechanisms and help users prioritise, monitor and coordinate their needs/tasks/goals effectively and efficiently.

This study identifies the significance of the emotional state of the information seeker as a factor in dynamic information seeking and retrieval contexts and enlightens the existing areas of human information interaction. This exploratory study will contribute to the growth of knowledge in information science.

## <a id="author"></a>About the author

**Minsoo Park** is a senior researcher of Korea Institute of Science and Technology Information, Seoul, Korea. Her main areas of research interest are human-information interaction and human-computer interaction. She can be contacted at [mspark7@gmail.com.](mspark7@gmail.com)

</section>

<section>

## References

<ul>
    <li id="ban1982">Bandura, A. (1982). Self-efficacy mechanism in human agency. <em>American
            Psychologist</em>, <strong>37</strong>(2), 122-147
    </li>
    <li id="bob1975">Bobrow, D. G. &amp; Collins, A. (1975). <em>Representation and understanding: atudies
            in cognitive science</em>. New York, NY: Academic Press.
    </li>
    <li id="bri1979">Brigham, T.A. (1979). Some effect of choice academic performance. In L. C. Perlmuter
        &amp; R. A. Monty (Eds.), <em>Choice and perceived control</em>. Hillsdale, NJ: Erlbaum.
    </li>
    <li id="bro1958">Broadbent, D. E. (1958). <em>Perception and communication</em>. New York, NY: Pergamon
        Press.
    </li>
    <li id="bru2001">Brusilovsky, P. (2001). Adaptive hypermedia. <em>User Modeling and User-Adapted
            Interaction</em>, <strong>11</strong>(1), 87-110.
    </li>
    <li id="bur2000">Burgess, P. W. (2000). Real-world multitasking from a cognitive neuroscience
        perspective. In Monsell, S. &amp; Driver, J. (Eds.), <em>Control of cognitive processes: attention
            and performance XVIII</em> (pp. 465-472). Cambridge, MA: MIT Press.
    </li>
    <li id="bys1995">Byström, K. &amp; Järvelin, K. (1995). Task complexity affects information seeking and
        use. <em>Information Processing &amp; Management</em>, <strong>31</strong>(2), 191-213.
    </li>
    <li id="cam1988">Campbell, D.J. (1988). Task complexity: a review and analysis. <em>Academy of
            Management Review</em>, <strong>13</strong>(1), 40-52.
    </li>
    <li id="car2000">Carlson, R. A. &amp; Sohn, M.-Y. (2000). Cognitive control of multistep routines:
        <em>Information processing and conscious intentions. In S. Monsell &amp; J. Driver (Eds.), Control
            of cognitive processes: attention and performance XVIII</em> (pp. 443-464). Cambridge, MA: MIT
        Press.
    </li>
    <li id="cla1980">Clark, R.E. (1980). <em>Do students enjoy the instructional method from which they
            learn the least? Antagonism between enjoyment and achievement in ATI studies</em>. Paper
        presented at annual convention of the American Educational Research Association. Boston, April,
        1980.
    </li>
    <li id="cra1948">Craik, K.J.W. (1948). Theory of the human operator in control systems: II. Man as an
        element in a control system. <em>British Journal of Psychology</em>, <strong>38</strong>(3),
        142-148.
    </li>
    <li id="cul1983">Culnan, M.J. (1983). Environmental scanning: the effects of task complexity and source
        accessibility on information gathering behavior. <em>Decision Sciences</em>, <strong>14</strong>(2),
        194-206.
    </li>
    <li id="dam1991">Damos, D. L. (1991). <em>Multiple-task performance</em> . London: Taylor &amp; Francis.
    </li>
    <li id="har1991">Hart, P.J. &amp; Rice, R.E. (1991). Using information from external databases:
        contextual relationships of use, access method, task, database type, organization differences, and
        outcomes. <em>Information Processing &amp; Management</em> <strong>27</strong>(5), 461-479.
    </li>
    <li id="joh1986">Johnson, W. A. &amp; Dark, V. J. (1986). Selective attention. <em>Annual Review of
            Psychology</em>, <strong>37</strong>, 43-76.
    </li>
    <li id="joh1978">Johnston, W. A. &amp; Heinz, S. P. (1978). Flexibility and capacity demands of
        attention. <em>Journal of Experimental Psychology: General</em>, <strong>107</strong>(4), 420-435.
    </li>
    <li id="jus2001">Just, M.A., Carpenter, P.A., Keller, T.A., Emery, L., Zajac, H. &amp; Thulborn, K.R.
        (2001). Interdependence of non-overlapping cortical systems in dual cognitive tasks.
        <em>NeuroImage</em>, <strong>14</strong>(2), 417-426.
    </li>
    <li id="kah1973">Kahneman, D. (1973). <em>Attention and effort</em>. Englewood Cliffs, NJ: Prentice Hall
    </li>
    <li id="kra2004">Krathwohl, D. R. (2004). <em>Methods of educational and social science research: an
            integrated approach</em>. Long Grove, IL: Waveland Press.
    </li>
    <li id="lan1978">Langer, E., Blank, A. &amp; Chanowitz, B. (1978). The mindlessness of ostensibly
        thoughtful action: the role of ‘placebic’ information in interpersonal interaction. <em>Journal of
            Personality and Social Psychology</em>, <strong>36</strong>(6), 635-642.
    </li>
    <li id="lee2002">Lee, F. J. &amp; Taatgen, N. A. (2002). Multitasking as skill acquisition.
        <em>Proceedings of the 24th Annual Conference of the Cognitive Science Society</em> (pp. 572-577).
        Hillsdale, NJ: Lawrence Erlbaum Associates
    </li>
    <li id="miw2001">Miwa, M. (2001). User situations and multiple levels of users’ goals in information
        problem-solving processes of AskERIC users. <em>Proceedings of the Annual Meeting of the American
            Society for Information Sciences and Technology, <strong>38</strong></em>, 355-371.
    </li>
    <li id="mor2002">Morgan, D. L. (2002). <em>Essentials of learning and cognition</em>. Boston, MA:
        McGraw-Hill
    </li>
    <li id="rei1988">Reid, G. B. &amp; Nygren, T. E. (1988). The subjective workload assessment technique: A
        scaling procedure for measuring mental workload. In P. A. Hancock and N. Meshkati (Eds.), <em>Human
            mental workload</em> (pp. 185-218). Amsterdam: North-Holland.
    </li>
    <li id="sal1981">Salomon, G. (1981). Introducing AIME: The assessment of children’s mental involvement
        with television. In H. Kelly &amp; H. Gardner (Eds.), <em>New directions for child development:
            viewing children through television</em>. San Francisco, CA: Jossey-Bass.
    </li>
    <li id="sal1983">Salomon, G. (1983). The differential investment of mental effort in learning from
        different sources. <em>Educational psychologist</em>, <strong>18</strong>(1), 42-50
    </li>
    <li id="sal1984">Salomon, G. (1984). Television is “easy” and print is “tough”: the differential
        investment of mental effort in learning as a function of perceptions and attributions. <em>Journal
            of Educational Psychology</em>, <strong>75</strong> (4), 647-658
    </li>
    <li id="sallei1984">Salomon, G. &amp; Leigh, T. (1984). Predispositions about learning from print and
        television. <em>Journal of Communication</em>, <strong>34</strong>(2), 119-135
    </li>
    <li id="soa2005">Soanes, C. &amp; Hawker, S. (Eds.) (2005). <em>Compact Oxford English Dictionary. 3rd.
            edition.</em> Oxford: Oxford University Press.
    </li>
    <li id="spi2004">Spink, A. (2004). Everyday life multitasking information behavior: An exploratory
        study. <em>Journal of Documentation</em>, <strong>60</strong>(4), 336-345.
    </li>
    <li id="spi2002">Spink, A., Ozmutlu, H. C. &amp; Ozmutlu, S. (2002). Multitasking information seeking
        and searching processes. <em>Journal of the American Society for Information Science and
            Technology</em>, <strong>53</strong>(8), 639-652.
    </li>
    <li id="spi2006">Spink, A., Park, M. &amp; Cole, C. (2006). Multitasking and coordinating framework for
        human information behavior. In A. Spink &amp; C. Cole (Eds.), <em>New directions in human
            information behavior</em>. Berlin: Springer.
    </li>
    <li id="spiped2006">Spink, A., Park, M., Jansen, B. J. &amp; Pedersen, J. (2006). Multitasking during
        Web search sessions. <em>Information Processing and Management</em> , <strong>42</strong>(1),
        264-275.
    </li>
    <li id="spikos2006">Spink, A., Park, M. &amp; Koshman, S. (2006). Factors affecting assigned information
        problem ordering during Web search: an exploratory study. <em>Information Processing and
            Management</em>, <strong>42</strong>(5), 1366-1378.
    </li>
    <li id="ste1979">Steiner, I. D. (1979). Three kinds of reported choice. In L. C. Perlmuter &amp; R. A.
        Monty (Eds.), <em>Choice and perceived control</em>. Hillsdale, NJ: Erlbaum.
    </li>
    <li id="su1991">Su, L.T. (1991). <em>An investigation to find appropriate measures for evaluating
            interactive information retrieval</em>. Unpublished doctoral dissertation, Rutgers, the State
        University of New Jersey, New Brunswick, New Jersey, USA
    </li>
    <li id="tia1992">Tiamiyu, M. A. (1992). The relationships between source use and work complexity,
        decision-maker discretion and activity duration in Nigerian government ministries. <em>International
            Journal of Information Management</em>, <strong>12</strong>(2), 130-141.
    </li>
    <li id="tre1960">Treisman, A. M. (1960). Contextual cues in selective listening. <em>Quarterly Journal
            of Experimental Psychology</em>, <strong>12</strong>(4), 242-248.
    </li>
    <li id="tre1964">Treisman, A. M. (1964). Verbal cues, language, and meaning in selective attention.
        <em>American Journal of Psychology</em>, <strong>77</strong>(2), 206-219.
    </li>
    <li id="wal1997">Waller, M. J. (1997). Keeping the pins in the air: How work groups juggle multiple
        tasks. <em>Advances in interdisciplinary studies of team works</em>, <strong>4</strong>, 217-247
    </li>
    <li id="wei1985">Weiner, B. (1985). <em>Human motivation</em>. New York, NY: Springer-Verlag.
    </li>
    <li id="wic1989">Wickens, C. D. (1989). Attention and skilled performance. In D. Holding (Ed.),
        <em>Human skills</em> (pp. 71-105). New York, NY: Wiley &amp; Sons.
    </li>
    <li id="wic2002">Wickens, C.D. (2002). Multiple resources and performance prediction. <em>Theoretical
            Issues in Ergonomic Science</em>, <strong>3</strong>(2), 159-177.
    </li>
    <li id="wic2003">Wickens, C. D., Goh, J., Helleberg, J., Horrey, W.J. &amp; Talleur, D. A. (2003).
        Attentional models of multi-task pilot performance using advanced display technology. <em>Human
            Factors</em>, <strong>45</strong>(3), 360-380.
    </li>
</ul>

</section>

</article>