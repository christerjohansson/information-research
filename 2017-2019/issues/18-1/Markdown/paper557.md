<header>

#### vol. 18 no. 1, March, 2013

</header>

<article>

# What's newsworthy about 'information seeking'? An analysis of Google's News Alerts.

#### [T.D. Wilson](#author) and [Elena Maceviciute](#author)  
Swedish School of Librarianship and Information Science,  
University of Borås,  
Allégatan 1,  
501 90 Borås,  
Sweden

#### Abstract

> **Introduction**. The question guiding this research was to investigate to what extent has the concept of _information seeking_ entered the journalistic discourse in the Internet news media.  
> **Method**. A database of 194 news items, was collected through a Google Alert service over a two year period.  
> **Analysis**. Content analysis, genre analysis and newsworthiness analysis were performed on the news items, as well as quantitative analysis of the types and geographical origins.  
> **Results**. More than 50% of items are found on newspaper, newsletters, magazines, and news sites. The countries providing the bulk of news related to information seeking are the USA and India. The most popular topics are health information seeking and government information seeking, followed by seeking through mobile technology and Freedom of Information issues. News reports and news features are dominating as compared to opinion genres. The impact and significance as well as timeliness and unexpectedness make the information seeking newsworthy.  
> **Conclusion**. The concept of 'information seeking' has entered the public consciousness, as a result, mainly, of the impact of information seeking through mobile devices. Thirty percent of the news was based on research documents, part of which was research in the field of information seeking behaviour.

<section>

## Introduction

In the information science arena we are accustomed to thinking of _information seeking_ as a valid research topic covering a wide variety of contexts and environments from children's use of the Internet to the employment of mobile technologies for information sharing in the emergency services. The term is not new to the information age, the Oxford English Dictionary dates a first use to 1869, where the reference is to '_information-seeking readers_'; however, with the aid of [Google's Ngram Viewer](http://books.google.com/ngrams), we can take the first use date back to 1835, when The Atheneum (a noted magazine of the era) carried an advertisement, which included a review of a book in the _Tyne Mercury_ newspaper, which referred to various topics being '_discussed in a truth-loving, information seeking manner_' ([Atheneum 1835](#ath35)). The term is next found in 1858, when the _American Phrenological Journal_, published a short article on reading by one 'L.D.M.' referring to '_curiosity-loving and information seeking people_'. ([LDM 1858](#ldm58)).

The Ngram Viewer to shows a continuous stream of use of the term from at least 1700 onwards, but, in fact, until 1835 all instances are _false drops_ (the accidental co-occurrence of the two words), most of which result from the existence of a legal document, a formal charge, called _an information_, giving rise to statements such as:

> The ordinary jurisdiction of the Court over such a transaction, by means of an information seeking to have the funds recalled... ([Mylne and Craig 1838](#myl38)).

More false drops occur when the two words appear as _information, seeking_, as in '_eager for information, seeking knowledge_', and the two types occur together, as in '_an information, seeking…_' Use of the legal term, _an information_, continues into the 20th century but seems to disappear around the 1930s, when the occurrences become instances of _information seeking_ as we know it.

Throughout the period from 1800 onwards, the term is generally used as an adjective to describe a person, or a group of persons and up to 1960 the term appears regularly but not in any great numbers, as Figure 1 shows.

<figure>

![Google's Ngram Viewer output for information seeking](../p557fig1.png)

<figcaption>

**Figure 1: Google's Ngram Viewer output for _information seeking_**</figcaption>

</figure>

The surge from 1960 to the present must be associated with the emergence of the computer as an organizational tool and then with the Internet and the World Wide Web as well as with the emergence of research in information science and information systems over the same period. The Ngram Viewer does not reveal this connection, since when 'computer' is added to the analysis, the growth in its usage is so great that 'information seeking' is shown as a flat line at the origin of the graph!

However, we believe that there is enough evidence here for us to suggest that it will be worth exploring how the term is used in the news media at present, rather than by our research community. Such an analysis may reveal unexpected contexts of use that point to areas neglected by the information researcher, as well as revealing what aspects of information seeking the mass media find of interest, which could be of value in publicising our work.

At this point we need to add a caveat: we have relied here on Google's Ngram tool, which uses the corpus of English language materials digitized under the Google Books scheme. It is well known that Google's digitization processes can give rise to many conversion errors (see, for example, [Sullivan 2010](#sul10)), but these do not appear to affect the terms we were using. It may be argued also that the corpus is biased in some way, but, given the millions of volumes scanned and the diversity of those materials, this did not appear to us to be a significant argument against using the tool.

## Related work

The news coverage of different topics and its content in general is in the focus of mass media research as well as in some other social science disciplines, such as political science, law, medicine, cultural and public opinion studies. For our purposes we were most interested in the studies of science and, especially, technology content and discourse in media news. A variety of approaches has been used to study these reflections in pursuit of different goals of which the following are examples.

Stahl ([1995](#sta95)) explored the functions and changes of magical language used in the _Time_ magazine in relation to computer technology in 1979 and 1988\. Using critical theory he revealed how the terminology of magic used as metaphor presents '_a magical box_' computer as '_a source of hope amid fear_' of changing information society; and establish this technology as a road to financial success of '_your children_' as long as they follow the road of entrepreneurs and do not try to threaten the society as it is. '_The language of magic and religion, which had first signalled that something new and wondrous was happening and then played a role in defining the new technologies, began to decline_' ([Stahl 1995](#sta95): 254) as soon as the new technology stabilized and occupied an established space.

More recently Beacco _et al._ ([2002](#bea02)) explored the changes in French media discourse related to science. According to their research, communication from science to _lay_ readers has lost its original form. The analysis of linguistic dimensions of media discourse prove that '_some elements of scientific knowledge have entered the media and ordinary conversations, which are sustained by scientific knowledge drawn from these media, but also by the general public's convictions and their opinions on these matters that affect society_' ([Beacco _et al_. 2002](#bea02): 298). The authors draw a conclusion that _'science has now become an object of debate in the public arena – a sign of its crucial social role'_ ([Beacco _et al_. 2002](#bea02): 277). This would suggest that the perception of computers as 'magic' no longer applies.

Cukier and her colleagues have applied Habermas's theory of communicative action ([Habermas 1984-78](#hab84)) to conduct critical discourse analysis of media news on information technology. In their study of a project implemented by IBM and Acadian University in Canada they test their instrument of critical discourse analysis and find that communication discourse is distorted in favour of powerful actors (corporation and University administration) by reporting their claims uncritically and not giving enough space for dissenters' opinions. The communication distortions later enter the utterances of administrators and academic staff in other universities and are reproduced by government officials ([Cukier _et al._ 2009](#cuk09)). In an earlier study of the macro-discourse of information technology Cukier _et al_. found that the claims regarding the introduction of information technology are usually exaggerated, creating a sense of urgency, using metaphors that emphasize its positive and liberating features, marginalize the critics and opponents as resistant to change and as neo-Luddites, mainly benefiting the vendors of technology as well as media that sell better for catchy headlines ([Cukier _et al._ 2006](#cuk06)). A gap between the promise of technology and the reality it delivers may be partially explained by the media influence on the public consciousness and to some extent to technology decision making in organizations.

## Objectives

In relation to these findings, it is interesting to investigate how information seeking research is reflected in media news in the public arena.

The aim of this paper is to present an analysis of news items that include the term 'information seeking' and to consider how the concept is dealt with, what it means in the context of the news item, and to speculate on how the term enters the public arena and what role is played by the media in popularising the concept.

## The research perspective

The related research that we have found on the topics of our interest is conducted using different kinds of discourse analysis. However, media news messages have quite clear characteristics and content analysis is another method often used for investigating media news. Content analysis is '_any technique for making replicable and valid from texts (or other meaningful matter) to the contexts of their use_' ([Kripendorf 2004](#kri04): 18). Content analysis of news is used when researchers wish to understand the significance of a certain issue for public, identify its most publicized aspects, often in order to find out how public opinion is formed and what arguments are used for highlighting or downplaying of that issue. The issues can be as varied as crime coverage in metropolitan newspaper ([Humphries 1981](#hum81)), framing European politics in press and television news ([Semetko and Valkenburg 2006](#sem06)), or skin cancer prevention and detection issues in Associated Press articles ([Stryker _et al._ 2005](#str05)). In the first case, the ideological bias of lethal crime in the news describing offenders in terms of age, sex, racial membership, and employment status and situating individuals within the social world, is revealed. The framing of European politics in news differed not between media (television vs. the press) but between '_sensationalist vs. serious types of news outlets. Sober and serious newspapers and television news programs more often used the responsibility and conflict frames in the presentation of news, whereas sensationalist outlets more often used the human interest frame_' ([Semetko and Valkenburg 2006](#sem06): 106) . In the third case the authors reveal lack of general interest in skin cancer or any helpful educational information on its detection in the press, but show that medical research on skin cancer and celebrity-related stories help to increase the visibility of skin cancer in the news. In each of these articles the authors show how news is shaped by the context (cultural, historical, or political), but also how it influences the understanding and evaluation of certain issues and even can potentially affect behaviour of its users.

From our research perspective, a number of approaches are feasible and we have taken into account several categories from mass media studies that we deemed useful for our investigation: such as gate-keeping, news genres and content, and unintended media effects.

Barzilai-Nagon ([2009](#bar09)) in her review of literature of gate-keeping presents a number of aspects of the phenomenon appearing in different disciplines. That which suits our purposes relates to the editorial work and means '_A particular type of access – processes of selection used by journal editors, reviewers, and the news media to identify works to be published and disseminated. Often associated with power to define how field of study progresses, or what items are newsworthy._' ([Barzilai-Nagon 2009](#bar09): 17). The latter aspect of the newsworthiness of the messages on information seeking what we are exploring in this paper. This aspect implies the criteria of the newsworthiness that may affect the decision to include an item into the news or discard it. As we do not have access to actual decision-makers we will rely on a standard set of the features that characterise the news also keeping our minds open to some others that are not included in the following list compiled by us out of different lists since Galtung and Ruge ([1965](#gal65)) to Ivancheva ([2011](#iva11)):

1.  Impact: the significance, importance, or consequence of an event or trend.
2.  Timeliness: the more recent, the more newsworthy.
3.  Prominence: occurrences featuring well-know individuals or institutions are newsworthy.
4.  Unexpectedness: the unusual, unorthodox, or unexpected attracts attention.
5.  Proximity: closeness to the locality.
6.  Currency: occasionally something becomes an idea whose time has come.
7.  Human interest: those stories that have more of an entertainment factor.

We are analysing messages in the Internet news flow related to information seeking. Therefore, in addition to newsworthiness, the issues of the presentation of the contents and news genres are relevant to our investigation. News genres are still under development and their classification and the concept itself have been strongly affected by news delivery services. We will not go into detailed discussion of these issues. We have adopted the definition of genres as recognizable categories of information objects based on conventions of form, purpose and content ([Yoshioka _et al._ 2001](#yos01)). A widely spread classification of newspaper genres includes service information, opinion and news, which are further divided into hard news, feature articles, headlines and special topic news ([Bell 1991](#bel91)). Recently a number of attempts was made to develop classification of Internet newspaper genres ([Ihlström and Lundberg 2003](#ihl03)) and Web genres ([Santini and Sharoff 2009](#san09)). To establish the genres of the news related to information seeking we have used the preliminary taxonomy of news genres (shortened and simplified by us) created by Freund _et al._ ([2011](#fre11)) as it was developed specifically for online news (see Table 1).

<table><caption>

**Table 1: Taxonomy of news genres (adapted from Freund _et al._ 2011)**</caption>

<tbody>

<tr>

<th>Review</th>

<th>Informing</th>

<th>Factual  
information</th>

<th>Opinion/advice</th>

<th>Notice</th>

<th>Profile</th>

</tr>

<tr>

<td>Event review</td>

<td>Investigative piece</td>

<td>Data</td>

<td>Blog entry</td>

<td>Correction/retraction</td>

<td>Biography</td>

</tr>

<tr>

<td>Best of list</td>

<td>News feature</td>

<td>Infographic</td>

<td>Column</td>

<td>Event listings</td>

<td>Interview</td>

</tr>

<tr>

<td>Book (media) review</td>

<td>News report</td>

<td>Sidebar</td>

<td>Editorial</td>

<td> </td>

<td>Public service announcement</td>

</tr>

</tbody>

</table>

## Data collection

To explore the presented idea we have maintained a Google Alert on the topic of _information seeking_ for two years from February 2010\. Google's Alert service enables us to monitor how the concept is being dealt with on the Internet, at least as revealed by those sites monitored by Google as sources of news. The service covers a wide variety of sites, from personal blogs, through manifestations of the media on the Web (such as sites maintained by newspapers and television stations) to news aggregators in a wide variety of fields.

Between February 2010 and January 2012 a total of 370 news items was archived, however, there was a total of forty _false drops_, resulting from the accidental conjunction of the words _information_ and _seeking_ and a further thirty-six were currently inaccessible, either because access to the site archives demanded subscription or simply because the item had been removed from the site. Thus, a total of 194 items remained available for analysis.

Again, we add a caveat: the Alerts are based on what Google decides to scan and it is clear that a good deal more attention is given to US sources than to those from other parts of the world. This is not something we can correct and, therefore, we have to accept it, while noting the potential limitations.

## Analysis

We have looked at the type of the site, the genre of media news, the actual usage of the term _information seeking_, the context in which it appears, and what makes it newsworthy.

Indeed, one of the problems we came across in this study is typifying the kind of source in which a news item appears. The distinctions between social networking sites, media sites and blogs begin to disappear when we find that all of them are deriving their content from other Web sites, from organizational press releases, and from other news aggregators.

Each news item was coded using:

*   the media news genres from Table 1 above (but we have created some when necessary);
*   text surrounding the term _information seeking_;
*   characterizing the text, in which the term occurred, in relation to external information source and general content; and
*   assigning the category of newsworthiness from the list above.

We provide some quantitative information of this analysis, though '_the number of times an argument or term appears does not provide insight into the meaning of the texts, but it does provide some indication of the themes that dominate the discourse as well as the omissions that may suppress understanding_'. (Cukier _et al._ 2009)

## Results

### Who are the gate-keepers?

The news items were reviewed to identify the context in which the term _information seeking_ appeared and to categorise the type of Website and the topic of the item. By definition, all of the sites were what Google Alerts identified as sources of news and the categorisation of sites is not a particularly straightforward operation. Sites which might be regarded as very different turn out to be rather similar; for example, a site aimed at an audience of poker players reports a news item on an FBI investigation in Washington State, and the differences between newspaper sites and broadcaster sites (TV stations and general broadcasters such as the BBC) begin to disappear when the former embed video recordings of events. There is research into the genre analysis of Websites (e.g., [Gupta _et al._ 2005](#gup05)) and on the classification of sites (e.g., [Xu _et al._ 2009; Lindemann and Littig 2007](#xu09)); however, the former does not provide a detailed list of the genres discovered, while the latter, in common with most attempts at automatic classification over the years, results in only very broad categories of, in one case, types of site and, in the other, the subject content of sites.

Consequently, we had to devise our own categorisation, based on the nature of the site and its content. This was done by assigning a category during the initial recording of items into a Bento database and then reviewing the categories to simplify and standardise the categorisation. This process resulted in the following categories, ranked by the number of associated items:

<table><caption>

**Table 2: Distribution of news items over source types**</caption>

<tbody>

<tr>

<th>Source type</th>

<th>No</th>

<th>%</th>

</tr>

<tr>

<td>

Newspaper site (site of a print newspaper, e.g., [Times of India](http://timesofindia.indiatimes.com))</td>

<td>39</td>

<td>20.10</td>

</tr>

<tr>

<td>

News site (digital only news site, e.g., [businesswire.com](http://www.businesswire.com/portal/site/home/))</td>

<td>37</td>

<td>19.07</td>

</tr>

<tr>

<td>

Newsletter (regular online publication, e.g., [stockmarketsreview.com](http://stockmarketsreview.com))</td>

<td>23</td>

<td>11.86</td>

</tr>

<tr>

<td>

Magazine site (site of a print magazine, e.g., [Forbes](http://www.forbes.com))</td>

<td>17</td>

<td>8.76</td>

</tr>

<tr>

<td>

Company site (corporate site, e.g., [jdsupra.com](http://www.jdsupra.com))</td>

<td>10</td>

<td>5.15</td>

</tr>

<tr>

<td>

Broadcaster site (radio or TV station site, e.g., [CNN](http://edition.cnn.com))</td>

<td>9</td>

<td>4.64</td>

</tr>

<tr>

<td>

Press release distributor (e.g., [prweb.com](http://www.prweb.com))</td>

<td>9</td>

<td>4.64</td>

</tr>

<tr>

<td>

University site (e.g., [Massey University](http://www.massey.ac.nz/massey/home.cfm))</td>

<td>9</td>

<td>4.64</td>

</tr>

<tr>

<td>

Social networking site (e.g., [psychcentral.com](http://psychcentral.com))</td>

<td>8</td>

<td>4.12</td>

</tr>

<tr>

<td>

Entertainment site (gossip, games, or film site, e.g., [Cool Videos](http://www.jokeroo.com/videos/cool/))</td>

<td>6</td>

<td>3.09</td>

</tr>

<tr>

<td>

Information portal (e.g., [bigthink.com](http://bigthink.com))</td>

<td>5</td>

<td>2.58</td>

</tr>

<tr>

<td>

Organization site (non-profit organizations, e.g., [Pew Internet & American Life Project](http://www.pewinternet.org))</td>

<td>5</td>

<td>2.58</td>

</tr>

<tr>

<td>

Blog site (e.g., [Ineedhits.com](http://www.ineedhits.com))</td>

<td>4</td>

<td>2.06</td>

</tr>

<tr>

<td>

Journal site (e.g., [ABA Journal](http://www.abajournal.com))</td>

<td>4</td>

<td>2.06</td>

</tr>

<tr>

<td>

Technology site (focused on technology issues, e.g., [searchengineland.com](http://searchengineland.com))</td>

<td>4</td>

<td>2.06</td>

</tr>

<tr>

<td>

Government site (e.g., [Environmental Protection Agency](http://www.epa.gov))</td>

<td>3</td>

<td>1.55</td>

</tr>

<tr>

<td>

Health information site (e.g., [curetoday.com](http://curetoday.com))</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>

Political site (supporting or commenting on party and/or government politics, e.g., [http://biggovernment.com](http://biggovernment.com))</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Total</td>

<td>194</td>

<td>100.00</td>

</tr>

</tbody>

</table>

As Table 2 shows, more than 50% of the items were accounted for by the first four categories, which we might recognize as typical information sources.

<table><caption>

**Table 3: Geographical distribution of news items.**</caption>

<tbody>

<tr>

<th>Country</th>

<th>No.</th>

<th>%</th>

</tr>

<tr>

<td>USA</td>

<td>117</td>

<td>60.31</td>

</tr>

<tr>

<td>India</td>

<td>26</td>

<td>13.40</td>

</tr>

<tr>

<td>UK</td>

<td>16</td>

<td>8.25</td>

</tr>

<tr>

<td>Netherlands</td>

<td>8</td>

<td>4.12</td>

</tr>

<tr>

<td>Canada</td>

<td>6</td>

<td>3.09</td>

</tr>

<tr>

<td>Pakistan</td>

<td>3</td>

<td>1.55</td>

</tr>

<tr>

<td>Australia</td>

<td>2</td>

<td>1.03</td>

</tr>

<tr>

<td>Kashmir</td>

<td>2</td>

<td>1.03</td>

</tr>

<tr>

<td>Croatia</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>France</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Hong Kong</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Ireland</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Kenya</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Nepal</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>New Zealand</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Philippines</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Qatar</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Russia</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>S. Africa</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Sudan</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Switzerland</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Turkey</td>

<td>1</td>

<td>0.52</td>

</tr>

<tr>

<td>Total</td>

<td>194</td>

<td>100.00</td>

</tr>

</tbody>

</table>

The geographical distribution of the news items (Table 3) is highly skewed, with 60% of them being contributed by sites based in the USA, but India is the second-ranked country, with only 26 items (13.4%). Even so, we might ask how it is that the concept _information seeking_ appears to have entered the public (or perhaps the journalistic) consciousness in these two countries, while there is little evidence for it having entered the consciousness in, for example, Canada, Australia, South Africa or the UK?

The 194 items were drawn from 161 sources, indicating the widespread character of the concept. Only seven sources reported more than two items and 141 different sources reported only one item in the two-year period. The seven sources reporting more than two items are shown in Table 3:

<table><caption>

**Table 4: Sources with more than two items**</caption>

<tbody>

<tr>

<th>Source</th>

<th>No</th>

</tr>

<tr>

<td>7th Space Interactive (information portal)</td>

<td>6</td>

</tr>

<tr>

<td>Business Wire (online newsletter)</td>

<td>4</td>

</tr>

<tr>

<td>Smart Data Collective</td>

<td>4</td>

</tr>

<tr>

<td>Times of India (a newspaper)</td>

<td>4</td>

</tr>

<tr>

<td>BigThink (information portal)</td>

<td>3</td>

</tr>

<tr>

<td>Environmental Protection Agency (governmental agency)</td>

<td>3</td>

</tr>

<tr>

<td>Information Week Health Care (newsletter)</td>

<td>3</td>

</tr>

</tbody>

</table>

### Contents and contexts

The database items were coded from various viewpoints, as discussed in this section. One dimension was that of the context within which information seeking was mentioned. Table 4 shows the distribution of topics with five or more items associated with them under this heading. Fifty-five categories were assigned under this heading, giving an average of approximately 3.5 items to each category. However, as Table 4 shows, the distribution was highly skewed (categories with five or more items constituted over 63% of the total) and from this we can gain some idea of the newsworthiness of the topic from this perspective. Two topics topped the list with twenty-one items each: the health information context and information seeking by government. Some of the former were found on specialised news sites devoted to business (e.g., [www.businesswire.com](http://www.businesswire.com)) or marketing (e.g., [www.emarketer.com](http://www.emarketer.com)), some in material on sites specialising in health-related issues, such as [curetoday.com](http://curetoday.com), the site of a magazine devoted to cancer, and a cluster was found in the information portal [7thSpace.com](http://7thspace.com), which makes a feature of reporting scientific research on health topics

<table><caption>

**Table 5: The context within which _information seeking_ appears**</caption>

<tbody>

<tr>

<th>IS context</th>

<th>No.</th>

<th>%</th>

<th>Cum.%</th>

</tr>

<tr>

<td>Seeking for health information</td>

<td>21</td>

<td>10.8</td>

<td>-</td>

</tr>

<tr>

<td>Seeking by government</td>

<td>21</td>

<td>10.8</td>

<td>21.6</td>

</tr>

<tr>

<td>Use of mobile devices</td>

<td>14</td>

<td>7.2</td>

<td>28.9</td>

</tr>

<tr>

<td>In relation to Freedom of Information</td>

<td>12</td>

<td>6.2</td>

<td>35.1</td>

</tr>

<tr>

<td>Use of search engines</td>

<td>11</td>

<td>5.7</td>

<td>40.7</td>

</tr>

<tr>

<td>Help from libraries or librarians</td>

<td>11</td>

<td>5.7</td>

<td>46.4</td>

</tr>

<tr>

<td>Students' information seeking</td>

<td>9</td>

<td>4.6</td>

<td>51.0</td>

</tr>

<tr>

<td>Use of social media</td>

<td>9</td>

<td>4.6</td>

<td>55.7</td>

</tr>

<tr>

<td>Information seeking as competence</td>

<td>5</td>

<td>2.6</td>

<td>58.2</td>

</tr>

<tr>

<td>Information seeking by professionals</td>

<td>5</td>

<td>2.6</td>

<td>60.8</td>

</tr>

<tr>

<td>Information seeking by citizens</td>

<td>5</td>

<td>2.6</td>

<td>63.4</td>

</tr>

</tbody>

</table>

The information seeking by government category was the result of the false drop referred to earlier and most of the items were based on calls for information on specific topics, mainly by US government departments. Generally these information requests are from a specific department (or arm of the armed forces) and the announcement is usually an edited version of an announcement in the _[Federal Register](http://1.usa.gov/42kJFZ)_. These announcements appear on news sites relating to business in general or to a specific interest area for business, on government department sites, on general news sites, and in newspaper sites. For example, [this notice](http://yosemite.epa.gov/opa/admpress.nsf/1e5ab1124055f3b28525781f0042ed40/8b2cf822c77036838525786800570c1c!OpenDocument) appears on the Environmental Protection Agency site:

> EPA to Hold Public Hearing on Three Year Permitting Deferral for Biomass Facilities

Occasionally, the fact that a request has been published is reported as part of a longer piece on a related issue, for example:

> In April 2010, the army's Aviation and Missile Command released a request for information seeking options for a new class of precision guided weapons weighing less than 11.3kg. These would be small enough to accommodate on the RQ-7, along with the vehicle's optical sensor and a laser designator.

is found on the [Flight Global](http://www.flightglobal.com) site in [an article](http://bit.ly/xsInTf) on unmanned aircraft systems.

The items dealing with the use of mobile devices (mainly smartphones, with the occasional mention of iPads) are generally occasioned by the release of a market survey report, or a report on a research survey and the associated press releases. The items are found mainly in technically-oriented sources and business news sources, with an emphasis on the marketing possibilities that result from of people carrying out their information seeking activities on mobile devices. For example, reports on a survey by the Pew Research Center's Internet & American Life Project appeared in [TechJournalSouth](http://bit.ly/pwdCrz), and [TelecomPaper](http://bit.ly/wP4V5t), as well as on the [Center's own Website](http://bit.ly/nXCjgh). Similarly, a major survey by Microsoft Advertising, MEC and Mindshare across eleven countries was reported by [BestMediaInfo](http://bit.ly/lCoGtD), an Indian media news site, by [Indiantelevision.com](http://bit.ly/A3qBtL) and by [Asiamediajournal.com](http://bit.ly/Ay2FwD), based in Hong Kong. A study by Google was picked up by the search engine marketing blog, [Ineedhits.com](http://bit.ly/kIevyp) and by [TheMobileIndian](http://bit.ly/kWFjWM), an Indian site with mobile phone news.

Eleven of the twelve items relating to Freedom of Information concerned the Indian Right to Information Act (a much better title, by the way, than Freedom of Information), ten of them in Indian sources and one from Nepal. These generally related to the difficulties experienced by people seeking information under the act (for example, a report from the [Times of India](http://bit.ly/AjsrZW)) and, in one case, [criminal activity against information seekers](http://bit.ly/zPjO1w). The twelfth item related to an Australian professor being harassed through Freedom of Information Act demands from the Church of Scientology, following a critical article ([University Prof Patrick McGorry is targeted by Scientology church](http://bit.ly/d6LcL7)).

The items that occur in relation to the help that can be given in information seeking by libraries or librarians generally occur in letters from interested parties (librarians, Chairs of library committees) or columns written by librarians. Their occurrence appears to be a response, in at least some cases, to the threat of closure or loss of staff. For example, in a letter from the Chair of a library committee, headed _[Libraries need support](http://bit.ly/wsZXZ4)_:

> Our public library requires significant municipal funding to continually update services that keep pace with the new generation of users with fundamentally different information seeking habits. Those services are indispensable and provide significant return on investment. Without them, our community is at a disadvantage in attracting knowledge workers, recovering from the current economic recession and growing a knowledge economy.

In other cases, the writer, often a librarian or former librarian is reflecting on the value of libraries as information-seeking venues. For example, in a column on star-gazing, _[The library landscape: curiosity and your library](http://bit.ly/AkyxRF)_, a librarian notes:

> Now my curiosity had been properly raised—what are these stars that I've been watching wander across the sky?  
> I did what any information seeking individual should do; I turned to my local public library.  
> One day after work was done, I spent some time in the science section (with lots of new, updated books I might add) and came away with a couple of great titles to help me.

The remaining categories in Table 4 all had fewer than ten associated items, but it is worth noting the attention given to the emergence of social media as an avenue for information seeking and the extent to which this is being recognized by marketers. For example, an article, [Social media who's who](http://bit.ly/whv14G) cites a market research study and comments:

> The pressing issue is knowing who, exactly, is visiting social networking sites, blogs and other social media, as well as their motivations for being there, so that marketers know which ones are worth their time.

Information seeking by students mainly relate to the new contexts of studies and their future work or changing study methods, as in the following:

> Problem Based Learning is a process by which we expect the students to become self-directed learner and efficient in learning, reasoning, and information seeking.

Information as competence is often mentioned in relation to education process and development of communication skills as a part of learning process (e.g., for re-deployed soldiers or patients with disability). While information seeking by professionals deals with the work situations of managers, pharmacists and health care professionals or transition from educational to occupational life. Citizens' information seeking is treated as a general duty of voters or as information requests from local authorities, as well as an indication of the activity of citizens related to popular social movements or use of the sources of everyday news.

This analysis raised the question of the extent to which authors refer to research sources, including market research, recent PhD studies, or research papers. In all, almost 30% of the items were based on some research input.

### Genres and newsworthiness

The coding on genre dimension was rather detailed and these details will be presented later; but for the general overview Table 1 was modified to include some genres assigned to the items in the database. One of the divisions (_Notice_) of the original table was removed as only two items of _Event listings_ were assigned to it (they were included into the _News report_ section).

Thus, five main headings remained with several additional genres included under _Opinion/advice_ and _Factual information_. We found that some opinion and advice items similar to blog or column entries appear on some Websites irregularly and labelled them _Opinion feature_. Letters from readers were also included under _Opinion/advice_ heading as they are quite easily distinguished from any other opinion items. _Government information_ and _Event announcement_ were inserted into the _Factual information_ section instead of _Infographic_ and _Sidebar_ options, which did not occur.

Working descriptions were formulated for each genre for the coding purposes. However, there are no clear distinctions between the genres, e.g., an interview may relate to a reporting about a certain event or research survey and have little to do with profiling a person or an organization. An event review may also be treated as a news report about a conference or a meeting. Thus, there is some subjectivity in coding.

<table><caption>

**Table 6: Internet news genres assigned to the database items (the percents add to more than 100% because of rounding)**</caption>

<tbody>

<tr>

<th>Informing</th>

<th>N</th>

<th>%</th>

<th>Opinion/advice</th>

<th>N</th>

<th>%</th>

<th>Review</th>

<th>N</th>

<th>%</th>

<th>Factual information</th>

<th>N</th>

<th>%</th>

<th>Profile</th>

<th>N</th>

<th>%</th>

</tr>

<tr>

<td>News report</td>

<td>89</td>

<td>45.9</td>

<td>Opinion feature</td>

<td>17</td>

<td>8.8</td>

<td>Media review</td>

<td>10</td>

<td>5.1</td>

<td>Government information</td>

<td>6</td>

<td>3.1</td>

<td>Interview</td>

<td>7</td>

<td>3.6</td>

</tr>

<tr>

<td>News feature</td>

<td>14</td>

<td>7.2</td>

<td>Blog entry</td>

<td>12</td>

<td>6.2</td>

<td>Event review</td>

<td>9</td>

<td>4.6</td>

<td>Data</td>

<td>4</td>

<td>2.1</td>

<td>Biography</td>

<td>2</td>

<td>2.3</td>

</tr>

<tr>

<td>Investigative piece</td>

<td>2</td>

<td>1.0</td>

<td>Column</td>

<td>13</td>

<td>6.7</td>

<td> </td>

<td> </td>

<td> </td>

<td>Event announcement</td>

<td>3</td>

<td>1.5</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td>Letter</td>

<td>3</td>

<td>1.5</td>

<td> </td>

<td> </td>

<td> </td>

<td>Glossary</td>

<td>1</td>

<td>0.5</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td> </td>

<td>Editorial</td>

<td>1</td>

<td>0.5</td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Totals</td>

<td>105</td>

<td>54.1</td>

<td> </td>

<td>46</td>

<td>23.7</td>

<td> </td>

<td>19</td>

<td>9.7</td>

<td> </td>

<td>14</td>

<td>7.2</td>

<td> </td>

<td>9</td>

<td>5.9</td>

</tr>

</tbody>

</table>

The highest number of items fall under the news report genre, which was defined as an operative and clear message about an event or object presenting the essence of the event and drawing attention of readers to the most important topics and problems associated with it. This category included messages about meetings, conferences, research projects and reports, released surveys, company reports and events. News features related to a more extensive discussion of problems related to an event and presentation of an additional material or argumentation found by a journalist. Some of these items were related directly to press releases and in seven cases press releases were directly re-published on the news site without any editing or addition. The press releases were issued by marketing companies, government agencies and one university about a research grant. In general quite a significant proportion (29.4%) of news items relate to research in one way or another.

<table><caption>

**Table 7: Research based news items**</caption>

<tbody>

<tr>

<th>Research type</th>

<th>No.</th>

</tr>

<tr>

<td>Report on a research paper/PhD study</td>

<td>22</td>

</tr>

<tr>

<td>Report on a market research survey</td>

<td>14</td>

</tr>

<tr>

<td>Report on research survey</td>

<td>10</td>

</tr>

<tr>

<td>Citation to research paper</td>

<td>5</td>

</tr>

<tr>

<td>Report on projected research</td>

<td>5</td>

</tr>

<tr>

<td>Report on a university research project</td>

<td>1</td>

</tr>

<tr>

<td>

**Total**</td>

<td>57</td>

</tr>

</tbody>

</table>

Of these, a large part (nine items) is related to market surveys, especially those related to information technology, Internet searching through mobile devices or Internet use for finding certain types of products and services (e.g., tourism or brands). However, most surveys (eleven) look into health information seeking of different kinds of patients or for certain types of health information. Several news reports relate to information seeking in educational situations (three) or usage of media for information seeking (three), but nine news items relate to information seeking research (e.g., results of the LearnIT project at the University of Borås, reports on two PhD theses in information seeking from Amsterdam, or the JISC study of Generation Y students' information behaviour, etc.). Interesting examples from this category are two reports on DIEM eye-tracking experiments for information seeking on screen, as these are the only two multimedia items that include the video clips from films with visible eye-tracks.

Opinion items mainly deal with some kind of technological or social issue. Most of the blog entries discuss social media and its information seeking potential. Several library related items (4) are found among different opinion pieces (a letter, a blog entry, a column).

Media reviews mentioning information seeking deal with search engines (two) and Web services and Web sites (four), an application and a book on information technology. Most interviews are with the company representatives discussing information technology, a government official, and two with academic staff (an environment researcher and a media researcher). Information seeking is a sideline, except in one concerning search engines and one on health information. It is worth mentioning that two biographical items relate to Garry Marchionini's appointment to the national health report working group.

This last item carries clear prominence and significance features that are newsworthy. Looking into the newsworthiness it is quite clear that news mentioning information seeking are selected as new, up-to-date, timely events (eighty-seve). However, it is even more important that these items should be significant to the community, profession, organization, or society. The features of importance and impact are found in 115 items in our database. Sixty of them are both timely and significant. Another important feature is unexpectedness, e.g., finding that Generation Y needs quite significant guidance in information seeking or that health information seeking in the USA has gone down in 2010 as compared with 2007 may draw attention of the audiences. This feature can be found in forty-nine items and combines well with timeliness (thirteen) and significance (thirteen). Human interest is important for the same reason, drawing the attention of the public. It characterizes such items as information seeking about health of pets, looking for information about magic, as well as interviews with celebrities. All in all, 41 items carries this feature. Sometimes it can be found in items featuring known or high standing people and institutions, i.e., having the feature of prominence. Usually, these are government officials and celebrities or big companies and powerful governmental departments. However, it may be interesting that apart from G. Marchionini, David Nicholas and Louise Limberg are mentioned in the news, as well as JISC, Michigan State University, University of Nebraska Lincoln, University of Minesota, and University of Borås among others. Closeness to the community (proximity) features less (twenty-six items) and mainly in the local media sites. Most of library related news occurs in this context. Very few items (nineteen) carried the currency feature; that is, current attention to older topics was rare. For example, the suggestion that voters should seek information about candidates' goals and programmes as their duty or that libraries are a public resource providing trust and public good can be examples of such items.

In general, newsworthiness characteristics help us to understand better why the gatekeepers select specific items for publication in addition to their content and meaning.

## Conclusion

The impression we have, following this analysis, is that _information seeking_ has become accepted into the general media discourse as an aspect of human behaviour that is widely understood. Internationally, organizational representatives of many different kinds are reported in these news items as referring to 'information seeking' in a wide variety of contexts. For example, in a newspaper report on a child's death from heat exposure we are told that,

> Chief Strunz says, 'We are in the information seeking mode. We're trying to answer a lot of questions we have as well'. ([Mackin 2010](#mac10))

The choice of the term is interesting since police officers until very recently would probably have spoken of 'gathering evidence' rather than of being in an 'information seeking mode'. Our suggestion is that this is probably owed to the presence of the concept in the news (note that the '_information search and access_' topic was the most frequently found in the analysed material) closely followed by '_health information_', where the words regularly appeared.

The impact of mobile communication technologies is particularly notable. A number of reports have drawn attention the significance of the mobile (cell) phone as an information seeking device, to the extent that this is now commonly recognised in news items. For example,

> The mobile opportunity for online product **information seeking** and online promotion is just one piece of the online pharma service model explored by ePharma Physician® v11.0\. ([New Manhattan… 2011](#new11))

The analysis of the news items reveals that a number of different genres appear on the various Websites. First, the press release plays an important role: some sites simply present a press release as it is issued, others select information from the press release to create a (usually) shorter account of the news item. Some sites act as aggregators and redistributors of such information (for example, pr-inside.com advertises itself as '_a Website for the free submission of public relations distribution, news, and press releases_'). From this point of view, universities and information seeking researchers themselves could act as providers of ready made news to the gatekeepers of the news sites.The reliance of news sources on published research suggests that if researchers wish to have their work noticed, it is worth spending time to discover which sites are likely to use the information. Typically, a recently completed PhD research project will probably be of interest to the local newspaper site, especially if it deals with matters of relevance to local business and industry, and to national and international sites that focus on the subject area. For example, new work on seeking health information is likely to be of interest to health sites, while a study of, say, students' use of mobile phones to access hobby and entertainment related information, is likely to be of interest to sites concerned with Internet marketing. In other words, we can do much more than is generally done at present to ensure that the familiarity of the media with our work is more than a matter of chance.

On the other hand, it might be beneficial for information seeking research to look into other sources than news presented in Google Alerts. Sources covering wider newspaper output on international level, trade magazines catering for various professional communities may be a further object of research of this type. It is also possible to follow bigger information seeking and behaviour research projects and note how they publicize their work throughout the life-time for wider public and which media outlets pick up their public releases. The classification of the news sources that we have made for this paper is rather subjective. Closer investigation of the news on information seeking may help to develop this classification further.

## Acknowledgements

We would like to thank the original ISIC referees for their helpful comments on the original paper, and for the recommendation by one that Google's Ngram Viewer would prove useful. We would also like to thank the third referee, who reviewed the paper after it was presented at ISIC, for further helpful suggestions.

## About the authors

**Elena Maceviciute** is Professor in the Swedish School of Librarianship and Information Science, University of Borås. She is also Professor in the Faculty of Communication, Vilnius University, Lithuania. She is a habilitated doctor in Information and Communication studies at Vilnius University. Her research relates to information use in organization, digital libraries and resources, and, currently, the role of e-books modern society.  
**Tom Wilson** is Professor Emeritus, University of Sheffield and Visiting Professor in the Swedish School of Librarianship and Information Science, University of Borås. His research has spanned areas of information behaviour and information management. He holds a Bachelor's degree in Economics and Sociology from the University of London, and a PhD from the University of Sheffield. He has been awarded Honorary Doctorates from the Universities of Gothenburg, Sweden and Murcia, Spain.

</section>

<section>

## References

*   <a id="ath35"></a>The Atheneum. (1835). [Advertisement for books published by Chapman and Hall]. _The Atheneum_, (No. 403), 560
*   <a id="bar09"></a>Barzilai-Nahon, K. (2009). Gatekeeping: a critical review. _Annual Review of Information Science and Technology_, **43**, 1-79.
*   <a id="bea02"></a>Beacco J.-C., Claudel, C., Doury, M., Petit, G. & Reboul-Touré, S. (2002). Science in media and social discource: new channels of communication, new linguistic forms. _Discourse Studies_, **4**(3), 277-300.
*   <a id="bel91"></a>Bell, A. (1991). _The language of news media_. Oxford: Blackwell Publishers Ltd.
*   <a id="bli08"></a>Blidook, K. (2008). Media, public opinion and health care in Canada: how the media affect 'The way things are'. _Canadian Journal of Political Science_, **41**(2), 355-374.
*   <a id="cuk06"></a>Cukier, W., Bauer, R. & Nesselroth, E. (2006). Distortions in the media: a Habermasian approach to analysing technology macro discourse. Paper presented at the Critical Management Studies Research Workshop, Atlanta, GA, June 11-12, 2006.
*   <a id="cuk09"></a>Cukier, W., Ngwenyama, O., Bauer, R. & Middleton, C. (2009). A critical analysis of media discourse on information technology: preliminary results of a proposed method for critical discourse analysis. _Information Systems Journal_, **19**(2), 175–196.
*   <a id="fre11"></a>Freund, L., Berzowska, J., Lee, J., Read, K. & Shiller, H. (2011). Digging into digg: genres of online news. In _Proceedings iConference'11, February 8-11, 2011, Seattle, WA, USA_. (pp. 674-675). New York: ACM Press
*   <a id="gal65"></a>Galtung, J. and Holmboe Ruge, M. (1965). The structure of foreign news: the presentation of the Congo, Cuba and Cyprus crises in four Norwegian newspapers. _Journal of Peace Research_, **2**(1), 64-90
*   <a id="gup05"></a>Gupta, S., Kaiser, G., Stolfo, S. & Becker, H. (2005). _[Genre classification of websites using search engine snippets.](http://www.webcitation.org/6DocGsLo4)_ New York, NY: Columbia University, Department of Computer Science. (Computer Science Technical Reports CUCS-004-05) Retrieved 20 January, 2012 from http://academiccommons.columbia.edu/download/fedora_content/download/ac:109571/CONTENT/cucs-004-05.pdf (Archived by WebCite® at http://www.webcitation.org/6DocGsLo4)
*   <a id="hab84"></a>Habermas, J. (1984-1987). _The theory of communicative action._ 2 vols. Boston, MA: Beacon Press.
*   <a id="hum81"></a>Humphries D. (1981). Serious crime, news coverage, and ideology: a content analysis of crime coverage in a metropolitan paper. _Crime & Delinquency_, **27**(2), 191-205.
*   <a id="ihl03"></a>Ihlström, C. and Lundberg, J. (2003). [The online news genre through the user perspective](http://vir.liu.se/~TDDC62/material/newsgenre-HICCS36.pdf). Paper presented at the 36th Hawaii International Conference on System Sciences. Retrieved 19 January, 2012 from http://vir.liu.se/~TDDC62/material/newsgenre-HICCS36.pdf
*   <a id="iva11"></a>Ivancheva, T. (2011). _[News values-revised](http://eprints.nbu.bg/846/1/News_Values_Revised.pdf)_. Sofia: New Bulgarian University. (Unpublished working paper). Retrieved 10 January 2012 from http://eprints.nbu.bg/846/1/News_Values_Revised.pdf
*   <a id="kri04"></a>Krippendorff, K. (2004). _Content analysis: an introduction to its methodology_. Thousand Oaks, CA: Sage Publications.
*   <a id="ldm58"></a>L.D.M. (1858). What shall we read? _American Phrenological Journal_, **27**(5), 68-69.
*   <a id="mac10"></a>Mackin, T. (2010, July 19). [Coroner: toddler died of heat exposure.](http://www.wkow.com/Global/story.asp?S=12835179) _WKOW.com_ Retrieved 5 January 2012 from http://www.wkow.com/Global/story.asp?S=12835179
*   <a id="mcq94"></a>McQuail D. (1994). _Mass communication theory_. 3rd ed. London: Sage.
*   <a id="myl38"></a>Mylne, J.W. & Craig, R.D. (1838). _Reports of cases argued and determined in the High Court of Chancery during the time of Lord Chancellor Cottenham._ Vol. II. 1836-7\. – 7 Will. IV & 1 Vict. London: Saunders and Benning
*   <a id="new11"></a>[New Manhattan research study finds strong physician demand for access to pharma product information on smartphones and iPads.](http://bit.ly/yCEnVe) (2011, July 26). _Business Wire_. Retrieved 5 January 2012 from http://bit.ly/yCEnVe
*   <a id="san09"></a>Santini, M. and Sharoff S. (2009). Web genre benchmark under construction. _Journal of Language Technology and Computational Linguistics_, **24**(1), 129-145.
*   <a id="sem06"></a>Semetko H.A. and Valkenburg P.M. (2006). Framing European politics: a content analysis of press and television news. _Journal of Communication_, **50**(2), 93-109.
*   <a id="sta95"></a>Stahl, W.A. (1995). Venerating the black box: magic in media discourse on technology. _Science, Technology & Human Values_, **20**(2), 234-258.
*   <a id="str05"></a>Stryker J.e., Solky B.A and Emmons K.M. (2005). A content analysis of news coverage of skin cancer prevention and detection,1979 to 2003\. _Archives of Dermatology_, **141**(4), 491-496.
*   <a id="sul10"></a>Sullivan, D. (2010, December 29). [When OCR goes bad: Google's Ngram viewer & the F-word.](http://www.webcitation.org/67AHpAzSf) _Search Engine Land_. Retrieved 24 April, 2012 from http://searchengineland.com/when-ocr-goes-bad-googles-ngram-viewer-the-f-word-59181 (Archived by WebCite® at http://www.webcitation.org/67AHpAzSf)
*   <a id="xu09"></a>Xu, Z-M., Gao, X-B. & Lei, M. (2009). Web site classification based on key resources. In _Proceedings of the Eighth International Conference on Machine Learning and Cybernetics, Baoding, 12-15 July 2009_ (pp. 3522-3526). Washington, DC: IEEE Computer Society.
*   <a id="yos01"></a>Yoshioka, T., Herman, G., Yates, J. & Orlikowski, W. (2001). Genre taxonomy: a knowledge repository of communicative actions. _ACM Transactions on Information Systems_, **19**(4), 431-456.

</section>

</article>