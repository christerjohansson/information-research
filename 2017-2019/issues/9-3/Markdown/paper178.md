# Constructing a model of effective information dissemination in a crisis

#### [Fiona Duggan](mailto:f.duggan@northumbria.ac.uk) and [Linda Banwell](mailto:linda.banwell@northumbria.ac.uk)  
Information Management Research Institute  
School of Informatics, Northumbria University  
Newcastle upon Tyne, UK.

#### **Abstract**

> A model of effective information dissemination in a crisis was developed through a Ph.D. study of information dissemination during a suspected TB outbreak. The research aimed to characterise and evaluate the dissemination of information to the community during the incident. A qualitative systematic review of the research literature identified twenty relevant studies. Meta-ethnographic analysis of these studies highlighted the key factors in effective dissemination. Consideration of these factors in relation to dissemination theory provided the links between the key factors. When the resulting model was applied to the specific circumstances of the incident two barriers to effective information dissemination were identified. Incorporating these barriers into the original model enabled the construction of a model of effective information dissemination in a crisis. The implications of this model for information professionals include incorporating social marketing as a core element of education and training and adopting multi-method dissemination strategies.

## Introduction

Whilst information dissemination has been the subject of much scrutiny, a recent PhD study highlighted the paucity of research considering the dissemination of information in crisis situations. Previous research has considered crisis situations from a management perspective (Pijnenburg & van Duin, [1990](#pijnenburg)), an information systems perspective (Pelletier & Msukwa,[1990](#pelletier)) or a risk communication perspective (MacLehose _et al._, [2001](#maclehose)) but not from a specific information behaviour perspective.

Initially, it was anticipated that the research project would produce guidelines for practitioners detailing the procedures and methods required for effective information dissemination in crises. It was anticipated that collation of the results of several studies would highlight the most consistently effective method or methods of dissemination. In the event, the qualitative systematic review conducted for this research project did not yield the anticipated result, as the findings of the studies reviewed were not directly comparable in this regard. Another method of synthesis and analysis of the review studies was required to facilitate the extraction of meaningful data from the body of research retrieved. The review studies were comparable on an interpretive level that enabled the extraction of the key factors relating to effective information dissemination from the accounts. Thus, although the research was unable to identify specific effective information dissemination methods, by using a meta-ethnographic approach to synthesis and analysis of the review studies the researcher was able to construct a model of effective information dissemination from the key factors highlighted. The application of this model to the specific circumstances of the crisis study resulted in the construction of a model of effective information dissemination in a crisis.

## The study

The crisis at the heart of the study was a large-scale outbreak of suspected tuberculosis (TB) infection. Two aspects of the information dissemination process during the suspected TB outbreak were examined. The study sought to identify the existing evidence of the effectiveness of information dissemination strategies, and also considered the effectiveness of the information dissemination effort during the incident. The broad aim of the research was to characterise and evaluate the dissemination of information to the community during the crisis. It was envisaged that characterising the dissemination of information during the suspected TB incident would involve constructing a definition of effective information dissemination, thereby requiring the identification and determination of both the nature and findings of existing research considering the effectiveness of information dissemination strategies. The evaluation of the information dissemination process during the suspected TB incident could then utilise the characterisation of effective information dissemination as a framework for the remainder of the study.

## A qualitative systematic review of the research literature

In the health field, studies investigating outbreaks of infectious diseases, similar to the early stages of the suspected TB incident, would seek to "quantify risk factors and highlight statistically significant variables", thereby enabling identification of probable sources of infection (Llewellyn, _et al._,[1998](#llewellyn)). Published research papers in the information research field, however, encompass a diversity of methodological approaches. The methodological approach taken in this project was broadly qualitative, although some elements of quantitative research were employed where appropriate. The study corresponds to Stake's ([1998](#stake)) definition of an instrumental case study, where the focus of the research is upon providing an insight into an issue, in this case the effectiveness of information dissemination strategies. The data collection method employed in the first phase of the fieldwork was a qualitative systematic review of the research literature. Unlike traditional literature reviews, which may be "no more than a subjective assessment by an expert using a select group of materials to support their conclusion", the systematic review is designed to be "_systematic_ in both the identification and evaluation of materials, _objective_ in its interpretation and _reproducible_ in its conclusions" (University of Sheffield, [1996](#sheff)). Collating the results of a number of small studies in the systematic review process increases the power of the conclusions reached. In this project it was originally anticipated that collating a number of studies, considering the effectiveness of particular information dissemination methods, would indicate whether specific dissemination methods were consistently effective regardless of the circumstances of the dissemination. The review question set for this project, therefore, was: _"To determine, for any population, the effectiveness of various information dissemination methods from the perspective of both the user and the provider of the information."_

Consideration of the initial search results, however, highlighted that the user, or recipient, of the information is not in a position to determine the effectiveness of a dissemination strategy. The effectiveness of the strategy can only be determined in relation to criteria that are set by the information provider and are generally unknown to the recipient. The user, however, will have his or her own perspective on the effectiveness of the dissemination process, which may not always concur with that held by the information provider. In addition, where, for example, the measure of effectiveness is a change in attitude, the information provider is unable to determine the outcome of the dissemination process without the cooperation of the information recipient in the evaluation process. As the systematic review being conducted here was intended to inform the subsequent stages of the study, which sought to gain an holistic picture of the information dissemination process during the suspected TB incident, the review question was amended to:

> "To determine, for any population, the effectiveness of various information dissemination methods, from criteria set by the information provider, which relate to the information recipient and incorporate the user's perspective."

From the retrieved records sixteen studies conformed to the criteria set for the review (see [Appendix A](#app-a)). A further two relevant studies were identified from a hand search of the most frequently occurring journal and finally, two studies were identified from the references of relevant papers. These twenty studies provided the foundations of the model of effective information dissemination in a crisis ultimately constructed during the study.

Although the search for relevant literature was deliberately wide-ranging (see [Appendix B](#app-b)), all but two of the studies retrieved were in the field of health information dissemination. This reflects, perhaps, the importance of effective communication of health information particularly and the need for research-based evidence in this field.

## Synthesis and analysis

The twenty relevant studies retrieved for the systematic review employed a variety of dissemination methods to inform their target populations, ranging from live presentations (Sox _et al._. [1984](#Sox)) to an interactive computer program (Llewellyn-Thomas _et al._. [1995](#llewellyn-thomas)). As the review statement deliberately did not define the particular populations that the relevant studies should serve, the populations covered by the research projects were diverse. The effectiveness of the dissemination methods employed was evaluated by the researchers in relation to at least one of three outcome measures:

1.  An increase in the recipient's knowledge of the particular issue to which the information related.
2.  A change in the recipient's attitude, e.g., reduced anxiety, towards the issue.
3.  A change in the recipient's behaviour, e.g., compliance with cancer screening tests.

The research designs employed in the studies encompassed a wide range and included such diverse designs as randomised controlled trials and observation studies. The research designs employed in the studies thus reflected the variety of outcomes sought by the researchers.

At the outset of the project it was anticipated that collation of the reported results would highlight the most effective method of dissemination. Two factors, however, prevented this. The first factor was the definition of effectiveness employed for the evaluation, and the second was the difficulty experienced by researchers in attributing changes in knowledge or behaviour to the particular method being evaluated. The determination of effectiveness had, in a number of studies, a direct impact upon the perceived success of the project. In one study, for example, the desired outcome, a change in behaviour, was not identified, and although a change in attitude was detected in one group of participants the evaluators still concluded that the dissemination method was ineffective (Graham _et al._. [2000](#graham)). Similarly, another evaluation of a dissemination strategy aiming to influence teachers' and administrators' readiness to adopt a tobacco prevention programme was not considered effective, this despite an increase in the administrators' understanding of the programme being directly attributable to one element of the strategy (Brink _et al._. [1995](#brink)). Illustrating the second factor was a study where the researchers were unable to identify significant behaviour changes in the target group that could be attributed to the specific dissemination strategy (Tietge, _et al._, [1987](#tietge)). This was due to an already high compliance rate with the anticipated behaviour and widespread diffusion of relevant information from other sources. The problem is particularly evident in large-scale information studies. Thus, an evaluation of a radio drama concluded, that "while the general population improved in the accuracy of its knowledge of AIDS and in its awareness of being at risk, these changes could not be linked to listening to the radio drama alone." (Yoder, _et al._, [1996](#yoder)).

## A meta-ethnographic approach

Qualitative data analysis generally entails reducing the volume of the data collected by categorising and then making connections between the categories. Meta-ethnography, developed by Noblit and Hare ([1988](#noblit)) retains the essence of qualitative data analysis, whilst adhering to the tenet of replicability at the heart of the systematic review process. Meta-ethnography was intended to be a "rigorous procedure for deriving substantive interpretations about any set of ethnographic or interpretive studies" (Noblit & Hare, [1988](#noblit)). The approach does not attempt to aggregate the findings of numerous studies, but instead aims to interpret the multiplicity of their findings. The key elements of the meta-ethnographic process are the determination of the relationships between the studies, the translation of the studies into one another and the synthesis of these translations. To highlight the relationships between the studies Noblit and Hare suggest creating a list of key metaphors, phrases, ideas and concepts used in each account and juxtaposing them. Translation is the vehicle used to allow comparisons to be made between the key metaphors, and a further level of synthesis is achieved by analysing types of competing interpretations and translating them into each other.

The core of the meta-ethnographic approach is the selection of the key metaphors from each account. The focus of this review was, therefore, identification of the elements of an effective dissemination strategy. This method of synthesis is an interpretive process and it is important to acknowledge that the report produced represents one reading of the accounts; other authors with dissimilar interests may read the accounts differently. However, recording an audit trail, which enables a reader to follow the actions taken will ensure that, whilst an alternative reading is possible the basis for the proposed model is readily apparent to the reader. The evidence for the key metaphors employed in this study is provided in [Appendix C](#app-c). From this evidence the reader can trace the evolution of the meta-ethnographic synthesis from the individual findings through the key concepts to the initial representation displayed in Figure 1\.

## Key concepts

One of the first factors highlighted by this process was also noted in the initial attempt to synthesise the studies; the variety of measures employed in the studies. These measures, therefore, constituted the first set of key concepts extracted from the accounts. Similarly, problems associated with attributing changes to the specific method of dissemination were noted in several of the studies and grouped together under the term evaluation.

Another set of distinct ideas extracted from the accounts related to the theme of knowledge acquisition. The concepts of an individual's need for new knowledge and their awareness of knowledge sources were identified as key factors, and, particularly in studies where a change in behaviour was sought, the individual's willingness to change as a result of new knowledge was also a significant factor. Linked to this in a number of studies was the concept of the relevance of the material disseminated as perceived by the target audience. Similarly, and in particular with newer technologies, the ability to interact with the information was an important facet of the effective information dissemination process. Underlying these concepts, however, is the need for an understanding of information seeking styles. Miller and Mangan's ([1983](#miller)) two styles of information seeking, _monitors,_ who actively seek out information when threatened with an adverse event, and _blunters_, who ignore the event by distracting themselves, have obvious implications for the effectiveness of any information dissemination strategy. Barriers and constraints to effective dissemination were noted in a number of the studies. These barriers tended to be either cultural in nature, for example taboos regarding sex, or socio-economic, for example education levels.

The final concept identified related to the information source itself and whether the information disseminated was founded upon existing research and, therefore, reinforced the recipients existing knowledge.

Extracting these concepts from the studies illustrated that the accounts were comparable and that synthesis and analysis was achievable. The next stage of the process was the translation of these key concepts.

## Translation

The real contribution of the synthesis is in determining how the key factors fit together and translate into effective dissemination. The first step in this process was to determine how the factors identified relate to each other with regard to effective information dissemination. There are two parties to the dissemination process, the information provider and the information recipient, and it was in relation to these two roles that the factors were first divided. Listing the concepts with regard to whether they related to the information provider or recipient, clarified that within each category the factors could be further defined as internal or external to each of the roles. The recipient's recognition of the need for new knowledge, for example, was internal to that individual, whilst evidence of their involvement in any dissemination strategy was readily available to the information provider. On the part of the information provider, although the cultural constraints and socio-economic barriers to effective information dissemination were applicable to many circumstances the evaluation process was specific to their particular strategy. Displaying the factors in a diagram illustrates these divisions (Figure 1).

<figure>

![Figure 1](../p178fig1.gif)

<figcaption>

**Figure 1: Factors influencing effective dissemination.**</figcaption>

</figure>

The diagram displays how the factors relate to each other, but to translate these factors into effective dissemination it is necessary to determine the pattern behind the relationships. Finding the pattern required consideration of the theoretical underpinnings of the studies. All of the studies were founded upon one of the following theories:

> *   Diffusion of innovation theory.
> *   Social cognitive theory.
> *   Social marketing.

As the theories are not mutually exclusive and share many characteristics, a number of the studies included elements of more than one theory in their research design and evaluation.

Consideration of the factors displayed in Figure 1, in relation to the theories outlined above, highlighted that no one factor was crucial for effective dissemination. Indeed, it was not possible to create a hierarchy of factors because it was the combination of factors that was important for effective dissemination. The underlying theories, however, not only provide the links between the internal and external factors, but also between the roles of information provider and information recipient. Three concepts from the theories have a direct or indirect relationship with all of the other factors. These concepts are:

> 1.  The willingness of the recipient to accept new knowledge.
> 2.  Targeting information for a specific audience.
> 3.  The role of opinion leaders.

<figure>

![Figure 2](../p178fig2.gif)

<figcaption>

**Figure 2: How the concepts link together**</figcaption>

</figure>

All of the factors must be considered as elements of an integrated whole rather than each in isolation. For the information provider, therefore, the dissemination method should be matched to the target audience, opinion leaders should be identified and utilised as a resource in the strategy, and the optimum conditions for acceptance of the new knowledge can be created by involving participants in the process. Similarly, for the information recipient the perceived relevance of the material can be influenced by the opinion leaders, information targeted at them should reflect their cultural and socio-economic backgrounds and interaction with the information will positively affect their willingness to change behaviour or attitudes.

One of the studies in the review illustrates these links. The study was an evaluation of communication strategies employed by management during the incorporation of a college of health care studies as a new faculty of a university (Whitaker, _et al._, [1994](#whitaker)). The management employed six different methods of dissemination ranging from a regular newsletter to large-scale staff meetings with workshops _( matching method with audience),_ one-to-one meetings with senior managers were available _(opinion leaders)_ and an Incorporation Advisory Group, comprised of staff from all areas at all levels, collected information on issues that worried staff and communicated management's response to staff _(involving participants in the process)._ The main area of concern for the staff in this situation related to their posts in the new structure. The newsletter and Advisory Group were rated the most useful communication methods by the staff. The newsletter contained personnel information as posts were filled in addition to details of the progress of the incorporation _(reflecting the cultural and economic background)._ The advisory group was set up to channel staff concerns to the decision-making groups; communication between the two groups was rapid and perceived as helpful _(interaction with the information)._ Finally, although staff rated the opportunity of one-to-one meetings as beneficial, these were not greatly utilised. The researchers concluded that the offer of the senior managers' time could be perceived as evidence that that the management was prepared to take the trouble to listen to the concerns of staff _(influence of opinion leaders)._ This study illustrates how consideration of the elements highlighted by the review can result in effective dissemination.

## Applying the model

### Targeting information

The model of effective information dissemination thus produced was employed as a framework for the evaluation of the information dissemination effort during the suspected TB incident. The key factors highlighted by the review informed both the interview guide and subsequent coding and categorisation of data from interviews with key informants, and also the drafting and subsequent analysis of questionnaires distributed amongst the community.

The dissemination process during the suspected TB incident followed a classical, centralized diffusion system model, whereby information is "diffused as a uniform package" (Freimuth, [1987](#freimuth)). Adoption of this model suggested that targeting information to the requirements of the various audiences in the community was less likely to occur than would be the case with a less hierarchical diffusion model. The potential audiences for the information disseminated during the incident were many and varied. The community involved in the incident included such diverse groups as school-age children, elderly residents and individuals whose first language was not English. Providing information to such diverse groups presents a challenge to any information provider, providing information to these diverse groups during a crisis would be particularly challenging.

School children were at the heart of the crisis in the community. As well as being the focus of the TB screening process, the children were also the main delivery mechanism for information, at least in the initial stages of the crisis. The information providers did acknowledge that children were a potential audience, however, no specific measures were taken to target information for this particular audience. Information providers justified these actions with their assumption that children would receive information from their parents during the incident. The responses to the children's questionnaires, however, highlighted that children were just as likely to be informed by the media coverage of the crisis, as they were by receiving information directly from their parents. Elderly residents might also have been considered a potential audience for information during the crisis. Although the initial delivery mechanism for the newsletters, via the local school children, was later modified in response to demand from the community, no other measures were specifically employed to target information for this audience. Analysis of survey data, however, indicated that elderly residents in the community did not consider the information relevant to their circumstances.

One group within the community who did require specific measures were those individuals for whom English was their second language. For this group, neither formal written communications, requiring a high level of literacy in English, nor telephone support, requiring reasonably sophisticated verbal communication skills, were appropriate methods of information dissemination. The assumption that individuals requiring further information were able to use the contact telephone number created an unnecessary barrier between the information provider and the intended information recipient.

### Opinion leaders

Diffusion research suggests that the role of opinion leaders is to persuade rather than to inform, whilst social cognitive theory contends that the importance of opinion leaders is in their 'modelling' of appropriate behaviour (Bandura, [1986](#bandura)). 'Modelled behaviour' was demonstrated in this study. Health officials, for example, informed the community that they, too, were personally affected by the abnormal test results. The 'modelled behaviour' was to adhere to the medication programme, despite the increasing uncertainty over the initial diagnosis of a TB infection. The questionnaire responses indicated that opinion leaders did persuade the respondents to accept the information, of which they had previously been informed in the newsletters.

An important aspect of the role of opinion leaders in the dissemination of information is the concept of homophilous communication, whereby communication between individuals sharing similar attributes is more effective than that between heterophilous individuals, whose attributes are dissimilar.

The heterophilous nature of communication between the health professionals and individuals who did not share the same attributes also suggests that the individuals might have identified other persons to fulfil the role of opinion leader. Communication between the children involved in the crisis and the formal information providers was especially heterophilous. In addition, those respondents in this study whose first language was not English identified a number of potential opinion leaders in the community, on the basis of their shared status and attributes in relation to the respondent and the situation that they faced.

These findings indicate that whilst opinion leaders are an important element of an effective information dissemination strategy, heterophilous communication can create barriers between information providers and information recipients.

### Willingness to accept new knowledge

The remaining factor in the model of effective information dissemination explored in relation to the crisis was the concept of the information recipient's willingness to accept new knowledge. In particular the research explored the impact of the individual's information seeking style upon the effectiveness of the information dissemination process.

Analysis of the key informant interview data identified an assumption, on the part of the information providers, that not seeking information was an indication that recipients' were satisfied with the information provided. Miller and Mangan's ([1983](#miller)) research, however, demonstrates that individual's may adopt an information blunting strategy towards a crisis, thereby choosing not to actively seek information. This does not, however, suggest that individuals adopting this information behaviour strategy are necessarily satisfied with the information they have already received. Indeed, the opposite may be true. Brashers, _et al._, ([2002](#brashers)) contend, for example, that an individual might adopt an information blunting strategy when there is a possibility that new information will conflict with their existing health beliefs. Analysis of the questionnaire data indicated that the survey respondents included both information monitors and information blunters, and that consideration of information seeking styles was an important element of an effective information dissemination strategy.

## A model of effective information dissemination in a crisis

Employing the model of effective information dissemination derived from the systematic review as a framework for the evaluation of the dissemination of information during the suspected TB incident confirmed that the three linking factors were relevant. Exploration of these elements of the model, however, also highlighted that two further elements should be incorporated in the model. These two elements are the barriers to effective information dissemination created by information providers' assumptions about their audience and the need for homophilous communication. The resulting model of effective information dissemination in a crisis is displayed in Figure 3.

<figure>

![Figure 3](../p178fig3.gif)

<figcaption>

**Figure 3: Effective information dissemination in a crisis**</figcaption>

</figure>

## Conclusion

The model of effective information dissemination in a crisis, in conceptualising effective information dissemination as a process involving both the information provider and information recipient, contrasts with the traditional expert to lay-person model of risk communication. The anticipated outcome of any information strategy is twofold—an informed audience achieved with the optimum use of resources. As the model above illustrates, there are five elements that ought to be particularly considered in a dissemination strategy. Consideration of these elements, however, has implications for information providers, which go beyond the actual dissemination process. A commitment to targeting information to a specific audience, for example, involves gathering a considerable amount of information about the intended recipients before dissemination takes place. Employing market segmentation techniques will help information providers to understand their target audience, thus enabling them to make more accurate predictions "...which in turn are prerequisites to the ability to influence outcomes." (Kotler, _et al.,_ [1989](#kotler)). Employing market segmentation techniques, however, invariably highlights the need to adopt a multi-method dissemination strategy.

Identification of the role of opinion leaders in the dissemination strategy requires that information providers should be aware of those individuals who might perform this function in a particular situation. The benefits for information providers in identifying opinion leaders are considerable, as,

> ...the opinions and behaviour of those who possess status and prestige are likely to have a greater impact upon what spreads through a social network, than the activities modelled by peripheral members. (Bandura, [1986](#bandura)).

Knowledge of the environment is a key aspect of social marketing, where the focus of the approach is upon market analysis (knowing the environment), market segmentation (targeting the intended audience) and market strategy. The model constructed in this research project suggests that the social marketing approach to information dissemination should be a core element of education and training for information professionals. Educators in the field of information studies should ensure that practitioners are trained not only to gather data about the target audience but also to manipulate the data to allow them to define their target audience accurately. Practitioners will thus be armed with the necessary tools to ensure maximum benefit for their audience for the optimum use of the information providers' resources.

## References

*   <a id="bandura"></a>Bandura, A. (1986). Social foundations of thought and action : a social cognitive theory. Englewood Cliffs, NJ: Prentice Hall.
*   <a id="brashers"></a>Brashers, D.E., Goldsmith, D.J. & Hsieh, E. (2002). Information seeking and avoiding in health contexts _Human Communication Research_ **28**(2), 258-271
*   <a id="brink"></a>Brink, S.G., Basen-Engquist, K.M., O'Hara-ompkins, N.M., Parcel, G.S., Gottlieb, N.H., & Lovato, C.Y. (1995) Diffusion of an effective tobacco prevention program: part 1: evaluation of the dissemination phase _Health Education Research_ **10**(3), 283-295
*   <a id="freimuth"></a>Freimuth, V.S. (1987) The diffusion of supportive information, In Terrance L. Albrecht and Mara B. Aldeman, _Communicating Social Support._ (pp. 212-237) Newbury Park, CA: Sage.
*   <a id="graham"></a>Graham, W., Smith, P., Kamal, A, Fitzmaurice, A., Smith, N. & Hamilton, N. (2000). Randomised controlled trial comparing effectiveness of touch screen system with leaflet for providing women with information on prenatal tests _British Medical Journal_, **320**(7228), 155-160
*   <a id="kotler"></a>Kotler, P., Roberto, E.L. & Roberto, N. (1989). _Social marketing: strategies for changing public behavior_. New York, NY: Free Press.
*   <a id="llewellyn"></a>Llewellyn, L.J., Evans, M.R & Palmer, S.R. (1998). Use of sequential case-control studies to investigate a community salmonella outbreak in Wales. _Journal of Epidemiology and Community Health_ **52**(4), 272-276.
*   <a id="llewellyn-thomas"></a>Llewellyn-Thomas, H., Thiel, E., Sem, F., & Harrison-Woermke, D. (1995). Presenting clinical trial information: a comparison of methods. _Patient Education and Counselling_, **25**(2), 97-109.
*   <a id="maclehose"></a>MacLehose, R., Pitt, G., Will, S., Jones, A., Duane, L., Flaherty, S. _et al._. (2001) Mercury contamination incident. _Journal of Public Health Medicine_ **23**(1), 18 -22
*   <a id="miller"></a>Miller, S.M. & Mangan, C.E. (1983) Interacting effects of information and coping style in adapting to gynaecologic stress: should the doctor tell all? _Journal of Personality and Social Psychology_ **45**(1), 223-236
*   <a id="noblit"></a>Noblit, G.W & Hare, R.D. (1988). Meta-ethnography: synthesizing qualitative studies. Newbury Park, CA: Sage
*   <a id="pelletier"></a>Pelletier, D.L. & Msukwa, L.A.H. (1990). The role of information systems in decision-making following disasters: lessons from the mealy bug disaster in Northern Malawi _Human Organisation_ **39**(3), 245-250
*   <a id="pijnenburg"></a>Pijnenburg, B. & van Duin, M.J. (1990). The Zeebrugge ferry disaster _Contemporary Crises_, **14**(4), 321-349
*   <a id="sox"></a>Sox, H.C., Marton, K.I., Higgins, M.C., & Hickam, D.H. (1984) Tutored videotape instruction in clinical decision making _Journal of Medical Education_ **59**(3), 189-195
*   <a id="stake"></a>Stake, R.E. (1998). Case studies, In: Norman K. Denzin & Yvonna S. Lincoln, (Eds.) _Strategies of qualitative inquiry._ Thousand Oaks, CA: Sage.
*   <a id="tietge"></a>Tietge, N.S., Bender, S.J. & Scutchfield, F.D. (1987). Influence of teaching techniques on infant car seat use _Patient Education and Counselling_, **9**(2), 167-175
*   <a id="sheff"></a>University of Sheffield. _School of Health and Related Research_, _Critical Reviews Advisory Group_ (1996) _[Introduction to systematic reviews](http://www.shef.ac.uk/scharr/triage/docs/systematic/) _. Retrieved 3 April, 2004 from the University of Sheffield, School of Health and Related Research Web site: http://www.shef.ac.uk/scharr/triage/docs/systematic/.
*   <a id="whitaker"></a>Whittaker, C., Dickinson, H., Humphreys, J. & Ramsammy, R. (1994). An evaluation of communication strategies during the process of incorporating a college of health studies into a university _Journal of Advanced Nursing_, **19**(4), 653-658
*   <a id="yoder"></a>Yoder, P.S., Horvik, R. & Chirwa, B.C. (1996) Evaluating the program effects of a radio drama about AIDS in Zambia _Studies in Family Planning_ **27**(4), 188-203

# <a id="app-a"></a>Appendix A

# Qualitative systematic review studies

Atwood, Jan R. _et al._ (1991). Acceptability, satisfaction and cost of a model-based newsletter for elders in a cancer prevention adherence promotion strategy. _Patient education and counselling_, **18**, 211-221

Bazyk, Susan & Jeziorowski, John (1989). Videotaped versus live instruction in demonstrating evaluation skills to occupational therapy students. _The American journal of occupational therapy_ **43**, 465-468

Brink, Susan G. _et al._ (1995). Diffusion of an effective tobacco prevention program: Part 1: evaluation of the dissemination phase. _Health education research_, **10** (3), 283-295

Browner, C H., Peloran, Mabel & Press, Nancy A. (1996). The effects of ethnicity, education and an informational video on pregnant women's knowledge and decisions about a prenatal diagnostic screening test. _Patient education and counselling_, **27**, 135-146

Brunham, Sandra _et al._ (1992). The effectiveness of videotapes in communicating information to rural physiotherapists. _Physiotherapy Canada_, **44**(3), 30-34

Bush, Mary, Butler, A.A. & Sabry, Jean Henderson (1977). Consumer acceptance of mailed nutrition and health information. _Canadian journal of public health_, **68**, 296-300

Graham, Wendy _et al._ (2000).. Randomised controlled trial comparing the effectiveness of touch screen system with leaflet for providing women with information on prenatal tests. _British Medical Journal_, **320**, 155-160

Lechner, L. & De Vries, H. (1996). The Dutch cancer information helpline: experience and impact. _Patient Education and Counselling_, **28**, 149-157

Llewellyn-Thomas, Hilary A. _et al._ (1995). Presenting clinical information: a comparison of methods. _Patient Education and Counselling_, **25**, 97-107

Luck, Andrew _et al._ (1999). Effects of video information on precolonscopy anxiety and knowledge: a randomised trial. _The Lancet_, **354**, 2032-2035.

McGill, Deborah & Joseph, W. D. (1997) An HIV/AIDS awareness prevention project in Sri Lanka: evaluation of drama and flyer distribution interventions. _International Quarterly of Community Health Education_, **16**(3), 237-255.

Nelson, Patricia Tanner (1986). Newsletters: an effective delivery mode for providing educational information and emotional support to single parent families? _Family Relations_, **35**, 183-188.

Rasanen, Leena, Ahlstrom, Antti & Rimpela, Matti (1974). Pretesting the channels of distribution for a nutrition education leaflet. _Scandinavian Journal of Social Medicine_, **2**, 135-140.

Rimer, Barbara K _et al._ (1999). The impact of tailored interventions on a community health center population. _Patient Education and Counselling_, **37**, 125-140.

Schofield, Margot J, Edwards, Kim & Pearce, Robert (1997). Effectiveness of two strategies for dissemination of sun-protection policy in New South Wales primary and secondary schools. _Australian and New Zealand journal of public health_. **21**(7), 743-750.

Sox, Harold C _et al._ (1984). Tutored videotape-instruction in clinical decision making. _Journal of Medical Education_, **59**, 189-195.

Tietge, Nancy S., Bender, Stephen J. & Scutchfield, F. Douglas (1987). Influence of teaching techniques on infant car seat use. _Patient Education and Counselling_, **9**, 167-175.

Turner, Bonnie J., Martin, Garth W. & Cunningham, John A. (1998). The effectiveness of demonstrations in disseminating research-based counselling programs. _Science Communication_, **19**(4), 349-365/p>

Whitaker, Carol _et al._ (1994). An evaluation of communication strategies during the process of incorporating a college of health studies into a university. _Journal of Advanced Nursing_, **19**, 653-658.

Yoder, Stanley, Horvik, Robert & Chirwa, Ben C. (1996). Evaluating the program effects of a radio drama about AIDS in Zambia. _Studies in Family Planning_, **27**(4), 188-203.

* * *

# <a id="app-b"></a>Appendix B

# Extraction of key factors

## Measures of effectiveness

### Changes in behaviour

Sharing knowledge with others (1, 13)  
Intrafamily discussions about risk (3)  
Compliance with treatment regime (8, 6)  
Compliance with test programme (10, 19)  
Policy adoption (2)  
Proper and consistent use of child safety seats (11)

### Change in attitude

Increased sense of risk (3, 1)  
Impact on anxiety levels (12, 19, 20)  
Willingness to implement new programme (13, 15)  
Increased confidence in abilities (7)  
Perceived benefits of trial / change (9, 14)  
Decreased feeling of professional isolation (6)

### Change in knowledge

Topic recall (4, 7, 16, 18)  
Understanding of purpose of test/ trial (14, 19)  
Awareness of treatment advances (6)  
Increased knowledge of assessment tool (5)  
Increased knowledge of assessment decision (17)  
Increased knowledge of risk behaviour (3)  
Knowledge change before and after event (1)

### Cost-effectiveness

Cost effectiveness of method (8)  
Cost effectiveness of programme (13)  
Reduced programme costs (7)

## Evaluation

Study design limited by ethical concerns (14)  
Aversion to randomisation (14)  
Difficult to show further positive movement (13)  
Population did not respond dramatically (11)  
Extended community participation increased awareness (1)  
Changes could not be linked to intervention alone (3)  
Importance of other factors outwith study(12)  
Participants already had a positive attitude (12)  
Earlier campaigns and population's positive attitude (16)  
A sensitive measure of degree of awareness (18)  
Difficult to exclude other confounders (2)  
High participation due to charging structure (15)  
Both groups showed improvements (19)

## Cultural constraints

Social sanctions against reported behaviour (3)  
Cultural reluctance to discuss topic openly (1)  
Conflict between message and traditional habits (16)  
Black women less likely to be advised (10)  
Concept virtually absent in traditional Mexican culture (4)  
Lack of clear role for teachers (13)  
Adapting to new organizational culture (9)

## Socio-economic factors

Literacy and education levels (1, 3, 4, 8, 11, 16, 18)  
Access to mass media (1, 3, 18)  
Community participation (18)  
Age of recipients (8)  
Gender of recipients (7, 10, 14, 20)  
Ethnic origin (4, 7, 10, 11)

## Other sources of information

Primary sources - TV and newspapers (1)  
Province had numerous ways to learn (3)  
Previous sources of information on topic (18)  
Previous interest in topic (15, 17)  
Interpersonal channels (8)  
Baseline level of knowledge already high (11, 19)  
Alternative ways of getting information (13)  
Respondents obtained answers by other means (9)

## Reinforcement of existing knowledge

Audience had a good understanding before (1)  
Reinforcing existing knowledge (18)  
Recall derived from previous interest in topic (16)  
Improvement from a high starting point (19)  
Schools receive a vast amount of information (2)  
Legally required to give patients booklet (4)  
National and state effort to increase awareness (11)  
Controversy surrounding widespread use of test (4)  
Focus groups provided local knowledge (3)

## Research based information

Topics evolved from research literature (7)  
One of the most intensively researched subjects (19)  
Interventions that had good empirical support (10)  
Topics based on an established model (8)  
Cost-effective, research based treatment (15)  
Based on recommendations by Medical Boards (16)  
Constructed from pre-existing literature (1)  
Consistent with messages drawn from research (3)  
Information derived from a clinical trial (14)  
The script addressed theoretical constructs (13)

## Perceived relevance of the information

Highlights women's receptiveness to information (19)  
The materials were 'meant for me' (10)  
Demonstration had increased their interest (15)  
Intended to read it later (16)  
A third elected not to accept the information (18)  
Generally held belief that AIDS is a foreign problem (1)  
Someone they knew had AIDS (3)  
Think that students would not co-operate (2)  
Already aware of preventative behaviour (11)  
Thought a lot before deciding (4)  
See the information as more relevant to them (13)  
Wanted information specific to their situation (12)  
'It voiced my own views' (7)

## Interaction with the information

Found it easier to interrupt the tape to ask (17)  
Did not like limited interaction with instructor (5)  
Influential because colleagues had been involved (15)  
89% planned to share their information (1)  
Session included actual practice by the subject  
Respondent actively involved in information search (14)  
Both groups had expectations from their interaction(12)  
Enthusiastic about one-to-one meetings (9)  
Students disliked the restriction (5)  
Format associated with a more positive attitude (14)

## Participant involvement in strategy

If given a choice would prefer live instruction (5)  
Counsellor may have helped to draw attention (10)  
Community leaders asked for intervention (1)  
Church leaders asked to tell congregations (3)  
Encourage physicians to take an active role (11)  
Strategies were two-way processes (9)  
Representatives asked to show and discuss video (13)  
Opinion leaders attendance increased discussion (13)

## Recognition of need for new knowledge

Previous experience did affect anxiety scores (20)  
Women perceived the problem as much greater (1)  
Considered the most serious health problem in area (3)  
Issue not seen as sufficiently high priority (2)  
Feeling obligated to learn all that they can (4)  
Gaining new ideas scored the lowest (6)  
Receptivity was extremely favourable (13)  
Four main areas of concern (9)  
I think something is only my problem (7)  
Lack of access to new ideas is a big problem (6)  
Claimed to always read the pamphlets (18)  
Opportunity to ask questions if unclear (5)

## Information seeking style

Preference for shared decision making (14)  
Most said they had only skimmed it (4)  
A greater need for interpersonal feedback (12)  
Prefer live demonstration as it is more reliable (5)  
Effect of information on anxiety is controversial (20)  
Demonstrations may persuade adopters (15)  
People tend to retain leaflets (16)  
Chose not to accept information by ignoring (18)  
Concept of 'teachable' moment (11)  
Refusers tended to be women (14)  
Do not incorporate the information (13)  
Elected not to use workshops or meetings (9)

## Awareness of information sources

More aware if information provided orally (15)  
Supported by similar information via other media (16)  
Had heard of guide before reading pamphlet (18)  
Learned primarily from the media (1)  
Receiving information from many sources (3)  
Already aware of preventative behaviours (11)  
Feeling obligated to learn all they can (4)  
Most became aware through media channels (13)  
Obtained answers by other means (9)  
Would not have easy access to expert knowledge (6)

## Willingness to change as a result of new information

Demonstration had influenced their interest (15)  
Claimed to have discussed at least one pamphlet (18)  
Shift towards seeking the clergy out for advice (1)  
Difficulty of transposing unit setting to rural setting (6)  
Staff were positive about impending incorporation (9)  
A number of attitudes and behaviours were changed (7)  
Self-efficacy was targeted continually (8)  
Respondents able to use their own strategies (14)  
Thought a lot before deciding to take test or not (4)  
Indicative of what people think they should be doing (3)  
Receptivity towards programme very favourable (13)

* * *

# <a id="app-c"></a>Appendix C

# Systematic review search list

## Databases

ANTE  
Arts and Humanities Index  
ASSIA  
ASTI  
British Education Index  
Cinahl  
Dissertation Abstracts Online  
ERIC  
Information Science Abstracts  
Library Literature  
LISA  
Medline  
Psyclit  
Social Sciences Citation Index  
Sociological Abstracts  
Wilson Social Science Abstracts

## Web-based searches

BUBL  
Ingenta Journals  
Library Management  
OMNI  
SOSIG  
Uncover  
University Web sites  
Update-software.com

## Miscellaneous sources

BMJ customised alert service  
Contents direct table of contents service  
LIS - medical mailing list  
UNN library catalogue