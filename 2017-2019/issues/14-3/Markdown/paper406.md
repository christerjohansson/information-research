#### vol. 14 no. 3, September, 2009

* * *

# Propuesta de indicadores métricos para gestores sociales de noticias: análisis de la prensa digital española en _Menéame_

#### [Enrique Orduña-Malea](#authors) and [José-Antonio Ontalba-Ruipérez](#authors)  
Instituto de Diseño y Fabricación, Universidad Politécnica de Valencia, Spain

#### Resumen

> **Introducción.** La prensa digital quiere posicionarse en los Gestores Sociales de Noticias o GSN (Digg, Menéame, etc.) para ganar visibilidad y aumentar el tráfico de sus webs. Este trabajo pretende proponer unos indicadores bibliométricos que calculen la presencia e impacto de la prensa en los GSN.  
> **Método.** Se propone el ICP (Impacto de Comentarios Ponderado) como indicador del impacto de un medio a lo largo de un mes a partir de los comentarios recibidos, y el FPM (Factor de Poder del Medio) como indicador global del impacto del medio a lo largo de un mes. Para la validación de dichos parámetros se realiza un análisis de contenido descriptivo longitudinal retrospectivo de las noticias remitidas al GSN Menéame durante 16 meses.  
> **Resultados.** La validación del ICP muestra a El País como el medio con mayor impacto en comentarios, seguido de 20 Minutos y El Mundo, hecho que concuerda con el ranking de comentarios totales recibidos. La validación del FPM, en cambio, indica que 20 Minutos es el medio con un valor más alto, seguido por El País y El Mundo.  
> **Conclusiones.** Los parámetros propuestos son válidos para calcular el grado de impacto de la prensa digital en los GSN.

#### [Abstract in English](#ingles)

## Introducción

La Web ha provocado un impacto irreversible en los distintos sistemas tradicionales de publicación, cuyas limitaciones en rapidez, puntualidad, gestión y distribución, entre otras, son cada vez más evidentes.

Disponer de una copia impresa ya no es un requisito para leer o enviar un documento textual, esto provoca que aparezcan nuevas formas de difusión y distribución de la información ([Hane y Pitkow 2005](#han05)), permitiendo de este modo la aparición de nuevos modelos de negocio ([López Sánchez 2007](#lop07)), pese a que las tipologías documentales (que no formatos) que las explicitan prácticamente no han cambiado.

Por otra parte, el hecho de que todos los sistemas de publicación se vean obligados en la Web a publicar en un mismo soporte (disco duro) documentos legibles por máquina provoca la pérdida de las fronteras tradicionales entre los distintos tipos de documentación, tanto en función de su soporte (papel, cinta magnética, DVD, etc.), como de sus formatos de presentación fuera de línea (libro, revista, periódico, etc.).

Debido a esto, los distintos sistemas de publicación, pese a seguir siendo diferentes en su esencia y motivación, comienzan a converger y a presentar ciertos aspectos comunes.

Entre los distintos sistemas de publicación informativa (radio, televisión y prensa), esta convergencia es todavía más acusada, pues su diferencia fuera de línea se basa en el formato de comunicación. De hecho, "hoy, un periódico, una radio o una televisión en la Red llegan a parecerse tanto entre sí, que comienza a resultar anacrónico seguirles llamando según su viejo nombre" ([Albertos 2001](#alb01)).

La prensa ha tomado cierta ventaja con respecto a los demás medios tradicionales -radio y televisión- tanto en presencia como en protagonismo dentro de la Red, donde la lucha por la visibilidad es esencial. Esta ventaja se debe, en parte, a las limitaciones en ancho de banda y factores de compresión ([Jiménez López _et al._ 2002](#jim02)) que impedían hasta hace poco el adecuado desarrollo de la televisión y la radio en la Web.

Debido a la gran presencia, audiencia, expansión y grado de desarrollo de la prensa digital, y teniendo en cuenta la comentada convergencia que se viene produciendo entre distintos sistemas de publicación, su análisis puede servir de referente para conocer las futuras tendencias y modificaciones que puedan sufrir otros sistemas de publicación, como por ejemplo el científico.

En ese sentido, el presente de la prensa digital pasa ineludiblemente por conocer su presencia en la Web social, pues ésta es el presente de Internet.

De esta forma, el análisis de la presencia de la prensa digital española en la Web social, así como la propuesta y estudio de distintos métodos para llevar esto a cabo, son los propósitos generales de este trabajo.

### Recopilación de noticias en la Web social

La llegada de la Web social provoca una evolución de los servicios de recuperación de noticias debido a la conjunción de dos factores: el desarrollo de unas herramientas y aplicaciones web preparadas para describir y compartir contenidos, y la aparición de un conjunto de usuarios, cada vez mayor y más influyente, que las solicita.

Según Lerman ([2008](#ler08)), estas aplicaciones se caracterizan por los siguientes elementos:

*   Los usuarios crean información.
*   Los usuarios comentan la información, con palabras clave o etiquetas.
*   Los usuarios evalúan el contenido, de forma pasiva o activa.
*   Los usuarios crean redes sociales asignando a otros usuarios con intereses afines como amigos o contactos.

Estas características (sobre todo las tres primeras) provocan la aparición de una nueva subclase de servicios de recopilación de noticias, que se distingue de las anteriores (bases de datos, _clipping_ y buscadores) en tanto que las empresas que proporcionan las noticias a los usuarios no son empresas que actúan como intermediarias, sino que los mismos usuarios se encargan de recopilarlas y ofrecerlas a los demás lectores (gracias al desarrollo del _software_ comentado anteriormente).

Estos "servicios de recopilación social de noticias", en la medida que han ido adoptando las características mencionadas por Lerman, han evolucionado desde su aparición. Se proponen las siguientes fases:

#### Fase I. Estructuración automática, recopilación manual y publicación controlada: canales de noticias

Corresponde al fenómeno conocido como redifusión y agregación de contenidos y no debe confundirse con los servicios de agregación de contenidos realizados por intermediarios.

Es un servicio automático, pues la noticia, publicada en un medio de información cualquiera, se describe y estructura automáticamente mediante un esquema de metadatos de noticias (como la familia RSS o _Atom_) a través de un _script_ que se inserta en el código fuente del propio medio.

Es también un servicio social, pues son los propios usuarios los que se suscriben a los canales de noticias que les interesan para recibir posteriormente las descripciones bibliográficas de las mismas (e incluso el texto completo) en ciertas aplicaciones (agregadores de noticias) que ellos controlan, gestionan e incluso comparten.

Finalmente, es un servicio de publicación controlado, pues se depende de los canales de noticias que, desde el origen, los diferentes medios decidan que pueden redifundirse.

La utilización de sistemas de sindicación de contenidos mediante RSS aparece en la prensa española en Internet en 2003 en el diario _[El Mundo](http://www.elmundo.es/)_ y la mayoría de medios ya ofrecen hoy esta funcionalidad ([Guallar 2007](#gua07)), aunque su utilización por el público no está siendo tan amplia como se esperaba ([Franganillo 2007](#fra07)).

#### Fase II. Estructuración y recopilación manual, publicación controlada: recopiladores sociales de noticias (RSN)

Esta fase se distingue de la anterior en que los metadatos que describen la noticia ya no son creados de forma automática en el origen (en el medio), sino por los propios usuarios en el destino.

La publicación es controlada, pues existe un equipo de edición que decide, en función de la calidad, importancia o veracidad de la noticia, entre otras consideraciones, si aprueba o no su publicación.

Una característica fundamental es que el servicio es totalmente abierto y, además, funciona en su conjunto como una obra de referencia: es una recopilación social de descripciones bibliográficas de noticias.

El servicio pionero fue _[Slashdot](http://slashdot.org/)_, creado en septiembre de 1997, siendo su equivalente en España _[Barrapunto](http://barrapunto.com/)_ en 2000\. Estos servicios se convierten en mecanismos de creación de esferas públicas en línea ([Baoill 2000](#bao00) ; [Poor 2005](#poo05)).

Este carácter abierto y su creciente popularidad y alto nivel de visitas, provoca la aparición del llamado "efecto _slashdot_", fenómeno por el que se colapsa una web a causa de la cantidad de visitas en poco tiempo redirigidas desde este servicio ([Adler, 1999](#adl99) ; [Notess 2007](#not07)).

#### Fase III. Estructuración, recopilación y publicación social: editores sociales de noticias (ESN)

Supone la adaptación completa de estos modelos de comunicación periodística a la Web 2.0\. Los usuarios, además de recopilar y describir las noticias, las ponderan mediante comentarios y votos decidiendo así su publicación o rechazo: las noticias se publican por promoción democrática; son editores sociales de noticias.

El primer servicio de este estilo surge a la luz a finales de 2004 con _[Digg](http://digg.com/)_. El éxito de este servicio lleva a la publicación de su versión española, _Menéame_, en diciembre de 2005.

#### Fase IV. Tendencias futuras

La vertiginosa velocidad con la que estos servicios aparecen, evolucionan y caen en desuso en Internet hace difícil evaluar las posibles fases futuras de los servicios de recopilación de noticias. Sin embargo, destacan dos servicios que denotan posibles tendencias futuras.

Por una parte destaca _[CoRank](http://es.corank.com)_, servicio donde cada usuario se crea su propia recolección de noticias que comparte con otros usuarios. Este servicio es similar al que proporcionan los agregadores de contenidos de canales de noticias (que también permiten compartir las suscripciones mediante un estándar de metadatos llamado _[OPML](http://www.opml.org/)_), con la salvedad de que las noticias no llegan por redifusión, sino por recopilación y descripción del propio usuario.

Por otra parte, destaca el reciente _Yahoo! Buzz_, servicio que mezcla características de _Digg_ y _Menéame_, junto con las de los buscadores gratuitos como _[Google News](http://news.google.com)_ o el propio buscador de _Yahoo!_ Podría encuadrar en la categoría de buscador social gratuito de noticias.

En adelante se usará la denominación GSN (gestores sociales de noticias) para designar de forma conjunta tanto a los RSN como a los ESN.

### Estudios previos

Los estudios realizados, a fecha de hoy, de los GSN se podrían dividir en las siguientes áreas:

**a) Trabajos descriptivos/recopilatorios**

Estudios centrados en la explicación de una o varias herramientas, desde la óptica de la Web 2.0\. Suelen ser trabajos poco analíticos y provenientes de medios de prensa ([Cohn 2007](#coh07b) ; [Notess 2007](#not07)) y/o blogs, fundamentalmente.

**b) Estudios de usuarios**

Esta área de estudio tiene tres objetivos claramente diferenciados. En primer lugar, conocer el comportamiento de los usuarios desde un punto de vista sociológico. En segundo lugar, usar el conocimiento anterior para mejorar los algoritmos de publicación de noticias en portada (pese a que éstos se suelen ocultar).

Finalmente, conocer al usuario lo máximo posible para poder realizar campañas posteriores de marketing y publicidad, completamente personalizadas.

Los primeros trabajos publicados acerca de este tema se centran en _Slashdot_ ([Poor 2005](#poo05) ; [Kaltenbrunner _et al._ 2007](#kal07)) y posteriormente en _Digg_.

Respecto a éste, destacan los exhaustivos estudios realizados por Lerman ([2008](#ler08)), fundamentalmente orientados a la propuesta de modelos matemáticos con los que predecir la variación del número de votos que recibe una noticia con el tiempo, así como la variación del _ranking_ de un usuario.

Asimismo, se destaca la propuesta de un factor de caída de la atención de los usuarios ante noticias nuevas basado en el número máximo de votos que éstas alcanzan en un intervalo de tiempo ([Wu y Huberman 2007](#wu07)).

**c) Estudios temáticos, de contenidos o tags.**

Esta área de trabajo se centra en el estudio de las etiquetas o tags mediante las que los usuarios describen las noticias que envían al GSN.

Pese a que a nivel internacional no se detecta ningún estudio al respecto, a nivel español se puede destacar el _Departamento de Lenguajes y Sistemas Informáticos_ de la _Universidad de Sevilla_ ([De la Rosa 2007](#ros07)), orientados al estudio de _Menéame_.

Estos trabajos suponen un comienzo en la aplicación de ciertos conceptos de la Bibliometría en gestores sociales de noticias.

Se detecta, no obstante, una carencia de trabajos orientados al estudio de la fuente (autoría) de las noticias y, por tanto, de la presencia de la prensa digital.

En este sentido se puede reseñar el estudio realizado por _esCiudad.com_ ([2007](#esc07)), que se queda en los datos y apenas profundiza, los trabajos en curso de Eva Ferreras ([2008](#fer08)) y diversas estadísticas ofrecidas desde en el propio blog de _Menéame_ ([2007](#men07)).

Recientemente ([Orduña-Malea y Ontalba-Ruipérez 2008](#ord08)), se ha realizado un primer estudio centrado en la presencia de la prensa digital española en _Menéame_ (figura 1), el GSN con mayor impacto y audiencia en España, a partir de métodos cuantitativos (número de noticias publicadas por medio) y cualitativos (promedio de votos y comentarios por noticia), asumiendo que los votos y los comentarios recibidos son un primer indicio de la calidad, o al menos el impacto, que una determinada noticia, y por tanto un determinado medio, tiene en un GSN.

<figure>

![Figura 1: Portada de Menéame (fuente: Menéame)](../p406fig1.jpg)

<figcaption>

**Figura 1: Portada de Menéame (fuente:_[Menéame](http://www.meneame.net)_)**</figcaption>

</figure>

### Influencia de los GSN en los medios de prensa

Tal es el poder de los principales GSN y el interés de la prensa en luchar por el espacio web que, en apenas unos años, ésta ha pasado de intentar evitar que sus contenidos se difundieran sin su permiso a que prácticamente todos los periódicos digitales más importantes ya incluyan, a través de un _plug-in_, enlaces en sus noticias para que puedan ser enviadas directamente a los gestores sociales de noticias y, así, conseguir un mayor tráfico ([Ferreras 2008](#fer08)).

En España, este hecho todavía es más significativo en medios como _[El País](http://www.elpais.com/)_, si se tiene en cuenta que entre noviembre de 2002 y junio de 2005 había sido el único medio español que había decidido cerrar el acceso libre a su sitio haciéndolo completamente de pago ([Guallar 2007](#gua07)).

Este cambio radical en la política de los medios de prensa viene motivado por el aumento potencial de visitas que puede recibir un sitio web cuando una noticia publicada en dicho sitio ha logrado aparecer en la portada de algún GSN.

El pionero en este aspecto, como se indicó al principio, fue _Slashdot_. Con posterioridad se ha repetido con diferente intensidad en los demás GSN provocando la aparición del "efecto _Digg_", "efecto _Menéame_", etc.

Con el objetivo de disponer de un nombre común a dicho fenómeno de forma independiente al servicio que lo provoque, en este trabajo se denominará en adelante "efecto GSN". En otros trabajos ([Notess 2007](#not07)) este efecto es definido como _DDoS attack (Distributed Denial of Service)_, definición centrada en la consecuencia técnica del fenómeno, pero que no hace referencia a su origen en un GSN, sino a cualquier ataque al servidor.

La cantidad de visitas que recibe el medio, a riesgo de colapsar el servidor si se producen en gran número y breve espacio de tiempo es, hoy por hoy, el principal aval de los medios para justificar precios por publicidad.

De esta forma, los medios intentan aparecer en los distintos GSN bajo la hipótesis de que existe una mayor probabilidad de ganar futuros lectores si aumenta el número de noticias suyas éstos. Conjetura, por otra parte, todavía no comprobada.

Siguiendo con este razonamiento, no es de extrañar que los propios medios comiencen a plantearse la realización de campañas de publicidad para anunciarse en los GSN ([Dans 2008a](#dan08a)). La publicidad en estos servicios es un punto de reflexión importante de cara al futuro.

### Presencia de la prensa en un GSN y su análisis métrico

Debido al creciente interés de los diferentes medios por aparecer en los distintos GSN como método para obtener mayor visibilidad y, posiblemente, mayor audiencia en sus sedes web, la disponibilidad de unos indicadores que permitan medir la mayor o menor presencia e impacto de las noticias de un medio en un GSN se hace cada vez más necesaria.

Un GSN, como se ha visto, forma hoy parte del sistema de publicación informativo, mientras que los indicadores bibliométricos son válidos fundamentalmente para el sistema de publicación científico, pues asumen unos supuestos ([Sancho 1990](#san90)) que hacen comprensibles sus cálculos y soluciones.

No obstante, debido entre otras cosas a la convergencia entre los sistemas de publicación que se produce en la Web, tal como se reseña al comienzo, resulta comprensible pensar que la aplicación de estos indicadores es posible en un GSN, con las modificaciones y salvedades oportunas.

La introducción de indicadores bibliométricos en los GSN, como se ha visto anteriormente, es un aspecto hasta el momento poco abordado por la literatura, donde abundan más los estudios basados en índices de productividad.

Así como el número de noticias publicadas de un medio es un indicador de la productividad (o presencia) de un medio y el número de comentarios y votos que las noticias reciben son indicadores cualitativos tanto de una supuesta calidad de las noticias como de una mayor actividad de los usuarios, se precisan indicadores que permitan medir el impacto que los medios presentan en el GSN.

La utilidad de estos indicadores radica en la posibilidad de establecer un valor del impacto que las noticias de un determinado medio alcanzan en un GSN para poder comparar éste con otros indicadores (como la audiencia web, por ejemplo), de una forma estandarizada y no basada únicamente en aspectos meramente cuantitativos.

De esta forma, los objetivos que se plantea este trabajo son los siguientes:

- Proponer dos indicadores del impacto de un medio de prensa en un GSN, basados en la equivalencia noticia-artículo y medio-autor:

a) El ICP (impacto de comentarios ponderado), como indicador del impacto de un medio a lo largo de un mes, a partir del cálculo del Hcn (índice de comentarios a noticias), parámetro basado en el índice h pero aplicado a los comentarios que reciben las noticias de un determinado medio durante un mes.

b) El FPM (factor de poder del medio), como indicador global del impacto de un medio a lo largo de un mes, teniendo en cuenta tanto aspectos cuantitativos (número de noticias), como cualitativos (votos y comentarios recibidos).

- Validar los parámetros anteriores mediante su aplicación a las noticias provenientes de medios de prensa españoles publicadas en el GSN de mayor audiencia en España (_Menéame_), entre enero de 2007 y abril de 2008.

## Material y métodos

En primer lugar se detallan los indicadores propuestos en este trabajo (ICP y FPM) y finalmente la metodología empleada para su validación.

### Impacto de comentarios ponderado (ICP)

El índice h ([Hirsch 2005](#hir05)) es un indicador reciente que pretende medir el impacto de la productividad científica de un autor.

Un científico tiene un índice h si "h" de sus N artículos publicados tiene al menos h citas cada uno y el resto de artículos (N-h), tiene menos de h citas recibidas cada uno de ellos.

De este modo, para el autor mostrado en la tabla 1, tras ordenar sus artículos por orden decreciente de citas recibidas, su índice h sería igual a 4.

<table><caption>

**Tabla 1: índice h para un autor científico**</caption>

<tbody>

<tr>

<th>Autor X</th>

<th>Citas recibidas</th>

</tr>

<tr>

<td>Artículo 1</td>

<td>20</td>

</tr>

<tr>

<td>Artículo 2</td>

<td>16</td>

</tr>

<tr>

<td>Artículo 3</td>

<td>14</td>

</tr>

<tr>

<td>Artículo 4</td>

<td>8</td>

</tr>

<tr>

<td>Artículo 5</td>

<td>4</td>

</tr>

</tbody>

</table>

Debido a la facilidad de cálculo y a la debilidad de muchos profesionales por los _rankings_, el índice h tiene una gran aceptación y su aplicación y uso se expande cada vez más ([Rodríguez Navarro e Imperial Ródenas 2007](#rod07)).

Así, no es de extrañar que una de las primeras experiencias en aplicar indicadores bibliométricos en la Web social haya estado basada en el mismo, a partir de la propuesta del llamado Hc ([Cabezas 2008](#cab08)), como indicador para evaluar el impacto de los comentarios de un blog y, por tanto, del impacto del mismo en la comunidad.

No obstante, el índice h tiene una serie de limitaciones importantes, ya conocidas por los profesionales, basadas en que autores con carreras científicas muy diferentes pueden alcanzar un mismo índice. La tabla 2 muestra un ejemplo:

<table><caption>

**Tabla 2: Comparativa de carreras científicas con mismo índice h**</caption>

<tbody>

<tr>

<th>Artículos</th>

<th>Citas recibidas (Autor 1)</th>

<th>Citas recibidas (Autor 2)</th>

<th>Citas recibidas (Autor 3)</th>

</tr>

<tr>

<td>Artículo 1</td>

<td>20</td>

<td>20</td>

<td>20</td>

</tr>

<tr>

<td>Artículo 2</td>

<td>16</td>

<td>16</td>

<td>16</td>

</tr>

<tr>

<td>Artículo 3</td>

<td>14</td>

<td>14</td>

<td>14</td>

</tr>

<tr>

<td>Artículo 4</td>

<td>8</td>

<td>8</td>

<td>8</td>

</tr>

<tr>

<td>Artículo 5</td>

<td>4</td>

<td>4</td>

<td>4</td>

</tr>

<tr>

<td>Artículo 6</td>

<td>3</td>

<td>0</td>

<td>-</td>

</tr>

<tr>

<td>Artículo 7</td>

<td>3</td>

<td>0</td>

<td>-</td>

</tr>

<tr>

<td>Artículo 8</td>

<td>3</td>

<td>0</td>

<td>-</td>

</tr>

</tbody>

</table>

Estos tres autores tendrían un mismo índice h (igual a 4), pero con carreras muy diferentes.

Esta cuestión, ya tratada en diversas publicaciones ([Egghe 2006](#egg06);[Rousseau 2006](#rou06)), se plantea en el foro ISSI ([Martínez _et al._ 2006](#mar06)) donde, de forma privada, varios profesionales aportan las siguientes consideraciones:

- La idea detrás del índice h es la consistencia de los artículos de un autor, más allá del número de citas recibidas.

- Una buena idea sería poder encontrar un índice similar a la desviación típica, pero en el caso del índice h.

Tomando en consideración estas ideas, en este trabajo se propone un indicador basado en el índice h pero aplicado a los comentarios que recibe una noticia de un medio de prensa en un GSN a lo largo de un mes; a este indicador se le aplica un índice de corrección para minimizar las limitaciones vistas anteriormente.

El impacto de comentarios ponderado (ICP) se expresa mediante dos parámetros: el índice de comentarios propiamente dicho y un factor de corrección mostrado entre paréntesis:

_ICP<sub>x</sub> = H<sub>CN<sub>x</sub></sub>(fc<sub>x</sub>)_

Donde _H<sub>CN<sub>x</sub></sub>_ (índice de comentarios a noticias) corresponde al equivalente al índice h aplicado a los comentarios de recibidos por las noticias de un medio en un GSN a lo largo de un mes "x". El parámetro _fc_ expresa un factor de corrección o ponderación.

Por tanto, un medio de prensa tiene un índice de valor _H<sub>CN<sub>x</sub></sub>_, si "_H<sub>CN<sub>x</sub></sub>_" de sus _N<sub>M<sub>x</sub></sub>_ noticias publicadas a lo largo de un mes, tiene al menos _H<sub>CN<sub>x</sub></sub>_ comentarios y el resto de noticias _(N<sub>M<sub>x</sub></sub> — H<sub>CN<sub>x</sub></sub>)_ tienen menos de _H<sub>CN<sub>x</sub></sub>_.

El factor de corrección, por su parte, sirve para ponderar la importancia de los trabajos con un número de comentarios menor a _H<sub>CN<sub>x</sub></sub>_.

En este trabajo se plantean dos correctores, llamados "a" y "b", que se describen a continuación:

_a) Factor de corrección "a":_

Se calcula simplemente mediante la media de comentarios para las noticias con un número de comentarios menor a _H<sub>CN<sub>x</sub></sub>_ para el mes "x":

<figure>

![equation](../p406eq6.gif)</figure>

Este factor de corrección oscila entre los valores [0 _H <sub>CN<sub>x</sub></sub>)_], donde:

<figure>

![equation](../p406eq7.gif)

<figcaption>

= sumatorio de comentarios de aquellas noticias cuyo número de comentarios sea inferior a _H<sub>CN<sub>x</sub></sub>_ en cada una de ellas;</figcaption>

</figure>

<figure>

![equation](../p406eq5.gif)

<figcaption>

= número de noticias que alcanzan un número de comentarios inferior a _H <sub>CN<sub>x</sub></sub>_.</figcaption>

</figure>

En caso de que _(N<sub>M<sub>x</sub></sub> — H<sub>CN<sub>x</sub></sub>_ ![equation](../p406eq5.gif) sea igual a "0", el factor de corrección "a" es nulo (que no 0), y se expresa mediante el símbolo (-).

_b) Factor de corrección "b":_

Este factor de corrección pretende contemplar el porcentaje de noticias que quedan por debajo del umbral _H <sub>CN<sub>x</sub></sub>)_ respecto del total de noticias publicadas en dicho mes:

<figure>

![equation](../p406eq8.gif)</figure>

Este parámetro oscila entre los valores [0 1]. En caso de ser "1", significa que todas las noticias tienen al menos _N<sub>M<sub>x</sub></sub>_ comentarios cada una.

Tomando como base el caso planteado en la tabla 2, se puede calcular el ICP en un GSN, aplicando las siguientes equivalencias y asumiendo sus diferencias:

Autor - Medio.  
Artículo - Noticia.  
Cita recibida - Comentario recibido.  

De esta forma, se obtendrían los siguientes valores (tabla 3):

<table><caption>

**Tabla 3: Impacto de comentarios ponderado**</caption>

<tbody>

<tr>

<th>Medio</th>

<th>ICP(a)</th>

<th>ICP(b)</th>

</tr>

<tr>

<td>1</td>

<td>4(3.25)</td>

<td>4(0.5)</td>

</tr>

<tr>

<td>2</td>

<td>4(1)</td>

<td>4(0.5)</td>

</tr>

<tr>

<td>3</td>

<td>4(4)</td>

<td>4(0.8)</td>

</tr>

</tbody>

</table>

Como se observa, el factor de corrección "a" pondera el valor del _H <sub>CN<sub>x</sub></sub>_ y lo adecua a la actividad por debajo del mismo, premiando el impacto y la actividad para las noticias con un número de comentarios inferior a _H <sub>CN<sub>x</sub></sub>_ .

Además, el hecho de estar calculado sólo para las noticias a lo largo de un mes, permite que no premie a los medios con una trayectoria más larga, tal como ocurre en el índice h (hecho que pondera el índice M mediante la consideración de los años de ejercicio científico de los autores).

El factor de corrección "b", por su parte, premia lo contrario, es decir, es mayor cuantos menos artículos por debajo del _H <sub>CN<sub>x</sub></sub>_ (en teoría, los menos citados) se poseen.

Pese a que el indicador ICP sirve para evaluar el impacto de un medio en un GSN durante un mes, se propone igualmente la aplicación de dicho factor de ponderación para el índice h original.

### Factor de poder del medio (FPM)

El Impacto de Comentarios Ponderado, no obstante, refleja una parte únicamente de la importancia de un medio, dejando de lado el otro valor cualitativo estudiado con anterioridad: los votos.

En este punto, se propone el cálculo de un nuevo indicador, llamado Factor de Poder de un Medio (FPM), con el objetivo de calcular de una forma completa y diferente a la metodología basada en Hirst, con el fin de conocer el poder que un medio tiene en un GSN.

El FPM se calcula en este trabajo, al igual que el ICP, para un medio durante un intervalo de un mes. Su expresión matemática es la siguiente:

_FPM(x) = P<sub>Mx</sub> + I<sub>Mx</sub>_

Donde _P<sub>Mx</sub>_ y _M<sub>Mx</sub>_ corresponden a la presencia (factor cuantitativo) y al impacto (factor cualitativo) del medio _m_ en un mes _x_, respectivamente.

La presencia del medio expresa el porcentaje de noticias de éste respecto al total de noticias de medios publicadas en un GSN a lo largo del mes _x_, y se calcula de la siguiente forma:

<figure>

![equation](../p406eq12.gif)</figure>

Por su parte, el impacto del medio se calcula como la media del factor de poder de cada noticia en particular. Su expresión es la siguiente:

<figure>

![equation](../p406eq13.gif)</figure>

Donde el _FPN(x)_ corresponde al Factor de Poder de la Noticia, que se calcula de la siguiente manera:

<figure>

![equation](../p406eq15.gif)</figure>

Donde:

_V<sub>Mx</sub>_ = Número de votos que ha recibido la noticia.

![equation](../p406eq17.gif)= Número medio de votos realizados a noticias de medios de prensa, durante el mes _x_.

_C<sub>Mx</sub>_ = Número de comentarios que ha recibido la noticia.

![equation](../p406eq19.gif)= Número medio de comentarios realizados a noticias de medios de prensa durante el mes _x_.

Los valores medios de los votos y comentarios, restringidos a medios de prensa para poder comparar en igualdad de condiciones, sirven para relativizar el valor de los votos y comentarios recibidos por una noticia, con respecto a la media.

La razón fundamental para realizar esto es que no es lo mismo recibir 200 comentarios un mes en el que la media de comentarios (a noticias de prensa) ha sido 300, que en un mes en el que la media ha sido 500, donde el impacto de la noticia, pese a tener el mismo número de comentarios, ha sido claramente inferior.

_I<sub>Mx</sub>_ no tiene límite superior, aunque es extraño que alcance valores muy superiores a "2".

Por su parte, _P<sub>Mx</sub>_ tiene un valor máximo de "1", pues está limitado por el número máximo de noticias de prensa publicadas durante un mes. En el caso hipotético de que todas las noticias de prensa publicadas fuesen del mismo medio (algo difícil de suceder), su valor sería igual a "1".

Puesto que este valor será bajo de forma general, se multiplica por "10", para obtener un valor del orden de cada uno de los componentes de _I<sub>Mx</sub>_. Pese a ello, alcanzar un valor igual a "0.5" ya se considerará de importancia.

Así, el FPM alcanzará valores que oscilarán generalmente entre "1.5" y "2.5". Aquellos que bajen de este intervalo serán medios con poco poder, y los que superen el intervalo, medios con un gran poder en el GSN.

### Validación de los parámetros

Análisis de contenido descriptivo longitudinal retrospectivo de las 23.373 noticias remitidas a _Menéame_ desde el 1 de enero de 2007 hasta el 30 de abril de 2008 (muestra total de 16 meses).

El procedimiento se basa en recoger, para cada noticia publicada en _Menéame_, la fecha de su publicación, el número de votos y comentarios recibidos y el medio, para con ello, posteriormente, decidir si esa noticia proviene de un medio nacional de prensa digital o no.

Los datos relativos a las noticias publicadas se recogen manualmente de febrero a mayo de 2008 y son analizados durante el mes de junio. La captura se realiza a través de la página de portada (lugar donde se archivan la totalidad de noticias enviadas y publicadas desde el inicio de estos servicios) de _Menéame_.

Para los objetivos de este trabajo, no se considera noticia de prensa nacional digital a las enviadas desde blogs, medios de prensa extranjeros, ni otros medios de comunicación, como páginas web de radio, televisión o portales temáticos. Sin embargo, sí que se tienen en cuenta las agencias de noticias, como _Reuters_, _Europa Press_ o _BBC Mundo_, entre otras.

De esta misma forma, no son consideradas como noticias de prensa nacional todas las enviadas desde las secciones de blogs de algunos periódicos digitales (periodismo ciudadano), pues el objetivo del trabajo es precisamente estudiar la visibilidad de las noticias formales y tradicionales, redactadas por profesionales en medios de prensa.

Tampoco se consideran como tales el envío de imágenes, viñetas o tiras cómicas, aunque éstas sí provengan de un medio de prensa nacional, por no considerarse estos envíos como noticias. Asimismo, quedan excluidas las noticias aparecidas en semanarios o revistas.

Todos los datos son posteriormente volcados a una hoja de cálculo _Excel_, desde la cual son analizados.

## Resultados

El total de noticias enviadas a _Menéame_ durante el intervalo de muestreo ha sido de 23.373, de las cuales 8.505 (36,4%) se han considerado como noticias provenientes de medios de prensa digitales españoles y, por tanto, constituyen la muestra de noticias sobre la que se aplican los indicadores propuestos.

### Impacto de comentarios ponderado (ICP)

El análisis completo de los cinco medios con más número de noticias en _Menéame_ se muestra en la tabla 4.

<table><caption>

**Tabla 4: Índice de comentarios a noticias (_I<sub>Mx</sub>_) y factores de corrección**</caption>

<tbody>

<tr>

<th rowspan="2">Mes</th>

<th colspan="3">El País</th>

<th colspan="3">20 minutos</th>

<th colspan="3">El mundo</th>

<th colspan="3">La vanguardia</th>

<th colspan="3">El Periódico</th>

</tr>

<tr>

<th>Hcn</th>

<th>f(a)</th>

<th>f(b)</th>

<th>Hcn</th>

<th>f(a)</th>

<th>f(b)</th>

<th>Hcn</th>

<th>f(a)</th>

<th>f(b)</th>

<th>Hcn</th>

<th>f(a)</th>

<th>f(b)</th>

<th>Hcn</th>

<th>f(a)</th>

<th>f(b)</th>

</tr>

<tr>

<td>Enero</td>

<td>31</td>

<td>16,75</td>

<td>0,26</td>

<td>30</td>

<td>18,75</td>

<td>0,41</td>

<td>28</td>

<td>14,09</td>

<td>0,45</td>

<td>14</td>

<td>10,6</td>

<td>0,74</td>

<td>12</td>

<td>-</td>

<td>1</td>

</tr>

<tr>

<td>Febrero</td>

<td>31</td>

<td>17,55</td>

<td>0,3</td>

<td>30</td>

<td>17,33</td>

<td>0,56</td>

<td>23</td>

<td>15,83</td>

<td>0,56</td>

<td>11</td>

<td>6,25</td>

<td>0,73</td>

<td>9</td>

<td>7</td>

<td>0,82</td>

</tr>

<tr>

<td>Marzo</td>

<td>34</td>

<td>18,15</td>

<td>0,34</td>

<td>31</td>

<td>18,27</td>

<td>0,33</td>

<td>24</td>

<td>15,53</td>

<td>0,43</td>

<td>17</td>

<td>13,38</td>

<td>0,68</td>

<td>15</td>

<td>9,6</td>

<td>0,75</td>

</tr>

<tr>

<td>Abril</td>

<td>28</td>

<td>17,13</td>

<td>0,38</td>

<td>25</td>

<td>14,45</td>

<td>0,46</td>

<td>23</td>

<td>12,96</td>

<td>0,48</td>

<td>19</td>

<td>11,64</td>

<td>0,58</td>

<td>19</td>

<td>12,67</td>

<td>0,68</td>

</tr>

<tr>

<td>Mayo</td>

<td>28</td>

<td>17,63</td>

<td>0,44</td>

<td>30</td>

<td>22,26</td>

<td>0,61</td>

<td>27</td>

<td>18,45</td>

<td>0,57</td>

<td>19</td>

<td>11,2</td>

<td>0,66</td>

<td>20</td>

<td>17,29</td>

<td>0,74</td>

</tr>

<tr>

<td>Junio</td>

<td>32</td>

<td>21,51</td>

<td>0,48</td>

<td>33</td>

<td>19,67</td>

<td>0,52</td>

<td>28</td>

<td>16,25</td>

<td>0,58</td>

<td>18</td>

<td>13,6</td>

<td>0,78</td>

<td>20</td>

<td>17</td>

<td>0,8</td>

</tr>

<tr>

<td>Julio</td>

<td>31</td>

<td>20,52</td>

<td>0,48</td>

<td>29</td>

<td>20,6</td>

<td>0,54</td>

<td>26</td>

<td>16,8</td>

<td>0,63</td>

<td>17</td>

<td>13,67</td>

<td>0,65</td>

<td>17</td>

<td>7</td>

<td>0,94</td>

</tr>

<tr>

<td>Agosto</td>

<td>33</td>

<td>22</td>

<td>0,42</td>

<td>22</td>

<td>16,89</td>

<td>0,71</td>

<td>29</td>

<td>17,95</td>

<td>0,59</td>

<td>16</td>

<td>10,75</td>

<td>0,8</td>

<td>16</td>

<td>13</td>

<td>0,8</td>

</tr>

<tr>

<td>Septiembre</td>

<td>33</td>

<td>21,05</td>

<td>0,46</td>

<td>30</td>

<td>19,6</td>

<td>0,6</td>

<td>30</td>

<td>20,08</td>

<td>0,54</td>

<td>28</td>

<td>16,07</td>

<td>0,65</td>

<td>14</td>

<td>13</td>

<td>0,81</td>

</tr>

<tr>

<td>Octubre</td>

<td>36</td>

<td>21,12</td>

<td>0,36</td>

<td>31</td>

<td>21,14</td>

<td>0,58</td>

<td>34</td>

<td>21,16</td>

<td>0,44</td>

<td>27</td>

<td>17,32</td>

<td>0,55</td>

<td>20</td>

<td>17,5</td>

<td>0,91</td>

</tr>

<tr>

<td>Noviembre</td>

<td>37</td>

<td>23,67</td>

<td>0,43</td>

<td>33</td>

<td>22,59</td>

<td>0,47</td>

<td>30</td>

<td>18,17</td>

<td>0,46</td>

<td>27</td>

<td>19,36</td>

<td>0,71</td>

<td>21</td>

<td>15,8</td>

<td>0,81</td>

</tr>

<tr>

<td>Diciembre</td>

<td>34</td>

<td>23,15</td>

<td>0,51</td>

<td>25</td>

<td>17,5</td>

<td>0,76</td>

<td>35</td>

<td>23,16</td>

<td>0,44</td>

<td>17</td>

<td>10,8</td>

<td>0,77</td>

<td>17</td>

<td>15,67</td>

<td>0,7</td>

</tr>

<tr>

<td>Enero</td>

<td>34</td>

<td>22,93</td>

<td>0,45</td>

<td>36</td>

<td>25,9</td>

<td>0,63</td>

<td>29</td>

<td>20,31</td>

<td>0,5</td>

<td>27</td>

<td>20,2</td>

<td>0,73</td>

<td>24</td>

<td>19,2</td>

<td>0,83</td>

</tr>

<tr>

<td>Febrero</td>

<td>38</td>

<td>26,88</td>

<td>0,48</td>

<td>31</td>

<td>25</td>

<td>0,62</td>

<td>37</td>

<td>26,56</td>

<td>0,54</td>

<td>26</td>

<td>19,35</td>

<td>0,6</td>

<td>13</td>

<td>-</td>

<td>1</td>

</tr>

<tr>

<td>Marzo</td>

<td>37</td>

<td>24,27</td>

<td>0,45</td>

<td>35</td>

<td>23,28</td>

<td>0,58</td>

<td>36</td>

<td>24,78</td>

<td>0,57</td>

<td>26</td>

<td>16,58</td>

<td>0,68</td>

<td>15</td>

<td>14</td>

<td>0,88</td>

</tr>

<tr>

<td>Abril</td>

<td>41</td>

<td>28,26</td>

<td>0,43</td>

<td>37</td>

<td>26,81</td>

<td>0,54</td>

<td>36</td>

<td>21,9</td>

<td>0,55</td>

<td>31</td>

<td>21,78</td>

<td>0,78</td>

<td>20</td>

<td>14,33</td>

<td>0,87</td>

</tr>

<tr>

<th>MEDIA</th>

<th>34</th>

<th>21,41</th>

<th>0,42</th>

<th>31</th>

<th>20,63</th>

<th>0,56</th>

<th>30</th>

<th>19,00</th>

<th>0,52</th>

<th>21</th>

<th>14,53</th>

<th>0,69</th>

<th>17</th>

<th>13,79</th>

<th>0,83</th>

</tr>

</tbody>

</table>

Se observa cómo el medio que ha alcanzado un valor máximo para _HCN<sub>x</sub>_ ha sido _El País_, con un valor de 41 (abril 2008). El f(a) asociado a este valor (28,26) es, a su vez, el mayor alcanzado.

Por otra parte, el valor mínimo para _HCN<sub>x</sub>_ lo ostenta _El Periódico de Catalunya_, con un valor de 9 (febrero 2007), mientras que el f(a) mínimo calculado corresponde a _La vanguardia_, con 6,25 (febrero 2007).

El f(b) máximo posible (1) lo alcanza únicamente _El Periódico de Catalunya_, en dos ocasiones (enero 2007 y febrero 2008). El menor f(b) calculado corresponde a _El País_, con un valor de 0.26 (enero 2007).

La evolución mensual del _HCN<sub>x</sub>_, para cada medio, es mostrada en la figura 2.

<figure>

![Figura 2: Evolución del índice de comentarios a noticias](../p406fig2.jpg)

<figcaption>

**Figura 2: Evolución del índice de comentarios a noticias (_HCN<sub>x</sub>_)**</figcaption>

</figure>

Se observa un primer grupo formado por _El País_, _20 Minutos_ y _El Mundo_, que han aumentado sus resultados desde enero de 2007 hasta abril de 2008\. En concreto, _El País_ ha pasado de 31 a 41; _20 Minutos_ de 30 a 37 y _El Mundo_ de 28 a 36.

Relativamente por debajo, pero con un crecimiento muy reseñable, se sitúa _La Vanguardia_. Por último, y ya muy retrasado, aparece _El Periódico de Catalunya_.

Se constata una diferencia importante entre estos resultados y los obtenidos por Orduña-Malea y Ontalba-Ruipérez ([2008](#ord08)) para la evolución del promedio de comentarios mensuales por noticia, donde _El País_ alcanzaba unas cifras mucho más discretas, debidas sobre todo a la gran cantidad de noticias que publica mensualmente.

Otro aspecto interesante de estudio es el análisis de los dos factores de corrección: f(a) y f(b).

<figure>

![Figura 3: Comparativa Hcn y f(a) en El País y 20 Minutos](../p406fig3.jpg)

<figcaption>

**Figura 3: Comparativa _HCN<sub>x</sub>_ y f(a) en _El País_ y _20 Minutos_**</figcaption>

</figure>

El corrector f(a) es un indicativo de la cantidad de comentarios que reciben las noticias de un medio, en un mes, que no alcanzan _HCN<sub>x</sub>_ comentarios. Es una manera de cuantificar la altura de la "larga cola" de comentarios.

La figura 3 detalla la alta correlación existente entre _HCN<sub>x</sub>_ y su factor de corrección f(a), lo cual indica que, a mayor índice de comentarios, mayor es la cantidad de comentarios que reciben las noticias por debajo del umbral _HCN<sub>x</sub>_.

<figure>

![Figura 4: Distribución de los comentarios en marzo para El País](../p406fig4.jpg)

<figcaption>

**Figura 4: Distribución de los comentarios en marzo para _El País_**</figcaption>

</figure>

<div>

<div>  

La figura 4 muestra la distribución de los comentarios para todas las noticias publicadas por _El País_ en el mes de marzo de 2007 (mes con mayor _HCN<sub>x</sub>_), mostrando en rojo el punto umbral. La correlación indica que cuanto más a la izquierda se sitúe el umbral -mayor _HCN<sub>x</sub>_ - la media del número de noticias por debajo de éste será también mayor -mayor f(a)-. Por ello, a igualdad de _HCN<sub>x</sub>_, un mayor f(a) nos mostrará una _larga cola_ de mayor importancia y, por tanto, de mayor valor.

Por su parte, el corrector f(b) es un indicador no de la altura de la larga cola, sino de su anchura, pues tiene en cuenta la cantidad de noticias que no superan el umbral respecto al total de noticias publicadas.

<figure>

![Figura 5: Evolución del factor f(b) en los medios (Menéame)](../p406fig5.jpg)

<figcaption>

**Figura 5: Evolución del factor f(b) en los medios (_Menéame_)**</figcaption>

</figure>

Como se observa en la figura 5, este corrector presenta niveles inversos al f(a) y, por tanto, también al _HCN<sub>x</sub>_.

Se muestra un _ranking_ prácticamente inverso al obtenido por los procedimientos anteriores. _El Periódico_ muestra los niveles más elevados, llegando incluso en dos ocasiones (enero 2007 y febrero 2008) al valor máximo (igual a "1"). Esto significa que todas las noticias superan el umbral, no existiendo "larga cola".

El corrector f(b) muestra de una forma sencilla cómo los medios que publican una cantidad de noticias menor, tienen, en comparación, un gran impacto en comentarios.

Pese a ello, se debe tener en cuenta que para que un medio como _El País_, con la cantidad de noticias que publica, alcanzase unos valores semejantes en f(b), esto supondría una casi triplicación de su _HCN<sub>x</sub>_. No obstante, este corrector puede marcar de algún modo la cantidad de noticias "menores" por mes en porcentaje.

Finalmente, se muestra el ICP promediado para todos los meses, de forma que se obtenga un valor más real que el obtenido para un mes determinado, eliminando de esta forma el crecimiento debido a noticias puntuales con gran número de comentarios.

<table><caption>

**Tabla 5: Impacto de comentarios ponderado (ICP) promedio**</caption>

<tbody>

<tr>

<th>Medio</th>

<th>ICP(a)</th>

<th>ICP(b)</th>

</tr>

<tr>

<td>El País</td>

<td>34(21,41)</td>

<td>34(0,42)</td>

</tr>

<tr>

<td>20 Minutos</td>

<td>31(20,63)</td>

<td>31(0,56)</td>

</tr>

<tr>

<td>El Mundo</td>

<td>30(19)</td>

<td>30(0,52)</td>

</tr>

<tr>

<td>La vanguardia</td>

<td>21(14,53)</td>

<td>21(0,69)</td>

</tr>

<tr>

<td>El Periódico de Catalunya</td>

<td>17(13,79)</td>

<td>17(0,83)</td>

</tr>

</tbody>

</table>

Se observa como la media del _HCN<sub>x</sub>_ concuerda con el _ranking_ de comentarios totales recibidos, donde _El País_ es primero (47.059 comentarios totales), seguido por _20 Minutos_ (35.868), _El Mundo_ (35.338), _La vanguardia_ (17.988) y _El Periódico de Catalunya_ (12.836).

### Factor de poder del medio (FPM)

El FPM pretende ser un indicador global de la calidad e impacto de un medio en el GSN, de forma que se tengan en cuenta tanto aspectos cuantitativos (número de noticias de ese medio publicadas) como cualitativos (votos y comentarios recibidos) y, además, relativizarlos respecto a los valores obtenidos en cada mes.

A continuación se detallan en la tabla 6 los resultados obtenidos para los cinco medios de prensa más destacados en _Menéame_.

<table><caption>

**Tabla 6: Factor de poder del medio (FPM) en _Menéame_**</caption>

<tbody>

<tr>

<th rowspan="2">Mes</th>

<th>Imx</th>

<th>FPM</th>

<th>Imx</th>

<th>FPM</th>

<th>Imx</th>

<th>FPM</th>

<th>Imx</th>

<th>FPM</th>

<th>Imx</th>

<th>FPM</th>

</tr>

<tr>

<th>El País</th>

<th>El País</th>

<th>El Mundo</th>

<th>El Mundo</th>

<th>20 minutos</th>

<th>20 minutos</th>

<th>La vanguardia</th>

<th>La vanguardia</th>

<th>El Periódico</th>

<th>El Periódico</th>

</tr>

<tr>

<td>Enero</td>

<td>1,87</td>

<td>2,64</td>

<td>2,11</td>

<td>2,51</td>

<td>2,12</td>

<td>2,60</td>

<td>1,78</td>

<td>1,90</td>

<td>2,47</td>

<td>2,55</td>

</tr>

<tr>

<td>Febrero</td>

<td>1,99</td>

<td>2,73</td>

<td>2,07</td>

<td>2,36</td>

<td>2,19</td>

<td>2,58</td>

<td>1,54</td>

<td>1,65</td>

<td>1,83</td>

<td>1,91</td>

</tr>

<tr>

<td>Marzo</td>

<td>2,15</td>

<td>2,79</td>

<td>2,10</td>

<td>2,46</td>

<td>2,01</td>

<td>2,61</td>

<td>1,67</td>

<td>1,83</td>

<td>1,88</td>

<td>2,01</td>

</tr>

<tr>

<td>Abril</td>

<td>1,75</td>

<td>2,26</td>

<td>1,81</td>

<td>2,14</td>

<td>1,97</td>

<td>2,35</td>

<td>2,06</td>

<td>2,29</td>

<td>2,25</td>

<td>2,44</td>

</tr>

<tr>

<td>Mayo</td>

<td>1,75</td>

<td>2,16</td>

<td>2,03</td>

<td>2,33</td>

<td>2,53</td>

<td>2,85</td>

<td>1,60</td>

<td>1,79</td>

<td>2,03</td>

<td>2,20</td>

</tr>

<tr>

<td>Junio</td>

<td>2,00</td>

<td>2,50</td>

<td>1,87</td>

<td>2,23</td>

<td>1,97</td>

<td>2,44</td>

<td>1,77</td>

<td>1,94</td>

<td>1,96</td>

<td>2,15</td>

</tr>

<tr>

<td>Julio</td>

<td>1,90</td>

<td>2,41</td>

<td>1,96</td>

<td>2,29</td>

<td>2,27</td>

<td>2,71</td>

<td>1,58</td>

<td>1,79</td>

<td>2,38</td>

<td>2,52</td>

</tr>

<tr>

<td>Agosto</td>

<td>1,83</td>

<td>2,47</td>

<td>2,22</td>

<td>2,62</td>

<td>1,99</td>

<td>2,25</td>

<td>1,62</td>

<td>1,79</td>

<td>1,97</td>

<td>2,13</td>

</tr>

<tr>

<td>Septiembre</td>

<td>1,92</td>

<td>2,44</td>

<td>1,98</td>

<td>2,39</td>

<td>2,15</td>

<td>2,52</td>

<td>1,87</td>

<td>2,18</td>

<td>1,60</td>

<td>1,72</td>

</tr>

<tr>

<td>Octubre</td>

<td>1,95</td>

<td>2,59</td>

<td>1,89</td>

<td>2,39</td>

<td>2,34</td>

<td>2,68</td>

<td>1,84</td>

<td>2,15</td>

<td>2,56</td>

<td>2,70</td>

</tr>

<tr>

<td>Noviembre</td>

<td>2,09</td>

<td>2,65</td>

<td>1,96</td>

<td>2,38</td>

<td>2,09</td>

<td>2,55</td>

<td>2,08</td>

<td>2,33</td>

<td>1,95</td>

<td>2,12</td>

</tr>

<tr>

<td>Diciembre</td>

<td>1,96</td>

<td>2,46</td>

<td>1,92</td>

<td>2,51</td>

<td>2,32</td>

<td>2,56</td>

<td>1,94</td>

<td>2,10</td>

<td>1,94</td>

<td>2,09</td>

</tr>

<tr>

<td>Enero</td>

<td>1,92</td>

<td>2,39</td>

<td>1,83</td>

<td>2,19</td>

<td>2,01</td>

<td>2,37</td>

<td>1,93</td>

<td>2,17</td>

<td>2,34</td>

<td>2,46</td>

</tr>

<tr>

<td>Febrero</td>

<td>2,07</td>

<td>2,61</td>

<td>2,00</td>

<td>2,48</td>

<td>1,94</td>

<td>2,29</td>

<td>1,79</td>

<td>2,09</td>

<td>1,89</td>

<td>1,98</td>

</tr>

<tr>

<td>Marzo</td>

<td>1,83</td>

<td>2,36</td>

<td>2,36</td>

<td>2,76</td>

<td>1,87</td>

<td>2,26</td>

<td>1,97</td>

<td>2,22</td>

<td>1,82</td>

<td>1,93</td>

</tr>

<tr>

<td>Abril</td>

<td>2,06</td>

<td>2,62</td>

<td>1,86</td>

<td>2,25</td>

<td>2,09</td>

<td>2,49</td>

<td>2,16</td>

<td>2,40</td>

<td>1,96</td>

<td>2,10</td>

</tr>

<tr>

<th>TOTAL</th>

<th>31,03</th>

<th>40,08</th>

<th>31,96</th>

<th>38,29</th>

<th>33,88</th>

<th>40,10</th>

<th>29,20</th>

<th>32,60</th>

<th>32,83</th>

<th>35,01</th>

</tr>

</tbody>

</table>

La diferencia de valor entre el FPM calculado y el _I<sub>Mx</sub>_ es el valor debido al _P<sub>Mx</sub>_, cuyo valor máximo, como se indica en el apartado dedicado a la metodología, es "10", pero que raramente superará la unidad.

En la figura 6 se detalla la evolución del _I<sub>Mx</sub>_ (impacto del medio) a lo largo de los meses.

<figure>

![Figura 6: Evolución del impacto del medio (Menéame)](../p406fig6.jpg)

<figcaption>

**Figura 6: Evolución del impacto del medio (_I<sub>Mx</sub>_) (_Menéame_)**</figcaption>

</figure>

Se observa cómo el impacto oscila en torno a un valor de 2\. Únicamente en dos ocasiones (_20 Minutos_ en mayo de 2007 y _El Periódico_ en octubre de 2007) logran alcanzar o superar los 2.5 puntos.

La evolución tiende a ser bastante irregular, sucediéndose subidas y bajadas prácticamente cada mes. Por ejemplo, _El Mundo_ se sitúa en primera posición en marzo de 2008 y, por el contrario, último al mes siguiente.

Por estas razones, se supone que un valor acumulado de este valor puede proporcionar lecturas más interesantes, pues el impacto mensual está influenciado por el impacto de noticias determinadas y puntuales y, por ello, no responden a ninguna tendencia clara.

<table><caption>

**Tabla 7: _I<sub>Mx</sub>_ y FPM acumulado en _Menéame_**</caption>

<tbody>

<tr>

<th>Medio</th>

<th>Imx</th>

<th>Medio</th>

<th>FPM</th>

</tr>

<tr>

<td>20 Minutos</td>

<td>33,88</td>

<td>20 Minutos</td>

<td>40,01</td>

</tr>

<tr>

<td>El Periódico</td>

<td>32,83</td>

<td>El País</td>

<td>40,08</td>

</tr>

<tr>

<td>El Mundo</td>

<td>31,96</td>

<td>El Mundo</td>

<td>38,29</td>

</tr>

<tr>

<td>El País</td>

<td>31,03</td>

<td>El Periódico</td>

<td>35,01</td>

</tr>

<tr>

<td>La vanguardia</td>

<td>29,2</td>

<td>La vanguardia</td>

<td>32,6</td>

</tr>

</tbody>

</table>

Este aspecto se observa en la tabla 7, donde los resultados obtenidos son diferentes a los aportados por el resto de mediciones. En impacto acumulado a lo largo de los 16 meses, _El País_ aparece en una discreta cuarta posición, siendo superado por _20 Minutos_ (primera en impacto), por _El Periódico de Catalunya_ y por _El Mundo_.

Respecto al FPM acumulado, _El País_ escala posiciones, apareciendo en segundo lugar. Se comprueba cómo su alta presencia en número de votos propicia esta subida.

Finalmente, _20 Minutos_ queda como el medio con más poder en _Menéame_, en función de los cálculos debidos a los indicadores propuestos.

## Conclusiones

1\. Los parámetros propuestos en el trabajo (ICP y FPD) son válidos para calcular el grado de impacto de la prensa digital en los GSN, aportando más información que los meros indicadores cuantitativos basados en el conteo de número de noticias, votos y comentarios recibidos.

2\. El análisis de la evolución mensual del ICP muestra unos resultados diferentes al de la evolución de los comentarios por noticia, ofreciendo datos más ajustados a tendencias que a oscilaciones provocadas por noticias puntuales con gran impacto. No obstante, el _ranking_ del ICP promedio (en los meses estudiados) concuerda con el _ranking_ de comentarios totales recibidos.

3\. La altura de la larga cola de los comentarios a un medio durante un mes, f(a), es, de forma general, proporcional al valor del Hcn (punto umbral). Este hecho se cumple para los cinco medios con más presencia en _Menéame_.

4\. La evolución de la anchura de la larga cola de los comentarios a un medio durante un mes, f(b), es, de forma general, inversamente proporcional a la evolución del valor del Hcn (punto umbral). Este aspecto, sin embargo, no se cumple para _El Mundo_, quien, a pesar de tener menor Hcn que _20 Minutos_, también tiene menor f(b), lo que indica que posee una "larga cola" más estrecha y más corta que _20 Minutos_, por lo que se deduce que su impacto real (en comentarios recibidos) está siendo menor.

5\. El FPM presenta valores que oscilan en torno a 2 y muestra claramente el impacto mensual de las noticias de un medio. No obstante, para un análisis más completo, es preferible su valor acumulado durante varios meses, para poder realizar _rankings_ de impacto de medios.

6\. El Imx muestra cómo _El País_, pese a ser el medio con más número de noticias publicadas en _Menéame_, es solamente el 4º en impacto. _20 Minutos_ se sitúa el primero (33,88), seguido por _El Periódico de Catalunya_ (32,83) y _El Mundo_ (31,96).

7\. El medio de prensa con mayor factor de poder (FPM) es _20 Minutos_, seguido de cerca por _El País_, que alcanza un valor alto gracias sobre todo a la gran presencia, medida en número de noticias publicadas, que tiene en _Menéame_. Le siguen en el _ranking El Mundo, El Periódico de Catalunya_ y, más rezagado, _La vanguardia_.

## <a id="authors"></a>About the authors

Enrique Orduña-Malea es ingeniero técnico de telecomunicaciones (EPSG), licenciado en documentación (UPV) y master en contenidos y aspectos legales en la sociedad de la información. Es ayudante de investigación en la UPV y redactor jefe del Anuario ThinkEPI. [enorma@upv.es](mailto:enorma@upv.es)

José-Antonio Ontalba-Ruipérez es licenciado en geografía e historia, licenciado y doctor en documentación. Actualmente es profesor de los estudios de documentación en la Universidad Politécnica de Valencia [joonrui@upv.es](mailto:joonrui@upv.es)

## References

*   <a id="adl99"></a>Adler, S. (1999).[The slashdot effect: an analysis of three internet publications](http://www.webcitation.org/5ebf251Uo). _Linux Gazette_, (38). Retrieved 10 September, 2008 from http://linuxgazette.net/issue38/adler1.html (Archived by WebCite® at http://www.webcitation.org/5ebf251Uo)
*   <a id="alb01"></a>Albertos, José Luis M. (2001). [El mensaje periodístico en la prensa digital](http://www.webcitation.org/5ebf251VF). _Estudios sobre el mensaje periodístico_, No. 7\. Retrieved 10 September, 2008 from http://www.ucm.es/info/perioI/Period_I/EMP/Numer_07/7-3-Pone/7-3-01.htm (Archived by WebCite® at http://www.webcitation.org/5ebf251VF)
*   <a id="bao00"></a>Baoill, A. Ó. (2000). Slashdot and the public sphere. _First Monday_, **5** (9).
*   <a id="cab08"></a>Cabezas, A. (2007, Diciembre 10). [El índice Hc. Aplicación a los 12 blogs más destacados de Biblioteconomía y Documentación](http://www.webcitation.org/5ebf251Vu). _Documentación, biblioteconomía e información_. Retrieved 10 September, 2008 from http://www.lacoctelera.com/documentacion/post/2007/12/10/el-aandice-hc-aplicaciain-los-12-blogs-maas-destacados-de (Archived by WebCite® at http://www.webcitation.org/5ebf251Vu)
*   <a id="coh07b"></a>Cohn, D. (2007, Jan/Feb). Digg This. _Columbia Journalism Review_, **45**(5), 8-9.
*   <a id="dan08a"></a>Dans, E. (2008a). [Menéame, social media y publicidad](http://www.webcitation.org/5ebf251WD). _El blog de Enrique Dans_. Retrieved 10 September, 2008 from http://www.enriquedans.com/2008/04/meneame-social-media-y-la-publicidad.html (Archived by WebCite® at http://www.webcitation.org/5ebf251WD)
*   <a id="ros07"></a>De la Rosa Troyano, F. F. (2007, July 18). _[Trabajo Menéame.](http://www.webcitation.org/5ipdRmTKG)_ Retrieved 10 September, 2008 from http://www.lsi.us.es/~ffrosat/index.php/Ffrosat/TrabajoMeneame (Archived by WebCite® at http://www.webcitation.org/5ipdRmTKG
*   <a id="egg06"></a>Egghe, L. (2006). Theory and practice of the g-index. _Scientometrics_, **69**(1), 131-152.
*   <a id="esc07"></a>Esciudad (2007, April). [Los reyes de Meneame](http://www.webcitation.org/5ebf251Wg). esCiudad.com. Retrieved 10 September, 2008 from http://www.esciudad.com/meneame.html (Archived by WebCite® at http://www.webcitation.org/5ebf251Wg)
*   <a id="fer08"></a>Ferreras Rodríguez, E. M. (2008). Nuevas herramientas en el ecosistema digital: la promoción social de noticias en castellano. Análisis de Menéame. [En imprenta]
*   <a id="fra08"></a>Franganillo, J. (2008). Necesidad de buenas prácticas en la redifusión de contenido digital. _Anuario ThinkEPI 2008_, 17-19.
*   <a id="gua07"></a>Guallar, J. (2007). La renovación de los diarios digitales: rediseños y web 2.0\. _El profesional de la información_, **16**(3), 235-242.
*   <a id="han05"></a>Hane, P.J. & Pitkow, J. (2005, March). [Moreover, the news aggregator: interview with Jim Pitkow](http://www.webcitation.org/5ipeU8cKC). _Information Today_, **22**(3). Retrieved 6 August, 2009 from http://www.infotoday.com/it/mar05/hane.shtml (Archived by WebCite® at http://www.webcitation.org/5ipeU8cKC)
*   <a id="hir05"></a>Hirsch, J. E. (2005). [An index to quantify an individual's scientific research output](http://arxiv.org/PS_cache/physics/pdf/0508/0508025v5.pdf). _Proceedings of the National Academy of Sciences_, **102**(46), 16569-16572\. Retrieved 10 September, 2008 from http://arxiv.org/PS_cache/physics/pdf/0508/0508025v5.pdf
*   <a id="jim02"></a>Jiménez López, M. À., González Quesada, A. & Fuentes, M. E. (2002). Diaris digitals a Internet: panorama actual i expectativas professionals per als bibliotecaris i documentalistes. _Item_, No. 32, 27-44.
*   <a id="kal07"></a>Kaltenbrunner, A., Gómez Vicenç, M. A., Meza Rodrigo, B. J. & López, V. (2007). [Homogeneus Temporal Activity Patterns in a large online communication space](http://www.webcitation.org/5ebf251X7). _Proceedings of the BIS 2007 Workshop on Social Aspects of the Web Poznan, Poland, April 27_. Retrieved 10 September, 2008 from http://ftp.informatik.rwth-aachen.de/Publications/CEUR-WS/Vol-245/paper1.pdf (Archived by WebCite® at http://www.webcitation.org/5ebf251X7)
*   <a id="ler08"></a>Lerman, K. (2008). [Social networks and social information filtering on digg](http://arxiv.org/PS_cache/cs/pdf/0612/0612046v1.pdf). Retrieved 10 September, 2008 from http://arxiv.org/PS_cache/cs/pdf/0612/0612046v1.pdf
*   <a id="lop07"></a>López Sánchez, J. I. (2007). [Evolución de los modelos de negocio en internet: situación actual en España de la economía digital](http://www.mityc.es/NR/rdonlyres/DA658866-A29D-4C4B-87D1-D9978CACEE0E/0/213.pdf). _Economía industrial_, No. 364, 213-229\. Retrieved 10 September, 2008 from http://www.mityc.es/NR/rdonlyres/DA658866-A29D-4C4B-87D1-D9978CACEE0E/0/213.pdf
*   <a id="mar06"></a>Martínez, P., Felip, L. & Orduña-Malea, E. (2006, November 06). H index question. Message posted to Scientometrics, Informetrics and Cybermetrics, archived at http://listserv.rediris.es/archives/issi.html
*   <a id="men07"></a>Menéame: blog oficial (2007, April 10). [Estadísticas históricas de noticias](http://www.webcitation.org/5ebf251YJ). Retrieved 10 September, 2008 from http://blog.meneame.net/2007/04/10/estadisticas-historicas-de-noticias (Archived by WebCite® at http://www.webcitation.org/5ebf251YJ)
*   <a id="not07"></a>Notess, G. R. (2007). Community filtering: digg, slashdot, and the social web. _Online_, **31**(1), 45-47.
*   <a id="ord08"></a>Orduña-Malea, E. & Ontalba-Ruipérez, J-A. (2008). Presencia de la prensa digital española en la Web social. _El profesional de la información_, **17**(5), 511-518.
*   <a id="poo05"></a>Poor, N. (2005). [Mechanisms of an online public sphere: The website Slashdot](http://www.webcitation.org/5ebf251Z6). _Journal of Computer-Mediated Communication_, **10**(2). Retrieved 10 September, 2008 from http://jcmc.indiana.edu/vol10/issue2/poor.html (Archived by WebCite® at http://www.webcitation.org/5ebf251Z6)
*   <a id="rod07"></a>Rodríguez Navarro, A. & Imperial Ródenas, J. (2007). [índice h. Guía para la evaluación de la investigación española en ciencia y tecnología utilizando el índice h](http://www.webcitation.org/5ebf251ZF). [Madrid]: Consejería de Educación. Dirección General de Universidades e Investigación, 2007\. Retrieved 10 September, 2008 from http://www.madrimasd.org/informacionidi/biblioteca/Publicacion/coleccion-madrimasd (Archived by WebCite® at http://www.webcitation.org/5ebf251ZF)
*   <a id="rou06"></a>Rousseau, R. (2006). [New developments related to the Hirsch index](http://www.webcitation.org/5ebf251ZO). _Science focus_, **1**(4), 23-24\. Retrieved 10 September, 2008 from http://eprints.rclis.org/archive/00006376/01/Hirsch_new_developments.pdf (Archived by WebCite® at http://www.webcitation.org/5ebf251ZO)
*   <a id="san90"></a>Sancho, R. (1990). Indicadores bibliométricos utilizados en la evaluación de la Ciencia y la Tecnología. Revisión bibliográfica. _Revista Española de Documentación Científica_, **13**(3/4), 842-865.
*   <a id="wu07"></a>Wu, F. & Huberman, B. A. (2007). [Novelty and collective attention](http://arxiv.org/PS_cache/arxiv/pdf/0704/0704.1158v1.pdf). _Proceedings of the National Academy of Sciences of the United States of America_, **104**(45), 17599-17601\. Retrieved 10 September, 2008 from http://arxiv.org/PS_cache/arxiv/pdf/0704/0704.1158v1.pdf
*   <a id="yah08"></a>Yahoo! Buzz. Retrieved 10 September, 2008 from http://buzz.yahoo.com/

#### <a id="ingles" name="ingles"></a>Abstract in English

> **Introduction**. Social bookmarking sites such as _Digg_, _Menéame_, etc. have become a reference for the digital press. Therefore, news producers wish to position themselves in such systems to gain visibility and to increase traffic to their Websites. This paper proposes two bibliometric indicators to calculate the presence and impact of the press in social bookmarking systems, providing both qualitative and quantitative measures.  
> **Method**. The Weighted Comments Impact (ICP) is proposed as an indicator of the impact of a news medium over a month, and the Media Power Factor (FPM) is proposed as an indicator of the overall impact of a news medium over a month. To validate these parameters, a longitudinal retrospective descriptive analysis of the content of news sent to the Menéame social bookmarking site over sixteen months has been carried out.  
> **Results**. The validation of the Weighted Comments Impact shows that the digital medium which has reached the highest average value is _El País_, followed by _20 Minutos_ and _El Mundo_, which is consistent with the overall ranking of comments received. The validation of the Media Power Factor, however, indicates that _20 Minutos_ is the digital newspaper with the highest value, followed by _El País_ and _El Mundo_.  
> **Conclusions**.The main finding of this study is that the parameters proposed are valid for calculating the degree of impact of the digital press in a social bookmarking site.

</div>

</div>

</div>