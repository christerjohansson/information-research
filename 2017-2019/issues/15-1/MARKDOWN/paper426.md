#### vol. 15 no. 1, March, 2010

# Open access and civic scientific information literacy


#### [Alesia Zuccala](#author)  
Rathenau Institute, Science System Assessment Unit, Anna van Saksenlaan 51, 2593 HW, The Hague, The Netherlands.

#### Abstract

> **Introduction.** We examine how residents and citizens of The Netherlands perceive open access to acquire preliminary insight into the role it might play in cultivating civic scientific literacy. Open access refers to scientific or scholarly research literature available on the Web to scholars and the general public in free online journals and institutional repositories.  
> **Method.** Four focus group sessions were held at a conference centre near Amsterdam. Participants were between the ages of twenty-one to sixty and grouped on the basis of age and educational background. All were invited to complete a brief digital literacy and information literacy questionnaire, and contribute to a set of ranking and vignette exercises designed to encourage discussion.  
> **Results.** Participants generally agreed that open access literature could be useful for personal decision making and conveyed an interest in medical treatment research and other research 'that has to do with people.' Some concern was expressed about the cognitive accessibility of scientific research, but participants were confident that they had the online search skills to find this literature. Science journalists were appreciated for their role as interpreters; however, universities and scholars were considered more credible as information sources, though some participants wondered if scientists and scholars were making their work visible enough to the lay public.  
> **Conclusions.** Current science policy in The Netherlands is focused on motivating young people to raise their interest in science and technology and engage in science-related careers. We recommend the introduction in schools of strategic e-learning programmes designed to help young citizens develop a greater appreciation for scholarly and scientific research and improve their capacity to make decisions as online information consumers.



## Introduction

The Netherlands is one of many European member states to act in support of the open access movement and sign the Berlin Declaration of 2003\. With this signature, the Dutch academic community has agreed to make all '_scholarly and scientific articles_ [i.e., the results of publicly funded research] _available in open access archives_' ([Surf Foundation 2007](#sur07)). The Network of Digital Academic Repositories (DAREnet) in The Netherlands is a Web-based search service which gives free access 'to 514,347 scientific publications (204,499 of which are open access publications), 13,905 data sets, and information on researchers (expertise), research projects and research institutes in the Netherlands' ([NARCIS 2010](#nar10)). As of April 2008 DAREnet became a subset of NARCIS (the gateway to Dutch scientific information), which requires all institutional members to take responsibility for their own repository.

For Dutch citizens, open access has potential to support and encourage the public's engagement with science by making the reality of scientific and scholarly research more visible in the interactive environment of the Internet. The layperson is given a choice to read or not to read open access literature. It is not clear how important this choice is for most people, including how peer-reviewed research should be mediated or interpreted online to improve the layperson's comprehension.

In this study, four pilot focus groups were carried out to identify what the average layperson in The Netherlands knows about open access in order to provide Dutch policymakers with information that might help them consider new approaches to pre-university science education and civic scientific literacy. Our understanding is that general information literacy for the layperson is a feature of civic scientific literacy (see [Association of College & Research Libraries 2000](#ACR00)) as well as digital literacy (i.e., skills for locating digital information and recognizing that it differs from print). A scientific information literate person is someone who possesses some '_knowledge and understanding of scientific concepts and processes required for participation in a Digital Age society_' and '_can identify scientific issues underlying national and local decisions and express a position that is scientifically and technologically informed_' ([U.S. _National Academy of Science_ 1998](#US): 22).

## Background to the study

Open access is a scholarly communication movement developed by scholars for scholars to increase the impact of future scientific research and create a cost-effective publication system. The goal of open access is to enhance scientific knowledge work by making peer-reviewed research literature openly available on the Web with the creation of institutional preprint repositories or archives (the green route) and free online journals (the gold route).

Open access has been debated and discussed widely in terms of publishing economics, institutional archiving, copyright law, and issues of distributive injustice yet little attention has been given to the broader issue of lay access to specialized knowledge (see [Jacobs 2006](#jac06)). At present, little is known about the impact that free scholarly research literature might have on the knowledge and interests of laypeople. Willinsky states that open access may:

> ...mean little enough, admittedly to most people, most of the time. Still, it is not difficult to imagine occasions when a dedicated history teacher, an especially keen high school student, an amateur astronomer, or an ecologically concerned citizen might welcome the opportunity to browse the current and relevant literature pertaining to their interests ([Willinsky 2006](#wil06): 111).

Proponents of the Budapest Open Access Initiative also state that:

> The public good... is the world-wide electronic distribution of the peer-reviewed journal literature and completely free and unrestricted access to it by all scientists, scholars, teachers, students, and other curious minds. Removing access barriers to this literature will accelerate research, enrich education, share the learning of the rich with the poor and the poor with the rich, make this literature as useful as it can be, and lay the foundation for uniting humanity in a common intellectual conversation and quest for knowledge. ([Budapest Open Access Initiative 2002](#bud02))

If open access is expected to '_unite humanity in a common intellectual conversation_', then perhaps the average layperson will have to demonstrate a motivation to look for and read peer-reviewed scientific literature. Once this literature is found and read, the person will have to work towards understanding it, and if it is not understood, then someone will have to interpret its value.

To understand what motivates laypeople to look for and use open access literature, it is important to consider their everyday information seeking behaviour. Information seeking research is concerned with modelling the cognitive and affective behaviour of individuals with information needs, including how these needs arise in context ([Wilson 1981](#wil81); [1999](#wil99)), how individuals make sense of situations to bridge knowledge gaps ([Dervin 1992](#der92)), manage feelings of uncertainty ([Kuhlthau 1997](#kuh97)) and move through stage-related processes ([Ellis 1989](#ell89); [Ellis _et al._ 1993](#ell93); [Kuhlthau 1991](#kuh91)).

Information seeking occurs in various information-use environments ([Taylor 1991](#tay91)) and when information is received by an individual, it has potential to change his/her knowledge structure ([Cole 1997](#col97)). Individuals who search for information often do so as a coping mechanism, due to psychological stress, or because they are motivated to find answers to serious problem ([Wilson 1999](#wil99)). People also have particular source preferences and an individual's preference usually depends on the information source's familiarity and credibility within their immediate social milieu ([Chatman 1991](#cha91); [Spink and Cole 2001](#spi01); [Wilson 1983](#wil83)).

Credibility is a concept that is often associated with believability: '_credibility strongly influences the impact of a message_' ([Wathan and Burkell 2002](#wat02): 134). Some people presume that an information source is credible, while others think a source is credible by its reputation. Credibility judgments can also be based on the superficial scanning of an information source, or through repeated first-hand experience with the source ([Fogg and Tseng 1999](#fog99); [Self 1996](#sel96); [Tseng and Fogg 1999](#tse99)). When a scholarly or scientific information source is consulted, two factors play a role: _verifiable credibility_ and _cost-effort credibility_ ([Liu 2004](#liu04)). A scholarly document is verifiably credible, if a user can see that it has been evaluated, cited, linked to another credible source on the Web, or published in a printed journal. Cost-effort credibility refers to the document's ease of access and whether or not a piece of Web-based information is free, must be purchased or requires a subscription fee. Liu suggests that '_the ease in accessing free scholarly information may have an impact on credibility perception_.' Laypeople '_may take free information from the Web for granted and/or find it increasingly difficult to determine which document should be believed and used_' ([Liu 2004](#liu04): 1036).

Here, the concept of credibility relates very closely to cognitive authority because both are perceived in terms of quality ([Wilson 1983](#wil83)). A person may judge whether or not a piece of information is of higher or lower quality based on its institutional authority (publisher), textual type authority (document type), intrinsic plausibility authority (content of text), or the cognitive authority of another person delivering the information (i.e., they know what they are talking about) ([Wilson 1983](#wil83); [Rieh 2002](#rie02)). It can also be difficult for an individual to determine the credibility or cognitive authority of a piece of scholarly information if s/he is not able to comprehend the work.

Research concerning the public understanding of science focuses on the degree to which laypeople '_understand the process or nature of scientific inquiry_' ([Miller 2004](#mil04): 273). Miller suggests that the layperson needs to possess a level of scientific literacy that is '_sufficient to read and comprehend the Tuesday section of the_ New York Times' ([Miller 2004](#mil04): 274), but in order to judge a piece of research (e.g., a journal article) the same person would have to be familiar with how a scientific study is properly constructed, including the concepts of experimentation and probability, and the use of special terms; e.g., DNA, radiation, molecule, and stem cells. It is also valuable to the person if s/he has the ability to recognize how science is embedded institutionally, who its patrons are, and how it is socially organized ([Sturgis and Allum 2004](#stu04); [Wynne 1995](#wyn92)).

In academia, two prevailing theories exist regarding the public understanding of science; one is the _deficit model_ and the other is the _contexualist_ perspective. The _deficit model_ assumes that people are '_deficient_' in their knowledge of science and that due to '_a lack of proper understanding of relevant facts, people [will] fall back on mystical beliefs and irrational fears of the unknown_' ([Sturgis and Allum 2004](#stu04): 57). The _contextualist_ perspective asserts that it is not enough for laypersons to have a textbook understanding of science – i.e., to '_recall large numbers of miscellaneous facts_' but the must also have '_a keen appreciation of the places where science and technology articulate smoothly with one's experience of life_' ([Sturgis and Allum 2004](#stu04): 58; see also [Jasanoff 2000](#jas00)). In other words, we want a scientifically literate public to be sure that progress in scientific research makes sense to individuals and that they are aware of the impact that new discoveries can have on daily living. Some scholars also believe that a scientifically literate public _needs to have sufficient levels of accurate information on which to base their assessments of policy alternatives [so] that their policy preferences best reflect their own self or group interests_ ([Sturgis and Allum 2004](#stu04): 56).

Sometimes laypeople are not able to understand, interpret or easily appreciate the value of a scientific research project; thus a mediator or interpreter is needed to explain the work in lay terms. While science production is '_aimed at the advancement of knowledge_', scientific communication is '_aimed at bridging the distance between science and the public_'. The impetus for bridging this gap is the '_political duty in democratic societies to inform citizens_' ([Bensaude-Vincent 2001](#ben01): 99).

Science mediators or '_popularizers_' have previously been criticized for their role: some scholars believe that their '_noble mission_' is simply a mechanism for '_self-legitimization_' ([Hilgartner 1990](#hil90); [Jurdant 1969](#jur69)). Others are convinced that the inherent problem with science communication is not so much the gap itself, but the reiteration that a piece of scientific knowledge (e.g., a peer-reviewed article) goes through before it is deemed suitable for the public. Bensaude-Vincent explains that '_the communication of ideas always results in a change of the content, and each passage from one collective to another one creates a new meaning rather than simply transferring a stable message_' ([Bensaude-Vincent 2001](#ben01): 100).

Although a gap usually does exist, it is important also to consider Latour's ([1987](#lat87)) notion that it is natural: the technical and specialized nature of scientific research is not negative, but essential to the construction of hard facts.

To close the gap between scientists and the public, a number of universities across Europe have adopted the co-production model of science, which targets people belonging to groups such trade unions, pressure groups, non-profit organizations, social groups, environmental groups and consumer groups and reinforce the idea that '_laypeople have knowledge and competencies which enhance and complete those of scientists and specialists_' ([Callon 1999](#cal99): 8). In the Netherlands, this model is represented by the creation of science shops; mediating agents tied to universities, which give or have given graduate students opportunities to carry out research relevant to particular citizen groups ([Leydesdorff and van den Besselaar 1987](#ley87); [Leydesdorff and Ward 2005](#ley05)).

The public education model of science, compared to the co-production model, is more prominent because it exists widely for people regardless of what they want to know. With this model, new scientific discoveries are mediated by television, newspapers, in science magazines and on the Internet. Studies confirm that there are advantages and disadvantages associated with each form of mediation, and people tend to select certain mediums to suit particular needs ([Dijkstra _et al._ 2005](#dij05); [Koolstra _et al._ 2006](#koo06)). The clear benefit of the Internet is that it allows individuals to process information at their own rate and provides opportunities for interactivity ([Koolstra _et al._ 2006](#koo06)).

As we enter into a new open access era, online research literature can be used to create a new kind of public awareness, whereby the traditional networks of popular science versus academic science need not exist in parallel anymore. With the availability of more scientific literature on the Web we are likely to see more network interaction or cross-linkages between the two sides. Scholarly insight indicates that in the past popular science did not necessarily mean '_popularized science_' ([Bensaud-Vincent 2001](#ben01): 105); hence, with open access, there is an opportunity to move towards truly '_popularizing_' science. This means that scientists might choose to take on a more prominent role as mediators and make use of the Internet to help members of the lay public become more aware of their work.

Zuccala ([2009](#zuc09)) suggests that open access supports a new contextualized model of public understanding, which differs considerably from the public-education and co-production models. The public education model is '_the simplest and most widespread model_' and its priority is the education of a scientifically illiterate public. Here '_the ties between scientists and the public are indirect: they are the responsibility of the state_' ([Callon 1999](#cal99): 82-83). The co-production model tries to overcome the limits of the public education model '_by actively involving laypeople in the creation of knowledge concerning them_' ([Callon 1999](#cal99): 89).

Open access is unique because it does not assume an obvious educational role, nor does it attempt to involve laypeople in close collaboration with scientists. At present, it simply provides direct opportunities for the public to encounter peer-reviewed research via the Web. Given this direct opportunity, it is now time to determine what this means for the general public and what type of policies are needed to help laypersons cultivate an appropriate level of civic scientific literacy.

## Focus group method

Research pertaining to the public's attitude toward science and science-related policies is often carried out using large-scale surveys (see [Bauer _et al._ 1994](#bau94)); however, focus groups are beginning to '_fill a gap in the toolbox of participatory policy_' ([Durrenberger _et al._ 1999](#dur99): 342). The same authors state that lay citizens should be integrated into policy assessment processes more often and that 'the focus group is a promising tool to achieve such inclusion' ([Durrenberger _et al._ 1999](#dur99): 341). They explain that,

> the strength of focus group research is to increase qualitative insights into specific topics, attitudes and behaviour, especially in fields about which people are not yet well informed and/or in which only limited social science research insights exist, and/or for which policy formation is in an early stage and could benefit from citizen participation ([Durrenberger _et al._ 1999](#dur99): 343).

Some focus groups are conceived for an instrumental research purpose; that is, to improve policy making by providing an opportunity for citizen acceptance. Other focus groups are designed with a participatory purpose; that is, to include citizens in the process of policy formulation ([Durrenberger _et al._ 1999](#dur99): 344-345). Open access in The Netherlands has required little citizen involvement; hence the focus groups in this study were conceived for a substantive research purpose, that is, to gain insight into Dutch citizens' perceptions, concerns, visions and judgments regarding this new public information policy.

A focus group is a naturalistic event, not a natural event, so a certain measure of planning is required. The researcher's approach begins with giving thought to the group size and nature (i.e., will it be homogeneous or heterogeneous?) and the type of people who would be appropriate for the study topic. The researcher should also be prepared to facilitate rapport amongst group members, listen to what they say and follow the direction of the group's discussion; yet maintain a restrained contribution ([Berg 1998](#ber98)). A focus group is not meant to be a group interview, but an observation of group norms and meanings ([Bloor _et al._ 2001](#blo01)). Prior to conducting a focus group a written plan is essential, and normally a set of response questions, group ranking exercises and vignette exercises are included to stimulate a purposeful discussion (see [Appendix - Focus Group Exercises](#app)).

Twenty-three Dutch citizens were recruited by an agency in Amsterdam to participate in four separate focus groups held on September 20th, October 4th, October 13th, 2007 and March 13th, 2008, in a small meeting room at the ARISTO Conference facility, Sloterdijk. Table 1 outlines the general age and educational criteria used to assign individuals to distinct groups. By grouping individuals according to their age and education we aspired to promote ease of communication. A questionnaire was administered prior to the group sessions to obtain participants' personal details (e.g., name, address, e-mail) and a general assessment of each individual's level of digital and information literacy. People who arrived on time and stayed for the full session received a stipend. All of the discussions were recorded on tape and transcribed for analysis.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Selection criteria for assigning individuals to groups**</caption>

<tbody>

<tr>

<th>Educational level</th>

<th>Ages: 18-35</th>

<th>Ages: 35+</th>

</tr>

<tr>

<td>University and college education (Hoger Beroeps Onderwijs (Higher vocational education); Wetenschappelijk Onderwijs (Science education))</td>

<td>Group 1</td>

<td>Group 2</td>

</tr>

<tr>

<td>Some college education (Middelbaar Beroepsonderwijs (Secondary Vocational Education); Hoger Beroeps Onderwijs (Vocational higher education); Wetenschappelijk Onderwijs (Science education))</td>

<td>Group 4</td>

<td>Group 3</td>

</tr>

</tbody>

</table>

The recruitment process for a focus group is not always ideal and participation is not guaranteed. Sometimes individuals fail to arrive in time (or at all) for a scheduled meeting, and amongst a given set of participants, a moderator can never predict who will be willing to speak honestly and freely with others. Bloor _et al._ note that smaller groups (less than four) '_can potentially result in limited discussion_' and larger groups (more than six) '_can become difficult to moderate and may be frustrating for participants if they have not had adequate time to express their views and opinions_' ([Bloor _et al._2001](#blo01): 27). In this study an over-recruitment strategy was used to ensure an optimal number of six participants. When there was an attendance problem caused by, for example, participants withdrawing their participation unannounced, we continued with the scheduled focus group. A focus group of four individuals was still considered sufficient for a discussion and still allowed the moderator to observe interactions.

Of the twenty-three focus group participants, twelve were female and eleven were male. The participants ranged in age from twenty-one to sixty. Nine were university undergraduate students and fourteen were working professionals: e.g., an artist or painter, cameraman, social worker, hotel administrator, fireman, businessman, financial controller, part-time medical assistant, primary school teacher, policewoman, accountant, and medical receptionist. Although the recruited students and professionals were not necessarily representative of the entire Dutch population, we found that all were familiar with searching the Web (revealed through preliminary questionnaire results) and all were articulate enough to provide valuable perspectives, including both naïve and well-considered points and arguments.

Below we list our primary research questions.

1.  What do laypeople know about open access to scholarly and scientific literature?
2.  What level of interest do laypeople have in reading peer-reviewed publications produced in different scientific and scholarly research areas (e.g., health sciences; psychology; agriculture; food sciences; media studies, etc)?
3.  Are laypeople aware of and do they agree with the notion that there are civic benefits associated with open access?
4.  What types of situations are likely to motivate a layperson to look for open access research literature?
5.  What would be the most significant barrier for laypeople when they look for and use open access research literature?
6.  What types of mediation strategies would be most helpful to laypeople when they look for and use open access literature on the Web?

## Focus group results

### What do laypeople know about open access to scholarly/scientific literature?

At the start of each focus group session, participants were asked to note a few preliminary negative, neutral and/or positive ideas concerning open access and convey them to other members of the group. The ideas that were expressed are shown below in a brief form.

Positive ideas:

> *   F2EM: '_interesting; I would like to find out what scholars and scientists are writing_'
> *   F2AJ: '_interesting; can follow new developments, especially on illnesses, environmental issues, animal welfare_'
> *   F4SV: '_people can get more scientific information about certain topics; e.g. disease information_'
> *   F2TK: '_more cooperation and interaction is possible_' and '_medical doctors can exchange information much faster_'
> *   F4WH: '_saves money for universities so that they can spend more on students_'
> *   F4MP: '_accessible to everyone and not only through membership at a library_'
> *   F4LH: '_stimulates people to be more critical of information_'
> *   F2PH: '_assuming that knowledge is power, the public can begin to get information for self-education_'
> *   F4KV: '_knowledge and scientific progress [information] should be free for everyone_'
> *   F2JH: '_it is democratic to know that the sites are available_'
> *   F2DC: '_cutting-edge knowledge at my fingertips_'

Negative ideas:

> *   F1DM: '_dangerous for some people, depending on the research_'
> *   F3MZ: '_you are not going to know if the research you find is legitimate_'
> *   F4KV: '_harder to discern legitimate high quality research_'
> *   F4MP: '_more difficult to distinguish between good and bad literature_'
> *   F3RD: '_difficult to find out what to use or what is real_'
> *   F4LH: '_plagiarism_'
> *   F2MV: '_people will copy it and use it and make it look like it was their own thoughts_'
> *   F4LH: '_more information means that you have to search harder for good information_'
> *   F1PH: '_science is not neutral but value-driven and open access could lead to misinterpretation_'
> *   F3DJ: '_it depends on what subject: it could be used in the wrong way_'
> *   F1NB: '_the language is too difficult to understand for normal people_'
> *   F2EM: '_the information is for a small group of people_'
> *   F2JH: '_people who publish don't get paid_'
> *   F2TK: '_no income for publishers_'
> *   F2DC: '_too complicated and not that easy to find sometimes_'
> *   F1SP: '_too much information and not all of it is correct and updated_'

### What level of interest do laypeople have in reading peer-reviewed publications produced in different scholarly or scientific research areas?

Group participants were asked to think about scholarly/scientific research and identify, from a list of fourteen discipline cards, which would be of primary reading interest to the lay public [see Appendix]. For discussion purposes they were invited to create a ranking system associated with the disciplines and to speak openly about their own personal interests.

Table 2 shows the rank results. Research produced in health sciences and psychology was ranked quite high in terms of the public's interest, including business and economics, and earth and environmental sciences. Most group participants considered chemistry and mathematics and statistics to be of little public interest.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 2: Ranked results from cognitive exercise C**</caption>

<tbody>

<tr>

<th style="vertical-align: top; text-align: center; font-weight: bold; background-color: rgb(255, 255, 204);">Group 1</th>

<th style="vertical-align: top; text-align: center; font-weight: bold; background-color: rgb(255, 255,  204);">Group 2</th>

<th style="vertical-align: top; text-align: center; font-weight: bold; background-color: rgb(255, 255, 204);">Group 3</th>

<th style="vertical-align: top; text-align: center; font-weight: bold; background-color: rgb(255, 255, 204);">Group 4</th>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(204, 204, 255);">1\. Health sciences and psychology</td>

<td style="vertical-align: top; background-color: rgb(204, 204, 255);">1\. Health sciences and psychology</td>

<td style="vertical-align: top; background-color: rgb(204, 204, 255);">1A. Health sciences and psychology</td>

<td style="vertical-align: top; background-color: rgb(255, 204, 255);">1\. Business and economics</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">2A. Biology and life sciences</td>

<td style="vertical-align: top; background-color: rgb(204, 255, 255);">2\. Earth and environmental sciences</td>

<td style="vertical-align: top; background-color: rgb(255, 204, 255);">1B. Business and economics</td>

<td style="vertical-align: top; background-color: rgb(204, 255, 255);">2A. Earth and environmental sciences</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(204, 255, 255);">2B. Earth and environmental sciences</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3\. Technology and engineering</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">1C. Philosophy and religion</td>

<td style="vertical-align: top; background-color: rgb(153, 255, 153);">2B. Agriculture and food sciences</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3A. Philosophy and religion</td>

<td style="vertical-align: top; background-color: rgb(153, 255, 153);">4\. Agriculture and food sciences  
</td>

<td style="vertical-align: top; background-color: rgb(153, 255, 153);">2A. Agriculture and food sciences</td>

<td style="vertical-align: top; background-color: rgb(204, 204, 255);">3A. Health sciences and psychology</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3B. History and archeology</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">5\. Philosophy and religion</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">2B. Technology and engineering</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3B. Sociology and media studies</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">4\. Technology and engineering</td>

<td style="vertical-align: top; background-color: rgb(255, 204, 255);">6\. Business and economics</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">2C. Physics and astronomy</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">4A. Law and political science</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">5. Law and political science</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">7. Sociology and media studies</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3A. Biology and life sciences</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">4B. Philosophy and Religion</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 204, 255);">6\. Business and Economics</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">8\. History and archeology</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3B. Law and political science</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">5\. Biology and life sciences</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(153, 255, 153);">7\. Agriculture and food sciences</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">9\. Biology and life sciences</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">3C. History and archaeology</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">6\. History and archaeology</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">8\. Sociology and media studies</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">10\. Arts and architecture</td>

<td style="vertical-align: top; background-color: rgb(204, 255, 255);">4A. Earth and environmental sciences</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">7\. Arts and architecture</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">9\. Arts and architecture</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">11\. Law and political science</td>

<td style="vertical-align: top; color: rgb(255, 255, 255); background-color: rgb(102, 102, 102);">4B. Mathematics and statistics</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">8\. Technology and engineering</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">10\. Physics and astronomy</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">12\. Physics and astronomy</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">4C. Arts and architecture</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">9\. Physics and astronomy</td>

</tr>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 255);">11A. Chemistry</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 255);">13\. Chemistry</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 255);">5A. Chemistry</td>

<td style="vertical-align: top; color: rgb(255, 255, 255); background-color: rgb(102, 102, 102);">10\. Mathematics and statistics</td>

</tr>

<tr>

<td style="vertical-align: top; color: rgb(255, 255, 255); background-color: rgb(102, 102, 102);">11B. Mathematics and statistics</td>

<td style="vertical-align: top; color: rgb(255, 255, 255); background-color: rgb(102, 102, 102);">14\. Mathematics and statistics</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">5B. Sociology and media studies</td>

<td style="vertical-align: top; background-color: rgb(255, 255, 255);">11\. Chemistry</td>

</tr>

</tbody>

</table>

During the group discussions, the following views were articulated:

> *   F4CH: '[Suppose] you have a specific source of cancer in your family and you want to look at how that can affect you and what the chances are that you get it. You can look at biology and the life sciences. There is bound to be some articles that can interest you. And people want to have an opinion at parties [bit of laughter] about the Al Gore movie for instance... and a lot of people say this [global warming] is [expletive] and that this is not happening, and a lot of people say no, it's scientifically proven. It's nice to be able to take a stand.'
> *   F3DO: 'People will always be interested in anything that has to do with people, and that can be health, history, ehm, social matters. I think for instance, chemistry, most people are far from it. It stands far from most people.'
> *   F2PI: 'Indeed. I mean, many people don't have nothing to do with mathematics or statistics. And they're like, OK, as long as I can count.. it's enough.'
> *   F3MO: 'It has to do with milieu. If you look at it, you can split the questions into very personal things. Health and psychology - that has to do with your person [your most] private matters. And then you have things like, chemistry. That is not so much a personal thing but it has to do with general society.'
> *   F2NI: 'Agriculture and life sciences, this is something which we need, all of us. Every day from the moment of birth till death... I think that without food and agriculture there will be no business and economics. So we have to put that first.'
> *   F2AS: 'My opinion is that technology studies are important. This one is more important than agriculture, because, ehm... the development of technology makes, certain things happen and possible in agriculture. So, I think that technology is very important nowadays. I mean, everything is digital. We live in a digital world'.

### Are laypeople aware of and do they agree with the notion that there are civic benefits associated with open access?

Group participants were asked to rank the most important open access benefit for laypeople, from a list of given benefits, and discuss their opinions. Table 3 presents the results of the second group ranking exercise.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 3: Open access benefits. Ranking exercise D.**</caption>

<tbody>

<tr>

<th> </th>

<th>Group 1</th>

<th>Group 2</th>

<th>Group 3</th>

<th>Group 4</th>

<td colspan="1" rowspan="6"> </td>

<th>Mode</th>

<th>Mean</th>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Open access will empower laypeople who want to read and use research literature for personal decision making and problem solving.</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Open access will allow people to satisfy their curiosity about what type of research is being done in certain fields and the latest findings.</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">2.5</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Open access literature will help to increase the level of understanding that people have of scientific research terms (e.g., DNA; stem cells; greenhouse effect), research processes, and findings.</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Open access will help people to see what scientific researchers are doing in their own country and acquire sufficient levels of accurate information on which to base their assessments of government policies so that their policy preferences best reflect their own interests.</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4.3</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Open access will allow tax-paying citizens to see where and how money is being invested to support new scientific research.</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">4.3</td>

</tr>

</tbody>

</table>

Participants were positive about the notion that openly accessible scientific/scholarly information has potential to empower them, especially in terms of personal decision making:

> *   F4LI: 'This [card] about personal decision making... I think that is the most important thing that people will look for...'
> *   F4CH: 'Well, I think that for most people, it takes effort to find good literature and to read it all. It takes time and effort. And people will more likely do that if it's a personal problem. If it's something that is important to them, otherwise its just general interest.'

Some participants were convinced that people need to be curious about science and technology first, before reading a piece of scientific literature, and others were convinced that curiosity is a function of having the ability to understand the language of science:

> *   F3TA: 'I think the next one would be curiosity, because that's why you.. how you start to, you know, search for information. If your curiosity subsides you cannot go to this next level of understanding.'
> *   F3AN: 'I don't know. I don't think that everybody is interested in research.'
> *   F3AN: 'How much are people going to learn about scientific terms in a scientific journal? They're not. Because a scientist is not going to explain what he's talking about. He assumes that his public already knows. So I think this should come as last.'
> *   F4SR: 'I think that this [card about laypeople satisfying curiosity] is a good one. People get curious when they see something on television or in the news. '
> *   F4LI: 'Like the Al Gore movie [An Inconvenient Truth], yeah. People were curious.'
> *   F4CH: 'Well, people are interested in the big results [of science] and in the final products and not in the tiny steps that it takes to get to the product. And ninety-nine percent of the articles are just tiny, tiny steps... Well, you have to have the knowledge to place the steps in the whole picture, at the start.'

Participants were more likely to link open access to personal benefits rather than the role it can play in helping them become more aware of government policies and tax spending:

> *   F3TA: 'Who's interested in [points to card: allowing taxpaying citizens to see how money is invested]?'
> *   F3AN: 'I am. I think that this should come second.'
> *   F3TA: 'Yeah, but you know, we're talking like probably one person or one percent of the Dutch population.'
> *   F3DA: 'No, that's not true.'
> *   F3AN: 'No, but it's important to know, I think. If you know what research is being done, you would like, you would want to invest in those kinds of research.'
> *   F3AN: 'But do people really sit down in front of a computer to find out where their tax money is?'
> *   F3DA: 'No no its sort of... Things change, because if you can find it, maybe you are going to use the information and think: Hey, next time I'm going to vote, I'm not going to vote for this party, because I see that my tax money is not being spent right!'
> *   F4CH: [Re: Informing taxpaying citizens] 'A lot of people won't understand because science is taking very very small steps. You have an entire research [project] that takes several years and then you find a tiny piece of the puzzle associated with a problem and people will think, well, that costs so much money and you have almost nothing to show for us. Well, that's the only way that science really advances. And, maybe lay people won't get that.'

### What types of situations are likely to motivate a layperson to look for open access research literature?

The second exercise (Exercise E. Vignettes) was designed to encourage participants to discuss why it is that some people might choose to look for research literature as a personal problem solving aid or why they might choose other sources of information.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">VIGNETTE 1: A 28-year-old woman has a child who is two years old and she has just discovered from a paediatrician that the child may be autistic. The doctor tells her that he will arrange a visit to a specialist, and explains that the cause of autism is still not specifically known.  
</td>

</tr>

</tbody>

</table>

> *   F1NA: 'I think a study could be useful, because I think... I am also a mother, and most mothers when there is something wrong with their child, they think it has to do with them. When you read a study and you see that, ehm, the cause of all this is not yet to be found, it can give you some kind of... yeah, how do you say, a feeling that oh it doesn't have to do with you, because there has been a lot of research about it. And that's why I think a study can help'
> *   F1PI: 'Yeah, I mean, for example, in my family there is a disease and there is no medicine yet, but every time that research is being published, then I am the first one to read it. Just to know, well, is there a medicine. You want to keep your knowledge up to date, maybe even your hope: you wanna keep it up to date.'
> *   F1NA: 'I think [research] literature gives you the facts. If you use other information, it's all... it's often based on people's opinions or people's feelings, things like that. And when you read literature, you know it's based on facts.'

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">VIGNETTE 2: A 42-year-old father notices that his 10-year-old son enjoys playing violent video games, and has said that he would rather do this than go outside and play football. The father is becoming more and more concerned about it and what this means for his son's social and emotional development.  
</td>

</tr>

</tbody>

</table>

> *   F3RE: 'I don't think about scientific information. I think it's too deep for the father, as he has to go to the university or so. It's very difficult. Everyone can have such a child, but yes, to look what the information is, I don't think it's good.'
> *   Moderator: 'So you don't think it's worth looking at research information?'
> *   F3DO: 'I don't think so. Indeed as I may say something as the only one here without children. I think it's very difficult to know that research will help, because every situation with a child is unique. And, it's very hard to do, you'll never do OK for the child. No solution will be a hundred percent the best one. You won't find this, ehm, on the Web, in the research I think. It's all about how to deal with a child.'
> *   F3DA: 'I would look just about anywhere if I was a father. I mean, it's such a pressing problem. So, I mean, if you can find information and it's available and reliable, I would use it.'
> *   F3JA: 'There are magazines for education. There are [more] parents [who] read those magazines.'
> *   F3AN: 'I think he would best first go to see his doctor and ask his doctor for information.'
> *   F3MA: 'Well, the research is probably more reliable.'
> *   Moderator: 'How so? How is it more reliable?'
> *   F3MA: 'Because they did research on it.'
> *   F3DA: 'Well, I disagree, because research for example, showed that all the sex we see on TV.. what we see nowadays has not really changed our attitude towards sex. You know, one would assume that everybody is having his or her way and it's not true. So we have to... I think the research will be interesting to, for example, show a relation between children being aggressive and the video games they're playing. I would want to know the truth, so to say.'

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="vertical-align: top; background-color: rgb(255, 255, 204);">VIGNETTE 3: A 37-year-old high school teacher has been educating her students on the topic of global climate change. In class she must respond to a question from a 17-year-old student, who says: 'My uncle has told me that humans are not really the major cause of global warming and that there is not much that we can do now to change it.'  
</td>

</tr>

</tbody>

</table>

> *   F3MZ: 'To answer the question from the student, you have to come with proof. The student asked you something. He's going to ask why it isn't or why it isn't like that and you've got have some proof. It's like that. It is probably the cause of humans or whatever the answer is. I think that's what the student expects. So [the teacher] has to go and look for research. It is like that.'
> *   F3DO: 'And give examples of how we can change it. That we can change it.'
> *   F3MO: '[The teacher] would also have to show examples that we have already changed something.'
> *   F3DO: 'And therefore for all those things, you can do research indeed on the Internet.'
> *   Moderator: 'Yeah, so why would it be useful to find research information instead of another form of information?'
> *   F3MO: 'I think [that the teacher] should look for both. That sort of information [i.e. research] should work together with other information because there is... because one of the most dangerous things at this moment is the manipulation. It's very easy. Look at what Al Gore did. I mean, now we, now I very much agree with this guy, but he also [did something quite powerful] to manipulate people to think that we can change. We can, if we take some decisions. So, that's aggressive. It's a combination, [of ] manipulation so that people really want to change.. [and then providing] some hope that it is possible. Science is the important thing, because it's practical. Science is very practical.'
> *   Moderator: 'What else is science?'
> *   F3MO: 'It's proof, science really proves things... Yeah, it can make things visual? [visible?]

### What would be the most significant barrier for laypeople when they look for and use open access research literature?

Group participants were asked to rank the most significant barriers associated with Open Access literature (from a list of given barriers) and discuss their opinions. Table 4 presents the results of the third group ranking exercise.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 4: Barriers associated with open access literature. Ranking exercise F.**</caption>

<tbody>

<tr>

<th style="vertical-align: top; width: 40%;"> </th>

<th style="vertical-align: middle;">Group 1</th>

<th style="vertical-align: middle;">Group 2</th>

<th style="vertical-align: middle;">Group 3</th>

<th style="vertical-align: middle;">Group 4</th>

<th colspan="1" rowspan="8" style="text-align: center; vertical-align: middle;"> </th>

<th style="text-align: center; vertical-align: middle;">Mode</th>

<th style="text-align: center; vertical-align: middle;">Mean</th>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Not being able to understand some of the scientific terms used in the research or the research methods used by the scientists.</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Not being able to recognize what the research means within the context of a specific research field or related fields and how it compares to other research that has been done.  
</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Feeling uncertain about the scientific results and what they mean in the context of everyday life.</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4.3</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Finding two or more research papers that give contradictory information and not being able to decide which information is correct or most useful.</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">4.5</td>

</tr>

<tr>

<td style="vertical-align: top;">Not being able to find scientific literature that is written in a preferred language (Note: a language other than English).</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">4.5</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Not being able to judge whether or not the research is of high quality.</td>

<td style="text-align: center; vertical-align: middle;">7</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">7</td>

<td style="text-align: center; vertical-align: middle;">7</td>

<td style="text-align: center; vertical-align: middle;">5.3</td>

</tr>

<tr>

<td style="vertical-align: top;">Not knowing how to search the Web effectively to find the scientific literature in open access databases and journals.</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">7</td>

<td style="text-align: center; vertical-align: middle;">7</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">7</td>

<td style="text-align: center; vertical-align: middle;">5.3</td>

</tr>

</tbody>

</table>

Participants were primarily concerned about the fact that scientific or scholarly research literature is not cognitively accessible, and that comprehension is integral to knowing whether or not a document is credible.

> *   F4BA: 'I think that this [card] 'not being able to understand the terms' is very, very important.'F4WO: 'It has a relationship with this one [points to card about judging the quality of the research]'
> *   F4WO: 'because if you don't understand the terms, you're not able to judge...'
> *   F4BA: 'Yeah, the quality...'
> *   F4CH: 'And you can't compare [research articles]'
> *   F4LI: 'Yeah, but the quality would be high if it's in the database.'
> *   F3DA: 'Research is not just research in general. I mean, for example the global warming thing. I saw this programme on TV, National Geographic, about several guys saying global warming is just a lot of... you know... nonsense. And they were sponsored by an oil company. So gee, I mean, I wanna know the quality [of the research behind this statement].'
> *   Moderator: 'Maybe open access should tell you who the research sponsor is'
> *   F3DA: 'That would be terrific. If it's really totally open, like this, you know, about smoking and smoking is ok, but [the research] is sponsored by [a tobacco company]'

Some participants were not at all concerned about whether or not their online search skills were adequate enough. Their primary assumption was that as long as they could type keywords into a Google search engine they would find appropriate information.

> *   F3DO: 'I think that if you really want to know something, you will find a way to get there in any way. Finding it yourself or asking others.'
> *   F3MO: 'OK, but you also have to know the right terms to find it.'
> *   F3MZ: 'But even if you misspell the word, it is still going to go to the right place.'
> *   F3MO: 'Yeah, that's the truth. Yeah, yeah, you're right.'
> *   F3DO: 'Yeah [the search engine will say]: Are you looking for this perhaps?'
> *   F3MZ: 'Yeah, that's what it says. As you put in Google. So it doesn't matter. If you want to know about, let's say, headache, you type in 'head' not even 'ache' and you find everything, including headache, so I don't think that this is the most important thing anymore.'

There was a discussion amongst some participants concerning the large portion of open access literature available in English:

> *   F4MA: 'I think this one is second. The language one is on the same level as not knowing how to search.'
> *   F4BA: 'Yeah, but all the literature is almost in English, all the important ones, so I don't think so.'
> *   F4LI: 'Yeah, but it will be a big problem for, let's say, ehm, Dutch people or Italian people who don't speak English very well.'
> *   F4CH: 'On the other hand, most people that are likely to search the database also speak English.'
> *   F3AN: 'No, it has nothing to do with the Dutch. It has to do with if you are academically schooled, and if you're capable of understanding the English.'

When more than one piece of scientific or scholarly information is found on the Web, and the information is contradictory, some participants indicated that the contradictory evidence would be the most confusing and problematic if the underlying consequences associated with the information were serious (e.g., a medical disease or medical treatment).

> *   Moderator: 'Okay, finding two or more research articles that are contradictory [in their results and conclusions]. Is this a barrier?'
> *   F3DA: 'Well, it depends. Suppose you're looking at research developments regarding your own health, then you might not be able to understand it. Say you are a historian, looking for new historical evidence, then you might. So it depends, I think.'
> *   F2MZ: 'If it is life threatening, like one [research article] says that you are gonna die from it and the other says you're not gonna die from it. Yeah, then it's contradictory. You're gonna be like, OK, I want to hear the real story now!'
> *   F2DO: 'Yeah, it makes you want to go further.'
> *   F2MZ: 'It depends on the consequences.'

### What types of mediation strategies would be most helpful to lay people when they look for and use open access literature on the Web?

Group participants were asked to rank a set of open access mediation strategies on the Web (from a list of given strategies) and discuss their opinion concerning what would be most helpful to laypeople. Table 5 presents the results of the fourth group ranking exercise.

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 5: Mediation strategies associated with open access literature. Ranking exercise G.**</caption>

<tbody>

<tr>

<td style="vertical-align: top; width: 40%;"> </td>

<td style="text-align: center; vertical-align: middle;">Group 1</td>

<td style="text-align: center; vertical-align: middle;">Group 2</td>

<td style="text-align: center; vertical-align: middle;">Group 3</td>

<td style="text-align: center; vertical-align: middle;">Group 4</td>

<td colspan="1" rowspan="6" style="text-align: center; vertical-align: middle;"> </td>

<td style="text-align: center; vertical-align: middle;">Mode</td>

<td style="text-align: center; vertical-align: middle;">Mean</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">Web news reports or articles written by journalists (e.g., New York Times science page; BBC science page; Scientific American) with links to open access research articles.</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">3</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">A Web page or site prepared by a scientist or scientific research team explaining the importance of their research, with links to open access research articles.</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">3</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">A blog written by a scientist or scientific research team, providing weekly reports on the progress of their work, and links to open access research papers.</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">3</td>

<td style="text-align: center; vertical-align: middle;">3.5</td>

</tr>

<tr>

<td style="vertical-align: top; width: 40%;">A Wikipedia entry on the Web, which describes a subject in science and provides links to related open access research articles.</td>

<td style="text-align: center; vertical-align: middle;">6</td>

<td style="text-align: center; vertical-align: middle;">5</td>

<td style="text-align: center; vertical-align: middle;">1</td>

<td style="text-align: center; vertical-align: middle;">2</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">3.5</td>

</tr>

<tr>

<td style="vertical-align: top;">A discussion blog or Internet news group where individuals interested in scientific issues can contribute their opinions regularly and post links to open access research articles.</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

<td style="text-align: center; vertical-align: middle;">4</td>

</tr>

</tbody>

</table>

Some participants thought that Wikipedia might be a good starting point for locating information pertaining to science but others were concerned about whether or not it could be trusted as a reliable information source:

> *   F3MO: 'You can start at Wikipedia. It's an easy access for everybody as a start. And then, then you can find out if there is a blog or something where you can discuss with people the matter... the subject.'
> *   F4LI: 'Wikipedia is not very trustworthy. I mean, you can add stuff that's not true to Wikipedia.'
> *   F4WO: 'It's a good starting point.'
> *   F4LI: 'But it's not really reliable.'
> *   F4CH: 'Well, it's more reliable than a blog or a discussion. At least these are about the same, because on Wikipedia you have a [pause]. Behind the scenes of Wikipedia there's a huge discussion board [for people] to rage on and on and on about [whether or not] there should be a comma in the text or not. And if that's done behind the scenes, then the general public can just see the result of that discussion. Instead of having to read the entire discussion on blogs and on, ehm, yeah well, different people giving their opinions.'
> *   F4WO: 'If this links to the research, then people will start probably. If you Google any word now, you get Wikipedia very high on the list. So, if you click on this and then below on the Wikipedia page you see the link to the article, I think it's very useful.'
> *   F4MA: 'Yeah, because Wikipedia is really easy to enter.'
> *   F4CH: 'If you really want to dive into the subject, you can go to the research paper. But I doubt many people will. People who go to Wikipedia probably won't follow the links that are at the bottom of the text. They'll just take the information as granted. '

A few participants were wary about using blogs to discuss new research information:

> *   F3RE: 'You can get your information directly, with a blog, a discussion blog. And with this other [points to card regarding journalist news reports] you can only find a paper, but you can't talk to others.'
> *   F3MZ: 'Yeah, I understand, but not everybody wants to openly discuss their search with everybody. I want to do it for myself. So that's personal, I don't want to share what I'm looking for, [for instance, information about] a terrible disease or something. That's why I want to look [for information] about it for myself first. If I really cannot find it, then I go to an open discussion with somebody.'
> *   F3MO: 'That is a decision. If you find something on Wikipedia, then you can make a choice. If you want to find a blog written by a scientist, or you...[pause]. This has to do with all the choices. Which way you want.'

Other participants held a debate about whether or not journalists and newspapers are trustworthy mediators of new science information:

> *   F1NA: 'For me personally, it's a news reporter, because most of the time they try to explain things in just simple language without using too many terms. And if I get interested in the subject that the news reporter [presents], I can always search further by the links that they give. So, for me personally, the first one is the news reporter.'
> *   F1DA: 'The problem with [journalists] is that they show you the things they want to show... the thing you want to see.'
> *   F1NA: 'But you get that with everyone, everything, because that's also what the researcher does, or what Wikipedia does, they all do that.'
> *   F1PI: 'The [media/journalists] has the tendency indeed to decide what subject we discuss. And also what subjects to ignore. So, let's say that health care is a very hot topic, then you'll find every link about that, but let's say that, ehm, crime rates are not a hot topic or something, they would ignore that probably.'
> *   F3DO: 'I think that once it is a journalist who writes about [a research issue], it's more coloured.'
> *   F3MZ: 'Yeah, journalist – the name says it. You don't always trust it.'
> *   F3RE: 'I think they will know more about [a scientific research subject] than the discussion blog.'
> *   F3MO: 'You don't trust journalists so much anymore.'
> *   F3MZ: 'No' [Laughter amongst the group]
> *   F3RE: 'When they have to write something, they have to look for information before. They have to.'
> *   Moderator: 'And what about the way they present the information.'
> *   F3MZ: 'I think it's more in a clear language than what the scientist would do.'

Some individuals from the focus group were convinced that new information should be mediated by scientists themselves, and wondered if Web sites created by scientists were visible enough:

> *   F4LI: 'I think that for the more educated people, they will probably go to the Website based on a university topic. Because you have found an article about [Professor X], you want to know more and then you go to his page, and then you go to the open access research.'
> *   F4CH: 'It won't be objective, because [Professor X] will just praise his own research.'
> *   F4LI: 'Yeah, but then there's more about the topic.'
> *   F4BA: 'But how do you get to the Website of [Professor X] in the first place?'
> *   F4CH: 'Exactly. That's something you are not gonna find.'
> *   F4SI: 'I think it is better to go to a university page.'
> *   Moderator: 'So do you think that the universities should do more of the front work?'
> *   F4WO: 'They should, yeah, in their homepage.'

## Conclusion

This research was based upon a small but varied sample of Dutch citizens, thus it cannot be considered a full explication of the information seeking and use habits of all persons living in The Netherlands. Our intent was to obtain preliminary insight into open access as a public information policy; hence, the opinions expressed by our focus group participants are presented mainly for discussion purposes and to inform individuals acting within a policy context.

In sum, we will focus on the following key issues: 1) lay conceptions associated with scientific or scholarly knowledge and the accessibility of scholarly research literature online (i.e., focus group outcomes), 2) The Netherlands current policy pertaining to science education and public awareness of science, and 3) some policy-oriented recommendations.

1) Lay conceptions associated with scientific or scholarly knowledge and the accessibility of scholarly research online.

As a result of our focus group sessions, we have learned the following:

*   Lay people recognize the value of health and medical treatment research (e.g., '_It has to do with people_' and '_gives hope_') but seem to be less aware of and less able to discuss the value of other areas of research and their impact on people and society (e.g., chemistry, physics, mathematics).
*   Laypeople are aware of the fact that advancements in science are slow and that science is '_taking very small steps_'.
*   Laypeople do not often question the credibility or authority of a piece of research, although some individuals will be concerned about the average person's ability to interpret the presentation of research '_facts_'. Most participants from the focus groups agreed that research would provide a person with information that was '_up to date_'.
*   Laypeople are less concerned about the skills that they need to locate research information on the Web and more concerned about understanding the scientific process, particularly the terms and methodological jargon used in some research.
*   Laypeople are wary of the fact that there is too much information on the Web, but assume that open access will stimulate users to be more critical of online information, because it will be more challenging to determine what types of literature are credible or not credible.
*   Laypeople appreciate the fact that research information might be useful for personal decision making and problem solving, but recognize that research outcomes are not necessarily useful in all situations. Participants in the focus groups emphasized a preference for human sources of information; either direct or by reference.
*   Laypeople are generally motivated to look for research information when confronted with a medical problem, even when the problem seems to also be challenging for scientists. If the individual knows that the cause of a disease or illness is uncertain, research literature can be of comfort, and help to alleviate self-blame [Vignette 1].
*   Laypeople prefer to approach research information when it is used in conjunction with other types of information, e.g., lay-oriented films, magazines and news reports: anything that will help the person '_move closer to the truth_' or be of use to people so that they can more easily make up their mind about what to believe and what not to believe [Vignette 3].
*   Laypeople do not seem to agree that openly accessible research information can help them to assess government policies or to determine that these policies best reflect their personal interests.
*   Laypeople generally trust journalists to interpret science for them in lay terms, but believe that [Dutch] journalists normally insert their own opinion; thus information coming directly from a university source is considered to be more trustworthy.

2) The Netherlands current policy and programmes for increasing public awareness.

In 2006, the _Science, Technology and Innovation report on Policies, Facts and Figures_ of the Dutch Ministry of Economic Affairs gave attention to 'public awareness' as a key science policy issue:

> Realising the Dutch ambition within Europe requires not only investments in research and innovation, but also calls for changes in the education system and an adequate strategy in terms of communicating science and technology. The problem is that relatively few pupils in secondary education choose a profile in science and technology, and the same is also the case for students in higher education. Public communication policy on science and technology is intended to motivate the general public, especially young persons, and to raise their interest in science and technology. ([The Netherlands 2006](#net06): 29).

In light of this issue, the Dutch government launched a National Action Plan on Science and Technology known as _the Delta Plan_. The purpose of which was to '_increase enrolment into (15% more), progression through and graduation from (15% more), science and technology subjects_'. A Science and Technology Platform '_was established to put this ambition into practice... and the platform has developed programmes throughout the educational chain... tailored to various sectors_' of the education system (Science, Technology and Innovation in the Netherlands 2006: 53). Final outcomes, including the impact of the Delta Plan are not yet known, but past research has shown that a person's level of education and civic scientific literacy are inextricably linked. How an individual makes use of informal science information resources, such as news magazines, science magazines and science Web sites also bears a positive relationship to scientific literacy (see [Miller 2001](#mil01); [2004](#mil04)).

3) Policy-oriented recommendations

Civic scientific literacy is a critical issue for countries who want citizens that can participate in a modern, knowledge-based economy. A scientifically literate population is as essential to economic prosperity as it is for social inclusion: individuals who possess some knowledge of scientific facts and concepts can follow science news and participate in public discourse on science-related issues. The best approach to scientific literacy is not to focus on the information provider's point of view, but to focus on ways to stimulate the public's engagement with problems and issues related to science. Past research, including the results of our focus group sessions, shows that people,

> do not simply expose themselves to information randomly; rather, they actively choose different media channels and types of information purposively, depending on their particular goals and their expectations about how well the media channels and information types will meet those goals ([Treise _et al._ 2003](#tre03): 315).

With this in mind, policymakers in The Netherlands are urged to consider how information-based technologies, including open access, might play a role in shaping social inclusion. For instance, Dutch institutions and government organizations might become more active in providing annotated links to digital repositories and open access journals as a way of encouraging persons from all walks of life to develop more refined attitudes of appreciation, interest, and inquiry surrounding science. This is a public good because it respects the individual's ability to make choices as an information consumer.

At the Rathenau Institute for Technology Assessment, policy researcher Jan Steyaert ([2000](#ste00)) examined the digital skills that citizens need in order to deal with technological developments in the information society, and distinguishes between instrumental skills (i.e., new structures in which information is contained) and strategic skills (i.e., the readiness to look for information, take decisions based on information and scan the environment for relevant information). One of the conclusions drawn from this research was that the government and other parties throughout The Netherlands have been focusing too much on one dimension—the physical infrastructure—and neglecting the skills that citizens would need to use new technologies.

Our focus group study provides evidence to suggest that laypeople are growing more and more familiar with the various types of information available on the Web, but do not necessarily know whether or not their search skills are effective, and cannot always decide upon what to believe or not to believe. To reap the benefits of open access, we recommend that strategic e-learning programmes be incorporated into Dutch classrooms to teach young students how to develop their capacity as online information consumers. School-based e-learning programs, with a strong focus on civic scientific literacy, might help prepare young Dutch citizens to: 1) recognize when scholarly or scientific research information is needed for problem-solving, 2) know where to look for such research information online and recognize its authority and credibility, 3) understand how research information is socially situated and produced, and 4) understand what this information means in the context of a scientific communication network and society as a whole.

## Acknowledgements

The author would like thank Prof. Peter van den Besselaar and The Rathenau Institute for providing financial support for this research, including the individuals who participated in the focus group sessions, and the anonymous referees who gave helpful comments on earlier drafts of this paper.

## <a id="author" name="author"></a>About the author

Alesia Zuccala is currently a Research Fellow at the Centre for Science and Technology Studies (CWTS), Leiden University. She received a Master's degree in Library and Information Science from the University of Western Ontario, Canada, and her PhD from the Faculty of Information at the University of Toronto, Canada. The work reported here was carried out as a Research Fellow at the Rathenau Institute, Science System Assessment Unit, The Hague, The Netherlands. She can be contacted at: [a.a.zuccala@cwts.leidenuniv.nl](mailto:a.a.zuccala@cwts.leidenuniv.nl)

<form action="">

<fieldset><legend style="border-right: 1px solid navy; border-bottom: 1px solid navy; padding: 0.1ex 0.5ex; color: white; background-color: rgb(94, 150, 253); font-size: medium; font-weight: bold;">References</legend>

*   <a id="ACR00" name="ACR00"></a>Association of College & Research Libraries. (2000). _[Information literacy competency standards for higher education.](http://www.webcitation.org/5o3KCESd2)_ Chicago, IL: Association of College and Research Libraries. Retrieved 26 November, 2007 from http://www.ala.org/ala/mgrps/divs/acrl/standards/informationliteracycompetency.cfm. (Archived by WebCite® at http://www.webcitation.org/5o3KCESd2)
*   <a id="bau94" name="bau94"></a>Bauer, M.W., Durant, J. & Evans, G. (1994). European public perceptions of science. _International Journal of Public Opinion Research_, **6**(2), 163-186.
*   <a id="ben01" name="ben01"></a>Bensaude-Vincent, B. (2001). A genealogy of the increasing gap between science and the public. _Public Understanding of Science_, **10**(1), 99-113\.
*   <a id="ber98" name="ber98"></a>Berg, B.L. (1998). _Qualitative research methods for the social sciences._ (3rd. ed.). Needham Heights, MA: Allyn & Bacon.
*   <a id="blo01" name="blo01"></a>Bloor, M., Frankland, J., Thomas, M. & Robson, K. (2001). _Focus groups in social research_. London: Sage Publications Ltd.
*   <a id="bud02" name="bud02"></a>[Budapest Open Access Initiative](http://www.webcitation.org/5o3KL9e06). (2002). Retrieved 20 June, 2008, from http://www.soros.org/openaccess/read.shtml. (Archived by WebCite® at http://www.webcitation.org/5o3KL9e06)
*   <a id="cal99" name="cal99"></a>Callon, M. (1999). The role of laypeople in the production dissemination of scientific knowledge. _Science Technology & Society_, **4**(1), 81-94\.
*   <a id="cha91" name="cha91"></a>Chatman, E. A. (1991). Life in a small world: applicability of gratification theory to information-seeking behavior. _Journal of the American Society for Information Science_, **42**(6), 438-449.
*   <a id="col97" name="col97"></a>Cole, C. (1997). Information as process: the difference between corroborating evidence and 'information' in humanistic research domains. _Information Processing and Management_, **33**(1), 55-67.
*   <a id="der92" name="der92"></a>Dervin, B. (1992). From the mind's eye of the user: the sense-making qualitative-quantitative methodology. In J. Glazier and R. L. Powell (Eds.), _Qualitative research in information management_ (pp. 61-83). Englewood, CO: Libraries Unlimited.
*   <a id="dij05" name="dij05"></a>Dijkstra, M., Buijtels, H.E.J.J.M. & van Raaij, W.F. (2005). Separate and joint effects of medium type on consumer responses: a comparison of television, print, and the Internet. _Journal of Business Research_, **58**(3), 377-386.
*   <a id="dur99" name="dur99"></a>Durrentberger, G., Kastenholz, H. & Behringer, J. (1999). Integrated assessment focus groups: bridging the gap between science and policy? _Science and Public Policy_, **26**(5), 341-349\.
*   <a id="ell89" name="ell89"></a>Ellis, D. (1989). A behavioural approach to information retrieval design. _Journal of Documentation_, **45**(3), 171-212\.
*   <a id="ell93" name="ell93"></a>Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_, **49**(4), 356-369.
*   <a id="fog99" name="fog99"></a>Fogg, B. J. & Tseng, H. (1999). [The elements of computer credibility.](http://www.webcitation.org/5o3LJAvhp) In _Proceedings of the SIGCHI conference on Human factors in computing systems: the CHI is the limit, Pittsburgh, Pennsylvania._ (pp. 80-87). New York, NY: ACM Press. Retreived 7 March, 2010 from http://captology.stanford.edu/pdf/p80-fogg.pdf (Archived by WebCite® at http://www.webcitation.org/5o3LJAvhp)
*   <a id="hil90" name="hil90"></a>Hilgartner, S. (1990). The dominant view of popularization: conceptual problems, political uses. _Social Studies of Science_, **20**(3), 519-539.
*   <a id="jac06" name="jac06"></a>Jacobs, N. (Ed.). (2006). _Open access: key strategic, technical and economic aspects._ Oxford: Chandos Publishing Ltd.
*   <a id="jas00" name="jas00"></a>Jasanoff, S. (2000). The 'science wars' and American politics. In M. Dierkes and C.von Grote (Eds.), _Between understanding and trust: the public, science and technology_ (pp. 39-60). Amsterdam: Harwood Academic Publishers.
*   <a id="jur69" name="jur69"></a>Jurdant, B. (1969). [Vulgarisation scientifique et idéologie.](http://www.webcitation.org/5o3Lb1YJj) [Ideology and the popularisation of science.] _Communications_, No. 14, 150-161\. Retrieved 7 March, 2010 from http://www.persee.fr/web/revues/home/prescript/article/comm_0588-8018_1969_num_14_1_1203 (Archived by WebCite® at http://www.webcitation.org/5o3Lb1YJj)
*   <a id="kim07" name="kim07"></a>Kim, H.-S. (2007). PEP/IS: a new model for communicative effectiveness of science. _Science Communication_, **28**(3), 287-313.
*   <a id="koo06" name="koo06"></a>Koolstra, C.M., Boss, M.J.W. & Vermeulen, I.E. (2006). Through which medium should science information professionals communicate with the public: television or the Internet? _Journal of Science Communication_, **5**(3), 1-8.
*   <a id="kuh91" name="kuh91"></a>Kuhlthau, C.C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-367.
*   <a id="kuh97" name="kuh97"></a>Kuhlthau, C.C. (1997). The influence of uncertainty on the information seeking behaviour of a securities analyst. In P. Vakkari, R. Savolainen, and B. Dervin (Eds.), _Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different contexts, 14-16 Tampere, Finland_ (pp. 268-275). London: Taylor Graham.
*   <a id="lat87" name="lat87"></a>Latour, B. (1987). _Science in action_. Cambridge, MA: Harvard University Press.
*   <a id="ley87" name="ley87"></a>Leydesdorff, L. & van den Besselaar, P. (1987). What have we learned from the Amsterdam science shop? _Sociology of the sciences yearbook_, **11**, 135-160.
*   <a id="ley05" name="ley05"></a>Leydesdorff, L. & Ward, J. (2005). Science shops: a kaleidoscope of science-society collaborations in Europe. _Public Understanding of Science,_ **14**(4), 353-372.
*   <a id="liu04" name="liu04"></a>Liu, Z. (2004). Perceptions of credibility of scholarly information on the Web. _Information Processing and Management_, **40**(6), 1027-1038.
*   <a id="mil01" name="mil01"></a>Miller, J. D. (2001). The acquisition and retention of scientific information by American adults. In J. H. Faulk (Ed.) _Free-choice science education: how we learn outside of school_ (pp. 93-114). New York. NY: Teachers College Press.
*   <a id="mil04" name="mil04"></a>Miller, J. D. (2004). Public understanding of, and attitudes toward, scientific research: what we know and what we need to know. _Public Understanding of Science_, **13**(1), 273-294.
*   <a id="nar10" name="nar10"></a>_[NARCIS. The gateway to Dutch scientific information.](http://www.webcitation.org/5oG3zzb61)_ (2010). Retrieved 14 March, 2010 from http://www.narcis.info/ (Archived by WebCite® at http://www.webcitation.org/5oG3zzb61)
*   <a id="net06" name="net06"></a>The Netherlands. _Ministry of Economic Affairs._ (2006). _[Science, technology and innovation in the Netherlands. Policies, facts and figures](http://www.minocw.nl/documenten/Science-Technology-Innovation-brochure-2006.pdf)_. The Hague: Ministry of Economic Affairs. Retrieved 3 February, 2009 from http://www.minocw.nl/documenten/Science-Technology-Innovation-brochure-2006.pdf
*   <a id="rie02" name="rie02"></a>Rieh, S. Y. (2002). Judgement of information quality and cognitive authority in the Web. _Journal of the American Society for Information Science and Technology_, **53**(2), 145-161.
*   <a id="sel96" name="sel96"></a>Self, C. S. (1996). Credibility. In M. Salwen & D. Stacks (Eds.) _An integrated approach to communication theory and research_ (pp. 421-444). Mahwah, NJ: Erlbaum.
*   <a id="spi01" name="spi01"></a>Spink, A. & Cole, C. (2001). Information and poverty: information-seeking channels used by African American low-income households. _Library and Information Science Research_, **23**(1), 45-65.
*   <a id="ste00" name="ste00"></a>Steyaert, J. (2000). _Digital vaardigheden: geletterdheid in de informatiesamenleving_ [Digital skills: literacy in the information society] The Hague: Rathenau Instituut. (Rathenau Instituut Werkdocument 76).
*   <a id="stu04" name="sty04"></a>Sturgis, P. & Allum, N. (2004). Science in society: re-evaluating the deficit model of public attitudes. _Public Understanding of Science_, **13**(1), 55-74.
*   <a id="sur07" name="sur07"></a>Surf Foundation. (2007). _[Support growing for open access to scientific publications.](http://www.webcitation.org/5oG4SfFwx)_ (Press release. March 1, 2007.) Retrieved 15 March, 2010 from http://www.surffoundation.nl/en/actueel/Pages/Support-growing-for-open-access-to-scientific-publications.aspx (Archived by WebCite® at http://www.webcitation.org/5oG4SfFwx)
*   <a id="tay91" name="tay91"></a>Taylor, R.S. (1991). Information use environments. _Progress in Communication Sciences_, **10**, 217-250.
*   <a id="tre03" name="tre03"></a>Treise, D., Walsh-Childers, K., Weigold, M.F. & Friedman, M. (2003). Cultivating the science internet audience: impact of brand and domain on source credibility for science information. _Science Communication_, **24**(3), 309-332\.
*   <a id="tse99" name="tse99"></a>Tseng, S. & Fogg, B. J. (1999). Credibility and computing technology. _Communications of the ACM_, **42**(5), 39-44\.
*   <a id="US" name="US"></a>U.S. National Academy of Sciences. (1998). [National science education standards](http://www.webcitation.org/5oG3COA9m). Washington, DC: National Academies Press. Retrieved 15 March, 2010 from http://www.nap.edu/openbook.php?record_id=4962&page=R2 (Archived by WebCite® at http://www.webcitation.org/5oG3COA9m)
*   <a id="wat02" name="wat02"></a>Wathen, C. N. & Burkell, J. (2002). Believe it or not: factors influencing credibility on the Web. Journal of the American Society for Information Science and Technology, **53**(2), 134-144.
*   <a id="wil06" name="wil06"></a>Willinsky, J. (2006). _The access principle: the case for open access to research and scholarship_. Cambridge, MA: Massachusetts Institute of Technology Press.
*   <a id="wil83" name="wil83"></a>Wilson, P. (1983). _Second-hand knowledge: an inquiry into cognitive authority_. Westport, CT: Greenwood Press.
*   <a id="wil81" name="wil81"></a>Wilson, T.D. (1981). [On user studies and information needs.](http://www.webcitation.org/5o3XvDUSu) _Journal of Documentation_, **37**(1), 3-15\. Retrieved 7 March, 2010 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite® at http://www.webcitation.org/5o3XvDUSu)
*   <a id="wil99" name="wil99"></a>Wilson, T.D. (1999). [Models in information behaviour research.](http://www.webcitation.org/5o3Y3sUIY) _Journal of Documentation_, **55**(3), 249-270\. Retrieved 7 March, 2010 from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite® at http://www.webcitation.org/5o3Y3sUIY)
*   <a id="wyn92" name="wyn92"></a>Wynne, B. (1992). Public understanding of science research: new horizons or hall of mirrors? _Public Understanding of Science_, **1**(1), 37.
*   <a id="zuc09" name="zuc09"></a>Zuccala, A. (2009). The layperson and open access. _Annual Review of Information Science and Technology_, **43**, 359-396.

</fieldset>

</form>



## <a id="app" name="app"></a>Appendix - Focus group exercises

A. Cognitive response exercise.

On the following paper, write down the first thoughts or ideas that come to your mind about open access research literature located on the Web. (Note: this exercise is designed to determine what people know or do not know about open access and what they think of this subject prior to the influence of the group discussions).

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="vertical-align: top; text-align: center;">Negative ideas</td>

<td style="vertical-align: top; text-align: center;">Neutral ideas</td>

<td style="vertical-align: top; text-align: center;">Positive ideas</td>

</tr>

</tbody>

</table>

B. Debriefing.

After the first exercise we provided participants with the following written explanation:

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="vertical-align: top;">What is open access? Open access is an internet-based scholarly communication movement dedicated to facilitating the work of scholars and scientists and increase the citation impact, quality and value of new research.  

By Open access to scholarly and scientific research... 'we mean its free availability on the public internet, permitting any users to read, download, copy, distribute, print, search, or link to the full texts of these articles... or use them for any other lawful purpose, without financial, legal, or technical barriers other than those inseparable from gaining access to the internet itself' [5].  

For the international research community to achieve open access two strategies are recommended:  
1) Self archiving: scholars deposit their refereed journal articles in open electronic archives or digital repositories created at their universities or research institutes.  
2) Open access journals: a new generation of journals on the Web that no longer invoke copyright to restrict access to and use of the material they publish, but make literature freely available to anyone who wants to download and read it.  

Show the group participants pages - print or overhead projection - of different open access repositories on the Web. Place example copies of open access articles on the table.  

</td>

</tr>

</tbody>

</table>

C. Cognitive response question.

Each card names an area of scholarly or scientific study. Sort the cards according to the level of interest laypeople might have in reading research produced in these areas, and explain what their motivations may be. You may share personal experiences. (Note: Exercise is designed to encourage participants to think about which areas of research would be of interest to the lay public and which would be most readable. Here we want individuals to speak about their own personal interests).

<table width="70%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd">

<tbody>

<tr>

<td style="vertical-align: top; text-align: center;">Agriculture and food sciences</td>

<td style="vertical-align: top; text-align: center;">Arts and architecture</td>

<td style="vertical-align: top; text-align: center;">Chemistry</td>

<td style="vertical-align: top; text-align: center;">Health sciences and psychology</td>

<td style="vertical-align: top; text-align: center;">Mathematics and statistics</td>

</tr>

<tr>

<td style="vertical-align: top; text-align: center;">Physics and astronomy</td>

<td style="vertical-align: top; text-align: center;">Biology and life sciences</td>

<td style="vertical-align: top; text-align: center;">Economics and business</td>

<td style="vertical-align: top; text-align: center;">Earth and environmental sciences</td>

<td style="vertical-align: top; text-align: center;">History and archaeology</td>

</tr>

<tr>

<td style="vertical-align: top; text-align: center;">Law and political sciences</td>

<td style="vertical-align: top; text-align: center;">Technology and engineering</td>

<td style="vertical-align: top; text-align: center;">Sociology and media studies</td>

<td style="vertical-align: top; text-align: center;">Philosophy and religion</td>

<td style="vertical-align: top; text-align: center;"> </td>

</tr>

</tbody>

</table>

D. Ranking exercise.

What is the most important open access benefit for laypeople? Discuss the points below, rank in order of importance, and indicate other possible benefits:

*   Open access literature will help to increase the level of understanding that people have of scientific research terms (e.g., DNA, stem cells, greenhouse effect), research processes, and findings.
*   Open access will allow people to satisfy their curiosity about what type of research is being done in certain fields and the latest findings.
*   Open access will empower laypeople who want to read and use research literature for personal decision making and problem solving.
*   Open access will allow tax-paying citizens to see where and how money is being invested to support new scientific research.
*   Open access will help people to see what scientific researchers are doing in their own country and acquire sufficient levels of accurate information on which to base their assessments of government policies so that their policy preferences best reflect their own interests.

E. Vignettes exercise.

With each case described below, explain why you think that it would be useful to look for Open Access research information or why you think other forms of information would be beneficial?

*   A 28-year-old woman has a child who is two years old and she has just discovered from a paediatrician that the child may be autistic. The doctor tells her that he will arrange a visit to a specialist, and explains that the cause of autism is still not specifically known.
*   A 42-year-old father notices that his 10-year-old son enjoys playing violent video games, and has said that he would rather do this than go outside and play football. The father is becoming more and more concerned about it and what this means for his son's social and emotional development.
*   A 37–year-old high school teacher has been educating her students on the topic of global climate change. In class she must respond to a question from a 17 year old student, who says: 'My uncle has told me that humans are not really the major cause of global warming and that there is not much that we can do now to change it'.

F. Ranking exercise.

What would be the most significant barrier for laypeople when they look for and use open access research literature? Discuss the points below, rank in order of importance, and indicate other possible barriers:

*   Not knowing how to search the Web effectively to find the scientific literature in open access databases and journals.
*   Not being able to find scientific literature that is written in a preferred language (note: a language other than English).
*   Not being able to understand some of the scientific terms used in the research or the research methods used by the scientists.
*   Feeling uncertain about the scientific results and what they mean in the context of everyday life.
*   Not being able to judge whether or not the research is of high quality.
*   Not being able to recognize what the research means within the context of a specific research field or related fields and how it compares to other research that has been done.
*   Finding two or more research papers that give contradictory information and not being able to decide which information is correct or most useful.

G. Ranking exercise.

What types of open access mediation strategies would be most helpful to laypeople on the Web? Discuss the points below, rank in order of importance, and indicate other possible mediation strategies [Show the group printed examples of a blog, Wikipedia entry etc.]:

*   A Web page or site prepared by a scientist or scientific research team explaining the importance of their research, with links to open access research articles.
*   A blog written by a scientist or scientific research team, providing weekly reports on the progress of their work, and links to open access research articles.
*   A discussion blog or internet newsgroup where individuals interested in scientific issues can contribute their opinions regularly and post links to open access research articles.
*   Web news reports or articles written by journalists (e.g., New York Times science page; BBC science page; Scientific American) with links to research articles.
*   A Web page or site posted by an institute or university providing interpretive information concerning new research done by the institute or university's scientists, including links to research articles.
*   A Wikipedia entry on the Web, which describes a subject in science and provides links to related open access research articles.  



