#### vol. 13 no. 4, December, 2008


# Criterios para la evaluación de la usabilidad de los recursos educativos virtuales: un análisis desde la alfabetización en información



#### [M. A. Marzal](mailto:mmarzal@bib.uc3m.es), [Javier Calzada-Prado](mailto:fcalzada@bib.uc3m.es) and [Marina Vianello](mailto:mvianell@bib.uc3m.es)  
Departamento de Biblioteconomía y Documentación. Universidad Carlos III de Madrid. C/ Madrid, 128, 28903, Getafe, Madrid, Spain.

#### Abstract

> **Introducción.** Para que el aprendizaje en entornos virtuales resulte realmente significativo, deben darse dos condiciones básicas: disponer de competencias relacionadas con la alfabetización en informaicón y, en especial, disponer de criterios adecuados para evaluar contenidos digitales.  
> **Method.** El principal objetivo de este trabajo es presentar un modelo de evaluación de la usabilidad de recursos educativos, desarrollado desde el punto de vista cognitivo del procesamiento de la información así como de las competencias esperadas en los estudiantes.  
> **Analysis.** El método utilizado para desarrollar el modelo ha sido la revisión de literatura y categorización de criterios de usabilidad relevantes.  
> **Results.** Un modelo de evaluación para contenidos educativos digitales habrá de considerar los siguientes criterios: Captación (relacionada con el mecanismo cognitivo de la atención), Fidelidad (relacionado con la percepción y Capacidad Alfabetizadora (ligada a la memoria).  
> **Conclusions.** Las Ciencias de la Documentación pueden desempeñar un papel relevante en distintas áreas: diseño de instrumentos adecuados para la gestión de contenidos educativos, implementación del constructivismo desde su perspectiva y para sus propios propósitos, adoptar competencias relacionadas con la alfabetización en información como objetivos didácticos en la formación profesional y el desarrollo de métodos adecuados para evluar competencias y contenidos.


## Introducción

La Sociedad de la información ejerce dos efectos críticos en la Educación : la virtualidad y la primacía del usuario. Tales efectos implican, por una parte, el uso de recursos educativos virtuales disponibles principalmente en la Web, y por otra, la interacción directa del educando con los mismos. Para que el aprendizaje en entornos virtuales sea realmente significativo, habrán de cumplirse dos premisas. Por una parte, el educando habrá de disponer de habilidades cognitivas relacionadas con el pensamiento crítico para la toma de decisiones y la resolución de problemas, relacionadas en definitiva con aprender a aprender (metaaprendizaje). Por otra parte, los recursos seleccionados con fines educativos bien por el educando, bien por el educador o el bibliotecario, habrán de cumplir una serie de requisitos de usabilidad y accesibilidad que posibiliten una óptima experiencia de aprendizaje.

El presente trabajo tiene precisamente por objetivo principal presentar un modelo evaluativo de la usabilidad de los recursos educativos virtuales. Para ello, analiza en primer lugar el efecto de las Tecnologías de la Información y Comunicación (en adelante TIC) en la interacción entre información y cognición humana para la construcción del conocimiento, dando cuenta de las principales teorías pedagógicas que definen el nuevo modelo educativo de competencias para la Sociedad del conocimiento. A continuación, se introduce el concepto de usabilidad y los mecanismos cognitivos implicados en el acceso a la información contenida en los recursos para, finalmente, presentar el modelo de evaluación desarrollado.

## Conocimiento y teorías pedagógicas para un nuevo espacio de conocimiento

Desde la perspectiva actual de las Ciencias de la Educación, el conocimiento es una elaboración personal, fruto de las asociaciones que nuestro cerebro es capaz de hacer a partir de su experiencia, de la información que ya tenemos y de la que hemos seleccionado y analizado ([Marqués 2002](#marq02)). Esta perspectiva, para las Ciencias de la Documentación, se traduce en una secuencia cognitiva: los datos que conforman la información se perciben mediante los sentidos, luego se integran en la memoria cognitiva que acoge la _semántica del mundo_ ([Ingwersen 1992](#ingwer92)) para generar la información, que a su vez permite tomar decisiones. Mediante los procesos inferenciales, la información representa los datos en forma de _concepto_ para hacer posible el conocimiento de algo concreto, así como establece las formas de almacenar los conceptos para su posterior uso mediante una _asimilación_, que garantiza el conocimiento. El procesamiento de la información puede derivar, pues, en _cognición_, que tiene como componentes principales la interpretación, la representación y la formación de modelos que representan nuestra realidad.

Para determinadas corrientes epistemológicas, la naturaleza del conocimiento se distancia del _objeto_, elemento que proporciona la _experiencia_, diluyendo la relevancia de la _objetividad_ en el análisis de la realidad. Esto justifica que la función del _objeto_ y de la _objetividad_ en las inferencias sea relevante por optimizar la transformación de la información hacia el conocimiento. En esta transformación, para las Ciencias de la Documentación el _documento_ es esencial, en tanto que es al mismo tiempo _objeto_ y _experiencia_. McLuhan llegó a afirmar cómo el _mensaje_ (los datos convertidos en contenido informativo) _es el medio_ (el documento, cuya 'diplomática' determinaba el contenido). Sin embargo, el _medio_ ha sufrido una rápida y radical mutación como consecuencia del impacto de las TIC, originando el _hipertexto_ e _hipermedia_, donde el _mensaje_ debe amoldarse a sus determinantes requerimientos tecnológicos: el _medio es el mensaje_ ([Castells 2000](#castell00)).

Sus efectos sobre el conocimiento, durante el proceso cognitivo, son muy importantes: a) la virtualidad altera profundamente la _percepción_ y la _experiencia_ del objeto, por ende, también su _objetividad_, por cuanto proporciona un _plano intelectivo_ por el que se aprende por observación de un comportamiento preestablecido, así como un _plano de la experiencia_, donde se aprende por simulación e interacción; b) la primacía del usuario hace que el protagonismo del _aprendizaje_ en el proceso informativo hacia el conocimiento adquiera una dimensión cualitativamente distinta, por adquirir una función cognitiva en una _educación_ no formal.

Este 'contexto educativo' de los documentos derivados de las TIC, _documentos digitales_ o _recursos de información_ digitales, justifica la importancia de una eficaz _gestión de contenidos_ conforme a una 'arquitectura de la información', determinada por la accesibilidad y usabilidad de estos documentos. Los sistemas hipermedia, en efecto, permiten individualizar el acceso a los contenidos de los documentos digitales, en negación a la 'audiencia global', al tiempo que favorece el acceso individual por el interés, habilidad e interacción con los contenidos digitales a través de enlaces horizontales asociativos propios ([Pérez _et al._ 2001](#perez01)). Constituye, pues, un nuevo _espacio de conocimiento_ donde la asimilación de contenidos y consecución de destrezas se obtiene por la asimilación de capacidades y la consecución de competencias.

Dos teorías pedagógicas apoyan el diseño del nuevo escenario: el Cognitivismo y el Constructivismo ([Ertmer y Newby 1993](#ertmer93)). El _Cognitivismo_ (derivación del paradigma conductista) concibe el aprendizaje como producto de la interacción del sujeto con la información procedente del entorno, mediante unas representaciones mentales estructuradas en categorías para procesar la información, especialmente la _atención, la percepción, la memoria, la inteligencia, el lenguaje_ y _el pensamiento_. En el _Constructivismo_, por su parte, el conocimiento no se descubre, sino que se construye a través de un proceso de aprendizaje, que utiliza conocimientos anteriores, transformados por una selección que fundamenta hipótesis y toma de decisiones ([Onrubia 2005](#onrubia05)). El docente asienta su estrategia didáctica en el uso de los conocimientos previos, mientras el currículo estará organizado en espiral para sustentar el nuevo saber sobre saberes anteriores. El Constructivismo, pues, concede un incuestionable protagonismo al individuo y sitúa al aprendizaje en el corazón del proceso de acceso al conocimiento. En efecto, el _aprendizaje constuctivista_ descansa en la acción educativa cooperativa y en la plena libertad de navegación interactiva en una comunidad de aprendizaje, donde el docente constituye un referente para garantizar la calidad en la selección de contenidos educativos de los documentos digitales.

En el contexto de los nuevos espacios virtuales y la consiguiente evolución hacia nuevos modelos educativos, el Constructivismo parece converger con el Cognitivismo, especialmente en el impacto de los _modelos mentales_ y su representación (recuérdese los _mapas conceptuales_ de Ausubel), así como incorpora aportaciones del _Instructivismo_ respecto a la adquisición de conocimiento por uso experto de materiales didácticos, un método que justifica la eficacia educativa de los _tutoriales inteligentes_ y los _objetos de aprendizaje_.

## **Competencias y calidad de los contenidos educativos para el conocimiento**

La convergencia de estas corrientes pedagógicas y los estilos de aprendizaje más oportunos definen las destrezas técnicas y las habilidades intelectivas como prólogo de las _competencias,_ que el sujeto debe adquirir y desarrollar para su pleno progreso personal, laboral e intelectual en la Sociedad de la Información.

Una primera acepción de _competencia_ triunfó en el mundo anglosajón, referida a los procesos productivos. Los efectos de las TIC en el último cuarto del siglo XX, sin embargo, han ido alterando esta noción inicial. En 1982, R. Boyatzis concebía las competencias como una combinación de motivación, conocimientos, habilidades e idiosincrasia personal ([Boyatzis 1982](#boyatzis82)), una concepción que matizó Hayes, en 1985, entendiéndolas como una capacidad para usar conocimientos y destrezas relacionadas con productos y procesos en la consecución de objetivos ([Hayes 1985](#hayes85)). En la década de los 90, sin embargo, P. Gilbert y M. Parlier, las relacionan con la acción y la conducta, entendiéndolas como un conjunto de conocimientos, capacidades de acción y de comportamiento estructurados en función de un objetivo ante una situación concreta ([Gilbert y Parlier 1992](#gilbert92)). Bunk incluso orientó las capacidades y aptitudes al ejercicio de una profesión en la resolución de problemas y organización del trabajo, distinguiendo competencias técnica, metodológicas y sociales ([Bunk 1994](#bunk94)). A finales de los 90 e inicios del siglo XXI, las competencias se relacionan con la información y el _saber-hacer_, de modo que Marbach divide las competencias en saber, saber hacer, comportamiento, cognición, potencial y talento ([Marbach 1999](#marbach99)), dando pie a la concepción de Le Boterf, quien entiende las competencias como la combinación de conocimientos, redes de relación y saber hacer ([Le Boterf 2000](#leboterf00)).

El proceso de la llamada _globalización_, consustancial a la Sociedad de la Información, tiene como impacto económico la calidad de la producción, en íntima relación con una elevada cualificación profesional de la mano de obra, cuyo objetivo es adecuar al ciudadano al acelerado ritmo de la innovación científico-tecnológica y los cambios en la estructura y organización del trabajo. Esto implica, necesariamente, el traslado de la noción y función de las competencias laborales al ámbito educativo, una educación basada en competencias y orientada a la obtención de resultados, que son los productos del aprendizaje (conocimientos, habilidades, destrezas, aptitudes y actitudes). Las _competencias_, transformadas en un objetivo didáctico de primera magnitud, han precisado de una necesaria redimensión conceptual para acomodarlas a su función educativa. Surgen, así, nuevas definiciones, entre las que son significativas las de Lévy-Leboyer, en 2003, como _conjunto de conductas organizadas en el seno de una estructura mental, también organizada y relativamente estable y movilizable cuando es preciso_ ([Lévy-Levoyer 2003](#levy03)). La OCDE aprobó a finales de 1997 el proyecto DeSeCo (Definición y Selección de Competencias), cuyo informe ha sido publicado en 2003\. El proyecto DeSeCo ha identificado las competencias en tres dimensiones, a saber, la interacción en grupos socialmente heterogéneos, el ejercicio de conductas autónomas y la capacidad de usar herramientas de forma interactiva ([Colás Bravo 2005](#colas05)). La Unión Europea, por su parte, promovió el Proyecto _Tunning Educational structures in Europe_, también en 2003, cuyo objetivo era considerar los títulos universitarios en términos de resultados de aprendizaje y de competencias, tanto genéricas (instrumentales, interpersonales y sistemáticas), como específicas de cada área temática, entendiendo por competencias genéricas aquellas útiles en cualquier titulación (toma de decisiones, organización de tareas, autonomía en el aprendizaje, etc.), en tanto las competencias específicas se refieren al dominio cognitivo de cada área de conocimiento.

El paradigma en el modelo educativo se transforma: las estrategias didácticas de aprendizaje tendrán por objeto el proceso de aprendizaje del educando, absolutamente implicado y protagonista en su proceso formativo ([Martínez y Martí 2003](#martinez03)), mientras que las estrategias didácticas de enseñanza se orientarán a la asunción por el educando de habilidades, destrezas y competencias, en un orden cognitivo creciente. El paradigma educativo se encaminará hacia el _aula extendida_, un espacio virtual de comunidades digitales, organizadas sobre la interactividad de los materiales didácticos y la comunicación telemática a tiempo real con el profesor y sus condiscípulos.

La transformación del conocimiento para la Sociedad del conocimiento, cimentada en un modelo educativo competencial, supone una directa implicación de la investigación y profesión de la Documentación en el proceso. Así se ha hecho patente en la Declaración _Faros_ _para la Sociedad de la información_, durante el Coloquio de Expertos de Alejandría, reunido entre el 6 y 9 de noviembre de 2005 y organizado por la UNESCO y por el _National Forum on Information Literacy_, donde se enunciaron los dos referentes primordiales (faros) en el tránsito desde la Sociedad de la información: el _aprendizaje permanente_ y la _alfabetización en información_ .

•  **Aprendizaje permanente**, entendido como un instrumento idóneo para el progreso económico y social en una comunidad, que ya no descansa en la acción innovadora de un grupo selecto de científicos y técnicos, sino en la excelencia ciudadana en todos sus niveles, competentes en diferentes acciones y contextos. Se concibe así la _formación a lo largo de la vida_, en la que 'ocupación' y 'profesión' adquieren un significado distinto: la profesión se refiere a una formación competencial constante, sucesivamente más cualificada por la experiencia y una formación acumulativa a lo largo de la vida. El aprendizaje permanente, objeto de atención por parte de los ministros de Educación de los países de la OCDE, en 1996, se convierte en un instrumento de cohesión y de integración económica, social y cultural de los ciudadanos. Esta acepción de aprendizaje reclama el desarrollo de una _educación a lo largo de la vida_, que puede nutrirse por una formación formal, no formal e informal, por lo que _usabilidad_ y _accesibilidad_ de los recursos educativos adquieren una importancia evidente.

•  **Alfabetización en información** (en adelante ALFIN)**,** inicialmente entendida como la transformación de los servicios bibliotecarios tradicionales y medio de formar una ciudadanía crítica y activa. La ALFIN se entiende como un marco intelectual para comprender, encontrar, evaluar, y utilizar información, actividades que pueden ser conseguidas en parte por el manejo de las TIC, en parte por la utilización de métodos válidos de investigación, pero, sobre todo, a través del pensamiento crítico y el razonamiento. La ALFIN inicia, sostiene y extiende el aprendizaje a lo largo de toda la vida. Esta acepción ha relacionado la ALFIN con la Educación y particularmente con el concepto de aprendizaje permanente, convergencia que ya defendiera P.S. Breivik en el Congreso _Libraries and the Search for Academic Excellence_ en 1987 y se apuntara en las _Big Six_ de Eisenberg y Werkowitz . En 2000, la ALA publicaba las _Information Literacy Standards for Higher Education_, referente normativo mundial, que impulsó la edición en 2001 por la ACRL de los Objetivos de formación para la alfabetización en información: un modelo de declaración para bibliotecas universitarias y las Normas de Alfabetización en Información por el _Council of Australian University Librarians_, un modelo normativo que ha complementado en 2004 el _Australian and New Zealand Institute for Information Literacy_. La evolución conceptual de la ALFIN tiene como efecto convertirla en un instrumento idóneo para gestión de contenidos en recursos de información con intención educativa, razón por la que la investigación y aplicación de la ALFIN puede contar entre sus objetivos la evaluación de la accesibilidad y usabilidad de los recursos para la eficiencia en una educación por competencias, siendo un paso inicial necesario la definición de índices de calidad y su posterior conversión en indicadores ALFIN.

La prioridad en el análisis de accesibilidad y usabilidad de los recursos de información con intención educativa para la enunciación de un esquema de categorías, como apunte para la propuesta de un modelo evaluativo de competencias ALFIN, fue uno de los objetivos planteados en las fases metodológicas de dos Proyectos de investigación, uno financiado por la Dirección General de Investigación del Ministerio de Ciencia y Tecnología de España, DOTEINE (ref. BSO2003-04895), y otro por la Dirección General de Universidades e Investigación de la Consejería de Educación de la Comunidad de Madrid, IACORIE (ref. 06/HSE/0165/2004).

La finalidad del modelo evaluativo actual se dirige a fomentar, en los agentes críticos implicados en los procesos educativos, acciones destinadas a lograr resultados próximos a los objetivos que se plantean para la calidad y adecuación del sistema educativo en la Sociedad del conocimiento. El objetivo planteado es la evaluación del impacto educativo eficaz, por una adecuada interacción, entre unos recursos de información con una usabilidad de calidad y unos usuarios-educandos con unas competencias ALFIN suficientes, en concordancia con los mecanismos humanos de cognición.

El objetivo director de la propuesta llevó a la elaboración de un cuestionario destinado a ser estudio inicial de campo sobre usabilidad de recursos y competencia de usuarios. Los resultados ([Marzal y Calzada-Prado 2003](#marcal03)) sirvieron de base para establecer unos primeros criterios selectivos en usabilidad y competencias ALFIN ([Marzal, Colmenero y Morato 2003](#marcol03)), a su vez fundamento para establecer un repertorio de categorías evaluativos, preliminar de futuros indicadores. Resultados y criterios fueron inscritos en el marco de modelos pedagógicos de aprendizaje, así como de principios de usabilidad desde la Documentación, conforme a las tendencias detectadas en la escasa literatura científica especializada en este campo concreto. La noción de usabilidad a efectos de la propuesta, su metodología y resultado se exponen a continuación.

## Usabilidad y acceso al conocimiento

Aunque el concepto de usabilidad no es exclusivo de un ámbito concreto, y por tanto existen diversas aproximaciones al mismo, la definición de mayor aceptación es sin duda la presentada por la norma ISO/IEC 9241 _Ergonomic requirements for visual display terminals_ ([1998](#iso98)), en su undécima parte, _Guidance for usability_, en la que se define como el grado con que un determinado producto, en un contexto de uso específico, permite al usuario alcanzar sus objetivos con eficacia, eficiencia y satisfacción.

Los orígenes de la usabilidad se encuentran en la ergonomía cognitiva, disciplina que estudia cómo el usuario construye un modelo mental del objeto que usa y cómo éste afecta a su interacción con el mismo. Para la ergonomía cognitiva, el formato adoptado en la presentación y representación de la información es crítico para su uso, de ahí que la usabilidad se preocupe de cuidar que las interfaces, mediadoras entre la máquina y el usuario, reflejen la lógica del sistema y sean capaces de transmitir al usuario la mejor forma de utilizarlo.

Estrictamente ligado a la representación de un modelo cognitivo, la interfaz es el elemento que condiciona la generación de significado. Puede ser considerado un medio, que refleja, por lo tanto, un modelo predefinido que se plasma en función de una idea preconcebida de las necesidades del usuario. Puede ser también considerado el elemento que proporciona las condiciones de la acción para que el usuario intervenga sobre unos objetos que, a su vez, están determinados por una prácticas de trabajo. Por esta razón, el éxito de su diseño guarda estrecha relación con el concurso de conceptos y metodologías de varias disciplinas, como la Informática (concepción de las aplicaciones e ingeniería de las interfaces humanos) la Psicología (aplicación de las teorías de los procesos cognitivos y análisis empírico del comportamiento de los usuarios), la Sociología , la Antropología (interacción entre tecnología, trabajo y organización) y el diseño industrial (ergonomía), sin olvidar que los sitios web son artefactos cognitivos y su usabilidad depende en primer lugar de su capacidad para respetar nuestros procesos físicos y cognitivos.

### Los mecanismos de la cognición humana

Los mecanismos de la cognición han sido estudiados desde el punto de vista del procesamiento humano de la información por, entre otros, Delclaux y Seoane ([1982](#delclaux82)) o Vickery y Vickery ([2004](#vickery04)). Para estos últimos, el procesamiento de la información implica las siguientes actividades: 1) Percepción; 2) Formación de conceptos; 3) Almacenamiento en la memoria; 4) Recuperación de la memoria; 5) Contrastación de lo percibido con la memoria; 6) Contrastación de los atributos almacenados con el entorno. Nos detendremos en los tres principales elementos participantes en el proceso: la percepción, la atención y la memoria.

#### Percepción

La percepción es la actividad encargada de elaborar la información que los sentidos captan del ambiente, para integrarla en la actividad mental. Los órganos sensoriales actúan de forma altamente especializada, registrando los estímulos que cumplen determinadas condiciones. Sucesivamente los órganos implicados interpretan e integran las diferentes características del mensaje para sucesivamente traducirlos en una señal nerviosa que será elaborada por otras áreas del cerebro adquiriendo significado para el sujeto. Concluidas estas actuaciones se produce el reconocimiento del objeto como perteneciente a cierta clase. El sujeto opera entonces una selección entre los estímulos ambientales que recibe, mediatizada por sus experiencias anteriores y sus necesidades ([Postman _et al._ 1948](#postman48)), y afectada por las distintas propiedades de los estimulos, como la novedad, la incertidumbre, el conflicto y la complejidad, que atraen de manera especial la atención del individuo ([Berlyne,1960](#berlyne60)).

Todo cuanto atañe a la percepción afecta a aquellos aspectos de la usabilidad que permiten el acceso al documento. La usabilidad de los contenidos textuales en la Web depende en gran medida de su legibilidad, tanto lingüística como visual. Mientras la legibilidad lingüística depende del tipo de lenguaje empleado en la confección de los documentos, la legibilidad visual depende de l a facilidad y precisión con la que el lector identifica y decodifica las letras que componen las palabras y las frases. La correcta percepción es condición necesaria para la accesibilidad, requisito que todo documento debe satisfacer para todo usuario, tenga o no problemas perceptivos. El incumplimiento de este aspecto dificulta, o en el peor de los casos impide, el acceso al contenido de los documentos digitales.

#### Atención

La atención es la función responsable de regular las actividades de los procesos mentales filtrando y organizando las informaciones recibidas para que el sujeto pueda emitir respuestas adecuadas y para que pueda actuar tanto de forma automática como controlada. En realidad, no es fácil trazar una clara línea de demarcación entre percepción y atención, ya que las dos funciones resultan fuertemente imbricadas entre sí. Existe, sin embargo, un acuerdo general en que la atención interviene en diferentes niveles de los procesos mentales y posee diferentes estrategias de actuación. La neuropsicología actual suele distinguir entre atención distribuida, orientada espacialmente, focalizada, selectiva y sostenida.

Se habla de atención distribuida cuando el sujeto puede tener bajo control todo el campo perceptivo que lo rodea a la espera de la aparición de un estimulo que le atraiga. Entonces la atención actúa ante de forma espacial dirigiéndose hacía la posición donde apareció el estimulo ([Sokolov 1983](#sokolov63)). Sin embargo, cuando la atención es capturada en posiciones que luego resultan no ser informativas, pasado un breve intervalo de tiempo a la espera que se produzca algún tipo de acontecimiento, se desplaza de forma automática para explorar nuevas zonas ([Posner y Cohen 1984](#posner84); entre otros). Esto nos permite extrapolar que será más facil capturar la atención del lector cuidando la coherencia estilística de los sitios, o, en caso de páginas cuyos tiempos de carga presenten alguna demora, anticipar la visualización de su estructura.

La focalización de la atención posee un componente automático y uno voluntario. El automático resuelve eventuales conflictos entre esquemas de acción y el mecanismo de control consciente permite adoptar, ante situaciones nuevas, una respuesta flexible ([Norman y Shallice 1980](#norman80)). Sin embargo, la capacidad de control resulta limitada y es fácil constatar que con la práctica muchas actividades acaban por automatizarse. Este proceso parece ser el resultado de una reestructuración cognitiva que permite ahorrar recursos cognitivos ([Cheng 1985](#cheng85)). Una vez automatizados los procedimientos ya no requieren control ninguno, pero por esta misma razón resultan difíciles de modificar.

La fijación del foco es el prerrequisito para el buen funcionamiento de la atención selectiva que consiste en la habilidad de concentrar la atención en un canal que contiene informaciones fuertemente contrastadas por elementos distractores. Su intervención, necesaria para evitar que nuestro sistema cognitivo se sobrecargue de información, no se produce en un momento prefijado sino que varía en función del material, de la modalidad y del tipo de tarea. Un ejemplo paradigmático del funcionamiento de la atención selectiva en la Web es el fenómeno del _banner blindness_. El usuario ignora las bandas publicitarias presentes en la página cuando su mensaje resulta lejano del contexto de sus intereses, pues las registra como un elemento distractor y molesto, sin importar cuánto brillen y se muevan.

#### La memoria

La memoria comprende los procedimientos que permiten adquirir, conservar, recuperar y utilizar conocimientos y habilidades. Sus tres etapas esenciales son: la codificación, el almacenamiento y la recuperación ([Sternberg 1996](#sternberg96)). La codificación implica la selección del material a codificar por parte de la atención y puede ser llevada a cabo en diferentes niveles de profundidad ([Craik y Tulving 1975](#craik75)). La etapa de almacenamiento es la encargada de conservar la información en el tiempo y a menudo ha sido explicada recurriendo a una analogía con el ordenador. El modelo clásico de Atkinson y Shiffrin ([1971](#atchinson91)) describe la memoria como un sistema organizado en tres etapas: sensitiva, a corto plazo y a largo plazo. Tras la experiencia sensitiva, caracterizada por una duración muy limitada y una ilimitada capacidad para registrar estímulos ([Neisser 1967](#neisser67)), se pasa a la etapa de memoria a corto plazo. Esta memoria, que puede ser interpretada como una memoria distribuida, compuesta de una unidad interna y una externa, permite delegar a un sistema externo parte de las informaciones necesarias para desarrollar una tarea, evita la sobrecarga cognitiva y hace que el comportamiento del sujeto sea determinado tanto por sus recursos cognitivos como por sus vínculos y su capacidad de interactuar con el mundo exterior creando estrategias específicas ([Zhang 1994](#zhang94)). Finalmente, la memoria a largo plazo, cuya capacidad y duración es ilimitada, se ocupa de la información que no está siendo elaborada. Este tipo de memoria ofrece un acceso a la información en ocasiones difícil, requiriendo mecanismos que faciliten la recuperación.

Anderson ([1983](#anderson83)) ha desarrollado una teoría general de la memoria como sistema de producción. Este modelo parte de la idea de que todo conocimiento arranca siendo proposicional o declarativo para, en una etapa sucesiva, poder convertirse en procedimiento. Así, en la memoria a largo plazo se distingue la memoria declarativa, que almacena la información según criterios fácticos o conceptuales y según formatos que pueden ser verbales o simbólicos; y la memoria procedimental, que almacena los procedimientos siguiendo las reglas de producción. Estos procedimientos vienen aplicados a los contenidos de la memoria de trabajo y pueden producir nuevos conocimientos declarativos, crear nuevas reglas de producción o modificar viejas reglas almacenadas con anterioridad.

La situación del usuario que se enfrenta a la tarea de buscar información presenta numerosos puntos en común con las tareas necesarias para asimilar nuevos contenidos. El usuario, cuando se mueve por la red buscando información, necesita un conjunto de herramientas que, representando una extensión de su memoria natural, le permitan:

*   organizar su navegación superando la sensación de estar perdiendo información;
*   mantener una visión clara de cual es el entorno informativo en que se mueve;
*   seleccionar los recursos que le interesan y dejar constancia de la dirección seguida para confeccionar la ruta;
*   exteriorizar la finalidad de su navegación anotando y organizando los recursos presentes en el sitio según criterios argumentales para su explotación.

## Criterios generales para la evaluación de la usabilidad

La duodécima parte del estándar ISO/IEC 9241 ([1998](#iso98)) desarrolla los tres grandes principios de la usabilidad (eficacia, eficiencia y satisfación) en una serie de parámetros, necesarios para medir cuantitativamente el grado de cumplimiento de los mismos. Como parámetros que afectan al aspecto gráfico de la interfaz, el estándar señala la claridad, la capacidad de discriminación, la concisión, la coherencia, la identificabilidad, la legibilidad y la comprensibilidad. Como parámetros que afectan la esfera cognitiva de la interacción usuario-interfaz se señalan la adecuación a la tarea, la autodescripción, la controlabilidad, la conformidad con las expectativas del usuario, la tolerancia al error, la posibilidad de personalización y la facilidad de aprendizaje.

Otros requisitos generales identificados en la literatura sobre usabilidad son:

*   La velocidad de descarga o funcionamiento del recurso ([Shneiderman 1998](#shneiderman98); [Nielsen 1993](#nielsen93)).
*   La retención en el tiempo. El usuario no debería olvidar el manejo del recurso si deja de usarlo temporalmente ([Shneiderman 1998](#shneiderman98); [Nielsen 1993](#nielsen93)) .
*   La satisfacción subjetiva al usar el recurso ([Shneiderman 1998](#shneiderman98); [Nielsen 1993](#nielsen93)) .
*   La diferencia entre el grado de usabilidad objetiva y la facilidad de uso percibida ([Venkatesh y Davis 1996](#venkatesh96)). La facilidad de uso no sólo es determinada por la usabilidad del sistema, sino también por la competencia y el grado de eficacia de cada usuario
*   Habilidades. El usuario debe tener la sensación de que el sistema apoya, complementa y realza sus habilidades y experiencia ([Cato 2001](#cato01)).
*   Las capacidades (y limitaciones) motoras, cognitivas y preceptúales del usuario.
*   Privacidad. El sistema dispone de los medios necesarios para proteger su información y/o la de sus clientes ([Cato 2001](#cato01)).

Por su parte, el ya famoso decálogo de Nielsen nos proporciona una reglas de carácter general para la evaluación de la usabilidad ([Nielsen y Morkes 1998](#nielsen98)):

*   Visibilidad. El sistema debe transmitir en todo momento su estado. El usuario debe saber si se está produciendo la carga de una página o si esta función se ha interrumpido por algún problema en la red. De no ser así, el usuario no sabrá a qué atenerse.
*   Correspondencia entre el sistema y el mundo real. La información presente en las páginas debe considerar la realidad de su usuario ideal.
*   Libertad y posibilidad de control del sistema por parte del usuario. El sistema debe hacer visible su organización del contenidos desde la primera página.
*   Coherencia interna y externa y conformidad con los estándares comúnmente aceptados. Para evitar que el usuario se desoriente, todas las páginas de un sitio deben ser homogéneas estilísticamente y conceptualmente.
*   Cuidado en la prevención de los errores.
*   Reconocer mejor que recordar. El interfaz debe permitir una fácil comprensión de sus funciones en lugar de obligar el usuario a memorizar una serie de procedimientos.
*   Flexibilidad y eficiencia. El interfaz debe respetar también las necesidades del usuario experto poniendo a su disposición herramientas para un uso más avanzado del sitio.
*   Diseño minimalista para evitar tiempos de carga largos sin renunciar a calidades de tipo estético.
*   Proporcionar al usuario los medios para corregir los errores.
*   Proporcionar los instrumentos de ayuda y las instrucciones de uso del sitio.

Han _et al._ ([2001](#han01)) han realizado la, hasta ahora, más sistemática y completa propuesta de criterios de evaluación de la usabilidad. Su trabajo, enfocado hacia la evaluación de productos electrónicos de consumo, presenta 48 posibles dimensiones de la usabilidad, clasificadas en dos grupos: las relacionadas con el uso del producto (categorizados según el estadio cognitivo de procesamiento de la información implicado) y las relacionadas con la percepción del usuario respecto al producto. Su distribución es la siguiente:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 1: Criterios de evaluación de usabilidad de Han _et al._ ([2001](#han01))**</caption>

<tbody>

<tr>

<th colspan="2" valign="middle">

<div align="left">Performance dimensions</div>

</th>

</tr>

<tr>

<td width="29%">Perception/cognition</td>

<td>

<div align="left">Directness, Explicitness, Modelessness, Observability, Responsiveness, Simplicity.</div>

</td>

</tr>

<tr>

<td valign="middle">Learning/memorization</td>

<td>Consistency, Familiarity, Informativeness, Learnability, Memorability, Predictability.</td>

</tr>

<tr>

<td>Control/action</td>

<td>Accesibility, Adaptability, Controllability, Effectiveness, Efficiency, Error prevention, Flexibility, Helpfulness, Multithreading, Recoverability, Task conformance.</td>

</tr>

<tr>

<th colspan="2" valign="middle">

<div align="left">**Image/impression dimensions**</div>

</th>

</tr>

<tr>

<td>Basic sense</td>

<td>Shape, Color, Brightness, Texture, Translucency, Balance, Heavyness, Volume.</td>

</tr>

<tr>

<td>Description of image</td>

<td>Metaphoric design image, Elegance, Granularity, Harmoniousness, Luxuriousness, Magnificence, Neatness, Rigidity, Salience, Dynamicity.</td>

</tr>

<tr>

<td>Evaluative feeling</td>

<td>Acceptability, Comfort, Convenience, Reliability, Attractiveness, Preference, Satisfaction.</td>

</tr>

</tbody>

</table>

## Criterios para la evaluación de la usabilidad de recursos educativos

Basándose en los modelos de evaluación convencionales que acabamos de mencionar, Marzal, Colmenero y Morato ([2003](#marcol03)) realizaron una primera aproximación a la definición de criterios de usabilidad y alfabetización en información aplicables a recursos educativos:

*   Usabilidad: Auxiliaridad, asociatividad, comunicabilidad, navegación, organización, secuenciación, gráficos, texto, lenguaje y retroalimentación.
*   Alfabetización: Pedagogía, tipo de usuario, cantidad y calidad de información, actividades, interactividad y diseño del módulo.

Recientemente, Nokelainen ([2006](#nokelainen06)) ha realizado una revisión de los principales trabajos que han tratado la usabilidad de recursos educativos, destacando los trabajos de Reeves ([1994](#reeves94)), Squires y Preece ([1996](#squires96), [1999](#squires99)), Quinn ([1996](#quinn96)), Albion ([1999](#alb99)) y Horila, Nokelainen, Syvänen y Överlund ([2002](#horila02)). En estos trabajos, Nokelainen identifica cinco criterios communes, que atribuye a aspectos técnicos de los recursos: '_learner control_', '_possibility for cooperative or collaborative learning activities_', '_explicit learning goals_', '_authenticity of learning material_' y '_learner support (scaffolding)_'. El modelo desarrollado por Nokelainen, que posteriormente se traslada a un cuestionario, el PMLQ o _Pedagogically Meaningful Learning Questionnaire_, aborda el problema desde el punto de vista de la experiencia subjetiva de los estudiantes en su interacción directa con los contenidos educativos, y está compuesto por diez criterios, con sus respectivas subdimensiones, que los definen y delimitan:

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 2: Criterios de evaluación de usabilidad en recursos educativos de Nokelainen ([2006](#nokelainen06))**</caption>

<tbody>

<tr>

<td width="29%">**Criterios**</td>

<td>**Subdimensiones**</td>

</tr>

<tr>

<td valign="middle">1\. Learner control</td>

<td>Minimum memory load; meaningful enconding; Responsibility for learning; User control; Elaboration.</td>

</tr>

<tr>

<td>2\. Learner activity</td>

<td>Reflective thinking; Problem-based learning; Use of primary data sources; Immersion; Ownership; Primary data source (for PBL) (for teacher); Facilitative teacher (for teacher); Didactic teacher (for teacher); Individual/distance learning (for teacher).</td>

</tr>

<tr>

<td>3\. Cooperative/Collaborative learning</td>

<td>Support for conversation and dialogue; Group work; Asynchronous social navigation; Synchronous social navigation; Asynchronous social navigation monitoring (for teacher); Synchronous social navigation monitoring (for teacher); Tertiary courseware.</td>

</tr>

<tr>

<td>4\. Goal orientation</td>

<td>Explicit goals; Usefulness of goals; Focus on results; Focused goals; Monitor one's own studies (pedagogic feedback); Set one's own goals (for teacher)</td>

</tr>

<tr>

<td>5\. Applicability</td>

<td>Authentic material; Perceived usefulness; Learning by doing; Adequate material for the learners needs (human development); Pretesting and diagnostics; Prompting; Fading; Scaffolding; Meaningful encoding.</td>

</tr>

<tr>

<td>6\. Added value</td>

<td>Overall added value for learning; Effectiveness for learning; Added value of pictures; Added value of sounds; Added value of animations.</td>

</tr>

<tr>

<td>7\. Motivation</td>

<td>Intrinsic goal orientation; Extrinsic goal orientation; Meaningfulness of studies; Immersion.</td>

</tr>

<tr>

<td>8\. Valuation of previous knowledge</td>

<td>Prerequisites; Elaboration; Examples.</td>

</tr>

<tr>

<td>9\. Flexibility</td>

<td>Pretesting and diagnostics; Task decomposition; Repetitive tasks.</td>

</tr>

<tr>

<td>10\. Feedback</td>

<td>Encouraging feedback; Accurate feedback; Errorless learning.</td>

</tr>

</tbody>

</table>

## Modelo de evaluación de usabilidad en recursos educativos

A partir los premisas conceptuales anteriormente expuestas , se han formulado 23 criterios de evaluación agrupados en torno a tres categorías: Captación, vinculada al mecanismo cognitivo de la atención, Fidelización, vinculada a la percepción y Capacidad alfabetizadora, vinculada con la memoria.

La distribución de los 23 criterios y sus correspondiente subcriterios definidos para estas categorías es la siguiente:

### **A. CAPTACIÓN**

El recurso, en su dimensión sintáctica, es capaz de suscitar el interés del docente como instrumento para sostener el estímulo y motivar al educando de manera que éste pueda ejercitar adecuadamente la habilidad del aprendizaje en red. Es útil, por tanto, al docente para evaluar la idoneidad del recurso.

#### **A.1\. Captación selectiva**

El recurso se muestra técnicamente eficaz y fiable para enseñar por su:

> ##### A.1.a.** _Coherencia en el uso_**
> 
> Cuando el educando accede al recurso, identifica rápidamente cómo interactuar con él y anticipar cuál va a ser la respuesta de éste. Entre otros aspectos, comprendería:
> 
> > •  Que cada página que siga una secuencia debe mostrar su ubicación.
> > 
> > •  Cada explicación ha de disponer de una secuencia que permita la navegación al inicio, fin, siguiente y previo.
> > 
> > •  Que si existen enlaces en la secuencia a recursos externos, debe estar indicado.
> 
> ##### A.1.b.** _Coherencia estilística_**
> 
> Este concepto hace referencia al estilo y formas de presentación de los contenidos en los recursos. Entre otros aspectos, hace referencia a un diseño sencillo y claro y la utilización de colores estándar para enlaces visitados o no, homogéneos en el sitio.
> 
> ##### A.1.c. **_Fiabilidad_**
> 
> Implicaria la llamada de atencion al usuario sobre las posibles consecuencias que puedan producirse como resultado de sus acciones (salida del recurso, apertura un enlace en una nueva pagina, etc.).

#### A.2.**Captación distribuida**

El recurso diversifica sus potencialidades, en función del educando.

> ##### A.2.a. **_Multitarea_**
> 
> El recurso permite la realización de varias tareas al mismo tiempo (por ejemplo, oir una pieza musical al tiempo que se mira una colección de textos sobre esa pieza o imágenes de su época).
> 
> ##### A.2.b. **_Personalización interactiva_**
> 
> Entendemos flexibilidad en cuanto a las posibilidades técnicas de personalización que el recurso ofrece al usuario, en función de niveles de capacitación (agentes, cookies, etc.). El recurso puede así llegar a adelantarse a las necesidades del educando en base a perfiles de uso, con lo que su experiencia de aprendizaje puede verse enriquecida. Se trata de un concepto íntimamente ligado al de Flexibilidad, mas en ésta priman los contenidos (sostienen la atención a partir del creciente interés en los contenidos) mientras que en la Personalización interactiva prima el componente técnico (posibilidad de que el recurso trabaje por debajo, proponiendo realizar varias tareas que se pueden llevar a cabo a un tiempo).
> 
> ##### _A.2.c._ **_Interactividad._**
> 
> Se manifestaria a través de la presencia de listas de distribución, foros o la posibilidad de intercambiar aportaciones mediante contacto como e-mail, etc.

#### A.3\. **Captación sostenida**

El recurso se diversifica según los ritmos de aprendizaje propios de cada educando.

> ##### A.3.a. **_Velocidad de respuesta_**
> 
> Significaría que el recurso responde a la velocidad de navegación que imponga el educando, en funcion de su ritmo de aprendizaje. Si el recurso no responde en este sentido, el estudiante pierde el interés.
> 
> ##### A.3.b. **_Flexibilidad_**
> 
> Sería la personalización orientada a los contenidos, es decir, que el recurso ofrece al educando múltiples caminos y posibilidades de acceso a contenidos de diferente grado de especificidad y, por tanto, dificultad.
> 
> ##### A.3.c. **_Adecuación a las expectativas_**
> 
> Entendida como adaptacion al nivel cognitivo y lingüístico del educando.
> 
> ##### A.3.d. **_Estimulación_**
> 
> Conjunto de elementos (entre ellos los multimedia como imágenes, sonido, vídeo, etc.) que hacen que el recurso resulte atractivo al educando.

### B. **FIDELIZACIÓN**

Este concepto hace referencia a que el recurso, por su diseño semiótico, logra ser asimilado como un recurso para aprender. Es útil al educando para identificar y seleccionar los materiales que le son más útiles.

> #### B.1\. **Amigabilidad**
> 
> Conjunto de características que hacen que la utilización del recurso le resulte fácil y agradable al educando (que sea fácil de usar, claro, legible, etc.) y que por ello el alumno lo prefiera frente a otros similares, lo que le invita a regresar al recurso o recomendarlo a otros educandos.
> 
> #### B.2\. **Metáfora de signos**
> 
> Entendemos por tal aspectos como la correspondencia de los iconos con el contenido a que dan acceso o la adecuacion de las imagenes utilizadas.
> 
> #### B.3\. **Accesibilidad**
> 
> Adaptación a características especiales del educando (discapacidades, etc.).
> 
> #### B.4\. **Navegabilidad**
> 
> Comprende los siguientes aspectos:
> 
> > •  Opciones de navegación claras y consistentes.
> > 
> > •  Enlaces y URL no ambiguos, sino claros y específicos.
> > 
> > •  Enlaces para retroceder.
> > 
> > •  Disponibilidad de enlaces a la página principal y/o resto de páginas desde cualquier punto del recurso .
> > 
> > •  Necesidad de descargar software adicional para poder visualizar correctamente los contenidos.
> 
> #### B.5\. **Organización**
> 
> Comprende los siguientes aspectos:
> 
> > •  Se da una estructura jerárquica de los conceptos antes de argumentar o crear actividades con ellos.
> > 
> > •  Existencia de un mapa del sitio.
> > 
> > •  Se muestran relaciones de causa-efecto.
> > 
> > •  El contenido está en un sitio destacado.
> > 
> > •  Explicación del concepto principal en un lugar destacado, carece de información irrelevante.

### C. **CAPACIDAD ALFABETIZADORA**

> #### **C.1\. Memoria procedimental.**
> 
> > ##### C.1.a. **Contextualidad**
> > 
> > Integración del entorno de interés del educando, en función del tópico del recurso. Se manifiesta a través de la existencia de informacion que trate de 'conectar' con los conocimientos previos del educando sobre el tema o enlaces que complementen aquellos y le permitan poder acceder fácilmente a la asimilación de los contenidos propios de recurso.
> > 
> > ##### **C.1.b. Autoridad**
> > 
> > Posibilidad que ofrece el recurso de identificar al autor o autores de la información que en él se alberga. Esto puede dar una idea preliminar sobre la calidad de un recurso, aunque también otros muchos aspectos, como la fundamentación o justificación (identificación de las fuentes o referencia a otras informaciones para contrastar su veracidad y/o validez, etc.).
> > 
> > ##### **C.1.c. Efectividad didáctica**
> > 
> > Este concepto se relaciona con la posibilidad de visualizar el camino o recorrido que el educando ha seguido por el sitio (por medio, por ejemplo de las denominadas "huellas"), de manera que no pueda verse desorientado.
> > 
> > ##### **C.1.d. Auxiliaridad**
> > 
> > Este aspecto se manifiesta en la presencia de normas, instrucciones u objetivos didácticos con aplicacion de una planificación y un modelo pedagógico.
> 
> #### **C.2\. Memoria declarativa.**
> 
> > ##### C.2.a. **Granularidad**
> > 
> > Significaría que cuando un usuario/educando está visualizando el recurso, éste le ofrezca información de distintos tipos y niveles de especificidad: imágenes, texto, video, iconos, etc.
> > 
> > ##### C.2.b. **Eficacia**
> > 
> > Este aspecto se manifiesta en la existencia de interactividad con tutoración, actividades o lo que se ha venido a llamar edutenimiento.
> > 
> > ##### C.2.c. **Eficiencia**
> > 
> > Se aprecia en la determinación del esfuerzo cognitivo y la comunicación eficiente.
> > 
> > ##### C.2.d. **Dinamicidad**
> > 
> > Entendida como la actualización de los contenidos del recurso.

## Prueba preliminar del modelo en la comunidad escolar

Con objeto de comprobar la validez del modelo presentado así como sugerir un método de aplicación, se realizó un ensayo del mismo, para lo cual se desarrolló un cuestionario compuesto por 41 preguntas ([anexo I](#anexo)), cuya correspondencia con los distintos criterios del modelo se presenta en la tabla 4\. La experiencia se desarrolló gracias a la colaboración voluntaria de seis profesores de Primaria y Secundaria de seis centros de la Comunidad de Madrid y un total de 364 alumnos de entre 10 y 13 años. Los principales datos estadísticos se presentan en la siguiente tabla:

<table width="34%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 3: Principales datos estadísticos de la muestra del estudio**</caption>

<tbody>

<tr>

<td width="28%">**Curso**</td>

<td width="24%">**Total**</td>

<td width="24%">**Mujeres**</td>

<td width="24%">**Varones**</td>

</tr>

<tr>

<td valign="middle">4º Primaria</td>

<td>

<div align="center">100</div>

</td>

<td>

<div align="center">53</div>

</td>

<td>

<div align="center">47</div>

</td>

</tr>

<tr>

<td>5º Primaria</td>

<td>

<div align="center">109</div>

</td>

<td>

<div align="center">56</div>

</td>

<td>

<div align="center">53</div>

</td>

</tr>

<tr>

<td>6º Primaria</td>

<td>

<div align="center">103</div>

</td>

<td>

<div align="center">55</div>

</td>

<td>

<div align="center">48</div>

</td>

</tr>

<tr>

<td>Total Primaria</td>

<td>

<div align="center">312</div>

</td>

<td>

<div align="center">164</div>

</td>

<td>

<div align="center">148</div>

</td>

</tr>

<tr>

<td>Secundaria</td>

<td>

<div align="center">52</div>

</td>

<td>

<div align="center">26</div>

</td>

<td>

<div align="center">26</div>

</td>

</tr>

<tr>

<td>**Total**</td>

<td>

<div align="center">364</div>

</td>

<td>

<div align="center">190</div>

</td>

<td>

<div align="center">174</div>

</td>

</tr>

</tbody>

</table>

La evaluación se desarrolló en dos partes: en primer lugar, se requirió a los estudiantes la cumplimentación de un cuestionario para su caracterización estadística y de competencias básicas en el acceso, manejo y uso de la información (competencias ALFIN), cuyo modelo se presenta en Marzal y Calzada-Prado ([2003](#marcal03)). En segundo lugar, se presentó a los alumnos dos recursos educativos en red: _[Guido contra el señor de las sombras](http://www.educathyssen.org/pequenothyssen/aventuras2.html)_, del Museo Thyssen Bornemisza ([http://www.educathyssen.org/pequenothyssen/aventuras2.html](http://www.educathyssen.org/pequenothyssen/aventuras2.html)) y [Experimentar](http://www.experimentar.gov.ar/) ([http://www.experimentar.gov.ar/](http://www.experimentar.gov.ar/)), del Ministerio de Educación, Ciencia y Tecnología argentino. Una vez que los estudiantes hubieron interactuado con los recursos, se les pidió que cumplimentaran el cuestionario referido a la usabilidad de los recursos.

Experimentar (a partir de aquí, recurso 1), es portal educativo que contiene diversos recursos educativos, en su mayoría actividades infantiles de experimentación interactivas.

<div align="center">![Figura 1\. Recurso 1: portal de recursos educativos 'Experimentar'](p387fig1.jpg)</div>

<div align="center">  
**Figura 1\. Recurso 1: portal de recursos educativos 'Experimentar'**</div>

_Guido contra el señor de las sombras_ , por su parte, (a partir de aquí, recurso 2) es un recurso educativo en formato flash que presenta, en forma de juego visualmente atractivo para niños, una actividad cuyo objetivo didáctico es el reconocimiento de obras y autores de la colección del Museo Thyssen Bornemisza.

<div align="center">![Figura 2\. Recurso educativo 2: 'Guido contra el señor de las sombras'](p387fig2.jpg)</div>

<div align="center">  
**Figura 2\. Recurso educativo 2: 'Guido contra el señor de las sombras'**</div>

El análisis de los resultados obtenidos en esta prueba preliminar del modelo indican lo siguiente:

En cuanto a la **captación** como instrumento para sostener el estímulo y motivar al educando, ambos recursos parecen, en líneas generales, adecuados para que los estudiantes puedan ejercitar convenientemente el aprendizaje en red. En cuanto a lo que denominamos **captación selectiva**, referida a si los recursos se muestran técnicamente eficaces y fiables para enseñar, los estudiantes han apreciado en ambos gran coherencia en el uso (véase cuadro presentado a continuación, categoría A.1.a), así como coherencia estilística (A.1.b), si bien han acusado cierta falta de fiabilidad en su capacidad para llamar la atencion al usuario sobre las posibles consecuencias que puedan producirse como resultado de sus acciones (A.1.c) . Han observado igualmente que, en general, la **captación distribuida** en cuanto diversificación de sus potencialidades en función del educando es aceptable en ambos recursos, ya que ambos soportan la realización de varias tareas al mismo tiempo (A.2.a) y son claras sus posibilidades técnicas de personalización interactiva (A.2.b), si bien sólo en el caso del recurso 1 han encontrado posibilidades de interacción virtual (A.2.c). Respecto a la **captación sostenida** o diversificación de los recursos según los ritmos de aprendizaje propios de cada educando, los estudiantes han observado que los recursos analizados son adecuados, ya que por lo general cumplen con los criterios establecidos. Así, han apreciado la rápida respuesta de los recursos (A.3.a), su flexibilidad en cuanto navegación a través de distintos niveles de contenido (A.3.b) aunque no en cuanto a la disponibilidad de versiones multilingües (A.3.b), ni a la adecuación a sus expectativas en cuanto adaptacion a su nivel cognitivo y lingüístico , ya que les ha resultado difícil el lenguaje empleado (A.3.c) y el contenido (A.3.c) aun tratándose de recursos dirigidos a público de su edad, aunque sí respecto a la estimulación por sus aspectos estéticos (A.3.d.p6). Respecto a la **fidelización** como asimilación del recurso como recurso educativo, encontramos que ambos recursos son potencialmente válidos. En efecto, los estudiantes han encontrado que los recursos son amigables e incluso manifiestan mayoritariamente que volverían a visitar los recursos y los recomendarían a otros estudiantes (B.1). La **metáfora de signos** ha resultado adecuada para la mayoría de estudiantes (B.2); no así en el caso de la accesibilidad (B.3). La valoración de la **navegabilidad** de los dos recursos (B.4) ha sido bastante uniforme, a pesar de sus propias características intrínsecas, al tratarse de un portal de actividades y una actividad concreta. En ninguno han localizado herramientas de búsqueda internas o publicidad que impida visualizar correctamente los contenidos; han observado que no es necesaria la instalación de software adicional para su correcto uso (p04), que ambos disponen de enlaces para retroceder (p07) o acceder a la página principal y/o al resto de páginas desde cualquier punto (p14), que todos los enlaces que han intentado seguir han funcionado y que ninguno de los recursos permite informar a su administrador de los enlaces 'rotos' (p28). En cuanto a la **organización** de contenidos en los recursos (B.5), los estudiantes han observado que ambos recursos disponen de mapa del sitio, que son pertinentes sus cabeceras y que, en general, la organización de los contenidos resulta clara. En cuanto a **capacidad alfabetizadora** , ambos recursos se muestran potencialmente aptos. En lo referido a la **memoria procedimental** (C.1.a), los estudiantes han manifestado que disponían de conocimientos previos sobre el contenido de las actividades del recurso 1, pero no sobre el del 2, y han observado que el recurso 1 remite a otros recursos temáticamente complementarios, a diferencia del recurso 2\. Respecto a la identificación de **autoridad** (C.1.b), los estudiantes han encontrado posible identificar a los respectivos autores de los recursos y han percibido seguridad en la calidad de sus contenidos. La valoración de la **efectividad didáctica** de los recursos (C.1.c) difiere: en el caso del recurso 1 se percibe como mayor que en el caso del recurso 2\. La **auxiliaridad** en cuanto presencia de normas, instrucciones u objetivos didácticos se percibe claramente en ambos recursos. En lo referido a la **memoria declarativa** (C.2), los recursos se perciben como de un gran nivel de granularidad (C.2.a), y se perciben **eficaces** (C.2.b) por disponer de mecanismos para facilitar el control de acciones o su planteamiento como juego o entretenimiento. Asimismo, los recursos seleccionados son percibidos por los estudiantes como eficientes (C.2.c) por plantear actividades de diferente complejidad en función de los resultados obtenidos aunque observan falta de dinamicidad (C.2.d) por carecer de información referente a la fecha de actualización de sus contenidos.

<table width="90%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Tabla 4: Resumen de resultados de la valoración de usabilidad de los recursos educativos**</caption>

<tbody>

<tr>

<th>Categoría/Pregunta</th>

<th colspan="3">Experimentar (recurso 1)</th>

<th colspan="3">Guido (recurso 2)</th>

</tr>

<tr>

<td> </td>

<th>Sí (%)</th>

<th width="13%">No (%)</th>

<th>Ns/Nc</th>

<th width="14%">Sí (%)</th>

<th>No (%)</th>

<th width="15%">Ns/Nc</th>

</tr>

<tr>

<td>A.1.a (p36)</td>

<td align="center">**278** (74.5)</td>

<td align="center">71 (19\. 1)</td>

<td align="center">24 (7.54)</td>

<td align="center">**316** (84\. 7)</td>

<td align="center">36 (9.6)</td>

<td align="center">36 (9.6)</td>

</tr>

<tr>

<td>A.1.b (p15)</td>

<td align="center">**273** (73.2)</td>

<td align="center">63 (16.9)</td>

<td align="center">38 (10.2)</td>

<td align="center">**296** (79.3)</td>

<td align="center">42 (11.3)</td>

<td align="center">35 (9.4)</td>

</tr>

<tr>

<td>A.1.b (p17)</td>

<td align="center">**200** (53.6)</td>

<td align="center">91 (24.4)</td>

<td align="center">82 (22)</td>

<td align="center">**204** (54.7)</td>

<td align="center">97 (26)</td>

<td align="center">72 (19.3)</td>

</tr>

<tr>

<td>A.1.c (p26)</td>

<td align="center">134 (36)</td>

<td align="center">**153** (41)</td>

<td align="center">86 (23)</td>

<td align="center">142 (38.1)</td>

<td align="center">**162** (43.4)</td>

<td align="center">69 (18.5)</td>

</tr>

<tr>

<td>A.2.a (p29)</td>

<td align="center">**182** (48.8)</td>

<td align="center">95 (25.5)</td>

<td align="center">96 (25.7)</td>

<td align="center">**253** (67.8)</td>

<td align="center">81 (21.7)</td>

<td align="center">39 (10.4)</td>

</tr>

<tr>

<td>A.2.b (p24)</td>

<td align="center">**245** (65.7)</td>

<td align="center">56 (15)</td>

<td align="center">72 (19.3)</td>

<td align="center">**193** (51.7)</td>

<td align="center">79 (21.2)</td>

<td align="center">101 (27.1)</td>

</tr>

<tr>

<td>A.2.c (p11)</td>

<td align="center">**252** (67.6)</td>

<td align="center">54 (14.5)</td>

<td align="center">67 (18)</td>

<td align="center">88 (23.6)</td>

<td align="center">**218** (58.4)</td>

<td align="center">67 (18)</td>

</tr>

<tr>

<td>A.2.c (p12)</td>

<td align="center">**300** (80.4)</td>

<td align="center">37 (9.9)</td>

<td align="center">36 (9.6)</td>

<td align="center">46 (12.3)</td>

<td align="center">**265** (71)</td>

<td align="center">62 (16.6)</td>

</tr>

<tr>

<td>A.3.a (p25)</td>

<td align="center">**244** (65.4)</td>

<td align="center">79 (21.2)</td>

<td align="center">50 (13.4)</td>

<td align="center">**271** (73.6)</td>

<td align="center">67 (18)</td>

<td align="center">35 (9.4)</td>

</tr>

<tr>

<td>A.3.b (p10)</td>

<td align="center">69 (18.5)</td>

<td align="center">**220** (59)</td>

<td align="center">84 (22.5)</td>

<td align="center">105 (33)</td>

<td align="center">**212** (49.4)</td>

<td align="center">56 (15)</td>

</tr>

<tr>

<td>A.3.b (p22)</td>

<td align="center">**236** (63.3)</td>

<td align="center">51 (13.7)</td>

<td align="center">86 (23)</td>

<td align="center">**202** (54.1)</td>

<td align="center">108 (29)</td>

<td align="center">62 (16.6)</td>

</tr>

<tr>

<td>A.3.c (p09)</td>

<td align="center">118 (31.6)</td>

<td align="center">**207** (55.5)</td>

<td align="center">48 (12.9)</td>

<td align="center">79 (21.2)</td>

<td align="center">**274** (73.4)</td>

<td align="center">20 (5.4)</td>

</tr>

<tr>

<td>A.3.c (p37)</td>

<td align="center">102 (27.3)</td>

<td align="center">**249** (66.7)</td>

<td align="center">22 (5.9)</td>

<td align="center">112 (30)</td>

<td align="center">**241** (64.6)</td>

<td align="center">20 (5.4)</td>

</tr>

<tr>

<td>A.3.d (p06)</td>

<td align="center">**283** (75.9)</td>

<td align="center">55 (14.7)</td>

<td align="center">35 (9.4)</td>

<td align="center">**317** (85)</td>

<td align="center">45 (12.1)</td>

<td align="center">11 (2.9)</td>

</tr>

<tr>

<td>B.1 (p08)</td>

<td align="center">**290** (77.7)</td>

<td align="center">51 (13.7)</td>

<td align="center">32 (8.6)</td>

<td align="center">**314** (84.2)</td>

<td align="center">45 (12.1)</td>

<td align="center">14 (3.7)</td>

</tr>

<tr>

<td>B.1 (p40)</td>

<td align="center">**288** (77.2)</td>

<td align="center">59 (15.8)</td>

<td align="center">26 (7)</td>

<td align="center">**312** (83.6)</td>

<td align="center">39 (10.4)</td>

<td align="center">22 (5.9)</td>

</tr>

<tr>

<td>B.1 (p41)</td>

<td align="center">**320** (85.8)</td>

<td align="center">33 (8.8)</td>

<td align="center">20 (5.4)</td>

<td align="center">**320** (85.8)</td>

<td align="center">34 (9.1)</td>

<td align="center">19 (5.1)</td>

</tr>

<tr>

<td>B.2 (p16)</td>

<td align="center">**245** (65.7)</td>

<td align="center">60 (16.1)</td>

<td align="center">68 (18.2)</td>

<td align="center">**274** (73.4)</td>

<td align="center">41 (11)</td>

<td align="center">58 (15.5)</td>

</tr>

<tr>

<td>B.2 (p30)</td>

<td align="center">**283** (81.13)</td>

<td align="center">55 (14.7)</td>

<td align="center">35 (9.4)</td>

<td align="center">**311** (83.4)</td>

<td align="center">36 (9.6)</td>

<td align="center">26 (7)</td>

</tr>

<tr>

<td>B.3 (p35)</td>

<td align="center">80 (21.4)</td>

<td align="center">**171** (45.8)</td>

<td align="center">122 (32.7)</td>

<td align="center">107 (28.7)</td>

<td align="center">**162** (43.4)</td>

<td align="center">104 (27.9)</td>

</tr>

<tr>

<td>B.4 (p04)</td>

<td align="center">145 (38.9)</td>

<td align="center">**156** (37.11)</td>

<td align="center">72 (19.3)</td>

<td align="center">140 (37.5)</td>

<td align="center">**174** (46.6)</td>

<td align="center">59 (15.8)</td>

</tr>

<tr>

<td>B.4 (p07)</td>

<td align="center">86 (23)</td>

<td align="center">**246** (65.9)</td>

<td align="center">38 (10.2)</td>

<td align="center">90 (24.1)</td>

<td align="center">**263** (70.5)</td>

<td align="center">20 (5.4)</td>

</tr>

<tr>

<td>B.4 (p14)</td>

<td align="center">68 (18.2)</td>

<td align="center">**234** (62.7)</td>

<td align="center">71 (19.03)</td>

<td align="center">88 (23.6)</td>

<td align="center">**250** (67)</td>

<td align="center">35 (9.4)</td>

</tr>

<tr>

<td>B.4 (p18)</td>

<td align="center">**189** (50.7)</td>

<td align="center">124 (33.24)</td>

<td align="center">60 (16.1)</td>

<td align="center">133 (35.6)</td>

<td align="center">**186** (49.9)</td>

<td align="center">54 (15.41)</td>

</tr>

<tr>

<td>B.4 (p19)</td>

<td align="center">**206** (55.2)</td>

<td align="center">97 (26)</td>

<td align="center">70 (18.8)</td>

<td align="center">137 (36.7)</td>

<td align="center">**163** (43.7)</td>

<td align="center">73 (19.6)</td>

</tr>

<tr>

<td>B.4 (p27)</td>

<td align="center">**267** (71.6)</td>

<td align="center">79 (21.2)</td>

<td align="center">27 (7.2)</td>

<td align="center">**271** (72.6)</td>

<td align="center">80 (21.4)</td>

<td align="center">22 (5.9)</td>

</tr>

<tr>

<td>B.4 (p28)</td>

<td align="center">117 (31.4)</td>

<td align="center">**129** (34.6)</td>

<td align="center">127 (34)</td>

<td align="center">96 (25.7)</td>

<td align="center">**153** (41)</td>

<td align="center">124 (33.2)</td>

</tr>

<tr>

<td>B.5 (p03)</td>

<td align="center">**271** (72.6)</td>

<td align="center">59 (15.8)</td>

<td align="center">43 (11.5)</td>

<td align="center">**148** (39.7)</td>

<td align="center">172 (46.1)</td>

<td align="center">53 (14.2)</td>

</tr>

<tr>

<td>B.5 (p05)</td>

<td align="center">**230** (61.6)</td>

<td align="center">82 (22)</td>

<td align="center">61 (16.3)</td>

<td align="center">**214** (57.4)</td>

<td align="center">92 (24.7)</td>

<td align="center">67 (18)</td>

</tr>

<tr>

<td>B.5 (p21)</td>

<td align="center">**272** (73)</td>

<td align="center">63 (16.9)</td>

<td align="center">38 (10.2)</td>

<td align="center">**319** (85.5)</td>

<td align="center">28 (7.5)</td>

<td align="center">26 (7)</td>

</tr>

<tr>

<td>C.1.a (p23)</td>

<td align="center">**145** (38.9)</td>

<td align="center">101 (27.1)</td>

<td align="center">127 (34)</td>

<td align="center">104 (27.9)</td>

<td align="center">179 (48)</td>

<td align="center">90 (24.1)</td>

</tr>

<tr>

<td>C.1.a (p38)</td>

<td align="center">**184** (49.3)</td>

<td align="center">138 (37)</td>

<td align="center">51 (13.7)</td>

<td align="center">146 (39.1)</td>

<td align="center">**193** (51.7)</td>

<td align="center">33 (8.8)</td>

</tr>

<tr>

<td>C.1.b (p02)</td>

<td align="center">**175** (47)</td>

<td align="center">136 (36.5)</td>

<td align="center">62 (16.6)</td>

<td align="center">**167** (44.8)</td>

<td align="center">140 (37.5)</td>

<td align="center">66 (17.7)</td>

</tr>

<tr>

<td>C.1.b (p39)</td>

<td align="center">**243** (65.1)</td>

<td align="center">51 (13.7)</td>

<td align="center">78 (21.2)</td>

<td align="center">**280** (75.1)</td>

<td align="center">30 (8)</td>

<td align="center">63 (16.9)</td>

</tr>

<tr>

<td>C.1.c (p20)</td>

<td align="center">**157** (42.1)</td>

<td align="center">118 (31.6)</td>

<td align="center">98 (26.3)</td>

<td align="center">71 (19)</td>

<td align="center">**229** (61.4)</td>

<td align="center">73 (19.6)</td>

</tr>

<tr>

<td>C.1.d (p01)</td>

<td align="center">**259** (69.4)</td>

<td align="center">39 (11.01)</td>

<td align="center">75 (20.1)</td>

<td align="center">**265** (71)</td>

<td align="center">28 (7.5)</td>

<td align="center">80 (21.4)</td>

</tr>

<tr>

<td>C.2.a (p31)</td>

<td align="center">**203** (54.4)</td>

<td align="center">83 (22.2)</td>

<td align="center">87 (23.3)</td>

<td align="center">**253** (67.8)</td>

<td align="center">55 (14.7)</td>

<td align="center">65 (17.4)</td>

</tr>

<tr>

<td>C.2.b (p33)</td>

<td align="center">**253** (67.8)</td>

<td align="center">78 (21)</td>

<td align="center">42 (11.3)</td>

<td align="center">**320** (85.8)</td>

<td align="center">34 (9.1)</td>

<td align="center">19 (5.1)</td>

</tr>

<tr>

<td>C.2.b (p34)</td>

<td align="center">**143** (38.3)</td>

<td align="center">121 (32.4)</td>

<td align="center">109 (29.2)</td>

<td align="center">**178** (47.7)</td>

<td align="center">99 (26.5)</td>

<td align="center">96 (25.7)</td>

</tr>

<tr>

<td>C.2.c (p32)</td>

<td align="center">**196** (52.5)</td>

<td align="center">81 (21.7)</td>

<td align="center">96 (25.7)</td>

<td align="center">**160** (42.9)</td>

<td align="center">116 (31.1)</td>

<td align="center">97 (26)</td>

</tr>

<tr>

<td>C.2.d (p13)</td>

<td align="center">123 (33)</td>

<td align="center">**139** (37.3)</td>

<td align="center">111 (29.7)</td>

<td align="center">58 (15.5)</td>

<td align="center">**249** (66.7)</td>

<td align="center">66 (17.7)</td>

</tr>

</tbody>

</table>

## Conclusiones y desarrollos futuros

En conclusión, el desarrollo metodológico de este trabajo y sus resultados preliminares, referidos a la enunciación de categorías evaluativas para un modelo de indicadores, demuestra que un análisis evaluativo de la usabilidad de los recursos informativos con intención educativa no puede divergir de la competencia del usuario en instrumentos de tecnologías de la información y comunicación: la Captación y Fidelización derivan necesariamente a la Capacidad Alfabetizadora del recurso. Esta comprobación debe tener dos desarrollos futuros: de un lado, la relevancia de la interactividad de los recursos, no sólo para optimizar un aprendizaje, sino para avanzar en el análisis de la predictibilidad del consumo de información por los usuarios en los sistemas de información, en tanto que base para unas eficaces estrategias didácticas en la gestión de contenidos; de otro lado, dirigir el carácter de los indicadores no sólo hacia aspectos cuantitativos, sino sobre todo hacia aspectos cualitativos para una educación por competencias, de modo que la usabilidad sea considerada desde la óptica de la ALFIN.

Este trabajo, por lo demás, intenta presentar una reflexión sobre unas vías de investigación que, para la investigación en _alfabetización en información,_ parecen relevantes. El esquema de categorías propuesto procura apuntar hacia una proyección que les permita evolucionar hacia indicadores, esto es, podría plasmarse en un Modelo de Instrucción para **bibliotecas digitales educativas,** bien universitarias en proceso de transformación a CRAI (Centro de Recursos para el Aprendizaje y la Investigación ), bien a escolares en proceso de transformación a CREA (Centros de Recursos para la Enseñanza y Aprendizaje), con el objetivo de transformar radicalmente el concepto y aplicación de la formación de usuarios en un modelo de formación en ALFIN. El modelo de instrucción ALFIN, tanto en CRAI como en CREA, precisa en efecto de una programación y un desarrollo de acciones y actividades concretas, para las que el esquema de categorías descrito en este trabajo podría ser un preliminar muy útil por parte de los bibliotecarios, con competencias formativas en sus bibliotecas.

En este mismo sentido educativo, en el entorno de bibliotecas digitales educativas, parece sumamente importante dirigir la atención hacia los objetos de _aprendizaje_ , con sus caracteres y propiedades específicos. Es evidente que es un nuevo tipo documental y que precisará sus propios criterios de 'desarrollo de colección'. En efecto, los objetos de aprendizaje deberán ser sometidos a unos criterios de selección y calidad, para los que será muy conveniente generar indicadores, derivados desde iniciativas como la que aquí se presenta, con el apoyo de los diseños evaluativos que apuntan las guías del Constructivismo, tan apreciada por las actuales corrientes de innovación docente, implementadas por las aportaciones del más reciente Cognitivismo, tan cercano a la visualización y asociatividad propias de los recursos informativos en red.

Una última reflexión, que sugieren los resultados de este trabajo, es que las categorías evaluativas apuntadas pueden ser inspiración para un seguimiento eficaz de los progresos del usuario-educando en la lectura digital. Es una lectura que reclama una diferente competencia lectora, muy importante porque determina la asimilación de los conceptos descritos y su conversión en conocimiento. La lectura digital implica unas habilidades y destrezas cuyo logro debe ser plasmado en una calificación objetiva a través de indicadores evaluativos de competencia lectora, como los que aquí se apuntan.

## References

*   <a name="alb99" id="alb99"></a>Albion, P. (1999). [Heuristic evaluation of educational multimedia: From theory to practice](http://www.usq.edu.au/users/albion/papers/ascilite99.html). _16th Annual conference of the Australasian Society for Computers in Learning in Tertiary Education_. Retrieved 23 May 2007, from http://www.usq.edu.au/users/albion/papers/ascilite99.html
*   <a name="anderson83" id="anderson83"></a>Anderson, J.R. (1983) _The architecture of cognition_. Cambridge: Harvard University Press.
*   <a name="atchinson91" id="atchinson91"></a>Atkinson, R.C.; Shiffrin, R. M. (1971) The control of short-term memory. _Scientific American_, **August**, 82-90.
*   <a name="berlyne60" id="berlyne60"></a>Berlyne, D.E. (1960). _Conflict, arousal, and curiosity_. Nueva York: McGraw-Hill.
*   <a name="bernhard02" id="bernhard02"></a>Bernhard, P. (2002). La formación en el uso de la información: una ventaja en la enseñanza superior. Situación actual. _Anales de Documentación_, **5**, 409-435.
*   <a name="boyatzis82" id="boyatzis82"></a>Boyatzis, R. (1982). _The competent manager: a model for effective performance_. Nueva York: Wiley.
*   <a name="bunk94" id="bunk94"></a>Bunk, G. P. (1994). La transmisión de competencias en la formación y perfeccionamiento profesionales en la RFA. _Revista Europea de Formación Profesional,_ **1**, 8-14.
*   <a name="castell00" id="castell00"></a>Castells, M. (2000). _La Era de la información_. Madrid: Alianza Editorial, **1**, 359-408.
*   <a name="cato01" id="cato01"></a>Cato, J. (2001). _User-centered web design_. Harlow: Addison-Wesley.
*   <a name="cheng85" id="cheng85"></a>Cheng, P.W. (1985). Restructuring versus automaticity: Alternative accounts of skill acquisition. _Psychological Review_, **92**, 414-42.
*   <a name="colas05" id="colas05"></a>Colás Bravo, M. (2005). La formación universitaria en base a competencias. In T. Colás Bravo & J. De Pablos Pons. (Eds.), _La Universidad en la Unión Europea_. Málaga: Imagraf.
*   <a name="craik75" id="craik75"></a>Craik, F. I. M.; Tulving, E. (1975). Depth of processing and the retention of words in episodic memory. _Journal of Experimental Psychology: General_, **104**, 268-294.
*   <a name="delclaux82" id="delclaux82"></a>Delclaux, I. y Seoane, J. (1982). _Psicología cognitiva y procesamiento de la información_. Madrid: Pirámide.
*   <a name="ertmer93" id="ertmer93"></a>Ertmer, P.A.; Newby, T.J. (1993). Behaviorism, Cognitivism, Constructivism: Comparing Critical Features from an Instructional Design Perspective. _Performance Improvement Quarterly_, **6** (4), 50-72.
*   <a name="gilbert92" id="gilbert92"></a>Gilbert, P.; Parlier, M. (1992). La compétence: du "mot valise" au concept opératoire. _Actualité de la formation permanente_, **116**, 14-18.
*   <a name="han01" id="han01"></a>Han, S. _et al._ (2001). Usability of consumer electronic products. _International Journal of Industrial Ergonomics_, **28** (3-4), 143-151.
*   <a name="hayes85" id="hayes85"></a>Hayes, R.H. (1985). Strategic Planning – Forward in Reverse. _Harvard Business Review_, November-December, 111-119.
*   <a name="horila02" id="horila02"></a>Horila, M.; Nokelainen, P.; Syvänen, A.; Överlund, J. (2002). _Criteria for the pedagogical usability, version 1.0_. Hämeenlinna, Finland: Häme Polytechnic and University of Tampere.
*   <a name="ingwer92" id="ingwer92"></a>Ingwersen, P. (1992). _Information retrieval interaction_. London: Taylor Graham.
*   <a name="iso98" id="iso98"></a>International Standards Organization (1998). _Ergonomic requirements for visual display terminals ISO/IEC 9241_.
*   <a name="leboterf00" id="leboterf00"></a>Le Boterf, G. (2000). _Construire les competentes individuelles et collectives_. París: Editions d’Organisation.
*   <a name="levy03" id="levy03"></a>Lévy-Levoyer, C. (2003). _Gestión de las competencias_. Barcelona: Gestión 2000.
*   <a name="marbach99" id="marbach99"></a>Marbach, V. (1999). _Évaluer et rémunérer les competentes_. París: Editions d’Organisation.
*   <a name="marq02" id="marq02"></a>Marqués Graells, P. (2002). _[Información y Conocimiento](http://dewey.uab.es/pmarques/infocon.htm)_. Retrieved 20 May, 2007, from http://dewey.uab.es/pmarques/infocon.htm
*   <a name="martinez03" id="martinez03"></a>Martínez, D.; Martí, R. (2003). [La factoría de recursos docentes](http://biblioteca.uam.es/paginas/palma.html). _Los Centros de Recursos del Aprendizaje y la Investigación en los procesos de innovación docente: Jornadas Rebiun_. Retrieved 31 March, 2006, from http://biblioteca.uam.es/paginas/palma.html
*   <a name="marcal03" id="marcal03"></a>Marzal, M.A.; Calzada-Prado, F.J. (2003). [Un análisis de necesidades y hábitos informativos de estudiantes universitarios en Internet](http://www.uem.es/binaria/in.html). _Binaria_, **3**. Retrieved 10 May, 2007, from http://www.uem.es/binaria/in.html
*   <a name="marcol03" id="marcol03"></a>Marzal, M.A.; Colmenero, M.J.; Morato, J. (2003). Selección de recursos didácticos en red: accesibilidad y usabilidad como elementos de un sistema de evaluación para la Educación. _Segunda Conferencia Iberoamericana en Sistemas, Cibernética e Informática, CISCI 2003_, 31 July- 2 August, Orlando, Florida.
*   <a name="neisser67" id="neisser67"></a>Neisser, U. (1967). _Cognitive Psychology_. New York: Appleton-Century-Crofts.
*   <a name="nielsen93" id="nielsen93"></a>Nielsen, J. (1993). _Usability engineering_. New York: Academic Press.
*   <a name="nielsen98" id="nielsen98"></a>Nielsen, J.; Morkes, J., (1998). _[Applying Writing Guidelines to Web Pages](http://www.useit.com/papers/webwriting/rewriting.html)_. Retrieved 23 May, 2007, From http://www.useit.com/papers/webwriting/rewriting.html
*   <a name="nokelainen06" id="nokelainen06"></a>Nokelainen, P. (2006). An empirical assessment of pedagogical usability criteria for digital learning material with elementary school students. _Educational Technology & Society_, **9** (2), 178-197.
*   <a name="norman80" id="norman80"></a>Norman, D.; Shallice, T. (1980). Attention to action: Willed and automatic control of behaviour. In: R. Davidson, G. E. Schwartz & D. Shapiro. _Consciousness and self regulation_. New York: Plenum times, 1-15.
*   <a name="onrubia05" id="onrubia05"></a>Onrubia, J. (2005). [Aprender y enseñar en entornos virtuales: actividad conjunta, ayuda pedagógica y construcción del conocimiento](http://www.um.es/ead/red/M2/). _Revista de Educación a Distancia_, **2**. Retrieved 22 May, 2007, from http://www.um.es/ead/red/M2/
*   <a name="perez01" id="perez01"></a>Pérez, T.A.; Gutiérrez, J.; González, A.; Vadillo, J.A. (2001). Hipermedia, adaptación, constructivismo e instructivismo. _Inteligencia Artificial: Revista Iberoamericana de Inteligencia Artificial_, **12**, 29-38.
*   <a name="postman48" id="postman48"></a>Postman, L.; Bruner, J.S.; McGinnies, E. (1948). Personal values as selective factors in perception. _Journal of abnormal and social psycologogy_, **43**, 142-154.
*   <a name="posner84" id="posner84"></a>Posner, M. I.; Cohen, Y. (1984). Components of visual orienting. In: H. Bouma y D. Bouwhuis (Eds.) _Attention and Performance_. London: Lawrence Erlbaum, 531- 556.
*   <a name="quinn96" id="quinn96"></a>Quinn, C. (1996). Pragmatic evaluation: lessons from usability. Retrieved 23 May, 2007, from http://www.ascilite.org.au/conferences/adelaide96/papers/18.html
*   <a name="reeves94" id="reeves94"></a>Reeves, T. C. (1994). Evaluating what really matters in computer-based education. In M.Wild & D. Kirkpatrick (Eds.) _Computer education: new perspectives_, Perth, Australia: MASTEC, 219-246.
*   <a name="shneiderman98" id="shneiderman98"></a>Shneiderman, B. (1998). _Designing the user interface: strategies for effective human-computer interaction_. Reading: Addison-Wesley.
*   <a name="sokolov63" id="sokolov63"></a>Sokolov, E.N. (1983). _Perception and the conditioned reflex_. New York: McMillan.
*   <a name="squires96" id="squires96"></a>Squires D., & Preece, J. (1996). Usability and Learning: Evaluating the Potential of Educational Software. _Computers & Education_, **27** (1), 15-22.
*   <a name="squires99" id="squires99"></a>Squires, D., & Preece, J. (1999). Predicting quality in educational software: Evaluating for learning, usability and the synergy between them. _Interacting with Computers_, **11**, 467-483.
*   <a name="sternberg96" id="sternberg96"></a>Sternberg, R.J. (1996). _Cognitive Psychology_. New York: Holt, Rinehart & Winston.
*   <a name="venkatesh96" id="venkatesh96"></a>Venkatesh, V.; Davis, F. (1996). A model of antecedents of perceived ease of use: development and tests. _Decision Sciences_, 27 (3), 451-481.
*   <a name="vickery04" id="vickery04"></a>Vickery, B.C.; Vickery, A. (2004). _Information science in theory and practice_. München: K. G. Saur.
*   <a name="zhang94" id="zhang94"></a>Zhang, J.; Norman D. (1994). Representations in Distributed Cognitive Tasks. _Cognitive Science_, **March**, 87-122.  

wn purposes, adopting information literacy skills-abilities as didactic objectives in professional training and developing adequate methods to evaluate competencies and contents.



## Anexo I: Cuestionario

> 1\. ¿Dispone el recurso de una presentación que contextualice y permita optimizar su uso (no en cuanto a comandos técnicos, sino en cuanto a aprovechamiento de los contenidos)?
> 
> 2\. ¿Es posible identificar al autor del recurso (persona, institución, etc.)?
> 
> 3\. ¿Dispone el recurso de un 'mapa del sitio'?
> 
> 4\. ¿Dispone de una herramienta de búsqueda para la localización de información en él contenida (tipo casilla para introducir texto libre)?
> 
> 5\. ¿Son pertinentes las cabeceras del recurso (por así decirlo, los 'titulares' que aparecen en la página principal sobre los contenidos que se pueden encontrar en él)?
> 
> 6\. ¿Resulta atractivo el aspecto general (colores, formas) de los elementos que componen el recurso (imágenes, fondo, texto, etc.)?
> 
> 7\. ¿Presenta publicidad que impida visualizar correctamente los contenidos?
> 
> 8\. ¿Son fácilmente legibles los textos?
> 
> 9\. ¿Te resulta difícil el lenguaje empleado en el recurso?
> 
> 10\. ¿Dispone de versiones en varios idiomas?
> 
> 11\. ¿Permite enviar comentarios al webmaster o creador/administrador del recurso?
> 
> 12\. ¿Dispone de foros de discusión?
> 
> 13\. ¿Facilita información referente a la fecha de actualización de sus contenidos?
> 
> 14\. ¿Es necesaria la instalación de software adicional para su correcto uso?
> 
> 15\. ¿Resulta su diseño suficientemente claro para utilizarlo y localizar información en él?
> 
> 16\. ¿Tienen los iconos relación directa con su contenido (es decir, que si por ejemplo pinchas en un libro hay información sobre literatura)?
> 
> 17\. ¿Cambian siempre los enlaces de color de la misma manera cuando los pinchas?
> 
> 18\. ¿Dispone de enlaces ("botones") para retroceder o tienes que recurrir al botón 'Back' de tu navegador?
> 
> 19\. ¿Se dispone de enlaces a la página principal y/o al resto de páginas desde cualquier punto del recurso (es decir, no existen documentos "huérfanos")?
> 
> 20\. ¿Permite el recurso visualizar los enlaces que se van siguiendo de manera que puedas ver la secuencia de tus movimientos?
> 
> 21\. ¿Te parece clara la organización de los contenidos en el sitio?
> 
> 22\. ¿Posibilita la navegación entre contenidos cada vez más específicos (es decir, que si quieres profundizar el el tema que trata el recurso, éste te lo permite ofreciendo contenidos cada vez más específicos)?
> 
> 23\. ¿Remite el recurso a otros temáticamente relacionados que complementen el contenido del mismo?
> 
> 24\. ¿Te ofrece de forma interactiva diferentes posibilidades de navegación por sus contenidos en función del uso que hagas en cada momento?
> 
> 25\. ¿Responde el recurso adecuadamente a tu ritmo de navegación?
> 
> 26\. ¿Permite prevenir o corregir errores (por ejemplo, avisándote de que si pinchas en un enlace determinado vas a salir del recurso o si se va a abrir una nueva ventana, etc.)?
> 
> 27\. ¿Han funcionado todos los enlaces que has intentado seguir?
> 
> 28\. Si no es así, ¿permite informar a su administrador de los enlaces 'rotos'?
> 
> 29\. ¿Permite realizar varias tareas al mismo tiempo (leer, ver imágenes o vídeos y/u oir sonidos, etc.)?
> 
> 30\. ¿Te parecen apropiadas e ilustrativas las imágenes utilizadas?
> 
> 31\. ¿Presenta el recurso de forma simultánea información de distintos tipos y niveles de especificidad (texto, imágenes, video, iconos, etc.)?
> 
> 32\. En cuanto a tareas, ¿plantea el recurso actividades de diferente complejidad en función de los resultados obtenidos?
> 
> 33\. ¿Siguen las actividades propuestas un planteamiento de juego o entretenimiento?
> 
> 34\. ¿Dispone el recurso de mecanismos para facilitar el control de tus acciones (evaluación de resultados, tutoración, etc.)?
> 
> 35\. ¿Está el recurso preparado para adaptarse a discapacidades?
> 
> 36\. En general, ¿resulta su manejo fácil e intuitivo (es decir, no requiere mucho esfuerzo y/o tiempo aprender a manejarlo)?
> 
> 37\. ¿Te resulta difícil el contenido o tema del recurso?
> 
> 38\. ¿Disponías previamente de conocimientos sobre el tema del recurso?
> 
> 39\. ¿Te ofrece el recurso seguridad en la calidad de sus contenidos?
> 
> 40\. ¿Volverías a visitar este recurso para aprender más?
> 
> 41\. ¿Recomendarías este recurso a otras personas interesadas en el tema que trata?

