#### vol. 13 no. 4, December, 2008



# Information behaviour in research network building by relocated scholars in Swedish higher education: a report on a pilot project



#### [Elena Macevičiūtė](mailto:elena.maceviciute@hb.se)  
Faculty of Communication, Vilnius University, Saule.tekio al. 9, I ru-mai, Vilnius, Lithuania  
and [T.D. Wilson](mailto:wilsontd@gmail.com)  
University of Borås, Allégatan 1, 501 90 Borås, Sweden

#### Abstract

> **Introduction.** A pilot investigation is reported into the information behaviour of scholars re-locating to Sweden in building their research networks.  
> **Method.** Using activity theory as a conceptual framework for the study, structured interviews (using mainly open questions) were carried out with eight scholars in different disciplines and institutions. The resulting data were analysed using the qualitative analysis package Atlas.ti.  
> **Results**. Information systems and technologies play little part in the information behaviour of re-located scholars in building, or re-building, their research support networks. Integration into the academic institution is easier than integration into Swedish society and language issues arise mainly in respect of the latter.  
> **Conclusion**. Re-located scholars adopt interpersonal communication as the dominant information behaviour in building their research networks. They are aided, in Sweden, by the generally welcoming and supportive atmosphere of their departments (although one respondent in the social sciences noted problems of conflict), but experience difficulty in establishing social networks within Swedish society.

## Introduction

One of the main objectives of the European Union is the encouragement of mobility on the part of students, teachers, researchers and workers in general. In the education field, this has been assisted by a number of programmes, such as Tempus and Socrates (see e.g., [Corbett 2005](#cor05)). In some of the European countries (and more widely) economic pressures have acted as a spur to movement, along with the promise of superior research and study facilities.

As a result, scholars are more mobile than almost any time in the past (at least in normal, peace-time conditions). In some cases, former PhD students return to a country in which they carried out part of their doctoral studies, in other cases, scholars are attracted by the possibility of working with a leading-edge research group, in others, individuals are 'head-hunted' to lead research programmes and, finally, some attracted to another country for economic or life-style or career-development reasons. Institutions have also had a deliberate policy of encouraging scholars to re-locate to capture scarce skills, thereby adding to the flow.

Whatever the original motive for their re-location, scholars in this situation are faced with building their interpersonal research-support and information-sharing networks as they pursue their careers in a new environment. Even when the environment is not entirely new (as in the case of the returning PhD scholar), new roles force the creation of new networks, and even when a scholar is well-established in his or her field, with a wide range of international contacts, the reality of working in a new institutional environment and in a new environment of funding agencies and potential research sites demands the same attention to new interpersonal network building. This paper is concerned with aspects of the information behaviour of re-located scholars in building their research networks.

## Review of the literature

Within information behaviour research, as summarized by Case ([2007](#cas07): 258), '_recent consensus seems to be that all kinds of scientists and scholars satisfy much of their information needs through contact with their colleagues in the workplace and at conferences_'. As Sonnenwald and Ivonen ([1999](#son99b)) note, the study of social networks is essential for a full understanding of individual information behaviour. This view is supported by, among others, Lin's ([2001](#lin01)) theory of social capital, Granovetter's ([1973](#gra73)) theory of weak ties in social networks, and Sonnenwald's concept of 'information horizons' ([1999](#son99b)).

A thorough review of the literature failed to reveal any previous research specifically on the issue of social network building that employed an activity theory framework. In this sense, the work is unique. Nor did that review reveal anything that dealt specifically with the more general topic of research network building by relocated scholars. There is a considerable body of work that deals with immigrant scholars in various ways, but nothing that focuses specifically upon the research network building process.

The kind of work that does exist includes Bagchi's ([2001](#bag01)) study on how weak ties with prospective employers helps immigrant scholars through the immigration process, which was not an issue raised by our investigation. There is also research and commentary on the impact of immigrant scholars on the economy of the academic system. For example, Finn ([1995](#fin95)) reviewed the debate on scholarly immigration to the United States, in which immigrant scholars have been blamed for a lowering of academic salaries, but where many regard immigrant scholars as beneficial. We have no indication from our own work that our respondents felt other than academically welcome in their institutions.

The experience of women immigrant scientists into the USA has also been explored ([Goyette & Xie 1999](#goy99)), finding that immigrant women were less likely to be employed and promoted than either male immigrants or native-born women. We have no evidence of any similar situation in Sweden from our study: the problems experienced by women related to family matters rather than to work. Thus, one women worked in Sweden while her husband retained an academic position in the home country, and another was employed in her discipline, while her equally qualified husband was engaged in manual labour, unable to find a job in his discipline.

The concept of social capital as an aid to research performance has been investigated and, although the study ([Lazega _et al._ 2006](#laz06)) does not relate to immigrant scholars, the finding that organizational social capital proved more important than personal social capital could be of significance for immigrant scholars. However, in our study, the welcoming character of the department or the research group meant that most respondents were provided with access to the organizational capital in building their personal research networks and, reciprocally, fed those contacts back into the organization's capital.

International movement generally is shown to be beneficial for research network building. For example, in a study of Swedish post-doctoral researchers who carried out their research abroad Melin found that, 'Contacts are made during the postdoc and they often lead to research collaboration and co-publication later on' ([Melin 2004](#mel04): 95). The same study noted that senior colleagues often provide contacts for the visiting post-doctoral researcher and this was found by the one post-doctoral research interviewed in this study.

The same researcher ([Melin 2000](#mel00)) has also studied '_What goes on in the scientific networks and the research teams?_' concluding that the collaboration within the research team is key and that '_collaborations are characterized by strong pragmatism and a high degree of self-organization_'. This would be confirmed by our own investigation: acceptance by the research team was a key and the collaborations formed by the team were very much driven by the key research issues and, for example, by their involvement in the organization of conferences and workshops.

This brief survey demonstrates the complexity of the factors involved in the process of building research networks and in the experience of the immigrant scholar and it was not intended in our research to explore all of these issues. For further work, however, a number of interesting potential directions are indicated.

## Methodology

The conceptual framework used to guide the investigation was 'activity theory'. Activity theory has its origins in the Soviet Union as an alternative to the prevailing Western psychological theory of behaviourism and is based on the Marxist (and earlier) view of the relationship between the world and human consciousness. Within the activity theory perspective, consciousness is formed through human activities and, as 'objects' in the world that constitute the subject of activity are changed, so those changes reflect back into human consciousness. The subject has been presented in more detail elsewhere by one of the authors ([Wilson 2006](#wil06), [2008](#wil08)) and, therefore, only a brief introduction is offered here.

From an origin in psychology (associated mainly with the name of Vygotsky), however, activity theory was developed within a community focus through the work of Leont'ev ([1977](#leo77)) and Leont'ev's work was taken and developed by the Finnish educational researcher Engeström ([1987](#eng87)) into a framework for the exploration of change in organizations, social structures and human learning. Engeström's model of activity theory has been widely employed in fields such as human-computer interaction, computer-supported co-operative work and computer-supported learning, and is usually represented by the figure below:

<div align="center">![Figure 1: The activity system](p371fig1.jpg)</div>

<div align="center">Figure 1: The activity system (based on [Engeström, 1987](#eng87): 78)</div>

Using the activity theory approach, we define the activity as _network building_, that is, interacting with the research community to build a network of research-related contacts: the _object_ is the individual's existing network of contacts and the aim is to transform it into a richer and more relevant network in the context of the new environment. Where the individual is a member of a tightly-knit research group, the _subject_ may be that group, rather than the individual.

The _instruments_ (tools or artefacts) used to support the activity are assumed to be the tools of social interaction, that is, communication tools of one kind or another, ranging from face-to-face communication at conferences to technology-based artefacts such as e-mail and participation in online discussion lists.

The activity of network building is carried out within the norms and rules of scholarly activity in general and the rules and norms of the discipline or reference group to which the individual scholar (or research team) relates. These rules and norms are generally informal and which include such norms as responding to a stranger when that person seeks help in relation to a scientific problem or solicits participation in a project. Scholarship is essentially collaborative and norms of free exchange of information and sharing of contacts apply in the scholarly community.

Activity is also carried on within a context of the community of scholars within which a scholar is embedded: this may be the local community of a university department, but if the individual works within a highly specialised area, not shared with other departmental members, the community may be thought of as the wider network of scholars working in the same field.

Particularly in research teams and groups, there may be division of labour among the members in developing a network of contacts for the group: thus, person A attends a conference, making new contacts, which he or she brings back to the group, sharing details of the new contacts.

All this undertaken within the general socio-historico-cultural context, which involves family, institutional structures, decision-making processes and power structures.

Thus, the strength of activity theory, for our purpose, lies in its holistic approach to human activity. We are encouraged, through the framework, to explore the central aspect of our study, namely information behaviour in building research networks, in the wider context of the individual's situation. The result is that a wider range of contextual influences on that behaviour are drawn to the investigator's attention than would be the case with approaches that focus solely upon the actions taken by the persons under investigation. The framework also has the practical benefit of providing a structure for the development of the research instrument, a structured interview schedule. Each of the above elements of activity theory was reflected in the questions used and the results are reported within the same framework.

## Methods

The project has been a pilot project only, involving desk research on the general issue, together with structured interviews with eight scholars in three higher education institutions. Questions covered five areas: background on the scholar's discipline, their perception of the nature of the research community in Sweden, the norms and rules under which they operated (couched in terms of the 'way things are done'), the role of teamwork in network building, how they evaluated the efforts they had made to establish themselves, and a concluding section on what efforts might be made to help them. The interview schedule was pilot-tested with one re-located scholar and, appropriately for a pilot investigation, was continuously re-developed through the remaining interviews. Consequently, not all of the respondents were asked the same questions. This is reflected in the results, where some topics can be presented in less detail than others. The final state of the interview schedule is given in the Appendix. The interviews were carried out in the scholars' offices and lasted, on average, for about one hour.

The respondents were selected from among the employees of universities operating in the West of Sweden by surveying the Internet sites of different departments. Though we aimed for an equal representation of natural sciences, social sciences and humanities, the majority of our respondents came from the natural sciences area, since there were many fewer re-located scholars in the social sciences, and re-location in the humanities is extremely rare (with exception of language teaching). The criteria for selection were: being born outside of Sweden and getting the PhD outside of Sweden. We assumed that the foreigners who got PhD in Swedish institutions build their networks in the same way as Swedes who have chosen an academic and research career.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1: Demographics of respondents**</caption>

<tbody>

<tr>

<th style="width: 12%;">Respondent</th>

<th>Country  
of origin</th>

<th>Discipline</th>

<th>Time in  
Sweden</th>

<th>Sex</th>

</tr>

<tr>

<td align="center">1</td>

<td>Germany</td>

<td>Biology</td>

<td align="center"><1 year</td>

<td align="center">M</td>

</tr>

<tr>

<td align="center">2</td>

<td>UK</td>

<td>Physics</td>

<td align="center">>8 years</td>

<td align="center">F</td>

</tr>

<tr>

<td align="center">3</td>

<td>India</td>

<td>Comp.Sci.</td>

<td align="center">20 years</td>

<td align="center">M</td>

</tr>

<tr>

<td align="center">4</td>

<td>UK</td>

<td>Biology</td>

<td align="center">>13 years</td>

<td align="center">M</td>

</tr>

<tr>

<td align="center">5</td>

<td>Mexico</td>

<td>Soc. Sci.</td>

<td align="center">>22 years</td>

<td align="center">F</td>

</tr>

<tr>

<td align="center">6</td>

<td>Bulgaria</td>

<td>Maths</td>

<td align="center">16 years</td>

<td align="center">F</td>

</tr>

<tr>

<td align="center">7</td>

<td>Ukraine</td>

<td>Maths</td>

<td align="center">12 years</td>

<td align="center">F</td>

</tr>

<tr>

<td align="center">8</td>

<td>USA</td>

<td>Soc. Sci.</td>

<td align="center">5 years</td>

<td align="center">F</td>

</tr>

</tbody>

</table>

## Results

The results set out below are tentative because of the pilot nature of the project and cannot be generalised to the population of relocated scholars in Sweden. We believe, however, that the results are sufficiently interesting to warrant further research.

### The scholar

The scholar himself or herself, is the _subject_ in activity theory and it is clear that individual motivations and various aspects of the individual's life and career development stage will have an impact on their determination and effectiveness in building a research network.

One of the first distinctions to emerge was that of the established vs. the not-yet-established researcher. Thus, one senior scholar noted:

> The position that I came to helped me enormously because everybody knew of it and it meant that I was already an established researcher.

and, in relation to acquiring research funding, the same scholar commented:

> If you have a personal track record with these decision-makers, it is easier for them to judge a good project, for example, you gave a good talk. If you are visible to them it matters.

On the other hand, a scientist who came to Sweden immediately following his PhD research found matters more problematical:

> I haven't figured out how this place works. How you get in, who is out and when a transition is made. Power structures' decisions and research directions go together.

This experience was contrary to that of a post-doctoral researcher, who found colleagues extremely helpful: the difference appears to be that he was immediately inducted into a research team and a project, whereas the former pursued a career as a 'lone researcher' whose work appeared not to be of interest to his immediate colleagues.

Although the family situation does not appear to affect research network building (other than where both partners are working in the same research area), it can affect social relationships and the desire to develop such relationships. One female scholar's husband worked in the home country and her daughter had moved to the USA; consequently,

> I try not to think about any home, I try to be universal. My heart is happy when I connect to my relatives, because I am very lonely here.

A final aspect of the scholar's work that affected his or her academic life was its permanent or temporary character. Once a scholar acquired a permanent academic position he or she would be assured a period of stability during which the research network could be consolidated and changed or expanded as necessary. The temporary appointee, however, such as a post-doctoral researcher without a permanent post had to keep his or her options open and this would mean that the search for contacts would be more active and with more diverse intentions: for example, securing a position in Sweden, ensuring that a return to the home country would be possible, or ensuring that a move to another country would be possible.

Within activity theory, especially as developed by Engeström, the _subject_ is not necessarily the individual: subjects may be groups or teams and it would be instructive to carry out similar work to that reported here (where the focus is on the individual) with research teams as the focus.

### The nature of the research community

The nature of the research community with which the scholar interacted varied considerably: in all cases, interaction with the international community was of more concern than interaction with the local or national community. This was sometimes a consequence of the degree of specialization in the field. For example, one respondent noted:

> It's a very small area. I have more contacts in Denmark and Norway. In Sweden there are only about three people, who are spread around in the country. In Norway there are around 25 people working in the area and there are several hundred in Europe.

while another commented:

> The field has two directions (mathematical and engineering): in Sweden, the engineering direction is strong (fifteen to twenty researchers), the mathematical direction is smaller (four to six people who understand each other).

Of course, scale is a relative concept and even a physicist described her specialization as small:

> [The field] is very small [in Sweden] and everybody knows everybody.

That quotation draws attention to a desirable characteristic of smallness: the fact that a scholar working in a small area can know everyone with like interests.

Another feature of any kind of community that affects its willingness to take in the stranger is its openness. The scholars studied experienced the research communities with which they were involved rather differently: some, for example, were very positive:

> I was very much welcomed by that community and it was easy for me to move in.

> It's open and cooperative unlike in other places I have been before. I could not say how it was before, but since I have come my initiatives and efforts are warmly welcomed.

> The groupings we've got here result in lots of strong international contacts - EU projects. So the community is open because of that.

others were rather less so:

> [Area X] is the most 'academic' and dominated by an intellectual elite, very difficult to penetrate and close.

> My research community is small, tightly knit, difficult to get into and difficult to attract people to your field.

An aspect of the university community (and the wider community of researchers in Sweden), which was presented as problematic was the power structures in the community. Swedish institutions were perceived as, on the surface, consensus-seeking, but there was a belief that this position was used by those with the power to manipulate matters.

> I don't like the decision-making procedures - 'consensus' means that those in power and have decided already sit there and just wait for others to agree. The power structures are self-seeking.

> What I find difficult to cope with. is that the Swedes very rarely argue; it's difficult to know what they are really thinking and you get a watered down consensus at the end of the meeting. I'd be happier if people were banging the table more and you could know what it is they actually think.

> The way committees work is difficult to accept - it happens behind your back.... people can go off and argue against the decision with power-holders and get a different decision.

> One thing is conflict handling in academia - it's not handled openly and ignored as long as possible and never really resolved. It is difficult to understand how they see these conflicts as they are never voiced, hidden under. In [X] department it was conflict ridden, but everybody pretended nothing was happening. This is the culture of being afraid of conflicts.

(We can note that conflict avoidance is a known phenomenon of Swedish society ( [Daun 2005](#dau05): 22-32))

### The tools (mediating artefacts) employed in making contact

A very common pattern of information behaviour involved in network building was noted. The most important 'tool' was the scholarly meeting or one kind or another; conference, workshop, etc. All of the respondents noted the importance of such meetings, for example:

> We just tried strengthening the network - last week we had a workshop in our field, including others from outside Sweden. I brought in a researcher from Norway and next year we'll bid for a new project. Right now we have a feeling of a tightly bound network evolving.

> I've met Stockholm people in small workshops and they told me a lot about what was going on here: political and personal situation that was very useful to know. They knew these conditions much better than I did.

> We try to establish links. partly through the annual. conference

> We started seminars and developed a network. Then it disappeared and we had only informal contacts. We have re-created a Nordic Network, since four years ago it has been reconstructed and our institute leads it.

Even the mathematician, working independently, needs conferences:

> People are mainly solitary workers, like myself. But they meet at conferences.

One respondent noted the difficulty of working in a field where the international conferences are considered to be of greater importance:

> One problem we have here with x is that there is no a good meeting place for the National [X] Society. They have small meetings every second year, but they are very small and the main people tend not to come there. Therefore, it is very difficult to become known in Sweden, as the professors prefer to go to the American [X] Society meetings not the Swedish.

On the other hand, as a meeting place for international organizations, Sweden has a very particular advantage:

> Yes - people like to have friends in Sweden, especially in physics and chemistry. because of the Nobel Prize. They try to be invited for lectures. In international conferences all the 'big shots' expect to be invited and they all want to give plenary papers.

In one of the quotations above collaborative projects are mentioned as a way of developing and maintaining contacts and, again, these were regularly mentioned as a useful way of developing the network. This works in two ways: existing contacts may come together to create a collaborative project (usually for EU funding but also for Swedish funding), and/or in seeking to put a project team together, an individual may search for other contacts through the existing network members.

> Under EU Framework 6 we have a network of excellence, which helps to establish contacts with representatives of other areas in the UK.

> My PhD comes from an EU-funded project. I still keep in with these European project participants - those that are most useful for finding a position or solving problems of EU cooperation.

Because network building is an interpersonal activity, it is not surprising that opportunities for face-to-face communication are valued most. Other tools such as research directories, institutional Websites, e-mail and discussion lists are used to some degree, however:

> Just general Web searches, no specific directory sites.

> Web pages of conferences, e-mail lists, invitations to conferences. There is a discussion list (not named).

> I use many Websites in my work. These are for data, not for persons. I know people by names who developed these Websites.

From an information behaviour perspective, the relative insignificance of what we might term 'formal information tools' is striking and raises the issue of how far re-located scholars can be helped through the provision of such tools, or through greater interaction with information specialists.

### Cultural issues

Respondents identified two areas where cultural differences were experienced when compared with either their home country or others in which they had worked: the work situation and social relationships. In both cases, language as a mediating tool, was often associated with the problems experienced.

The work/life division in Sweden is characteristic of Swedish life style and mentality and investigators (e.g., [Allwood 1981](#all81); [Daun 2005](#dau05): 18-20) have identified it as related to protestant morality and historical conditions. It is not surprising, therefore, that, overall, respondents experienced a clear segregation of work life and social life in Sweden, with little attempt by their Swedish colleagues to help them in their integration into society. As one noted:

> Work and social life are segregated from each other here (like in Germany) - not so in the US.

And, as a result,

> When you are new it is difficult to establish connections

Most respondents had experienced difficulties in forming social relationships, partly as a consequence of the segregation of work and social life. This was sometimes attributed to the Swedes being either 'cold' or 'shy'. For example,

> In Sweden, on the personal front, it is difficult to make friends. They are shy and keep to themselves. They will not come to you. It takes more effort to get to know them. For example, if you move into a new house, no neighbour will come and bring you a flask of coffee - [in the USA, that would be normal].

A formality/informality split was also seen as associated with the work/social life split:

> There is a sense that social contacts are a little more formal - you don't go to visit someone unannounced - that leads to some problems for people who are less formal. That is the main difference.

But, rather paradoxically,

> In academia it is not so at present, though might have been before - many things can be formal, but now became less formal than in [my home country] - that is my impression. We never use titles in Swedish universities, even when talking in formal settings.

In both work and social life, language differences and problems created barriers:

> Sometimes we [foreigners] sit at the table together [in the coffee room] and the Swedes sit at another table.

> Sometimes everyone will speak Swedish despite having a very honoured foreign guest in the company. In informal situations there is social incompetence.

Some respondents spoke of the difficulty of _reading_ either work or social situations, partly because of language problems, but also because of the perceived ambiguity in the way Swedish colleagues dealt with issues.

> It takes time to become aware about differences, understand them. Still, after 16 years, I don't read well people's reactions. Sometimes you talk to a person and get an impression that you are approved and this is not the case. The way statements are given is strange - it is polite disapproval. This is an accepted way to communicate here, but it takes time to understand.

As might be expected, those respondents who had become integrated into Swedish society through marriage experienced fewer such problems and were less critical of Swedish mores.

All respondents recognized the importance of speaking Swedish in order fully to integrate into society, although for most, it was sufficient to speak English at work, and to be able to teach in English.

> It's not a problem in everyday research work, since English is spoken fluently. More of a problem in everyday life, social life - it's difficult to participate in a group when everyone is speaking Swedish.

> Language is not a problem. English works everywhere. It helps to know Swedish - it's very easy to get to the stage of sitting in Committees and using Swedish after the first year. But my spoken Swedish is bad and for a long time I would speak English. It helps to speak the language, but it is not vital.

> I learnt some Swedish as guest scientist and when I came I could speak a little Swedish. What I did - I asked colleagues not to speak to me in English but Swedish, so it happened quite quickly, though I didn't know what's going on for a while. There isn't really a language barrier as everyone speaks English anyway.

Overall, we felt that a certain tension existed for those respondents who had not 'married into' Swedish society. All had made efforts to learn Swedish, some to the level of being able to teach in the language, but, because of the split between work and social life, the 'shy' Swede and the perceived Swedish desire for privacy in social relationships, fluency in the language had not gained them full admittance to Swedish society.

### Teamwork and the division of labour

The scholars interviewed see themselves as belonging to a set of communities: the local research group or department, the university of which they are a part, the collaborative project upon which they may be engaged, the community of fellow researchers in Sweden and, most notably, the international research community in their field.

In their immediate place of work, however, the individual scholar may be a solitary researcher: this was particularly the case with the mathematicians, and one computer scientist, whom we interviewed. For such solitary workers, the immediate environment provides little if anything in the way of research connections. One respondent described as being his own fault and commented:

> I come alive when I get away from this place.' When there is a conference my research community comes to me. It's difficult to say that I haven't made the best of the community that exists but I could have made more. I heaped every possible difficulty on myself by choosing the area which is so unpopular and that guarantees that I am left in peace.

Another noted the difficulty the lone scholar has in attracting research grants:

> Research in groups is supported more than individual research. The big decisions come from somewhere and have the effect on us. In mathematics, it is many lonely scholars.

Where teamwork exists, however, it is valued as a source of support and as means of helping to develop a network. The team may be the research group in the department or it may be the group formed around a specific project, which may also be international in character.

> My project was important for finding people.

> [The team is] very supportive - I bring contacts, others suggest other contacts. We know each other and talk. It's essential, as I am new and young. It is vital, if I am not to be isolated. I also am a part of the local team. You can try to do it by yourself, but you need to be at a higher level if you try by yourself.

### How the scholars evaluate their efforts at network building.

At the end of the interview we asked, Looking back on the process, how easy or difficult to you think it is for someone in your position to establish a contact network? To some degree, the nature of the answers depended upon the status of the person: appointees at the professorial level found it easier than those at more junior positions. For example, a senior respondent commented:

> From my experience, it is relatively easy as the national community is small. People know about a new professor and they are curious about what this person is going to bring with him. Internationally, it is very easy to make contacts if you are a professor in Sweden. If you are interested in having international contacts, it is very easy to have them whether you are an Academy member or not as all the professors can nominate Nobel candidates.

And another professorial level respondent:

> I don't see it as essentially too difficult. If you come and take the initiative, people are positive.

While a post-doctoral position holder noted that the individual had to want to develop a network - it was a matter of personal motivation and, for the junior researcher, funding:

> I think the network. you have to have your own personal mindset. that must be that you want a network. It works only if you have funding. For me, right now, funding is the key.

Getting the initial permanent position was also seen as key - but this problem was restricted to the newcomers:

> Difficult thing is to get a job. After that it is just work and things happen. One of my friends in physics finished a temporary job at the university and is looking for a job for two years. There are not many academic positions, but Swedes face the same problem.

> It would now be easier [now having a permanent position] - to establish contacts is easier, but it is still difficult to get a position. Establishing contacts does not mean much as such.

Family circumstances also influenced the perception of how easy it was to develop contacts. One respondent, whose husband continues to work in the home country, commented:

> For someone like myself, it would be difficult - my personal situation is different. Otherwise, for a single, ambitious woman who gets a job and is in a research group - it's easy, she has all chances to establish herself.

Overall, there was no single experience for all of the respondents: too many variables affected the situation from status to permanence of appointment, and from family circumstances to the extent of teamwork in the research field. However, a fairly common line though expressed by the younger newcomers was the need for support as a newcomer. They noted that this would not only help the newcomer but also help Swedish research by establishing Sweden as a supportive environment in which to work.

## Contradictions and disturbances

One of the key ideas of activity theory is the notion of 'contradictions'. These are the conflicts and tensions within and between elements of an activity system and between activity systems. A number of such contradictions are evident from the reported experience of the scholars we interviewed.

First, the institutions in which they work have recruited foreign scholars because of their perceived competence and because they fill gaps in research and teaching that are difficult to fill from local candidates. However, it appears that the same institutions do little to ensure that the immigrant scholar is made to feel part of Swedish society. The only respondents who felt that Sweden was now their permanent home were those who had married Swedes, the rest intend to return to their home country or move on to another country if a suitable opportunity was offered. Failure to deal with this contradiction, therefore, could prove to be expensive for Swedish institutions.

A second contradiction of a similar kind exists in the departments or work groups: engagement in the research process is not a problem and colleagues are described as welcoming and encouraging. However, that welcome and that encouragement does not appear to extend from work into social life. On Friday evening the Swedish colleagues go off to spend their weekends with their own family and personal friends and assume that their foreign colleagues (and even, as one respondent put it, 'honoured foreign guests') are happy to do the same.

A third contradiction, again one that affects both work and social relationships, is that the norms of behaviour in both work and social life are difficult to grasp and internalise. This is partly a problem of language, but more a problem of ambiguous 'signals' from those involved in both formal academic situations and in social life. Scholars who relocate to Sweden have some difficulty in recognising and negotiating these ambiguities and one feels that a formal process of induction into the new institution, which dealt with such issues, would be helpful.

Finally, at least in terms of this short study, there is a contradiction between the perceived need for research expertise in Sweden and the difficulty immigrant scholars experience in '_getting the first grant_'. Breaking into the research funding process can be both difficult and frustrating and where those processes involve (as, inevitably, they do) social relationships among the members of the relevant committees, it is even more difficult for the newly arrived scholar not yet fluent in the language.

## Conclusions

The primary objective of this pilot project was t_o explore how re-located scholars build, re-build or add to interpersonal research networks for research support and information sharing in their new environment_. As may be seen from the report on our findings above, this objective has been met. We have determined that _personal contact_, whether in the immediate team, or the collaborative project, or through conferences and workshop, is the dominant means through which scholars in all disciplines build their networks. So far, new information and communication technologies have made little impact on the process, although one or two of the scholars did note the use of Web searches and Websites as sources of information about people.

A second objective was to determine the value of the activity theory conceptual framework as a basis for research in this area. Again, the report has shown its value: it helped to determine the objectives of the project, it aided the design of the interview schedule and it has guided the presentation of the results. We feel that this approach has much to offer in the area of scholarly communication and information behaviour. It is also evident that, had the focus been simply upon the means whereby networks are built and maintained, the interviews would have been of very brief duration and the study would have been of very limited interest. As it is, the holistic character of activity theory points us towards aspects of the scholar's situation that influence their network-building behaviour, and to the tensions and contradictions that exist in their organizations and in Swedish society generally, which also influence that behaviour. We argue, in other words, that activity theory directs us to the contextual richness of situations in ways that lead to a deeper understanding of information behaviour.

Given that this was a pilot project, involving eight scholars, no general claims can be made about the experience of scholars relocating to Sweden. However, sufficient variation of experience has been found to suggest that the subject is worth further exploration. Consequently, following upon this pilot project, we intend to develop a revised research proposal and submit it to an appropriate agency for further funding.

## Acknowledgements

We would express our thanks to the respondents who willingly gave their time to the investigation and to the anonymous referees whose comments we have tried to address. The study was financed by the Centre for Collaborative Innovation at the University of Borås, Sweden.

## References

*   <a name="all81" id="all81"></a>Allwood, J. (1981). Finns det svenska kommunikationsmönster? [Do Swedish communication patterns exist?] In _Vad är svensk kultur?_ [What is Swedish culture?] (Uppsatser från ett symposium i Göteborg i maj 1981.) Gothenburg, Sweden: University of Gothenburg, Department of Linguistics. (Papers in Anthropological Linguistics, 9, 6-50)
*   <a name="bag01" id="bag01"></a>Bagchi, A.D. (2001). Migrant networks and the immigrant professional: an analysis of the role of weak ties. _Population Research and Policy Review_, **20**(1-2), 9-31
*   <a name="cor05" id="cor05"></a>Corbett, A. (2005). _Universities and the Europe of knowledge - ideas, institutions and policy entrepreneurship in European Union higher education policy, 1955-2005._ New York, NY: Palgrave MacMillan.
*   <a name="dau05" id="dau05"></a>Daun, Å. (2005). _En stuga på sjätte våningen: Svensk mentalitet i en mångkulturell värld._ [A hut on the seventh floor: Swedish mentality in a multicultural world.] Stockholm: Brutus Östlings Bokförlag Symposion.
*   <a name="eng87" id="eng87"></a>Engeström, Y. (1987). _[Learning by expanding: an activity-theoretical approach to developmental research](http://www.webcitation.org/5aFRgsatH) _. Helsinki, Orienta-Konsultit. Retrieved 21 August, 2008 from http://lchc.ucsd.edu/MCA/Paper/Engestrom/expanding/toc.htm (Archived by WebCite® at http://www.webcitation.org/5aFRgsatH)
*   <a name="fin95" id="fin95"></a>Finn, R. (1995). [Scientists' heated debate on immigration mirrors issues argued throughout U.S.](http:// www.the-scientist.com/1995/11/27/1/4/) _The Scientist_, **9**(23), 1 Retrieved 21, August, 2008 from http:// www.the-scientist.com/1995/11/27/1/4/ (Subscription needed)
*   <a name="goy99" id="goy99"></a>Goyette, K. & Xie, Y. (1999). The intersection of immigration and gender: labor force outcomes of immigrant women scientists. _Social Science Quarterly_, **80**(2), 395-408
*   <a name="gra73" id="gra73"></a>Granovetter, M.S. (1973). The strength of weak ties. _American Journal of Sociology_, **76**(6), 1360-1380.
*   <a name="laz06" id="laz06"></a>Lazega, E., Mounier, L., Jourda, M.T. & Stofer, R.L. (2006). Organizational vs. personal social capital in scientists' performance: a multi-level network study of elite French cancer researchers (1996-1998). _Scientometrics_, **67**(1), 27-44.
*   <a name="leo77" id="leo77"></a>Leont'ev, A.N. (1977). [Activity and consciousness](http://www.webcitation.org/5aFRVr4Fd). In, _Philosophy in the USSR: problems of dialectical materialism._ (pp. 180-202). Moscow: Progress Publishers. Retrieved 21 August, 2008 from http://www.marxists.org/archive/leontev/works/1977/leon1977.htm (Archived by WebCite® at http://www.webcitation.org/5aFRVr4Fd)
*   <a name="lin01" id="lin01"></a>Lin, N. (2001). _Social capital: a theory of social structure and action._ Cambridge: Cambridge University Press.
*   <a name="mel00" id="mel00"></a>Melin, G. (2000). Pragmatism and self-organization - research collaboration on the individual level. _Research Policy_, **29**(1), 31-40.
*   <a name="mel04" id="mel04"></a>Melin, G. (2004). Postdoc abroad: inherited scientific contacts or establishment of new networks? _Research Evaluation_, **13**(2), 95-102
*   <a name="son99a" id="son99a"></a>Sonnenwald, D. (1999). Evolving perspectives of human information behaviour: contexts, situations, networks and information horizons. In: T.D. Wilson and D.K. Allen, (Eds.). _Exploring the contexts of information behaviour_. (pp. 176-190) London: Taylor Graham.
*   <a name="son99b" id="son99b"></a>Sonnenwald, D. & Ivonen, M. (1999). An integrated human behaviour research framework in information studies. _Library and Information Science Research_, **21**(4), 429-457.
*   <a name="wil06" id="wil06"></a>Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory](http://InformationR.net/ir/11-4/paper260.html). _Information Research_, **11**(4), paper 260\. Retrieved 21 August, 2008 from http://InformationR.net/ir/11-4/paper260.html (Archived by WebCite® at http://www.webcitation.org/5aG5Kl7gM)
*   <a name="wil08" id="wil08"></a>Wilson, T.D. (2008). Activity theory and information seeking. _Annual Review of Information Science and Technology_, **42**, 119-161\. [Note: through an editorial error, the chapter was mis-named, the title intended was simply 'Activity theory'.


