####  vol. 13 no. 4, December, 2008



# Link between contractually integrated retail entrepreneurs' working experience and their information gathering and interpreting styles



#### [Arto Lindblom](mailto:arto.lindblom@hse.fi)  
Helsinki School of Economics, Department of Marketing and Management, Lapuankatu 6, FIN-00100 Helsinki, Finland.  
[Rami Olkkonen](mailto:rami.olkkonen@tse.fi)  
Turku School of Economics, Department of Marketing, Rehtorinpellonkatu 3, FIN-20500 Turku, Finland

#### Abstract

> **Introduction:** This article offers new perspectives on information acquisition and interpretation in the context of contractually integrated retailing organizations. Its aim is to the shed light on the question: do the cognitive styles (i.e., information gathering and interpreting styles) differ between contractually integrated retail entrepreneurs with different length of working experience, and if they do, what are the major differences?  
> **Method:** An online survey was conducted among K-retailers from _K-alliance_. K-retailers are retail entrepreneurs who are the owners and managers of their retail businesses, and who invest considerable personal and financial resources in these enterprises. The total number of questionnaires sent out was 1170\. Of these, 226 were satisfactorily completed for use in the analysis.  
> **Results:** K-retail entrepreneurs with different length of working experience seem to have very similar mindsets. There were no clear differences between the groups that were formed. K-retailers from short to long working experience emphasise sensing over intuiting in their information gathering, and thinking over feeling as they make sense of the gathered information.  
> **Conclusions:** With regard to future research, qualitative studies could reveal issues that would enable more thorough operationalization of the concepts associated with the information gathering and interpreting in the contractually integrated retailing organizations.



## Introduction

As de Wit and Meyer observed, the managerial mind is complex and fascinating. At times it can astonish and dazzle; at other times it can disappoint and frustrate. A manager's cognitive knowledge is stored in the mind in the form of cognitive maps or schemas, which are formed over time through education, experience, and interaction with others. But these individual cognitive maps are limited by various factors. These include: (i) limited information-processing capacity; (ii) limited information-sensing ability; and, (iii) limited information-storage capacity ([de Wit and Meyer 1998](#dew98)). As a result, managers can never be as perfectly rational as computers, even if they wanted to be.

According to de Wit and Meyer, reasoning is the thought process that leads to knowing. They posited that reasoning involves the cognitive processes of identifying, diagnosing, conceiving, and realizing. White _et al._ ([2003](#whi03)) described reasoning in terms of an individual manager's 'cognitive style'. According to these authors, this cognitive style reflects a particular manager's preferred mode of gathering and interpreting information. There is an increasing number of studies on managers' cognitive styles, i.e., how managers acquire and use information in their work (e.g., [Kaye 1995](#kay95); [Brossard 1998](#bro98)). For example, Auster and Choo ([1993](aus93)) investigated how managers in the publishing and telecommunications industries acquire and use information about the external business environment. They found out that the managers use multiple, complementary sources: internal and external sources, as well as personal and impersonal sources.

Over the last few years, researchers have focused their studies on _learning_ in an organizational context (e.g., [Akbar 2005](#akb05); [Aksu and Özdemir 2005](#aks05); [Bennet and Bennet 2004](#ben04); [Sabherwal and Sabherwal 2005](#sab05); [Sanchez 2005](#san05)). Without entering into a quite complex conceptual debate between different views of learning, this study will concentrate on information gathering and interpreting the information, realizing that these phases are essential elements for learning and that these elements to a great extent define the cognitive style of the person (see for example [Bender and Fish 2000](#ben00) or [Kalpic and Bernus 2006](#kal06)).

White _et al._ ([2003](#whi03)) underline that there exist significant differences among the cognitive styles of various managers. Even more differences can be observed among entrepreneurs. According to Alvarez and Busenitz ([2001](#alv01)), there is probably no group of people that has received more scrutiny in the management and marketing literature and that has been shown to be more different from the rest of the population, than entrepreneurs. Personality theories are being increasingly used to understand how entrepreneurs think and make decisions ([Alvarez and Busenitz 2001](#alv01)).

In understanding and explaining different cognitive styles, perhaps the most tested theory is Jung's ([1946](#jun46); [1921/1971](#jun71)) theory of personality. Jung proposed that people develop one of two dominant preferences for information used in perceiving their world: sensation or intuition. In addition, Jung argued that people develop one of two dominant ways for judging the information in order to reach decisions and take action: thinking or feeling. Dominant way of perceiving and judging information define cognitive style of the person (see, [Stumpf & Dunbar 1991](#stu91)).

Despite the growth of studies related to entrepreneurs' cognitive styles (e.g., [Allinson _et al._ 2000](#all00); [Beugelsdijk and Noorderhaven 2005](#beu05); [Busenitz and Barney 1997](#bus97); [Forbes 2005](#for05); [Malach-Pines _et al._ 2002](#mal02); [Palich and Bagby 1995](#pal95)), relatively little research has focused on the question of how the length of working experience affects entrepreneurs' perception and judgement. Questions such as, do entrepreneurs with long working experience prefer to gather information through hunches or via their five senses and, do entrepreneurs with long working experience prefer to use their emotional intelligence as they make sense of the gathered information more than less experienced entrepreneurs, still remain fairly unexplored. Consequently, the research question of the present study is formulated in the following way: do the cognitive styles (i.e., information gathering and interpreting styles) differ between entrepreneurs with different length of working experience and if they do, what are the major differences?

The subjects of the study are contractually integrated K-retail entrepreneurs (K-retailers) with different length of working experience in Finland. K-retailers are entrepreneurs who are the owners and managers of their retail businesses and who invest considerable personal and financial resources in these enterprises. K-retailers are neither solo self-employed entrepreneurs nor corporate managers; they can be viewed as intermediate actors between managers and solo self-employed entrepreneurs. K-retailers belong to _K-alliance_, one of the largest retail organizations in Finland, which includes [Kesko](http://www.kesko.fi/default.asp?lang=eng), Finnish K-retailers and the K-retailers' Association ([Mitronen and Möller 2003](#mit03)). _K-alliance_ can be called a contractually integrated retailing organization.

## Theoretical background

### On the concept of cognitive style

According to Ford _et al._ ([2002](#for02)), cognitive styles are tendencies consistently displayed by individuals to adopt a particular type of information processing strategy (see also [Allinson _et al._ 2000](#all00)). Although several classifications of cognitive styles exist (e.g., [Witkin and Goodenough 1981](#wit81); [Pask 1988](#pas88); see also [Ford _et. al_. 2002](#for02); [Wood _et al._ 1996](#woo96)), White _et al._ ([2003](#whi03)) have observed that the most widely adopted classification schemes are based on the work of Carl Jung ([1921/1971](#jun71); [1946](#jun46)), who proposed that people can be grouped into types on the basis of their preferences for different sources of information and their preferences for dealing with that information in different ways. Jung's ([1946](#jun46)) typology theory has been the basis of several instruments, the best known of which is the Myers-Briggs Type Indicator (MBTI) ([Myers 1962](#mye62); [Myers and McCaulley 1985](#mye85)). The Myers-Briggs Type Indicator was designed specifically to measure Jungian theory constructs and to determine personality types. The instrument is widely used in business, psychology, education and career counseling ([Edwards _et al._ 2002](#edw02)). Despite its popular appeal, the Myers-Briggs Type Indicator has been the subject of criticism. Wheeler, Hunton and Bryant ([2004](#whe04)) noted that it captures the _direction_ of preference, rather than its _strength_; that is, it is appropriate for sorting, but does not provide continuous measurement ([Wheeler 2001](#whe01); [Mendelsohn _et al._ 1982](#men82); [Weiss _et al._ 1982](#wei82)).

The Myers-Briggs Type Indicator measures four dimensions of psychological preference: (i) extraversion/introversion; (ii) judging/perceiving; (iii) sensing/intuition; and (iv) thinking/feeling. These four dimensions of personality are integrated into a _personality typology_ of sixteen personality types. The present study focuses on the last two of these (sensing/intuiting (for perception) and thinking/feeling (for judgment)) which Jung described as the basic mental functions of an individual ([Gould 1991](#gou91); [Stumpf and Dunbar 1991](#stu91)). A typology of personality type, based on the dimensions of sensing/intuiting (for perception) and thinking/feeling (for judgment), consists of the following four groups of functions: (i) sensing-thinking (ST); (ii) sensing-feeling (SF); (iii) intuiting-thinking (NT); and (iv) intuiting-feeling (NF).

The types can be summarized as follows ([Gould 1991](#gou91); [Volkema and Gorman 1998](#vol98); [Stumpf and Dunbar 1991](#stu91)).

*   The _ST personality type_ is the epitome of the individual decision-maker about whom classical and neo-classical economists have theorized. The ST comes closest to assumptions about the rational, economic person, in that he or she does try to make rational decisions logically and empirically.
*   The _SF personality type_ differs from the ST type in that the SF makes decisions more subjectively, based on things that matter in terms of personal values, as opposed to logic.
*   The _NT personality type_ is likely to consider a wide range of possibilities, largely through imagination and then weighs them logically in the mind.
*   The _NF personality type_ is the precise opposite of the ST. Like the NT, the NF type considers a range of possibilities and operates in holistic manner; however, the NF type often ignores traditional methods and standardized procedures in favour of novel solutions.

### Forging links between cognitive style and length of working experience

The present study aims to analyse the possible link between above described cognitive styles (i.e., preferences for the ways in which individuals perceive and judge information) and the length of working experience (i.e., the cumulative body of knowledge and skills).

According to Grégoire ([2003](#gre03)), some researchers argue that as people age their frame of values and their personality traits do not change significantly. In other words, cognitive styles are seen to be very stable. Theorists belonging to the second school think that personality is built through different stages corresponding to different life periods. Thus, cognitive styles are seen to change as individuals are getting older. All in all, Grégoire ([2003](#gre03)) argues that the question of how personality changes with age is very complex to answer. According to him, just a few studies have addressed this issue and these are in consumer behavior and marketing literature (e.g., [Moschis 1994](#mos94)). Readers interested in this complex topic may find the article by Kogan ([1990](#kog90)) useful.

Weber and Schaper ([2004](#web04)) have stated that there is a need to investigate differences between younger and older entrepreneurs. They underlined that a better understanding of the so-called _grey entrepreneurs_ is a substantive gap in contemporary research that needs to be rectified. According to Weber and Schaper, one resource that most older entrepreneurs would appear to possess in greater quantities than their younger counterparts is experience: the cumulative body of knowledge, skill, practice and learning that is acquired over time. Thereby, it is interesting to study how the experience of _grey entrepreneurs_ is manifested in their personality. In particular, there are two questions that should be studied more closely:

*   are the more experienced entrepreneurs more intuitive-dominant than the less experienced entrepreneurs as far as information gathering styles are concerned
*   are the more experienced entrepreneurs more feeling-dominant than the less experienced entrepreneurs as far as information interpreting is considered.

### On the concept of contractually integrated entrepreneurs

The present study explores the cognitive styles of entrepreneurs. Knight ([1984](#kni84)) states that entrepreneurs range from the solo self-employed individual to the corporate entrepreneur who may work for a large corporation, but has many of the behavioural characteristics of the independent entrepreneur. Knight argues that degree of independence (or, alternatively, degree of autonomy) determines the entrepreneur type in question.

Figure 1 illustrates one framework for studying the entrepreneur's behaviour and degree of independence. Although the types presented in Figure 1 are not exhaustive, they cover a broad range of entrepreneurial behaviour, from the independent entrepreneur to the large corporation manager.

<div align="center">![Figure 1 Degree of independence of various entrepreneur types](p384fig1.gif)</div>

<div align="center">  
**Figure 1: Degree of independence of various entrepreneur types ([Knight 1984](#kni84))**</div>

Within this wide range of entrepreneurial activity, the present study focuses on the cognitive styles of contractually integrated entrepreneurs. Contractually integrated entrepreneurs are neither solo self-employed entrepreneurs nor corporate managers ([Knight 1984](#kni84); [Gassenheimer _et al._ 1996](#gas96); [Kaufmann and Dant 1998](#kau98)). [The term 'contractually integrated' refers to interorganizational, contractual-based, governance arrangement (see e.g., [Heide 1994](#hei94), [Grandori 1997](#gra87); [Grandori and Soda 1994](#gra944)). Franchising represents perhaps the most widespread contractual-based governance arrangement among retail entrepreneurs (e.g., [Kaufmann and Dant 1998](#kau98); [Tuunanen and Hyrsky 2001](#tuu01)). However, there are also several other types of contractual-based arrangements among the parties than franchising; for example, alliances, cooperatives, constellations and network partnerships ([Gassenheimer _et al._ 1996](#gas96)).] According to the literature, contractually integrated entrepreneurs operate fairly independently and are often responsible for primary investments in their enterprise ([Gassenheimer _et al._ 1996](#gas96)). However, they are expected to closely follow the shared norms and standards in their network (e.g., [Kaufmann and Dant 1998](#kau98)) (for example, joint marketing and common product selection). Even though there is a certain level of uniformity among contractually integrated entrepreneurs, each retailer has still wide latitude to develop unique ways of marketing their product and services in their particular location. In other words, contractually integrated entrepreneurs are expected to be active in taking advantage of local opportunities ([Kaufmann and Dant 1998](#kau98); [Sen 1998](#sen98); [Tuunanen and Hyrsky 2001](#tuu01)).

Tuunanen and Hyrsky ([2001](#tuu01)) studied entrepreneurial characteristics of franchisees (which can be understood as one type of contractual-based entrepreneurship) operating in various sectors and found out that franchisees can be viewed as intermediate actors between employee managers and independent owners (see also [Knight 1984](#kni84)). Despite the growth of studies on contractual integrated entrepreneurship, studies on the personality of contractually integrated entrepreneurs have remained relatively sparse.

## Research design

The basic research design used in this study was an online survey using data from a sample of K-retailers. K-retailers offer a varied selection of retail products, including groceries, household goods and specialty goods. These are offered to consumers though a variety of store formats, including small convenience groceries, specialty stores, department stores and large hypermarkets ([Mitronen and Möller 2003](#mit03)).

_K-alliance_ is a retailing organization that differs fundamentally from other large international retailing organizations such as Wal-Mart and Tesco. The latter own all of their retail outlets and manage them centrally; they do not have retail entrepreneurs making independent decisions at the store level. In contrast, _K-alliance_ is a contractually integrated strategic network ([Powell 1990](#pow90)). This network consists of _Kesko_ and its subsidiaries and personnel operating in Finland and abroad, together with legally independent K-retailers and their personnel. In a broad sense, the relationships between Kesko and K-retailers have some features of the relationship between franchisee and franchisor even though _K-alliance_ is not considered as a franchising system by the parties to the relationships. In Europe, ICA AB in Sweden and Edeka in Germany have similar structures to that of _K-alliance_ in Finland.

The sample for the present study consisted of 1170 K-retailers from _K-alliance_. An online survey was conducted (with [Webropol](http://www.webcitation.org/5crwJtOiY) software) in May 2005\. Having obtained the e-mail addresses of all K-retailers in Finland through the good offices of the [K-retailers' Association](http://www.webcitation.org/5crwUGm4D), an initial e-mail was sent to each retailer describing the purpose of the study and requesting the retailer's participation. Approximately one week after the initial mailing, a second e-mail directed the retailers to a Website where the survey questionnaire was located. A total of 247 K-retailers completed the survey, of which 226 responses were satisfactory for data analysis. The overall response rate to the mailout was 19.3%. This can be considered satisfactory for the present analytical purposes. Respondents included both food K-retailers (63%) and consumer-durable goods K-retailers (37%). Consumer-durable goods K-retailers offer a versatile selection of retail products covering building supplies and interior decoration items, agricultural supplies and machinery and home and specialty goods.

The content of the survey questionnaire was determined on the basis of previous theoretical and empirical research into the cognitive styles of individuals and several lengthy discussions with K-retailers and managers from Kesko. A pre-test was then carried out by asking five K-retailers to complete the questionnaire. The objectives of the pre-test were to assess whether retailers understood the questions and to determine how long it took to complete the questionnaire. No unclear items were found. This procedure, in its entirety, aimed to ensure the content validity of the study.

The information acquisition and the interpretation of acquired information of K-retailers were conceptualized on the basis of Jung's ([1921/1971](#jun71)) typology. As indicated above, the basic conceptual assumption was that individual K-retailers preferred one of two ways (_sensing_ or _intuiting_) to perceive information and one of two ways (_thinking_ or _feeling_) to judge information. It was further assumed that these basic preferences defined four cognitive styles (i.e., personality types) of K-retailers as described previously: (i) sensing-thinking (STs); (ii) sensing-feeling (SFs); (iii) intuiting-thinking (NTs); and (iv) intuiting-feeling (NFs).

The theoretical constructs of the study were measured on five-point Likert-type scales (1 = strongly disagree to 5 = strongly agree). For each of the constructs, scale scores were computed to provide the mean of each individual item. Where necessary, reverse scoring was done. The final scale items used were developed and refined from an initial pool of scale items, based on the principle of finding the optimal Cronbach's alpha score among the theoretically interrelated items.

The following items were used to measure the <a name="OLE_LINK1">sensing/intuiting</a>(for acquiring the information) construct:

*   My style of information-gathering is systematic and well-planned;
*   I gather information regularly;
*   I gather information on my own initiative;
*   I do not collect information actively; and
*   I collect information only when the chain headquarters tell me to do so.

Cronbach's alpha was utilized as a test of whether items are sufficiently interrelated to justify their combination as a construct. Typically, reliability coefficients of 0.7 or more are considered adequate ([Nunnally 1978](#nun78)). However, Nunnally further states that acceptable Cronbach alpha values for new scales can be taken as 0.6\. The scales exhibiting reliability values greater than 0.7 can be accepted straightaway. In the present study, Cronbach's alpha of the sensing/intuiting construct was 0.78 which can be considered as high enough ([Nunnally 1978](#nun78); [Nunnally & Bernstein 1994](#nun94)).

The following items were used to measure the thinking/feeling (for the interpretation of acquired information) construct:

*   My style of interpreting the information is analytic and rational;
*   I use a considerable amount of time to analyse the gathered information;
*   I am capable enough to analyse the collected information myself;
*   I extensively use the analysed information in my decision making;
*   I am critical of the gathered information;
*   Decision-making would be very difficult without extensive information gathering;
*   I consider the interpretation of the gathered information as being obsolete when it comes to decision-making; and
*   The gathered information remains unanalysed.

Cronbach's alpha of the thinking/feeling construct (0.72) can be considered as sufficient for research purposes.

The form of perception (i.e., sensing/intuiting, Cronbach's alpha = 0.78) and judgement (i.e., thinking/feeling, Cronbach's alpha = 0.72) were measured by creating two composite variables which were based on the sum of the values of the original variables that operationalize our constructs of information gathering (perception) and information interpreting (judgment). The value range of the composite variable of perception varied from 5 to 25 whereas the value range of judgment composite varied from 7 to 35.

In the present study, the K-retail entrepreneurs were classified into three groups in accordance with the length of their working experience as a retailer:

*   K-retailers with short working experience, i.e., working experience as a retailer less than 3 years (n = 40)
*   K-retailers with moderate working experience, i.e., working experience as a retailer between 3-15 years (n = 144)
*   K-retailers with long working experience, i.e., working experience as a retailer more than 15 years (n = 39)

With the help of this classification, the aim is to understand the relationship between the acquisition of information and the interpretation of acquired information and K-retailers' working experience (i.e., cumulative body of knowledge and skills).

## Findings

The objective of the study was to classify the cognitive styles (i.e., the acquisition of information and the interpretation of acquired information) of K-retailers along the dimensions of sensing/intuiting (perception) and thinking/feeling (judgment). The ultimate aim was to shed light on the relationship between the acquisition of information and the interpretation of acquired information, and K-retailers' working experience. Three groups were formed in order to highlight possible differences in cognitive styles. These groups were retailers with short, with moderate and with long working experience.

Within the all three groups, it was not possible to classify all of the respondents into pure personality types according to their styles of information-gathering and their styles of interpreting that information. Consequently, in addition to the four basic personality types presented by Jung (as noted above), the present study identified _hybrids_, which oscillated between the pure types (see Tables 1, 2 and 3).

Three value categories for the composite variables were formed as follows. With respect to the dimension of perception, if the value of the style of information-gathering construct was between 19 and 25, this style of information-gathering was categorized as being predominantly _sensing_. If the value was between 5 and 11, this style of information-gathering was categorized as being predominantly _intuiting_. The style of information-gathering was categorized as a sensing/intuiting _hybrid_ if the value of the construct ranged from 12 to 18\. With respect to the dimension of judgment, if the value of the style of information-interpreting construct was between 25 and 35, it was categorized as predominantly _thinking_. If the value was between 7 and 15, the style of information-interpreting was categorized as being predominantly _feeling_. The style of information-interpreting was categorized as a thinking/feeling _hybrid_ if the value of the construct ranged from 16 to 24.

<table style="width:80%; text-align: center;" border="1" cellpadding="3" cellspacing="0" align="center"><caption align="bottom">  
**Table 1: Classification of cognitive styles of K-retailers with short working experience along the dimensions sensing/intuiting and thinking/feeling**</caption>

<tbody>

<tr style="background-color: #fdffdd">

<th colspan="2" rowspan="2"> </th>

<th colspan="4">Style of gathering information</th>

</tr>

<tr style="background-color: #fdffdd">

<th>Predominantly intuiting</th>

<th>Sensing-intuiting-hybrid</th>

<th>Predominantly sensing</th>

<th>Row total</th>

</tr>

<tr style="background-color: #fdffdd">

<th rowspan="3">Style of interpreting information</th>

<th>Predominantly feeling</th>

<td style="background-color: #FBFBFB;">0  
0 %</td>

<td style="background-color: #FBFBFB;">0  
0 %</td>

<td style="background-color: #C0C0C0;">0  
0 %</td>

<td>0  
0 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th>Thinking-feeling hybrid</th>

<td style="background-color: #FBFBFB;">2  
5.0 %</td>

<td style="background-color: #C0C0C0;">14  
35.0 %</td>

<td style="background-color: #808080;">6  
15.0 %</td>

<td>22  
55.0 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th>Predominantly thinking</th>

<td style="background-color: #C0C0C0;">0  
0 %</td>

<td style="background-color: #808080;">7  
17.5 %</td>

<td style="background-color: #808080;">11  
27.5 %</td>

<td>18  
45.0 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th colspan="2" align="right">Column total   </th>

<td>2  
5.0 %</td>

<td>21  
52.5 %</td>

<td>17  
42.5 %</td>

<td>n=40  
100 %</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="width:20px; background-color: #FBFBFB;"> </td>

<td colspan="5" style="text-align: left;">= Intuiting-feeling oriented (5 %)</td>

</tr>

<tr>

<td style="background-color: #C0C0C0;"></td>

<td colspan="5" style="text-align: left; background-color: #fdffdd">= Hybrid (35 %)</td>

</tr>

<tr>

<td style="background-color: #808080;"></td>

<td colspan="5" style="text-align: left; background-color: #fdffdd">= Sensing-thinking oriented (60 %)</td>

</tr>

</tbody>

</table>

The most common cognitive style of the K-retailers with short working experience was the sensing-thinking oriented type (60%). In other words, the majority of the K- retailers with less than three years working experience emphasise active and systematic information-gathering and analytical information-interpreting. There were also several K-retailers who could be classified as hybrid types (35%). Only few represented the intuiting-feeling type (5%).

<table style="width:80%; text-align: center;" border="1" cellpadding="3" cellspacing="0" align="center"><caption align="bottom">  
**Table 2: Classification of cognitive styles of K-retailers with moderate working experience along the dimensions sensing/intuiting and thinking/feeling**</caption>

<tbody>

<tr style="background-color: #fdffdd">

<th colspan="2" rowspan="2"> </th>

<th colspan="4">Style of gathering information</th>

</tr>

<tr style="background-color: #fdffdd">

<th>Predominantly intuiting</th>

<th>Sensing-intuiting-hybrid</th>

<th>Predominantly sensing</th>

<th>Row total</th>

</tr>

<tr style="background-color: #fdffdd">

<th rowspan="3">Style of interpreting information</th>

<th>Predominantly feeling</th>

<td style="background-color: #FBFBFB;">1  
0.8 %</td>

<td style="background-color: #FBFBFB;">0  
0 %</td>

<td style="background-color: #C0C0C0;">0  
0 %</td>

<td>1  
0.8 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th>Thinking-feeling hybrid</th>

<td style="background-color: #FBFBFB;">8  
6.3 %</td>

<td style="background-color: #C0C0C0;">39  
30.7 %</td>

<td style="background-color: #808080;">21  
16.5 %</td>

<td>68  
53.5 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th>Predominantly thinking</th>

<td style="background-color: #C0C0C0;">0  
0 %</td>

<td style="background-color: #808080;">17  
13.4 %</td>

<td style="background-color: #808080;">41  
32.3 %</td>

<td>58  
45.7 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th colspan="2" align="right">Column total   </th>

<td>9  
7.1 %</td>

<td>56  
44.1 %</td>

<td>62  
48.8 %</td>

<td>n=127  
100 %</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="width:20px; background-color: #FBFBFB;"> </td>

<td style="text-align: left;" colspan="5">= Intuiting-feeling oriented (7.1 %)</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="background-color: #C0C0C0;"> </td>

<td style="text-align: left;" colspan="5">= Hybrid (30.7 %)</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="background-color: #808080;"> </td>

<td style="text-align: left;" colspan="5">= Sensing-thinking oriented (62.2 %)</td>

</tr>

</tbody>

</table>

As Table 2 shows, the most common cognitive style among K-retailers with moderate working experience was the also sensing-thinking oriented type (62 %). Almost one third of K-retailers with three to fifteen years working experience could be classified as hybrid types (31 %). Only minority of the K-retailers with moderate working experience represented intuiting-feeling type (7 %). Among this group, one of the respondents represented a pure _intuiting-feeling_ type.

<table style="width:80%; text-align: center;" border="1" cellpadding="3" cellspacing="0" align="center"><caption align="bottom">  
**Table 3: Classification of cognitive styles of K-retailers with long working experience along the dimensions sensing/intuiting and thinking/feeling**</caption>

<tbody>

<tr style="background-color: #fdffdd">

<th colspan="2" rowspan="2"> </th>

<th colspan="4">Style of gathering information</th>

</tr>

<tr style="background-color: #fdffdd">

<th>Predominantly intuiting</th>

<th>Sensing-intuiting-hybrid</th>

<th>Predominantly sensing</th>

<th>Row total</th>

</tr>

<tr style="background-color: #fdffdd">

<th rowspan="3">Style of interpreting information</th>

<th>Predominantly feeling</th>

<td style="background-color: #FBFBFB;">0  
0 %</td>

<td style="background-color: #FBFBFB;">0  
0 %</td>

<td style="background-color: #C0C0C0;">0  
0 %</td>

<td>0  
0 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th>Thinking-feeling hybrid</th>

<td style="background-color: #FBFBFB;">6  
15.4 %</td>

<td style="background-color: #C0C0C0;">18  
46.2 %</td>

<td style="background-color: #808080;">4  
10.3 %</td>

<td>28  
71.8 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th>Predominantly thinking</th>

<td style="background-color: #C0C0C0;">0  
0 %</td>

<td style="background-color: #808080;">2  
5.1 %</td>

<td style="background-color: #808080;">9  
23.1 %</td>

<td>11  
28.2 %</td>

</tr>

<tr style="background-color: #fdffdd">

<th colspan="2" align="right">Column total   </th>

<td>6  
15.4 %</td>

<td>20  
51.3 %</td>

<td>13  
33.3 %</td>

<td>n=39  
100 %</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="width:20px; background-color: #FBFBFB;"> </td>

<td colspan="5" style="text-align: left;">= Intuiting-feeling oriented (15.4 %)</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="background-color: #C0C0C0;"></td>

<td colspan="5" style="text-align: left;">= Hybrid (46.2 %)</td>

</tr>

<tr style="background-color: #fdffdd">

<td style="background-color: #808080;"></td>

<td colspan="5" style="text-align: left;">= Sensing-thinking oriented (38.5 %)</td>

</tr>

</tbody>

</table>

Among the K-retailers with long working experience the most common cognitive style was the hybrid type (46%). There were also several retailers who could be classified as sensing-thinking oriented types (39%). It was interesting to find out that in this group there were also many retailers who could be classified as intuiting-feeling oriented types (15%). In other words, K-retailers with long working experience are more likely to lean on intuition than less experienced K-retailers (chi-squared test indicated statistically significant differences in the percentage of retailers between the categories of working experience and cognitive styles, p=.000). It seems that the existence of intuition-dominant cognitive features and length of working experience are related to each other, at least to some extent. However, further studies are needed in order to assess this possible connection.

## Discussion

In this study, the basic conceptual assumption was that individual K-retailers prefer one of two ways (_sensing_ or _intuiting_) to perceive information and one of two ways (_thinking_ or _feeling_) to judge the gathered information. It was further assumed that these basic preferences defined four cognitive styles among K-retailers: (i) sensing-thinking; (ii) sensing-feeling; (iii) intuiting-thinking; and (iv) intuiting-feeling.

Results revealed that the most common cognitive style of among K-retailers with short and moderate working experience was the sensing-thinking type. Among these both groups there were also many K-retailers who could be classified as hybrid types. Only a minority represented the intuiting-feeling type. Among the K-retailers with long working experience the most common cognitive style was the hybrid type. Among this group there were also several retailers who could be described as being more _intuition-dominant_ than _sensation-dominant_. Based on the research results, it could be argued at least to some extent that the more a retail entrepreneur has working experience (i.e., a cumulative body of knowledge and skills), the more likely he or she is to lean on intuition, in contrast to those with less working experience. However, to assess this relation more directly, it would be necessary to study, in greater detail, how the actual decision-making of retailers is dependent on the length of working experience.

All in all, the K-retail entrepreneurs with different length of working experience seem to have very similar mind-sets. There were no clear differences between the groups that were formed. K-retailers from short to long working experience emphasise sensing over intuiting in their information gathering and thinking over feeling as they make sense of the gathered information.

It is interesting to discuss in more detail why the majority of K-retailers emphasize sensing and thinking in their information gathering and analysing of gathered information. One explanation for the high level of sensing and thinking might be found in the systems of training in _K-alliance_. _K-alliance_ might prepare and coach K-retailers to actively gather information from various information sources. Another explanation could be that the _K-alliance_ recruits people who have an inherently sensing-thinking cognitive style. All in all, there can be numerous factors that influence and channel retailers' information search and acquisition.

## Conclusions

There is an increasing number of studies that promote psychological profiling as a useful tool in the study of entrepreneurs (see for example, [Alvarez & Busenitz 2001](#alv01)). In accordance with this trend, the purpose of this article was to investigate the possible relation between cognitive styles and entrepreneurs' working experience. The research question was formulated in the following way: do the cognitive styles (i.e., information gathering and interpreting styles) differ between entrepreneurs with different length of working experience and if they do, what are the major differences?

In the present study all four of Jung's personality _types_ were found among the K-retailers. In addition, there emerged hybrid personality types that could not be classified in any of Jung's _types_. These _hybrid types_ oscillated between the _pure types_ and could be found in all three groups (short, moderate and long working experience).

The majority of K-retailers could be classified as sensing-thinking-oriented types. K-retailers, regardless of their length of working experience, seem to make their decisions based on active and systematic information-gathering and analytic information-interpreting. However, the present study indicates that (at least to some) extent retailers with long working experience are more likely to rely on intuition than retailers with less working experience. To assess this relation more directly, it would be necessary to study in greater detail how the actual decision-making of retailers might be dependent on the length of working experience.

It should be noted that the revealed small differences in cognitive styles among contractually integrated K-retailers could be explained by the existence of other factors not considered in the present study. In other words, there can be numerous factors that influence and channel K-retailers' information search and acquisition. In addition, it is possible that the cognitive styles of K-retailers are to some extent incompletely measured, even though the items and the sub-constructs were carefully formed and pre-tested. Consequently and with regard to future research avenues, the more various variables should be taken into account in assessing the relationship between entrepreneurs' cognitive styles and length of working experience. In addition to quantitative studies, qualitative studies could reveal issues that would enable more thorough operationalization of the concepts associated with the entrepreneurs' cognitive styles in the context of contractually integrated retailing organizations. Although there is still a lot of research to do, the present study forms a good base for further, more comprehensive studies.

## Acknowledgements



## References

*   <a id="akb03" name="akb03"></a>Akbar, H. (2003). Knowledge levels and their transformation: towards the integration of knowledge creation and individual learning. _Journal of Management Studies_, **40**(8), 1997-2021.
*   <a id="aks05" name="aks05"></a>Aksu, A. & Özdemir, B. (2005). Individual learning and organization culture in learning organizations: five star hotels in Antalya region of Turkey Managerial. _Auditing Journal_, **20**(4), 422-441.
*   <a id="all00" name="all00"></a>Allinson, C.W., Chell, E. & Hayes, J. (2000). Intuition and entrepreneurial behaviour. _European Journal of Work and Organizational Psychology_, **9**(1), 31-43.
*   <a id="alv01" name="alv01"></a>Alvarez, S. & Busenitz, L.W. (2001). The entrepreneurship of resource-based theory. _Journal of Management_, **2**(6), 755-776.
*   <a id="aus93" name="aus93"></a>Auster, E. & Choo, C. (1993). Environmental scanning by CEOs in two Canadian industries. _Journal of the American Society for Information Science,_ **44(**4) 194-203.
*   <a id="ben00" name="ben00"></a>Bender, S. & Fish, A. (2000). The transfer of knowledge and the retention of expertise: the continuing need for global assignments. _Journal of Knowledge Management_, **4**(2), 125-137.
*   <a id="ben04" name="ben04"></a>Bennet, A. & Bennet, D. (2004). The partnership between organizational learning and knowledge management. In _Handbook on knowledge management 1: knowledge matters, Vol. 1_ (pp. 439-455). Berlin: Springer.
*   <a id="beu05" name="beu05"></a>Beugelsdijk, S. & Noorderhaven, N. (2005). Personality characteristics of self-employed: an empirical study. _Small Business Economics_, **24**(2), 159-167.
*   <a id="bro98" name="bro98"></a>Brossard, H. L. (1998). Information sources used by an organization during a complex decision process: an exploratory study. _Industrial Marketing Management_, **27**(1), 41-50.
*   <a id="bus97" name="bus97"></a>Busenitz, L. & Barney (1997). Differences between entrepreneurs and managers in large organizations: bias and heuristic in strategic decision-making. _Journal of Business Venturing,_ **12**(9), 9-31.
*   <a id="dew98" name="dew98"></a>de Wit, B. & Meyer, R. (1998). _Strategy: process, content, context: an international perspective._ (2nd. ed.). London: Thompson Business Press.
*   <a id="edw02" name="edw02"></a>Edwards, J.A., Lanning, K. & Hooker, K. (2002). The MBTI and social information processing: an incremental validity. _Journal of Personality Assessment_, **78**(3), 432-450.
*   <a id="for05" name="for05"></a>Forbes, D. (2005). Are some entrepreneurs more overconfident than others? _Journal of Business Venturing_, **20**, 623-640.
*   <a id="for02" name="for02"></a>Ford, N., Wilson, T.D., Foster, A., Ellis, D. & Spink, A. (2002). Information seeking and mediated searching. Part 4: Cognitive styles in information seeking. _Journal of the American Society for Information Science and Technology_, **53**(9), 728-735.
*   <a id="gas96" name="gas96"></a>Gassenheimer, J.B., Baucus, D.B. & Baucus, M.S. (1996). Cooperative arrangements among entrepreneurs: an analysis of opportunism and communication in franchise structures. _Journal of Business Research,_ **36**, 67-79\.
*   <a id="gou91" name="gou91"></a>Gould, S. (1991). Jungian analysis and psychological types: an interpretive approach to consumer choice behavior. _Advances in Consumer Research_, **18**(1), 743-748.
*   <a id="gra97" name="gra97"></a>Grandori, A. (1997). An organizational assessment of interfirm coordination modes. _Organization Studies_, **18**(6), 897-925.
*   <a id="gra95" name="gra95"></a>Grandori, A. & Soda, G. (1995). Inter-firm networks: antecedents, mechanisms and forms. _Organization Studies_, **16**(2), 183-214.
*   <a id="gre03" name="gre03"></a>Grégoire, Y. (2003). The impact of aging on consumer responses: what do we know? _Advances in Consumer Research_, **30**(1), 19-26.
*   <a id="hei94" name="hei94"></a>Heide, J. (1994). Interorganizational governance in marketing channels. _Journal of Marketing,_ **58**(1), 71-85.
*   <a id="hil95" name="hil95"></a>Hill, R. & Levenhagen, M. (1995). Metaphors and mental models: sensemaking and sensegiving in innovative and entrepreneurial activities. _Journal of Management_, **21**(6), 1057-1075.
*   <a id="jun46" name="jun46"></a>Jung, C.G. (1946). _Psychological types, or the psychology of individuation_. New York, NY: Harcourt, Brace.
*   <a id="jun71" name="jun71"></a>Jung, C.G. (1921/1971). _Psychological types_. Princeton, NJ: Princeton University Press.
*   <a id="kal06" name="kal06"></a>Kalpic, B. & Bernus, P. (2006), Business process modeling through the knowledge management perspective. _Journal of Knowledge Management_, **10**(3), 40-56.
*   <a id="kau98" name="kau98"></a>Kaufmann, P.J. & Dant, R.P. (1998). Franchising and the domain of entrepreneurship research. _Journal of Business Venturing_, **14**, 5-16\.
*   <a id="kay95" name="kay95"></a>Kaye, D. (1995). Sources of information, formal and informal. _Management Decision_, **33**(5), 13-16.
*   <a id="kni84" name="kni84"></a>Knight, R. (1984). The independence of the franchisee entrepreneur. _Journal of Small Business Management_, **22**(2), 53-61.
*   <a id="kog90" name="kog90"></a>Kogan, N. (1990). Biological and health influences on behaviour. In J.E. Birren & W.K. Schaie (Eds), _Handbook of the psychology of aging_ (pp. 330-343). San Diego, CA: Academic Press.
*   <a id="mal02" name="mal02"></a>Malach-Pines, A., Sadeh, A., Dvir, D. & Yafe-Yanai, O. (2002). Entrepreneurs and managers: similar yet different. _The International Journal of Organizational Analysis_, **10**(2), 172-190\.
*   <a id="mit03" name="mit03"></a>Mitronen, L. & Möller, K. (2003). Management of hybrid organisations: a case study in retailing. _Industrial Marketing Management_, **32**, 419-429.
*   <a id="mos96" name="mos96"></a>Moschis. G.P. (1996, September). [Life stages of the mature market.](http://www.webcitation.org/5crpt6JmF) _American Demographics,_ 44-50\. Retrieved 5 December, 2008 from http://bit.ly/DTyI (Archived by WebCite® at http://www.webcitation.org/5crpt6JmF)
*   <a id="mye62" name="mye62"></a>Myers, I.B. (1962). _Manual: the Myers-Briggs Type Indicator_. Princeton, NJ: Educational.
*   <a id="mye85" name="mye85"></a>Myers, I.B. & McCaulley, M.H. (1985). _Manual: a guide to the development and use of the Myers-Briggs Type Indicator._ Palo Alto, CA: Consulting Psychologists Press.
*   <a id="nun78" name="nun78"></a>Nunnally, J.C. (1978). _Psychometric theory_. New York, NY: McGraw-Hill.
*   <a id="nun94" name="nun94"></a>Nunnally, J.C. & Bernstein,I.H. (1994). _Psychometric theory_. New York, NY: McGraw-Hill.
*   <a id="pal95" name="pal95"></a>Palich, L. & Bagby, D. (1995). Using cognitive theory to explain entrepreneurial risk-taking: challenging conventional wisdom. _Journal of Business Venturing_, **10**(6), 425-438.
*   <a id="pas88" name="pas88"></a>Pask, G. (1988). Learning strategies, teaching strategies and conceptual or learning style. In R.R Schmeck (Ed.), _Learning strategies and learning styles_ (pp. 83-99). New York, NY: Plenum Press.
*   <a id="pow90" name="pow90"></a>Powell, W.W. (1990). Neither market nor hierarchy: network forms of organization. In B.M. Staw & L.L. Cummings (Eds), _Research in Organizational Behavior_, (pp. 295-336). Greenwich, CT: JAI Press.
*   <a id="sab05" name="sab05"></a>Sabherwal, R. & Sabherwal, S. (2005). Knowledge management using information technology: determinants of short-term impact on firm value. _Decision Sciences_, **36**(4), 531-567\.
*   <a id="san01" name="san01"></a>Sanchez, R. (2001). Managing knowledge into competences: the five learning cycles of the competent organization. In R. Sanchez (Ed.), _Knowledge management and_ _organizational competence_, (pp. 3-37). Oxford: Oxford University Press.
*   <a id="san04" name="san04"></a>Sanchez, R. (2004). _['Tacit knowledge' versus 'explicit knowledge' approaches to knowledge management practice.](http://www.webcitation.org/5crqpoj6l) _Copenhagen: Copenhagen Business School, Dept. of Industrial Economics and Strategy. (Working Paper No. 04-1). Retrieved 5 December, 2008 from http://www.knowledgeboard.com/download/3512/Tacit-vs-Explicit.pdf . (Archived by WebCite® at http://www.webcitation.org/5crqpoj6l)
*   <a id="san05" name="san05"></a>Sanchez, R. (2005). _[Knowledge management and organizational learning: fundamental concepts for theory and practice.](http://www.webcitation.org/5crrFLRpj)_ Lund, Sweden: Lund University, Institute of Economic Research. (Working Paper Series, 2005/3) Retrieved 5 December, 2008 from http://www.lri.lu.se/pdf/wp/2005-3.pdf (Archived by WebCite® at http://www.webcitation.org/5crrFLRpj)
*   <a id="sen98" name="sen98"></a>Sen, K. (1998). The use of franchising as a growth strategy by US restaurant franchisors. _Journal of Consumer Marketing_, **15**(4), 397-407.
*   <a id="stu91" name="stu91"></a>Stumpf, S. & Dunbar, R. (1991). The effects of personality type on choices made in strategic decision situations. _Decision Sciences_, **22**(5), 1047-1072.
*   <a id="tuu01" name="tuu01"></a>Tuunanen, M. & Hyrsky, K. (2001). Entrepreneurial paradoxes in business format franchising: an empirical survey of Finnish franchisee. _International Small Business Journal_, **19**(4), 47-62\.
*   <a id="wei82" name="wei82"></a>Weiss, D.S., Mendelsohn, G.A. & Feimer, N.R. (1982). Reply to the comments of Block and Ozer. _Journal of Personality and Social Psychology_, **42**, 1182-1189.
*   <a id="whe01" name="whe01"></a>Wheeler, P. (2001). The Myers-Briggs Type Indicator and applications to accounting education research. _Issues in Accounting Education_, **16**(1), 125-151.
*   <a id="whe04" name="whe04"></a>Wheeler, P., Hunton, J. & Bryant, S. (2004). Accounting information systems research opportunities using personality type theory and the Myers-Briggs Type Indicator. _Journal of Information Systems_, **18**(1), 1-19.
*   <a id="whi03" name="whi03"></a>White, J., Varadarajan, P. & Dacin, P.A. (2003). Market situation interpretation and response: the role of cognitive style, organizational culture and information use. _Journal of Marketing_, **67**(3), 63-79.
*   <a id="wit81" name="wit81"></a>Witkin, H.A. & Goodenough, D.R. (1981). Cognitive styles: essence and origins. New York, NY: International Universities Press.
*   <a id="vol98" name="vol98"></a>Volkema, R.J. & Gorman, R.H. (1998). The influence of cognitive-based group composition on decision-making process and outcome. _Journal of Management Studies,_ **35**(1), 105-123.
*   <a id="web04" name="web04"></a>Weber, P. & Schaper, M. (2004). Understanding the grey entrepreneur. _Journal of Enterprising Culture_, **12**(2), 147-164.
*   <a id="woo96" name="woo96"></a>Wood, F., Ford, N., Miller, D., Sobczyk, G. & Duffin, R. (1996) Information skills, searching behaviour and cognitive styles for student-centered learning: a computer-assisted learning approach, _Journal of Information Science_, **22**(2), 79-92.

