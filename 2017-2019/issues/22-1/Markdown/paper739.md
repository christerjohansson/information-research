<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# An investigation of the intellectual structure of opinion mining research

## [Yongjun Zhu, Meen Chul Kim](#author), and [Chaomei Chen](#author)

> **Introduction.** Opinion mining has been receiving increasing attention from a broad range of scientific communities since early 2000s. The present study aims to systematically investigate the intellectual structure of opinion mining research.  
> **Method.** Using topic search, citation expansion, and patent search, we collected 5,596 bibliographic records of opinion mining research. Then, intellectual landscapes, emerging trends, and recent developments were identified. We also captured domain-level citation trends, subject category assignment, keyword co-occurrence, document co-citation network, and landmark articles.  
> **Analysis.** Our study was guided by scientometric approaches implemented in CiteSpace, a visual analytic system based on networks of co-cited documents. We also employed a dual-map overlay technique to investigate epistemological characteristics of the domain.  
> **Results.** We found that the investigation of algorithmic and linguistic aspects of opinion mining has been of the community’s greatest interest to understand, quantify, and apply the sentiment orientation of texts. Recent thematic trends reveal that practical applications of opinion mining such as the prediction of market value and investigation of social aspects of product feedback have received increasing attention from the community.  
> **Conclusion.** Opinion mining is fast-growing and still developing, exploring the refinements of related techniques and applications in a variety of domains. We plan to apply the proposed analytics to more diverse domains and comprehensive publication materials to gain more generalized understanding of the true structure of a science.

<section>

## Introduction

Opinion mining refers to the task of finding opinions of people about specific entities ([Feldman, 2013](#fel13)). It employs computational linguistics and natural language processing to identify and extract subjective information from source texts. Sentiment analysis, which parallels opinion mining, aims to quantify and determine the polarity of an individual with regards to judgement, affective state, or emotion ([Pang and Lee, 2008](#pan08)). Sentiment analysis is used to automatically analyse evaluative texts and materials. Opinion mining and sentiment analysis are often seen as interchangeable concepts and express a mutual meaning ([Medhat, Hassan, and Korashy, 2014](#med14)). Pang and Lee ([2004](#pan04)) described the year of 2001 as the beginning of widespread awareness of opinion mining and sentiment analysis. Most of the early studies placed emphasis on proposing computational techniques to measure the sentiment orientation of product reviews in a binary way ([Thet, Na, and Khoo, 2010](#thet10)). Recent studies have been working on in-depth examination of source texts with a variety of techniques and units of analysis such as document, sentence, feature, or aspect. Within this context, these has been an exponentially increasing number of articles written on this topic (See Figure 2). Hundreds of startup companies are also developing sentiment analysis solutions and software packages ([Feldman, 2013](#fel13)).

As the volume of the literature has exponentially grown, a wide range of communities has also paid large attention to opinion mining. Thus, a systematic analysis of the intellectual structure of the field is needed to understand its main applications and current challenges. The systematic domain analysis will allow the followings. First, it helps the opinion mining community to be more self-explanatory as it has a detailed bibliometric profile. Secondly, researchers in the field can benefit from the systematic domain analysis by better positioning their research, identifying research trends, and expanding research territories. Finally, it guides researchers who are interested in the field to learn about emerging trends and current challenges. To our knowledge, however, there has been little concerted effort which aims to systematically understand the intellectual landscapes in opinion mining. The motivation of the present study lies in our intention to identify the intellectual structure of opinion mining and sentiment analysis research in a quantitative and systematic manner. We explored thematic patterns, landmark articles, emerging trends, and new developments of the field. In particular, our study was guided by a computational approach implemented in CiteSpace, a visual analytic system for illustrating emerging trends and critical changes in scientific literature ([Chen, 2006](#che06); [Chen _et al._, 2012](#che12)). We also employed a dual-map overlay technique ([Chen and Leydesdorff, 2014](#che14)) to expand the present study to the investigation of citation patterns at a disciplinary level .

In what follows, three research questions frame our investigation:

*   _Research question #1_: what are epistemological characteristics of opinion mining research?
*   _Research question #2_: which thematic patterns of research do occur in the domain?
*   _Research question #3_: what are emerging trends and recent developments in the field?

The rest of the study is organized as follows. First, we survey preceding work. Then, the scientometric methodology of the study is introduced. We describe intellectual structure of the field and key findings. This is followed by the conclusion and proposed future study.

## Related work

To date, there has been little research that has systematically investigated comprehensive intellectual landscapes and emerging research trends in opinion mining. Therefore, we examined review articles on opinion mining since review papers aim to survey and synthesize key findings and research trends in a specific domain of interest. Pang and Lee ([2008](#pan08)) presented a detailed review on broad aspects of opinion mining and sentiment analysis. This survey featured applications of opinion mining and sentiment analysis such as classification and extraction of textual units and summarization of sentiment information from a set of single and multi-documents. Based on the authors’ expertise on the field, a large number of articles were surveyed. A list of classifiers and their performance in grouping opinions were discussed by Govindarajan and Romina ([2013](#gov13)). They summarized the performance of naive Bayes, support vector machine, and genetic algorithm on a variety of data sources along with the selection of different features. Evolution of the field and its future research trends can be found at a work done by Cambria and colleagues ([2013](#cam13)). They reviewed 1) opinion mining techniques ranging from heuristics to discourse structure, 2) different levels of analysis ranging from document-level to aspect-level, and 3) four approaches regarding the identification of implicit information associated with word tokens such as keyword spotting, lexical affinity, statistical methods, and concept-based approaches. The authors considered the multi-modal sentiment analysis as one of the promising approaches, laying stress on the importance of investigating a variety of sources such as textual, acoustic, and video features. Feldman ([2013](#fel13)) discussed opinion mining techniques to solve five specific problems: 1) document-level sentiment analysis, 2) sentence-level sentiment analysis, 3) aspect-level sentiment analysis, 4) comparative sentiment analysis, and 5) sentiment lexicon acquisition. He introduced major applications of sentiment analysis such as customer review mining towards products and services, preference mining on candidates running for political positions, and market value prediction. Medhat and colleagues ([2014](#med14)) surveyed 54 research papers on algorithms and applications of sentiment analysis. They categorized these papers into six groups: 1) sentiment analysis, 2) sentiment classification, 3) feature selection, 4) emotion detection, 5) building resource, and 6) transfer learning. This paper showed that naive Bayes and support vector machine are the most frequently used techniques for grouping opinions. In addition, WordNet was said to be the most commonly used lexical source to solve sentiment analysis problems.

The review articles discussed above examined a variety of aspects of opinion mining research. However, there has been little concerted effort to investigate emerging trends and recent developments in opinion mining in such a comprehensive, systematic, and computationally driven manner that we use in the present study. In addition, one commonality of these papers is that the surveyed articles were selected based on prior domain knowledge or without specific selection criteria. While aggregated and/or brief discussions on individual papers were provided, the implication and importance of individual papers to the field were missing. This may have helped readers approach individual papers more systematically and selectively for a deeper understanding. The absence of clear thematic categories is also one possible limitation of these reviews. Therefore, these surveys lack objectivity and clarity in describing the intellectual structure and emerging trends in the field. Since opinion mining is a developing, fast-growing domain, a systematic and comprehensive investigation of intellectual landscapes and recent developments is essential. To bridge these gaps, the present study aimed to explore the intellectual structure of the field through a bibliometric analysis of extensive scientific literature, taking a variety of units of analysis into account. We also took a close look at landmark articles of the field that are chosen by multi-faceted criteria.

## Methods

In this section, we introduce the data collection method and analytical approaches of the present study. The research procedure is shown in Figure 1.

<figure>

![Figure 1: Research procedure](../p739fig1.png)

<figcaption>Figure 1: Research procedure</figcaption>

</figure>

### Data collection

The goal of the present study was to explore the intellectual structure of opinion mining research. We aimed to identify thematic patterns, research networks, emerging trends and new developments, together with landmark articles in the domain. Toward that end, we retrieved bibliographic records from the Web of Science, using a topic search. There were two problems that we encountered with this data collection approach per se. First, it is acknowledged that the topic search does not include relevant records if querying terms do not appear in the targeted fields such as titles, abstracts, and keywords. In addition, despite the fact that conference proceedings are a common document type in computer and information-related sciences, the Web of Science under-represents publications from conferences ([Kim and Chen, 2015](#kim15)). To remedy these issues, we employed a two-step complementary approach in data collection. First, we assumed that if an article cites at least one of the records retrieved by the topic search, then the article is topically relevant to opinion mining ([Chen, Hu, Liu, and Tseng, 2012](#che12)). As Garfield ([1979](#gar79)) argues, citation indexing is an alternative strategy to capture a much broader context of a study. Therefore, we collected additional records, using a citation expansion implemented on the Web of Science. The dataset obtained by the topic search was regarded as the core dataset and the expanded one represents a broader context of the core. Using the citation expansion supports our goal to explore much broader landmark references influencing emerging trends and recent developments in opinion mining. Second, we triangulated our data collection by conducting patent and citation searches on Derwent Innovations Index. This database indexes technological inventions in chemical, electrical, electronic, and mechanical engineering.

The detailed procedure of our data collection is as follows. First, a data collection method proposed by preceding scientometric research ([Chen, Dubin, and Kim, 2014a](#che14a), [b](#che14b); [Kim and Chen, 2015](#kim15); [Kim, Zhu, and Chen, 2016](#kim16)) was used. For this basic search, four query phrases identified as representative for the domain were used: _“opinion mining”_ OR _“sentiment anal*”_ OR _“subjectivity anal*”_ OR _“review mining”_. We employed the wildcard * to capture relevant variations of a word, including no character, such as sentiment analysing and sentiment analytics. The use of double quotation marks helped queries considered to be clauses. Then, a record was regarded as relevant if any of the terms is found in the title, abstract or keyword fields of the record. The queries resulted in 2,010 records from 2002 through 2015 as of August 31 2016 when considering articles, proceeding papers, and review articles as representative record types. Then, the core records were cited by 3,418 articles, proceedings, and review articles on the Web of Science. Finally, the query set returned 168 patent records between 2005 and 2015 from Derwent Innovations Index. We integrated all of these data sets and used the merged one for the present scientometric investigation. Table 1 describes the brief statistics of the dataset. Figure 2 depicts the number of records over time. As depicted in the figure, opinion mining has received exponentially increasing attention from the community. Table 2 describes the query terms used to search records and the number of corresponding records to each term. It shows _“sentiment anal*”_ contributes the literature most.

<table><caption>Table 1: Data statistics</caption>

<tbody>

<tr>

<th>Dataset</th>

<th>Duration</th>

<th>Results</th>

<th>Articles</th>

<th>Proceedings</th>

<th>Reviews</th>

<th>Authors</th>

<th>References</th>

<th>Keywords</th>

</tr>

<tr>

<td>Core</td>

<td>2002-2015</td>

<td>2,010</td>

<td>643</td>

<td>1,322</td>

<td>45</td>

<td>6,470</td>

<td>52,948</td>

<td>12,815</td>

</tr>

<tr>

<td>Expanded</td>

<td>2005-2016</td>

<td>3,418</td>

<td>2,170</td>

<td>1,222</td>

<td>126</td>

<td>11,354</td>

<td>150,503</td>

<td>31,393</td>

</tr>

<tr>

<td>Patent</td>

<td>2005-2015</td>

<td>168</td>

<td>-</td>

<td>-</td>

<td>-</td>

<td>577</td>

<td>936</td>

<td>1,047</td>

</tr>

</tbody>

</table>

<figure>

![Figure 2: Records distribution over time](../p739fig2.png)

<figcaption>Figure 2: Records distribution over time</figcaption>

</figure>

<table><caption>Table 2: Query terms and retrieved records (in descending order of the number of core records)</caption>

<tbody>

<tr>

<th>Query term</th>

<th>Core dataset</th>

<th>Expanded dataset</th>

<th>Patent dataset</th>

</tr>

<tr>

<td>sentiment anal*</td>

<td>1,615</td>

<td>2,972</td>

<td>149</td>

</tr>

<tr>

<td>opinion mining</td>

<td>767</td>

<td>1,591</td>

<td>17</td>

</tr>

<tr>

<td>subjectivity anal*</td>

<td>34</td>

<td>213</td>

<td>1</td>

</tr>

<tr>

<td>review mining</td>

<td>30</td>

<td>76</td>

<td>1</td>

</tr>

</tbody>

</table>

### Investigation of the intellectual landscapes, emerging trends, and new developments in opinion mining

Scientometrics is the quantitative study of science. We employed scientometric approaches to analyse bibliographic records for the present domain analysis. There are several advantages in our study in comparison with a conventional domain analysis as follows ([Chen, Dubin, and Kim, 2014b](#che14b)). Firstly, a much broader and more diverse range of relevant topics can be explored due to the use of citation expansion and patent records. Secondly, this kind of domain investigation can be conducted as frequently as needed, although an inquirer does not have prior expertise in a targeted domain. Finally, a scientometric analysis provides an additional point of reference.

CiteSpace, developed by Chen ([2006](#che06)), is a scientometric toolbox to generate and analyse networks of co-cited references using bibliographic records. The input is a collection of academic literature relevant to a specific topic. Given bibliographic records , this tool computationally detects and depicts thematic patterns and emerging trends in science. On top of this, CiteSpace provides a visual representation called a dual-map overlay which renders a domain-level view of the growth of the literature ([Chen and Leydesdorff, 2014](#che14)).

In a scientometric study, the intellectual landscapes of a scientific domain can be represented by a variety of networked entities such as cited references and keywords ([Chen, Dubin, and Kim, 2014b](#che14b)). Specifically, we focused on document citation networks and networks of co-occurring keywords to explore emerging trends and recent developments in opinion mining research. The key features of the present investigation include: 1) domain-level citation paths, 2) frequently assigned subject categories, 3) keyword co-occurrence, 4) networks of highly cited references, and 5) influential articles selected by a variety of metrics.

### Terminology

In order to clearly communicate the technical approaches and findings of the present study to the audience, we introduce structural measures and text mining techniques used in this paper as follows:

_g-index_: The _g_-index, suggested by Egghe ([2006](#egg06)), is an author-level metric to measure the scientific productivity of an individual. It considers the unique largest number of the top _g_ highly cited articles received together more than or equal to _g_ square citations. Examining the entire entities in our data, _i.e._ keywords and cited references, may be computationally challenging. It may not intuitively communicate the true structure of the domain to the audience as well. In addition, we argue that employing the _g_-index in selecting core entities is sounder than using the _h_-index since the _g_ number of cores is always at least as big as the _h_ cores. Thus, we used _g_-index to select a significant fraction of frequently occurring keywords and cited articles within a 1-year slice of time .

_Pathfinder networks_: Bibliographic networks can be highly dense with many links between entities. Network pruning or link reduction, the process in order to systematically remove excessive links, can address this issue. Based on the proximity of entity pairs, pathfinder networks capture the shortest paths, so links are eliminated when they are not on shortest paths ([Schvaneveldt, Durso, and Dearholt, 1989](#sch89)). In this study, we employed pathfinder networks to eliminate redundant links between entities of analysis.

_Betweenness centrality_: Betweenness centrality is an indicator of a node considering the number of shortest paths from all vertices to all others that pass through the node ([Brandes, 2001](#bra01)). A node with a high value of betweenness centrality has a large influence on the transfer of information through the network ([Chen, 2011](#che11)). If a node provides the only link between two large but previously unconnected groups of nodes, it would have a very high degree of betweenness centrality. In our study, we regarded this topological property as a significant sign of a bibliographic entity’s influence.

_Burstiness_: Kleinberg ([2002](#kle02)) proposed an algorithm called burst detection which captures the burstiness of events with certain features rising sharply in frequency. Based on this concept, an entity can be regarded as having bursting activities if it shows an intensive frequency of appearance during a specific duration of time. It overcomes the limitation of just considering the cumulative number of metrics such as citations as a measure of an entity’s impact.

_Sigma_: Sigma is a measure identifying scientific publications with topological burstiness. It is defined as (betweenness centrality + 1) to the power of burstiness such that the temporal brokerage mechanism plays more prominent role than the rate of raw citations ([Chen, _et al._, 2009](#che09)). We regarded this metric as another important sign of a bibliographic entity’s structural burst.

_Automatic cluster labelling_: In order to automatically label clusters of cited references, we extracted candidate terms from titles and abstracts of citing articles. In CiteSpace, these terms are selected by three different algorithms: 1) latent semantic indexing (LSI) ([Deerwester, _et al._, 1990](#dee90)), 2) log-likelihood ratio (LLR) ([Dunning, 1993](#dun93)), and 3) mutual information (MI). Labels extracted by LSI tend to capture implicit semantic relationships across records, whereas those chosen by LLR and MI tend to reflect a unique aspect of a cluster ([Chen, Ibekwe-SanJuan, and Hou, 2010](#che10)).

## Results

### Research trends at a disciplinary level

A dual-map overlay is an analytical approach that presents the domain-level concentration of citations through their reference paths ([Chen and Leydesdorff, 2014](#che14)). A base map depicts the interconnections of over 10,000 published journals and these journals are grouped into some regions that represent publications and citation activities at a domain level ([Chen, Dubin, and Kim, 2014a](#che14a)). The dual-map overlay of opinion mining research is displayed in Figure 3\. In the visual representation, the left clusters, _i.e._ research fronts, represent where the retrieved records publish, while the right clusters indicate where they cite. Regions are labelled by common terms found in the underlying journals. Citation trajectories are distinguished by citing regions’ colours. The thickness of these trajectories is proportional to the z-score-scaled frequency of citations. Based on this map, we can identify patterns of how published articles in opinion mining refer to other intellectual bases (cited references). As rendered in the figure, there are four main citation paths in our dataset. Table 3 summarizes these paths with citing and cited region names.

<figure>

![Figure 3: Domain-level citation patterns in opinion mining research](../p739fig3.png)

<figcaption>Figure 3: Domain-level citation patterns in opinion mining research</figcaption>

</figure>

<table><caption>Table 3: Citation trends at a domain level</caption>

<tbody>

<tr>

<th>Citing region</th>

<th>Cited region</th>

<th>z-score</th>

</tr>

<tr>

<td>psychology, education, health</td>

<td>psychology, education, social</td>

<td>7.729</td>

</tr>

<tr>

<td>mathematics, systems, mathematical</td>

<td>systems, computing, computer</td>

<td>5.803</td>

</tr>

<tr>

<td>psychology, education, health</td>

<td>systems, computing, computer</td>

<td>2.727</td>

</tr>

<tr>

<td>mathematics, systems, mathematical</td>

<td>psychology, education, social</td>

<td>2.468</td>

</tr>

</tbody>

</table>

The relationships are sorted by the z-scores in descending order where the values are rounded to the nearest thousandth. Each row is identified by the same colour of the corresponding path displayed in Figure 3\. As described in Table 3, the domains the most frequently covering the records are: 1) _6\. psychology, education, health_, and 2) _1\. mathematics, systems, mathematical_. Then, this literature is mostly influenced by _7\. psychology, education, social_ and _1\. systems, computing, computer_. On top of these domains, _5\. physics, materials, chemistry_ (rendered in purple lines), _2\. molecular, biology, immunology_ (depicted in yellow lines), and _4\. medicine, medical, clinical_ (illustrated in green lines) contribute to the domain-level citation trends in opinion mining. It indicates a multidisciplinary aspect of opinion mining since publications from multiple domains contribute to the citation landscape of the domain. It also shows that social sciences study opinion mining and sentiment analysis within similar disciplinary contexts while the mathematical and algorithmic concepts of the domain are mostly influenced by the literature that investigates systems and computer (See the first and second rows of Table 3). Based on these observations, we argue that opinion mining research is partially multidisciplinary and partially monodisciplinary. We also identify an interdisciplinary characteristic as described at the third and fourth rows of Table 3\. It is shown that psychological, educational, and biomedical investigations in opinion mining are based on systems perspectives of computing and vice versa.

A subject category of an article can also be regarded as evidence showing an upper-level thematic concentration of the article. In order to specify the findings above, we examined the Web of Science subject categories assigned to the records. Table 4 describes 20 subject categories most frequently given to the dataset. The table shows each category’s year of first occurrence, cumulative frequency, and betweenness centrality in descending order of the assignment frequency (third column). As described in the table, computer science, engineering, linguistics, and social sciences such as library and information science, business, economics, management, and psychology are among the leading subject categories in opinion mining. Among them, computer science, engineering, and their related fields have been assigned to the records most frequently. In addition, the category, interdisciplinary applications of computer science, shows to have the highest betweenness centrality of 0.21\. It indicates that interdisciplinary applications of computer science have had the largest influence on the emergence, development, and diffusion of the ideas in opinion mining. In turn, psychology plays an important bridging role between domains that participate in opinion mining research (betweenness centrality: 0.20). It is also obvious that the publications from electrical engineering, artificial intelligence, linguistics, and library and information science have transferred scientific findings to opinion mining. Recently, opinion mining has received significant attention from operations research and management. It is also interesting to see that business and economics have published literature from the very early years of the domain. This may indicate that opinion mining started with an aim of understanding and scaling customers’ subjective opinions toward products. Based on these observations, we assume that the researchers in the domain began by investigating algorithmic, linguistic, and psychological aspects of opinion mining in order to understand, quantify, and measure the sentimental orientation of texts. Successive literature may have explored the improvement and practical applications of the techniques to multiple domains. In the next subsections, we try to understand these findings deeper at the keyword and reference levels.

<table><caption>Table 4: Top 20 frequently assigned subject categories in the dataset</caption>

<tbody>

<tr>

<th>Category</th>

<th>First occurrence</th>

<th>Frequency</th>

<th>Centrality</th>

</tr>

<tr>

<td>cs</td>

<td>2003</td>

<td>3,453</td>

<td>0.03</td>

</tr>

<tr>

<td>cs, artificial intelligence</td>

<td>2003</td>

<td>1,769</td>

<td>0.10</td>

</tr>

<tr>

<td>cs, information systems</td>

<td>2005</td>

<td>1,671</td>

<td>0.03</td>

</tr>

<tr>

<td>cs, theory & methods</td>

<td>2007</td>

<td>1,303</td>

<td>0.03</td>

</tr>

<tr>

<td>engineering</td>

<td>2007</td>

<td>1,029</td>

<td>0.03</td>

</tr>

<tr>

<td>engineering, electrical</td>

<td>2007</td>

<td>888</td>

<td>0.14</td>

</tr>

<tr>

<td>cs, interdisciplinary apps.</td>

<td>2004</td>

<td>423</td>

<td>0.21</td>

</tr>

<tr>

<td>info. sci. & lib. sci.</td>

<td>2005</td>

<td>348</td>

<td>0.07</td>

</tr>

<tr>

<td>business & economics</td>

<td>2002</td>

<td>332</td>

<td>0.02</td>

</tr>

<tr>

<td>ops. res. & mgmt. sci.</td>

<td>2008</td>

<td>327</td>

<td>0.03</td>

</tr>

<tr>

<td>cs, software engineering</td>

<td>2006</td>

<td>326</td>

<td>0.01</td>

</tr>

<tr>

<td>linguistics</td>

<td>2004</td>

<td>251</td>

<td>0.08</td>

</tr>

<tr>

<td>language & linguistics</td>

<td>2004</td>

<td>227</td>

<td>0.02</td>

</tr>

<tr>

<td>business</td>

<td>2006</td>

<td>188</td>

<td>0.06</td>

</tr>

<tr>

<td>telecommunications</td>

<td>2008</td>

<td>182</td>

<td>0.06</td>

</tr>

<tr>

<td>cs, hardware & architecture</td>

<td>2007</td>

<td>182</td>

<td>0.01</td>

</tr>

<tr>

<td>management</td>

<td>2006</td>

<td>177</td>

<td>0.04</td>

</tr>

<tr>

<td>robotics</td>

<td>2008</td>

<td>145</td>

<td>0.00</td>

</tr>

<tr>

<td>cs, cybernetics</td>

<td>2006</td>

<td>108</td>

<td>0.04</td>

</tr>

<tr>

<td>psychology</td>

<td>2006</td>

<td>106</td>

<td>0.20</td>

</tr>

</tbody>

</table>

### Keywords as indicators of emerging trends and new developments

The investigation of keywords can add richer interpretations to understanding the concentration of research themes since keywords represent underlying concepts of an article. Table 5 describes 20 keywords the most frequently given by authors and indexers to the records. It shows each keyword’s year of first occurrence, cumulative frequency, and betweenness centrality. Based on its frequency of appearance and centrality, it is evident that _sentiment analysis_ is the keyword considered as the most representative of the literature from the early years of the domain. This keyword is followed by _opinion mining_ which is the application of sentiment analysis to a variety of textual materials such as _web_ and _review_. Following _sentiment analysis_ (betweenness centrality: 0.20), _emotion_ is also located on the shortest path connecting pairs of other concepts in opinion mining research (betweenness centrality: 0.19). It indicates that the identification and extraction of subjective information in source texts have been regarded as the most important concepts in opinion mining and had the largest influence on the growth of the domain. Technique-wise, _text mining_, _machine learning_, and _natural language processing_ have been frequently employed to quantify sentiment analysis. Classification is among the most frequently investigated techniques, also being a goal of understanding the sentiment orientation of text. As reflected in the recent keywords such as _social media_, _twitter_, _social network_, and _word of mouth_, the literature now focuses on the practical applications of opinion mining to marketing and social networking platforms.

<table><caption>Table 5: Top 20 frequently occurring keywords in the dataset</caption>

<tbody>

<tr>

<th>Keyword</th>

<th>First occurrence</th>

<th>Frequency</th>

<th>Centrality</th>

</tr>

<tr>

<td>sentiment analysis</td>

<td>2006</td>

<td>1,458</td>

<td>0.20</td>

</tr>

<tr>

<td>opinion mining</td>

<td>2006</td>

<td>721</td>

<td>0.09</td>

</tr>

<tr>

<td>social media</td>

<td>2011</td>

<td>378</td>

<td>0.07</td>

</tr>

<tr>

<td>twitter</td>

<td>2011</td>

<td>340</td>

<td>0.06</td>

</tr>

<tr>

<td>classification</td>

<td>2009</td>

<td>303</td>

<td>0.07</td>

</tr>

<tr>

<td>text mining</td>

<td>2007</td>

<td>246</td>

<td>0.05</td>

</tr>

<tr>

<td>model</td>

<td>2010</td>

<td>242</td>

<td>0.03</td>

</tr>

<tr>

<td>social network</td>

<td>2009</td>

<td>207</td>

<td>0.03</td>

</tr>

<tr>

<td>machine learning</td>

<td>2006</td>

<td>194</td>

<td>0.07</td>

</tr>

<tr>

<td>emotion</td>

<td>2005</td>

<td>184</td>

<td>0.19</td>

</tr>

<tr>

<td>word of mouth</td>

<td>2011</td>

<td>182</td>

<td>0.07</td>

</tr>

<tr>

<td>web</td>

<td>2010</td>

<td>179</td>

<td>0.02</td>

</tr>

<tr>

<td>text</td>

<td>2009</td>

<td>178</td>

<td>0.11</td>

</tr>

<tr>

<td>information</td>

<td>2010</td>

<td>174</td>

<td>0.02</td>

</tr>

<tr>

<td>system</td>

<td>2009</td>

<td>165</td>

<td>0.03</td>

</tr>

<tr>

<td>network</td>

<td>2010</td>

<td>159</td>

<td>0.03</td>

</tr>

<tr>

<td>natural language processing</td>

<td>2009</td>

<td>154</td>

<td>0.04</td>

</tr>

<tr>

<td>review</td>

<td>2011</td>

<td>153</td>

<td>0.01</td>

</tr>

<tr>

<td>sentiment classification</td>

<td>2006</td>

<td>145</td>

<td>0.09</td>

</tr>

<tr>

<td>internet</td>

<td>2007</td>

<td>132</td>

<td>0.04</td>

</tr>

</tbody>

</table>

Figure 4 (below) displays the co-occurrence network of these keywords. For the clarity of the figure, we only labelled keywords listed in Table 5\. The size of a node is proportional to its frequency and each node is coloured by community membership ([Blondel, Guillaume, Lambiotte, and Lefebvre, 2008](#blo08)). Finally, the more frequently keywords co-occur, the more closely they are located. As depicted in the figure, the formulation of the network is consistent with the findings above.

<figure>

![Figure 4: Keyword co-occurrence network](../p739fig4.png)

<figcaption>Figure 4: Keyword co-occurrence network</figcaption>

</figure>

Table 6 (below) describes 20 keywords receiving the most intensive attention during a specific span of time. We sorted the keywords by the beginning years of the bursts so that we could identify temporal patterns. _Opinion mining_ and _sentiment analysis_ are among the most intensively bursting keywords across the entire time duration. _Sentiment classification_ has been one of the most important goals from the earlier years. _Opinion extraction_ is the task of automatically extracting structured opinions from unstructured data such as text. For _sentiment classification_ and _opinion extraction_, _machine learning_ and _text mining_ have been employed from the community. In the previous section of examining trends in subject category assignment, we assumed that literature in an earlier stage may have explored linguistic aspects sentiment orientation. The burstiness of the keyword _language_ complements this assumption. A variety of techniques to extract sentiment and to apply quantified subjectivity have shown bursting trends with _language_ (See _information retrieval_ and _information extraction_). Then, the automatic collection of a _corpus_ from consumer contributed content such as _blog_ has had large attention and _natural language processing_ has been intensively employed to this end. Recently, there has been growing interest in applying _data mining_ and _sentic computing_ to a variety of textual genres such as _web_ and _microblogging_. Thus, investigating the burstiness of keywords adds richer interpretations to the understanding of the emerging trends in opinion mining than just considering the cumulative number of keyword occurrence.

<table><caption>

**Table 6: Top 20 bursting keywords in the dataset (bursts rounded to the nearest thousandth)**</caption>

<tbody>

<tr>

<th>Keyword</th>

<th>Burst</th>

<th>Begin</th>

<th>End</th>

<th>2002-2015</th>

</tr>

<tr>

<td>opinion mining</td>

<td>52.860</td>

<td>2006</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂<span style="color:red">▃▃▃▃▃▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>sentiment analysis</td>

<td>8.549</td>

<td>2006</td>

<td>2011</td>

<td><span style="color:#42ebf4">▂▂▂▂<span style="color:red">▃▃▃▃▃▃</span>▂▂▂▂</span></td>

</tr>

<tr>

<td>sentiment classification</td>

<td>7.197</td>

<td>2006</td>

<td>2011</td>

<td><span style="color:#42ebf4">▂▂▂▂<span style="color:red">▃▃▃▃▃▃</span>▂▂▂▂</span></td>

</tr>

<tr>

<td>machine learning</td>

<td>4.859</td>

<td>2006</td>

<td>2009</td>

<td><span style="color:#42ebf4">▂▂▂▂<span style="color:red">▃▃▃▃</span>▂▂▂▂▂▂</span></td>

</tr>

<tr>

<td>text mining</td>

<td>8.486</td>

<td>2007</td>

<td>2011</td>

<td><span style="color:#42ebf4">▂▂▂▂▂<span style="color:red">▃▃▃▃▃</span>▂▂▂▂</span></td>

</tr>

<tr>

<td>opinion extraction</td>

<td>3.621</td>

<td>2007</td>

<td>2011</td>

<td><span style="color:#42ebf4">▂▂▂▂▂<span style="color:red">▃▃▃▃▃</span>▂▂▂▂</span></td>

</tr>

<tr>

<td>language</td>

<td>9.148</td>

<td>2008</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂<span style="color:red">▃▃▃▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>information retrieval</td>

<td>4.798</td>

<td>2008</td>

<td>2011</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂<span style="color:red">▃▃▃▃</span>▂▂▂▂</span></td>

</tr>

<tr>

<td>information extraction</td>

<td>3.619</td>

<td>2008</td>

<td>2010</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂<span style="color:red">▃▃▃</span>▂▂▂▂▂</span></td>

</tr>

<tr>

<td>corpus</td>

<td>2.729</td>

<td>2009</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂<span style="color:red">▃▃▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>blog</td>

<td>5.115</td>

<td>2009</td>

<td>2013</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂<span style="color:red">▃▃▃▃▃</span>▂▂</span></td>

</tr>

<tr>

<td>natural language processing</td>

<td>3.434</td>

<td>2009</td>

<td>2010</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂<span style="color:red">▃▃</span>▂▂▂▂▂</span></td>

</tr>

<tr>

<td>opinion analysis</td>

<td>3.684</td>

<td>2009</td>

<td>2011</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂<span style="color:red">▃▃▃</span>▂▂▂▂</span></td>

</tr>

<tr>

<td>data mining</td>

<td>3.700</td>

<td>2010</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂<span style="color:red">▃▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>document</td>

<td>3.796</td>

<td>2010</td>

<td>2013</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂<span style="color:red">▃▃▃▃</span>▂▂</span></td>

</tr>

<tr>

<td>sentiment analysis</td>

<td>7.981</td>

<td>2010</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂<span style="color:red">▃▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>web</td>

<td>7.511</td>

<td>2010</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂<span style="color:red">▃▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>text analysis</td>

<td>3.220</td>

<td>2011</td>

<td>2013</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂▂<span style="color:red">▃▃▃</span>▂▂</span></td>

</tr>

<tr>

<td>microblogging</td>

<td>4.547</td>

<td>2011</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂▂<span style="color:red">▃▃</span>▂▂▂</span></td>

</tr>

<tr>

<td>sentic computing</td>

<td>3.922</td>

<td>2011</td>

<td>2012</td>

<td><span style="color:#42ebf4">▂▂▂▂▂▂▂▂▂<span style="color:red">▃▃</span>▂▂▂</span></td>

</tr>

</tbody>

</table>

### Document co-citation network

Figure 5 (below) visualises the document co-citation network in the dataset. The network consists of _g_ highly cited references within a given slice of time. Each node represents a cited article and the size of a node is proportional to its cited frequency. References with citation bursts are rendered with rings in red whereas nodes with purple rings have high betweenness centrality values. Landmark articles cited more than or equal to 133 times are labelled in black. Nodes are grouped in same lines by a clustering technique called smart local moving ([Waltman and van Eck, 2013](#wal13)). Clusters are numbered in such a way that higher rankings are given to the clusters containing more references. The colour legend at the top indicates that links and citations in cooler colours happen closely to the year of 2002 whereas hotter ones occur in close years to 2015\. Referring to the legend, we can keep track of the thematic trends and temporal developments in opinion mining research.

<figure>

![Figure 5: Timeline visualization of the network (labelled by LSA)](../p739fig5.png)

<figcaption>Figure 5: Timeline visualization of the network (labelled by LSI)</figcaption>

</figure>

Considering both the size and recency of member nodes, we consider clusters #0 through #4 as representing emerging research themes in the domain. Table 7 summarizes these clusters in terms of number of member articles, three types of labels extracted from titles and abstracts, and average published year of citees. Among these clusters, cluster #0 is the largest and oldest one in terms of the cluster size and the mean year of cited references. The references in this cluster influenced successive research applying sentiment analysis to the investigations of social media and user requirements. It is interesting that even though social media is one of the recently appearing keywords (See Table 5), relatively old papers influenced research under this theme. Cluster #1 is the second largest and second oldest one. Literature citing references in this group focuses on mining opinion words and analysing customer preferences. Cluster #2, one of the most recent ones, consists of references which influenced successive research on developing a fuzzy system for the extraction of affective information. Cluster #3 includes references affecting later literature on understanding newspaper readers’ opinions and sharing behaviour on social media. Cluster #4 studies the application of sentiment analysis to comparing specific products. It coheres with the findings from examining subject categories as reflected in _management_ and _business_ (See Table 4) and keyword assignment (See _word of mouth_ in Table 5).

<table><caption>Table 7: Emerging clusters in the dataset</caption>

<tbody>

<tr>

<th rowspan="2">Cluster</th>

<th rowspan="2">Size</th>

<th colspan="3">Cluster labelling technique</th>

<th rowspan="2">Mean year</th>

</tr>

<tr>

<th>LSI</th>

<th>Log-likelihood ratio</th>

<th>Mutual information</th>

</tr>

<tr>

<td>0</td>

<td>44</td>

<td>sentiment analysis | key issues</td>

<td>social media</td>

<td>user requirement</td>

<td>2004</td>

</tr>

<tr>

<td>1</td>

<td>37</td>

<td>Pessimists | opinion mining</td>

<td>opinion word</td>

<td>customer preferences analysis</td>

<td>2008</td>

</tr>

<tr>

<td>2</td>

<td>35</td>

<td>sentiment analysis | explicit sentiment analysis</td>

<td>affective information</td>

<td>fuzzy system</td>

<td>2011</td>

</tr>

<tr>

<td>3</td>

<td>35</td>

<td>critical framing | new york times readers opinions</td>

<td>social media</td>

<td>sharing behavior</td>

<td>2011</td>

</tr>

<tr>

<td>4</td>

<td>26</td>

<td>comparison | german</td>

<td>electronic cigarette</td>

<td>comprehensive empirical comparison</td>

<td>2010</td>

</tr>

</tbody>

</table>

### Landmark literature

Emerging trends and recent developments in a domain can be investigated in detail by surveying influential manuscripts of the discipline. For measuring the importance of an article, we considered a variety of indicators such as cumulative citation counts, intensity of citations during a specific span of time, topological importance, and burstiness of structural importance. Tables 8, 9, 10, and 11 show landmark articles identified by these criteria. Landmark articles are extracted from cited references. In each table, manuscripts are coloured as follows: 1) green: articles that appear three times across all the tables, 2) yellow: articles that appear in two tables, and 3) white: articles that only appear in one table. In reviewing them, we chose only the articles that appear at least twice across the tables. In addition, some articles are not considered for this discussion if they are review papers, books, and book chapters. Topically irrelevant articles are also omitted. We argue that they do not influence thematic patterns and emerging trends in a domain. Eight papers selected in this way are summarized in Table 12\. In the following paragraphs, we discuss each of eight papers in chronological order.

<table><caption>Table 8: Top 10 highly cited articles in the dataset</caption>

<tbody>

<tr>

<th>Citation count</th>

<th>Reference</th>

<th>Cluster #</th>

</tr>

<tr>

<td>888</td>

<td>

Pang and Lee ([2008](#pan08))</td>

<td>5</td>

</tr>

<tr>

<td>257</td>

<td>

Taboada, _et al._ ([2011](#tab11))</td>

<td>5</td>

</tr>

<tr>

<td>256</td>

<td>

Liu ([2010](#liu10))</td>

<td>5</td>

</tr>

<tr>

<td>190</td>

<td>

Bollen, Mao, and Zeng ([2011](#bol11))</td>

<td>2</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">174</td>

<td style="background-color:#DCFF7F;">

Thelwall, _et al._ ([2010](#the10))</td>

<td style="background-color:#DCFF7F;">3</td>

</tr>

<tr>

<td>166</td>

<td>

Ding, Liu, and Yu ([2008](#din08))</td>

<td>1</td>

</tr>

<tr>

<td>147</td>

<td>

Abbasi, Chen, and Salem ([2008](#abb08))</td>

<td>9</td>

</tr>

<tr>

<td>144</td>

<td>

Wilson, Wiebe, and Hoffmann ([2009](#wil09))</td>

<td>0</td>

</tr>

<tr>

<td>142</td>

<td>

Liu ([2012](#liu12))</td>

<td>7</td>

</tr>

<tr>

<td>133</td>

<td>

Cambria, Schuller, Xia, and Havasi ([2013](#cam13))</td>

<td>1</td>

</tr>

</tbody>

</table>

<table><caption>Table 9: Top 10 articles with the highest citation bursts</caption>

<tbody>

<tr>

<th>Burst</th>

<th>Reference</th>

<th>Cluster #</th>

</tr>

<tr>

<td style="background-color:#DCFF7F;">51.24</td>

<td style="background-color:#DCFF7F;">

Turney ([2002](#tur02))</td>

<td style="background-color:#DCFF7F;">11</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">47.15</td>

<td style="background-color:#DCFF7F;">

Pang, Lee, and Vaithyanathan ([2002](#pan02))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">46.48</td>

<td style="background-color:#DCFF7F;">

Turney and Littman ([2003](#tur03))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td>35.94</td>

<td>

Wiebe, _et al._ ([2004](#wie04))</td>

<td>0</td>

</tr>

<tr>

<td>34.39</td>

<td>

Pang and Lee ([2004](#pan04))</td>

<td>0</td>

</tr>

<tr>

<td>28.45</td>

<td>

Dave, Lawrence, and Pennock ([2003](#dav03))</td>

<td>10</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">22.54</td>

<td style="background-color:#DCFF7F;">

Kim and Hovy ([2004](#kim04))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td>21.49</td>

<td>

Wilson ([2005](#wil05))</td>

<td>0</td>

</tr>

<tr>

<td style="background-color:#96FFB7">20.18</td>

<td style="background-color:#96FFB7">

Liu, Hu, and Cheng ([2005](#liu05))</td>

<td style="background-color:#96FFB7">1</td>

</tr>

<tr>

<td style="background-color:#96FFB7">13.27</td>

<td style="background-color:#96FFB7">

Pang and Lee ([2005](#pan05))</td>

<td style="background-color:#96FFB7">10</td>

</tr>

</tbody>

</table>

<table><caption>Table 10: Top 10 articles with the highest betweenness centrality values</caption>

<tbody>

<tr>

<th>Centrality</th>

<th>Reference</th>

<th>Cluster #</th>

</tr>

<tr>

<td>0.14</td>

<td>

Jindal and Liu ([2008](#jin08))</td>

<td>10</td>

</tr>

<tr>

<td style="background-color:#96FFB7">0.12</td>

<td style="background-color:#96FFB7">

Pang and Lee ([2005](#pan05))</td>

<td style="background-color:#96FFB7">10</td>

</tr>

<tr>

<td>0.12</td>

<td>

Chevalier and Mayzlin ([2006](#chev06))</td>

<td>6</td>

</tr>

<tr>

<td>0.12</td>

<td>

Tang, Tan, and Cheng ([2009](#tan09))</td>

<td>5</td>

</tr>

<tr>

<td style="background-color:#96FFB7">0.11</td>

<td style="background-color:#96FFB7">

Liu, Hu, and Cheng ([2005](#liu05))</td>

<td style="background-color:#96FFB7">1</td>

</tr>

<tr>

<td>0.10</td>

<td>

Kanayama and Nasukawa ([2006](#kan06))</td>

<td>1</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">0.10</td>

<td style="background-color:#DCFF7F;">

Thelwall, _et al._ ([2010](#the10))</td>

<td style="background-color:#DCFF7F;">3</td>

</tr>

<tr>

<td>0.10</td>

<td>

Qiu, Liu, Bu, and Chen ([2011](#qiu11))</td>

<td>1</td>

</tr>

<tr>

<td>0.09</td>

<td>

Wilson ([2005](#wil05))</td>

<td>0</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">0.09</td>

<td style="background-color:#DCFF7F;">

Riloff, Wiebe, and Wilson ([2003](#ril03))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

</tbody>

</table>

<table><caption>Table 11: Top 10 articles with the highest sigma</caption>

<tbody>

<tr>

<th>Sigma</th>

<th>Reference</th>

<th>Cluster #</th>

</tr>

<tr>

<td style="background-color:#DCFF7F;">26.48</td>

<td style="background-color:#DCFF7F;">

Pang, Lee, and Vaithyanathan ([2002](#pan02))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">18.09</td>

<td style="background-color:#DCFF7F;">

Turney ([2002](#tur02))</td>

<td style="background-color:#DCFF7F;">11</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">14.54</td>

<td style="background-color:#DCFF7F;">

Turney and Littman ([2003](#tur03))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td style="background-color:#96FFB7">7.62</td>

<td style="background-color:#96FFB7">

Liu, Hu, and Cheng ([2005](#liu05))</td>

<td style="background-color:#96FFB7">1</td>

</tr>

<tr>

<td>6.12</td>

<td>

Wilson ([2005](#wil05) )</td>

<td>0</td>

</tr>

<tr>

<td>4.41</td>

<td>

Chevalier and Mayzlin ([2006](#chev06))</td>

<td>6</td>

</tr>

<tr>

<td style="background-color:#96FFB7">4.32</td>

<td style="background-color:#96FFB7">

Pang and Lee ([2005](#pan05))</td>

<td style="background-color:#96FFB7">10</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">4.21</td>

<td style="background-color:#DCFF7F;">

Kim and Hovy ([2004](#kim04))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">3.90</td>

<td style="background-color:#DCFF7F;">

Riloff, Wiebe, and Wilson ([2003](#ril03))</td>

<td style="background-color:#DCFF7F;">0</td>

</tr>

<tr>

<td>2.63</td>

<td>

Yu and Hatzivassiloglou ([2003](#yu03))</td>

<td>0</td>

</tr>

</tbody>

</table>

<table><caption>Table 12: Select landmark articles in the dataset</caption>

<tbody>

<tr>

<th>Reference</th>

<th>Cluster #</th>

<th>Citation</th>

<th>Burst</th>

<th>Centrality</th>

<th>Sigma</th>

</tr>

<tr>

<td style="background-color:#96FFB7">

Liu, Hu, and Cheng ([2005](#liu05))</td>

<td style="background-color:#96FFB7">1</td>

<td style="background-color:#96FFB7"></td>

<td style="background-color:#96FFB7">X</td>

<td style="background-color:#96FFB7">X</td>

<td style="background-color:#96FFB7">X</td>

</tr>

<tr>

<td style="background-color:#96FFB7">

Pang and Lee ([2005](#pan05))</td>

<td style="background-color:#96FFB7">10</td>

<td style="background-color:#96FFB7"></td>

<td style="background-color:#96FFB7">X</td>

<td style="background-color:#96FFB7">X</td>

<td style="background-color:#96FFB7">X</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">

Turney ([2002](#tur02))</td>

<td style="background-color:#DCFF7F;">11</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">

Pang, Lee, and Vaithyanathan ([2002](#pan02))</td>

<td style="background-color:#DCFF7F;">0</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">

Turney and Littman ([2003](#tur03))</td>

<td style="background-color:#DCFF7F;">0</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">

Riloff, Wiebe, and Wilson ([2003](#ril03))</td>

<td style="background-color:#DCFF7F;">0</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;">X</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">

Kim and Hovy ([2004](#kim04))</td>

<td style="background-color:#DCFF7F;">0</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

</tr>

<tr>

<td style="background-color:#DCFF7F;">

Thelwall, _et al._ ([2010](#the10))</td>

<td style="background-color:#DCFF7F;">3</td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;"></td>

<td style="background-color:#DCFF7F;">X</td>

<td style="background-color:#DCFF7F;"></td>

</tr>

</tbody>

</table>

Turney ([2002](#tur02)) proposed an unsupervised learning algorithm using mutual information to classify reviews into positive and negative groups. The method calculates the semantic orientation of a phrase, considering mutual information between the phrase and the sentiment groups, _excellent_ and _poor_. The semantic orientation of a review is then classified compared to the average semantic orientation of included phrases. The proposed technique achieved an average accuracy of 74%, given a dataset consisting of documents on automobiles, banks, movies, and travels. Even though the method is simple and straightforward, the performance of the method needs to be further validated, due to the limited size of the dataset.

Pang, Lee, and Vaithyanathan ([2002](#pan02)) tested the performance of three machine learning algorithms: Naïve Bayes, maximum entropy classification, and support vector machine on the sentiment classification of movie reviews. In testing these algorithms, they employed different feature sets such as unigrams, bigrams, and parts of speech. The support vector machine based on unigrams achieved the best performance, which reached 82.9%. The contribution of the work lies in its broad investigation of different features in classifying peoples’ opinions. The authors laid stress on the importance of identifying plausible features that screen whether sentences are on-topic. The application of these approaches to other domains might be helpful for strengthening the generalizability of the study.

Turney and Littman ([2003](#tur03)) introduced a method that infers the semantic orientation of words based on their statistical associations with other positive and negative words. Two co-occurrence based measures, pointwise mutual information and latent semantic analysis, were employed in the study. The evaluation showed an accuracy of 82% and it could be raised up to 95% when applied to mild words. The proposed approach can be applied to a variety of parts of speech such as adjectives, adverbs, nouns, and verbs. The limitation of the method is that it requires a large size of corpora to calculate significant statistical associations among words to achieve better performance.

Riloff, Wiebe, and Wilson ([2003](#ril03)) proposed a system that classifies subjective and object sentences. The system uses extraction patterns to learn subjective nouns using bootstrapping algorithms. Then, a Naïve Bayes classifier was trained using the identified subjective nouns and other features such as discourse features and subjective clues. Results showed that the system achieved 77% recall and 81% precision. A contribution of the work is the use of bootstrapping methods for the identification of extraction patterns.

Kim and Hovy ([2004](#kim04)) proposed a system that determines the sentiment of opinions by examining people who hold opinions toward a given a topic. The system has modules for determining sentiments of words as well as sentences that contain those words. To achieve this, they started with selecting less than 100 seed words (both positive and negative) manually and expanded the list by extracting additional words from WordNet, and finally obtained 20,000 words with the polarity information. They proposed three models with different ways of combing sentiments of individual words: a model that only considers polarities, a model that uses the harmonic mean of sentiment strengths, and a model that uses the geometric mean of sentiment strengths. Evaluation showed that the highest score the system achieved was 81% accuracy.

Liu, Hu, and Cheng ([2005](#liu05)) proposed a system that analyses online customers’ reviews toward products. The system supports a feature-by-feature comparison of customer opinions. A language pattern mining-based approach was proposed for the extraction of product features. This system provides a visual comparison of consumer opinions on competing products. A contribution of this work is the proposal of a supervised pattern discovery method that supports the automated identification of products features from Pros and Cons.

Pang and Lee ([2005](#pan05)) proposed a meta-algorithm for rating-inference problem that take multi-point scale of rating into account. Instead of classifying a review as either positive or negative, they viewed the problem as a multi-category classification. They compared the performance of three approaches: multiclass SVM (OVA), regression, and metric labelling on the task. While there was no single approach that performed best in every cases, each approached showed its own strength in different tasks.

Thelwall and colleagues ([2010](#the10)) proposed a novel algorithm to extract sentiment strength from informal English text. They employed a machine learning approach to optimize sentiment term weightings. The authors extracted sentiment information from texts of nonstandard spelling. An evaluation was performed on MySpace comments and results showed that the proposed method achieved 60% and 72% accuracy in predicting positive and negative emotion respectively.

The literature discussed here has examined how to define and quantify the semantic orientation of texts, using lexicon- and/or machine learning-based approaches. Lexicon-based approaches examine words or phrases in a document and use metrics such as mutual information to judge their semantic orientation. Machine learning-based techniques employ widely accepted algorithms such as Naive Bayes and support vector machines. These techniques train classifiers with labelled sentiment. On top of this, classification was among the most frequently employed techniques. In general, lexicon-based techniques show lower performance but higher applicability, _i.e._ can be applied to many domains, while machine learning-based techniques outperform, but are only applicable to limited domains. These approaches have been applied to a variety of units of analysis, ranging from individual words to complete reviews that are comprised of multiple sentences.

To sum up, most of the landmark papers focused on the computation and grouping of sentiment orientation. Even though often context-specific, they provide practical implications towards the understanding and applications of sentiment analysis. These findings are also consistent with the findings in previous sections.

## Discussion

### Research question #1: what are epistemological characteristics of opinion mining research?

The investigation at the domain level reveals the following epistemological characteristics of opinion mining research. First, most of the studies came from two domain groups, _psychology, education, health_ and _mathematics, systems, mathematical_, and each of these disciplinary groups cites research within similar fields, _psychology, education, social_ and _systems, computing, computer_, respectively. The literature from other domains such as _physics, materials, chemistry, molecular, biology, immunology_, and _medicine, medical, clinical_ also contribute to the domain-level citation trends. Second, we also identified the interdisciplinary nature of opinion mining: the two groups of domains frequently cross-reference to each other. Next, the advent and growth of opinion mining was mainly led by a few subject categories. Interdisciplinary applications of computer science and psychology were the driving forces that have had the largest influence on the emergence, development, and diffusion of the ideas in opinion mining. Moreover, opinion mining is relatively new and still developing. There are a few emerging research themes identified from keyword co-occurrence and document co-citation network. Finally, opinion mining has received large attention from multidisciplinary domains recently.

### Research question #2: which thematic patterns of research do occur in the domain?

First, the subject category assignment shows that computer science, engineering, linguistics, and social sciences such as library and information science, business, economics, management, and psychology are among the leading categories in opinion mining research. In particular, computer science, psychology, electrical engineering, artificial intelligence, linguistics, and library and information have transferred important knowledge to the domain. Second, the exploration of keywords adds a richer interpretation: the identification and extraction of subjective information in source texts and the applications of sentiment analysis to a variety of textual materials have been regarded as one of the most important themes of the domain. The detection of emotion from source texts was the concept located on the shortest path connecting pairs of other concepts in opinion mining research. _Machine learning, text mining, data mining_, and _natural language processing_ have been employed to this end. _Classification_ is among the most frequently investigated techniques, also being a goal of understanding the sentiment orientation of text. The automatic collection of a _corpus_ from a variety of textual genres such as _web_, _blog_, and _microblogging_ has received large attention for opinion mining purposes. Finally, the examination of the landmark articles reveals that investigating algorithmic and linguistic aspects of sentiment analysis has been of the community’s greatest interest: they have put stress on understanding, quantifying, and applying the sentimental orientation of texts.

### Research question #3: what are emerging trends and recent developments of the field?

Recently, opinion mining has received large attention from many multidisciplinary domains and recent literature has explored the practical applications of opinion mining to marketing and social networking platforms. Bursting keywords such as _word of mouth, social media, social network_, and _twitter_ evidence this trend. The analysis of document co-citation network identifies that emerging clusters of research include 1) understanding consumer attitudes for effective online marketing and prediction of a product’s market value, 2) developing a system for extracting affective information from text, 3) investigating newspaper readers’ opinions and information sharing behaviour on social media. In recent years, landmark manuscripts also have explored the improvement and applications of opinion mining to a variety of domains such as informal English text.

## Conclusion and future work

In the present study, we aimed to explore the intellectual structure of opinion mining research in a systematic and comprehensive way. Toward that end, we investigated domain-level citation trends, assignment of subject categories, keyword co-occurrence, document co-citation network, and landmark articles. Based on the findings from the study, we articulate that the field of opinion mining is still emerging. For example, a few domains have participated in publication while they mainly cite similar kinds of research. They less frequently cross-cite each other. Next, the advent, development, and diffusion of opinion mining have been largely led by a few domains such as computer science, engineering, linguistics, psychology, and library and information science. These domains have explored algorithmic and linguistic aspects of sentiment analysis to understand, quantify, and apply the sentiment orientation of texts. Recently, multidisciplinary domains have participated in studying opinion mining and this body of literature has explored the practical applications of opinion mining such as to marketing and analysing social networking platforms.

The approaches of the present study provide advantages in investigating intellectual structure of a science as follows. First, we systematically triangulated data collection. Conventional studies of domain analysis often cover only a fraction of published literature. The combination of topic search, citation indexing, and patent search in our method provides a systematic way to broaden the coverage of a knowledge domain, thus provides a much broader and more integrated context. Second, we investigated the domain from a multi-faceted point of view. Bursting keywords, document co-citation networks, emerging thematic patterns, and citation trends were identified in this study. In particular, we employed the concept of burstiness for measuring the importance of units of analysis. An entity's frequency of occurrence is a significant indicator reflecting its impact. However, we cannot identify its influence or density of that impact during a particular span of time if only considering the cumulative measure. Instead, we argue that using the concept of citation and keyword bursts is much more convenient in understanding the advent and decline of scientific trends. Finally, the analytical procedure and tool used in the present study enabled us to explore time-aware research trends in the domain. The scientometric approach and tool employed in this study can support comprehensive data collection and scalable analytics. In addition, one can conduct domain analysis of his or her concern as frequently as needed without prior knowledge. Thus, the proposed approaches have advantages such as having a relatively higher reproducibility and lower cost for conducting studies at a larger scale, especially as the number of publications in sciences is fast-growing.

There are still several challenges in our study despite the fact that we have a variety of methodological advantages described above. First, the topic search along with citation indexing still may not be able to capture some relevant records. Generally, the vocabulary mismatch is said to present a challenge for keyword-based search ([Deerwester, Dumais, Furnas, Landauer, and Harshman, 1990](#dee90)). Second, the Web of Science as our source of data may have underrepresented conference proceedings, which is also reported to be an issue for disciplines such as social sciences and arts and humanities ([Mongeon and Paul-Hus, 2016](#mon16)). In addition, at the time of data collection, we could only collect records from the core collection of the Web of Science due to the institutional subscription. It might be possible that the number of records collected in each organization from the core collection of Web of Science varies due to different subscription policies. Some relevant records might have been omitted from our data sets accordingly. Additional sources such as Scopus are recommended for future refinements of this type of analysis. Unfortunately, at the time of the study, we did not have access to Scopus. Third, we selected _g_ highly cited references for generating the intellectual landscapes. In spite of this metric’s authority, we might be too strict in extracting an important proportion of records. It may be worth conducting a separate study of the theoretical implications of using a variety of conceivable selection criteria. We also plan to apply this scientometirc approach to much more comprehensive records that cover a various type of publication materials.

## <a id="author"></a>About the authors

**Yongjun Zhu** is a doctoral candidate in Information Science at Drexel University, Philadelphia, PA. His research interests include information retrieval, network science, and text mining. He can be contacted at [zhu@drexel.edu.](mailto:zhu@drexel.edu)  
**Meen Chul Kim** is a doctoral candidate in Information Science at Drexel University, Philadelphia, PA. His research interests include learning analytics and data science. He can be contacted at [meenchul.kim@drexel.edu.](mailto:meenchul.kim@drexel.edu)  
**Chaomei Chen** is a Professor at the College of Computing and Informatics at Drexel University, Philadelphia, PA. His research interests include information visualization, visual analytics, knowledge domain visualization, network analysis and modelling, scientific discovery, science mapping, scientometrics, citation analysis, and human-computer interaction. He can be contacted at [chaomei.chen@drexel.edu.](mailto:chaomei.chen@drexel.edu)

</section>

<section>

## References

<ul>
<li id="abb08">Abbasi, A., Chen, H. &amp; Salem, A. (2008). Sentiment analysis in multiple languages: feature selection for opinion classification in Web forums. <em>ACM Transactions on Information Systems, 26</em>(3), 12.
</li>
<li id="blo08">Blondel, V.D., Guillaume, J.-L., Lambiotte, R. &amp; Lefebvre, E. (2008). Fast unfolding of communities in large networks. <em>Journal of Statistical Mechanics: Theory and Experiment, 10</em>, P10008.
</li>
<li id="bol11">Bollen, J., Mao, H. &amp; Zeng, X. (2011). Twitter mood predicts the stock market. <em>Journal of Computational Science, 2</em>(1), 1-8.
</li>
<li id="bra01">Brandes, U. (2001). A faster algorithm for betweenness centrality. <em>Journal of Mathematical Sociology, 25</em>(2), 163-177.
</li>
<li id="cam13">Cambria, E., Schuller, B., Xia, Y. &amp; Havasi C. (2013). New avenues in opinion mining and sentiment analysis. <em>IEEE Intelligent Systems, 28</em>(2), 15-21.
</li>
<li id="che06">Chen, C. (2006). CiteSpace II: Detecting and visualizing emerging trends and transient patterns in scientific literature. <em>Journal of the American Society for Information Science and Technology, 57</em>(3), 359-377.
</li>
<li id="che11">Chen, C. (2011). <em>Turning points: The nature of creativity.</em> Beijing: Higher Education Press.
</li>
<li id="che14a">Chen, C., Dubin, R. &amp; Kim, M.C. (2014a). Emerging trends and new developments in regenerative medicine: A scientometric update (2000-2014). <em>Expert Opinion on Biological Therapy, 14</em>(9), 1295-1317.
</li>
<li id="che14b">Chen, C., Dubin, R. &amp; Kim, M.C. (2014b). Orphan drugs and rare diseases: A scientometric review (2000-2014). <em>Expert Opinion on Orphan Drugs, 2</em>(7), 709-724.
</li>
<li id="che09">Chen, C., Chen, Y., Horowitz, M., Hou, H., Liu, Z. &amp; Pellegrino, D. (2009). Towards an explanatory and computational theory of scientific discovery. <em>Journal of Informetrics, 3</em>(3), 191-209.
</li>
<li id="che12">Chen, C., Hu, Z., Liu, S. &amp; Tseng, H. (2012). Emerging trends in regenerative medicine: a scientometric analysis in CiteSpace. <em>Expert Opinions on Biological Therapy, 12</em>(5), 593-608.
</li>
<li id="che10">Chen, C., Ibekwe-SanJuan, F. &amp; Hou, J. (2010). The structure and dynamics of co-citation clusters: a multiple-perspective co-citation analysis. <em>Journal of the American Society for Information Science and Technology, 61</em>(7), 1386-1409.
</li>
<li id="che14">Chen, C. &amp; Leydesdorff, L. (2014). Patterns of connections and movements in dual-map overlays: A new method of publication portfolio analysis. <em>Journal of the American Society for Information Science and Technology, 65</em>(2), 334-351.
</li>
<li id="chev06">Chevalier, J.A. &amp; Mayzlin, D. (2006). The effect of word of mouth on sales: online book reviews. <em>Journal of marketing research, 43</em>(3), 345-354.
</li>
<li id="dav03">Dave, K., Lawrence, S. &amp; Pennock, D.M. (2003). Mining the peanut gallery: opinion extraction and semantic classification of product reviews. <em>Proceedings of the 12th International Conference on World Wide Web</em>, 519-528.
</li>
<li id="dee90">Deerwester, S., Dumais, S.T., Furnas, G.W., Landauer, T.K. &amp; Harshman, R. (1990). Indexing by latent semantic analysis. <em>Journal of the American Society for Information Science, 41</em>(6), 391-407.
</li>
<li id="din08">Ding, X., Liu, B. &amp; Yu, P. (2008). A holistic lexicon-based approach to opinion mining. <em>Proceedings of the 2008 International Conference on Web Search and Data Mining</em>, 231-240.
</li>
<li id="dun93">Dunning, T. (1993). Accurate methods for the statistics of surprise and coincidence. <em>Computational Linguistics, 19</em>(1), 61-74.
</li>
<li id="egg06">Egghe, L. (2006). Theory and practise of the g-index. <em>Scientometrics, 69</em>(1), 131-152.
</li>
<li id="fel13">Feldman, R. (2013). Techniques and applications for sentiment analysis. <em>Communications of the ACM, 56</em>(4), 82-89.
</li>
<li id="gar79">Garfield, E. (1979). <em>Citation indexing: its theory and applications in science, technology, and humanities.</em> New York, NY: Wiley.
</li>
<li id="gov13">Govindarajan, M. &amp; Romina, M. (2013). A Survey of Classification Methods and Applications for Sentiment Analysis. <em>The International Journal of Engineering and Science, 2</em>(12), 11-15.
</li>
<li id="jin08">Jindal, N. &amp; Liu, B. (2008). Opinion spam and analysis. <em>Proceedings of the 2008 International Conference on Web Search and Data Mining</em>, 219-230.
</li>
<li id="kan06">Kanayama, H. &amp; Nasukawa, T. (2006). Fully automatic lexicon expansion for domain-oriented sentiment analysis. <em>Proceedings of the 2006 Conference on Empirical Methods in Natural Language Processing</em>, 355-363.
</li>
<li id="kim15">Kim, M.C. &amp; Chen, C. (2015). A scientometric review of emerging trends and new developments in recommendation systems. <em>Scientometrics, 104</em>(1), 239-263.
</li>
<li id="kim16">Kim, M.C., Zhu, Y. &amp; Chen, C. (2016). How are they different? A quantitative domain comparison of information visualization and data visualization (2000-2014). <em>Scientometrics, 107</em>(1), 123-165.
</li>
<li id="kim04">Kim, S.M. &amp; Hovy, E. (2004). Determining the sentiment of opinions. <em>Proceedings of the 20th International Conference on Computational Linguistics</em>, 1367.
</li>
<li id="kle02">Kleinberg, J. (2002). Bursty and hierarchical structure in streams. <em>Proceedings of the Eighth ACM SIGKDD International Conference on Knowledge Discovery and Data Mining</em>, 91-101.
</li>
<li id="liu10">Liu, B. (2010). Sentiment Analysis and Subjectivity. <em>Handbook of Natural Language Processing, 2</em>, 627-666.
</li>
<li id="liu12">Liu, B. (2012). <em>Sentiment Analysis and Opinion Mining: Synthesis Lectures on Human Language Technologies.</em> San Rafael, CA: Morgan &amp; Claypool Publishers.
</li>
<li id="liu05">Liu, B., Hu, M. &amp; Cheng, J. (2005). Opinion observer: Analyzing and comparing opinions on the web. <em>Proceedings of the 14th International Conference on World Wide Web</em>, 342-351.
</li>
<li id="med14">Medhat, W., Hassan, A. &amp; Korashy, H. (2014). Sentiment analysis algorithms and applications: a survey. <em>Ain Shams Engineering Journal, 5</em>(4), 1093-1113.
</li>
<li id="mon16">Mongeon, P. &amp; Paul-Hus, A. (2016). The journal coverage of web of science and Scopus: a comparative analysis. <em>Scientometrics, 106</em>(1), 213-228.
</li>
<li id="pan04">Pang, B. &amp; Lee, L. (2004). A sentimental education: sentiment analysis using subjectivity summarization based on minimum cuts. <em>Proceedings of the 42th Annual Meeting on Association for Computational Linguistics</em>, 271.
</li>
<li id="pan05">Pang, B. &amp; Lee, L. (2005). Seeing stars: exploiting class relationships for sentiment categorization with respect to rating scales. <em>Proceedings of the 43rd Annual Meeting on Association for Computational Linguistics</em>, 115-124.
</li>
<li id="pan08">Pang, B. &amp; Lee, L. (2008). Opinion mining and sentiment analysis. <em>Foundations and Trends in Information Retrieval, 2</em>(1-2), 1-135.
</li>
<li id="pan02">Pang, B., Lee, L. &amp; Vaithyanathan, S. (2002). Thumbs up? Sentiment classification using machine learning techniques. <em>Proceedings of the 7th ACL-02 Conference on Empirical Methods in Natural Language Processing, 10</em>, 79-86.
</li>
<li id="qiu11">Qiu, G., Liu, B., Bu, J. &amp; Chen, C. (2011). Opinion word expansion and target extraction through double propagation. <em>Computational Linguistics, 37</em>(1), 9-27.
</li>
<li id="ril03">Riloff, E., Wiebe, J. &amp; Wilson, T. (2003). Learning subjective nouns using extraction pattern bootstrapping. <em>Proceedings of the seventh conference on Natural language learning at HLT-NAACL 2003, 4</em>, 25-32.
</li>
<li id="sch89">Schvaneveldt, R.W., Durso, F.T. &amp; Dearholt, D.W. (1989). Network Structures in Proximity Data. In G. Bower (Ed.) <em>The psychology of learning and motivation: Advances in research and theory, 24</em>, 249-284. New York, NY: Academic Press.
</li>
<li id="tab11">Taboada, M., Brooke, J., Tofiloski, M., Voll, K. &amp; Stede, M. (2011). Lexicon-based methods for sentiment analysis. <em>Computational Linguistics, 37</em>(2), 267-307.
</li>
<li id="tan09">Tang, H., Tan, S. &amp; Cheng, X. (2009). A survey on sentiment detection of reviews. <em>Expert Systems with Applications, 36</em>(7), 10760-10773.
</li>
<li id="the10">Thelwall, M., Buckley, K., Paltoglou, G., Cai, D. &amp; Kappas, A. (2010). Sentiment strength detection in short informal text. <em>Journal of the Association for Information Science and Technology, 61</em>(12), 2544-2558.
</li>
<li id="thet10">Thet, T.T., Na, J.-C. &amp; Khoo, C.S.G. (2010). Aspect-based sentiment analysis of movie reviews on discussion boards. <em>Journal of Information Science, 36</em>(6), 823-848.
</li>
<li id="tur02">Turney, P.D. (2002). Thumbs up or thumbs down? Semantic orientation applied to unsupervised classification of reviews. <em>Proceedings of the 40th Annual Meeting on Association for Computational Linguistics</em>, 417-424.
</li>
<li id="tur03">Turney, P.D. &amp; Littman, M. L. (2003). Measuring praise and criticism: inference of semantic orientation from association. <em>ACM Transactions on Information Systems, 21</em>(4), 315-346.
</li>
<li id="wal13">Waltman, L. &amp; van Eck, N.J. (2013). A smart local moving algorithm for large-scale modularity-based community detection. <em>European Physical Journal B, 86</em>(11), 471.
</li>
<li id="wie04">Wiebe, J., Wilson, T., Bruce, R., Bell, M. &amp; Martin M. (2004). Learning subjective language. <em>Computational Linguistics, 30</em>(3), 277-308.
</li>
<li id="wil05">Wilson, T., Wiebe, J. &amp; Hoffmann, P. (2005). Recognizing Contextual Polarity in Phrase-Level Sentiment Analysis. <em>Proceedings of Human Language Technology Conference and Conference on Empirical Methods in Natural Language</em>, 347-354.
</li>
<li id="wil09">Wilson, T., Wiebe, J. &amp; Hoffmann, P. (2009). Recognizing Contextual Polarity: an Exploration of Features for Phrase-Level Sentiment Analysis. <em>Computational Linguistics, 35</em>(3), 399-433.
</li>
<li id="yu03">Yu, H. &amp; Hatzivassiloglou, V. (2003). Towards answering opinion questions: separating facts from opinions and identifying the polarity of opinion sentences. <em>AProceedings of the 2003 Conference on Empirical Methods in Natural Language Processing</em>, 129-136.
</li>
</ul>

</section>

</article>