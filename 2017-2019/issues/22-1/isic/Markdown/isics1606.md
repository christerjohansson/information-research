<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Tacit knowledge sharing: the determination of a methodological approach to explore the intangible

## [Iris Buunk, Hazel Hall](#author) and [Colin Smith](#author)

> **Introduction.** Research design should take into account both (a) the specific nature of the object under scrutiny, and (b) approaches to its study in the past. This is to ensure that informed decisions are made regarding research design in future empirical studies. Here these factors are taken into account with reference to methodological choice for a doctoral study on tacit knowledge sharing, and the extent to tacit knowledge sharing may be facilitated by online tools. The larger study responds to calls for the two domains of knowledge management and human information behaviour to be considered together in terms of their research approaches and theory development.  
> **Method.** Relevant literature – both domain-specific (knowledge management) and general (research methods in social science) - was identified and analysed to identify the most appropriate approaches for an empirical study of tacit knowledge sharing.  
> **Analysis.** The analysis shows that there are a number of challenges associated with studying an intangible entity such as tacit knowledge. Quantitative, qualitative and mixed methods have been adopted in prior work on this theme, each with their own strengths and weaknesses.  
> **Results.** The analysis has informed a decision to adopt a research approach that deploys mixed methods for an inductive case study to extend knowledge of the influence of online tools on tacit knowledge sharing.  
> **Conclusion.** This work intends to open the debate on methodological choice and routes to implementation for studies that are subject to practical constraints imposed by the context in which they are situated.

<section>

## Introduction

The focus of this paper is the presentation of a pragmatic methodology for a doctoral study in the domain of knowledge management. The study has a particular focus on tacit knowledge sharing. Approaches, theories and research from knowledge management have been discussed with human information behaviour in the past, and calls have been made for others to consider the two domains together ([Halbwirth & Olsson, 2007](#hal07a)). This paper is therefore of interest to a wide audience that includes human information behaviour researchers.

The investigation in question will consider tacit knowledge sharing practices amongst public sector employees. It will contribute to closing a gap in knowledge related to organizational knowledge sharing with a focus on social media use (as discussed by [Panahi, 2013](#pan13), p. 380). It will thus address an identified need for the enhancement of understanding of the coexistence of information behaviour and knowledge sharing practices in virtual environments ([Widen-Wulff, Ek, Ginman, _et al._, 2008](#wid08), p. 352). It has been noted that a better appreciation of information behaviour in such contexts can lead to the identification of means of addressing issues related to informal learning activities ([Mills, Knezek & Khaddage 2014](#mil14a), p. 333). Amongst these are challenges related to tacit knowledge transfer.

The larger study addresses the following research questions:

*   How do social media facilitate the sharing of tacit knowledge between employees?
*   To what extent do social media bring new capabilities in the sharing of tacit knowledge?
*   Which situated factors may provide the appropriate context for using social media to enhance tacit knowledge sharing practices?

The need for the development of robust methodological approaches for studies of tacit knowledge sharing has been articulated for some time. For example, Kane, Ragsdell and Oppenheim, highlighted this in 2005\. More recently, Venkitachalam and Busch ([2012, p.360](#ven12)) revisit this question. A particular problem is the practice of applying methodological approaches initially intended to study explicit knowledge to investigations of tacit knowledge, ([Kane _et al._, 2005](#kan05), p.143). This is evident in a number of extant studies that take a positivist approach and generate theory in a deductive manner ([for example, Du, Ai & Ren, 2007](#dur07); [Hsu & Lin, 2008](#hsu08)).

For future work to contribute to research in the domain, it is important to take into account (1) the specific nature of the object of study (tacit knowledge) and (2) approaches to its study in the past (through the adoption of quantitative, qualitative, and mixed methods). This is to ensure that informed decisions are made regarding research design for future empirical studies. These two factors are explored with reference to the doctoral study described above, with a firm focus on tacit knowledge, knowledge sharing practice, and methodological choice. 

## Options for exploring the intangible

Research in the domain of knowledge management has a bias towards exploring knowledge in its explicit form, largely because explicit knowledge is more easily observed than tacit knowledge ([Kane _et al._, 2005](#kan05), p. 142). It is also quantifiable, and therefore measureable ([Virtanen, 2010](#vir10), p.3). This is evident in much knowledge management research conducted in organizational settings ([for example, Du, Ai, & Ren, 2007](#dur07); [Hsu & Lin, 2008](#hsu08)).

Amongst the challenges of studying tacit knowledge and associated practices (such as tacit knowledge sharing) is the intangible nature of the object of study ([Lin, 2007](#lin07), p. 412; [Desouza, 2003](#des03), p. 86; [Miller, 2002](#mil02), p. 6), and complexities in comprehending - and then articulating - its facets ([Nonaka, 1994](#non94), p.24). Despite this, a number of researchers have explored tacit knowledge, often motivated by the recognition of the high value of tacit knowledge and desires to capitalise on this ([Huysman & Wulf, 2006](#huy06); [Ngah, & Jusoff, 2009](#nga09); [Nonaka, 1994](#non94)).

Some studies of tacit knowledge have taken a positivist approach through the adoption of quantitative methods - particularly in attempts to _model_ tacit knowledge sharing, ([for example, Salleh, Chong, Ahmad, & Ikhsan, 2013](#sal13); [Tsai, 2014](#tsa14)). Others follow the tradition of interpretivist research, using qualitative techniques ([for example, Abdullah, Ingram, & Welsh, 2009](#abd09); [Scully, Buttigieg, Fullard, Shaw, & Gregson, 2013](#scu13)). A number use mixed methods ([for example, Desouza, 2003](#des03); [Garcia-Perez, & Mitra, 2007](#gar07)). Presented below are the key characteristics of each approach as relevant to the question of research design for a study of tacit knowledge sharing practices amongst public sector employees.

Typically studies of tacit knowledge which deploy quantitative methods are based on large-scale surveys, some of which make extensive use of the Likert scale ([for example, Borges, 2013](#bor13); [Lin, 2007](#lin07); [Tsai, 2014](#tsa14)). Such studies have been criticised on the basis that these are often implementations initially designed for the study of explicit knowledge, and thus overlook the multidimensional nature of tacit knowledge ([Kane _et al.,_ 2005](#kan05), p.143). A further deficiency of these studies is that their findings, and the models that emanate from them, remain untested ([for example, Hendricks, 1999](#hen99); [Li & Zhang, 2010](#liy10)). More important, however, is that a positivist approach to this domain of study fails to recognise that knowledge is socially constructed, embedded within, and inseparable from work activities and practice ([Hislop, 2013, p. 31](#his13)). Thus positivist studies risk the production of findings that describe the _assets_ generated from tacit knowledge (such as explicit knowledge in the form of information), rather than develop new theory on tacit knowledge per se. This is not to say that such work lacks value. Rather the requirements of research validity are not met because it does not truly measure the construct in question.

In contrast, knowledge management researchers who take an interpretivist stance accept from the outset that knowledge cannot be studied objectively ([for example, Panahi, 2014, p.67](#pan14)). So in their work they deploy qualitative techniques such as interviews, focus groups and surveys, in case study settings ([for example, Hall & Goody, 2007](#hal07b)). Such work includes a number of studies that focus on questions related to tacit knowledge ([for example, Murray & Peyrefitte, 2007](#mur07); [Neve, 2003](#nev03); [Whyte & Classen, 2012](#why12)). These studies usually do not generate models, but instead provide nuanced understandings of particular aspects of knowledge management. This body of work is subject to common criticisms of qualitative research in the social sciences: for example, claims that limited population sampling results in findings that cannot be generalised, and are therefore not reliable ([Bryman, 2012](#bry12), p. 69-70; [LeCompte & Goetz, 1982](#lec82), p. 35). However, it can be argued that a deep analysis through the generation of a single case study is valuable because it can contribute to a _'collective process of knowledge accumulation_' ([Flyvbjerg, 2006](#fly06), p. 227). The _'power of good example_' ([Flyvbjerg, 2001](#fly01), p. 77), where close observation of the object of the study in depth - for example in a single information-rich case adopted for theoretical rather than statistical reasons - has the potential to broaden understanding of a phenomenon. This argument is supported by knowledge management researchers such as Kane _et al._ ([2005](#kan05), p.147-148) who argue for the use of ethnographic studies in knowledge management research, especially for work that is focused on tacit knowledge sharing. Equally others have pointed to the value of knowledge management studies that collect data over long time periods to generate robust findings ([Milton, 2014](#mil14b); [Rasmussen & Hall, 2016](#ras16), p. 366). 

A third option open to knowledge management researchers is to adopt a mixed method research design, i.e., one that incorporates both qualitative and quantitative strategies. Seven per cent of studies that consider public sector knowledge management take this approach ([Massaro, Dumay & Garlatti, 2015](#mas15), p. 539). Although it is routinely stated that a mixed methods strategy lends robustness to research, particularly in respect of triangulation, some knowledge management researchers with interests in explorations of tacit knowledge are critical of such claims. Citing Smith ([1983](#smi83)), Bryman, ([2012](#bry12), p. 629) points to the different ontological roots of qualitative and quantitative methods and their lack of compatibility. It has also been suggested that those who combine methods are misguided in thinking that this will guarantee the validity and reliability of their research, hoping that it will be recognised as ‘scientific’ to an external audience ([Kane _et al._, 2005](#kan05), p.147).

## The compromise of a pragmatic approach to explore the intangible

A reading of the general research literature, in combination with that specific to studies in knowledge management where the focus is tacit knowledge, as summarised above would suggest a ‘gold standard’ of deep ethnographic studies conducted over extended time periods by researchers immersed in the environment under scrutiny where they are able to study in situ the information behaviours of data subjects. For full-time doctoral students, however, an obvious challenge of meeting this ideal is the time limit imposed on the period of study, especially since their work is usually carried out (at least in the social sciences) as an independent endeavour as a form of research apprenticeship.

Other compromises also need to be made with respect to the object of investigation, for example to access the selected population (in this case employees in a public sector organization) in an appropriate context (the organization itself). Thus in this case a pragmatic interpretivist approach has been determined to allow for an examination of subjective experiences in an organizational context, the key features of which are summarised in Table 1.

<table><caption>Table 1: Key features of the research approach</caption>

<tbody>

<tr>

<th>Feature of research design</th>

<th>Decision</th>

<th>Justification</th>

</tr>

<tr>

<td>

**Approach**</td>

<td>Qualitative</td>

<td>

*   Follows dominant practice of knowledge management research in public sector settings ([Massaro, Dumay, Garlatti, 2015, p. 539](#mas15))
*   Allows for an interpretivist perspective
*   Reflects the philosophical standpoint that knowledge of reality is a social construction
*   Appreciates the standpoints of research participants, and situates these in the organizational landscape

</td>

</tr>

<tr>

<td>

**Methods**</td>

<td>Mixed</td>

<td>

*   For triangulation purposes (with attention paid to risks identified by [Kane _et al.,_ 2005](#kan05))

</td>

</tr>

<tr>

<td>

**Research site**</td>

<td>Case study</td>

<td>

*   Follows dominant practice in knowledge management research in public sector settings ([Massaro, Dumay, Garlatti, 2015, p. 539](#mas15))
*   Allows for depth of analysis within a bounded environment of a defined community

</td>

</tr>

<tr>

<td>

**Data collection**</td>

<td>Four activities</td>

<td>

*   Cross-over online survey to establish features of the participants’ landscape and serve as preface for interviews, e.g. platforms available and how they are used, demographic data to profile the user population
*   Semi-structured interviews to explore individual perspectives, allowing a degree of flexibility on the part of the researcher and interviewees (recruited from survey responses)
*   Focus groups explore group perspectives and validate interpretation of results from analysis of survey and interview data
*   Content analysis of documentation related to the environment under scrutiny (organizational information and details of platform development) to provide complementary contextual information about the implementation of knowledge sharing tools (e.g. social media) within the research setting and triangulate survey, interview and focus group data

</td>

</tr>

</tbody>

</table>

Thus the methodological approach viewed as most relevant within the frame of this empirical research is one that deploys mixed methods executed as an inductive case study. This will take into account the limitations imposed by the choices adopted, as explored in the literature evaluated above. While there is unavoidable compromise in the implementation of the research, the risk to the integrity of research findings will be minimised.

This paper has illustrated the importance of a systematic consideration of the nature of constructs that occupy the core of a doctoral study (in this case tacit knowledge) at the stage of research design. This is important to ambitions for, and claims of, extending theoretical perspectives in the domain in the final output of the work. As such this work intends to open the debate on methodological choice and routes to implementation for studies that are subject to practical constraints imposed by the context in which they are situated.

## <a id="author"></a>About the author

**Iris Buunk** is a doctoral student within the School of Computing at Edinburgh Napier University. She holds an MSc in Information Science from the Haute Ecole de Gestion de Genève and the L'École de Bibliothéconomie et des Sciences de l'Information) at the Université de _Montréal,_ and a Diploma in Librarianship, Documentation and Archives from the École Supérieur d’Information Documentaire, Geneva. She is currently exploring the role, use, and value of social media and tacit knowledge sharing practice in public sector organizations for her doctoral thesis. Iris blogs at https://theknowledgeexplorer.org/, and can be contacted at [i.buunk@napier.ac.uk](mailto:i.buunk@napier.ac.uk). Her mailing address is School of Computing, Edinburgh Napier University, 10 Colinton Road, Edinburgh EH10 5DT, UK.  
**Dr Hazel Hall** is Professor and Director of the Centre for Social Informatics within the School of Computing at Edinburgh Napier University. She holds a PhD in Computing from Napier University, an MA in Library and Information Studies from the University of Central England, and a BA (Spec Hons) in French from the University of Birmingham. Her research interests include information sharing in online environments, knowledge management, social computing/media, online communities and collaboration, library and information science research, and research impact. She blogs at http://hazelhall.org and can be contacted at [h.hall@napier.ac.uk](mailto:h.hall@napier.ac.uk). Her mailing address is School of Computing, Edinburgh Napier University, 10 Colinton Road, Edinburgh EH10 5DT, UK.  
**Dr Colin F Smith** is Senior Lecturer and Subject Group Leader for Information Systems within the School of Computing at Edinburgh Napier University. He holds a PhD in Management and a Masters degree in Information and Administrative Management from Glasgow Caledonian University, and a BA (Hons) in Politics from the University of Strathclyde. His research interests focus on the relationships between new information and communication technologies, strategic innovation and organizational change. He can be contacted at [cf.smith@napier.ac.uk](mailto:cf.smith@napier.ac.uk). His mailing address is School of Computing, Edinburgh Napier University, 10 Colinton Road, Edinburgh EH10 5DT, UK.

</section>

<section>

## References

<ul>
<li id="abd09">Abdullah, F., Ingram, A. &amp; Welsh, R. (2009). Managers’ perceptions of tacit knowledge in Edinburgh's Indian restaurants. <em>International Journal of Contemporary Hospitality Management</em>, <em>21</em>(1), 118–127.
</li>
<li id="bor13">Borges, R. (2013). Tacit knowledge sharing between IT workers: the role of organizational culture, personality, and social environment. <em>Management Research Review</em>, <em>36</em>(1), 89–108.
</li>
<li id="bry12">Bryman, A. (2012) <em>Social research methods</em>. Oxford: Oxford University Press.
</li>
<li id="des03">Desouza, K. C. (2003). Facilitating tacit knowledge exchange. <em>Communications of the ACM</em>, <em>46</em>(6), 85–88.
</li>
<li id="dur07">Du, R., Ai, S. &amp; Ren, Y. (2007). Relationship between knowledge sharing and performance: a survey in Xi’an, China. <em>Expert Systems with Applications</em>, <em>32</em>(1), 38–46.
</li>
<li id="fly01">Flyvbjerg, B. (2001). <em>Making social science matter: why social inquiry fails and how it can succeed again</em>. Oxford, UK: Cambridge University Press.
</li>
<li id="fly06">Flyvbjerg, B. (2006). Five misunderstandings about case-study research. <em>Qualitative Inquiry</em>, 12(2), 219–245.
</li>
<li id="gar07">Garcia-Perez, A. &amp; Mitra, A. (2007). Tacit knowledge elicitation and measurement in research organizations: a methodological approach<em>. Electronic Journal of Knowledge Management</em>, 5(4), 373–385.
</li>
<li id="hal07a">Halbwirth, S.J. &amp; Olsson, M.R. (2007). Working in parallel: themes in knowledge management and information behaviour. In S. Hawamdeh (Ed.) <em>Creating collaborative advantage through knowledge and innovation</em> (pp. 69–89). Singapore: World Scientific.
</li>
<li id="hal07b">Hall, H. &amp; Goody, M. (2007). Knowledge management culture and compromise – interventions to promote knowledge sharing supported by technology in corporate environments.&nbsp;<em>Journal of Information Science</em>,&nbsp;33(2), 181-188.
</li>
<li id="hen99">Hendriks, P. (1999). Why share knowledge? The influence of ICT on the motivation for knowledge sharing. <em>Knowledge and Process Management</em>, 6(2), 91–100.
</li>
<li id="his13">Hislop, D. (2013). <em>Knowledge management in organizations: a critical introduction</em> (3<sup>rd</sup> ed.). New York: Oxford University Press.
</li>
<li id="hsu08">Hsu, C. L. &amp; Lin, J. C. C. (2008). Acceptance of blog usage: the roles of technology acceptance, social influence and knowledge sharing motivation. <em>Information and Management</em>, 45(1), 65–74.
</li>
<li id="huy06">Huysman, M. &amp; Wulf, V. (2006). IT to support knowledge sharing in communities, towards a social capital analysis. <em>Journal of Information Technology</em>, 21(1), 40–51.
</li>
<li id="kan05">Kane, H., Ragsdell, G. &amp; Oppenheim, C. (2005). Knowledge management methodologies. <em>The Electronic Journal of Knowledge Management</em>, 4(2), 141–152.
</li>
<li id="lec82">LeCompte, M. D. &amp; Goetz, J. P. (1982). Problems of reliability and validity in ethnographic research. <em>Review of Educational Research</em>, 52(1), 31
</li>
<li id="liy10">Li, Y. &amp; Zhang, L. (2010). The knowledge sharing models of knowledge management implemented with multi-agents. In <em>Proceedings of the International Conference on E-product E-Service and E-Entertainment (ICEEE 2010)</em> (pp. 2441-2444). New York: IEEE.
</li>
<li id="lin07">Lin, C. P. (2007). To share or not to share: Modeling tacit knowledge sharing, its mediators and antecedents. <em>Journal of Business Ethics</em>, 70(4), 411–428.
</li>
<li id="mas15">Massaro, M., Dumay J. &amp; Garlatti, A. (2015). Public sector knowledge management: a structured literature review. <em>Journal of Knowledge Management</em>, 19(3), 530-558.
</li>
<li id="mil02">Miller, F. (2002). I= 0-(Information has no intrinsic meaning). Information Research, 0. <em>Information Research</em>, 8(1), 1-14. Retrieved from http://informationr.net/ir/8-1/paper140.html (Archived by WebCite® at http://www.webcitation.org/6htdqDAOe)
</li>
<li id="mil14a">Mills, L. A., Knezek, G. &amp; Khaddage, F. (2014). Information seeking, information sharing, and going mobile: three bridges to informal learning. <em>Computers in Human Behavior</em>, 32, 324–334.
</li>
<li id="mil14b">Milton, N. (2014). Findings from international surveys providing a snapshot of the state of knowledge management from a practitioner point of view. <em>Journal of Entrepreneurship, Management and Innovation</em> <em>(EMI),</em> 10(1), 109–127.
</li>
<li id="mur07">Murray, S. R. &amp; Peyrefitte, J. (2007). Knowledge type and communication media choice in the knowledge transfer process. <em>Journal of Managerial Issues</em>, 19(1), 111–133.
</li>
<li id="nev03">Neve, T. O. (2003). Right questions to capture knowledge. <em>Knowledge Creation Diffusion Utilization</em>, 1(1), 47–54.
</li>
<li id="nga09">Ngah, R. &amp; Jusoff, K. (2009). Tacit knowledge sharing and SMEs’ organizational performance. <em>International Journal of Economics and Finance</em>, 1(1), 216–220.
</li>
<li id="non94">Nonaka, I. (1994). A dynamic theory of organizational knowledge creation. <em>Organization Science</em>, 5(1), 14–37.
</li>
<li id="pan13">Panahi, S., Watson, J. &amp; Partridge, H. (2013). Towards tacit knowledge sharing over social web tools. <em>Journal of Knowledge Management</em>, 17(3), 379–397.
</li>
<li id="pan14">Panahi, Sirous. (2014). <em>Social media and tacit knowledge sharing: physicians' perspectives and experiences</em>. (Unpublished doctoral thesis). Brisbane: Queensland University of Technology.
</li>
<li id="ras16">Rasmussen, L. &amp; Hall, H. (2016). The adoption process in management innovation: a knowledge management case study. <em>Journal of Information Science</em>, <em>42</em>(3), 386-395.
</li>
<li id="sal13">Salleh, K., Chong, S.C., Ahmad, S.N.S. &amp; Ikhsan, S.O.S.S. (2013). The extent of influence of learning factors on tacit knowledge sharing among public sector accountants. <em>Vine: The Journal of Information and Knowledge management Systems</em>, 43(4), 424–441.
</li>
<li id="scu13">Scully, J. W., Buttigieg, S. C., Fullard, A., Shaw, D. &amp; Gregson, M. (2013). The role of SHRM in turning tacit knowledge into explicit knowledge: a cross-national study of the UK and Malta. <em>The International Journal of Human Resource Management</em>, 24(12), 2299–2320.
</li>
<li id="smi83">Smith, J. K. (1983). Quantitative versus qualitative research: an attempt to clarify the issue. <em>Educational Researcher, 12</em>(3), pp. 6-13.
</li>
<li id="tsa14">Tsai, A. (2014). An empirical model of four processes for sharing organizational knowledge. <em>Online Information Review</em>, 38(2), 305–320.
</li>
<li id="ven12">Venkitachalam, K. &amp; Busch, P. (2012). Tacit knowledge: review and possible research directions. <em>Journal of Knowledge Management</em>, 16(2), 356-371.
</li>
<li id="vir10">Virtanen, I. (2010). Epistemological problems concerning explication of tacit knowledge. <em>Journal of Knowledge management Practice</em>, 11(4), 1–17.
</li>
<li id="why12">Whyte, G. &amp; Classen, S. (2012). Using storytelling to elicit tacit knowledge from SMEs. <em>Journal of Knowledge Management</em>, 16(6), 950–962.
</li>
<li id="wid08">Widen-Wulff, G., Ek, S., Ginman, M., Perttila, R., Sodergard, P. &amp; Totterman, A.-K. (2008). Information behaviour meets social capital: a conceptual model. <em>Journal of Information Science,</em> 34(3), 346–355.
</li>
</ul>

</section>

</article>