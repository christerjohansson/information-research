<!DOCTYPE html>
<html lang="en">

<head>
	<title>Scientific publications as boundary objects: theorising the intersection of classification and research
		evaluation</title>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
	<meta name="dcterms.title"
		content="Scientific publications as boundary objects: theorising the intersection of classification and research evaluation">
	<meta name="author" content="Fredrik Åström,Björn Hammarfelt,Joacim Hansson">
	<meta name="dcterms.subject"
		content="When using bibliometrics for research evaluation, the classification of research fields is an issue of great importance. The purpose of this paper is to outline a brief theoretical framework for analysing the role of classification in research evaluation practices.">
	<meta name="description"
		content="When using bibliometrics for research evaluation, the classification of research fields is an issue of great importance. The purpose of this paper is to outline a brief theoretical framework for analysing the role of classification in research evaluation practices. Taking departure in the concept of ‘boundary objects’ we develop a theoretical framework for analyses of how scientific publications negotiate between different social worlds. Moreover, by adding the perspective of large evaluative infrastructures our study seeks to highlight tensions between local practices and global standards. One scientific article was analysed in terms of the different ways it can be classified on author and affiliation levels, on a documental level, and on a bureaucratic level. Publications are boundary objects residing between social worlds: the context of communication and the context of evaluation. Tensions between social worlds become apparent in infrastructures, which aims to serve the demands both of communication and of evaluation.">
	<meta name="keywords" content="research evaluation, classification, boundary objects, infrastructure,">
	<meta name="robots" content="all">
	<meta name="dcterms.publisher" content="University of Borås">
	<meta name="dcterms.type" content="text">
	<meta name="dcterms.identifier" content="ISSN-1368-1613">
	<meta name="dcterms.identifier" content="http://InformationR.net/ir/22-1/colis/colis1623.html">
	<meta name="dcterms.relation" content="http://InformationR.net/ir/22-1/infres221.html">
	<meta name="dcterms.format" content="text/html">
	<meta name="dc.language" content="en">
	<meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
	<meta name="dcterms.issued" content="2017-03-15">
	<meta name="geo.placename" content="global">
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<header>
		<h4 id="vol-22-no-1-march-2017">vol. 22 no. 1, March, 2017</h4>
	</header>
	<article>
		<h1
			id="scientific-publications-as-boundary-objects-theorising-the-intersection-of-classification-and-research-evaluation">
			Scientific publications as boundary objects: theorising the intersection of classification and research
			evaluation</h1>
		<h2><a href="#author">Fredrik Åström</a>,
			<a href="#author">Björn Hammarfelt</a> and <a href="#author">Joacim Hansson</a></h2>
		<blockquote>
			<p><strong>Introduction.</strong> When using bibliometrics for research evaluation, the classification of
				research fields is an issue of great importance. The purpose of this paper is to outline a brief
				theoretical framework for analysing the role of classification in research evaluation practices.<br>
				<strong>Theory.</strong> Taking departure in the concept of ‘boundary objects’ we develop a theoretical
				framework for analyses of how scientific publications negotiate between different social worlds.
				Moreover, by adding the perspective of large evaluative infrastructures our study seeks to highlight
				tensions between local practices and global standards.<br>
				<strong>Empirical example.</strong> One scientific article was analysed in terms of the different ways
				it can be classified on author and affiliation levels, on a documental level, and on a bureaucratic
				level.<br>
				<strong>Discussion.</strong> Publications are boundary objects residing between social worlds: the
				context of communication and the context of evaluation. Tensions between social worlds become apparent
				in infrastructures, which aims to serve the demands both of communication and of evaluation.</p>
		</blockquote>
		<section>
			<h2 id="introduction">Introduction</h2>
			<p>The way of categorising research fields in bibliometrics has traditionally been by using Web of Science
				(WoS) Subject Categories; but over the last decade, work has increasingly been done on developing more
				organic categorisations (e.g. <a href="#col15">Colliander, 2015</a>; <a href="#kla06">Klavans and
					Boyack, 2006</a>; <a href="#rui15">Ruiz-Castillo and Waltman, 2015</a>). There is also a plethora of
				other classification systems being used, such as the OECD fields of science and technology
				classification (<a href="#oecd07">OECD, 2007</a>); and in a wide variety of contexts: from local
				publication databases using the department affiliation of the author, to subject panels for reviewing
				research grant proposals. We find variations in terms of purpose of categorisation as well as purpose of
				evaluation, on what levels the distinctions are being made, and in terms of on what principles the
				categories are being defined. Are we defining the field affiliation of an individual document? Are we
				categorising an article by the subject category of the journal it was published in? Are we defining the
				research field of an author through institutional affiliation?</p>
			<p>The classification of publications becomes an increasingly pertinent question when bibliometric measures
				are used for assessing research output. Research evaluation is permeating scholarly and scientific
				activities today; and often, research funds are being allocated based on bibliometric indicators on
				scholarly productivity and impact on all levels: from individual scholars to national research systems
				(<a href="#der16">de Rijcke et.al, 2016</a>). The main purpose of these evaluative activities is to
				assign specific values to research outputs, and then, take action based on the outcome. However, before
				value can be assigned, the property being valued has to be identified and defined; and this process of
				classification is at the heart of any evaluative process. Classification is a necessary precondition for
				performing bibliometric analysis, and ideally the chosen categories should correspond with the research
				activities being evaluated (<a href="#had15">Haddow, 2015</a>). In bibliometric research and practices,
				the need for taking differences between research fields into account is increasingly being emphasised,
				to avoid making unfair comparisons between areas with wildly differing publication and citation
				practices. Practically, this is done in activities ranging from the selection of publications from a
				certain field for publication analyses, to the construction of reference sets for field normalised
				citation analyses. In short, we want to be able to retrieve similar documents within the same research
				field. But to be able to identify documents from the same research field, one question need to be
				addressed: how do we define a research field and delineate it from other fields? When financial
				resources are being distributed based on bibliometrics taking field definitions into account, how we
				determine whether an article is presenting research belonging to one field or another, becomes an
				important question.</p>
			<p>The problem of classification is a key topic in the bibliometric literature, and problems of field
				delineation, both automated and non-automated, is an ongoing discussion (e.g. <a href="#raf09">Rafols
					and Leydesdorff, 2009</a>; <a href="#van09">Van Leeuwen and Media, 2009</a>). A central question has
				been how to define particular areas of research in bibliometric terms, and different theoretical and
				conceptual approaches have been proposed (<a href="#sug15">Sugimoto and Weingart, 2015</a>). Terms such
				as discipline, field, domain and specialty are commonly used. Each of these terms have different
				connotations: discipline points to the institutional characteristics such as departments and
				conferences, while field is a more loosely defined term that can be described as areas of common
				research interests.</p>
			<p>This paper is part of an ongoing project addressing the problematic relation between research evaluation
				practices, bibliometrics and classification. It outlines a brief theoretical framework for analysing the
				role of classification in research evaluation practices, based on bibliometrics. Moreover, the paper
				concerns itself with classificatory practices actually being used by academic institutions and actors
				rather than ideal constructs designed for implementation in a specific research setting. Taking the
				perspective of the individual researcher, rather than a top-down view of a discipline or field, we
				choose to zoom in on one particular publication to illustrate the numerous ways in which this object can
				be classified. The suggested analytical approach is to conceptualise scholarly publications as boundary
				objects (<a href="#sta89">Star and Griesemer, 1989</a>), with distinct differences in terms of
				definition and function of the publication depending on whether it is situated in a context of scholarly
				communication or a context of research evaluation. The varying functions of the boundary object - in
				this case the scientific article - in different social worlds becomes clear when contextualised within
				the concept of infrastructures (<a href="#sta89">Star and Griesemer, 1989</a>), in this case publication
				databases, citation indices and evaluation systems, and not the least, in classification systems.</p>
			<h2 id="research-publications-as-boundary-objects">Research publications as boundary objects</h2>
			<p>In defining her concept boundary object Susan Leigh Star emphasises combinations of materialities and
				processes of objects placed between different social worlds (<a href="#sta89">Star and Griesemer,
					1989</a>; <a href="#bow99">Bowker and Star, 1999</a>; <a href="#sta10">Star, 2010</a>). The point of
				departure is the investigation of “the nature of cooperative work in the absence of consensus” (<a
					href="#sta10">Star, 2010, p. 604</a>) in complex endeavours such as scientific practices. A boundary
				object has a kind of flexibility which allows it to simultaneously meet the requirements of different
				social worlds (or actors), yet maintaining a kind of integrity in its own right. In the original article
				on boundary objects, Star and Griesemer (<a href="#sta89">1989</a>) notes the necessity in creating
				boundary objects in scientific work, in order to achieve a common representation and informational
				consistency. They state that” scientists and other actors contributing to science translate, negotiate,
				debate, triangulate and simplify in order to work together” (<a href="#sta89">Star and Griesemer, 1989,
					p. 388-389</a>). They point at two separate, but related, processes needed to achieve informational
				consensus: methodological standardisation and the creation of boundary objects as such. In research, the
				scientific article is a central boundary object. The scientific article is a fundamental prerequisite,
				not only for the negotiation of methodological standardisation (i.e. what is a scientific article?), but
				also for the definition of the research fields of relevance for those contributing to scientific
				production and evaluation (<a href="#fra08">Francke, 2008</a>; <a href="#fro04">Frohmann, 2004</a>; <a
					href="#haiip">Haider and Åström, in press</a>). In short, in this paper we conceptualise scientific
				articles as boundary objects based on the duality present in the articles being contextualised both in
				systems of scholarly communication and systems of research evaluation.</p>
			<p>We propose that the negotiating character of the scientific article is possible to define and analyse on
				several levels:</p>
			<ul>
				<li>individual level: who is the author in terms of academic degree, institutional belonging, and
					self-perceived disciplinary identity?</li>
				<li>institutional level: in one sense, what is the institutional affiliation of the author(s); and in
					another sense, what is the institutional context of the publication?</li>
				<li>documental level: according to which scientific convention is the article written (i.e. humanities,
					social science, science); and how is the article defined within the context of the chosen
					journal/publisher, as well as within relevant knowledge organisation systems or science
					classifications?</li>
				<li>bureaucratic level: How does the article, and the research it reports, relate to funding agencies
					and evaluation categorisations, such as the OECD classification or the Swedish adaption thereof, by
					Statistics Sweden/the Swedish Higher Education Authority (SCB/UKÄ).</li>
			</ul>
			<p>To understand the relation between, on one hand these levels of classification and, on the other hand
				research evaluation and classification practices, scientific publications need to be considered as
				boundary objects within research evaluation infrastructures.</p>
			<h2 id="evaluative-infrastructures">Evaluative infrastructures</h2>
			<p>Through local use and processes of standardisation, boundary objects become part of infrastructures (<a
					href="#sta10">Star, 2010</a>). In our particular case the boundary objects (publications) becomes
				parts of both communicative, and of evaluative infrastructures. The role of infrastructures in
				accounting for research impact and its central role for bibliometric evaluation have recently been
				highlighted by Wouters (<a href="#wou14">2014</a>), and by Power (<a href="#pow15">2015</a>). We build
				on these early considerations of evaluative infrastructures by highlighting the central role of boundary
				objects in their formation, and we contrast the concept of infrastructures with Dahler-Larsen’s (<a
					href="#dah12a">2012a</a>; <a href="#dah12b">2012b</a>) description of evaluation systems.</p>
			<p>Our understanding of infrastructures is mainly derived from the work of Star and Ruhleder. They view
				infrastructures as the result of “tension between local, customized, intimate and flexible use on the
				one hand, and the need for standards and continuity on the other” (<a href="#sta96">Star and Ruhleder,
					1996, p. 112</a>). Infrastructures become apparent in practice and are linked to activities. In this
				sense it is a relational concept; infrastructures emerge in connection to practices. Moreover, Star and
				Ruhleder finds that infrastructures are embedded in other technologies and social arrangements, having a
				reach beyond a single event or a specific context. Typically, infrastructures are also transparent; and
				do not have to be reinvented each time they are used. An infrastructure is often taken for granted
				within a particular group or community and newcomers or outsiders have to learn about it. Consequently,
				infrastructures both form and are formed by conventions within a community. An installed base (e.g. a
				digital database building on older library catalogues) is the basis of an infrastructure, allowing for
				backward compatibility. Finally, infrastructures embody standards, and the overall design (including
				standards) of infrastructures becomes visible only when they fail to work.</p>
			<p>An important observation made by Star and Ruhleder (<a href="#sta96">1996</a>) is that infrastructures
				emerge to solve friction between the local (contextualised) and the global (standardised). This tension,
				between local specific classifications and the standardised, one-fits all, design of large-scale
				evaluation systems, is at the core of the problem that this study engages with. The contradictory
				demands from formal systems and local informal practices may result in a double bind, which ultimately
				has to be resolved by the user (in our case the researcher/author). Overall, the success of an
				infrastructure can be judged by the creation of objects and procedures. Hence, the greater agreement on
				definition, and stabilisation, of objects, the better an infrastructure works.</p>
			<p>A type of infrastructure of particular interest for this study is research evaluation systems. These
				systems are routinised, permanent and extended over time and space, and in comparison with regular (one
				off) evaluations, these systems are less reliant on the views and approaches taken by individual
				evaluators (<a href="#dah12b">Dahler-Larsen 2012b</a>). An important function of research evaluation
				systems is captured by Star and Ruhleder’s characterisation of infrastructures as intermediates between
				the local (contextualised) and global (standardised). A recent study of bibliometric evaluation systems
				at Swedish universities found that local evaluation infrastructures often were initiated as means for
				negotiating evaluation systems on the national level. Although, it was found that all these systems are
				unique in one way or another, they tend to be derived from the same ‘installed base’ of available
				publication databases and citation indices (<a href="#hamip">Hammarfelt et.al, in press</a>). Moreover,
				they tend to build on the same principal unit for evaluation: the publication.</p>
			<h2 id="empirical-example">Empirical example</h2>
			<p>To exemplify, we use one article to illustrate the problems we are outlining in this paper, and how these
				problems can be understood from a theoretical perspective. The article was not chosen at random; we
				deliberately selected one in our immediate vicinity that we know is well suited as an example, an
				article spanning over several different research fields and that can be classified in different ways
				depending on what method for classification is chosen (Figure 1).</p>
			<figure>
				<img src="colis1623fig1.jpg"
					alt="Figure1: Example article: Landström, H. et.al. (2012). Entrepreneurship: Exploring the knowledge base. Research Policy, 41(7), 1154-1181.">
				<figcaption>Figure 1: Example article: Landström, H. _et al._ (2012). Entrepreneurship: Exploring the
					knowledge base. Research Policy, 41(7), 1154-1181.</figcaption>
			</figure>
			<h3 id="individual-level">Individual level</h3>
			<p>We begin with the individual and social aspects related to the authors of the article. The authors cover
				three different fields of research, based on how they identify themselves in relation to research
				fields: entrepreneurship research, innovation studies and bibliometrics/information studies. In addition
				to this, one author is also professor in both business management and entrepreneurship research. The
				institutional affiliations of the authors matches the profile we find when combining how they identify
				themselves as scholars and the degrees they’ve been awarded, albeit one author is not affiliated to an
				information studies department but to a university library. Thus, on author level we find at least four
				fields of research represented:</p>
			<ul>
				<li>business administration</li>
				<li>entrepreneurship research</li>
				<li>bibliometrics/information studies</li>
				<li>innovation studies.</li>
			</ul>
			<h3 id="document-level">Document level</h3>
			<p>The second dimension of classification is related to the publication per se, where we can look at, on one
				hand, the journal the article is published in; and on the other, the article per se. The article is
				published in Research Policy, a journal describing itself as a “multi-disciplinary journal devoted to
				analysing, understanding and effectively responding to the [...] challenges posed by innovation,
				technology, R&amp;D and science” (<a
					href="http://www.journals.elsevier.com/research-policy/">http://www.journals.elsevier.com/research-policy/</a>).
				In the Web of Science (WoS)/Journal Citation Reports subject classification, the journal is categorized
				as both “Management” and “Planning &amp; Development”. In the Spanish SCImago Journal Rank (<a
					href="http://www.scimagojr.com/">http://www.scimagojr.com/</a>), the journal is classified within
				three subject areas: “Business, Management and Accounting”, “Decision Sciences” and “Engineering” –
				which is also the subject categories used for describing the journal in Scopus; and four subject
				categories: “Engineering (miscellaneous)”, “Management of Technology and Innovation”, “Management
				Science and Operations Research“ and “Strategy and Management”.</p>
			<p>Categorising the article becomes more complex, and dependent on where the information is gathered, as
				well as depending on whether descriptors used emanates from a controlled vocabulary or not, as well as
				if they are automatically generated or not. The keywords used by the authors to describe the article is
				“Entrepreneurship”, “Research field”, “Handbooks” and “Bibliometric analysis”; of which the first and
				the last are keywords immediately relatable to fields of research. In WoS we also find “KeyWords Plus”
				descriptors such as “Science Policy”, “Innovation”, “Citations” and “Economics”. In the database
				<em>Business Source Complete</em>, we find subject terms describing the article like “Entrepreneurship”,
				“Management research”, “Economics”, “Technological Innovations” and “Bibliographical Citations”; and the
				pattern in for instance Scopus is the same as the other databases.</p>
			<h3 id="bureaucratic-level">Bureaucratic level</h3>
			<p>One source of classification for academic publications is the indexing in publication archives, which in
				this case can be seen as bridging the document and the bureaucratic dimensions of classification. This
				is partly since the classification of research publications in the Swedish publication archive SwePub
				(<a href="http://swepub.kb.se/">http://swepub.kb.se/</a>) is based on the Swedish adaptation of the OECD
				fields of science classification, the SCB/UKÄ classification; and perhaps more importantly, since SwePub
				is currently being remodelled with the pronounced purpose of becoming a reliable data source for
				bibliometric analyses for research evaluations on both local and national level. In SwePub – as well as
				the local repository for the university from which the article emanates, from which SwePub harvests the
				bibliographical data – this particular article is classified as “Information Studies” and “Social
				Sciences Interdisciplinary”. Most likely, rather than relating to the topic of the article per se, these
				classifications emanates from a combination of the institutional affiliations of the authors: the
				university library as well as connections to an information studies research group, and an innovation
				studies research centre; and the journal in which the article is published, a multi-disciplinary journal
				with a focus on issues related to research policy and innovation studies. The economics and business
				perspective reflected in the article analysing entrepreneurship research is not visible in SwePub. As a
				point of comparison, another article by the same three authors using bibliometrics to compare
				entrepreneurship research and innovation studies in the <em>International Entrepreneurship and
					Management Journal</em> – and where the first author uses an affiliation at an entrepreneurship
				research centre – the SwePub classification is “Social Sciences Interdisciplinary”, but also “Economics
				and Business” and “Business Administration”; while the information studies aspect is missing.</p>
			<p>Aside from the connections between the SwePub classification and the Swedish adaptation of the OECD
				fields of science, we could point to the current Swedish system for allocating resources between
				universities. The bibliometric component in this system builds on the number of publications produced
				(normalised against the average production within the field) and normalised citation scores (<a
					href="#san09">Sandström and Sandström, 2009</a>). Additional weighting is then given depending on
				domain: natural sciences, humanities, medicine and social sciences. The effect of this weighting is that
				a highly cited article, categorised as social science, might yield a substantial return for a
				university. In fact, it was recently discovered that one particular highly cited social sciences article
				at Stockholm University contributed with over 5% of their total allocation (<a href="#nel15">Nelhans,
					2015</a>). How this particular article is classified is fundamental for how it is counted in the
				system, and a reclassification to a more citation dense category might influence the results
				considerably. Hence, the bureaucratic and economic dimension comes directly into play in these
				processes. It needs to be acknowledged that the bureaucratic dimension is not necessarily exclusive in
				relation to the individual and document levels. Different research evaluation systems use different
				categorisations, from counting various kinds of document types, to field normalised citation counts
				based on journal subject categories. An obvious example in this case is the indexing of the article in
				SwePub, which is a case of indexing an article in a bibliographic database, however with a
				categorization based on a classification system originating in a bureaucratic context.</p>
			<h2 id="discussion-and-outlook">Discussion and outlook</h2>
			<p>The example article can potentially enter research evaluation systems on a number of different levels.
				Initially, we can make a distinction between on one hand, evaluation processes of research project and
				grant proposals and the peer review evaluation of the article per se; and on the other hand, the post
				hoc evaluations of authors and the institutional settings, both locally and on the national level, to
				evaluate performance. Not all of these evaluations are performed using bibliometrics, but they all have
				the potential to tie in to the different levels of evaluation. At the same time, depending on level of
				evaluation - as well as for instance what evaluation systems and what bibliometric methods being used -
				the subject classification of field of research varies greatly; between evaluation levels but also
				within these levels. In the different classifications of the article used as an example here, at the
				different evaluation levels, at least six different fields of research - seven if we include “Social
				Sciences Interdisciplinary” - are identifiable. Taking into account different levels of evaluation
				processes, different levels of evaluation, and the different research fields the article can be related
				to, we are operating classification and evaluation processes in three different dimensions. The question
				is, however: what are the possibilities of translating and homogenising these processes? And what
				happens when evaluation processes at different levels have conflicting demands?</p>
			<p>Scientific publications are boundary objects in the sense that they “reside between social worlds” (<a
					href="#sta10">Star 2010, p. 604</a>). In our case two of these social worlds are the context of
				communication and the context of evaluation. Tensions between social worlds become apparent in
				infrastructures, which aims to serve the demands both of communication and of evaluation. This conflict
				is by no means a new phenomenon. For example, the <em>Science Citation Index</em> (1964) was established
				as a system for promoting communication, yet shortly after its inception it also became a tool for
				evaluation. Neither, do we claim to be unique in pointing to the inherent tension between communication
				systems and rewards systems. However, with the emergence of new, all-encompassing infrastructures, such
				as national Complete Research Information Systems (CRIS) (cf. SwePub), we suggest that the inherent
				tensions between communication and evaluation needs to be assessed further, in a systematic and
				theoretically informed way. Our conceptualisation of SwePub, and similar systems, as infrastructures
				which aim to solve frictions between different levels: the local (contextualised) and global
				(standardised), and purposes: communication and evaluation, might be a fruitful perspective when
				approaching this problem. The framing proposed in this paper of publications as boundary objects and
				publication databases as infrastructures should thus be seen as one small step in a much larger
				endeavour. The current focus of this paper has been on objects and infrastructures. Yet, we do
				acknowledge the importance of studying classification as a specific practice, and the crucial role of
				classificatory workers running these infrastructures – for example librarians (<a href="#ast13">Åström
					and Hansson, 2013</a>) – should not be underestimated.</p>
			<h2 id="acknowledgements">Acknowledgements</h2>
			<p>This paper was in part funded by Riksbankens Jubileumsfond: The Swedish Foundation for the Social
				Sciences and Humanities (grant number SGO14-1153:1).</p>
			<h2 id="author">About the authors</h2>
			<p><strong>Fredrik Åström</strong> (Ph.D.) is a reader in information studies and works as a specialist in
				bibliometrics and research evaluation systems at Lund University Library, P.O. Box 3, SE 221 00 Lund,
				Sweden. He specialises in research on bibliometrics and scholarly communication; and recently with a
				focus on effects of the use of, and policy aspects of, research evaluation systems based on
				bibliometrics. He can be contacted at: <a
					href="mailto:fredrik.astrom@ub.lu.se">fredrik.astrom@ub.lu.se</a>.<br>
				<strong>Björn Hammarfelt</strong> (Ph.D.) is a senior lecturer at the Swedish School of Library and
				Information Science (SSLIS), University of Borås, Allégatan 1, SE-501 90, Borås, Sweden and a visiting
				scholar at the Centre for Science and Technology Studies (CWTS), Leiden University. His research is
				situated at the intersection between information science and sociology of science, with a focus on the
				organisation, communication and evaluation of research. He can be contacted at: <a
					href="mailto:bjorn.hammarfelt@hb.se">bjorn.hammarfelt@hb.se</a>.<br>
				<strong>Joacim Hansson</strong> is Professor of Library and Information Science at the School of
				Cultural Sciences at Linnaeus University, P.O Box 451, SE-351 06 Växjö, Sweden. His research covers
				several areas, such as classification theory and history, library studies, document practice theory, and
				scholarly communication, with special interest in the role of academic libraries. He can be contacted at
				<a href="mailto:joacim.hansson@lnu.se">joacim.hansson@lnu.se</a></p>
		</section>
		<section>
			<h2>References</h2>
			<ul>
				<li id="ast13">Åström, F. &amp; Hansson, J. (2013). How implementation of bibliometric practice affects
					the role of academic libraries. Journal of Librarianship and information Science, 45(4), 316-322.
				</li>
				<li id="bow99">Bowker, G.C. &amp; Leigh Star, S. (1999). Sorting things out: classification and its
					consequences. Cambridge, MA: MIT Press.
				</li>
				<li id="col15">Colliander, C. (2015). A novel approach to citation normalization: a similarity-based
					method for creating reference sets. Journal of the Association for Information Science and
					Technology, 66(3), 489-500.
				</li>
				<li id="dah12a">Dahler-Larsen, P. (2012a). The evaluation society. Stanford: Stanford University Press.
				</li>
				<li id="dah12b">Dahler-Larsen, P. (2012b). Evaluation as a situational or a universal good? Why
					evaluability assessment for evaluation systems is a good idea, what it might look like in practice,
					and why it is not fashionable. Scandinavian Journal of Public Administration, 16(3), 29–46.
					Retrieved from: http://ojs.ub.gu.se/ojs/index.php/sjpa/article/view/1705 (Archived by WebCite®at <a
						href="http://www.webcitation.org/6jbSTERgv"
						target="_blank">http://www.webcitation.org/6jbSTERgv</a>)
				</li>
				<li id="der16">de Rijcke, S., Wouters, P.F., Rushforth, A.D., Franssen, T.P. &amp; Hammarfelt, B.
					(2016). Evaluation practices and effects of indicator use: a literature review. Research Evaluation,
					25(2), 161-169.
				</li>
				<li id="fra08">Francke, H. (2008). (Re)creations of scholarly journals: document and information
					architecture in open access journals. Borås: Valfrid.
				</li>
				<li id="fro04">Frohmann, B. (2004). Deflating information: from science studies to documentation.
					Toronto: University of Toronto Press.
				</li>
				<li id="had15">Haddow, G. (2015). Research classification and the social sciences and humanities in
					Australia: (mis)matching organizational unit contribution and the impact of collaboration. Research
					Evaluation, 24(3), 325-339.
				</li>
				<li id="haiip">Haider, J. &amp; Åström, F. (in press). Dimensions of trust in scholarly communication:
					problematizing the peer review process in the aftermath of John Bohannon’s ‘Sting’ in Science.
					Journal of the Association for Information Science and Technology.
				</li>
				<li id="hamip">Hammarfelt, B., Nelhans, G., Eklund, P. &amp; Åström, F. (in press). The heterogeneous
					landscape of bibliometric indicators: evaluating models for allocating resources at Swedish
					universities. Research Evaluation. Retrieved from:
					https://lup.lub.lu.se/search/publication/f6d828c4-d974-4190-ba95-f4f20fc71345 (Archived by
					WebCite®at <a href="http://www.webcitation.org/6jqhV8w1R"
						target="_blank">http://www.webcitation.org/6jqhV8w1R</a>)
				</li>
				<li id="kla06">Klavans, R. &amp; Boyack, K.W. (2006). Identifying a better measure of relatedness for
					mapping science. Journal of the American Society for Information Science and Technology, 57(2),
					251-263.
				</li>
				<li id="lan12">Landström, H., Harirchi, G &amp; Åström, F. (2012). Entrepreneurship: exploring the
					knowledge base. Research Policy, 41(7), 1154-1181.
				</li>
				<li id="nel15">Nelhans, G. (2015). Meaningful citation analysis? Paper presented at the 20th Nordic
					Workshop on Bibliometric and Research Policy, Oslo, Norway.
				</li>
				<li id="oecd07">OECD (2007). Revised field of science and technology (FOS) classification in the
					Frascati Manual. Paris: OECD (DSTI/EAS/STP/NESTI(2006)19/FINAL). Retrieved from:
					https://www.oecd.org/science/inno/38235147.pdf Archived by WebCite®at <a
						href="http://www.webcitation.org/6jbSoaSCR"
						target="_blank">http://www.webcitation.org/6jbSoaSCR</a>
				</li>
				<li id="pow15">Power, M. (2015). How accounting begins: object formation and the accretion of
					infrastructure. Accounting, Organizations and Society, 47, 43–55.
				</li>
				<li id="raf09">Rafols, I. &amp; Leydesdorff, L. (2009). Content-based and algorithmic classifications of
					journals: perspectives on the dynamics of scientific communication and indexer effects. Journal of
					the American Society for Information Science and Technology, 60(9),1823–1835.
				</li>
				<li id="rui15">Ruiz-Castillo, J. &amp; Waltman, L. (2015). Field-normalized citation impact indicators
					using algorithmically constructed classification systems of science. Journal of Informetrics, 9(1),
					102-117.
				</li>
				<li id="san09">Sandström, U. &amp; Sandström, E. (2009). The field factor: towards a metric for academic
					institutions. Research Evaluation, 18(3), 243–250.
				</li>
				<li id="sta10">Star, S. L. (2010). This is not a boundary object: reflections on the origin of a
					concept. Science, Technology &amp; Human Values, 35(5), 601-617.
				</li>
				<li id="sta89">Star, S. L. &amp; Griesemer, J.R. (1989). Institutional ecology, ‘translations’ and
					boundary objects: amateurs and professionals in Berkeley’s Museum of Vertebrate Zoology, 1907-39.
					Social Studies of Science, 19(3), 387-420.
				</li>
				<li id="sta96">Star, S. L. &amp; Ruhleder, K. (1996). Steps toward an ecology of infrastructure: design
					and access for large information spaces. Information Systems Research, 7(1), 111-134.
				</li>
				<li id="sug15">Sugimoto, C. &amp; Weingart, S. (2015). The kaleidoscope of disciplinarity. Journal of
					Documentation, 71(4), 775-794.
				</li>
				<li id="van09">Van Leeuwen, T.N. &amp; Media, C. (2009). Redefining the field of economics: improving
					field normalization for the application of bibliometric techniques in the field of economics. In
					B.L.J. Larsen (Ed.), Proceedings of ISSI 2009—12th international conference of the international
					society for scientometrics and informetrics (Vol. 1, pp. 411–420). São Paulo: BIREME/PAHO/WHO.
				</li>
				<li id="wou14">Wouters, P. (2014) The citation: from culture to infrastructure. In: B. Cronin and C.R.
					Sugimoto (Eds.) Beyond bibliometrics: harnessing multidimensional indicators of scholarly
					performance (pp. 47–66). Cambridge, MA: MIT Press.
				</li>
			</ul>
		</section>
	</article>
</body>

</html>