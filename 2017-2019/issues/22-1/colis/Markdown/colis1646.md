<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# To what ends, by which means?

## [Shannon Crawford Barniskis](#author)

> **Introduction.** This paper explores the historical development of the “library faith” as a sociotechnical imaginaire necessary to the development and support of public libraries. It examines a current iteration of this faith, described in library makerspace rhetoric. The study’s purpose is to consider how the faith has changed, and whether the latest incarnation of the faith aligns with foundational library values.  
> **Method.** The study looks at instances of the “library faith” to seek the instrumental means-ends chains identified in these texts. It also examines thirty-one texts featuring discussion on public library makerspaces. These texts were published between 2009 and 2014, the period in which pioneering libraries developed makerspace services.  
> **Analysis.** Discourse analysis methods examine the library faith and library makerspace literatures, including metaphor analysis.  
> **Results.** The library faith has shifted from public aims, such as a strong democracy, to individualistic aims, such as a marketable skill set. Though an individualistic faith was always apparent, it is particularly so in the makerspace rhetoric.  
> **Conclusion.** A theoretical shift to include Illich’s concept of “convivial tools” can re-centre the library faith rhetoric in broadly applicable social goals, while retaining utility for grounding makerspace services.

<section>

## Introduction and background

In 1947, a research project focused entirely on public libraries, known as _The Public Library Inquiry_, looked into the politics, users, services, and impacts of public libraries in the United States. Robert D. Leigh ([1950](#lei50)), the head of the research team, evaluated the concept of the “library faith” in his report. For Leigh, the library faith “consisted in a belief in the value of the printed word, especially of the book, the reading of which is held to be good in itself or from its reading flows that which is good” (p. 12). In the early years of public librarianship, “good” reading was assumed to lead to upstanding moral citizenship, while “bad” reading led to moral dissolution and poor civic engagement ([Wiegand, 1999, p. 4](#wie99)). Molz ([1964](#mol64)) elaborated on this point, saying that “the library extracted their support from the community on the basis of two axioms: ‘One, reading is good, and two, everyone should read” (pg. 96). This was one of the many versions of the library faith.

The library literature has linked this faith to a variety of outcomes, causes, and social aims. The faith has been grounded in books and reading, in the rules of conduct, the public space, community or social engagement, and technology. The library faith is assumed to result in resilient communities, a wholesome cultural life, a thriving democracy, an educated workforce, a strong economy, and social mobility ([Augst and Wiegand, 2001](#aug01); [Buschman, 2003](#bus03); [Coleman, 2008](#col08); [Garrison, 1979](#gar79); [Gorman, 2000](#gor00), [2007](#gor07); [McFadden, 1953](#mcf53); [Molz and Dain, 1999](#mol99); [Pungitore, 1995](#pun95); [Raber, 1994](#rab94), [1997](#rab97); [Shera, 1949](#she49); [Specht Kelly, 2003](#spe03); [Van Fleet and Raber, 1990](#van90); [Wiegand, 1999](#wie99); [Williams, 1981](#wil81)). However, the assertion of the various means and ends that comprise each version of the public library faith are little studied. This paper explores the development of the library faith in light of the focus on makerspaces and other collaborative creative places within public libraries. These spaces are collaborative areas in which people come together to share tools, experiences, and knowledge, and to create tangible objects, often—but not necessarily—using digital or electronic tools ([Britton, 2012b](#bri12b)). The intent of this study is to determine which social values the library faith has historically reflected, and compare these values to those expressed by the faith described in the makerspace literature. This study asks if the library makerspace expressions of the library faith are examples of what John Buschman ([2003](#bus03), [2005](#bus05)) considers the decline of public purposes—a shifting of the focus of library services from public good to private or individual benefit. In addition, it proposes a version of a library faith centred on the goal of social justice via the means of _convivial tools_ ([Illich, 1973](#ill73)).

Although Wiegand ([1999](#wie99)) begins this project with a recounting of the earliest incarnations of library faith, and Brenda Weeks Coleman ([2008](#col08)) responds to Wiegand’s work in her dissertation about the library faith and adult education, few scholars or practitioners may know of Coleman’s work, since no articles emerged from it. Yet, as library scholars such as Harris ([1986](#har86)) and Budd ([2003](#bud03), [2007](#bud07)) note, library ideologies (re)produce and (re)frame social ideals, class relationships, and library scholarship must make these ideologies visible whenever possible. If libraries are to remain funded, relevant, and just, librarians and scholars must investigate their own ideologies to reveal hegemonic assumptions and ensure equity. At the same time, the library faith must adjust to the needs and values of the funders and community members they serve. Since makerspaces have been offered as a way to ensure library relevance to funders and the community (e.g. [Enis, 2012a](#eni12a); [Slatter and Howard, 2013](#sla13)), or are “intended to transform the library’s organizational culture” ([Goldenson and Hill, 2013, para. 5](#gol13)), then library research needs to examine how, and why, these spaces are assumed to make these changes.

</section>

<section>

## Library faith as imaginaire

The library faith is an example of an imaginaire, or imaginary. These imaginaires are collective visions about a sociotechnical system:

> often-implicit but nonetheless effective systems of images, meanings, metaphors, and interlocking explanations-expectations within which people, in specific time periods and geographical-cultural climates, enact their knowledge and subjectivities and articulate their self-understandings as knowers…An imaginary is as productive in generating and sustaining images, metaphors, and operative idea(l)s that underwrite patterns of legitimacy and credibility as it is in situating and evaluating practices of scientific inquiry. ([Code, 2006, p. 245](#cod06))

Flichy ([2007](#fli07)) argues that imaginaires are necessary to a given system’s implementation. In any system, the imaginaire is the heavy weight against which any agents of change, working inside the system or coming from the outside, must push. The imaginaire is also the weight that allows the institutional momentum to get things done. For libraries the imaginaire—the library faith—is both grand and grandiose. It acts as an idealistic vision of what is possible in a public institution, a dream of literacy, self-education, technological adroitness, civic engagement, and middle class values and behaviour. At its root, the library faith imaginaire centres on social justice<sup>[1](#fn1)</sup>, and seeks to ensure that all members of society will flourish.

This vision occasionally appears bombastic when confronted with the reality that public libraries are rarely noticed during major policy conversations (e.g. [McCook, 2000](#mcc00); [Preer, 2001](#pre01)). It can appear retrogressive or culturally imperialistic when viewed in a new era ([Ditzion, 1947](#dit47); [Harris, 1972](#har72)). Nevertheless, the only way to advocate for funding and a “place at the table” during important decisions is to express an elaborated library faith imaginaire that suits the needs of the cultural moment.

The danger of an imaginaire appears when it strays too far into the field of either unrealisable utopia, or becomes an ideology that solidifies hegemonic and unjust power structures ([Flichy, 2007](#fli07)). Since the imaginaire expresses what Lukes ([2005](#luk05)) calls the third dimension of power—a power to shape outcomes by framing issues in a particular legitimating context—its effect is subtle and difficult to empirically measure. In the case of the library faith imaginaire, few studies examine its development, the prevalence of its tenets, though some studies examine professional attitudes more broadly ([Berg and Jacobs, 2016](#ber16); [Bunge, 1999](#bun99); [Foster and McMenemy, 2012](#fos12); [Jacobs and Berg, 2011](#jac11); [Koehler, 2003](#koe03); [Vaagan and Holm, 2004](#vaa04)), or whether the faith is grounded in measurable impact in the professional or scholarly literatures.

Library scholars have rarely explored the faith as a historical concept, or within the perceptions of library personnel or users, even though this imaginaire drives how library scholars, practitioners, and our culture understands and justifies the existence of public libraries, as well as how we then interpret their services in policy, practice, and research. Given that fact, an understanding of the development and challenges of ideologies expressed through the library faith is necessary if librarians wish to ensure that the faith is grounded in socially-just practice.

</section>

<section>

## Method

The research questions for this study are:

> RQ1: What means and ends justifying library services have been identified through the use of the term “library faith”?  
> RQ2: What sorts of means and ends are identified in the library makerspace literature, and are these different from earlier versions of the faith?

To answer these questions, this discourse analysis study examines the library literature discussing the library faith, public library ideologies, missions, and reasons for existing. The literature describing the library faith was located by purposive sampling beginning with keyword searches (i.e. “library faith”) in library databases, and by pearl-growing techniques generated by those texts. To explore the emerging library faith imaginaire justifying makerspace services, I qualitatively analysed thirty-one texts featuring discussion about public library makerspaces. I selected these texts from the potentially most-influential texts published for library practitioners on the phenomenon of library makerspaces between 2009 and 2014, the years in which these spaces swiftly developed. The texts appeared in the period that I am calling “the first wave” of makerspace services in libraries, when librarians were first trying to figure out what the spaces were, and why they might be valuable. I found the articles via keyword searches in professional literature databases and online, focusing on the materials a professional might use to explore, create, and justify a makerspace service in their library<sup>[2](#fn2)</sup>. The texts include books ([Bagley, 2014](#bag14); [Burke, 2014](#bur14)), three of the first library makerspace articles published in peer-reviewed library and information science journals ([Dixon, Ward, and Phetteplace, 2014](#dix14); [Moorefield-Lang and Seadle, 2014](#moo14); [Slatter and Howard, 2013](#sla13)), electronic articles in professional journal blogs associated with periodicals ([Britton, 2012b](#bri12b); [Enis, 2012a](#eni12a), [2012b](#eni12b); [S. Epstein, 2013](#eps13); [Good, 2013](#goo13); ["Manufacturing Makerspaces," 2013](#man13); [Samtani, 2013](#sam13); [Scott, 2012](#sco12); [Sun, 2014](#sun14)), print articles from professional journals ([Britton, 2012a](#bri12a); [Britton and Considine, 2012](#bri12); [Goldenson and Hill, 2013](#gol13); [McDermott, 2012](#mcd12); [Moorefield-Lang, 2015a](#moo15a)), conference papers ([Kelly, 2013](#kel13); [Peltonen and Wickström, 2014](#pel14)), an editorial in a practitioner journal ([Colegrove, 2013](#col13)), blog posts ([Breitkopf, 2011](#bre11); [Urban Libraries Council, n.d.](#urbnd); [Doctorow, 2013](#doc13); [Fiacre, 2009](#fia09); [Hamilton, 2012](#ham12); [LibraryLab, 2012](#lib12); [Rundle, 2013](#run13); [Torrone, 2011](#tor11)), a press release from a federal funding agency—the Institute of Museum and Library Services ([IMLS, 2014](#iml14)), and an online news sources/press release regarding a public library makerspace opening ("New fab lab," 2013).

I analysed the texts by exploring instances of library faith reasoning. The makerspace literature rarely identifies “library faith” explicitly (with the exception of [Brady, Salas, Nuriddin, Rodgers, and Subramaniam, 2014](#bra14)), so this study also uses metaphor analysis to determine any associations the writers are making with these spaces ([Fernandez, 1986](#fer86)). Metaphor analysis is used to provide a window into hidden assumptions, connections, and projections of past experiences onto future expectations (e.g. [Bouthillier, 2000](#bou00); [Kuhl, 2003](#kuh03); [Markham, 2005](#mar05)). In this method, texts are examined metaphors, and the metaphors are categorized into broader types. For example the term “lab” falls under the categories of “scientific” and “industrial.” In addition, the analysis looked for descriptions of any socially-valuable ends that public library makerspace services are expected to achieve (i.e. the library faith), as well as the means by which these ends are presumed to be met. I developed _in vivo_ codes derived from the data that collated the types of faith, which were then categorized into eight ends, and ten means (see Tables 1 and 3 for these categories). I compared these building blocks of the makerspace library faith to the historical versions of the imaginaire, to see if libraries’ goals and modes of attaining these goals have shifted.

</section>

<section>

## The ends and means of faith

The development of libraries as public institutions funded by taxpayer dollars rests upon a library faith that well-read and acculturated Americans would be good citizens and voters, engaged in civic life ([Ditzion, 1947](#dit47); [Kelman, 2001](#kel01); [Kranich, 2005](#kra05); [Leigh, 1950](#lei50); [Molz and Dain, 1999](#mol99); [Willingham, 2008](#wil08)). Informed citizens result in informed voting, and a stronger, healthier democracy, thus public libraries were positioned as crucial to the United States’ continued well-being (e.g. [Kranich, 2001](#kra01)). In some versions of this faith, providing a free, impartial public space where all socioeconomic classes can and do come into contact with one another will benefit society as a whole ([Aabø and Audunson, 2012](#aab12); [Black, 2003](#bla03)).

During the early development of public libraries proponents of the library faith asserted that libraries could acculturate new immigrants into “civilised” behaviour through the mechanisms of reading room manners and wholesome reading habits ([Ditzion, 1947](#dit47)). This faith coalesced around the idea that moral uplift was a desirable social goal, necessary for poor, immigrant, or immoral members of society ([Black, 1991](#bla91); [Burgess, 2013](#bur13); [Garrison, 1979](#gar79); [Mathiesen and Fallis, 2008](#mat08)). Critical library scholar Michael Harris ([1972](#har72), [1986](#har86)) challenged this version of the faith as an elitist reproduction of middle class values. Cultural integration remains an issue of concern in public library research and the library faith, though it is often labelled _social inclusion_ now (e.g. [Caidi and Allard, 2005](#cai05); [Pateman, 1999](#pat99); [Vårheim, 2011](#var11)), and acts as a marker of social justice ([Artiles, Harris-Murri, and Rostenberg, 2006](#art06); [Jaeger, Bertot, Thompson, Katz, and DeCoster, 2012](#jae12); [Real, Bertot, and Jaeger, 2013](#rea13)).

A version of the faith developed around education ([Anderson, 1987](#and87)). In his studies on this process, library scholar John Burgess ([2013](#bur13)) argues that, “In turning away from moral uplift, librarianship turned towards the Enlightenment, with its own form of secular uplift, progress” (p. 137). This secular uplift resounds in the current incarnations of the library faith. Mathiesen and Fallis ([2008](#mat08)) describe this shift in light of intellectual freedom:

> Librarianship has since moved away from a paternalistic conception of guiding the public taste and morals to a more libertarian conception that people will be better citizens to the extent that they are free to pursue their own intellectual interests. (p. 225)

Intellectual freedom stands as a central figure in the current library faith imaginaire. Sometimes scholars identify it as a means to other socially valued ends, and sometimes as an end in itself.

<table><caption>Table 1: Many of the ends identified as "what public libraries exist to do" are identified also as the means by which public libraries attain further goals. Alternatively, the means are sometimes identified in ends in themselves. Any of the ends in this table may be accomplished by any of the means identified.</caption>

<tbody>

<tr>

<th>Library faith ends</th>

<th>Library faith means</th>

</tr>

<tr>

<td>Civic behaviour</td>

<td>Reading and books</td>

</tr>

<tr>

<td>Acculturation</td>

<td>Rules of conduct</td>

</tr>

<tr>

<td>Education</td>

<td>Spatial arrangements</td>

</tr>

<tr>

<td>Economic development</td>

<td>Professional skills and principles</td>

</tr>

<tr>

<td>Community resilience</td>

<td>Literacies or lifelong learning</td>

</tr>

<tr>

<td>Democracy</td>

<td>Social interactions</td>

</tr>

<tr>

<td>Intellectual freedom (also a means  
to the above ends)</td>

<td>Technology use</td>

</tr>

<tr>

<td>Social justice/human rights</td>

<td>Access of various types</td>

</tr>

</tbody>

</table>

Social justice, labelled as such, is a more recently-identified goal for public libraries. It is a meta-end, or the end for which all other library faith ends become the means. Within the last decade, social justice as standalone concept (and/or the allied concept of human rights) has been the focus of library articles, entire journals, conferences, and books ([Jaeger, Shilton, and Koepfler, 2016](#jae16); [Jaeger, Taylor, and Gorham, 2015](#jae15); [Mathiesen, 2013](#mat13), [2015](#mat15); [McCook and Phenix, 2006](#mcc06); [Mehra, Albright, and Rioux, 2006](#meh06); [Mehra, Rioux, and Albright, 2010](#meh10); [Pateman and Vincent, 2010](#pat10); [Rioux, 2010](#rio10); [Samek, 2007](#sam07)). Social justice, involving equitable human flourishing, sits at the broadest level of abstraction for justifying the existence of public libraries, but appears taken-for-granted in many library faith narratives.

The library faith has rested in access generally, and access to technology specifically. Access is often the mechanism by which the faith is realised, whether it is access to books and other materials, or access to technology. This access mission relates to the strong intellectual freedom tradition of librarianship which allows the library to provide unique services to society ([Molz and Dain, 1999](#mol99); [L. S. Robbins, 1996](#rob96)). However, access is a shifting concept, which challenges easy solutions or definitions. Access comprises three interlocking aspects: physical, social, and cognitive, and involves the ability to benefit from something ([Britz, Hoffmann, Ponelis, Zimmer, and Lor, 2013](#bri13); [Croteau, Hoynes, and Milan, 2012](#cro12); [Falconer, 2007](#fal07); [Ribot and Peluso, 2003](#rib03)). Access can also be construed as a negative right (i.e. no one can censor the materials you access), or a positive welfare right (i.e. we as a society have the responsibility to ensure access; [Frické, Mathiesen, and Fallis, 2009](#fri00); [Mathiesen 2009](#mat09); [Rubel, 2014](#rub14)). Physical access has traditionally been the focus of policy aimed to alleviate problems such as digital divides ([D. Epstein, Nisbet, and Gillespie, 2011](#eps11); [Murdock and Golding, 2004](#mur04); [Stevenson, 2009](#ste09); [Van Dijk and Hacker, 2003](#van03); [Warschauer, 2004](#war04)). Other aspects of access, including social and cognitive aspects have come to the fore as impacting these problems ([Britz, 2008](#bri08); [Jaeger _et al_., 2012](#jae12); [Kinney, 2010](#kin10); [Lor and Britz, 2010](#lor10); [Ribot and Peluso, 2003](#rib03)). To address these cognitive and social access barriers, libraries have stepped in to fill the needs of many communities with positive welfare right-inspired services supporting information literacies and lifelong learning ([Educational Resources Information Center, 1995](#edu95); [Gilton, 2012](#gil12); [McCook and Barber, 2002](#mcc02); [Tuominen, Savolainen, and Talja, 2005](#tuo05)). These different types of access are all visible in various incarnations of the faith.

Access to reading, books, professional expertise, and information has been a traditional means to attain library faith ends. Today, technological access is the mainstay of the public library imaginaire. E-government access, linkages to medical and other vital information, and Library 2.0 interactivity emanate from digital information and communications technologies (ICTs) that libraries provide freely and equitably ([Bertot, Jaeger, Langa, and McClure, 2006a](#ber06a); [Casey and Savastinuk, 2007](#cas07); [Visser and Ball, 2013](#vis13)). Any faith in technological access might unravel somewhat when one considers the uneven distributions of high-speed internet services, funding, and staffing that safeguards equitable access to technology or services (e.g. [Barron, 1995](#bar95); [Bertot, Jaeger, Langa, and McClure, 2006b](#ber06b); [Oyana, 2011](#oya11)). In the last several years, the call for library makerspaces has materialized as a new version of technology access-centred library faith (e.g. [Britton, 2012b](#bri12b); [McDermott, 2012](#mcd12); [Moorefield-Lang, 2015a](#moo15a)).

Other means of attaining the library faith emerged from the literature review. Some theorists believe that library rules of conduct act as a civilizing agent or tool of acculturation, for good or for ill ([Ditzion, 1947](#dit47); [Garrison, 1979](#gar79); [Harris, 1972](#har72), [1986](#har86)). Spatial arrangements and communal programs and services act as catalysts for the social interactions and discoveries that promote various library faith ends (e.g. [Aabø and Audunson, 2012](#aab12); [Aabø, Audunson, and Vårheim, 2010](#aab10); [Black and Pepper, 2012](#bla12); [Buschman and Warner, 2016](#bus16); [Garrison, 1979](#gar79); [Leckie and Hopkins, 2002](#lec02); [Rooney-Browne and McMenemy, 2010](#roo10)).

Though few studies specific to public libraries establish whether library faith assumptions are well-founded, theory and research suggests that some of the tenets of faith have a basis in reality. For example, library scholars may report their assumption that public libraries support a stronger democracy (the ends) by supplying reading materials (the means), but gather little empirical evidence that this is the case. In the area of civic engagement and democracy, some of the evidence that public libraries help rests on the concept of social capital. Researchers looking into social capital and libraries (e.g. [Ignatow _et al_., 2012](#ign12); [Vårheim, 2014](#var14)) bolster statements of correlation between public libraries and civic benefits by theorists and practitioners such as Goulding ([2004](#gou04)), Elmborg ([2011](#elm11)), Jochumsen _et al_. ([2012](#joc12)), and Kranich ([2001](#kra01)). While some study results suggest that the social capital claims for public libraries may be overstated ([Johnson and Griffis, 2014](#joh14)), research in this area is one of the empirically-explored non-reading-based library faith ends.

While the theoretical case is often strong in some instantiations of the library faith, the lack of evidence hinders research and, especially, practice ([Durrance, 1991](#dur91); [Jaeger _et al_., 2014](#jae14); [Wiegand, 1999](#wie99)). This gap between the library faith and the evidence supporting it becomes particularly troubling given the perception that librarianship is a precarious profession ([Davis, 2008](#dav08); [Dilevko, 2009](#dil09)), and libraries are being attacked, ignored or defunded ([Alexander, 2013](#ale13); [Preer, 2001](#pre01); [Pritchard, 2009](#pri09)). This perception may drive the need to reclaim the library faith in terms amenable to the larger culture.

To reinforce the legitimacy of the library’s claim on the public funds and attention, librarians appear to be situating library services, including makerspaces, in the dominant ideology of our time, neoliberalism ([Harris, Hannah, and Harris, 1998](#har98); [Irwin, 2012](#irw12); [Jensen, 2004](#jen04); [Kajberg, 2013](#kaj13); [Warner, 1990](#war90)). Neoliberalism is the political theory that “proposes human well-being can best be advanced by liberating individual entrepreneurial freedoms and skills within an institutional framework characterised by strong property rights, free markets, and free trade” ([Harvey, 2005, p. 2](#har05)). Many library theorists question whether neoliberalism is an appropriate ideology to shape library services, which traditionally conflict with the market-centered tenets advocated by neoliberalism ([Buschman, 2003](#bus03), [2012](#bus12); [Huzar, 2014](#huz14); [Stevenson, 2011](#ste11)).

</section>

<section>

## Makerspaces and the library faith

The metaphors and instrumental rationales in the professional library literature position makerspaces within a largely technological vision of libraries. In general, this instantiation of the library faith describes social benefits in terms of access to digital or electronic tools for people to self-train in economically-desirable skills. This faith advocates a culture of innovation and entrepreneurialism, with creativity acting as a product/producer of job and economic stability. The predominant metaphors used in the professional literature are scientific, industrial, or business-based, or involving a STEM (Science, Technology, Engineering, and Mathematics) mindset (see Table 2). In contrast, the metaphors used by many librarians offering makerspaces are food and nature-based, or craft and arts-centered ([Crawford Barniskis, 2015](#cra15)).

<table><caption>Table 2: Some examples of the metaphors used in makerspace rhetoric. Some of the metaphors fit in multiple categories. Science/Technology and Industrial/Business metaphors are most common. Sports/Fitness and Military/Fighting metaphors are also common.</caption>

<tbody>

<tr>

<th>Sports/Fitness</th>

<th>Military/Fighting</th>

<th>Science/Technology</th>

<th>Industrial/Business</th>

<th>Food/Plants/Nature</th>

</tr>

<tr>

<td>black-belt</td>

<td>allies</td>

<td>hard-wired</td>

<td>beta space</td>

<td>blossom</td>

</tr>

<tr>

<td>dojo</td>

<td>bombs</td>

<td>engineer</td>

<td>early adoption</td>

<td>cross-pollination</td>

</tr>

<tr>

<td>playing field</td>

<td>brawl</td>

<td>ecosystem</td>

<td>entrepreneur</td>

<td>ertile</td>

</tr>

<tr>

<td>playbook</td>

<td>infiltrate</td>

<td>blueprint</td>

<td>circuitry</td>

<td>garden</td>

</tr>

<tr>

<td>workout</td>

<td>deploy</td>

<td>experiment</td>

<td>forged</td>

<td>raw</td>

</tr>

<tr>

<td>gymnasium</td>

<td>revolution</td>

<td>laboratory</td>

<td>on demand</td>

<td>test kitchen</td>

</tr>

</tbody>

</table>

In the public library scholarly literature, makerspace studies are just beginning to appear in print. Several case studies describe the spaces and affiliated programs (e.g. [Gierdowski, Reis, Seadle, and Greifeneder, 2015](#gie15); [Peltonen and Wickström, 2014](#pel14); [Sheridan _et al_., 2014](#she14)), or the competencies needed by library personnel offering these services ([Bowler, 2014](#bow14); [Koh and Abbas, 2015](#koh15); [Parham _et al_., 2014](#par14)). The literature cites several reasons for the provision of makerspaces: to certify libraries’ technological relevance, often in a “change or die” mindset; to serve previously excluded patrons; to expand library services; to provide new venues for social engagement; to promote access to tools and skill-building opportunities; to support the local and/or global economy; and to transform the idea of the library from a sphere aimed at consuming ideas to one aimed at creating ideas ([Brady _et al_., 2014](#bra14); [Britton, 2012b](#bri12b); [Burke, 2014](#bur14); [Dixon _et al_., 2014](#dix14); [Gierdowski _et al_., 2015](#gie15); [Kelly, 2013](#kel13); [Meyer and Fourie, 2015](#mey15); [Moorefield-Lang, 2015a](#moo15a), [2015b](#moo15b); [Moorefield-Lang, Seadle, and Greifeneder, 2015](#moo15); [Slatter and Howard, 2013](#sla13)).

Most of all, the research literature, along with the professional literature, highlights the educational aspects of these spaces and positions education and literacy as the primary warrant for the spaces (e.g. [Koh and Abbas, 2015](#koh15); [Moorefield-Lang, 2015a](#moo15a)). In many cases, authors proffer _learning_ as the defining factor of makerspaces, as when Bowler ([2014](#bow14)) describes them as a “physical place in the library where informal, collaborative learning can happen through hands-on creation” (p. 59).

Brady _et al_. ([2014](#bra14)) theorize ways to ensure makerspace-type programs are accessible for users with cognitive and visual impairments. The authors link concrete suggestions of how to arrange materials and spaces with theoretical discussion of the access mandate for public libraries. This article is the only one that specifically uses the term _library faith_. Library makerspace conversations rarely make the library faith assumptions so explicit. For example, Britton ([2012b](#bri12b)) says that, “Incorporating Maker spaces into library service can have a life-altering impact on community members, who then have the tools, access, and training necessary to tinker with and remake their world.” In this faith, access to tools and training are the means by which some life-altering ends are realized.

<table><caption>Table 3: The library faith means and ends change somewhat when viewed through the lens of the public library makerspace literature.</caption>

<tbody>

<tr>

<th>Library makerspace faith ends</th>

<th>Library faith makerspace means</th>

</tr>

<tr>

<td>Education</td>

<td>Technology use</td>

</tr>

<tr>

<td>Economic development</td>

<td>Literacies or lifelong learning</td>

</tr>

<tr>

<td rowspan="2">Acculturation in 21<sup>st</sup> century thinking</td>

<td>Creativity</td>

</tr>

<tr>

<td>Play</td>

</tr>

<tr>

<td>Community resilience (occasionally noted)</td>

<td>Access to tools, educational opportunities</td>

</tr>

<tr>

<td>Social justice/human rights (occasionally noted)</td>

<td>Social interactions (occasionally noted)</td>

</tr>

<tr>

<td></td>

<td>Spatial arrangements (occasionally noted)</td>

</tr>

</tbody>

</table>

</section>

<section>

### Play, entrepreneurialism, and creativity

As with earlier library faith imaginaires, the professional makerspace rhetoric describes the social goals and values that public libraries provide to validate their existence. This rhetoric notes the means by which makerspaces enact these values, by ensuring access to tools, training, creativity, play, and learning. All of the analysed articles and books on public library makerspaces use library faith language, even when they do not identify it as such. They cite educational and economic development ends more than any of the other library faith ends, though aspects of social justice and community resilience are also addressed ([Good, 2013](#goo13)). Acculturation is addressed through the lens of 21st century skills, including “design-thinking” ([Bowler, 2014](#bow14); [Breitkopf, 2011](#bre11); [Urban Libraries Council, n.d.](#urbnd); [Doctorow, 2013](#doc13); [IMLS, 2014](#iml14); [Samtani, 2013](#sam13)). The makerspace rhetoric highlights an entrepreneurial end to which these spaces are put (e.g. [Goldenson and Hill, 2013](#gol13); [LibraryLab, 2012](#lib12)). Such entrepreneurialism is less visible in earlier discussions of the library faith. Though earlier faith discussions do note the goal of individual and community economic development, this end appears as one of the main justifications for makerspaces, with skill-building in technological literacies the primary means to that end<sup>[3](#fn3)</sup>.

Another means-end provision in this literature is the idea of play or fun. At times, the writers position play as the means to larger ends, such as education ([Bowler, 2014](#bow14); [Hamilton, 2012](#ham12); [IMLS, 2014](#iml14); [Kelly, 2013](#kel13); [Peltonen and Wickström, 2014](#pel14)). At other times, play appears as a valuable end in itself ([Britton and Considine, 2012](#bri12)). While earlier library faith discussions do note the importance of recreation, it is not a specific goal of the traditional library faith imaginaire, and was often challenged as a valid goal for libraries to pursue ([Coleman, 2008](#col08); [Leigh, 1950](#lei50); [Pungitore, 1995](#pun95)). Recreation was often considered a “steppingstone to the library's primary objective—education" ([Lee, 1966, p. 117](#lee66)), and the goals of education and other “moral uplift” goals is sometimes pitted against the less socially-valuable goal of recreation ([Garrison, 1979](#gar79); [Schrader, 1984](#sch84); [Williams, 1981](#wil81)). This tension is visible in the makerspace rhetoric, in which play is sometimes positioned as consumption, subservient to creation activities ([Britton, 2012a](#bri12a); [Meyer and Fourie, 2015](#mey15)).

</section>

<section>

### Critiquing the makerspace faith

The professional rhetoric privileges advanced technological tools and skills, essentially ignoring or dismissing more traditional types of making or the social engagement that making in shared spaces allows. These lower-tech types of making include traditional crafting. The exclusion of these types of making from some of the discussion of makerspaces could exclude some socioeconomic groups from the emerging makerplaces, including women, older adults, or those who have little prior life context to make sense of the technologically advanced making involved in digital production, or little desire to do that type of making. This change challenges the foundational value of equitable access to all types of materials and programs in public libraries.

When the makerspaces focus is on the idea that “adults create prototypes for small business products with laser cutters and 3D printers” (["Manufacturing Makerspaces," 2013, para. 1](#man13)), the social and creative aspects of making is deemphasized to highlight entrepreneurial practices. The commodity most often produced in such makerspace contexts is the worker. Dyer-Witheford ([1999](#dye99)) sees a reproduction of labour relations in the “leisure” activities that “repair and prepare” workers (p. 92). As workers or future workers come to makerspaces to create objects, they may ready themselves for more effective labour as they enjoy their hobbies. One of the reasons libraries promote creative places and activities is to build job skills in purportedly enjoyable ways ([Adler, 2012](#adl12); [Bagley, 2014](#bag14); [Doctorow, 2013](#doc13)). The worker, having learned skills in the makerspace, returns to the job market more efficient, more skilled, and more productive. Although learning, especially hands-on “makerlearning” ([Meyer and Fourie, 2015](#mey15)) is one good reason for such spaces, it is not the only valuable means/end. No library studies have focused upon the joy of making, social connections, or other non-learning aspects of making. While society benefits from an educated workforce and the personal development that education supports, critical scholars worry that information literacy or lifelong learning-based faiths are mechanistic, neoliberal, and/or present a narrow view of the public library mission ([Coffield, 1999](#cof99); [Edwards, 2010](#edw10); [Enright, 2013](#enr13); [Pawley, 2003](#paw03); [Swanson, 2004](#swa04)). Some scholars identify a narrowing of the library faith as a decline of public purposes ([Buschman, 2005](#bus05)), or technologically deterministic ([Burgess, 2010](#bur10)).

Although skill-building is valuable and necessary, in the non-library literature, explorations of maker practices offer pathways toward continued active creative processes involving ownership, authorship, and active production of one’s own existence ([Shorthose, 2004, p. 4](#sho04)). These characterizations emphasize the idea that creative practices are critical, nonviolent, and personal acts of resistance to hegemonic economic and power structures ([Tanenbaum, Williams, Desjardins, and Tanenbaum, 2013, p. 2609](#tan13)). In the public library literature, these avenues of resistance are barely visible. In addition, the broader social focus of democracy and good citizenship visible in earlier library faiths falls away in the makerspace version of the faith.

</section>

<section>

## What might be missing from the library faith?

One of the concepts absent from any library faith discussion is Illich’s ([1973](#ill73)) concept of _convivial tools_, or the idea that users of libraries can shape the library as a tool to meet the ends they find most valuable. Convivial tools ensure that users may “invest the world with their meaning, to enrich the environment with the fruits of _their_ vision and

Scholars in the discipline of science and technology studies, education, DIY, design, development, human-computer interaction, computer-supported cooperative work, and urban planning all leverage the concept of convivial tools to describe sociotechnical systems in which a multiplicity of user needs are supported by the affordances of the tool or system ([Allen, 1994](#all94); [Gauntlett, 2011](#gau11); [Grimme, Bardzell, and Bardzell, 2014](#gri14); [Johri and Pal, 2012](#joh12); [Lange, 2012](#lan12); [Petrescu, 2012](#pet12); [Pfaffenberger, 1992](#pfa92); [Ratto and Boler, 2014](#rat14)). Makerspaces, and the tools and social interactions they provide, are often called convivial tools (e.g. [Fleming, 2015](#fle15); [Nascimento, 2014](#nas14)).

While Illich identified public libraries as essentially convivial tools (p. 27), this assertion is challenged by some studies of library practices. Library users do not always get to choose the ways in which they may use libraries, or the ends to which they put them. For example, Griffis ([2013](#gri13)) describes library users’ feelings that the library is not their space, but rather belongs to the library staff (p. 175). Robbins ([1975](#rob75)) published a study examining the involvement of citizens in library decisions, and anticipated that many libraries would provide strong mechanisms for such involvement. She was surprised to note that even the most inclusive-seeming libraries had little community involvement, which dissipated at middle management levels. These findings, though dated, may still align with practice today. It is impossible to know: Despite proclamations of user empowerment and participatory services, little research has surfaced examining the degree to which libraries include community members into fundamental decision-making processes. This issue sometimes appears in discussions of “Library 2.0” or participatory libraries ([Casey and Savastinuk, 2007](#cas07); [Cuong Nguyen, Partridge, and Edwards, 2012](#cuo12); [Deodato, 2014](#deo14); [Lankes, Silverstein, Nicholson, and Marshall, 2007](#lan07); [McShane, 2011](#mcs11)). Even in this literature, any discussion of user-centred or user-driven governance mechanisms or outcomes remains largely absent. The most forceful calls for user-centred practices still emphasise a type of peripheral participation (e.g. [Lankes _et al_., 2007](#lan07)). The implementation of participatory librarianship, including the “radical trust” required for truly participatory services ([Fichter, 2006](#fic06)), may be steps toward inclusive governance, but are also possibly “conviviality masks” ([Caire, Villata, Boella, and van der Torre, 2008](#cai08)), which reinforce existing power structures while seeming to working against them. Library scholars have not yet developed research investigating the balance of a strong professional identity and conviviality, or ways that institutional actors and library users can share power to ensure conviviality and social justice.

</section>

<section>

## Conclusion

The library faith concept is useful to compare the upwelling provision of public library makerspaces to earlier initiatives and the explanations for them. While a variety of means and ends comprise earlier versions of the library faith, the makerspace instantiations of the faith focus mainly on technology, economic development, and access, rather than broader social goals such as a strong democracy, which are visible in earlier versions of the faith. However, the library makerspace faith allows several paths forward toward human flourishing, such as play, social connectivity, and community resilience, as well as education. This imaginaire could include the idea of the library as a convivial tool, with an aim toward capabilities-based social justice. The values of conviviality are the values of access and intellectual freedom, taken to a logical end. Moreover, a convivial library model ensures that power is shared equitably, that social justice is the result of the work of the institution, and that each person has the capabilities they require to accomplish the ends desirable to them ([Sen, 2009](#sen09)). A convivial distribution of power would allow users, at the very least, to decide the ends to which they put the space, even though new distributions of power and decision-making may threaten the practices that re-recreate and reinforce the library’s original reasons for existing.

Library makerspaces aimed at being convivial tools could support a wide array of tools, programs, and services, identified by the users as valuable to them. More library research needs to examine the underlying assumptions of why public libraries might matter in a society, by tracing the faith-filled expressions of librarians to actual outcomes or perceptions of library users and community members. Nevertheless, in a convivial model of librarianship, the professionals and the users become teammates seeking the same ends: ensuring that all community members can use the library as a tool to make their lives better.

* * *

<sup id="fn1">1\. The definition of social justice I am using here derives from Amartya Sen ([2009](#sen09)) and Martha Nussbaum’s ([2011](#nus11), [2013](#nus13)) work, and involves public organisations dedicated to supporting human capabilities to equally exercise agency, and balancing individual needs with the needs of the entire community or society over space and time.</sup>

<sup id="fn2">2\. In earlier interviews with nine public librarians, I asked what sorts of materials they read to support this service, and these texts were among those they identified: Crawford Barniskis, S. (2016, in press). Access and express: Professional perspectives on public library makerspaces and intellectual freedom. _Public Library Quarterly_.</sup>

<sup id="fn3">3\. In contrast, makers in non-library contexts regularly identify the social interactions as the primary benefit of their collaborative making in such spaces (e.g. [Hall, 2001](#hal01); [Kostakis, Niaros, and Giotitsas, 2014](#kos14); [Kurlinkus, 2014](#kur14); [Moilanen and Vadén, 2013](#moi13); [Purdue, Dürrschmidt, Jowers, and O'Doherty, 1997](#pur97); [Rosner _et al_., 2014](#ros14)).</sup>

## <a id="author"></a>About the author

**Shannon Crawford Barniskis** is a doctoral candidate at the University of Wisconsin-Milwaukee’s School of Information Studies, and an adjunct instructor at the University of Kentucky. She can be reached at [crawfo55@uwm.edu](mailto:crawfo55@uwm.edu)

</section>

<section>

## References

<ul>
<li id="aab12">Aabø, S. &amp; Audunson, R. (2012). Use of library space and the library as place. Library &amp; Information Science Research, 34(2), 138-149.
</li>
<li id="aab10">Aabø, S., Audunson, R. &amp; Vårheim, A. (2010). How do public libraries function as meeting places? Library &amp; Information Science Research, 32(1), 16-26.
</li>
<li id="adl12">Adler, B. (2012). Library's 'MakerSpace' encourages new ideas. Minuteman News Center. Retrieved from http://minutemannewscenter.com/articles/2012/07/03/westport/news/doc4ff2f580bafd7690453922.txt (Archived by WebCite® at <a href="http://www.webcitation.org/6hBlTcYqt" target="_blank">http://www.webcitation.org/6hBlTcYqt</a>)
</li>
<li id="ale13">Alexander, O. D. (2013). Free public libraries charging for survival. Public Library Quarterly, 32(2), 138-149.
</li>
<li id="all94">Allen, B. (1994). Cognitive abilities and information system usability. Information Processing &amp; Management, 30(2), 177-191.
</li>
<li id="and87">Anderson, D. J. (1987). From Idealism to Realism: Library Directors and Children’s Services. Library Trends, 35(3), 393-412.
</li>
<li id="art06">Artiles, A. J., Harris-Murri, N. &amp; Rostenberg, D. (2006). Inclusion as social justice: Critical notes on discourses, assumptions, and the road ahead. Theory Into Practice, 45(3), 260-268.
</li>
<li id="aug01">Augst, T. &amp; Wiegand, W. (Eds.). (2001). The library as an agency of culture (Vol. 42): Mid-America American Studies Association.
</li>
<li id="bag14">Bagley, C. A. (2014). Makerspaces: Top trailblazing projects, A LITA guide. Chicago: American Library Association.
</li>
<li id="bar95">Barron, D. D. (1995). Staffing rural public libraries: The need to invest in intellectual capital. Library Trends, 44(1), 77-87.
</li>
<li id="ber16">Berg, S. A. &amp; Jacobs, H. L. (2016). Introduction: Valuing Librarianship: Core Values in Theory and Practice. Library Trends, 64(3), 459-467.
</li>
<li id="ber06a">Bertot, J. C., Jaeger, P. T., Langa, L. A. &amp; McClure, C. R. (2006a). Drafted: I want you to deliver e-government. Library Journal, 131(13), 34.
</li>
<li id="ber06b">Bertot, J. C., Jaeger, P. T., Langa, L. A. &amp; McClure, C. R. (2006b). Public access computing and Internet access in public libraries: The role of public libraries in e-government and emergency situations. First Monday, 9. Retrieved from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/1392/1310 (Archived by WebCite® at <a href="http://www.webcitation.org/6hBlZmxuU" target="_blank">http://www.webcitation.org/6hBlZmxuU</a>)
</li>
<li id="bla91">Black, A. (1991). Libraries for the many: The philosophical roots of the early public library movement. Library History, 9(1-2), 27-36.
</li>
<li id="bla03">Black, A. (2003). False Optimism: Modernity, Class, and the Public Library in Britain in the 1960s and 1970s. Libraries &amp; Culture, 38(3), 201.
</li>
<li id="bla12">Black, A. &amp; Pepper, S. (2012). From civic place to digital space: The design of public libraries in britain from past to present. Library Trends, 61(2), 440-470.
</li>
<li id="bou00">Bouthillier, F. (2000). The meaning of service: Ambiguities and dilemmas for public library service providers. Library &amp; Information Science Research, 22(3), 243-272.
</li>
<li id="bow14">Bowler, L. (2014). Creativity through "maker" experiences and design thinking in the education of librarians. Knowledge Quest, 42(5), 58.
</li>
<li id="bra14">Brady, T., Salas, C., Nuriddin, A., Rodgers, W. &amp; Subramaniam, M. (2014). MakeAbility: Creating accessible makerspace events in a public library. Public Library Quarterly, 33(4), 330-347.
</li>
<li id="bre11">Breitkopf, M. (2011). A makerspace takes over a local library. Retrieved from http://infospace.ischool.syr.edu/2011/12/01/a-makerspace-takes-over-a-local-library/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exXCC2cY" target="_blank">http://www.webcitation.org/6exXCC2cY</a>)
</li>
<li id="bri12a">Britton, L. (2012a). A fabulous laboratory: The makerspace at Fayetteville Free Library. Public Libraries, 52. (Archived by WebCite® at <a href="http://www.webcitation.org/6exXGrrdm" target="_blank">http://www.webcitation.org/6exXGrrdm</a>)
</li>
<li id="bri12b">Britton, L. (2012b, 1 October). The makings of maker spaces, part 1: Space for creation, not just consumption. The Digital Shift: On Libraries and New Media. Retrieved from http://www.thedigitalshift.com/2012/10/public-services/the-makings-of-maker-spaces-part-1-space-for-creation-not-just-consumption/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exXaNU5h" target="_blank">http://www.webcitation.org/6exXaNU5h</a>)
</li>
<li id="bri12">Britton, L. &amp; Considine, S. (2012). The makings of maker spaces. Library Journal, 137(16), 27-31.
</li>
<li id="bri08">Britz, J. J. (2008). Making the global information society good: A social justice perspective on the ethical dimensions of the global information society1. Journal of the American Society for Information Science and Technology, 59(7), 1171-1183.
</li>
<li id="bri13">Britz, J. J., Hoffmann, A., Ponelis, S., Zimmer, M. &amp; Lor, P. (2013). On considering the application of Amartya Sen's capability approach to an information-based rights framework. Information Development, 29(2), 106-113.
</li>
<li id="bud03">Budd, J. M. (2003). The library, praxis, and symbolic power. Library Quarterly, 73(1), 19.
</li>
<li id="bud07">Budd, J. M. (2007). Self-Examination: The present and future of librarianship: Greenwood Publishing Group.
</li>
<li id="bun99">Bunge, C. A. (1999). Beliefs, attitudes, and values of the reference librarian. The Reference Librarian, 31(66), 13-24.
</li>
<li id="bur10">Burgess, J. T. F. (2010). Smart-world technologies and the value of librarianship. Computers in Libraries, 30(10), 12-16.
</li>
<li id="bur13">Burgess, J. T. F. (2013). Virtue ethics and the narrative identity of American librarianship: 1876 to present. (PhD Dissertation), The University of Alabama, Ann Arbor, MI. (3596086)
</li>
<li id="bur14">Burke, J. J. (2014). Makerspaces: A practical guide for librarians (Vol. 8). Lanham, MD: Rowman &amp; Littlefield.
</li>
<li id="bus03">Buschman, J. (2003). Dismantling the public sphere: situating and sustaining librarianship in the public philosophy. Westport, CT: Libraries Unlimited.
</li>
<li id="bus05">Buschman, J. (2005). Libraries and the decline of public purposes. Public Library Quarterly, 24(1), 1-12.
</li>
<li id="bus12">Buschman, J. (2012). Information rights, human rights, and political rights: A précis on intellectual and contextual issues for library and information science. Progressive Librarian(38/39), 17-24.
</li>
<li id="bus16">Buschman, J. &amp; Warner, D. A. (2016). On Community, Justice, and Libraries. The Library, 86(1).
</li>
<li id="cai05">Caidi, N. &amp; Allard, D. (2005). Social inclusion of newcomers to Canada: An information problem? Library &amp; Information Science Research, 27, 302-324.
</li>
<li id="cai08">Caire, P., Villata, S., Boella, G. &amp; van der Torre, L. (2008). Conviviality masks in multiagent systems. Paper presented at the Proceedings of the 7th international joint conference on Autonomous agents and multiagent systems-Volume 3.
</li>
<li id="cas07">Casey, M. E. &amp; Savastinuk, L. C. (2007). Library 2.0: A guide to participatory library service. Medford, NJ: Information Today, Inc.
</li>
<li id="edu95">Educational Resources Information Center. (1995). Public Libraries and Community-based Education: Making the Connection for Life Long Learning Paper presented at the a Conference Sponsored by the National Institute on Postsecondary Education Libraries, and Lifelong Learning, Office of Educational Research and Improvement, US Department of Education. Commissioned Papers. Accessed at http://files.eric.ed.gov/fulltext/ED385252.pdf (Archived by WebCite® at <a href="http://www.webcitation.org/6hBmZXJbr" target="_blank">http://www.webcitation.org/6hBmZXJbr</a>)
</li>
<li id="cod06">Code, L. (2006). Ecological thinking: The politics of epistemic location. Oxford: Oxford University Press.
</li>
<li id="cof99">Coffield, F. (1999). Breaking the consensus: Lifelong learning as social control. British educational research journal, 25(4), 479-499.
</li>
<li id="col13">Colegrove, P. (2013). Editorial board thoughts: libraries as makerspace? Information Technology and Libraries, 32(1), 2-5.
</li>
<li id="col08">Coleman, B. W. (2008). Keeping the faith: The public library's commitment to adult education, 1950--2006. (PhD), University of Southern Mississippi.
</li>
<li id="cra15">Crawford Barniskis, S. (2015, March 24-27). Metaphors of privilege: STEAM and public library makerspaces. Paper presented at the iConference, Newport Beach, CA. Accessed at https://www.ideals.illinois.edu/bitstream/handle/2142/73726/39_ready.pdf?sequence=2 (Archived by WebCite® at <a href="http://www.webcitation.org/6hBmjFxVz" target="_blank">http://www.webcitation.org/6hBmjFxVz</a>)
</li>
<li id="cro12">Croteau, D., Hoynes, W. &amp; Milan, S. (2012). Media/society: Industries, images, and audiences (4th ed.). Thousand Oaks, CA: Sage Publications.
</li>
<li id="cuo12">Cuong Nguyen, L., Partridge, H. &amp; Edwards, S. L. (2012). Towards an understanding of the participatory library. Library Hi Tech, 30(2), 335-346.
</li>
<li id="dav08">Davis, C. (2008). Librarianship in the 21st Century–Crisis or transformation? Public Library Quarterly, 27(1), 57-82.
</li>
<li id="deo14">Deodato, J. (2014). The patron as producer: libraries, web 2.0, and participatory culture. Journal of Documentation, 70(5), 734-758.
</li>
<li id="dil09">Dilevko, J. (2009). The politics of professionalism: A retro-progressive proposal for librarianship. Duluth, MN: Library Juice press.
</li>
<li id="dit47">Ditzion, S. H. (1947). Arsenals of a democratic culture: A social history of the American public library movement in New England and the middle states from 1850 to 1900: American library association.
</li>
<li id="dix14">Dixon, N., Ward, M. &amp; Phetteplace, E. (2014). The maker movement and the Louisville Free Public Library. Reference &amp; User Services Quarterly, 54(1), 17-19.
</li>
<li id="doc13">Doctorow, C. (2013, Feb. 24). Libraries, hackspaces and e-waste: How libraries can be the hub of a young maker revolution. Retrieved from http://www.raincoast.com/blog/details/guest-post-cory-doctorow-for-freedom-to-read-week/ (Archived by WebCite® at http://www.webcitation.org/6exYtMlI4)
</li>
<li id="dur91">Durrance, J. C. (1991). Research needs in public librarianship. In C. R. McClure &amp; P. Hernon (Eds.), Library and information science research: Perspectives and strategies for improvement (pp. 279-295). Norwood, NJ: Ablex Pubishing Corporation.
</li>
<li id="dye99">Dyer-Witheford, N. (1999). Cyber-Marx: Cycles and circuits of struggle in high-technology capitalism. Urbana: University of Illinois Press.
</li>
<li id="edw10">Edwards, R. (2010). Lifelong learning: Emergent enactments. Pedagogy, Culture &amp; Society, 18(2), 145-157.
</li>
<li id="elm11">Elmborg, J. K. (2011). Libraries as the spaces between us: Recognizing and valuing the third space. Reference &amp; User Services Quarterly, 50(4), 338-350.
</li>
<li id="eni12a">Enis, M. (2012a, May 25). To remain relevant, libraries should help patrons create. Retrieved from http://www.thedigitalshift.com/2012/05/ux/to-remain-relevant-libraries-should-help-patrons-create/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exYxPQHf" target="_blank">http://www.webcitation.org/6exYxPQHf</a>)
</li>
<li id="eni12b">Enis, M. (2012b, 3 July). Westport Library unveils new maker space. Retrieved from http://www.thedigitalshift.com/2012/07/ux/westport-library-unveils-new-maker-space/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exZ1BZkB" target="_blank">http://www.webcitation.org/6exZ1BZkB</a>)
</li>
<li id="enr13">Enright, N. F. (2013). The violence of information literacy: Neoliberalism and the human as capital. In L. Gregory &amp; S. Higgins (Eds.), Information Literacy and Social Justice: Radical Professional Praxis (pp. 15-38). Sacramento, CA: Library Juice Press.
</li>
<li id="eps11">Epstein, D., Nisbet, E. C. &amp; Gillespie, T. (2011). Who's responsible for the digital divide? Public perceptions and policy implications. The Information Society, 27, 92-104.
</li>
<li id="eps13">Epstein, S. (2013, 6 August). 3D printers – a new can of worms? Public Libraries Online. Retrieved from http://publiclibrariesonline.org/2013/08/3d-printers-a-new-can-of-worms/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exZCpp13" target="_blank">http://www.webcitation.org/6exZCpp13</a>)
</li>
<li id="fal07">Falconer, C. (2007). "You got to have FAIFE": The role of free access to information and freedom of information. Library Hi Tech News, 24, 22-25.
</li>
<li id="fer86">Fernandez, J. W. (1986). Persuasions and performances: The play of tropes in culture: Indiana University Press.
</li>
<li id="fia09">Fiacre. (2009, Oct. 5). MiniSoOnCon 2009 and why libraries should embrace maker culture. Retrieved from http://www.librarycult.com/2009/10/minisooncon-2009-and-why-libraries-should-embrace-maker-culture/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exZ6BJUH" target="_blank">http://www.webcitation.org/6exZ6BJUH</a>)
</li>
<li id="fic06">Fichter, D. (2006, April 2). Web 2.0, library 2.0 and radical trust: A first take. Retrieved from http://library2.usask.ca/,fichter/blog_on_the_side/2006/04/web-2.html (Archived by WebCite® at <a href="http://www.webcitation.org/6exZGpabv" target="_blank">http://www.webcitation.org/6exZGpabv</a>)
</li>
<li id="fle15">Fleming, L. (2015). Worlds of making: Best practices for establishing a makerspace for your school. Thousand Oaks, CA: Corwin Press.
</li>
<li id="fli07">Flichy, P. (2007). The internet imaginaire. Cambridge, MA: MIT Press.
</li>
<li id="fos12">Foster, C. &amp; McMenemy, D. (2012). Do librarians have a shared set of values? A comparative study of 36 codes of ethics based on Gorman’s Enduring Values. Journal of Librarianship and Information Science, 44(4), 249-262.
</li>
<li id="fri00">Frické, M., Mathiesen, K. &amp; Fallis, D. (2000). The ethical presuppositions behind the library bill of rights. The Library Quarterly, 70(4), 468-491.
</li>
<li id="gar79">Garrison, D. (1979). Apostles of culture: The public librarian and American society, 1876-1920. Madison: University of Wisconsin Press.
</li>
<li id="gau11">Gauntlett, D. (2011). Making is connecting (Kindle ed.). Malden, MA: Polity.
</li>
<li id="gie15">Gierdowski, D., Reis, D., Seadle, M. S. &amp; Greifeneder, E. (2015). The MobileMaker: An experiment with a mobile makerspace. Library Hi Tech, 33(4).
</li>
<li id="gil12">Gilton, D. L. (2012). Lifelong learning in public libraries: Principles, programs, and people. Lanham, MD: Scarecrow Press.
</li>
<li id="gol13">Goldenson, J. &amp; Hill, N. (2013). Making room for innovation. Library Journal, 138(9), 26-28.
</li>
<li id="goo13">Good, T. (2013). Three makerspace models that work. American Libraries, 44, 45-47.
</li>
<li id="goo11">Goodman, E. &amp; Rosner, D. K. (2011, May 7-12). From garments to gardens: Negotiating material relationships online and'by hand'. Paper presented at the Proceedings of the SIGCHI Conference on Human Factors in Computing Systems, Vancouver, BC.
</li>
<li id="gor00">Gorman, M. (2000). Our enduring values: Librarianship in the 21st century: American Library Association.
</li>
<li id="gor07">Gorman, M. (2007). The wrong path and the right path: The role of libraries in access to, and preservation of, cultural heritage. New Library World, 108, 479-489.
</li>
<li id="gou04">Goulding, A. (2004). Libraries and social capital. Journal of Librarianship and Information Science, 36(1), 3-6.
</li>
<li id="gri13">Griffis, M. R. (2013). Space, Power and the Public Library: A Multicase Examination of the Public Library as Organization Space. (Ph.D. dissertation), The University of Western Ontario. Retrieved from http://ir.lib.uwo.ca/etd/1103 (Archived by WebCite® at <a href="http://www.webcitation.org/6exZOnTjp" target="_blank">http://www.webcitation.org/6exZOnTjp</a>)
</li>
<li id="gri14">Grimme, S., Bardzell, J. &amp; Bardzell, S. (2014, Oct. 28-30). We've conquered dark: Shedding light on empowerment in critical making. Paper presented at the Proceedings of the 8th Nordic Conference on Human-Computer Interaction: Fun, Fast, Foundational, Helsinki, Finland.
</li>
<li id="hal01">Hall, P. D. (2001). Technology and community -- Elite public culture or grassroots empowerment? A case study of New Haven, Connecticut. Paper presented at the Independent Sector Spring Research Forum, Washington, D.C.
</li>
<li id="ham12">Hamilton, B. J. (2012, 28 June). Makerspaces, participatory learning, and libraries. Retrieved from https://theunquietlibrarian.wordpress.com/2012/06/28/makerspaces-participatory-learning-and-libraries/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exZThDDt" target="_blank">http://www.webcitation.org/6exZThDDt</a>)
</li>
<li id="har72">Harris, M. H. (1972). The purpose of the American public library in historical perspective: A revisionist interpretation. ERIC Clearinghouse on Library and Information Sciences, Washington, D.C.
</li>
<li id="har86">Harris, M. H. (1986). State class and cultural reproduction: Toward a theory of library service in the United States. Advances in librarianship, 14, 211-252.
</li>
<li id="har98">Harris, M. H., Hannah, S. A. &amp; Harris, P. C. (1998). Into the future: The foundations of library and information services in the post-industrial era: Greenwood Publishing Group.
</li>
<li id="har05">Harvey, D. (2005). A brief history of neoliberalism: Oxford University Press.
</li>
<li id="huz14">Huzar, T. (2014). Neoliberalism, democracy and the library as a radically inclusive space. Paper presented at the IFLA WLIC 2014 - Lyon - Libraries, Citizens, Societies: Confluence for Knowledge, Lyon, France. http://library.ifla.org/id/eprint/835 (Archived by WebCite® at <a href="http://www.webcitation.org/6exZhFIoE" target="_blank">http://www.webcitation.org/6exZhFIoE</a>)
</li>
<li id="ign12">Ignatow, G., Webb, S. M., Poulin, M., Parajuli, R., Fleming, P., Batra, S. &amp; Neupane, D. (2012). Public libraries and democratization in three developing countries: Exploring the role of social capital. Libri, 62(1), 67-80.
</li>
<li id="ill73">Illich, I. (1973). Tools for conviviality. New York: Harper &amp; Row.
</li>
<li id="iml14">IMLS. (2014, June). Talking points: Museums, libraries, and makerspaces. Retrieved from https://www.imls.gov/assets/1/AssetManager/Makerspaces.pdf (Archived by WebCite® at <a href="http://www.webcitation.org/6exZj9N4X" target="_blank">http://www.webcitation.org/6exZj9N4X</a>)
</li>
<li id="irw12">Irwin, B. (2012). The value of a communitarian approach to public library board governance: Rejecting current neoliberal practice. La valeur d'une approche communautariste de la gouvernance du conseil d'administration d'une bibliothèque publique : Le rejet de la pratique néolibérale actuelle., 36(1/2), 1-15.
</li>
<li id="jac11">Jacobs, H. L. &amp; Berg, S. (2011). Reconnecting information literacy policy with the core values of librarianship. Library Trends, 60(2), 383-394.
</li>
<li id="jae12">Jaeger, P. T., Bertot, J. C., Thompson, K. M., Katz, S. M. &amp; DeCoster, E. J. (2012). The intersection of public policy and public access: Digital divides, digital literacy, digital inclusion, and public libraries. Public Library Quarterly, 31(1), 1-20.
</li>
<li id="jae14">Jaeger, P. T., Gorham, U., Taylor, N. G., Kettnich, K., Sarin, L. C. &amp; Peterson, K. J. (2014). Library research and what libraries actually do now: Education, inclusion, social services, public spaces, digital literacy, social justice, human rights, and other community needs. The Library Quarterly, 84(4), 491-493.
</li>
<li id="jae16">Jaeger, P. T., Shilton, K. &amp; Koepfler, J. (2016). The rise of social justice as a guiding principle in library and information science research. The Library Quarterly, 86(1).
</li>
<li id="jae15">Jaeger, P. T., Taylor, N. G. &amp; Gorham, U. (2015). Libraries, human rights, and social justice: Enabling access and promoting inclusion. Lanham, MD: Rowman &amp; Littlefield.
</li>
<li id="jen04">Jensen, R. (2004). The myth of the neutral professional. Progressive Librarian(24), 28-34.
</li>
<li id="joc12">Jochumsen, H., Casper Hvenegaard, R. &amp; Skot-Hansen, D. (2012). The four spaces: A new model for the public library. New Library World, 113(11/12), 586-597.
</li>
<li id="joh14">Johnson, C. A. &amp; Griffis, M. R. (2014). The effect of public library use on the social capital of rural communities. Journal of Librarianship and Information Science, 46(3), 179-190.
</li>
<li id="joh12">Johri, A. &amp; Pal, J. (2012). Capable and convivial design (CCD): a framework for designing information and communication technologies for human development. Information Technology for Development, 18(1), 61-75.
</li>
<li id="kaj13">Kajberg, L. (2013). Re-examining the values of the public library in times of uncertainty and hardship. Bibliothek Forschung und Praxis, 37(3), 293-305.
</li>
<li id="kel13">Kelly, A. (2013). Why do we need one of those?: The role of the public library in creating and promoting makerspaces. Paper presented at the ALIA National Library &amp; Information Technicians' Symposium, Canberra, Australia.
</li>
<li id="kel01">Kelman, A. (2001). The sound of the civic: Reading noise at the New York Public Library. American Studies, 42(3), 23-41.
</li>
<li id="kin10">Kinney, B. (2010). The internet, public libraries, and the digital divide. Public Library Quarterly, 29(2), 104-161.
</li>
<li id="koe03">Koehler, W. (2003). Professional values and ethics as defined by "The LIS discipline". Journal of Education for Library and Information Science, 99-119.
</li>
<li id="koh15">Koh, K. &amp; Abbas, J. (2015). Competencies for information professionals in learning labs and makerspaces. Journal of Education for Library and Information Science, 56(2).
</li>
<li id="kos14">Kostakis, V., Niaros, V. &amp; Giotitsas, C. (2014). Production and governance in hackerspaces: A manifestation of Commons-based peer production in the physical realm? International Journal of Cultural Studies.
</li>
<li id="kra01">Kranich, N. (2001). Libraries &amp; democracy: The cornerstones of liberty. Chicago: American Library Association.
</li>
<li id="kra05">Kranich, N. (2005). Civic partnerships: The role of libraries in promoting civic engagement. Resource Sharing &amp; Information Networks, 18, 89-103.
</li>
<li id="kuh03">Kuhl, N. (2003). Metaphor matters. In K. Bridges (Ed.), Expectations of librarians in the 21st century (pp. 197-204). Westport, CT: Libraries Unlimited.
</li>
<li id="kur14">Kurlinkus, W. C. (2014). Crafting designs: An archaeology of “craft” as god term. Computers and Composition, 33, 50-67.
</li>
<li id="lan12">Lange, P. G. (2012). Doing it yourself with others. New Media &amp; Society, 14(3), 533-538.
</li>
<li id="lan07">Lankes, R. D., Silverstein, J., Nicholson, S. &amp; Marshall, T. (2007). Participatory networks: The library as conversation. Information Research, 12.
</li>
<li id="lec02">Leckie, G. J. &amp; Hopkins, J. (2002). The public place of central libraries: Findings from Toronto and Vancouver. The Library Quarterly, 72(3), 326-372.
</li>
<li id="lee66">Lee, R. E. (1966). Continuing education for adults through the american public library, 1833-1964: American Library Association.
</li>
<li id="lei50">Leigh, R. D. (1950). The public library in the United States; The general report of the Public Library Inquiry. New York: Columbia University Press.
</li>
<li id="lib12">LibraryLab. (2012, July 11). Maker station in library parking lot = all kinds of awesome. Retrieved from http://boingboing.net/2012/07/11/maker-station-in-library-parki.html (Archived by WebCite® at <a href="http://www.webcitation.org/6exaAHOxl" target="_blank">http://www.webcitation.org/6exaAHOxl</a>)
</li>
<li id="lor10">Lor, P. J. &amp; Britz, J. J. (2010). To access is not to know: A critical reflection on A2K and the role of libraries with special reference to sub-Saharan Africa. Journal of Information Science, 36(5), 655-667.
</li>
<li id="luk05">Lukes, S. (2005). Power: A radical view (2nd ed.). New York: Palgrave Macmillan.
</li>
<li id="man13">Manufacturing makerspaces. (2013). American Libraries. Retrieved from http://www.americanlibrariesmagazine.org/article/manufacturing-makerspaces (Archived by WebCite® at <a href="http://www.webcitation.org/6exaFCzmr" target="_blank">http://www.webcitation.org/6exaFCzmr</a>)
</li>
<li id="mar05">Markham, A. N. (2005). Disciplining the future: A critical organizational analysis of internet studies. Information Society, 21(4), 257-267.
</li>
<li id="mat09">Mathiesen, K. (2009, Feb. 8-11). Access to information as a human right. Paper presented at the iConference, Chapel Hill, NC.
</li>
<li id="mat13">Mathiesen, K. (2013). The human right to a public library. Journal of Information Ethics, 22, 60-79.
</li>
<li id="mat15">Mathiesen, K. (2015). Human rights as a topic and guide for LIS research and practice. Journal of the Association for Information Science and Technology, 66(7), 1305–1322.
</li>
<li id="mat08">Mathiesen, K. &amp; Fallis, D. (2008). Information ethics and the library profession. In K. E. Himma &amp; H. T. Tavani (Eds.), Handbook of information and computer ethics (pp. 221-244). New York: John Wiley and Son.
</li>
<li id="mcc00">McCook, K. d. l. P. (2000). A place at the table: Participating in community building. Chicago: ALA Editions.
</li>
<li id="mcc02">McCook, K. d. l. P. &amp; Barber, P. (2002). Public policy as a factor influencing adult lifelong learning, adult literacy and public libraries. Reference &amp; User Services Quarterly, 42(1), 66.
</li>
<li id="mcc06">McCook, K. d. l. P. &amp; Phenix, K. J. (2006). Public libraries and human rights. Public Library Quarterly, 25(1/2), 57-73.
</li>
<li id="mcd12">McDermott, I. E. (2012). Make to learn. Searcher: the Magazine for Database Professionals, 20(8), 7-11.
</li>
<li id="mcf53">McFadden, M. (1953). Objectives and functions of public libraries. Library Trends, 1, 429-436.
</li>
<li id="mcs11">McShane, I. (2011). Public libraries, digital literacy and participatory culture. Discourse: Studies in the Cultural Politics of Education, 32(3), 383-397.
</li>
<li id="meh06">Mehra, B., Albright, K. S. &amp; Rioux, K. (2006). A practical framework for social justice research in the information professions. Proceedings of the American Society for Information Science and Technology, 43(1), 1-10.
</li>
<li id="meh10">Mehra, B., Rioux, K. S. &amp; Albright, K. S. (2010). Social justice in library and information science. In M. J. Bates &amp; M. N. Maack (Eds.), Encyclopedia of Library and Information Sciences (3rd ed., pp. 4820-4836). New York: Taylor and Francis.
</li>
<li id="mey15">Meyer, A., &amp;, Fourie, I. (2015). What to make of makerspaces: tools and DIY only or is there an interconnected information resources space? Library Hi Tech, 33(4).
</li>
<li id="moi13">Moilanen, J. &amp; Vadén, T. (2013). 3D printing community and emerging practices of peer production. First Monday, 18. Retrieved from http://firstmonday.org/ojs/index.php/fm/article/view/4271 (Archived by WebCite® at <a href="http://www.webcitation.org/6exahhtiH" target="_blank">http://www.webcitation.org/6exahhtiH</a>)
</li>
<li id="mol64">Molz, R. K. (1964). The public library: The people's university? The American Scholar, 34, 95-102.
</li>
<li id="mol99">Molz, R. K. &amp; Dain, P. (1999). Civic space/cyberspace : the American public library in the information age. Cambridge, MA: MIT Press.
</li>
<li id="moo15a">Moorefield-Lang, H. (2015a). Change in the Making: Makerspaces and the Ever-Changing Landscape of Libraries. TechTrends, 59(3), 107-112.
</li>
<li id="moo15b">Moorefield-Lang, H. (2015b). User agreements and makerspaces: a content analysis. New Library World, 116(7/8), 358-368.
</li>
<li id="moo14">Moorefield-Lang, H. &amp; Seadle, M. S. (2014). Makers in the library: Case studies of 3D printers and maker spaces in library settings. Library Hi Tech, 32(4).
</li>
<li id="moo15">Moorefield-Lang, H., Seadle, M. S. &amp; Greifeneder, E. (2015). When makerspaces go mobile: Case studies of transportable maker locations. Library Hi Tech, 33(4).
</li>
<li id="mur04">Murdock, G. &amp; Golding, P. (2004). Dismantling the digital divide: Rethinking the dynamics of participation and exclusion. In A. Calabrese &amp; C. Sparks (Eds.), Toward a political economy of culture: Capitalism and communication in the twenty-first century (pp. 244-260). Lanham, MD: Rowman and Littlefield.
</li>
<li id="nas14">Nascimento, S. (2014). Critical notions of technology and the promises of empowerment in shared machine shops. Journal of Peer Production, (5). Retrieved from http://peerproduction.net/issues/issue-5-shared-machine-shops/editorial-section/critical-notions-of-technology-and-the-promises-of-empowerment-in-shared-machine-shops/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exanOfEW" target="_blank">http://www.webcitation.org/6exanOfEW</a>)
</li>
<li id="new13">New fab lab opens at the Orlando Public Library. (2013, March 25). Orlando Science Center. Retrieved from http://www.osc.org/index.php?option=com_content&amp;view=article&amp;id=1031:new-fab-lab-opens-at-the-orlando-public-library&amp;catid=3:science-park&amp;Itemid=11 (Archived by WebCite® at <a href="http://www.webcitation.org/6exb2GT3D" target="_blank">http://www.webcitation.org/6exb2GT3D</a>)
</li>
<li id="nus11">Nussbaum, M. C. (2011). Creating capabilities. Cambridge, MA: Harvard University Press.
</li>
<li id="nus13">Nussbaum, M. C. (2013). Political emotions. Cambridge, MA: Harvard University Press.
</li>
<li id="oya11">Oyana, T. J. (2011). Exploring geographic disparities in broadband access and use in rural southern Illinois: Who's being left behind? Government Information Quarterly, 28(2), 252-261.
</li>
<li id="par14">Parham, K. E., Ferri, A. M., Fan, S., Murray, M. P., Lahr, R. A., Grguric, E., . . . Meyers, E. (2014). Critical making with a Raspberry Pi: Towards a conceptualization of librarians as makers. Paper presented at the 77th ASIS&amp;T Annual Meeting, Seattle, WA. https://www.asis.org/asist2014/proceedings/submissions/posters/261poster.pdf (Archived by WebCite® at <a href="http://www.webcitation.org/6exb5RNvo" target="_blank">http://www.webcitation.org/6exb5RNvo</a>)
</li>
<li id="pat99">Pateman, J. (1999). Social exclusion: An international perspective on the role of the State, communities and public libraries in tackling social exclusion. Journal of Information Science, 25(6), 445-463.
</li>
<li id="pat10">Pateman, J. &amp; Vincent, J. (2010). Public libraries and social justice: Ashgate Publishing, Ltd.
</li>
<li id="paw03">Pawley, C. (2003). Information literacy: A contradictory coupling. The Library Quarterly, 73(4), 422-452.
</li>
<li id="pel14">Peltonen, M. &amp; Wickström, M. (2014). 3D-prints and robots play a part in my story. Participatory learning action and content creation in a library maker space. Paper presented at the IFLA WLIC 2014 - Libraries, Citizens, Societies: Confluence for Knowledge, Lyon, France.
</li>
<li id="pet12">Petrescu, D. (2012). Relationscapes: Mapping agencies of relational practice in architecture. City, Culture and Society, 3(2), 135-140.
</li>
<li id="pfa92">Pfaffenberger, B. (1992). Technological Dramas. Science, Technology &amp; Human Values, 17(3), 282-312.
</li>
<li id="pre01">Preer, J. (2001). Where are libraries in Bowling Alone? American Libraries, 32, 60-62.
</li>
<li id="pri09">Pritchard, S. M. (2009). Crises and opportunities. portal: Libraries and the Academy, 9(4), 437-440.
</li>
<li id="pun95">Pungitore, V. L. (1995). Innovation and the library: The adoption of new ideas in public libraries. Westport, CN: Greenwood Press.
</li>
<li id="pur97">Purdue, D., Dürrschmidt, J., Jowers, P. &amp; O'Doherty, R. (1997). DIY culture and extended milieux: LETS, veggie boxes and festivals. The Sociological Review, 45(4), 645-667.
</li>
<li id="rab94">Raber, D. (1994). Inquiry as Ideology: The Politics of the Public Library Inquiry. Libraries &amp; Culture, 29(1), 49-60.
</li>
<li id="rab97">Raber, D. (1997). Librarianship and Legitimacy: The Ideology of the Public Library Inquiry. . Westport, CT: Greenwood Publishing Group.
</li>
<li id="rat14">Ratto, M. &amp; Boler, M. (2014). DIY citizenship: Critical making and social media. Cambridge, MA: MIT Press.
</li>
<li id="rea13">Real, B., Bertot, J. C. &amp; Jaeger, P. T. (2013). Rural public libraries and digital inclusion: Issues and challenges. Information Technology and Libraries, 33(1), 6-24.
</li>
<li id="rib03">Ribot, J. C. &amp; Peluso, N. L. (2003). A theory of access. Rural Sociology, 68(2), 153-181.
</li>
<li id="rio10">Rioux, K. (2010). Metatheory in library and information science: A nascent social justice approach. Journal of Education for Library &amp; Information Science, 51(1), 9-17.
</li>
<li id="rob75">Robbins, J. B. (1975). Citizen participation and public library policy: Metuchen, NJ: Scarecrow Press.
</li>
<li id="rob96">Robbins, L. S. (1996). Censorship and the American Library : the American Library Association’s response to threats to intellectual freedom, 1939-1969. Westport, CT: Greenwood Press.
</li>
<li id="roo10">Rooney-Browne, C. &amp; McMenemy, D. (2010). Public libraries as impartial spaces in a consumer society: possible, plausible, desirable? New Library World, 111(11/12), 455-467.
</li>
<li id="ros14">Rosner, D. K., Lindtner, S., Erickson, I., Forlano, L., Jackson, S. J. &amp; Kolko, B. (2014, March 14-18). Making cultures: Building things &amp; building communities. Paper presented at the Proceedings of the companion publication of the 17th ACM conference on Computer supported cooperative work &amp; social computing, Vancouver, BC.
</li>
<li id="rub14">Rubel, A. (2014). Libraries, electronic resources, and privacy: The case for positive intellectual freedom. The Library Quarterly, 84(2), 183-208.
</li>
<li id="run13">Rundle, H. (2013, January 2). Mission creep – a 3d printer will not save your library. Retrieved from http://hughrundle.net/2013/01/02/mission-creep-a-3d-printer-will-not-save-your-library/
</li>
<li id="sam07">Samek, T. (2007). Librarianship and human rights: A twenty-first century guide: Elsevier.
</li>
<li id="sam13">Samtani, H. (2013, 19 June). Meet the makers: Can a DIY movement revolutionize how we learn? Retrieved from http://www.thedigitalshift.com/2013/06/k-12/meet-the-makers-can-a-diy-movement-revolutionize-how-we-learn/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exbSsTvf" target="_blank">http://www.webcitation.org/6exbSsTvf</a>)
</li>
<li id="sch84">Schrader, A. M. (1984). Toward a theory of library and information science.
</li>
<li id="sco12">Scott, S. H. (2012). Making the case for a public library makerspace. Public Libraries Online. Retrieved from http://publiclibrariesonline.org/2012/11/making-the-case-for-a-public-library-makerspace/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exbVVfb7" target="_blank">http://www.webcitation.org/6exbVVfb7</a>)
</li>
<li id="sen09">Sen, A. K. (2009). The idea of justice. Cambridge, MA: Harvard University Press.
</li>
<li id="she49">Shera, J. H. (1949). Foundations of the public library: The origins of the public library in New England 1629-1855. Chicago: University of Chicago Press.
</li>
<li id="she14">Sheridan, K., Halverson, E. R., Litts, B., Brahms, L., Jacobs-Priebe, L. &amp; Owens, T. (2014). Learning in the making: A comparative case study of three makerspaces. Harvard Educational Review, 84(4), 505-531.
</li>
<li id="sho04">Shorthose, J. (2004). A more critical view of the creative industries: production, consumption and resistance. Capital and Class, 28(3), 1-10.
</li>
<li id="sla13">Slatter, D. &amp; Howard, Z. (2013). A place to make, hack, and learn: makerspaces in Australian public libraries. The Australian Library Journal, 62(4), 1-13.
</li>
<li id="spe03">Specht Kelly, M. (2003). Revisiting C. H. Milam's What Libraries Learned from the War and rediscovering the library faith. Libraries &amp; Culture, 38(4), 378-388.
</li>
<li id="ste09">Stevenson, S. (2009). Digital divide: A discursive move away from the real inequities. The Information Society, 25(1), 1-22.
</li>
<li id="ste11">Stevenson, S. (2011). New labour in libraries: the post-Fordist public library. Journal of Documentation, 67, 773-790.
</li>
<li id="sun14">Sun, C. (2014, 11 February). Kansas boy gets new hand, created at a library makerspace. Retrieved from http://lj.libraryjournal.com/2014/02/technology/kansas-boy-gets-new-hand-created-at-a-library-makerspace/ (Archived by WebCite® at <a href="http://www.webcitation.org/6hBnOQznI" target="_blank">http://www.webcitation.org/6hBnOQznI</a>)
</li>
<li id="swa04">Swanson, T. A. (2004). Applying a critical pedagogical perspective to information literacy standards. Community &amp; Junior College Libraries, 12(4), 65-78.
</li>
<li id="tan13">Tanenbaum, J. G., Williams, A. M., Desjardins, A. &amp; Tanenbaum, K. (2013, April 27-May 2). Democratizing technology: Pleasure, utility and expressiveness in DIY and maker practice. Paper presented at the Proceedings of the SIGCHI Conference on Human Factors in Computing Systems, Paris.
</li>
<li id="tor11">Torrone, P. (2011, March 10). Is it time to retool public libraries as techshops? Retrieved from http://makezine.com/2011/03/10/is-it-time-to-rebuild-retool-public-libraries-and-make-techshops/ (Archived by WebCite® at <a href="http://www.webcitation.org/6exbeIEV2" target="_blank">http://www.webcitation.org/6exbeIEV2</a>)
</li>
<li id="tuo05">Tuominen, K., Savolainen, R. &amp; Talja, S. (2005). Information Literacy as a Sociotechnical Practice. The Library Quarterly, 75(3), 329-345.
</li>
<li id="urbnd">Urban Libraries Council. (n.d.). Cleveland Public Library makerspace. Retrieved from http://www.urbanlibraries.org/cleveland-public-library-makerspace-innovation-1003.php?page_id=283 (Archived by WebCite® at <a href="http://www.webcitation.org/6exYimlAj" target="_blank">http://www.webcitation.org/6exYimlAj</a>)
</li>
<li id="vaa04">Vaagan, R. &amp; Holm, S. (2004). Professional values in Norwegian librarianship. New Library World, 105(5/6), 213-217.
</li>
<li id="van03">Van Dijk, J. &amp; Hacker, K. (2003). The digital divide as a complex and dynamic phenomenon. The Information Society, 19(4), 315-326.
</li>
<li id="van90">Van Fleet, C. &amp; Raber, D. (1990). The public library as a social/cultural institution: Alternative perspectives and changing contexts. In K. d. l. P. McCook &amp; D. P. Wallace (Eds.), Adult Services: An Enduring Focus for Public Libraries. Chicago: American Library Association.
</li>
<li id="var11">Vårheim, A. (2011). Gracious space: Library programming strategies towards immigrants as tools in the creation of social capital. Library &amp; Information Science Research, 33, 12-18.
</li>
<li id="var14">Vårheim, A. (2014). Trust in libraries and trust in most people: Social capital creation in the public library. The Library Quarterly, 84(3), 258-277.
</li>
<li id="vis13">Visser, M. &amp; Ball, M. A. (2013). The middle mile: The role of the public library in ensuring access to broadband. Information Technology and Libraries, 29, 187-194.
</li>
<li id="war90">Warner, A. S. (1990). Librarians as money makers: The bottom line. (cover story). American Libraries, 21, 946-948.
</li>
<li id="war04">Warschauer, M. (2004). Technology and social inclusion: Rethinking the digital divide. Cambridge, MA: MIT Press.
</li>
<li id="wie99">Wiegand, W. (1999). Tunnel vision and blind spots: What the past tells us about the present; reflections on the twentieth-century history of American librarianship. The Library Quarterly, 69(1), 1-32.
</li>
<li id="wil81">Williams, R. V. (1981). The public library as the dependent variable: Historically oriented theories and hypotheses of public library development. The Journal of Library History, 16(2), 329-341.
</li>
<li id="wil08">Willingham, T. L. (2008). Libraries as civic agents. Public Library Quarterly, 27, 97-110.
</li>
</ul>

</section>

</article>