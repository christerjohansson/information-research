<article>

<header>

#### vol. 22 no. 1, March, 2017

## Proceedings of the Ninth International Conference on Conceptions of Library and Information Science, Uppsala, Sweden, June 27-29, 2016

</header>

# Genre, format and medium across the information professions

#### [Tim Gorichanaz](mailto:gorichanaz@drexel.edu)

> **Introduction.** Convergence in the information professions has brought certain tensions to the fore. This motivates further research into key disciplinary concepts, which can be approached through the study of vocabulary.  
> **Method.** A survey of diverse information professionals was conducted to explore their use and understanding of genre, format and medium. Participants described multiple manifestations of textual, visual and auditory works according to these terms. Afterwards, participants gave their definitions of these terms, plus form and mode.  
> **Analysis.** Data were analysed qualitatively and quantitatively to address three research questions: What do these terms mean to working information professionals? How are they used in different cases? Are these terms understood differently by different types of information professionals?  
> **Results.** The essential themes that characterize each term are discussed. While different professionals see different nuances in these concepts, the differences do not seem to be related to their field, supporting the notion of convergence in the information disciplines.  
> **Conclusions.** Tensions between research and practice are reflected in different ways of arriving at and employing definitions. This study brings to light common essences that have been obscured by these tensions, opening the door to further research that seeks to bridge research & practice.

<section>

## Introduction: convergence in the information disciplines

Libraries, archives and museums maintain different sorts of collections which are used by different constituents for different purposes, but the structure of their activities is largely similar. This similarity was acknowledged in the literature by Hjerppe ([1994)](#hje94), who coined the term ‘memory institutions’ in his discussion of the notion of a generalized document, perhaps following the lead of White ([1992](#whi92)), who characterized the object of information science as ‘external memory’. The shared vision of memory institutions was articulated by Dempsey ([1999](#dem99)) and has since been the subject of much discussion, such as Bates’ ([2015](#bat15)) integrative model of the information professions.

This convergence is not merely terminological; rather, it is evidenced by numerous real-world memory institution mergers. For instance, Library and Archives Canada was created in 2004 from the previously distinct National Library and National Archives ([Doucet, 2007](#douc07)). Given and McTavish ([2010](#giv10)) note that this convergence may have resulted from the budgetary realities of shared staff and space as well as changing information practices in the digital age; in their view, it has also benefited from conceptual affinities among the information disciplines and their shared histories as organizations built around sharing knowledge.

Springing, perhaps, from this epistemological link among the memory institutions, the convergence in practice has been accompanied by convergence in theory. Consistent with Given and McTavish ([2010](#giv10)), Buckland ([2012](#buc12)) presents a unified conceptualization of information science as a research discipline. Indeed, this shared research agenda is reflected in recent efforts to rechristen the umbrella discipline of library and/or information science/studies, alias documentation (among a slew of other contending names, discussed in [Hjørland, 2014](#hjo14)). Settling on a name for the discipline has proven difficult—and more important than Juliet might have suggested (‘What’s in a name?’). For example, Hjørland ([2000](#hjo00)) discusses how the problems that arose in adopting the term information rather than the previously-preferred documentation reflect deep-seated, problematic assumptions. Furner ([2015](#fur15)), too, criticizes the field’s purported focus on the concept of information (and, moreover, status as a ‘science’).

These tensions bespeak a need for further clarity on key concepts in our discipline. The value of conceptual research in our discipline is only beginning to be widely acknowledged; our conceptual development still lags ([Gorichanaz, 2015](#gor15)). In Buckland’s ([1999](#buc99)) view, conceptual limitations are revealed through problems in vocabulary. Day ([2000](#day00)) agrees that vocabulary is central to our discipline, and that ‘the limits and possibilities of vocabulary as a whole are little discussed’ (p. 810). Thus investigations of vocabulary emerge as one horizon for conceptual research in our discipline. In the past few decades, some discussions around vocabulary have emerged, but these tend to focus on select terms, such as information and knowledge; as Hjørland ([2014](#hjo14)) points out, any number of other important concepts remain unarticulated, and these ‘basic concepts are not _given_’ ([Hjørland, 2014, p. 231,](#hjo14) emphasis his).

In this paper, I seek to contribute to the conceptual basis of our discipline by exploring the terms genre, format and medium, as well as form and mode. To do so, I compare how these terms have been used by both researchers and practitioners in the literature. I then present findings from a survey in which I asked information professionals in diverse fields to first operationalize and then conceptualize these terms to see how the terms are understood differently from field to field, ultimately with an eye toward extracting their essences. These essences are then discussed. All in all, this focussed exploration serves to make abstract concepts more concrete. I also find that, while different professionals do see different nuances in these concepts, the differences are not related to their field, supporting the notion of convergence in the information disciplines.

In this work, I hope to foster synergy between research and practice. Affinities notwithstanding, certain tensions have prevented strong integration between research and practice in our discipline. This is, perhaps, partly because of the fundamental difference between context-bound, fast-paced and pragmatic practitioners and the broad-viewed, slow-moving and meditative world of research. Fidel (2012, pp. 56–58) describes the mutual frustration of both camps as each effectively ignores the knowledge gained by the other. In seeking conceptual clarity by taking into account the way practitioners apply certain concepts in their work, I hope to avoid perpetuating this behavior.

## Literature review

### The practicality of the conceptual

One task that information professionals regularly face is the description of objects (not necessarily physical) in their collections. This description is done chiefly in order to aid in retrieval. Many aspects of objects can be described, including both content and form. Zinkham, Cloud and Mayo ([1989](#zin89)) argued for the utility of form headings such as form and genre, which they defined through lists of examples, and called for their increased use and further cooperation to ensure effective access to objects in collections. Since their writing, form headings have become more widely used, but problems remain. Caudle and Schmitz ([2014](#cau14)), for instance, emphasize how new and complex document forms are unaddressed by existing cataloguing systems.

Retrieval is most effective when an institution uses a controlled vocabulary. In our clime of convergence, dreams of a universal vocabulary for all memory institutions have surfaced. These dreams have been most fully realized, arguably, in Machine Readable cataloguing (MARC), which is ‘emerging as a new de facto international standard’ ([Bawden and Robinson, 2012, p. 109](#baw12)). Developing such a universal vocabulary for describing objects is challenging for a number of reasons, most chiefly because different constituents have different needs: objects can differ vastly in their characteristics (consider the differences between a digital book, an antique sculpture, a piece of music and a geological specimen); which characteristics are relevant for retrieval can likewise vary; and the needs of catalogers and audiences across time and space must also be taken into account.

Young and Mandelstam ([2013](#you13)) document the development of a controlled vocabulary with universal aspirations: the Library of Congress genre/form terms thesaurus, which is intended for use by an immensely diverse audience for the description of moving images, sounds, maps, legal works, literature, music and religious objects. This account suggests that, even considering diverse audience and objects, convergence is possible. If, that is, the headings rest on solid conceptual ground. In developing the genre/form terms thesaurus, the Library of Congress found conceptual work to be critical: ‘Significant decisions about the scope of the new thesaurus had to be made as headings were selected for inclusion’ ([Young and Mandelstam, 2013, p. 8](#you13)). ‘For instance, the decision to exclude nationality and language from the genre/form thesaurus was relatively easy once the definitions of genre and form were examined’ ([Young and Mandelstam, 2013, p. 9](#you13)). In other words, practical breakthroughs can arise from conceptual research.

### Genre, format and medium: An exploratory disentanglement

Though considering definitions was effective in the development of the genre/form thesaurus, as discussed above, the employed definitions were not without their limitations. In this sense, the research conducted could have been more rigorous. The definitions used by the Library of Congress were drawn from the extant guides _Moving image materials: genre terms and Moving image genre–form guide_.

*   Genre was understood as: _‘any recognized category of fictional works which is characterized by recognizable conventions [and] contain[s] conventions of narrational strategy and organizational structure, using similar themes, motifs, settings, situations, and characterizations’_ ([Young and Mandelstam, 2013, p. 8](#you13)).
*   And form: _‘any recognized category of works characterized by a particular format or purpose … and which are separate from its actual content, not necessarily implying a particular narrative construction’_ ([Young and Mandelstam, 2013, p. 8](#you13)).

While these definitions were helpful in formulating the thesaurus, they raise a number of questions when analysed: Why do some conventions cohere into genres, while others do not, and how does this occur? What is meant by format? How does format differ from form? How can ‘purpose’ be disentangled from ‘narrative construction’? Moreover, these definitions do not seem to aid in the conveying of the essence of the concepts of genre and form; practitioners seem, as it were, encouraged to simply memorize their laundry list rather than internalize the underlying principles. In this way, the definitions have limited utility for complex and new types of objects, to echo Caudle and Schmitz ([2014](#cau14)).

In search of the essences of these concepts, one might refer to the scholarly literature. Genre, to start with, has preoccupied the pages of countless publications. Andersen ([2008](#and08)) reviews this literature as it relates to information studies, arguing for an understanding of genre as posited by Miller ([1984](#mil84)), which sees genre not as a descriptor of an information object in isolation, but rather one that considers the production and consumption of that information.

This suggests an interplay between genre and form, though the nature of this interplay remains to be clarified. To this end, Yates and Orlikowski ([1992](#yat92)) articulate a view of genre that subsumes both purpose (communicative intent and rhetorical features) and form (readily observable features, such as layout and medium). This supports the finding of Clark, Ruthven, Holt, Song and Watt ([2014](#cla14)) that form (defined as layout and formatting) is the way that genre is recognized in reading emails. Still, the interface between genre and form remains to be articulated with clarity. Indeed, Graham ([2008](#gra08)) suggests that the complexity of the relationship between mode, medium and genre is little-acknowledged.

hough the concept of genre, as outlined above, has been much-discussed, it hinges on understandings of format and medium that are often left implicit. A deeper understanding of genre, then, requires a deeper understanding of format and medium.

The concept of format, it seems, is largely unexplored in the literature. A survey of academic publications using the term reveals that the term format is used primarily as an ad-hoc differentiator without explicit definition (e.g., digital vs. physical, electronic vs. print), secondarily in phrases such as ‘MARC formats,’ and tertiarily as a vague way to imply how something is represented. In the MARC 21 documentation, the only mention of format is in ‘Format of Notated Music,’ where it refers to the ‘musical or physical layout of the content’ ([Library of Congress, 2015b, para. 1](#loc15b)). A substantial discussion of the concept is found in Genette’s ([1997](#gen97)) Paratexts, where it is presented in relation to genre:

_The most all-embracing aspect of the production of a book—and thus of the materialization of a text for public use—is doubtless the choice of format. Over time, the meaning of this word has changed once or twice. Originally it designates two things: one is the manner in which a sheet of paper is or is not folded to end up as the “leaves” of a book (or, in common parlance, as the book’s pages, one recto-verso leaf naturally making two pages, even if one of the two remains blank); the other is the size of the original sheet itself, conventionally designated by a type of watermark (shell, Jesus, bunch of grapes, and so forth). The manner of folding thus did not by itself indicate the flat dimensions of a book; but it quickly became a shorthand way of estimating them: a folio volume (folded once, hence two leaves, or four pages per sheet), or a quarto volume (folded twice, hence four leaves, or eight pages per sheet) was a large book; an octavo volume (eight leaves [8vo]) was a medium book; and a duodecimo (12mo), a sextodecimo (16mo), or an octodecimo (18mo), a small book. In the classical period, “large formats” (quarto) were reserved for serious works (that is, works that were religious or philosophical rather than literary) or for prestige editions that enshrined a literary work._ ([Genette, 1997, p. 17](#gen97))

Genette goes on to describe how, over time, ‘format’ became less associated with standardized sizes and more approximate ones: best-sellers, for instance, are printed in large format for advertising purposes. This understanding of format as a measure of size, of course, does not take into account the lay understanding of format as arrangement (i.e., how ‘format’ is used in a word processor), which also seems to be encapsulated in the invocation of format in the above-cited definitions of genre and form. A satisfactory exploration of the concept of format, then, should incorporate both these senses.

Another word wrapped up with genre and important in the description of objects is medium, which suffers the same problems as genre, form and format. MARC 21 includes a few mentions of medium/media:

*   337 Media Type: ‘Media type reflects the general type of intermediation device required to view, play, run, etc., the content of a resource’ ([Library of Congress, 2015a](#loc15a)).
*   340 Physical Medium: ‘Physical description information for an item that requires technical equipment for its use or an item that has special conservation or storage needs’ ([Library of Congress, 2011](#loc11)).
*   382 Medium of Performance: ‘For manifestations: the instrumental, vocal, and/or other medium of performance embodied in the manifestation. For works and expressions: the instrumental, vocal, and/or other medium of performance for which a musical work was originally conceived or for which a musical expression is written or performed’ ([Library of Congress, 2015c](#loc15c)).

Again, these definitions do little to convey the essence of the term medium. That is, what ties these different senses of medium together? Similarly, Graham ([2008](#gra08)) exemplifies medium as ‘Internet, computer, Flash’ (p. 87) but does not seem to engage conceptually with the notion of medium itself.

## Research Questions

The above discussion presents an opportunity to explore the essences of the concepts genre, format and medium as used in the information disciplines, particularly in light of our contemporary (re)convergence—and with an eye toward bridging research and practice.

To this end, the following research questions are explored in this study:

*   How are the terms genre, format and medium used in different cases?
*   Are the terms genre, format and medium understood differently by different types of information professionals?

## Data collection

This research was conducted as an online survey hosted by SurveyMonkey, which was distributed to information professionals working at a number of diverse memory institutions in Philadelphia. These memory institutions included:

*   public, academic and special-collections libraries;
*   museums of art, local history, natural history, cultural history and medicine;
*   literary, historical, religious and scholarly archives;
*   gardens and arboreta;
*   historical societies.

Prospective participants were found on the public websites of the memory institutions; those considered eligible had job titles that included ‘librarian,’ ‘archivist,’ ‘curator,’ ‘collections manager,’ etc. Two hundred prospective participants were contacted with a link to the online survey in December 2015\. The survey remained open for two weeks, during which one reminder email was sent to those who had not yet participated. Eighty-three participants completed the survey.

The survey began with demographic questions regarding the participant’s education and current role. After that, the participants were asked to consider a number of items (presented as textual descriptions) and then describe the genre, format and medium of those items in open text areas. Items were selected to represent an array of document types, including textual, visual and auditory information in analog and digitized manifestations. Though breadth was sought, it was also desirable to keep the survey as short as possible in order to maximize participation. The items included in the survey are reproduced in Table 1.

<table><caption>Table 1: Items in the survey. The letters correspond to the order of presentation.</caption>

<tbody>

<tr>

<th> </th>

<th>Handmade</th>

<th>Analog/mechanical</th>

<th>Digitized</th>

</tr>

<tr>

<td>

**Textual**</td>

<td>

C. The manuscript of Ulysses, by James Joyce, written on paper by his own hand in the 1910s. It is not bound; rather, the manuscript is a stack of loose papers that are kept together in a box.</td>

<td>

A. The first edition of the novel The Goldfinch, by Donna Tartt, published as a clothbound hardcover by Little, Brown, in October 2013.</td>

<td>

B. The Kindle edition of the novel The Goldfinch, by Donna Tartt, published electronically by Little, Brown, in October 2013 and distributed by Amazon.com.</td>

</tr>

<tr>

<td>

**Visual**</td>

<td>

D. The actual, framed painting Vase with Twelve Sunflowers (Arles), by Vincent van Gogh in 1889\. The painting was done in oil on canvas and measures 92x72.5 centimeters. It depicts a diverse set of orange and yellow sunflowers in a yellow vase on a golden table against a blue background.</td>

<td>

E. A digitized image of the painting Vase with Twelve Sunflowers (Arles), by Vincent van Gogh in 1889\. This is a jpeg file measuring 550x684 pixels. The original painting was done in oil on canvas. It depicts a diverse set of orange and yellow sunflowers in a yellow vase on a golden table against a blue background.</td>

<td> </td>

</tr>

<tr>

<td>

**Auditory**</td>

<td> </td>

<td>

F. A magnetic-tape compact cassette containing the oral history interview with New York artist Philip Guston conducted by Joseph Trovato in January 1965\. The audio is 15 minutes, 27 seconds long.</td>

<td>

G. A digital audio file of the oral history interview with New York artist Philip Guston conducted by Joseph Trovato in January 1965\. The audio is 15 minutes, 27 seconds long and is an mp3 that occupies 21 megabytes.</td>

</tr>

</tbody>

</table>

After providing the genre, medium and format for each of these items, participants were asked to provide their own definitions for several terms in open text fields. These terms were:

*   genre
*   format
*   medium
*   form
*   mode

Finally, there was an open text area at the end of the survey for participants to include other terms they used to describe objects in their work beyond the ones surveyed here, as well as a final text area for additional comments. The survey was constructed such that participants could revisit and potentially revise previous answers if they chose to do so, though this was not explicitly mentioned.

## Analysis and findings

The survey data were analysed using both qualitative and quantitative methods. The specific analytical methods and corresponding findings are presented below for each of the research questions articulated above.

> RQ1: What do the terms genre, format and medium mean to working information professionals, and how are they related?

To address this question, thematic analysis ([Braun & Clarke, 2006](#bra06)) was conducted on the definitions provided by the participants in the latter part of the survey. The analysis for each term proceeded in this way: The definitions were aggregated and read several times. During these initial readings, informal notes were made of recurring themes; these notes functioned as a tentative, open list for codes. During a subsequent reading, these codes were applied formally. In the next reading, the coding scheme was refined and abstracted. Based on the frequency of and relationship among these codes, themes were extracted which were meant to represent the essence of each term.

### Genre

For this term, 53 definitions were analysed. Genre was largely understood to be a means of categorizing items (found in 41 definitions), either by focusing on the subject matter (specified in 14 definitions) or some aspect of the material (specified in 4 definitions). It was pointed out that specific genres vary by field, and are selected for their relevance to that field.

Example definitions included:

*   ‘_group of items that share the same format or content_’
*   ‘_Some genres are more closely linked than others to the physical characteristics of an item’._
*   ‘_often restricted to literary or artistic forms’_
*   _‘group of items that share the same format or content_’
*   ‘_type of content_’

### Format

For this term, 52 definitions were analysed. Format was understood to be a description of an object’s physical arrangement/structure (found in 25 definitions), as in file structure or physical dimensions, possibly regarding either how the object is presented (specified in 13 definitions) or how it was created (specified in 4 definitions). For 6 participants, format refers to a container/storage for information. Five participants saw format as a subtype of or synonym for medium./p>

Example definitions included:

*   ‘_physical package of the information object_’
*   _‘the method in which the object is displayed or viewed by audience_’
*   ‘_physical embodiment of object (even digital files)_’
*   ‘_classification of how item is presented (book on paper, ebook, jpeg, TIFF, mp3)_’
*   ‘_the precise form of the medium on which something is recorded_’

### Medium

For this term, 53 definitions were analysed. Medium is largely understood as a description of how an object was created, either in terms of the physical materials used (specified in 38 definitions) or the processes of production (specified in 27 definitions). Secondarily, medium is understood as the process of conveying information (found in 10 definitions) through the object.

*   ‘_materials or techniques from which object is made_’
*   ‘_physical structure_’
*   ‘_materials object was made from and/or way object is presented_’
*   ‘_communication method_’
*   ‘_Of course it really depends on the institution and the standards in place_’.

### Form

For this term, 31 definitions were analysed. This lower number (many participants left this item blank) suggests that this term is in less common usage than genre, format and medium. Form is a means of describing the physical shape or attributes of an object (15 definitions). Five participants understood it as a ‘detailed version’ of format, while four understood it as a detailed version of medium.

*   ‘_shape of something_’
*   ‘_format_’
*   ‘_shape and structure of an object’_
*   ‘_How the content is organized/expressed_’

### Mode

For this term, 29 definitions were analysed. Again, this term may not be a common one. Mode describes how an object is presented (9 definitions), how it is created (2 definitions) or how it is experienced or used (3 definitions). In this sense, mode can be understood as adverbial. Again, some participants saw mode as a synonym or subtype of format (3) or medium (2). Finally, the term mode can also be used as a way to describe the thoroughness of a cataloguing description.

*   ‘_the way something is presented_’
*   ‘_More detailed category of the items medium_’
*   ‘_How the object format is experienced_’.
*   ‘_approach to cataloguing, degree of detail, standard for final product_’
*   ‘_How an object can be transmitted to the viewer_’

### Relationships

For a given object, it seems that these participants conceptualize genre primarily as a descriptor of content and format primarily as a descriptor of arrangement, but these two overlap. Medium seems primarily to be processual, and perhaps it can be understood as the means by which the genre+format unit is conveyed. Form seems most related to format and seems to entail a more idiographic description of an object’s physical properties, rather than a nomothetic one, as in naming a format. Finally, mode is related to medium in its processual aspect and may also be considered idiographic.

#### RQ2: How are the terms genre, format and medium used in different cases?

To address this question, participants’ descriptions of the genre, medium and format of the seven items (Table 1) were analysed. First, responses were cleaned for consistency; capitalization and pluralization were regularized, spelling was corrected, etc. Then counts were conducted of the frequency of each answer. These results are presented in Table 2.

<table><caption>Table 2: Three most frequent responses for each item, along with a count for each. The number in parentheses represents the number of different responses.</caption>

<tbody>

<tr>

<th></th>

<th>A. Hardcover</th>

<th>B. Kindle</th>

<th>C. Manuscript</th>

<th>D. Painting</th>

<th>E. Jpeg</th>

<th>F. Cassette</th>

<th>G. MP3</th>

</tr>

<tr>

<td>

**Genre**</td>

<td>Fiction 59 Novel 13 Literature 5 (7)</td>

<td>Fiction 57 Novel 14 Book 3 (8)</td>

<td>Fiction 39 Novel 15 Literature 6 (8)</td>

<td>Still Life 29 Art 16 Painting 7 (9)</td>

<td>Art 7 Post-Impr’ism 6 Impr’ism 6 (16)</td>

<td>Oral History 38 Interview 16 Recording 2 (10)</td>

<td>Oral History 38 Interview 15 Recording 3 (8)</td>

</tr>

<tr>

<td>

**Medium**</td>

<td>Book 30 Paper/Ink 27 Print 18 (8)</td>

<td>Digital 16 Electronic 15 Book 11 (15)</td>

<td>Paper/Ink 33 Manuscript 25 Text 5 (6)</td>

<td>Oil/Canvas 58 Painting 7 Visual Art 3 (4)</td>

<td>Digital Img 13 Oil/Canvas 9 Digital File 7 (16)</td>

<td>Tape 18 Recording 13 Cassette 11 (11)</td>

<td>Digital Audio File 9 Recording 9 Audio 8 (17)</td>

</tr>

<tr>

<td>

**Format**</td>

<td>Book 28 Hardcover 25 Print 13 (11)</td>

<td>E-Book 31 Electronic 15 Kindle 7 (11)</td>

<td>Manuscript 53 Paper/Ink 10 Text 3 (9)</td>

<td>Painting 35 Framed 16 Oil/Canvas 9 (9)</td>

<td>Jpeg 28 Reproduction 9 Digital 8 (13)</td>

<td>Cassette 35 Tape 9 Recording 6 (10)</td>

<td>MP3 25 Digi Audio File 16 Digital 9 (9)</td>

</tr>

</tbody>

</table>

Based on the nature of these responses, genre, medium and format can be summarized as follows:

*   Genre: classification based on content (fiction, still life)
*   Medium: physical materials (paper/ink, oil/canvas), how it was made (recording)
*   Format: name for the way information is organized (book, jpeg, mp3)

> RQ3: Are the terms genre, format and medium understood differently by different types of information professionals?

To address this question, a Pearson’s chi-squared test was conducted using SPSS. This technique tests for independence between two sets of categorical data—in this case, the participants’ descriptions of the items and their roles. Information on roles was self-reported; at the beginning of the survey, participants were asked, ‘Do you identify as any of the following? Check any/all that apply,’ and presented with checkboxes for: Archivist; Bibliographer; Curator; Librarian; Records Manager; Researcher; Other (please describe). Answers for Other were considered if at least two participants provided the same answer; all participants identified with at least one role (with a mean of 2.14 roles per participant). These answers are summarized in Table 3\.

<table><caption>Table 3: Participants’ roles.</caption>

<tbody>

<tr>

<th>Role</th>

<th>

_n_</th>

</tr>

<tr>

<td>Archivist</td>

<td>25</td>

</tr>

<tr>

<td>Bibliographer</td>

<td>6</td>

</tr>

<tr>

<td>Curator</td>

<td>32</td>

</tr>

<tr>

<td>Educator</td>

<td>25</td>

</tr>

<tr>

<td>Librarian</td>

<td>32</td>

</tr>

<tr>

<td>Records Manager</td>

<td>15</td>

</tr>

<tr>

<td>Researcher</td>

<td>31</td>

</tr>

<tr>

<td>Artist</td>

<td>3</td>

</tr>

<tr>

<td>Administrator</td>

<td>2</td>

</tr>

<tr>

<td>Collections Manager</td>

<td>5</td>

</tr>

<tr>

<td>Conservator</td>

<td>2</td>

</tr>

</tbody>

</table>

The initial chi-squared test found the relationship between role and description to be largely insignificant. A second test was conducted without outliers: those descriptions that had only one or two mentions were removed; the chi-squared test requires as few categories as possible with less than five cases, and removing outliers was a step toward achieving this. Yet in this analysis, too, very few relationships were significant. Out of 231 relationships tested (each role from Table 3 with each cell in Table 2), 27 measured significant to p < 0.05\. This is not deemed meaningful, as in such a large pool of tests it is expected that some relationships will measure as significant purely by chance. Finally, another chi-squared test was conducted to look for a relationship between those participants who held an MLIS degree versus those who did not; like the other tests, this one also showed no significance.

These tests suggest that, though understandings of these terms vary from person to person, they do not necessarily have to do with that person’s specific information profession.

## Discussion and conclusion

This study allows for the comparison between the definitions as conceptualized by the participants (RQ1) and the definitions as operationalized (RQ2). Overall, the conceptually-derived and operationally-derived definitions are quite consistent with each other. For instance, though the conceptualization of genre included descriptions of both subject matter and physicality, the operational notion of genre seemed overwhelmingly to focus on subject matter, whereas the issue of physicality was taken up by medium. Regarding format, the conceptualized definition referenced the physical arrangement of information, while the operationally-derived definition revealed an emphasis on naming socially-established arrangements of information, similar to genre. In general, the operational definitions seem to be narrower, perhaps because they relate to concrete cases (narrow) rather than generalizations (broad). This is an important insight: It suggests that, when describing new or complex information objects, information professionals should consider the conceptual, broad definition, as a narrower definition may not include aspects that are salient in the novel object. This was reflected in the open-comment response of one participant: ‘Things get really nuts the further one strays from the traditional means of communicating information. Like, a conservation x-ray of the van Gogh sunflowers, pre-conservation’.

Detached, conceptual thinking may be non-customary for many information professionals, as they generally work with specific types of objects and in specific contexts. This seems to reflect the difference between inductive and deductive routes to arriving at definitions, as well as different values regarding generalizability, which characterize the difference between practitioners and researchers. For some participants, this resulted in difficulty responding to questions on the survey. One participant wrote in their final comments, for instance: ‘Words like medium, mode, format have several meanings depending on the context. I’d bet a person’s professional experience most informs their definition of the word’. Similarly, another participant said: ‘Asking these questions without the context of a larger finding aid or catalogue is very confusing’. These comments recall the viewpoint of Kwaśnik and Crowston ([2005](#kwa05)), who argued that concepts such as genre are delineated according to the specific needs of the application at hand (e.g., classification need, research question). Similarly, Clark, Ruthven, Holt, Song and Watt ([2014](#cla14)) write:

> _Any thorough book or literature review on genre … will divulge an overall lack of consensus on an appropriate definition of genre because so many questions remain unanswered with regard to how genres are replaced, created, evolve, function, overlap and interact with each other, which rules and patterns constitute a genre and how these characteristics are perceived._(p. 177)

To this end, it is notable that all definitions of genre focused on information objects in isolation, rather than on practices around them in the manner advocated for by Andersen ([2008](#and08)).

Differences in context shine through particularly when it comes to digital objects. Whether a digital object is to be considered an object in its own right is a question that may have different answers in different settings. In the case of the digital photograph of the van Gogh painting in the survey, many evidently ‘looked through’ the photograph at the original painting: nine participants described the medium of the photograph as ‘oil on canvas’. As one participant clarified: ‘If this was printed in a book or catalogue the medium would still be oil on canvas. If you are talking about the actual image which is never really considered as an entity in itself the medium is a digital file’. This seems to reflect the tension observed by Day ([2008](#day08)) regarding the concept of a work; traditional bibliography sees a work as epistemic content, whereas the concept of a work in the arts sees content and form as inseparable.

Though the relevant aspects of an information object may vary according to any number of factors, it does not seem that the concepts of genre, format and medium are irreconcilable across the information disciplines. Indeed, the non-significance of the chi-squared tests in the analysis above is testament to this. As the convergence of the information disciplines continues to progress—parallel with the introduction of novel sorts of information objects—our collective understanding of these and other fundamental concepts will slowly come to light. Further research can better elucidate the matter; the methodology presented here can be reused in other comparative studies, and different methodologies can, surely, be brought to bear.

Clarifying these concepts is important for both researchers and reflective practitioners, as they help us to describe and understand the relationship between content and form in a given document. As documents become increasingly digital and digitized, and new and complex forms of documents proliferate, rote lists of examples no longer serve in lieu of essential definitions. Conceptual clarity can also allow us to see cohesion where we usually focus on differentiation. For example, domain analysis recognizes that documents in different domains have different relationships between form and content ([Bawden and Robinson, 2012, pp. 97–98](#baw12)). While recognizing these differences is important, it is likewise important to recognize the essences shared across domains. Finally, this type of research can also serve to bring to light the concepts underlying our categories, which are often invisible ([Bowker and Star, 2000](#bow00)). Once visible, these essential concepts can be operationalized in future research questions that explore, for instance, practices surrounding differing sorts of documents.

## About the author

**Tim Gorichanaz** is a PhD candidate in information studies at the College of Computing & Informatics, Drexel University, U.S.A., whose research explores how people build understanding through their experiences with information. His research areas include document theory, ultrarunning, artwork and religious practice. He received his Bachelor's degree in Advertising and Spanish from Marquette University, U.S.A., a graduate certificate in Teaching English to Speakers of Other Languages from the University of Wisconsin–Milwaukee, U.S.A., and his Master's degree in Spanish and Latin American Linguistic, Literary and Cultural Studies from New York University in Madrid, Spain. He can be contacted at: [gorichanaz@drexel.edu](mailto:gorichanaz@drexel.edu).

</section>

<section>

## References

*   Andersen, J. (2008). The concept of genre in information studies. Annual Review of Information Science and Technology, 42, 339–367.
*   Bates, M. J. (2015). The information professions: knowledge, memory, heritage. Information Research, 20(1), paper 655\. Retrieved from http://InformationR.net/ir/20-1/paper655.html (Archived by WebCite® at http://www.webcitation.org/6WyNray1L)
*   Bawden, D., and Robinson, L. (2012). Introduction to information science. Chicago: Neal Schuman.
*   Bowker, G., and Star, S. L. (2000). Sorting things out: classification and its consequences. Cambridge, MA: The MIT Press.
*   Braun, V., and Clarke, V. (2006). Using thematic analysis in psychology. Qualitative Research in Psychology, 3(2), 77–101.
*   Buckland, M. (1999). Vocabulary as a central concept in library and information science. In T. Aparac, (Ed.), Proceedings of the Third International Conference on Conceptions of Library and Information Science (pp. 3-12). Zagreb: Lokve.
*   Buckland, M. (2012). What kind of science can information science be? Journal of the American Society for Information Science and Technology, 63(1), 1–7.
*   Caudle, D. M., and Schmitz, C. (2014). Keep it simple: using RDA's content, media, and carrier type fields to simplify format display issues. Journal of Library Metadata, 14(3-4), 222–238.
*   Clark, M., Ruthven, I., Holt, P. O., Song, D., and Watt, S. (2014). You have e-mail, what happens next? Tracking the eyes for genre. Information Processing and Management, 50, 175–198.
*   Day, R. E. (2000). The “conduit metaphor” and the nature and politics of information studies. Journal of the American Society for Information Science, 51(9), 805–811.
*   Day, R. E. (2008). Works and representation. Journal of the American Society for Information Science, 59(10), 1644–1652.
*   Dempsey, L. (1999). Scientific, industrial, and cultural heritage: a shared approach. Ariadne, 5(22). Retrieved from http://www.ariadne.ac.uk/issue22/dempsey/ (Archived by WebCite® at http://www.webcitation.org/6krp86knt)
*   Doucet, M. (2007). Library and Archives Canada: a case study of a national library, archives, and museum merger. RBM: A Journal of Rare Books, Manuscripts, and Cultural Heritage, 8(1), 61–66.
*   Furner, J. (2015). Information science is neither. Library Trends, 63(3), 1–16.
*   Genette, G. (1997). Paratexts: Thresholds of interpretation. Cambridge: Cambridge University Press.
*   Given, L. M., and McTavish, L. (2010). What’s old is new again: the reconvergence of libraries, archives, and museums in the digital age. The Library Quarterly, 80(1), 7–32.
*   Gorichanaz, T. (2015). Minting the reverse: LIS as an unfinished coin [Manuscript under review].
*   Graham, S. S. (2008). Mode, medium, genre: A case study of decisions in new-media design. Journal of Business and Technical Communication, 22(1), 65–91.
*   Heidegger, M. (2010). Being and time. Albany, NY: State University of New York Press. (Original work published 1927)
*   Hjerppe, R. (1994). A framework for the description of generalized documents. Advances in Knowledge Organization, 4, 173–180.
*   Hjørland, B. (2000). Documents, memory institutions and information science. Journal of Documentation, 56(1), 27–41\.
*   Hjørland, B. (2014). Information science and its core concepts: levels of disagreement. In F. Ibekwe–SanJuan and T. M. Dousa, Theories of information, communication and knowledge: a multidisciplinary approach (studies in history and philosophy of science, volume 34) (pp. 205–235). Dordrecht, Netherlands: Springer.
*   Kwaśnik, B. H., and Crowston, K. (2005). Introduction to the special issue: Genres of digital documents. Information Technology & People, 18(2), 76–87.
*   Library of Congress. (2007). 655 - Index term-genre/form (R). MARC 21 format for bibliographic data. Retrieved from http://www.loc.gov/marc/bibliographic/bd655.html (Archived by WebCite® at http://www.webcitation.org/6krpDpni7)
*   Library of Congress. (2011). 340 - Physical medium (R). MARC 21 format for bibliographic data. Retrieved from http://www.loc.gov/marc/bibliographic/bd340.html (Archived by WebCite® at http://www.webcitation.org/6krpI9iPy)
*   Library of Congress. (2015a). 337 - Media type (R). MARC 21 format for bibliographic data. Retrieved from http://www.loc.gov/marc/bibliographic/bd337.html (Archived by WebCite® at http://www.webcitation.org/6krpLqxTy)
*   Library of Congress. (2015b). 348 - Format of notated music (R). MARC 21 format for bibliographic data. Retrieved from http://www.loc.gov/marc/bibliographic/bd348.html (Archived by WebCite® at http://www.webcitation.org/6krpOs2lD)
*   Library of Congress. (2015c). 382 - Medium of performance (R). MARC 21 format for bibliographic data. Retrieved from http://www.loc.gov/marc/bibliographic/bd382.html (Archived by WebCite® at http://www.webcitation.org/6krpRruQB)
*   Miller, C. R. (1984). Genre as social action. Quarterly Journal of Speech, 70, 151-167.
*   White, H. D.  (1992). External memory.  In H. D. White, M. J. Bates, & P. Wilson, (Eds.), For information specialists: Interpretations of reference and bibliographic work (pp. 249-294). Norwood, NJ: Ablex.
*   Yates, J. A., and Orlikowski, W. J. (1992). Genres of organizational communication: A structurational approach to studying communication and media. Academy of Management Review, 17(2), 299–326.
*   Young, J. L., and Mandelstam, Y. (2013). It takes a village: Developing Library of Congress genre/form terms. cataloguing & Classification Quarterly, 51, 6–24.
*   Zinkham, H., Cloud, P. D., and Mayo, H. (1989). Providing access by form of material, genre, and physical characteristics: benefits and techniques. American Archivist, 52(3), 300–319.

</section>

</article>