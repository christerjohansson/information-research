<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# The many faces of practice theory in library and information studies

## [Ola Pilerot](#author), [Björn Hammarfelt](#author) and [Camilla Moring](#author)

> **Introduction.** This paper presents an exploration of how the notion of ”practice” is theorized and applied in library and information studies.  
> **Method.** To highlight how “practice” is conceptualized and to discern what methodological approaches are taken, a qualitative analysis was conducted of a selection of practice-oriented contributions to the library and information studies literature. A bibliometric study of publications relating to “practice theory” provided a complementary quantitative overview of the influence of “the practice turn” in library and information studies.  
> **Analysis.** Through reading a number of introductions to practice theories, a selection of prominent characteristics were identified. These characteristics provided guidance for the close-reading of the sample of practice-oriented library and information studies literature. As well as indicating direction for the qualitative analysis, the outcomes of the bibliometric study were recontexualized by the qualitative study.  
> **Results.** The practice-oriented library and information studies literature can be divided into four different areas based on disciplinary influences. Authors are highlighting different key tenets when adhering to practice theory. It is possible to identify at least four different methodological approaches to empirical studies in the discipline.  
> **Conclusions.** There is not one “practice-based approach” in library and information studies, but rather a multifaceted strand, which exhibits many different characteristics.

<section>

## Introduction

This paper serves to examine and discuss the notion of ”practice” as this is applied in library and information studies (library and information studies). The aim is to explore this notion and thereby highlight a set of features that distinguish different takes on practice in the library and information studies literature. Even though there are those who claim that the debate regarding key concepts in the discipline mainly is a matter of what is fashionable in scholarly circles at the moment ([Julien and Williamson 2011](#jul10)), it has been suggested that studies of practices are a topical area within library and information studies (e.g. [Palmer and Cragin 2008](#pal08); [Haider 2012](#hai12); [Pilerot 2013](#pil13)). A tentative retrospective view of the library and information studies literature, and in particular of the sub-area of information behaviour, suggests that the concept of “information practice” is gaining ground, and perhaps also that it sometimes tends to be used interchangeably with the concept of “information behaviour”. It can also be noted that the few existing overviews and introductions to practice theory in library and information studies are looking outwards; i.e. they are more occupied with practice theory outside of library and information studies than within the discipline. In this paper, our concern is that the notable variation within the practice idiom in general ([Schatzki 2001](#sch01); [Nicolini 2012](#nic12)) motivates a more nuanced picture than has hitherto been offered of what constitutes ‘_a practice-based approach_’ (e.g. [Ianelli and Giglietto 2015](#gig15)) within library and information studies. This proposition has been reflected previously:

> Perhaps the ubiquity of practice talk merely reflects current intellectual fashion with no substantial conceptual significance, or worse, an underlying theoretical confusion assimilating incompatible conceptions of social life under a superficially common term ([Rouse 2006, p. 500](#rou06)).

In order to address this concern, a qualitative and a quantitative analysis of selected literature were conducted.

The paper is structured as follows: the subsequent section reviews a small selection of previous contributions to the practice oriented literature of library and information studies. This is followed by a qualitative analysis of five relatively recent papers that taken together provides a nuanced view of what may constitute a practice-based approach in library and information studies. The qualitative study is then complemented by a bibliometric study of articles published in five journals deemed as major outlets for practice oriented studies. Finally, the results of the two studies are juxtaposed and implications for future research are outlined.

</section>

<section>

## A glimpse of forerunners and previous overviews of practice theory in library and information studies

When probing the literature of the discipline, a special issue of Library Quarterly (2007) on the theme of ‘_Discursive approaches to information seeking in context_’ stands out as somewhat of a milestone. Even though the explicit emphasis is on discursive approaches, the editors’ aim with this special issue was to highlight the implications that a practice turn might have for information seeking research ([Talja and Mckenzie 2007](#tal07)). In one of the contributions to this issue it was asserted that information practice was proposed as a critical response to the concept of information behaviour ([Savolainen 2007, p. 109](#sav07)), and it was concluded that the discourse on information behaviour primarily was associated with the cognitive viewpoint while information practice primarily was inspired by social constructionism. Davenport and Cronin ([1998](#dav98)) presents a line of reasoning that in significant respects resembles a practice approach, and this paper is also mentioned as one of the earlier contributions to the “practice turn” in library and information studies ([Talja and Mckenzie 2007](#tal07)). For example, they suggest that issues such as information needs, seeking and use should be ‘_interpreted in the context of local practice_’ ([Davenport and Cronin 1998, p. 264](#dav98)). Of the contributors to the just mentioned thematic issue, especially Lloyd has since then established a firm position in the practice strand of library and information studies (e.g. [Lloyd 2012](#llo12), [2014](#llo14)).

Thus far only a few papers with the prime aim of discussing practice-based approaches in library and information studies have been published (i.e. [Huizing and Cavanagh 2011](#hui11); [Cox 2012](#cox12), [2013](#cox13)). With the aim of introducing practice theory in the ‘garden’ of information science, Huizing and Cavanagh ([2011](#hui11)) discuss the characteristics of what they term the ‘_latest branch of practice thinking_’ (no. pag.), a branch that they primarily trace to science and technology studies (STS). Their contribution does indeed offer a thorough and insightful review of contemporary practice theorizing, but the ‘_garden of information science_’ remains remarkably invisible throughout their paper since there are hardly any connections made to practice-theoretical studies in library and information studies. Their view is outward-looking, concentrating on practice theory outside of library and information studies.

Cox’ ([2012](#cox12), [2013](#cox13)) contributions differ from that of Huizing and Cavanagh ([2011](#hui11)) in their aspiration to explore how the practice approach has been applied within library and information studies. However, Cox is still primarily occupied with tracing the key influences to practice theory in library and information studies. After reviewing a selection of definitions of practice (e.g. [Schatzki 2002](#sch02) and [Reckwitz 2002](#rec02)), he highlights a set of aspects characteristic of practice-based approaches. Primarily by referring to Wenger ([1998](#wen98)), it is asserted that a practice-based approach pays attention to _the social and social identity_. Another aspect concerns _embodiment and materiality_. Activities are ingrained in physical objects and ‘_practices shape how we use our bodies_’ ([2013, p. 69](#cox13)). In contrast to Huizing and Cavanagh ([2011](#hui11)) who highlight materiality with reference to the STS literature, Cox’ prime support for this claim is Schatzki ([2001](#sch01)). The last aspect noted by Cox is that of _routine and change_. Here, Cox turns to Swidler ([2001](#swi01)) in order to describe practices as activities preformed routinely rather than deliberately ([2013, p. 70](#cox13)).

Even though these reviews are valuable contributions to the discussion about practice in library and information studies, our conclusion is that there is need for further explication of the features that distinguish different takes on practice in the library and information studies literature.

</section>

<section>

## Practice theory outside of library and information studies

The field of practice theory is indeed broad and spans domains and disciplines (and authors) such as philosophy (e.g. [Schatzki 2002](#sch02)), sociology (e.g. [Giddens 1984](#gid84); [Bourdieu 1990](#bou90)), cultural theory (e.g. [Foucault 1980](#fou80)), science and technology studies (e.g. [Knorr Cetina 1997](#kno97); [Rouse 2006](#rou06)), organizational studies (e.g. [Nicolini 2012](#nic12)), and education and learning (e.g. Lave and Wenger 1991).

To prepare the ground for the qualitative analysis, a brief review of a selection of introductions and overviews of practice theory (i.e. [Schatzki 2001](#sch01); [Gherardi 2006](#ghe06); [Rouse 2006](#rou06); [Nicolini and Monteiro in press](#nicmon)) is presented in the subsequent section. The review identified a number of prominent key characteristics, which form the framework against which the qualitatively analysed texts were read. In addition to the identification of the key characteristics, we have also reviewed a small number of recommendations regarding how to conduct practice-based studies.

</section>

<section>

### Introductions and overviews of practice theory

A review of the practice theory literature indicates that practices are conceived as situated sets of embodied activities grounded in time and space, and as reproductive of the social, which means that what is going on in practice contributes to maintain practice. Practices are, furthermore, socially recognized and named, and they constitute a ‘_mode of ordering the world_’ (i.e. they have the capacity of both enabling and constraining). Moreover they are entities where agency is distributed over human and non-human agents. ([Gherardi 2006, p 35; passim](#ghe06))

Of particular importance to Rouse ([2006](#rou06)) is that practices generate rules and norms. Accordingly it is always possible to make mistakes in practice. The notion of practice is furthermore a conceptual means of avoiding the subject-object split and other problematic dualisms, (i.e. body-mind). Practices are rooted (and manifested) in bodily skills, and strongly related to language and tacit knowledge. They are also calling for reflexivity on the side of the practice theorist since the inquiry into practice is a practice among others. Finally, it is highlighted that practices primarily are theorized in terms of _the social_, and not in terms of psychology, biology and/or economics. ([Rouse 2006](#rou06))

A prominent feature of practices according to Nicolini and Monteiro ([in press](#nicmon)) is that they are composed of activities which are oriented towards ends. They also highlight that practices are normatively infused and exist in configurations – e.g. knots or assemblages ([Engeström 2008](#eng08); [Latour 2005](#lat05)) – maintained by collectives. Moreover, practices are situated in time and space and are set in specific material arrangements, which comprise inconsistencies and tensions and contain spaces for both collective and individual agency. ([Nicolini and Monteiro in press](#nicmon))

According to Schatzki’s ([2001](#sch01)) overview, practice theorists contend that practices disown distinctions between subjects and object, highlight non-propositional knowledge, and shed light over the conditions of intelligibility. They also free activities from social structures and systems and reduce the importance of individual action as constitutive of social phenomena. Practice theorists furthermore embrace language as a discursive activity, and allow for the reconsideration of distinctions between human and non-human entities. ([Schatzki 2001](#sch01))

It is evident, and not surprising, that these four accounts exhibit some overlapping. However, when pinpointing the individual traits that appear in the overviews, we end up with the subsequent set of prominent tenets in the practice-idiom. Practice theorists avoid dualisms and conceive of practices as:

*   Socially recognized and named
*   Sets of activities
*   Encompassing language as a discursive activity
*   Oriented towards ends
*   Situated in time and space
*   Comprising inconsistencies and tensions
*   Generative of rules and norms
*   Contributing to ordering the world
*   Reproductive of the social
*   Comprising both individual and collective agency
*   Embracing bodies and materiality

This list of prominent tenets in the practice theory literature will guide the reading of the selected practice-based library and information studies contributions. Before that, however, we turn to the issue of methodological considerations in practice-based studies.

</section>

<section>

### Methods from a practice perspective

In addition to the above characteristics, the practice-theory literature does also contain recommendations on method. A particularly clear example is provided by Nicolini and Monteiro ([in press](#nicmon)) who identify four research strategies, which differ according to the sort of practice-based approach taken by the researcher(s). Perhaps the most common approach is the one that they term the _situational approach_, which focuses on ‘_the concerted accomplishment of practices within orderly scenes of action_’ (p. 13). Examples of empirical cases suitable for the situational approach are workplace meetings, control rooms, hospital wards and class rooms. For this purpose, the authors recommend direct observation of action. A _genealogic approach_ takes a wider temporal perspective as it focuses on the development of specific practices, i.e. how and why practices emerge, change, and dissolve – in short, the dynamics of social practice (cf. [Shove, Pantzar and Watson 2012](#sho12)). This approach may also entail direct observation but can include the study of relevant documents as well; especially if the researcher is engaged in unveiling the historical development of the practice under study. For the study of geographically dispersed practices, and in order to explore ‘_how concerted accomplishments and performances are connected and hang together to form constellations or larger assemblages_’ ([Nicolini and Monteiro in press, p. 16](#nicmon)), a _configurational approach_ is suggested. This would include the ambition to follow the intermediaries in practice, i.e. particular actors or material objects, and resembles what has previously been termed multi-sited ethnography ([Marcus 1995](#mar95)). Finally, the authors suggest a _dialectical approach_. Here, researchers are oriented towards ‘_the co-evolution, conflict, interference of two or more practices_’ (p. 17). The task of the analyst would be to discern different subject positions in practice and to find out who is empowered and disempowered through the way the practice is configured. In addition to direct observation, such an approach would typically include interviews and/or the study of documents.

Schatzki ([2012](#sch12)) also provides recommendations on methods. He is clearly influenced by ethnography when emphasising the importance of spending time with, talking to, and participating with the people in practice. However, he also highlights the importance of interviews. Accordingly, the words people use for talking about activities and practices constitute entrance points to practices. It is furthermore suggested that the researcher prepare herself through attending courses, reading the literature, visiting conferences, and talking to people who are knowledgeable about the practice. In this way, the researcher will be well equipped to identify not only practices and activities but also material objects and arrangements where activities take place.

</section>

<section>

## Qualitative analysis of the practice theory literature

The qualitative analysis of a selection of practice-based contributions is conducted with the above tenets and the recommendations regarding method in mind. The five texts under analysis are purposely selected and used as examples in order to provide a rich description of the variations in the practice approach as it appears in library and information studies. The analytical stance taken includes an interest in what tenets of practice theory the authors have chosen to highlight, how practice is conceptualized, and what their methodological approaches are. The presentation of the analysis is divided into two sections of which the first focuses on prominent practice theoretical tenets in the analysed text, whereas the second section primarily discusses the articles from a perspective of methods. Key observations from both sections are summarized in table 1.

</section>

<section>

### Prominent tenets in the analysed studies

One of the articles in the thematic issue edited by Talja and Mckenzie ([2007](#tal07)) appears in the selection of papers being analysed here. Veinot’s ([2007](#vei07)) study of a vault inspector in a city in Canada displays a rich practice-theoretical contribution. With references to prominent theorists such as Schatzki ([2001](#sch01)) and Reckwitz ([2002](#rec02)) information practice is here conceptualized as a set of activities, which are oriented towards an end. The study participant, Kelly, ‘_observes, interprets, and document phenomena for use by a variety of other types of employees_ … [thereby she] _produces a boundary object that dynamically connects and coordinates a range of organizational activities, thus satisfying a range of organizational information requirements_’ ([Veinot 2007, p. 162](#vei07)), and – in extension – offers a ground for the naming of the particular practice in question, that is, an information practice. In passing, it can be noted that through employing the concept of _boundary object_, the author relates to a strand of practice theory shaped in science and technology studies (STS). This latter observation functions as a bridge to the next article under study, which reports a study of design researchers’ information sharing ([Pilerot 2014](#pil14)). When scrutinizing the references it appears as obvious that this study is also clearly rooted in a practice theoretical tradition, but its specific focus is on highlighting material qualities of information practices which, according to the author, have tended to be overshadowed in previous research. Through the explicit assumption that ‘_documents and information and communication technology (ICT) perform in the sense that they, for example, can coordinate and align activities as well as bind communities together_’ (p. 2006), the author does not only emphasize a view upon practice which embraces materiality, but also includes the notion of individual, collective and distributed agency.

Compared to Veinot ([2007](#vei07)) and Pilerot ([2014](#pil14)), which both adhere to a view of practice as a situated set of activities oriented towards ends and where material objects constitute prime analytical focus points, Sakai, Awamura and Ikeya ([2012](#sak12)) are more oriented towards language as a discursive activity. In focus for their study of a group of information technology hardware engineers stands ‘a situated practice that is accountable, i.e. describable and analysable, as members’ phenomena’ (no pag.). From this line of reasoning it seems consistent that the authors take an ethno-methodological stance and produce transcripts of conversations in practice. The authors further emphasize that the ‘ethnomethodological approach to examine the organization of situated practices [allow for] the practical management of information [to be] examined with its embedded nature’ (no pag.). Thereby it can be asserted that focus in their study is on practices as situated in time and space and as contributing to ordering the world.

The contribution of Sakai _et al_ ([2012](#sak12)) resembles our next example, Haider ([2012](#hai12)), in that both are occupied with the analysis of text, but where the former primarily is interested in the flow of and the turn-takings in conversation, the latter is concerned with different subject positions in practice as these are manifested in environment blogs. It should also be noted that even though Haider ([2012](#hai12)) conducted an analysis of ten blogs dedicated to environmentally friendly life, attention is not only paid to text per se, but also to the ways the content of the blogs is presented and related to other entities. The analytical starting point is that practices can be understood as sets of activities that, through repetition and routine, contribute to the ordering of the world. However, for Haider it is also of great importance to consider the disruptive potential of practices. Here, she draws on Knorr-Cetina’s ([2001](#kno01)) notion of _objectual practices_ which renders practices as accommodating interruption and reflection. It is against this theoretical frame that, for Haider ([2012, p. 652](#hai12)), ‘_oscillation between routine and interruption_’ functions as an analytical lens. In line with this lens, the concept of _information practice_ is perceived as ‘_an analytical tool rather than an empirical category_’ through which blog accounts can be seen as ‘_stories of information practices_’ (Ibid).

Our last example consists of a study by Schreiber ([2014](#sch14)) which explores, in the light of information literacy, the written assignment practice of university students. This is explained by Schreiber ‘_as a social practice consisting of a constellation of activities_’ (p. 347) in which information literacy unfolds. The author draws heavily on Schatzki whose work is explicated in relative detail. Nevertheless, emphasis is also put on language as a discursive activity in practice. This is to a great extent accomplished by referring to genre theory. Another prominent trait in the article concerns rules and norms. However, ‘_[t]he normative element is not seen as being made up of permanent rules but rather of performed practices, where the appropriate conduct is under negotiation_’ (p. 350). Also the concept of “routinisation”, which is adopted from Reckwitz, is central to Schreiber, which indicates a perception of practice as contributing to the ordering of the world.

</section>

<section>

### Analysis from a method perspective

From a perspective of methods, the five studies indeed display some variation (see Table 1). According to the categorisation presented by Nicolini and Monteiro ([in press](#nicmon)), we can sort Veinot’s and Pilerot’s contributions under the _configurational approach_ since both these aspire to follow the intermediaries in practice. In the case of Veinot, it is the job-related tasks of the vault inspector that are being explored through an in-depth interview and through the study of the documents used and produced by the inspector. Pilerot, on the other hand, take sight on the material objects used and referred to by the information sharers in his study. Thereby ‘_trajectories of sharing that reach across time and space_’ ([Pilerot 2014, p. 2006](#pil14)) are identified. Sakai _et al_ ([2012](#sak12)) take a strong _situational approach_ by delving into the practice through ethnography in order to produce audio and video recordings of ‘_orderly scenes of action_’ ([Nicolini and Monteiro in press, p. 13](#nicmon)). Both Haider’s ([2012](#hai12)) and Schreiber’s ([2014](#sch14)) studies comprise study objects which are dispersed over time and space. Our interpretation indicates however that the two take different approaches. Through the explicit aim of establishing how different practices are aligned with environmental activities, Haider adopts a dialectical approach where ‘_the co-evolution, conflict, interference of two or more practices_’ ([Nicolini and Monteiro, in press, p. 17](#nicmon)) are in focus. Schreiber ([2014](#sch14)) conducted interviews with students in order to become informed regarding ‘_the changing character of the written assignment practice_’ (p. 348). This aspiration seems to fit well with what is in focus for studies that take a _situational approach_, namely to explore ‘_the concerted accomplishment of practices within orderly scenes of action_’ ([Nicolini and Monteiro in press, p. 13](#nicmon)).

<table><caption>Table 1: Overview of qualitative analysis</caption>

<tbody>

<tr>

<th>Analysed texts</th>

<th>Empirical settings</th>

<th>Key tenets</th>

<th>Methodological approaches</th>

<th>Key PT references</th>

</tr>

<tr>

<td>Veinot (2007)</td>

<td>

“Blue-collar work”;  
Vault inspection</td>

<td>

- Sets of activities  
- Generative of rules and norms  
- Contributing to ordering the world  
- Embracing bodies and materiality</td>

<td>Configurational – interview, document study</td>

<td>

Schatzki (2001),  
Reckwitz (2002),  
Wenger (1998),  
Leigh Star and Griesemer (1989)</td>

</tr>

<tr>

<td>Pilerot (2014)</td>

<td>Interdisciplinary researchers’ information sharing</td>

<td>

- Embracing bodies and materiality  
- Comprising both individual and collective agency</td>

<td>Configurational – participatory observation</td>

<td>Knorr Cetina (1997),  
Leigh Star and Griesemer (1989),  
Suchman (2005),</td>

</tr>

<tr>

<td>Sakai, Awamura and Ikeya (2012)</td>

<td>The work of IT hardware engineers</td>

<td>

- Language as a discursive activity  
- Contributing to ordering the world  
- Situated in time and space</td>

<td>Situational – participant observation</td>

<td>

Garfinkel (1967),  
Coulter (1979)</td>

</tr>

<tr>

<td>Haider (2012)</td>

<td>Environment blogs</td>

<td>

- Sets of activities  
- Language as a discursive activity  
- Comprising inconsistencies and tensions</td>

<td>Dialectical – document study</td>

<td>

Schatzki (2001),  
Reckwitz (2002),  
Knorr Cetina (2001)</td>

</tr>

<tr>

<td>Schreiber (2014)</td>

<td>Students’ assignment writing</td>

<td>

- Sets of activities  
- Generative of rules and norms  
- Language as a discursive activity</td>

<td>Situational –interviews</td>

<td>

Schatzki (2002),  
Reckwitz (2002)</td>

</tr>

</tbody>

</table>

Our assumption is that decisions about methods are grounded in or related to theoretical stance. Apart from providing insights about the authors’ preferences regarding theory and method, table 1 should consequently also be seen as an indicator regarding the connection between theory and method in the analysed texts. This is an issue that we will touch upon again in the concluding discussion.

</section>

<section>

## Bibliometric study of ‘practice theory’ in library and information studies

We conducted a bibliometric study of publications relating to ‘practice theory’ in order to provide a complementary overview of the influence of practice theory in library and information studies. This quantitative approach offers insights into the volume (number of articles) and the interdisciplinary breadth of sources cited. Five journals deemed as major channels for practice oriented studies were selected (Table 2). In order to follow the development over time a long time frame was chosen (1990-2013). The search for articles was performed in _Thomson Reuters Web of Knowledge_ on the 10th of April 2014 using the web interface, and full records including references were downloaded to Bibexcel for further analysis (Persson, Danell and Schneider 1999). All items (including reviews and editorials) having “practice” or “practices” included in the title were selected amounting to a total of 199 documents. However, we limited the search so that documents on the topic of ‘_best practices_’ or ‘_evidence-based practice_’ were omitted as we judged these as being mainly unrelated to the topic of ‘_practice theory_’. The full search-string looks like this: _Title_: "practice" OR "practices" OR "information practice" _NOT_: "best practices" OR "best practice" OR "policy" OR "evidence-based practice" OR "evidence based practice".

<table><caption>Table 2: Characteristics of the dataset used.</caption>

<tbody>

<tr>

<th>Journal</th>

<th>Document type</th>

</tr>

<tr>

<td>

_Journal of Documentation_ (56 docs)</td>

<td>Article (119 docs)</td>

</tr>

<tr>

<td>

_Information Research_ (54 docs)</td>

<td>Book Review (78 docs</td>

</tr>

<tr>

<td>

_JASIST_ (47 docs)</td>

<td>Proceedings (18 docs)</td>

</tr>

<tr>

<td>

_Library Quarterly_ (30 docs)</td>

<td>Review (4 docs)</td>

</tr>

<tr>

<td>

_Library Trends_ (12 docs)</td>

<td>Letter (1 docs)  
Note (1 docs)</td>

</tr>

</tbody>

</table>

Documents published over a period of 24 years are included in our dataset but a vast majority of items are from 2005 and onwards (fig. 1). Given the large increase in practice-oriented publications in 2005-2009 we can with some confidence place the ‘practice turn’ in library and information studies to the middle of the decade.

<figure>

![Figure1: Distribution of articles over time](../colis1602fig1.jpg)

<figcaption>Figure 1: Distribution of articles over time; (note that the last period only covers four years).</figcaption>

</figure>

References given in these 199 documents were further analysed using _Bibexcel_. Many of these 5698 references are to “non-source” items – documents not indexed in the database. This demands that references are standardized before further analyses can be conducted. Consequently, the dataset was exported to _Excel_ and manually cleaned before citation frequencies and co-citation pairs were calculated ([Marshakova 1973](#mar73); [Small 1973](#sma73)). Thereafter the citation frequencies of documents and authors (table 3) were extracted; (references to Pettigrew have been assigned to Fisher).

<table><caption>Table 3: Authors cited ten times or more.</caption>

<tbody>

<tr>

<th>Author</th>

<th>Cit.</th>

<th>Author</th>

<th>Cit.</th>

<th>Author</th>

<th>Cit.</th>

</tr>

<tr>

<td>Savolainen R</td>

<td>42</td>

<td>Kuhlthau C</td>

<td>19</td>

<td>Choo C</td>

<td>12</td>

</tr>

<tr>

<td>Wilson T</td>

<td>41</td>

<td>Gherardi S</td>

<td>19</td>

<td>Foucault M</td>

<td>12</td>

</tr>

<tr>

<td>Lloyd A</td>

<td>31</td>

<td>Tuominen K</td>

<td>18</td>

<td>Chatman E</td>

<td>12</td>

</tr>

<tr>

<td>Wenger E</td>

<td>31</td>

<td>Hjorland B</td>

<td>17</td>

<td>Glaser B</td>

<td>12</td>

</tr>

<tr>

<td>Talja S</td>

<td>31</td>

<td>Bourdieu P</td>

<td>17</td>

<td>Cox A</td>

<td>12</td>

</tr>

<tr>

<td>Schatzki T</td>

<td>31</td>

<td>Kling R</td>

<td>16</td>

<td>Hara N</td>

<td>12</td>

</tr>

<tr>

<td>Brown J</td>

<td>28</td>

<td>Bowker G</td>

<td>15</td>

<td>Davenport E</td>

<td>12</td>

</tr>

<tr>

<td>Lave J</td>

<td>24</td>

<td>Knorr Cetina K</td>

<td>15</td>

<td>Buckland M</td>

<td>11</td>

</tr>

<tr>

<td>Fisher K</td>

<td>23</td>

<td>Nardi B</td>

<td>14</td>

<td>Orlikowski W</td>

<td>10</td>

</tr>

<tr>

<td>Sundin O</td>

<td>22</td>

<td>Suchman L</td>

<td>14</td>

<td>Limberg L</td>

<td>10</td>

</tr>

<tr>

<td>McKenzie P</td>

<td>22</td>

<td>Latour B</td>

<td>14</td>

<td>Palmer C</td>

<td>10</td>

</tr>

<tr>

<td>Dervin B</td>

<td>19</td>

<td>Cronin B</td>

<td>13</td>

<td>Giddens A</td>

<td>10</td>

</tr>

</tbody>

</table>

In the next step co-citation frequencies between authors cited ten times or more were calculated and clustered. Co-citation occurrences as well as citation frequencies were then exported to _Vosviewer_ which was used to produce a map of all first authors with ten citations or more (Fig. 2). _VosViewer_ produces a two-dimensional map where similarity of items is illustrated by their position on the map; often co-cited authors are depicted closer to each other. The basic rationale for using this method is the assumption that sources often cited together share a topical resemblance. We used the _cluster density view_ as this technique is advantageous when a general overview of clusters and relations among clusters are desirable ([Van Eck and Waltman 2010](#van10)).

<figure>

![Figure2: Co-citation map of authors cited ten times or more](../colis1602fig2.png)

<figcaption>Figure 2: Co-citation map of authors cited ten times or more</figcaption>

</figure>

The co-citation map can be interpreted in more than one way. It rather clearly indicates that the literature on this topic is dispersed over four different clusters (for details on authors included in these clusters see appendix 1). From a qualitative perspective with epistemology as our categorisation instrument, we can, on the one hand, take sight on authors from outside of library and information studies. In accordance with their respective disciplinary abode, it is possible to identify library and information studies contributions influenced by, and more or less related to, science and technology studies (red area at the bottom); studies more or less related to learning theories and education (blue area at the right); works inspired by and with references to philosophy and sociology (yellow area at the top); and, finally, contributions that seem more disconnected (compared to the other areas) to prominent practice theorists and perhaps also more inward-looking or oriented towards fellow library and information studies (green area to the left).

If we, on the other hand, calibrate our categorisation instrument in accordance with social and intellectual factors we can, for example, identify an accumulation of library and information studies authors related to the Scandinavian countries in the philosophy/sociology area at the top. In a similar manner we can discern a number of generally prominent and often-cited library and information studies authors in the library and information studies-oriented area to the left.

Whether the co-citation map can, albeit tentatively, be characterized in reasonable accordance with the strands that we have identified in the qualitative analysis is a matter that we will return to in the concluding discussion.

## Concluding discussion

Even though it is indeed possible to discern some common traits in the studies analysed in this paper, which is indicated in the overview displayed in table 1, the common denominator of the two analyses is variation. Practice-based studies in library and information studies are demonstrating a number of prominent features related to both theory and method. It is however not possible to fully match the areas visible in the co-citation map to the table that displays the overview of the qualitative analysis. Nevertheless, the latter suggests a distribution over theoretical key characteristics and over methodological approaches that potentially resembles the subject distribution found in the co-citation map. When contemplating the bibliometric study with its four clusters in the light of the overview of the qualitative analysis, it is not far-fetched to assert that the literature also from this point of view can be divided into four different, albeit rough and blurred, areas primarily based on methodological approaches. In the qualitative analysis, two contributions stand out as more closely related than the others to the STS oriented area of the practice literature of library and information studies as it emerges in the co-citation map, namely Veinot ([2007](#vei07)) and Pilerot ([2014](#pil14)). (Haider ([2012](#hai12)) does also attribute importance to a contributor outside of library and information studies who is associated with STS (e.g. Knorr Cetina), but the emphasis on materiality, which is a prominent feature in STS, is allegedly less emphasized in Haider than in Pilerot and Veinot). Both of these contributions have been identified as embracing a configurational methodological approach, which consequently, at least tentatively, can be related to the STS cluster of the map. Sakai _et al_ ([2012](#sak12)) escapes this kind of pairing since representatives of ethnomethodology are non-existing in the map. However, their methodological approach (situational) is the same as that assigned to Schreiber ([2014](#sch14)) who can be associated with the philosophy/sociology cluster of the map. The contribution by Haider ([2012](#hai12)) deviates from our analytical matrix since her dialectical approach also, according to her epistemological orientation, tends to relate to the philosophy/sociology cluster.

The relation between the qualitative analysis and the bibliometric analysis, including the co-citation map, should be understood as mutually complementary, and when juxtaposing the two it must be emphasized that co-citation not only reflects epistemological and intellectual similarity but also social relations and disciplinary structures. Referencing is not only an intellectual but also a social activity. Even though it is important to not overstate similarities, it can be asserted that as the bibliometric study provides us with a clue to an overall structure that can be recontextualized and enriched by the qualitative analysis, it also indicates direction for the qualitative analysis (cf. [Hammarfelt 2011](#ham11)).

In comparison to the overviews presented by Huizing and Cavanagh ([2011](#hui11)), and Cox ([2012](#cox12); [2013](#cox13)), which from the point of library and information studies are outward-looking, the present study has placed its analytical focus inside of library and information studies. Our aim was to highlight a set of features that distinguish different takes on _practice_ in the library and information studies literature. The two analyses separately, but also taken together, have shown that there is not one practice-based approach in library and information studies. Rather, we suggest that there is a multifaceted strand in library and information studies that adhere to a practice idiom and this strand exhibits many different characteristics. Our effort has not been to dictate what _proper use_ of practice theory is and the theoretical and methodological pluralism of practice theory in library and information studies may very well be perceived as strength rather than a weakness. Yet, in order to avoid that pluralism turns into vagueness we emphasize the importance of clearly outlining the type of practice theory used, its theoretical base and methodological underpinnings. Otherwise, we might eventually no longer recognize the different faces of practice theory in the crowd of research carrying this label.

</section>

<section>

## <a id="author"></a>About the authors

**Ola Pilerot** (Ph.D.) is a Senior Lecturer at the Swedish School of Library and Information Science, University of Borås, SE-501 90 Borås, Sweden. His main area of research interest concerns information literacies in academia and scholarly information practices. In his doctoral thesis he explored the information practices of design research scholars and in particular their information-sharing activities. He can be contacted at [ola.pilerot@hb.se](mailto:ola.pilerot@hb.se).  
**Björn Hammarfelt** (Ph.D.) is a senior lecturer at the Swedish School of Library and Information Science, University of Borås, Allégatan 1, SE-501 90, Borås, Sweden and a visiting scholar at the Centre for Science and Technology Studies (CWTS), Leiden University. His research is situated at the intersection between information science and sociology of science, with a focus on the organisation, communication and evaluation of research. He can be contacted at: [bjorn.hammarfelt@hb.se](mailto:bjorn.hammarfelt@hb.se).  
**Camilla Moring** is Associate Professor at University of Copenhagen, Royal School of Library and Information Science, 2300 Copenhagen S, Denmark. Her main research interests concerns information practice, information literacy, and workplace learning. She can be contacted at [camilla.moring@hum.ku.dk](mailto:camilla.moring@hum.ku.dk).

</section>

<section>

## References

*   Bourdieu, P. (1990). The logic of practice. Cambridge: Polity.
*   Coulter, J. (1979). The social construction of mind: studies in ethnomethodology and linguistic philosophy. Totowa, NJ: Rowman and Littlefield.
*   Cox, A. (2012). An exploration of the practice approach and its place in information science. Journal of Information Science, 38(2), 176-188.
*   Cox A. (2013). Information in social practice: A practice approach to understanding information activities in personal photography. Journal of Information Science, 39(1), 61-72.
*   Davenport, E. & Cronin, B. (1998). Texts at work: Some thoughts on “Just for you” service in the context of domain expertise. Journal of Education for Library and Information Science, 39(4), 264-274.
*   Engeström, Y. (2008). From teams to knots: activity-theoretical studies of collaboration and learning at work. Cambridge: Cambridge University Press.
*   Foucault, M. (1980). Power/knowledge: selected interviews and other writings 1972-1977\. Brighton: Harvester P.
*   Garfinkel, H. (1967). Studies in ethnomethodology. Englewood Cliffs, NJ: Prentice-Hall.
*   Gherardi, S. (2006). Organizational knowledge: the texture of workplace learning. Malden, Mass.: Blackwell.
*   Giddens, A. (1984). The constitution of society: Outline of the theory of structuration. Cambridge: Polity Press.
*   Giglietto, F. & Iannelli, L. (2015). Hybrid spaces of politics: the 2013 general elections in Italy, between talk shows and Twitter. Information, Communication & Society, 18(9), 1006-1021.
*   Haider, J. (2012). Interrupting practices that want to matter: the making, shaping and reproduction of environmental information online, Journal of Documentation, 68(5), 639-658.
*   Hammarfelt, B. (2011). Citation analysis on the micro level: The example of Walter Benjamin’s Illuminations. Journal of the American Society of Information Science and Technology, 62(5), 819-830.
*   Huizing, A. & Cavanagh, M. (2011). Planting contemporary practice theory in the garden of information science. Information Research, 16(4) paper 497\. Retrieved from http://InformationR.net/ir/16-4/paper497.html (Archived by WebCite® at [http://www.webcitation.org/6JlortA57](http://www.webcitation.org/6JlortA57))
*   Julien, H. & Williamson, K. (2010). Discourse and practice in information literacy and information seeking: gaps and opportunities. Information Research, 16(1) paper 458\. Retrieved from http://InformationR.net/ir/16-1/paper458.html (Archived by WebCite® at [http://www.webcitation.org/6epRXU1ny](http://www.webcitation.org/6epRXU1ny))
*   Knorr-Cetina, K. (1997). Sociality with objects: social relations in postsocial knowledge societies. Theory, Culture & Society, 14(4), 1-30.
*   Knorr-Cetina, K. (2001). Objectual practice. In: T.R. Schatzki, K. Knorr-Cetina, K., & E. von Savigny (eds.), The practice turn in contemporary theory (pp. 175-188). London, Routledge.
*   Latour, B. (2005). Re-assembling the social: an introduction to actor-network theory. Oxford: Oxford University Press.
*   Lloyd, A. (2012). Information literacy as a socially enacted practice: sensitising themes for an emerging people in practice perspective. Journal of Documentation, 68(6), 772-223.
*   Lloyd, A. (2014). Following the red thread of information in information literacy research: Recovering local knowledge through interview to the double. Library and Information Science Research, 36(2), 99-105.
*   Marcus, G.E. (1995). Ethnography in/of the world system. Annual Review of Anthropology, 24, 95-117.
*   Marshakova, I. V. (1973). A system of document connection based on references. Scientific and Technical Information Serial of VINITI, 6(2), 3-8.
*   Nicolini, D. (2012). Practice theory, work, and organization: An introduction. Oxford: Oxford University Press.
*   Nicolini, D. & Monteiro, P. (in press). The practice approach: For a praxeology of organizational and management studies. In Tsoukas, H. & Langley, A. (eds.), The SAGE Handbook of Process Organization Studies. London: SAGE.
*   Palmer, C. L., & Cragin, M. H. (2008). Scholarly information work and disciplinary practices. Annual Review of Information Science and Technology, 42, 165-211.
*   Persson, O., Danell, R., & Schneider, J. W. (2009). How to use Bibexcel for various types of bibliometric analysis. In Åström, F., Danell, R., Larsen, B. & Schneider, J. (eds.) Celebrating scholarly communication studies: A Festschrift for Olle Persson at his 60th Birthday, 9-24.
*   Pilerot, O. (2013). A practice theoretical exploration of information sharing and trust in a dispersed community of design scholars. Information Research, 18(4) paper 595\. Retrieved from www.informationr.net/ir/18-4/paper595.html (Archived by WebCite® at [http://www.webcitation.org/6M8gawYnD](http://www.webcitation.org/6M8gawYnD))
*   Pilerot, O. (2014). Making design researchers´ information sharing visible through material objects. Journal of the Association for Information Science and Technology, 65(10), 2006-2016.
*   Reckwitz, A. (2002). Toward a theory of social practices: A development in culturalist theorizing. European Journal of Social Theory, 5(2), 243-263.
*   Rouse, J. (2006). Practice theories. In: S.P. Turner & M.W. Risjord (eds.), Philosophy of anthropology and sociology (pp. 499-540). Amsterdam: Elsevier.
*   Sakai, S., Awamura, N. & Ikeya, N. (2012). The practical management of information in a task management meeting: taking 'practice' seriously. Information Research, 17(4) paper 537\. Retrieved from http://InformationR.net/ir/17-4/paper537.html (Archived by WebCite® at [http://www.webcitation.org/6epSlewFw](http://www.webcitation.org/6epSlewFw))
*   Savolainen, R. (2007). Information behavior and information practice: Reviewing the "umbrella concepts" of information-seeking studies. Library Quarterly, 77(2), 109-132.
*   Schatzki, T.R. (1996). Social practices: a Wittgensteinian approach to human activity and the social. Cambridge: Cambridge University Press.
*   Schatzki, T. (2001). Introduction: Practice theory. In Schatzki, T. R., Knorr-Cetina, K. & von Savigny, E. (eds.), The practice turn in contemporary theory (pp. 10-23). London: Routledge.
*   Schatzki, T.R. (2002). The site of the social: A philosophical account of the constitution of social life and change. University Park, Pa.: Pennsylvania State University Press.
*   Schatzki, T.R. (2012). A primer on practices: theory and research. In: J. Higgs (Ed.), Practice-based education: Perspectives and strategies (pp. 13-26). Rotterdam: Sense Publishers.
*   Schreiber, T. (2014). Conceptualizing students’ written assignments in the context of information literacy and Schatzki’s practice theory. Journal of Documentation, 70(3), 346-363.
*   Shove, E. Pantzar, M. & Watson, M. (2012). The dynamics of social practice: Everyday life and how it changes. Thousand Oaks, CA: Sage Publications.
*   Small, H. (1973). Co-citation in the scientific literature: A new measurement of the relationship between two documents. Journal of the American Society of Information Science, 24(4), 265-269.
*   Star, S.L., & Griesemer, J.R. (1989). Institutional ecology, “translations” and boundary objects: Amateurs and professionals in Berkeley’s museum of vertebrate zoology, 1907–39\. Social Studies of Science, 19(3), 387–420.
*   Suchman, L. (2005). Affiliative objects. Organization, 12(3), 379-399.
*   Swidler A. (2001). What anchors cultural practices. In: Schatzki T, Knorr Cetina K and von Savigny E (eds) The practice turn in contemporary theory (pp. 74-92). London: Routledge, 2001.
*   Talja, S. & McKenzie, P. (2007). Editors' introduction: special issue on discursive approaches to information seeking in context. Library Quarterly, 77(2), 97-108.
*   Van Eck, N. J., & Waltman, L. (2010). Software survey: VOSviewer, a computer program for bibliometric mapping. Scientometrics, 84(2), 523-538.
*   Veinot T. (2007). The eyes of the power company. Library Quarterly, 77(2) 151–179.
*   Wenger, (1998). Communities of practice: learning, meaning, and identity. Cambridge: Cambridge University Press.

</section>

</article>

* * *

<section>

## Appendices

<table><caption>Appendix 1: Clusters and authors</caption>

<tbody>

<tr>

<th>Green</th>

<th>Red</th>

<th>Yellow</th>

<th>Blue</th>

</tr>

<tr>

<td>Buckland, M.</td>

<td>Bowker, G.</td>

<td>Giddens, A.</td>

<td>Brown, J.</td>

</tr>

<tr>

<td>Chatman, E.</td>

<td>Bourdieu, P.</td>

<td>Limberg, L.</td>

<td>Cox, A.</td>

</tr>

<tr>

<td>Choo, C.</td>

<td>Cronin, B.</td>

<td>Lloyd, A.</td>

<td>Davenport</td>

</tr>

<tr>

<td>Dervin, B.</td>

<td>Hjörland, B.</td>

<td>Sundin, O.</td>

<td>Gherardi, S.</td>

</tr>

<tr>

<td>Fisher, K. (Pettigrew)</td>

<td>Kling, R.</td>

<td>Touminen, K.</td>

<td>Hara, N.</td>

</tr>

<tr>

<td>Foucault, M.</td>

<td>Knorr Cetina, K.</td>

<td>Schatzki, T.</td>

<td>Wenger, E.</td>

</tr>

<tr>

<td>Glaser, B.</td>

<td>Latour, B.</td>

<td></td>

<td></td>

</tr>

<tr>

<td>Kulthau, C.</td>

<td>Lave, J.</td>

<td></td>

<td></td>

</tr>

<tr>

<td>Mckenzie, P.</td>

<td>Nardi, B.</td>

<td></td>

<td></td>

</tr>

<tr>

<td>Savolainen, R.</td>

<td>Orlikowski, W.</td>

<td></td>

<td></td>

</tr>

<tr>

<td>Talja, S.</td>

<td>Palmer, C.</td>

<td></td>

<td></td>

</tr>

<tr>

<td>Wilson, T.</td>

<td>Suchman, L.</td>

<td></td>

<td></td>

</tr>

</tbody>

</table>

</section>