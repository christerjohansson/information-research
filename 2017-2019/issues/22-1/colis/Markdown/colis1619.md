<header>

#### vol. 22 no. 1, March, 2017

</header>

<article>

# Selecting and tailoring of images for visual impact in online journalism

## [Sylwia Frankowska-Takhari](#author), [Andrew MacFarlane](#author), [Ayse Göker](#author) and [Simone Stumpf](#author)

> **Introduction.** Images have a strong presence in online journalism and are used to attract readers’ attention to news content. Images are predominantly sourced from online collections offering access to ready imagery. Yet, the literature shows that image retrieval systems fall short of meeting users’ needs. This study investigated image selection and use in online journalism, in order to propose improvements to the effectiveness of image retrieval.  
> **Method.** Twelve image professionals working in online journalism participated in semi-structured interviews. Eight professionals were observed while performing real illustration tasks in situ. Thematic analysis was used on data to identify common themes. Illustrations created in this study were included in the data, and a social visual semiotics approach was used for the interpretation of visual meaning.  
> **Results.** A trend in how images were selected and tailored for online news content emerged, and a set of recurring image features was identified in illustrations used for online news content.  
> **Conclusions.** The primary contribution of this study is the identification of recurring image features and linking them to visual impact. It is expected that when applied as search functionality, these features will improve the effectiveness of retrieval of visually engaging images as required in online journalism.

<section>

## Introduction and background

Communication in the modern world, particularly within Western culture, is increasingly relying on imagery. This phenomenon is referred to as ‘_ocularcentrism_’ ([Sandywell, 2011, p.3](#san11)). We have been witnessing a shift from predominantly linguistic to pictorial culture ([Mitchell, 1994](#mit94)), and the reasons for this shift are rooted in the qualities of images. Unlike text, images have the power to convey meaning ‘_at one stroke_’ ([Barthes, 1973](#bar73)) and to speak directly to the mind transcending language barriers ([Rothstein, 1974](#rot74)). The ease with which images convey meaning suits well the fast-paced modern lifestyle where information can be – and is expected to be – delivered to the world instantaneously and in _real_ time.

Images are used in many professional contexts within various domains e.g., advertising, education, medicine, and media. In work environments, images are retrieved by end-users who need visuals to complete their work tasks, e.g. illustrating tasks, knowledge sharing, information disseminating ([Conniss _et al_ ., 2000](#con00)). In online journalism images have a particularly strong presence ([Pavlik, 2001](#pav01)), and are used to attract readers’ attention to news content ([Rössler _et al_ ., 2011](#ros11)). Therefore, it is essential that visually engaging images are selected to illustrate online news stories.

For many professionals, online image collections e.g., [Getty Images](http://www.gettyimages.co.uk), [Google Images](https://images.google.com), [Bing Images](http://www.bing.com/images), have become the primary source of imagery. Millions of _ready_ images that can be used and re-used in multiple contexts are uploaded to these vast collections every day. To allow users to search through the immensity of visual content, and to enable them to complete image-based tasks, these collections must employ effective image retrieval systems.

Content-based image retrieval systems operating on low-level image features, e.g., colour, shapes, texture, bring a promise of (semi-)automation of image indexing and categorisation. However, image users’ needs typically relate to high-level meaning, and such systems still fall short of fulfilling these needs. The foundation for reducing of the discrepancies between the systems and users’ needs lies in an in-depth understanding of these needs and user behaviour. However, there is still a paucity of qualitative literature investigating how images are sourced from large online collections, and as Westman ([2009](#wes09)) previously noticed, little is known about how images are used once they have been selected for use.

Therefore, we undertook a user study with image professionals working in online journalism. The goal of this study was to investigate the selection and use of images with online news content. The following two research questions were defined for this study:

1.  How are images selected to create visual impact in online journalism?
2.  How and when are images tailored to create visual impact in online journalism?

While this user study led to gaining an in-depth understanding of users’ needs and behaviour in the image selecting and tailoring processes, the main aim of this paper, is to present and discuss findings related to the set of recurring image features identified in illustrations used with online news content.

</section>

<section>

## Related work

Studies in media and visual communication (e.g. [Fahmy _et al_ ., 2014](#fah14); [Garcia and Stark, 1991](#gar91); [Knobloch _et al_ . 2003](#kno03); [Machin and Niblock, 2006](#mac06); [Rössler _et al_ ., 2011](#ros11)) prove that images in journalism serve as _eye-catchers_ that stimulate engagement in readers and elicit an interest in news stories. The selection and use of images in journalism has also been of interest to researchers from the field of image seeking and retrieval (e.g., [Conniss _et al_ ., 2000](#con00); [Markkula and Sormunen, 2000](#mar00); [Ørnager, 1995](#orn95); [Toms and O’Brien, 2008](#tom08); [Westman and Oittinen, 2006](#wes06); [Westman _et al_ ., 2008](#wes08)).

In their study with image users, Conniss _et al_ . ([2000](#con00)) identified seven classes of image use in professional settings. They found that journalist participants tended to use images mainly for illustration, and emotive/ persuasive purposes, where the function of images was to stimulate emotions or convey a particular message.

Markkula and Sormunen ([2000](#mar00)) conducted a study into image seeking in an editorial office and reported that journalists purposefully sought ‘_attractive_’, ‘_eye-catching_’, and ‘_attention grabbing_’ images. These descriptions were regarded as subjective, aesthetic opinions, and were not researched further. The same study reported that when selecting images, journalists paid attention to visual image features such as, e.g., colour and composition. Close-up photos of people were most often desired, and generally, photographs of specific people, and portraits dominated in news publications.

Westman and Oittinen ([2006](#wes06)) conducted a research with journalists, and confirmed that if the search topic was a person, a close-up photograph of that person was normally selected by the searcher. They also found that the ‘_abstract and affective_’ class of criteria (including eye-catching qualities, mood), as well as ‘_visual and compositional_’ image features (e.g., colours, composition, direction of gaze) were considered as important criteria in the image selection process.

These studies provide evidence that image professionals seek visually engaging images to illustrate journalistic content, and that they pay attention to visual image features in the selection process, however, there is no direct link made between visual features present in images and visual impact that images have on viewers.

Neither, is visual impact clearly defined in the literature for photo editors (e.g. [Ang, 2000](#ang00); [Lewis, 2005](#lew05); [Kobré, 2008](#kob08)), yet, the existence of a relation between visual impact and visual image features is evident.

In journalism images are rarely – if ever – used only as objects of contemplation for their artistic value, hence applying aesthetic interpretation may not be an appropriate approach to the analysis of journalistic imagery. Being an integral part of journalistic content, images in journalism are modes of communication and a tool for generating visual engagement, and encouraging readership. Therefore, in this study the visual social semiotics framework – as introduced by Kress and van Leeuwen ([1996](#kre96)), and further developed by Jewitt and Oyama ([2001](#jew01)) – was used to provide the interpretation of visual image features. The foundations of the visual social semiotics lay in the fact that in societies of Western world, despite people’s differences in age, ethnicity, gender, etc., there are visual features that evoke generally uniform reactions in viewers. This framework provides analytical tools for deconstruction of visual meaning in images, based on syntactic image features, e.g., positioning of objects within the image frame and in relation to other objects in the image, object size, colour, the sharpness of the image, the type of gaze, and of the photographic shot.

This study is concerned with surfacing the relation between visual image features present in images selected and used for illustrating online news content and visual engagement that these features carry. The interpretation of visual image features as proposed in the framework of the visual social semiotics is referred to in the analysis of the artefacts (e.g., Table 1.).

</section>

<section>

## Research design

### Participants and study environment

Twelve participants were recruited in the snowball sampling technique of whom eleven were the BBC Online image professionals, and one worked for a smaller news service. The study was conducted in participants’ work places. For the BBC employees, the location was the BBC Online editorial office in Manchester, UK. These participants sourced images from the BBC’s image database (ELVIS) providing online access to images from sources such as Associated Press, Reuters, or Metropolitan Police, as well as from Getty Images. The one non-BBC participant used ShutterStock and Google Images.

</section>

<section>

### Data collection

The data were collected in semi-structured interviews and observation sessions between July 2013 and February 2014.

The topics for interviews were pre-defined in Topic Guide that was a _living document_ – updated during the study, while new ideas and concepts were emerging. Questions were also adjusted to participants’ roles within the editorial team, i.e., questions for senior roles (e.g. Desk Editors) focused on general image selection guidelines, while questions for Assistant Content Producers whose role was to execute illustration tasks related to their individual hands-on experiences of selecting images.

Eight participants from the sample consented to be individually observed while performing _real_ illustration tasks for online news content. The observations captured the complete illustration process from the point the task was received by participants, through the selection process, to its final stage, when a finished illustration was queued for publication.

In total 25 individual illustrations tasks were observed, each resulting in one final illustration (N=25). 32 separate images were retrieved and used to produce these 25 illustrations. In 4 cases, the final illustrations were composed of more than one image.

A _think-aloud_ protocol was used to ensure a good understanding of participants’ decisions. For each illustration task, a written record of actions, search topics and search terms was kept. The observations were video recorded. _Artefacts_, i.e., all images that participants considered for selection, and those selected and edited for publication were collected.

</section>

<section>

### Data analysis

Since this study was driven by pre-defined research questions, Thematic Analysis was chosen as the most appropriate approach to data analysis. The TA phases ([Braun and Clarke, 2006](#bra06)), were followed. The researcher familiarised herself with the data by repeatedly reading transcripts from interviews and observations, and field notes including observable information. The videos from observations were watched several times to ensure completeness of the data in the field notes. The data was analysed in software Atlas.ti ([www.atlasti.com](http://atlasti.com/)).

</section>

<section>

### Collection and analysis of artefacts

The 25 final Artefacts collected in the study were analysed with a focus on the visual features present in them, and in regards to how the final effect had been achieved. The visual social semiotic framework and tools as described in Jewitt and Oyama ([2001](#jew01)), enabled the researcher to link these features to the visual meaning communicated through these illustrations (see: Table 1.)

</section>

<section>

## Findings

### Selection of ‘ready’ images of a specific person

This research showed that when image professionals selected _ready_ images, the primary function of the image was to attract the attention of readers and generate interest in the news stories that these images illustrated. To make selections, image professionals assessed topical relevance, recency, and accessibility of visual information in the image, and showed a clear preference for images depicting people whenever a specific person was related to the story. If a specific person’s name was used as a search term, the general rule was to select an image of this person with their face clearly visible.

Subsequently, the potential of the image to be used in the new context was assessed based on the following aspects of images: image technical quality, composition, colours, and the image background. Typically, images depicting one person on a blurred (or monotone) background were preferred to images with a fully conceived background because they showed a higher potential for re-contextualisation (see: Figure 1\. below).

Further, images were assessed for the presence of visual image features engendering visual engagement:

*   Composition: a single dominant object in the foreground, and centric positioning for a strong focal point (Salience),
*   Saturated and bright colours for ‘eye-catching’ effect (Salience),
*   Image background colour contrasting with the background colour of the webpage for strong framing effect (Framing).

<figure role="group">

<figure>

![Figure 1a](../colis1619fig1a.jpg)

<figcaption>

**a.**</figcaption>

</figure>

<figure>

![Figure1 1b](../colis1619fig1b.jpg)

<figcaption>

**b.**</figcaption>

</figure>

<figcaption>

**Figure 1:** Shows a real example of editorial work, where Image a. was selected over Image b. as it showed a higher potential for re-contextualisation (Images sourced from: a. Getty Images (Image 163415666 Licence #16623963); b. PA Images (PA18289004; Licence # LA171016)</figcaption>

</figure>

</section>

<section>

### Image tailoring

As previous research has shown ([Conniss _et al_ ., 2000](#con00)), in this study, too, image professionals tended to assess relevance of images at the level of individual objects depicted in them, e.g. a presence of a particular person, rather than of the image as an item. They clearly did not search for the ideal image, instead they tended to search for images that could be easily tailored in PhotoShop software to create visually engaging illustrations.

However, image tailoring was not equivalent to fixing of images. All images that were selected for illustrations had to be of a good technical quality, in terms of resolution and sharpness. By image tailoring, professionals aimed to create illustrations with visual impact that they described as ‘_appealing_’, ‘_eye-catching_’, and ‘_standing-out_’. Typically, the techniques included:

*   Cropping leading to a change of the shot type for increased visual impact, and to _decontextulisation_ by removing objects/people in the background (Figure 4.),
*   Background extension leading to a change in the positioning of the object within the frame for increased salience,
*   Colour saturation enhancements resulting in increased salience,
*   Removing the original background and creating of composite images.

Figure 2\. shows an example of how images such as the image a. in Fig. 1 were typically tailored, and Table 1\. provides the details in regards to the visual effect achieved through image tailoring.

<figure role="group">

<figure>

![Figure 2a](../colis1619fig2a.jpg)

<figcaption>

**a.**</figcaption>

</figure>

<figure>

![Figure1 2b](../colis1619fig2b.jpg)

<figcaption>

**b.**</figcaption>

</figure>

<figcaption>

Figure 2: Images such as image a. were typically tailored before they were published to image b. for the reasons given in Table 1\. (This image was sourced for the purpose of this paper from [https://pixabay.com/p-1481841/?no_redirect](https://pixabay.com/p-1481841/?no_redirect) under CC0 public domain)</figcaption>

</figure>

<table><caption>

**Table 1: This Table shows how tailored illustrations such as the image b. in Figure 2 were analysed with a reference to the visual social semiotics framework**</caption>

<tbody>

<tr>

<th>Technique applied</th>

<th>Achieved result</th>

<th>Relevant visual resource</th>

</tr>

<tr>

<td>Cropping</td>

<td>Waist shot of the specific person in the foreground</td>

<td>Close personal distance created Higher salience through increased size of the main object in proportion to background</td>

</tr>

<tr>

<td>Extending of background to the left</td>

<td>Composition improved (extension of background)</td>

<td>A strong focal point (centric positioning)</td>

</tr>

</tbody>

</table>

</section>

<section>

### Recurring image features

This study clearly showed that images in online journalism were selected and tailored for visual impact, and a set of recurring image features present in illustrations used for online news content was identified. This set, as presented in Table 2\. below, comprises of visual and non-visual image features.

<table><caption>

**Table 2: The set of recurring image features identified in this study for images depicting specific persons.**</caption>

<tbody>

<tr>

<th>Image features</th>

<th>Recurring features identified in this study</th>

</tr>

<tr>

<td>

_Visual features_</td>

<td></td>

</tr>

<tr>

<td>The main object</td>

<td>The specific person must be depicted in the image</td>

</tr>

<tr>

<td>Number of people</td>

<td>One</td>

</tr>

<tr>

<td>Type of shot</td>

<td>From waist up, head and shoulders, Face close-up</td>

</tr>

<tr>

<td>Gaze</td>

<td>Direct and side gaze</td>

</tr>

<tr>

<td>Framing</td>

<td>Large object in frame</td>

</tr>

<tr>

<td>Positioning</td>

<td>Centre, and to the right</td>

</tr>

<tr>

<td>Use of colour</td>

<td>Colour photography</td>

</tr>

<tr>

<td>Colour saturation</td>

<td>Saturated colours</td>

</tr>

<tr>

<td>Object - background</td>

<td>Object (the specific person) in foreground, sharp and in focus</td>

</tr>

<tr>

<td>Type of background</td>

<td>Blurred or monotone background</td>

</tr>

<tr>

<td>

_Technical quality_</td>

<td></td>

</tr>

<tr>

<td>Resolution</td>

<td>Highest resolution available</td>

</tr>

<tr>

<td>Sharpness</td>

<td>Sharp photos</td>

</tr>

<tr>

<td>

_Metadata_</td>

<td></td>

</tr>

<tr>

<td>Recency</td>

<td>Most recent</td>

</tr>

<tr>

<td>Source data filed</td>

<td>Filled in</td>

</tr>

</tbody>

</table>

</section>

<section>

## Discussion

The user study was conducted in participants’ real work environment. During the observation sessions the participants worked on real tasks, which gave the researcher a unique opportunity to observe their natural behaviour in regards to image selection and tailoring processes. Since systems are built to support people at work, understanding of users’ natural behaviour and their needs is fundamental to the development and design of systems, and this research was clearly successful in capturing such data. However, because the research was conducted in situ, the researcher had no input in the construction of tasks. The tasks naturally fell into a variety of categories, however, a significant majority was based on content that required an image depicting a specific person. 76% of illustrations (N=25, n=19) typically depicted one person, of which 73% (n=14) depicted an identifiable person. Images depicting people were selected purposefully, since the image professionals believed that these images would generate stronger visual impact on readers. Previous studies (e.g., [Markkula and Sormunen, 2000](#mar00); [Westman and Oittinen, 2006](#wes06)) also reported this preference. The analysis of the artefacts collected in observation sessions focused on the illustrations showing specific people.

The analysis of the user’s behaviour while searching and selecting ready images showed that currently used systems did not support their users well. There was much repetition in activities performed by image professionals, no use of search features except for sorting by _the most recent_ was observed. Previously, Markkula and Sormunen ([2000](#mar00)) discussed the possibilities for content-based image retrieval to be introduced to the domain of journalism and identified a number of opportunities for a system based on the hybrid approach. One of the recommendations was to enable clustering of returned results in theme-based sub-sets, or grouping of images in different photo categories. Modern image retrieval systems (e.g., Getty Images) allow to filter images into e.g., ‘_portrait_’, by number of people in the image; however, these systems require the user to apply each facet separately. The improvements coming from this study propose an implementation of the set of recurring image visual features as a single search functionality in a retrieval system, in order to enable users to have an easy access to images with visual impact. It is proposed that the functionality is a multifaceted filtering mechanism that will enable retrieving images with visual impact in a _one-click_ step.

This set of criteria is relevant to images that depict a specific person. If it proves effective, this approach may address several issues that image professionals currently face while using large image collections:

*   ease of access: one-step access to images with visual impact,
*   predictability of quality of returned search results,
*   reducing complexity of tasks,
*   saving time spent on routine image tailoring (editing e.g. cropping).

At this stage, further research is required to establish whether if applied, the identified criteria will improve the effectiveness of retrieval of images with visual impact, where the subject of the image is a specific person.

</section>

<section>

## Conclusion and future work

The overall objective of this project was to investigate image selection and tailoring processes in online journalism, where illustrations are created to generate visual impact on viewers. This study has given insights into how images are selected from large online image collections by professional image users, and therefore, contributed to the qualitative literature concerned with image users’ needs and behaviour.

As a result of this study, we uncovered:

*   a trend in how _ready_ images are selected and tailored for online news content;
*   a set of recurring image features for a subset of images depicting specific people, where a majority of these features relate to image syntax;
*   that the identified features can be analysed and interpreted with the use of the tools borrowed from the visual social semiotics framework, and linked directly to the visual impact they have on image viewers.

Further work will focus on an implementation of the identified set of image features into a search mechanism, and testing its impact on image retrieval with users. Such a user study is planned as the next stage in this research project.

</section>

<section>

## <a id="author"></a>About the authors

**Sylwia Frankowska-Takhari** is a PhD student at the Department of Computer Science at City University London. She can be contacted at: [sylwia.frankowska.1@city.ac.uk](mailto:sylwia.frankowska.1@city.ac.uk).

**Andrew MacFarlane** is a Reader in Information Retrieval at the Department of Computer Science at City University London. He can be contacted at: [andym@city.ac.uk](mailto:andym@city.ac.uk).

**Ayse Göker** is a Professor of Computational Systems at the School of Computing and Digital Media, at Robert Gordon University in Aberdeen. She can be contacted at: [a.s.goker@rgu.ac.uk](mailto:a.s.goker@rgu.ac.uk).

**Simone Stumpf** is a Senior Lecturer at the Department of Computer Science at City University London. She can be contacted at: [simone.stumpf.1@city.ac.uk](mailto:simone.stumpf.1@city.ac.uk).

</section>

<section>

## References

<ul>
<li id="ang00">Ang, T. (2000). Picture editing (2nd ed.). Oxford: Focal.
</li>
<li id="bar73">Barthes, R. (1973). Mythologies. London: Granada.
</li>
<li id="bra06">Braun, V. &amp; Clarke, V., (2006). Using thematic analysis in psychology. Qualitative Research in Psychology, 3(2), 77-101.
</li>
<li id="con00">Conniss, C., Ashford, J.A, &amp; Graham, M.E. (2000). Information seeking behaviour in image retrieval: VISOR I final report. Library and Information Commission Research Report 95. Newcastle: University of Northumbria.
</li>
<li id="fah14">Fahmy, S., Bock, A.M, &amp; Wanta, W. (2014). Visual communication theory and research: a mass communication perspective. London: Palgrave Macmillan.
</li>
<li id="gar91">Garcia, M. R. &amp; Stark, P. (1991). Eyes on the news. St. Petersburg, FL: Poynter Institute.
</li>
<li id="jew01">Jewitt, C. &amp; Oyama, R. (2001). Visual meaning: A social semiotic approach. In: Leeuwen van, T. and C. Jewitt (eds.), Handbook of visual analysis. London, UK: Sage Publications, pp.134–156.
</li>
<li id="kno03">Knobloch, S., Hastall, M., Zillmann, D. and Callison, C. (2003). Imagery Effects on the Selective Reading of Internet Newsmagazines. Communication Research, 30(1), 3-29.
</li>
<li id="kob08">Kobré, K. (2008). Photojournalism: the professionals' approach. London: Focal Press.
</li>
<li id="kre96">Kress, A.G. &amp; Leeuwen, T. van (1996). Reading images: the grammar of visual design. London, UK: Routledge.
</li>
<li id="lew05">Lewis, G. (2005). Photojournalism: content and technique. Madison, WI: Brown &amp; Benchmark.
</li>
<li id="mac06">Machin, D. &amp; Niblock,S. (2006). News production: theory and practice. New edition. New York and Oxon: Routledge.
</li>
<li id="mar00">Markkula, M. &amp; Sormunen, E. (2000). End-user searching challenges indexing practices in the digital newspaper photo archive. Information Retrieval, 1(4), 259-285.
</li>
<li id="mit94">Mitchell, W. J. T. (1994). Picture theory: essays on verbal and visual representation. Chicago, University of Chicago Press.
</li>
<li id="orn95">Ørnager, S. (1995). The newspaper image database: empirical supported analysis of users’ typology and word association clusters. Proceedings of the 18th Annual International ACM SIGIR Conference on Research and Development in Information Retrieval, July 9 – 13, Seattle, Washington, USA. ACM Press, pp.212 – 218.
</li>
<li id="pav01">Pavlik, J.V. (2001). Journalism and new media. New York: Columbia University Press.
</li>
<li id="rot74">Rothstein, A. (1974). Photojournalism. New York: Amphoto.
</li>
<li id="ros11">Rössler, P., Bomhoff, J., Haschke, J.F., Kersten, J., &amp; Müller, R. (2011). Selection and impact of press photography: an empirical study on the basis of photo news factors. Communications 36(4), 415–439.
</li>
<li id="san11">Sandywell, B. (2011). Dictionary of visual discourse: a dialectical lexicon of terms (6th ed.). Farnham, Surrey: Ashgate.
</li>
<li id="son75">Sontag, S. (1975). On Photography. London. Penquin.
</li>
<li id="tom08">Toms, E.G. &amp; O'Brien, H.L. (2008). Understanding the information and communication technology needs of the e-humanist. Journal of Documentation, 64(1), 102 – 130.
</li>
<li id="wes09">Westman, S. (2009). Image users’ needs and searching behaviour. In: Göker, A. and Davis, J. (eds.), Information retrieval. Searching in the 21st century. London: Wiley, pp.68-83.
</li>
<li id="wes08">Westman, S., Lustila, A., &amp; Oittinen, P. (2008). Search strategies in multimodal image retrieval. Proceedings of the second international symposium on Information Interaction in Context. London, UK, pp. 13-20.
</li>
<li id="wes06">Westman, S. and Oittinen, P. (2006). Image retrieval by end-users and intermediaries in a journalistic work context. Proceedings of the 1st International Conference on Information Interaction in Context, IIiX 2006. Copenhagen, Denmark, October 18-20, 2006, pp.102-110.
</li>
</ul>

</section>

</article>