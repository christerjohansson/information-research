<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">

<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="keywords"
    content="hypermedia, development methodology, instructional design, software experts, programmers">
  <meta name="description"
    content="This paper proposes an hypermedia development methodology with the aim of integrating the work of both educators, who will be primarily responsible for the instructional design, with that of software experts, responsible for the software design and development.  Hence, it is proposed that the educators and programmers should interact in an integrated and systematic manner following a methodological approach.">
  <meta name="VW96.objecttype" content="Document">
  <meta name="ROBOTS" content="ALL">
  <meta name="DC.Title" content="Developing educational hypermedia applications: a methodological approach">
  <meta name="DC.Creator" content="Jose Miguel Nunes, Susan P. Fowell">
  <meta name="DC.Subject"
    content="hypermedia, development methodology, instructional design, software experts, programmers">
  <meta name="DC.Description"
    content="This paper proposes an hypermedia development methodology with the aim of integrating the work of both educators, who will be primarily responsible for the instructional design, with that of software experts, responsible for the software design and development.  Hence, it is proposed that the educators and programmers should interact in an integrated and systematic manner following a methodological approach.">
  <meta name="DC.Publisher" content="Professor T.D. Wilson">
  <meta name="DC.Coverage.PlaceName" content="Global">
  <title>Developing educational hypermedia applications: a methodological approach</title>
  <style>
    code {
      white-space: pre-wrap;
    }

    span.smallcaps {
      font-variant: small-caps;
    }

    span.underline {
      text-decoration: underline;
    }

    div.column {
      display: inline-block;
      vertical-align: top;
      width: 50%;
    }
  </style>
  <link rel="stylesheet" href="style.css" />
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>

<body>
  <header id="title-block-header">
    <h1 class="title">Developing educational hypermedia applications: a methodological approach</h1>
  </header>
  <h4 id="information-research-vol.-2-no.-2-october-1996">Information Research, Vol. 2 No. 2, October 1996</h4>
  <hr />
  <h1 id="developing-educational-hypermedia-applications-a-methodological-approach">Developing educational hypermedia
    applications: a methodological approach</h1>
  <h4 id="josé-miguel-nunes-and-susan-p.-fowell"><a href="mailto:J.M.Nunes@sheffield.ac.uk">José Miguel Nunes</a> and <a
      href="mailto:Susan.Fowell@uts.edu.au">Susan P. Fowell</a></h4>
  <p>Department of Information Studies<br />
    University of Sheffield, Sheffield, UK</p>
  <hr />
  <h2 id="introduction">Introduction</h2>
  <p>Hypertext has played a role in teaching and learning since the mid 1980’s when the first versions of HyperCard
    appeared. This extremely popular environment and authoring tool presented hypermedia as a technology capable of
    resolving some of the needs of learners and educators using instructional technologies. Drawing on work from
    computer science, educational studies and psychology, hypertext and hypermedia designers have since then
    endeavoured, with varying degrees of success, to establish hypermedia as a credible educational technology.</p>
  <p>Hypertext systems have been described as providing a number of benefits for the learner, including: self paced,
    self selective learning; private learning allowing experimentation in a ‘safe’ environment; accommodation of
    different ability levels and types of learner; open access to information; reduced teaching costs; provision of
    reliable and timely help information; and reduced publication costs. However, despite its potential and some
    successful cases of its implementation (such as those presented by <a href="#ambr90">Ambron and Hooper, 1990</a>),
    hypermedia technology is not being extensively used by schools and universities. The question that immediately
    arises is: why isn’t hypermedia being used more widely, given the vast amount of interest and discussion about its
    perceived benefits?</p>
  <p>In answering this question, researchers and champions of hypermedia may attribute lack of widespread usage to the
    inertia of educators and educational organisations. Focusing on the case of higher education (HE), hypertext failure
    could additionally be attributed to the secondary importance attached to teaching in comparison to research
    activities by the majority of academics (<a href="#darb92">Darby, 1992</a>) or to the <em>Not Invented Here</em>
    syndrome pointed out by <a href="#laur93">Laurillard <em>et al.</em> (1993)</a>. This syndrome reflects the fact
    that academics are just about prepared to recommend a colleague’s textbook, but would not take on video or
    computer-based teaching material developed elsewhere, as the teaching philosophy would be unlikely to match their
    own. This is an important point and should not be dismissed as being simply the protectiveness of teachers to their
    own teaching. In spite of large amounts of funding being directed towards national hypermedia projects, there are
    only a few examples where the resulting products have been widely adopted. The role of hypermedia seems to be
    restricted to hypertextbook type applications and to emerging forms of hypermedia now supported by the World-Wide
    Web. The predominant model for hypermedia applications continues to focus on the production of information resources
    and much less on the learning activities that the student/learner participates in.</p>
  <p>The current situation shows an extraordinary discrepancy between educators’ perceptions of the high value and
    potential of hypermedia as an educational technology and its real use. Such discrepancy cannot be justified
    exclusively by the factors described above. There are other more fundamental application design issues which need
    addressing. The design of hypermedia applications should begin with the production of a <em>conceptual model</em>,
    which represents the various aspects of the subject matter at different levels of abstraction. Additionally, such a
    design should follow a <em>software development methodology,</em> integrating the perspectives of the main agents
    involved in the development process: educators and software developers. Such a methodology must establish the
    educational requirements for the particular subject matter in the preliminary stage, and then, in subsequent stages
    develop the application in response to the requirements of the conceptual model and the educational specifications.
  </p>
  <p>Failure to establish an appropriate conceptual model and use a suitable software development methodology, results
    in poor and ineffective applications. McKendree uses an analogy with the camcorder to characterise the current
    situation:</p>
  <blockquote>
    <p><em>“It lets amateurs make movies about themselves which they and their immediate family and friends can enjoy.
        However, it is unlikely that you or I will want to rent it from the video-store and watch it. The professionals
        are much better able to design and make something, for a wider audience. […] It is fine if some lecturers want
        to take time to hack together some on-line material for themselves and their students. They will probably have
        the pride and commitment to get them to use it. However, the material they produce will possibly not be as
        flexible or as widely applicable as something crafted professionally.”</em> (<a href="#mcke94">McKendree,
        1994</a>)</p>
  </blockquote>
  <p>The prevailing conceptual model for educational hypertext applications is the hypertextbook model. This approach
    supports the traditional textbook approach and enhances its traditional use by offering a link structure to provide
    elaborations of key concepts. However, while providing some useful learning resources this use of hypermedia does
    not capture the real power of the technology. Therefore, new conceptual models must be developed to take advantage
    of the hypermedia philosophy and characteristics, as discussed by <a href="#nune96">Nunes and Fowell (1996)</a>.</p>
  <p>This paper proposes an hypermedia development methodology with the aim of integrating the work of both educators,
    who will be primarily responsible for the instructional design, with that of software experts, responsible for the
    software design and development. Hence, it is proposed that the educators and programmers should interact in an
    integrated and systematic manner following a methodological approach.</p>
  <h2 id="educational-hypermedia-development-methodology">Educational hypermedia development methodology</h2>
  <p>Hypermedia is particularly appropriate for the production of interactive and exploratory educational applications,
    where large numbers of links and cross-references are provided and the learner can explore her/his own interests
    according to previous experience, background and perspective. To be effective, hypermedia applications need to be
    tailored to suit the particular learning tasks planned.</p>
  <p>In order to do this, the learning process itself must first be analysed and understood. Learning is a complex
    process involving a large range of activities, some active, some passive, some creative, some reactive, some
    directed, some exploratory (<a href="#hamm92">Hammond,1992</a>). Furthermore, as proposed by <a href="#nune96">Nunes
      and Fowell (1996)</a>, academic learning should also be seen as the process of construction of knowledge and the
    development of reflexive awareness, where the individual is an active processor of information. This type of
    learning occurs through interaction with rich learning environments, and results from engaging in authentic
    activities, and by social interaction and negotiation.</p>
  <p>This complexity of the learning process suggests the need for situated learning, social negotiation and multiple
    perspectives on the different aspects of the subject matter, the implication being that a number of different
    learning strategies must be adopted to assist the learner in the construction of knowledge. The adoption of these
    different strategies creates learning environments that <a href="#grab95">Grabinger and Dunlap (1995)</a> term
    <em>Rich Environments for Active Learning</em> (or <em>REALs</em>) [7]<em>.</em> REALs promote learning within
    authentic contexts, and encourage the growth of learner responsibility, initiative, decision-making, intentional
    learning and ownership over the acquired knowledge. Additionally, REALs should provide an atmosphere which
    encourages the formation of knowledge building learning communities that assist collaborative social negotiation of
    meanings and understandings among the members of the community (peers, tutors, and subject matter experts).</p>
  <p>In sum, the REAL must essentially support interactions between the tutor, the learner and her/his peers, subject
    matter specialists and the learning materials. All these interactions may, or may not, be computer mediated.
    Furthermore, and as defined by <a href="#nune96">Nunes and Fowell (1996)</a>, an educational hypermedia application
    is a software application specifically produced for a particular educational use, built using the hypermedia
    philosophy. They are developed to resolve a particular educational purpose or learning need, and are thus limited to
    the solution of the problems arising from that need. This means that although they might be linked with other
    hypermedia applications, other software applications, databases or even computer mediated communications facilities,
    they have clearly established boundaries. In this sense, an hypermedia application is one component of a much
    broader learning universe - the REAL in which they are embedded.</p>
  <p>REALs can be seen as instructional systems, in the sense put forward by <a href="#nerv90">Nervig (1990)</a>: as
    sets of interacting, interrelated, structured experiences that are designed to achieve specific educational
    objectives, but organised into a unified dynamic whole. The design of an hypermedia application, as with any other
    of the other components, should hence result from the design specifications for the overall REAL. In turn, the
    design of the REAL results from the process of analysing curricular problems. To design and implement the overall
    REAL, instructional systems design (ISD) should be used. The importance of this overall ISD rests in assuring that
    the whole REAL is implemented using the same learning theory. In fact, if not carefully planned, the REAL could
    result in a mix of eventually conflicting techniques from different theoretical perspectives.</p>
  <p>Accordingly, <a href="#bedn92">Bednar <em>et al.</em> (1992)</a> defend the notion that effective instructional
    design and development is only possible if it emerges from deliberate application of a particular theory of
    learning. Furthermore, the developers must have acquired reflexive awareness of the theoretical basis underlying the
    design. This will ensure that instruction design, hypermedia design and development, and the hypermedia conceptual
    models selected are compatible and all use the same learning theory philosophy.</p>
  <p>This paper addresses the design and development of hypermedia for higher education (HE). Academic learning is here
    defined as an active process in which meaning is developed on the basis of experience, in accordance with the
    constructivist theoretical frame (<a href="#nune96">Nunes &amp; Fowell, 1996</a>). So, to develop hypermedia
    applications in keeping with a constructivist approach, it is important to have an understanding of the kind of
    specifications that will result from constructivist instructional design.</p>
  <h2 id="constructivist-instructional-design">Constructivist instructional design</h2>
  <p>Traditionally, ISD is seen as a process approached from a systems strategy, based on the purpose of the system,
    using a systematic, data-based process for analysing curricular and instructional problems in order to develop
    tested, feasible solutions (<a href="#nerv90">Nervig, 1990</a>). Conversely, constructivist ISD focuses on the
    learner and on the learning process rather than solely on the subject matter. Since knowledge is constructed, the
    learning of a concept must be embedded in the use of the concept. In the traditional ISD, the designer analyses the
    conditions which bear on the instructional system in preparation for the specification of intended learning outcomes
    (<a href="#bedn92">Bednar <em>et al.</em> , 1992)</a>. Content, learner and instructional setting are analysed and
    the instruction is designed using the concepts of learning objectives and specification of goal outcomes.
    Constructivist ISD requires the separation of method and content, instructional designers develop learning
    environments rather then packaged instruction (<a href="#kemb91">Kember, 1991</a>).</p>
  <p>According to <a href="#lebo93">Lebow (1993)</a>, this constructivist ISD should be carried out while bearing in
    mind the seven primary constructivist values: collaboration, personal autonomy, generativity, reflectivity, active
    engagement, personal relevance and pluralism. From these principles Lebow draws a set of general design principles
    to be used in the ISD process:</p>
  <blockquote>
    <p>_make instruction relevant to the learner by providing a context for learning that supports both autonomy and
      relatedness;</p>
    <p>balance the tendency to control the learning situation with the desire to promote personal autonomy;</p>
    <p>support self-regulation through the promotion of skills and attitudes that enable the learner to assume
      increasing responsibility for the developmental restructuring process;</p>
    <p>increase emphasis on the affective domain of learning, treating learning and motivation as part of a unified
      whole process;</p>
    <p>strengthen the learner’s tendency to engage in intentional learning processes, especially by encouraging the
      strategic exploration of errors._ ( <a href="#lebo93">Lebow, 1993</a>)</p>
  </blockquote>
  <p>Although using a traditional ISD model, as shown in Fig. 1, these design principles lead to a development based on
    the constructivist philosophy. Since knowledge domains are not readily separated in the world, according to this
    philosophy, information from many sources bears on the analysis of any particular subject matter and it is not
    possible to isolate units of information. A <em>central core body of information</em> must thus be defined in the
    <em>analysis phase</em>, but boundaries of what may be relevant should not be imposed. Instead of dividing the
    subject matter into logical analysis of dependencies, the constructivist approach turns toward a consideration of
    what users of that knowledge domain do in real life contexts. The ultimate goal of this approach is to move the
    learner into thinking in the knowledge domain as if they were an expert user of that domain (<a
      href="#bedn92">Bednar _et al.,_1992</a> ). Hence, designers should instead identify the variety of experts on the
    subject matter and the <em>tasks</em> they do. The designer should than define simplified but still authentic tasks
    to be experienced by the learner. The goal is to portray authentic tasks, not to define the structure of learning to
    achieve the tasks, since it is the process of constructing a perspective or understanding that is important and no
    meaningful construction is possible if all relevant information is prespecified (<a href="#bedn92">Bednar _et
      al.,_1992</a> ).</p>
  <div data-align="center">
    <img src="Image54.gif" />
  </div>
  <p>Once identified in the analysis phase, tasks must be designed so that they are situated in real world contexts, are
    authentic, and provide multiple perspectives on the subject matter. Additionally, some degree of <em>coaching</em>
    or guidance must be provided, by including meaningful examples and the different perspectives of experts and peers.
    A central strategy for achieving this is to create collaborative learning environments where both face-to-face and
    computer mediated communication are available. Access to extra information sources must also be provided to allow
    different learner’s needs to be satisfied whenever needed.</p>
  <p>It is in the <em>development phase</em> that all the components of the learning environment are implemented
    according to the specifications coming from the design phase. Since different types of educational technologies may
    be needed, to implement all the planned tasks, examples and communication channels, different development
    methodologies may then be applied. If hypermedia applications are needed for a particular instructional task, then a
    specific hypermedia development methodology must be used, where the specifications are established during the design
    phase of the ISD. However, no task is isolated, but rather in a REAL it forms part of a larger context. Hence the
    testing and evaluation of all components of the learning environment must be done in an integrated manner. The
    hypermedia application must be system tested and field trialled as an embedded component in the overall learning
    environment.</p>
  <p>In summary, in a constructivist approach, the analysis phase of the ISD establishes a core body of information
    crucial for the subject matter, identifies the type of experts that use it and the tasks they perform. The design
    phase specifies a comprehensive set of authentic tasks and the coaching and support to be given to the learner. It
    also must specify the educational tools required and the function of each one of these tools. Therefore, if an
    hypermedia application is needed, its specifications arise from the design phase. During the development phase, the
    different educational applications and tools are developed in parallel and then system tested and field trialled
    together.</p>
  <p>Only educators, instructional designers and educational psychologists are involved until the design phase. In the
    development phase software specialists may be required. Thus, the development of these hypermedia applications
    requires software engineering methodologies that allow real co-operation between the two types of agents involved:
    the educators and the software experts. A software engineering discipline is emerging (<a href="#node92">Nodenot,
      1992</a>), called <em>Educational Software Engineering</em> (<em>ESE</em>), which uses methods mainly applied by
    educators with an educational point of view but which also encompasses principles of good software engineering. This
    paper proposes such a methodology specifically for hypermedia application development.</p>
  <h2 id="hypermedia-development-methodology">Hypermedia development methodology</h2>
  <p>The production of educational hypermedia involves collaboration between subject matter and education experts
    involved in the ISD, and hypermedia development experts involved in the application implementation. Hence, the
    communication between these agents becomes paramount. These groups usually speak different “languages” and do not
    readily understand the problems of the other (<a href="#moon86">Moonen, 1986</a>). An efficient educational
    methodology must thus integrate and support the dialogue between these different groups.</p>
  <p>The software development methodology that best supports this requirement is the rapid prototyping approach. A rapid
    prototype is a simplified and untested equivalent of the actual application, performing all the basic functions
    specified for the final product (<a href="#howe92">Howell, 1992</a>). As shown in Fig 2, by implementing a prototype
    first, the hypermedia designers are able to put forward a fully functioning application presenting all the basic
    features of the final product such as user-interface, link structure and coaching facilities. This is not a
    diagrammatic approximation or representation, which tends to be looked at as an abstract thing, but an actual
    implementation of the specifications for the application. These prototypes can be realistically tested and assessed
    and rapidly changed in an iterative manner until consensus is reached. Evaluation and testing of these prototypes
    must be done by instructional designers and ideally include pilot tests using target learners.</p>
  <p>Furthermore, hypermedia applications are inherently different from other software applications. The volume of
    actual code produced in scripts is relatively low and emphasis is put in user-interface design, link structure
    design and definition of contents entry as the different multimedia components. These characteristics along with
    widespread availability of authoring tools, make it possible for rapid development and testing of prototypes.</p>
  <p>The <em>Rapid Prototyping Software Development Cycle</em> presented in Fig. 2, reflects the traditional rapid
    prototyping philosophy. The <em>Specifications</em> are established during the overall ISD process for the REAL
    where the application should be integrated. Consequently, as discussed above, the <em>System Test</em> must be done
    along with the testing all other components of the REAL and as part of its ISD cycle.</p>
  <p>After the acceptance of the prototype, a thorough <em>Design</em> of the user-interface, link structure and
    multimedia integration must be undertaken and documented. To assure that the design is done using the constructivist
    approach, and therefore is in accordance with the overall philosophy chosen for the REAL, a constructivist
    conceptual model for hypermedia applications must be adopted. <a href="#nune96">Nunes and Fowell (1996)</a> discuss
    such a model.</p>
  <div data-align="center">
    <img src="Image55.gif" />
  </div>
  <p>The design phase is followed by <em>Implementation</em> using appropriate authoring and multimedia development
    tools. The implementation must then be subjected to a comprehensive <em>Test.</em> This testing must of two kinds:
    technical and instructional. The technical test aims at detecting and correcting technical problems, insufficiencies
    or inadequate use of the different media. It should be performed mainly by technical experts. The
    instructional-oriented test must establish if the developed application complies both with the educational
    philosophy adopted and the initial specifications. It should be performed by the subject matter experts and
    instructional designers.</p>
  <p>Finally a release version of the application must be produced and handed over to the ISD for further system
    testing. This final version should be accompanied by complete documentation both on-line and paper-based, including
    user manuals if necessary. If any problems are found, new prototypes are built and the process repeated.</p>
  <p>Like any other software methodology, the life-cycle of an educational hypermedia application should not stop after
    the implementation and hand-over. It should be maintained and continually improved. As suggested by Thomas, problems
    reported should be acted on at the earliest opportunity and feedback should be actively sought from learners and
    teachers (<a href="#thom94">Thomas, 1994</a>).</p>
  <h2 id="conclusions">Conclusions</h2>
  <p>Implementing hypermedia educational applications means much more than just designing a few screens and specifying
    their sequence. Today, such an approach is not sufficient to support effectively support the learning processes
    envisaged in constructivist, collaborative or experiential learning philosophies. This paper proposes instruction as
    the act of supporting the construction of knowledge on a particular subject matter, by improving the learner’s
    ability to use the content domain to carry out authentic tasks, and by providing these tasks with the tools needed
    to develop the skills of constructing an informed response and for evaluating alternative responses. Hypermedia
    applications are one such tool available to the instructional designer, which can be used to support constructivist,
    collaborative and experiential learning.</p>
  <p>Understanding hypermedia as an educational technology, and its role within educational practice, is the key to the
    development of successful learning environments. Moreover, the way to prevent a backlash against the use of this
    educational technology lies in recognising both the technical and pedagogic components of instructional design and
    integrating them in a methodologically coherent manner. Rapid prototyping is an ideal approach which facilitates the
    integration of the different agents in educational software development, the subject matter experts, the
    instructional designers and the software developers.</p>
  <p>However, the gap between expert/professional and non-expert/non-professional developers is narrowing, due to the
    increasingly more comprehensive and easy-to-use authoring facilities of the modern hypermedia authoring tools.
    Current authoring tools aim to support both professional quality and do-it-yourself endeavours, so that the
    developer of an hypermedia educational application is now often the educator her/himself. Nevertheless, the need for
    an adequate conceptual model and a comprehensive design process should always be present.</p>
  <h2 id="references">References</h2>
  <ul>
    <li><a name="ambr90">Ambron, S</a>. &amp; Hooper, K. <em>Learning with Interactive Multimedia: Developing and Using
        Multimedia Tools in Education</em>. Washington: Microsoft Press, 1990.</li>
    <li><a name="bedn92">Bednar A.,</a> Cunningham, D., Duffy, T. &amp; Perry, J. Theory into practice: how do we link?
      <u>In</u> Duffy, T. &amp; Jonassen, D. (Eds.) <em>Constructivism and the Technology of Instruction: A
        Conversation</em>. New Jersey, USA: Lawrence Erlbaum Associates, Inc, 1992, pp. 17-34.</li>
    <li><a name="crof93">Croft, R.</a> Adapting software design methodologies for instructional design. <em>Educational
        Technology</em>, August 1993, 24-32.</li>
    <li><a name="darb92">Darby, J.</a> The future of computers in teaching and learning. <em>Computers in
        Education</em>, <strong>19</strong>(1/2), 1992, 193-197.</li>
    <li><a name="grab95">Grabinger, R</a>. &amp; Dunlap, C. Rich environments for active learning. <em>Association for
        Learning Technology Journal</em>, <strong>3</strong>(2), 1995, 5-34.</li>
    <li><a name="hamm92">Hammond, N.</a> (1992) Tailoring hypertext for the learner. <u>In:</u>Kommers, P.; Jonassen, D.
      &amp; Mayes, J. (Eds.)<em>Cognitive Tools for Learning</em>.Berlin: Springer Verlag, 1992, pp. 149-160.</li>
    <li><a name="howe92">Howell, G.</a> <em>Building Hypermedia Applications: A Software Development Guide.</em> New
      York: McGraw-Hill, Inc., 1992.</li>
    <li><a name="kemb91">Kember, D.</a> Instructional design for meaningful learning. <em>Instructional Science</em>,
      <strong>20</strong>, 1991, 289-310.</li>
    <li><a name="laur93">Laurillard, D.,</a> Swift, B. &amp; Darby, J. Academics’ use of courseware materials: a survey.
      <em>Association for Learning Technology Journal</em>, <strong>1</strong>(1), 1993, 4-14.</li>
    <li><a name="lebo93">Lebow, D</a>. Constructivist values for instructional systems design: five principles toward a
      new mindset. <em>Educational Technology Research and Development</em>, <strong>41</strong>(3), 1993, 4-16.</li>
    <li><a name="mcke94">McKendree, J.</a> Design: more than meets the eye? <em>Association for Learning Technology
        Newsletter</em>, <strong>5</strong>, 1994, 2.</li>
    <li><a name="moon86">Moonen, J.</a> Toward an industrial approach to educational software development. <u>In</u>:
      Bork, A. and Weinstock, H. (Eds.) <em>Designing Computer-Based Materials</em>. Berlin: Springer Verlag, 1986,
      pp. 119-151.</li>
    <li><a name="nerv90">Nervig, N.</a> Instructional systems development: a reconstructed ISD model. <em>Educational
        Technology</em>, November 1990, 40-46.</li>
    <li><a name="node92">Nodenot, T.</a> Educational software engineering: a methodology based on cooperative
      developments. <u>In</u>: Tomek, I. (Ed.) <em>Computer Assisted Learning: Proceedings of 4th International
        Conference, ICCAL’92</em>. Berlin: Springer-Verlag, 1992, pp. 529-541.</li>
    <li><a name="nune96">Nunes, J.</a> &amp; Fowell, S.P. <a href="#paper12.html">Hypermedia as an experimental learning
        tool: a theoretical model. <em>Information Research New</em>, <strong>6</strong>(4), 1996, 15-27</a>.</li>
    <li><a name="thom94">Thomas, R</a>. Durable, low-cost, educational software. <em>Computers and Education</em>,
      <strong>22</strong>(1/2), 1994, 65-72.</li>
  </ul>
</body>

</html>