#### vol. 20 no. 3, September, 2015

# Neighbourhood book exchanges: localising information practices

#### [Tenny Webster](#author), [Kathleen Gollner](#author) and [Lisa Nathan](#author)  
iSchool, University of British Columbia, Vancouver, Canada

#### Abstract

> **Introduction.** Through this paper we report on an exploratory study into the design and use of neighbourhood book exchanges in North America. We identify dominant media framings of these book exchanges in North America, along with claims made concerning the influence of the exchanges. We compare the media claims with insights from interviews with the stewards of six book exchanges in an urban centre of the Pacific Northwest and weekly inventories of the six exchanges.  
> **Method.** We collected and analysed over 150 newspaper and media articles, conducted weekly extensive inventories of six book exchanges for three months and interviewed stewards of the six book exchanges at the beginning and end of our inventory period.  
> **Analysis.** We draw upon grounded theory analytic methods as developed by Kathy Charmaz.  
> **Results.** We provide the first empirically rigorous investigation focused on the information practices associated with neighbourhood book exchanges.  
> **Conclusions.** Through this work we provide an initial framing of the neighbourhood book exchange phenomenon. Most significantly, the neighbourhood book exchange stewards in this study actively work to design and support local information practices, reinvigorating and sustaining relationships with their neighbours.

## Introduction

Neighbourhood book exchanges, small enclosures filled with print books for sharing, are appearing in neighbourhoods across North America. Whether they are called book trading posts, little free libraries, pop-up libraries, or neighbourhood book exchanges, these small structures for sharing print books have captured public attention; hundreds of public interest stories have been published over last four years (e.g., [Aldrich, 2014](#ald14); [Groves, 2012](#gro12); [Newcomer, 2012](#new12)). In the age of ubiquitous, mobile computing, why are these offline, fixed boxes of print material appearing in neighbourhoods and generating so much media interest?

Through the exploratory project described in this paper, we question the exchanges' purported role in neighbourhoods and position in the information ecosystem. First, we consider North American media claims regarding the exchanges. We identify the dominant, laudatory claims reporters pose when discussing how the exchanges influence individuals and neighbourhoods. We proceed to describe an empirical examination of six neighbourhood book exchange installations in the Pacific Northwest of North America. We present our analysis of interviews with those who crafted and maintain the six exchanges. We include information from three months of weekly inventories for each exchange. Our work positions us to postulate why people are creating, installing, and using these installations in neighbourhoods already replete with Wi-Fi-enabled public libraries, coffee shops, and bookstores.

## Background

Neighbourhood book exchanges have been propagating through North American neighbourhoods for at least two decades. However, until the sudden surge of popular media attention in the last few years, there has been little discussion of the phenomena in academic discourse or popular media.

The earliest neighbourhood book exchange that we have identified was installed at an intersection in Portland, Oregon in 1996\. The exchange was part of Share-It-Square, a neighbourhood project to bring neighbours together and reclaim or repair their neighbourhood. They hoped to create a gathering place in their neighbourhood's otherwise unused public space. The neighbours gathered to paint a design in the street and build attractions at each corner of the intersection. These attractions included a community notice board, a children's playhouse, a solar-powered tea station, and a book exchange ([Jarvis, 2010](#jar10)). This neighbourhood project became the foundation for the City Repair Project, an effort to support other neighbourhood projects transforming public spaces into gathering places.

Similar intersection repair projects have been conducted across Portland and in other North American cities, and a few research studies have tried to gauge their impact. In [2003](#sem03), Semenza compared the use of Sunnyside Piazza in Portland, OR, a recently reclaimed neighbourhood intersection, to a similar but unimproved neighbourhood intersection. The research team was interested in determining whether these neighbourhood interventions impact public health. Their work suggests that neighbours of the Sunnyside Piazza had a better sense of community and better perception of their neighbourhood than the control site. In a later study, Semenza, March and Bontemp ([2007](#sem07)) attempted to evaluate three more City Repair Project interventions in Portland, OR. A survey instrument was administered to neighbours within a two-block radius of the intersection both before and after each intervention. Their results indicated improved mental health, sense of community and social capital, leading the authors to claim that the interventions had improved community life and neighbours' well-being ([Semenza et al., 2007](#sem07)). These studies focused specifically on intersection repair projects; they did not focus on or necessarily include a neighbourhood book exchange.

For work that targets the book exchange phenomena, our searches revealed a single paper entitled, "Marginalia: little libraries in the urban margins" by Mattern ([2012](#mat12)). Mattern provides an engaging analysis of the origins and impacts of various initiatives within the alternative information economy, including neighbourhood book exchanges, and she extrapolates on their impact on communities. She argues that despite the media assertion of a homogenous movement, the various installations including neighbourhood book exchanges and their relatives—pop-up libraries, library exhibits, etc.—are highly local phenomena, imparting a purpose and method that reflects the personality of a particular locale. Mattern's paper provides an evocative reflection on the character and complexities of alternative library installations. It is difficult, however, for a social science oriented reader to assess her analysis as few details of her methods, analytic approach or data are provided, as these terms may not align with her more humanistic perspective.

Through the project described in this paper we set out to develop a focused, empirical and extended investigation of the development and use of neighbourhood book exchanges. We offer an initial exploration of such book exchanges and insights into their growing presence in neighbourhoods across contemporary North America.

### What are neighbourhood book exchanges?

Many book exchanges are accessible to the public, but those in community centres or coffee shops may require membership. Some book exchanges are small, hosting only a half dozen volumes, while other book exchanges are considerably larger holding over a hundred books. In addition, some book exchanges may hold more than just books, attracting a wide array of material. Since there are so many varieties of book exchanges, defining a neighbourhood book exchange early in our project was necessary to determine which book exchanges to include in our study. For the purposes of this study, we use the phrase _neighbourhood book exchange_, or _exchange_ for short, to describe a small physical container that is:

*   _Accessible to the public:_ Location is critical. Neighbourhood book exchanges are positioned to be physically accessible to as many as possible. They may be on public property or on the fringes of private property; they are typically placed beside or near a sidewalk. They do not require membership with a particular group or organization, and they do not require patron a particular business or establishment to be patronised.
*   _Available 24 hours a day, 365 days a year:_ Exchanges do not have operating hours or seasons. They are available to passers-by at any time of day and any time of year. Importantly, this means that they are not situated inside a business or establishment that has operating hours, and the exchanges are, typically, out-of-doors.
*   _An invitation to exchange books:_ Whether explicitly stated or not, they invite passers-by to take a book or leave a book. Some exchanges may host other forms of recorded information—e.g., movies, music, or magazines—but neighbourhood book exchanges prioritise books, first and foremost. Anyone is welcome to take or leave items; participation is not restricted to members of a specific group or community.

Through these attributes we narrowed the scope of our investigation to book exchanges that are functionally similar and established a foundation for comparison between the exchanges in our study.

### Project positioning

We used a constructivist interpretation of grounded theory principles to guide, as opposed to dictate, our research process ([Charmaz, 2014](#cha14)). We approached the literature review, for example, without the strict orthodoxy of the original grounded theory tradition ([Glaser and Strauss, 1967](#gla67)) by remaining open to insights from other theories, frameworks and perspectives. We began by considering aspects of information behaviour (e.g., [Erdelez, 1995](#erd95); [Cooksey, 2004](#coo04)), social capital (e.g., [Leyden, 2003](#ley03); [Portes, 1998](#por98)), social network theory (e.g., [Granovetter, 1973](#gra73)), community informatics (e.g., [Gurstein, 2007](#gur07)) and practice theory (e.g., [Suchman, 2002](#suc02); [Reckwitz, 2002](#rec02); [Disalvo, Redström and Watson, 2013](#dis13)). Throughout the period of data collection and initial analysis, we reflected on these theories with a critical eye, often finding that the stories emerging from our data were suggesting a different direction.

While several of these theories could be generative for a future study of neighbourhood book exchanges, they were not appropriate for our initial exploration. For example, information behaviour theories, such as casual-leisure information behaviour ([Elsweiler, Wilson and Lunn, 2011](#els11)), browsing behaviour ([Ricci, Wöber and Zins, 2005](#ric05)), information encountering ([Erdelez, 1995](#erd95)), and serendipity ([Cooksey, 2004](#coo04); [Nutefall and Ryder, 2010](#nut10)) can support investigations of book exchanges from a visitor's point of view, but we were also pursuing the experiences of those who create and maintain the book exchanges. Similarly, attempting to understand book exchanges through a social capital and social network theory lens (e.g., [Leyden, 2003](#ley03); [Granovetter, 1973](#gra73)) was unconstructive given the scant reference in our data to the strength of ties between neighbours. Community informatics, despite offering a promising fit for understanding book exchanges initially, privileges new, digital information and communication technologies ([Gurstein, 2007](#gur07)). The Internet—access to it and how it shapes community processes—is a central theme of community informatics scholarship. This promised little in the way of understanding the decidedly offline nature of book exchanges ([Loader, 2003](#loa03)).

Thus, our initial literature review proved critical in helping us frame our work in terms of identifying and articulating differences between the focus of contemporary information studies research and the grounded, local and primarily non-digital book exchanges. Although some exchanges may have a Web presence, most notably those registered with the [Little Free Library](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.littlefreelibrary.org&date=2015-08-18) Website and its accompanying Google Map, the exchanges themselves are not digital.

Throughout this paper, the term information practice is used to refer to what Dourish and Anderson label collective information practice ([Dourish and Anderson, 2006](#dou06)). The concept of information practice allows one to consider the use of information tools within the broader context of life activities. It also takes as a given that to use an information tool one needs a theory, albeit informal or unarticulated, of what the tool is, how it works and how the tool is used ([Bijker, 1995](#bij95)). Understandings of a tool and how it is used are mutually constituted. Thus, an information practice is a collectively recognised and negotiated activity for creating, recording, organizing, storing, manipulating and sharing information ([Nathan, 2012](#nat12)).

## Methods

For our exploratory study we employed a mixed-methods approach, combining qualitative and quantitative methods to iteratively and reflectively improve the quality and depth of the study ([Fidel, 2008](#fid08)). We developed a series of questions related to our initial motivating query: In the age of ubiquitous, mobile computing why are these decidedly offline, stationary boxes of print material generating so much interest?

*   What are the roles and information practices developing around the book exchanges?
*   Why are people designing and (re)designing these exchanges?
*   Is there evidence that these are local, grassroots endeavours?

Early on we began a content analysis of media articles to examine the claims concerning neighbourhood book exchanges. To add depth to our investigation of these questions we also conducted case studies of six exchanges in an urban area in the Pacific Northwest. We interviewed individuals who designed or maintained one of the six exchanges to gain insight into their experiences with and reflections on establishing and maintaining their exchanges. We also surveyed the residents who lived near two of the exchanges to gain insights into their reactions to the exchange as well as their use or non-use, but we touch only lightly on that work within this paper. Additionally, we kept weekly inventories of the contents, appearance and surroundings of each book exchange for twelve weeks. We noted the ISBN numbers and condition of books contributed and taken from the exchange, as well as changes to the exchange's structure and immediate surrounding area—recording any instances of vandalism, garbage or damage, for instance. _Figure 1_ illustrates the timeline of our research activities.

<figure class="centre">![Figure 1: A timeline of our stages of research](p684fig1.jpg)

<figcaption>Figure 1: A timeline of our stages of research.</figcaption>

</figure>

In the following sections, we provide details on each component of our mixed-methods approach. We describe how we conducted a content analysis of media articles that discuss neighbourhood book exchanges and our empirical study of six exchanges in an urban area in the Pacific Northwest. We explain how we selected our sample of exchanges and we illustrate our methods for interviewing those who designed and/or maintain the exchanges. We also detail our process of inventorying the exchanges over a three month period. In addition, we briefly review our survey of neighbours of two of the exchanges since that work informed our second round of steward interviews.

### Media analysis

Early in our study of neighbourhood book exchanges we systematically collected media articles from the United States and Canada that report on the phenomena. We identified 163 articles in three databases—Factiva, LexisNexis Academic and Canadian Newsstand—that discussed neighbourhood book exchanges in Canada and the U.S. published between March 15, 2011 and March 15, 2013\. The articles were aggregated using terms we identified as referring to our definition of book exchanges: book trading post, corner library, community book exchange, neighbourhood book exchange and little free library. The two-year date range was selected because in wider searches we discovered a surge of media attention on the exchanges that began in the spring of 2011 and continued into the spring of 2013.

We prepared a content analysis schedule, based on extensive and iterative readings of media articles on the exchanges, to capture how the articles framed the purpose and impact of book exchanges. During the schedule development phase, codes were developed to reflect themes frequently discussed with respect to the exchanges in the media (e.g., perceived purpose), as well as aspects we were interested in investigating informed by our initial observations of local book exchanges (e.g., vandalism and observed community interactions). The resulting schedule had 35 codes organized within the following six facets:

1.  General article information—e.g., _date of publication_ and _word count_
2.  General exchange information—e.g., _geographic location_ and _mention of Little Free Library_
3.  Perceived purpose of exchange—e.g., _mentions sustainability_ and _promotes literacy_
4.  Observed impact of exchange—e.g., _good source of reading material_ and _poor quality of books_
5.  Vandalism—e.g., _reported vandalism_ like arson or theft
6.  Observed community interactions—e.g., _reported interactions_ such as people meeting for the first time or gatherings of neighbours at the exchange

For facets three and four, unanticipated data were recorded under an _other_ or miscellaneous code; if we recorded an aspect in the other field multiple times, we created a corresponding code in the appropriate facet—e.g., we created a code for neighbourhood project after observing a number of papers that mentioned a number of neighbours working together to establish an exchange.

Two researchers, working independently, coded a total of 161 articles; fifteen of these were randomly selected and coded by both researchers to calculate inter-coder reliability. The reliability was found to be 90.4% agreement, kappa = 0.714 (p>0.01), suggesting substantial agreement between two coders.

### Local book exchange inquiries

In June 2013, we began our investigation of six exchanges in our metropolitan area. At that time, six was the total number of exchanges of which we were aware. We identified the exchanges through media reports and conversations with known exchange stewards, by querying community and University listservs (e-mail lists) and from our personal experiences traveling throughout the area. We use pseudonyms for the six exchanges throughout this write up.

To ensure that there was a clear process of opting into the study, we crafted a letter to the individuals who designed and maintained each exchange that explained the purpose and procedures of our study. The letters were delivered according to our relationship with those associated with the exchange. If we already knew the address or e-mail of the individual(s) maintaining the exchange, we delivered the letter to them directly. If we had a secondary contact, we asked her or him to deliver the letter on our behalf. If we had no previous contact with the individuals associated with the exchange, we delivered the letter to the residence closest to the exchange. In each case, the letter eventually reached an individual active in designing and/or maintaining the exchange, and the stewards of all six exchanges in the metropolitan area agreed to participate in the study.

#### Interviews.

We conducted intensive interviews ([Charmaz, 2014](#cha14)) with those in charge of each exchange twice during the summer of 2013\. The first interview took place early in the summer and the second at the end of the twelve-week inventory period described below. Each interview followed a similar protocol. Two researchers, one to lead and one to take notes and operate the digital recorder, met with the interviewee(s) of each exchange. For the first set of interviews, we asked participants about their motivations for initiating a book exchange, their experiences installing and maintaining the exchange and their observations of the exchange's use and impact. The second set of interviews focused on the interviewees' observations and experiences during the twelve-week period between the two interviews and pursued points of interest from the first interview. This second interview also included a tour of the exchange under discussion. We asked participants to show the exchange to the researchers, explaining its features and their significance.

After completing an interview, we transcribed, anonymised and then destroyed the original recording. Three researchers analysed the written interview transcripts in Dedoose, an online software tool for qualitative analysis. We used a grounded theory approach—initial and focused coding and constant comparison—deeply informed by Charmaz ([2014](#cha14)) to code the interviews. Our coding process began on a granular level; we coded segments of the first round of interviews, generating hundreds of unique codes between the three researchers. These initial codes were reconciled and refined and applied to the second round of interviews to make comparisons in the data and to develop new codes. We later returned to the first round of interviews with updated codes in a number of iterations. Throughout these iterations, we developed the codes (now numbering over 400) into higher analytical levels, concepts and categories while staying close to the data. By allowing concepts to emerge from the data, we were able to capture threads that were unexpected, detect nuanced differences in responses and acknowledge each individual's unique narrative. For most of this work we used Dedoose, finishing the process by printing and sorting the codes by hand into themes explored in this paper.

#### Neighbourhood questionnaire.

Also as part of the overall project, we distributed survey questionnaires to residents who lived in the neighbourhood surrounding the two exchanges. The questionnaire asked respondents about their experience with the neighbourhood book exchange, as well as some basic demographic information. Out of the 103 surveys distributed in the Dewey neighbourhood, we received thirty-seven completed questionnaires; from the Briet neighbourhood, we received forty-four completed questionnaires from the 111 distributed. The data we gathered from these questionnaires influenced the development of our second set of interview questions, but we do not draw directly from them within this write up. Findings from this survey will appear in a future paper currently in preparation.

#### Observational data.

As mentioned above we recorded a weekly inventory of the contents of each exchange. The inventories were conducted each week, on the same day of the week (Tuesdays), for twelve consecutive weeks. We recorded information about the books and other items contained in each exchange. For books, we recorded the ISBN, if available. For books without ISBN information, we recorded the title, author, publisher, edition and date of publication. We also noted each book's condition. An overall score between zero and ten was assigned, where ten represented a book in pristine, like new condition and a score of zero represented an unreadable book. Letter codes located specific damages (e.g., the letter B indicated a broken spine; WD indicated there was evidence of previous water damage or it was waterlogged) and is best understood accompanied with the overall score. When magazines, journals or other recorded information were present, we noted the type, title and date of the publication, as well as its condition. For all other items (e.g., shoes, toys and other miscellany) we noted what it was and its condition. We also recorded notes about the exchange itself—for instance, whether it had been vandalised or whether garbage had been left behind.

All this information was recorded by hand on a printed out Excel sheet. After each visit, the field sheets were entered into a database and an updated sheet was printed for the following week. During the next visit items listed from the previous week were checked if present, new items were entered, and items not checked off (no longer in the box) were considered _exchanged_ or _culled_ items.

As a part of the inventory process, pictures of the book exchanges were taken each week from four different perspectives:

*   facing the front of the exchange, one picture with the door(s) closed, one with the door(s) open;
*   facing the back of the exchange;
*   standing at the right side and the left side;
*   standing over 2 metres away from the exchange to show its immediate environment.

To analyse the images we set up a matrix with Excel to compare the images over the course of the inventory period. The x-axis represents the week the images were taken while the y-axis contains each aspect. See _Figure 2_ for a condensed example.

<figure class="centre">![Figure 2: Example of the picture analysis matrix](p684fig2.jpg)

<figcaption>Figure 2: Example of the picture analysis matrix.</figcaption>

</figure>

The inventory complemented the data collected from interviews and questionnaires, providing us with evidence of how often material was exchanged, the condition the material was in and whether the structure or its immediate environment had changed (e.g., fresh paint, bench installation, vandalism). Images taken during each inventory allow us to document physical changes to the exchange's structure, surroundings and organization of its contents.

## Findings

Although our write-up follows a traditional format, the study itself was iterative, reflexive and exploratory. We drew upon grounded theory methods as developed by Charmaz ([2014](#cha14)) to guide analysis. As we collected data, we also coded, analysed and developed themes; we routinely returned to earlier data and related scholarship for further insight. Charmaz ([2014](#cha14)) articulates well the controversy over drawing upon existing theory in grounded theory research (pp. 305-310).

For this paper we concentrate on our initial findings from the media content analysis and the interviews. Although the inventories and the questionnaires yielded intriguing patterns, we found these generated more questions for future work than insights for this paper, thus we draw upon them to supplement rather than ground our presentation of findings.

In the following sections, we introduce and discuss themes that arose when we considered the practices that the exchanges facilitate.

### Exchange roles and types of activities

Based on our analysis of media articles and our investigation of the six exchanges, we identified two roles that are central to the daily information practices associated with the exchanges: stewards and visitors. Stewards build and/or maintain the physical structure, stock the initial collection and may or may not actively curate the content of an exchange.

A visitor is anyone who browses the contents of the exchange, regardless of the frequency of their visits. To be designated a visitor one did not need to live near an exchange. A visitor may take one or many books, leave one or many books, do both or neither. When a visitor stops to browse the contents of the exchange regardless of whether they take or leave any items, we call it a visit. Browsing, in this sense, can be considered scanning or perusing the contents of an exchange for an indeterminate amount of time.

Transactions—i.e., the exchange of materials through the book exchange structure—are typically asynchronous, with days or weeks between the moment when a book is left and then taken by someone else. These transactions, book donations and pickups typically lack attribution.

### Media framings

Neighbourhood book exchanges consistently garner glowing reports in the media. Our content analysis of the media articles revealed a strong emphasis on the positive aspects of the exchanges; only 5 percent of the articles raised a concern or problem. In addition, there was considerable repetition in the media's portrayal of the exchanges. Most articles used the same descriptive and anecdotal elements and portrayed the same perceived purpose and impact. Four-fifths of the articles discussed how the exchanges contribute to building community and/or promoting literacy. Other themes were apparent—for instance, how the exchanges contribute to sustainable initiatives (five percent) and how they are a resistance to digital technologies (seventeen percent)—but these themes were not as prevalent or as consistent as the themes detailed below.

#### Community catalyst.

Reporters repeatedly credit the exchanges with bringing people together (e.g., [Jones, 2012](#jon12); [Gold, 2012](#gol12); [Lee, 2011](#lee11)). Of the articles we analysed, the majority mention exchanges as social catalysts (seventy-seven percent), in most cases referring to _community_ specifically. One account describes long-time neighbours meeting for the first time while browsing an exchange ([O'Connor, 2012](#oco12)) and several tout the exchanges as convening spots for block parties (e.g., [Scrivener, 2011](#scr11); [Christian, 2012](#chr12)).

#### Literacy promoter.

Another popular framing credits neighbourhood book exchanges with supporting literacy (e.g., [Gessner, 2011](#ges11); [Jones, 2012](#jon12); [Christian, 2012](#chr12); [Newcomer, 2012](#new12)). More than half the papers in our analysis emphasised this narrative. Others draw attention to the ways that book exchanges cater to walk up visitors, providing a _literary water cooler_ atmosphere where neighbours can gather and share their love of reading (e.g., [Kelley, 2011](#kel11)).

#### Little Free Library affiliated.

Significantly, the [Little Free Library](http://www.webcitation.org/query?url=http%3A%2F%2Fwww.littlefreelibrary.org&date=2015-08-18) organization or proprietary exchange namesake, Little Free Library, was mentioned in 137 of the 146 articles in our analysis. There seems to be a striking parallel between the two themes outlined above—community catalyst and literacy promoter—and the Little Free Library's mission in 2012: '_To promote literacy and a love of reading; To build a sense of community; To build more than 2,510 libraries_' ([www.littlefreelibrary.org](http://www.littlefreelibrary.org)). This suggests the media's account of the exchanges was strongly influenced by the Little Free Library. Notably, the singularity of this media narrative does not reflect the nuances of locally-situated book exchanges.

### Exchange inspiration

The majority of exchanges in our study were inspired by another book exchange—usually an exchange seen in person but in one case discovered through the media. Several exchanges in our study were influenced by exchanges seen in neighbourhoods in other, sometimes faraway, cities. For example, one participating steward was motivated by the Share-It Square book exchange in Portland, OR. Other exchanges gained inspiration closer to home. Two exchanges in our study were influenced by an exchange in a nearby neighbourhood. And, finally, one exchange was inspired by the Little Free Library. The steward attributed a TV news story about a Little Free Library as a primary motivator; she decided to build one after contacting the organization for more details. The other five exchanges in our study were built without knowledge of or support from the Little Free Library.

### Exchange profiles

<table class="center">

<tbody>

<tr>

<td style="text-align:center;">![Figure 3: Briet book exchange](p684fig3.jpg)</td>

<td>

#### Briet.

Installed in Spring 2011, this exchange was the oldest in our study. It is built to resemble a three-story stick frame house with a cedar shake roof and wood framed Plexiglas door. A painted message snakes around the door. It is painted in bright colours with a small mural adorning the rear aspect. There is a chalkboard on one side of the exchange and a notice board on the other. A small sign designates the lowest shelf for children's books. It's surrounded by green space and a child-sized wooden bench sits beside it.

</td>

</tr>

<tr>

<td style="text-align:center;">![Figure 4: Casanova book exchange](p684fig4.jpg)</td>

<td>

#### Casanova.

Inspired by the Briet exchange, the Casanova exchange is of a very similar design, though slightly more voluminous. It is placed on the corner of an intersection and is surrounded by a low garden; three flagstones guide visitors to browse the contents without crushing any flowers. Opposite sides of the brightly coloured exchange host a chalkboard and a notice board. A few shells and rocks adorn the cedar shake roof. Content regularly includes French and Asian language books, reflecting the mixed demographics of this neighbourhood.

</td>

</tr>

<tr>

<td style="text-align:center;">![Figure 5: Duchamp book exchange](p684fig5.jpg)</td>

<td>

#### Duchamp.

Duchamp is similar in structure to Briet and Casanova, but much smaller, accommodating far fewer books and larger books only diagonally. It is painted in bright colours with a quote about books and gardens on one side, and a notice board on the other side. It is perched on the side of a fence that borders a popular sidewalk route between a public park and a busy commercial street. This steward offered to host the exchange on her fence, relieving the person who built it from having to get city permission to install it on public property.

</td>

</tr>

<tr>

<td style="text-align:center;">![Figure 6: Dewey book exchange](p684fig6.jpg)</td>

<td>

#### Dewey.

Unlike the other exchanges in our study, this exchange was conceived after the materials for the structure were collected. The steward found a large shelf with a drop down desk in an alley and, eventually, decided to use it for a book exchange. He and some friends fixed the shelf to the fence in the front yard and attached a roof. They modified the design considerably over time, adding more support, glass doors, garden plots, and a wood bench. The exchange was a popular interlude on a busy bike route and neighbourhood block.

</td>

</tr>

<tr>

<td style="text-align:center;">![Figure 7: Carnegie book exchange](p684fig7.jpg)</td>

<td>

#### Carnegie.

As the only registered Little Free Library in our study, this exchange was created after the steward heard a news story about little free libraries. It was created from an old Ikea cabinet: the steward added a roof, a coat of paint, and set it on a concrete slab at the edge of her property. It sits directly on the ground, allowing children to access it very easily. The lower shelf is reserved for children's books, the top shelf for adult books. It is located in a quiet residential neighbourhood.

</td>

</tr>

<tr>

<td style="text-align:center;">![Figure 8: Borges book exchange](p684fig8.jpg)</td>

<td>

#### Borges.

This exchange is located in a quiet, residential neighbourhood along a wood chip path created to substitute for the absent sidewalk on that block. The path weaves along the residents' lush community garden, allowing neighbours to visit the exchange, the garden and the chicken coop as they stroll through the property. The exchange itself is tall and thin—designed to accommodate adult paperback books. The door was an upcycled cabinet door outfitted with a spoon handle.

</td>

</tr>

</tbody>

</table>

Many of the book exchanges in our study area inherited characteristics from other book exchanges—both from the study area and beyond. Many showed similar features like a chalkboard, notice board and whimsical, brightly coloured decorations, and four of the six exchanges were similar in overall shape. But differences were many: the volume of the exchanges, the exchange location on a quiet or busy block, the number and age of stewards maintaining each exchange and the level of steward involvement.

_Table 1_ highlights some notable differences between the exchanges in our study. For steward involvement, we have assigned high, medium or low to describe how much time and resources the stewards allocated to their exchange. High steward involvement was assigned for cases where the stewards checked-up on the exchange and curated the contents on a daily basis and low steward involvement for cases where the stewards checked-up on the exchange or curated the contents very rarely (i.e., once a month).

<table class="center"><caption>  
Table 1: Comparing basic book exchange characteristics</caption>

<tbody>

<tr>

<th>Code name</th>

<th>Installation date</th>

<th>Number of stewards</th>

<th>Location of stewards</th>

<th>Steward involvement</th>

<th>Average no. of items</th>

<th>Average no. of transactions (per week)</th>

</tr>

<tr>

<th>Briet</th>

<td>Spring 2011</td>

<td style="text-align:center;">3</td>

<td>Within 200 metres</td>

<td>High</td>

<td style="text-align:center;">84</td>

<td style="text-align:center;">139</td>

</tr>

<tr>

<th>Casanova</th>

<td>Fall 2012</td>

<td style="text-align:center;">6+</td>

<td>Within a block</td>

<td>Medium</td>

<td style="text-align:center;">94</td>

<td style="text-align:center;">82</td>

</tr>

<tr>

<th>Duchamp</th>

<td>Summer 2013</td>

<td style="text-align:center;">2</td>

<td>One onsite, one offsite</td>

<td>Low</td>

<td style="text-align:center;">34</td>

<td style="text-align:center;">59</td>

</tr>

<tr>

<th>Dewey</th>

<td>Spring 2012</td>

<td style="text-align:center;">2</td>

<td>One onsite, one offsite</td>

<td>Medium</td>

<td style="text-align:center;">157</td>

<td style="text-align:center;">138</td>

</tr>

<tr>

<th>Carnegie</th>

<td>Spring 2012</td>

<td style="text-align:center;">1</td>

<td>Onsite</td>

<td>Low</td>

<td style="text-align:center;">50</td>

<td style="text-align:center;">12</td>

</tr>

<tr>

<th>Borges</th>

<td>Summer 2012</td>

<td style="text-align:center;">2</td>

<td>Onsite</td>

<td>Medium</td>

<td style="text-align:center;">50</td>

<td style="text-align:center;">15</td>

</tr>

</tbody>

</table>

For average number of transactions a week, each transaction is either an item that was contributed or an item that was taken. This amount does not reflect the possibility of more than one item taken or left during a single visit or of items pruned by the stewards. In addition, it does not reflect an item that was left and then taken between the dates our inventories were taken. Duchamp, for instance, had nearly twice as many transactions as its average number of items, suggesting a complete turnover within a week. It is very possible that more transactions occurred within that week, but the items were not available for recording during either of our inventory dates.

### Themes of practice

Below we introduce themes related to the local, situated practices that emerged from our interviews with the stewards of the exchanges described above. These are practices that stewards adopt as they introduce their book exchange in their neighbourhood—negotiating the use of soft-edge spaces in neighbourhoods and encouraging particular practices among their neighbours. To illustrate the practice-related coding scheme we created to develop these themes, we provide _Table 2_ below followed by further explication of the themes.

<table class="center"><caption>  
Table 2: Overview of interview codes  
</caption>

<tbody>

<tr>

<th>Theme</th>

<th>Codes and Sub codes (with examples)</th>

</tr>

<tr>

<th rowspan="3">Negotiating neighbourhood spaces</th>

<td>

Soft edges and interactions with neighbours

*   Dissolving/bridging public-private divide (e.g., City/private boundary; Ownership of quasi public space)
*   Ownership for renters | landlords (e.g., Permission avoided)
*   Exchange placement (e.g., Location consideration; Locating exchange according to walker behaviour)

</td>

</tr>

<tr>

<td>

Activating public spaces

*   Interactive spaces (e.g., Activating places for community participation; Recreate spaces)
*   Book exchange as a transformational tool or toolkit (e.g., Exchange as important neighbourhood architecture; Book exchange as an instrument of play; Book exchange part of neighbourhood culture of chatting and hanging out)
*   Book exchange as a destination (e.g., Neighbourhood focal point; Role as a point of interaction)

</td>

</tr>

<tr>

<td>

Interactions with local government

*   Negotiating exchange precedent (e.g., City permission avoidance; Describing exchange as grey area; Navigating city departments for approval; Tension between stewards and city)
*   City departments at odds with each other
*   Frustration with city rules and regulations (e.g., City making hurdles for project; City too regulatory; Disappointment with city rules; Expressing disdain for city processes)
*   No support from city government (e.g., City not allowing bench; City as an unwilling partner)
*   Support from city government (e.g., City finally relents; City adopting exchange; Getting city permission; Praise from city)

</td>

</tr>

<tr>

<th rowspan="5">Shaping neighbourhood practices</th>

<td>

Walking and biking

*   Exchange intended for particular people
*   Exchange placement (e.g., Location consideration)
*   Exchange role in neighbourhood (e.g., Exchange as neighbourhood destination)
*   Walkable neighbourhood

</td>

</tr>

<tr>

<td>

Early reading

*   Design elements (e.g., Exchange organization; Places to sit)
*   Exchange role for children and youth
*   Exchange role in reading (e.g., Providing books for low-income families; Reading encouraged among neighbourhood kids)
*   Motivation for book exchange (e.g., Kids' book exchange)

</td>

</tr>

<tr>

<td>

Neighbourhood socializing

*   Design elements (e.g., Places to sit; garden underneath)
*   Exchange as transformational tool for neighbourhood (e.g., Meeting new people; contribute to culture of chatting, hanging out; exchange as social catalyst)
*   Influencing neighbourhood dynamics
*   Organize neighbourhood events

</td>

</tr>

<tr>

<td>

Information sharing

*   Design elements (e.g., Making areas for communication)
*   Exchange as opportunity to share books
*   Exchange role as information hub (e.g., Sharing neighbourhood notices; going to events posted on noticeboard)

</td>

</tr>

<tr>

<td>

Indirect interacting

*   Design elements (e.g., Chalk board or notice board for interaction)
*   Exchange as transformational tool for neighbourhood (e.g., Exchange as an instrument of play)
*   Interactive spaces (e.g., Activating places for community participation; Importance of interacting with spaces)

</td>

</tr>

</tbody>

</table>

#### Negotiating neighbourhood spaces.

Quasi-public and private spaces are common in the neighbourhoods of our study area. They exist as the street facing margins of private properties, as boulevards, project out into traffic-calmed intersections as bulge-outs and dot intersections as traffic circles. Leveraging the ambiguity of these spaces, stewards create and shape potential interactions between neighbours, city officials and the book exchanges themselves.

##### Soft edges and interactions with neighbours.

The space a book exchange occupies is a key component of understanding the practices that develop through and around their physical structure. The location of the space influences interactions between and among key interestede parties—stewards, visitors and the municipality in which they are located—and the book exchange itself.

The six exchanges in our investigation were all placed on _soft edges_, a nebulous zone of semi-private space dividing private and public space ([Gehl, 1986, p. 95](#geh86)). According to Wilkerson, Carlson, Yen, and Michael ([2011](#wil11)), soft edges are spaces where people are more likely to interact. Consider spaces such as front lawns in suburban areas of North America, where neighbours are more likely to see and interact with each other compared to a purely private (e.g., inside a home) or purely public (e.g., large municipal park) spaces. By placing their exchanges on soft edges, stewards capitalise on a physical space that is more likely to scaffold interactions with and between passers-by.

##### Activating public spaces.

We witnessed a strong desire among some of the stewards in our study to personalise public or quasi-public spaces that are otherwise uninteresting, homogenous or boring. One steward, Janie, believes city policies contribute to the mall-like atmosphere of public space and represents a disservice to the city. Stewards like Janie countered bureaucratic sterility by _activating_ unused space with book exchanges.

By building an exchange in these areas, stewards are engaging in a practice to co-opt and alter public space to impart character and make it more interesting. One steward describes this practice as a means to _activate_ these passive parcels to display what is creatively possible, making homogenous space interactive. Two excerpts from our interviews demonstrate well these stewards' contempt for unused public space and the agency they use to combat it.

> Carrie: It's kind of that same thing of just, like, let's do interesting things with these spaces that, you know, rather than just sort of leave them as being these kind of undefined-without-much-character areas, um, can we do something different with them? Can we put a garden in? Can we make a little path? Can we, like, make it interesting for people to walk around it and you know, interact with that space differently and, so yeah, I would say that the [exchange] is one part of that.

> Janie: I think that's what makes cities really interesting is when people make it more personal and maybe it doesn't fit with everybody's viewpoints, but it becomes less, you know, like a mall. Or you know, it becomes less, it becomes more unique and that's why I like it.

In our conversations with stewards, very often, the topic of transforming neighbourhood spaces and book exchanges were not talked about in isolation. Other activities that complement the purpose of the exchange were fairly common. Community gardening, among other activities, were often not only mentioned in our conversations but also located nearby most of the exchanges in our study. Endowing a place with character and making it welcoming, to Carrie, includes 'everything from the [exchange] to our gardens to our chickens to our house to the fact that we're often outside, you know, puttering around...and we chit-chat with people'. Ten yards from the Dewey exchange is another example. Jack explains how neighbours are _activating_ a traffic circle with the addition of a table and four chairs, transforming it into a space that 'draws people into it'.

Our conversations with stewards revealed the breadth of activity aimed at creating interactive, interesting and welcoming spaces. While book exchanges are the focus of this research project, some stewards describe the practice of transforming spaces through additional local activities and projects; neighbourhood book exchanges are one aspect of a much larger effort to reinvent spaces in the soft edges of neighbourhoods.

##### Interactions with local government.

When stewards establish a new book exchange on a soft edge in the city, they also raise the potential for engagements—positive and/or negative—with municipal representatives. At the time our participating exchanges were built there were no existing bylaws or municipal processes to guide how the exchanges were installed. For example, the Borges stewards were unsure how the city would interpret the legality of their exchange, which was installed without city permission on the boulevard beside their house. While they were aware that city bylaws covered other practices, like boulevard gardens, they considered the book exchange as decidedly different.

> [Steward]: I know there actually are guidelines on boulevard gardening and that sort of thing. So there is, like, some, I guess formal-ish treatment of using public right of way space. There's not anything on [book exchanges].

Three stewards mentioned how they leveraged the ambiguity of soft edges to avoid asking permission for using city property.

> Jack: [I]t kind of blurs that in-between space between the house and the public sidewalk and we weren't certain of it, but now we are certain, like, the fence is the property line. But this space kind of feels in-between in that it's directly in front of the house, but also next to the sidewalk. So, that's why we wanted it to be this in-between public private space that we could slightly oversee from the house and is close by and is not, um, we also didn't have to ask anyone else, technically.

The oldest exchange in our study was also the first to involve the city. The Briet stewards considered themselves trailblazers, 'I think we were just, you know, clearing the way… the underbrush (laughing) to make a path for this to happen'. They were the first to negotiate for the city's approval of their exchange project, and they had to introduce the city to the idea of a neighbourhood book exchange. This negotiation was central to their framing of the challenges of initiating this practice. They expressed disdain for the approval process, disappointment with the city's rules and frustration with the city's bureaucracy. This sentiment towards the municipal process was common among all the stewards in our study.

Stewards of three of the six exchanges in our study avoided city permissions altogether, but their reasons varied substantially from each other. In the following excerpts we learn that one steward believes that permission is not necessary, while at the same time justifying the avoidance of negotiating city permission. Another steward avoids city permission for reasons of expediency—the city takes too long to process requests. In the third excerpt, the stewards imagine the city would take a supportive stance with regards to their exchange, but acknowledges taking a risk.

> Diane: When people do ask permission sometimes it suggests that they need that permission to do something that which they should be able to do anyways, like, it's not harming anybody.

> Janie: I guess it just takes a long time for city, um, anything that goes through the city it just takes forever. So he's [original steward] like, 'Oh, can we just put in on your fence?' And you're happy; extremely happy to say yes so he didn't have to go through all the paperwork.

> Carrie: It's not our own private property anyways. So, we've kind of taken a little bit of that risk. But I also know that the city is supportive of urban agriculture, and they are supportive of community building and all these things. So, I just, I think it's pretty unlikely that, even if we were breaking any current bylaws or anything that they would be strictly enforced.

These excerpts suggest that the physically, grounded nature of the exchanges resulted in opportunities and challenges for stewards when they considered the outcomes of exchange placement. By placing the book exchanges within soft edges, they increase the exchange's visibility and provide passers-by with unfettered access. Yet, the visibility and access it affords can put them in direct confrontation with city bylaws and unknown consequences of disregarding them.

##### Parameters of city involvement.

Stewards tended to adopt a deeply critical tone when describing their interactions with the city and, in some cases, deliberately avoided negotiations with the city altogether. However, this did not completely negate some stewards' desire to involve the city in neighbourhood book exchange projects in nominal ways. For stewards, defining the parameters of the city's involvement was complex and nuanced.

Two of the stewards, Valerie and Janie, definitively rejected the idea of the city being involved in any part of the exchange practice. Despite one of these stewards applying for a small neighbourhood grant to initiate their exchange, neither supported the idea of the city taking on that role.

> Janie: No, I think the city should just, like, butt out.

> Valerie: No. I don't think so. I don't see a role for them. Other than to get out of the way. If they were to put up barriers I'd be upset about that.

The Borges and Briet stewards held similar opinions regarding city involvement. These stewards, however, qualified one aspect where they could imagine city-steward collaboration—through small neighbourhood grants or similar funding to ease the burden of start-up costs for exchange projects. But that marked the extent they were willing to collaborate with the city.

The remaining two stewards were indecisive when we asked them whether the city should have a role to play in exchange projects. Their indecision had more to do with the breadth, not the depth of the city's involvement. Both stewards saw a very minimal role for the city. Where they thought the city could be a helpful partner was promotional and monetary in nature as opposed to having a hand in establishing the exchange (e.g., selecting a place, constructing it, stocking it).

At this point a line begins to emerge around exchange practice and city involvement. While stewards' opinion of city involvement is slightly discordant, they agree that the city's role should end at monetary or, possibly, promotional assistance. This is significant because it reveals how the stewards demarcate responsibility for soft edge spaces in their neighbourhoods. The stewards consider themselves and their neighbours responsible for shaping the type and tone of practices in their neighbourhood, while the municipal government is responsible for supporting those initiatives.

The stewards in our study intentionally situated their exchanges in the soft edges of their neighbourhoods. They installed their exchanges alongside the sidewalk, either on the periphery of their property or on the fringes of city property, where their neighbours were more likely to interact. But, as the stewards assume a role in developing these spaces, they disrupt the purview of the city, and, subsequently, the stewards either negotiate or evade permission to install their exchange. While soft edges provide an ideal location for installing a neighbourhood book exchange, they are ambiguous spaces; in effect, those who choose to leverage those spaces must decide how to traverse city policies and practices.

#### Shaping neighbourhood practices.

The stewards designed their exchange to encourage particular practices in their neighbourhoods. Each exchange was markedly different, reflecting the different priorities emphasised by each steward. But there were some shared priorities. Many of the stewards wanted to encourage one or more of the following practices: walking and biking, early reading, neighbourhood socialising, information sharing and indirect interacting. The stewards designed their exchange according to the practices they hoped to promote in the neighbourhood.

##### Walking and biking.

Neighbourhood book exchanges were often designed to encourage pedestrian and bicycle traffic in the neighbourhood. All the exchanges were built alongside a sidewalk, where pedestrians could easily access the exchange while walking by. Several stewards even considered the amount of pedestrian traffic before selecting a location for the exchange; they choose locations based on how frequently people passed by on their way to other destinations—parks and schools, for instance. Biking was also encouraged. Some stewards choose locations on popular bike routes; one exchange even provided bike parking, so that bicyclists could hang their bikes while they browsed the exchange. These design decisions indicate that stewards privileged pedestrians and bicyclists; they were less concerned with the requirements of visitors who arrived by car.

> Carrie: Our desire is more that it's directed to people walking through the neighbourhood or riding their bikes or whatever, than to generate a lot of car traffic to come and, you know, take books or put books in.

One steward excluded car traffic more overtly, 'It's not a drive thru. It's not McBooks'.

By installing a neighbourhood book exchange, some stewards hoped to create a _neighbourhood destination_—a place where neighbours could walk to and linger at ([Alfonzo, 2005](#alf05); [Wilkerson, 2011](#wil11)). Valerie described how neighbours 'make it part of their walk', and Carrie explained how:

> A lot of times people kind of have a circuit, especially when they're with their little kids. Like, they come to our house or our property and they look at the garden, they look at the [exchange]. They go back and look at the chickens and it's sort of like, that's kind of a destination for part of their walk with their little ones.

These exchanges were created by the stewards to entice their neighbours to spend more time in the neighbourhood. By creating a neighbourhood destination, the stewards wanted, as Valerie described it, '_to get people to come out and walk on the street and be around_'.

##### Early reading.

Some stewards wanted to encourage reading. By installing a neighbourhood book exchange, they hoped to make books more readily available to their neighbours and, thereby, encourage their neighbours to read. Janie commented on how '_everyone has that opportunity to potentially go and get a book_' from the exchange. For her, people in the neighbourhood may be more likely to read if the books were more readily available to them. Other stewards emphasised how it was important to provide low-income households with access to books—especially children's books.

Overall, most stewards who expressed a desire to encourage reading practices were especially interested in encouraging kids to read. These stewards designed their exchanges accordingly. They dedicated a shelf, usually the lowermost shelf, for children's books. This shelf was large and deep enough to accommodate the range of shapes and sizes typical of children's books. It was also low enough for kids to reach, and these stewards designed doors that the children could open and close themselves. By designing a child-friendly book exchange, the stewards hoped to encourage more families and kids to participate in the exchange and in reading practices.

Some of the stewards also hoped to encourage visitors to read at the exchange. Three of the exchanges—Borges, Briet and Dewey—placed benches or chairs nearby, in hopes that visitors would spend some time reading in the neighbourhood space. These benches and chairs also helped facilitate a social space—another commonly shared consideration.

##### Neighbourhood socialising.

The exchanges were also considered a social space. Some stewards wanted to create a space where neighbours could bump into each other or gather for planned events. By installing their exchange, these stewards hoped to encourage their neighbours to socialise more.

Location was critical for creating a social space. The stewards placed exchanges by sidewalks, so the exchanges were accessible to any and all walking by. They hoped neighbours would spend time chatting after bumping into each other while visiting or passing the exchange. Some stewards choose areas that were larger and more accommodating. In one case, the stewards chose the sidewalk on a traffic bulge—an extension of the curb and sidewalk that narrows the roadway to slow traffic. Jean considered how this location '_gives room here for people to stand around without being in other peoples' way_'. And some stewards developed their spaces to include more incentives to linger and engage with neighbours. Several exchanges included places to sit. Others included gardens, murals and other features intended to make the exchange more welcoming.

In addition, the book exchange provided an icebreaker for those who bumped into each other at the exchange. Neighbours could immediately find common ground for conversation. Jack reflected on how the exchange '_is a really good prompt, in a way, for people to hang out and get to know each other_'. And several stewards observed neighbours meeting and chatting at the exchange—something they had each hoped to encourage by creating the exchange.

The exchange also provides a space for hosting neighbourhood events. A few of the exchanges had been the gathering point for block parties, Christmas carolling or other planned gatherings—reinforcing the exchange's role as a place for neighbours to socialise. The stewards aimed to facilitate more socialising in the neighbourhood by drawing neighbours into an interesting and engaging space.

##### Information sharing.

The exchange was also created to facilitate asynchronous information sharing among neighbours. Stewards designed their exchanges differently, depending on the types of information they wanted neighbours to share and the types of informational interactions they hoped neighbours would experience. Some stewards wanted to encourage book sharing and some wanted to facilitate other types of information sharing—especially for local events, services and issues.

Several of the stewards included bulletin boards in their exchange designs to facilitate sharing community notices. For some stewards, it was important to include a space for visitors to post information. For others, they anticipated that visitors were going to post information—garage sale flyers and missing cat posters—anyways, so they decided to include a space for them. But the notice boards became an important feature for all these exchanges. Neighbours used them to share information on local services and community events. Some also posted information regarding local issues—for instance, one exchange hosted a laminated poster promoting awareness on a local environmental concern. Some of these stewards monitored the notice boards, removing dated items and managing available space.

> Jack: Sometimes we take down the odd, like, business advertisement. Cause we have a little notice board and it's limited space, so if we get flyers for something that doesn't—it's not a community based event, doesn't have any relevance to the neighbourhood, the area, or if it's someone's business, you know, it's like, well, it's not a place for your advertisements. So, we make that call sometimes.

Most stewards voiced opinions similar to Jack; local events and issues were a higher priority than commercial or non-local notices. The exchanges were considered an important feature for engaging the neighbours and supporting the neighbourhood, and, subsequently, it was important that the notice board reflect primarily local interests.

Some stewards also emphasised the value of sharing books. Four stewards lamented how they had accumulated unwieldy book collections, so they took measures to 'pass along', 'downsize', or 'give them back to the world'. The book exchange provided a venue to share personal libraries with others. For these stewards, the recycling aspect was important; they wanted their books to find renewed value with someone who needed them.

> Veronica: For me, it was more…time to pass these cookbooks on. I'm not using them anymore. I had been holding on to parenting books... It was time for me to pass those books along. Although, for me, I love books so much. It's actually a little bit hard to give them away. But knowing that someone is going to benefit from them. It made it a little bit easier.

By introducing a book exchange to the neighbourhood, the stewards provided a way for neighbours to share books. And by enhancing the design to include a notice board some of the stewards provided a way for neighbours to share other types of information asynchronously.

##### Indirect interacting.

Finally, the stewards created their exchanges in hopes of facilitating fun and playful connections in the neighbourhood. Several of the stewards emphasised the importance of engaging visitors in friendly exchanges—whether through exchanging books, posting notices or writing messages—as a way of propagating a sense of goodwill and amity in the neighbourhood.

> Carrie: We did it for the community building aspect of it; to give people an opportunity to have this interesting interaction with people that they may not ever necessarily meet. You know, either us or whoever put the book in that they took out or whoever, you know, they put a book in and then some other stranger takes that book out. Like it's kind of an interesting dynamic of goodwill directed at strangers that you may not ever actually have any sort of reciprocal relationship with.

While exchanging books was the primary design feature for encouraging benevolent but anonymous interactions, some stewards incorporated other design features to encourage similar interactions. Bulletin boards offered an opportunity to post messages and share information. Chalkboards provided an opportunity to write messages or draw pictures. And, interestingly, several of the stewards were entertaining the idea of new design features like logbooks and letter drops to provide additional opportunities for interactivity. For these stewards, their exchange provided an opportunity to propagate a sense of neighbourliness through any number of fun and interesting informational interactions.

No two neighbourhood book exchanges are exactly alike. They are designed according to the types of practices a steward hopes to encourage in their neighbourhood. These desired practices varied widely; the stewards, however, shared some common goals. Each steward in our study hoped to encourage some of the practices outlined above, and, most interestingly, they all emphasised a hope to facilitate more social connections in their neighbourhoods.

## Discussion

A close examination of the media portrayal of neighbourhood book exchanges and the experiences of stewards in our study reveal discrepancies that are worthy of further reflection. The media articles we analysed suggest a homogenous, global movement of book exchanges. The articles use broad and nebulous concepts to describe the purpose of the exchanges, and they reuse phrases, descriptions and anecdotes. This presents a single narrative of the neighbourhood book exchange phenomenon—what they are, how they started and what they accomplish. This narrative does not accurately account for the local information practices we observed in our study.

The media narrative suggest exchanges share two main goals—community building and literacy promotion. In contrast, the stewards in our study did not identify with these goals; rather, they had developed far more specific, detailed and individual views of what their exchanges could accomplish. _Neighbourhood socialising_, _activating public spaces_, and _indirect interactions_ are more nuanced and not necessarily equivalent to _community building_. In fact, there was scant reference to _community_ and _community building_ in our interviews with stewards; one steward even denied the role of community building overtly. Instead, the stewards introduced very specific ideas of the types of social interactions they hoped to facilitate in their neighbourhoods. Similarly, our themes for _early reading_ and _information sharing_ address the very specific ideas the stewards developed regarding access to books and other information; they did not use the term _literacy_ and did not consider the exchange as a tool to promote literacy. Their ideas are far more nuanced than simply _literacy promotion_. Using catch-all phrases like community building and literacy promotion do little to advance our understanding of how neighbourhood book exchanges impact neighbours.

In addition, the media articles present the exchanges as belonging to a cohesive global movement championed by the Little Free Library. However, none of the stewards in our study identified with the Little Free Library mission or movement—not even the steward who registered her exchange in their network. The exchanges in our study were built for their immediate neighbourhood; they were not considered in a larger context beyond a few city blocks. And each of these exchanges was designed according to each steward's nuanced, locally-situated ideas of what the exchange could accomplish in the neighbourhood.

Lastly, we revisit the three questions that guided our research and reflect on the conclusions we can draw and the questions we can pose following our analysis of media articles and investigation of six exchanges.

### What are the roles and information practices developing around the book exchanges?

The installation of a neighbourhood book exchange marks the inception of new information practices in the neighbourhood. When a steward initiates an exchange, they create an opportunity for neighbours to share information—books, primarily. These stewards may or may not monitor, remove or stock particular items in the exchange. Visitors are invited to browse the exchange and to take or leave items. In some cases, visitors are invited to participate in other information-based interactions—writing messages, posting notices or drawing pictures. The stewards design their exchange according to the information practices they hope to facilitate and the desired outcomes they hope to impart in their neighbourhood.

Interestingly, the stewards are establishing more than novel engagements with information. They are also introducing a grounded, local information practice to foster social interactions in their neighbourhood.

### Why are people designing and (re)designing these exchanges?

When stewards adopt the idea of a neighbourhood book exchange, they design their installation according to the practices they want to encourage in their neighbourhood. Sharing books was consistently mentioned as a desired neighbourhood information practice, but sharing books is not in and of itself a satisfactory explanation for the stewards' motivation.

Instead, what emerged from our observations and interviews with stewards is that designing and redesigning book exchanges can be understood as a means to encourage local interaction. Stewards designed exchanges to be welcoming and engaging destinations where neighbours could interact, both directly and indirectly. Essentially, the stewards _activated_ unused soft edge spaces to create opportunities for social interactions in the neighbourhood. Scaffolding these interactions was important for these stewards and served as an opportunity to reorient themselves in the neighbourhood.

The stewards routinely redesigned their exchanges, too. These modifications were usually directed towards encouraging more social practices: adding a bench to encourage people to linger longer, a garden to attract and welcome visitors, a whiteboard for sharing messages with neighbours or a letter drop to introduce new indirect interactions. As stewards discussed the changes they had implemented or planned to implement, they emphasised changes that would enhance the exchanges' role as a tool for scaffolding social interactions in the neighbourhood. Interestingly, the stewards felt that they—and not the city—were particularly well suited to scaffold the social interaction at the neighbourhood level.

### Is there evidence that these are local, grass root endeavours?

The media narrative emphasises a simplistic, global narrative for the exchanges; articles frequently refer to them as a cohesive 'global movement' (e.g., [Aldrich, 2014](#ald14); [Peterson, 2012](#pet12); [Schaff, 2012](#sch12)). However, our research suggests that neighbourhood book exchanges are local, grassroots endeavours.

The exchanges are local—each had particularities endemic to that location and steward(s). These particularities manifested themselves in form and function and were directly related to the practices each steward emphasised, which often differed in subtle ways. While the exchanges in our study were influenced by other exchanges and sometimes inherited common characteristics, the stewards were often only peripherally aware of other exchanges in the same neighbourhood. The exchanges were intended for the local population—the immediate neighbours and, sometimes, people who pass through the neighbourhood on their daily commute or frequent visits. They were not advertised to a wider population of the city.

The book exchanges are deliberately grassroots endeavours. The book exchanges in our study were built and maintained by individuals or small neighbourhood associations. Stewards either saw a very limited role for the city government or they were loath to involve them at all. Neither did these stewards see a large role for international organizations like the Little Free Library. Only one steward registered her book exchange as a Little Free Library, though her installation reflected many other grassroots aspects: she referred to it as her '_book house_', constructed it from repurposed Ikea furniture and developed her own ideas of how it should be used and what it could accomplish in her neighbourhood. As local endeavours, the exchanges in our study were reinvented by the stewards to suit their goals and the dynamic of their neighbourhoods. Interestingly, this is a point of departure from some municipal and commercial endeavours, as well as a departure from the dominant media narrative.

At the end of our study, we became aware of an effort by the city in which we conducted our study to install a handful of exchanges. Similar efforts have been proposed and acted on in other places as well (e.g., [Vian, 2014](#via14); [Vogler, 2014](#vog14)). These initiatives aim to create and install exchanges with an unspecified level of participation by nearby residents. The stewards in our study emphasised the importance of neighbourhood stewardship; an exchange established by a neighbour or neighbours will engender a sense of neighbourhood ownership, and, subsequently, the neighbours will be more likely to adopt and use the exchange. We question what neighbourhood participation and stewardship will look like for a municipality-imposed exchange.

The local, grassroots aspect of book exchanges that is so prevalent in our study are conspicuously absent in the media's narrative. Instead, it tends to reflect the mission of the Little Free Library. Significantly, Little Free Library offers opportunities to outsource virtually all aspects of book exchanges—from production to practice. While their customers are rooted in their respective neighbourhoods, the organization offers membership in a global network, as well as prefabricated exchanges and instructions for handling aspects of stewardship. These services seem removed from the local, grassroots, agentic aspect of book exchanges observed in our study. Based on our observations and interviews, we consider the exchanges in our study as grounded, local initiatives, where the stewards and visitors continually negotiate situated practices for the book exchanges.

## Limitations

This study of neighbourhood book exchanges, as an exploratory study of the phenomenon, is limited in the scope of the research and in the extensibility of our findings. At the beginning of our study, while conceptualising our work and developing our methods, we borrowed the questionnaire sampling method from well-established, robust sociological research related to neighbourhood-based phenomenon (e.g., [Greenbaum and Greenbaum, 1985](#gre85)). We do not fully conceptualise or operationalise neighbourhood in this paper. Future researchers may wish to further develop this grounding of the concept of neighbourhood, but we consider it outside the bounds of this particular exploratory study.

Our study provides a snapshot of neighbourhood book exchanges as they were discussed in the media during a specific time period and as they were experienced by the stewards of a limited number of exchanges in one geographic area. Our analysis of media articles was restricted to a two-year period, from March 2011 through March 2013\. Since then articles addressing the subject have continued to flourish, and these more recent articles may reflect different angles and patterns than those reported here. Our investigation of six exchanges in a metropolitan area of the Pacific Northwest is limited by the length of time we spent visiting the exchanges, the specifics of the area where the six exchanges were located, the number of stewards with whom we spoke and the limited number of responses to the questionnaires. As a result, the findings from this study may not apply to other book exchanges, in fact differences may help corroborate our argument. We hope future studies continue to develop a rich understanding of the emergent neighbourhood book exchange phenomenon.

## Conclusion

Through the exploratory project described in this paper, we are well positioned to ask further questions regarding neighbourhood book exchanges' purported role in communities and place in the information ecosystem. Our examination of six neighbourhood book exchange installations in the Pacific Northwest includes an analysis of interviews with those who crafted and maintain the six exchanges studied, information from a weekly inventory of the six exchanges over a three-month period and insights from a survey distributed across two of the neighbourhoods.

We found that the media articles share a similar narrative regarding book exchanges across a large geographical area, but this narrative diverges from our observations of neighbourhood book exchange installations. The media articles emphasise the exchanges' role as a means to catalyse community and promote literacy. This narrative is consistent with the mission of Little Free Library. But, significantly, the exchanges in our study area appear be more localized efforts. While the stewards typically found inspiration to install their own exchanges after seeing other exchanges, each steward reinvented the purpose and, subsequently, the design of their exchange to suit the practices they hoped to encourage in their neighbourhood. And, most often, their efforts to maintain a book exchange in their neighbourhood were one aspect of a larger effort to transform soft edge spaces into interesting and engaging neighbourhood places.

Our work positions us to postulate that people are creating, installing and using these exchanges in neighbourhoods already replete with wi-fi-enabled public libraries, coffee shops and bookstores to reorient themselves with their immediate neighbourhoods. While not suggesting a shift away from the use of digital technologies, neighbourhood book exchanges appear to be initiated by stewards to reinvigorate, ground and sustain relationships with their neighbours.

## Acknowledgements

We would like to thank all the Neighbourhood Book Exchange stewards who participated in this study, as well as the neighbours and visitors of the exchanges we studied. In addition, we express deep appreciation to Dr. Eric Meyers, Maggie Castor, Sheena Campbell and Robert Makinson for their valuable contributions to aspects of the project.

## About the authors

**Tenny Webster** is an Information Specialist for the Appalachian Trail Conservancy, Harpers Ferry, West Virginia. He received his Bachelor's degree in Sociology from the University of Montana, Missoula and Master of Library and Information Studies from the University of British Columbia, Vancouver, Canada. He can be contacted at [erictwebster@gmail.com](mailto:erictwebster@gmail.com)  
**Kathleen Gollner** is an Online Content Specialist for PubMed, National Center for Biotechnology Information, National Library of Medicine. She completed her Master of Library and Information Studies at the University of British Columbia in Vancouver, Canada. She can be reached at [kathleen.gollner@gmail.com](mailto:kathleen.gollner@gmail.com)  
**Lisa P. Nathan** is an Assistant Professor and Coordinator of the First Nations Curriculum Concentration at the University of British Columbia's iSchool in Vancouver, Canada. She directs the Sustaining Information Practice Research and Design Studio. She is the chair of the ACM SIG-CHI HCI & Sustainability Community. Lisa can be contacted at [lisa.nathan@ubc.ca](mailto:lisa.nathan@ubc.ca)

#### References

*   Aldrich, M. (2014, July 27). [The low-tech appeal of little free libraries](http://www.webcitation.org/6ZZkuJOq8). _The Atlantic_. Retrieved from http://www.theatlantic.com/education/archive/2014/07/the-low-tech-appeal-of-little-free-libraries/375101/ (Archived by WebCite® at http://www.webcitation.org/6ZZkuJOq8)
*   Alfonzo, M.A. (2005). To walk or not to walk? The hierarchy of walking needs. _Environment and Behaviour, 37_, 808-836.
*   Bijker, W. E. (1995). _Of bicycles, bakelites, and bulbs: toward a theory of sociotechnical change_. Cambridge, MA: MIT Press.
*   Charmaz, K. (2014). _Constructing grounded theory_ (2nd ed.). Thousand Oaks, CA: SAGE Publications Inc.
*   Christian, S. (2012, June 17). [Take a book, leave a book at Little Free Library in Roseville](http://www.webcitation.org/6ZZm7DK6w). _The Press Tribune_. Retrieved from http://rosevillept.com/detail/210963.html (Archived by WebCite® at http://www.webcitation.org/6ZZm7DK6w)
*   Cooksey, E.B. (2004). Too important to be left to chance—serendipity and the digital library. _Science & Technology Libraries, 25_(1), 23-32.
*   Disalvo, C., Redström, J. & Watson, M. (2013). Commentaries on the special issue on practice-oriented approaches to sustainable HCI. _ACM Transactions on Computer-Human Interaction, 20_(4), 1-15.
*   Dourish, P. & Anderson, K. (2006). Collective information practice: exploring privacy and security as social and cultural phenomena. _Human-Computer Interaction, 21_(3), 319–342.
*   Edwards P. J., Roberts I., Clarke M. J., DiGuiseppi C., Pratap S., Wentz R & Kwan, I. (2002). Increasing response rates to postal questionnaires: systematic review._British Medical Journal, 324_(7347), 1183-1185.
*   Edwards P. J., Roberts I., Clarke M. J., DiGuiseppi C., Wentz R., Kwan I., Cooper R., Felix L. M. & Pratap S. (2009). [Methods to increase response to postal and electronic questionnaires]((http://www.webcitation.org/6b0BPBKJB)). _Cochrane Database of Systematic Reviews_, (3). Retrieved from http://www.cochranelibrary.com/enhanced/doi/10.1002/14651858.MR000008.pub4(Archived by WebCite® at http://www.webcitation.org/6b0BPBKJB)
*   Elsweiler, D., Wilson, M.L. & Lunn, B.K. (2011). Chapter 9: Understanding casual-leisure information behaviour. In A. Spink & J. Heinström (Eds.), _New directions in information behaviour_. Bradford, UK: Emerald Group Publishing.
*   Erdelez, S. (1995). _Information encountering: an exploration beyond information seeking_. Unpublished doctoral dissertation, Syracuse University, Syracuse, New York, USA.
*   Fidel, R. (2008). Are we there yet? Mixed methods research in library and information science. _Library and Information Science Research, 30_(4), 265–272.
*   Gehl, J. (1986). "Soft edges" in residential streets. _Scandinavian Housing and Planning Research, 3_(2), 89-102.
*   Gessner, J. (2011, December 15). [Little Free Library: special ed. teacher brings concept to Burnsville](http://www.webcitation.org/6Za8bPXAt). _Sun This Week._ Retrieved from http://sunthisweek.com/2011/12/15/little-free-library/ (Archived by WebCite® at http://www.webcitation.org/6Za8bPXAt)
*   Glaser, B. G. & Strauss, A. L. (1967). _The discovery of grounded theory: strategies for qualitative research._ New Brunswick, NJ: Aldine Transaction.
*   Gold, R. (2012, April 9). [Little libraries: big ideas in a small box](http://www.webcitation.org/6b0BpLzSb). _Cape Cod Times._ Hyannis, MA. Retrieved from http://www.capecodtimes.com/article/20120409/NEWS/204090327 (Archived by WebCite® at http://www.webcitation.org/6b0BpLzSb)
*   Granovetter, M.S. (1973). The strength of weak ties. _American Journal of Sociology, 78_(6), 1360–1380.
*   Greenbaum, S.D. & Greenhaum, P.E. (1985). The ecology of social networks in four urban neighborhoods. _Social Networks, 7_(1), 47-76.
*   Groves, M. (2012, January 27). [Little free library brings neighbours together through books](http://www.webcitation.org/6Za9uPN25). _Los Angeles Times._ Retrieved from http://articles.latimes.com/2012/jun/27/local/la-me-little-free-library-20120628 (Archived by WebCite® at http://www.webcitation.org/6Za9uPN25)
*   Gurstein, M. (2007). _What is community informatics (and why does it matter)?_ Milan, Italy: Polimetrica.
*   Jarvis, B. (2010, May 12). [Building the world we want: interview with Mark Lakeman.](http://www.webcitation.org/6ZaATEPJW) _Yes!_ Retrieved from http://www.yesmagazine.org/happiness/building-the-world-we-want-interview-with-mark-lakeman (Archived by WebCite® at http://www.webcitation.org/6ZaATEPJW)
*   Jones, B. (2012, February 27). Tiny libraries have people thinking (and reading) outside the box; Free book exchanges popping up on lawns promote literacy, sense of community. _USA Today_.
*   Lee, S. (2011, July 16). Community book exchange project helps bring neighbours together. _Vancouver Sun_.
*   Leyden, K. (2003). Social capital and the built environment: the importance of walkable neighbourhoods. _American Journal of Public Health, 93_(9), 1546-1551.
*   Loader, B. (2003). Community informatics and development. In K. Christensen & D. Levinson (Eds.), _Encyclopedia of community: from the village to the virtual world_. Thousand Oaks, CA: Sage Publications.
*   Mattern, S. (2012, May). [Marginalia: little libraries in the urban margins.](http://www.webcitation.org/6ZbERfLfX) _Places_. Retrieved from https://placesjournal.org/article/marginalia-little-libraries-in-the-urban-margins/ (Archived by WebCite® at http://www.webcitation.org/6ZbERfLfX)
*   Nathan, L. P. (2012). Sustainable information practice: an ethnographic investigation. _Journal of the American Society for Information Science and Technology, 63_(11), 2254–2268.
*   Newcomer, E. P. (2012, July 17). [Barely bigger than a breadbox, but teeming with literary treasures in Brooklyn](http://www.webcitation.org/6ZbEqWIRQ) . _New York Times_. Retrieved from http://www.nytimes.com/2012/07/17/nyregion/book-trading-post-opens-on-a-brooklyn-sidewalk.html (Archived by WebCite® at http://www.webcitation.org/6ZbEqWIRQ)
*   Nutefall, J. E. & Ryder, P. M. (2010). The serendipitous research process. _The Journal of Academic Librarianship, 36_(3), 228-234.
*   O'Connor, K. (2012, July 10). [Little Free Libraries build community](http://www.webcitation.org/6ZbFCrbLJ). _Houston Chronicle_. Retrieved from http://www.chron.com/default/article/O-Connor-Little-Free-Libraries-build-community-3697349.php (Archived by WebCite® at http://www.webcitation.org/6ZbFCrbLJ)
*   Peterson, M. (2012, September 30). [Tiny libraries connect neighbourhoods, one book at a time](http://www.webcitation.org/6ZbdhV3ln). _The Chronicle of Philanthropy_. Retrieved from http://philanthropy.com/article/Tiny-Libraries-Connect/134734/ (Archived by WebCite® at http://www.webcitation.org/6ZbdhV3ln)
*   Portes, A. (1998). Social capital: its origins and applications in modern sociology. _Annual Review of Sociology, 24_(1), 1-24.
*   Reckwitz, A. (2002). Toward a theory of social practices: a development in culturalist theorizing. _European Journal of Social Theory, 5_(2), 243-263.
*   Ricci, F., Wöber, K. & Zins, A. (2005). Recommendations by collaborative browsing. In Andrew J. Frew, (Ed.). _Information and Communication Technologies in Tourism 2005 - Proceedings of the International Conference in Innsbruck, Austria, 2005._ (pp. 172-182). Wiesbaden, Germany: Springer für Professionals.
*   Schaff, M. (2012, July 26). [A 'little' global movement reaches Oak Creek—and it's free](http://www.webcitation.org/6ZbeGfU9t). _Oak Creek Patch_. Retrieved from http://patch.com/wisconsin/oakcreek/a-little-global-movement-reaches-oak-creek-and-its-free (Archived by WebCite® at http://www.webcitation.org/6ZbeGfU9t)
*   Scrivener, L. (2011, December 2). [Book lovers alert! This teeny library will be open 24/7](http://www.webcitation.org/6ZbeWUjfK). _Toronto Star_. Retrieved from http://www.thestar.com/news/article/1096071 (Archived by WebCite® at http://www.webcitation.org/6ZbeWUjfK)
*   Semenza, J. C. (2003). The intersection of urban planning, art, and public health: the sunnyside piazza. _American Journal of Public Health, 93_(9), 1439-1441.
*   Semenza, J. C., March, T. L. & Bontemp, B. D. (2007). Community-initiated urban development: an ecological intervention. _Journal of Urban Health, 84_(1), 8-20.
*   Suchman, L. (2002). Practice-based design of information systems: notes from the hyperdeveloped world. _The Information Society: An International Journal, 18_(2), 139-144.
*   Vian, J. (2014, October 23). [Little free libraries branching out](http://www.webcitation.org/6ZbesDlPh). _Coulee News_. Retrieved from http://lacrossetribune.com/couleenews/lifestyles/little-free-libraries-branching-out/article_30bba546-3e94-5c13-9dc1-744213ca8447.html (Archived by WebCite® at http://www.webcitation.org/6ZbesDlPh)
*   Vogler, P. (2014, October 27). [More little libraries coming to Denham](http://www.webcitation.org/6Zbf6Z2RA). _Wicked Local Denham_. Retrieved from http://dedham.wickedlocal.com/article/20141027/NEWS/141026665 (Archived by WebCite® at http://www.webcitation.org/6Zbf6Z2RA)
*   Wildemuth, B. M. (2009). _Applications of social research methods to questions in information and library science_. Westport, CT: Libraries Unlimited.
*   Wilkerson, A., Carlson, N. E., Yen, I. H., & Michael, Y. L. (2011). Neighborhood physical features and relationships with neighbors: Does positive physical environment increase neighborliness? _Environment and Behavior, 44_(5), 595–615.