#### Information Research, Vol. 2 No. 3, December 1996

* * *

# An investigation into the impact of the Sheffield Libraries strike

#### Richard Proctor, Gill Sobczyk and [Bob Usherwood](mailto:r.usherwood@shef.ac.uk)  
Department of Information Studies  
University of Sheffield  
Sheffield, UK

## Introduction

This paper summarises the principal findings of the work undertaken to investigate the impact on user attitudes and behaviour of the temporary closure of Sheffield Libraries and Information Services in 1995\. Strike action by library staff following a breakdown in negotiations with Sheffield City Council over the Council's proposal to withdraw enhanced pay for Saturday working, resulted in an eight-week closure of 32 out of 34 library service points.

The service disruption provided an unprecedented opportunity to investigate aspects of user behaviour and attitudes not covered by previous research ([Aslib, 1995](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#asli95); [Comedia,](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#come93) 1993). Previous studies demonstrated the value placed on the public library service but only by users and members of the public who had made their own choices about whether or not to use the service. The value of our research is that it provided the opportunity to investigate the behaviour and attitudes of users who are, quite unexpectedly, deprived of their library service long enough for it to have some impact on their lives. When libraries have been forced to close temporarily through some disaster such as a fire, published literature tends to focus on 'crisis management' rather than on the impact of the closure on users ([Simpson, 1994](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#simp94); [Holderness, 1995](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#hold95)). Literature on the effect of service disruption due to strike action is equally lacking. [Myers](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#myer91) (1991) reported on the impact of strike action on staff only, [Backhouse](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#back84) (1984) only on the management of strike action. The temporary closure in Sheffield enabled researchers to examine the robustness of library use and the extent to which previous patterns of use can be resumed, to identify where the competition for library users' time lies and to make an assessment of the impact of closure on the local infrastructure. The overall objective was to help library professionals and policy makers to understand the impact service disruption or temporary closure might have on the future use of the service.

## Methodology

The research used a combination of quantitative and qualitative methods. Semi-structured interviews to identify what distinctive changes took place in the behaviour and habits of library users during and because of the strike were conducted with 518 library users returning library material following the reopening of libraries. The five service points (Central Lending Library, Broomhill, Darnall, Southey and Waterthorpe) selected for participation in the survey captured a representative sample of the library user population in Sheffield in terms of covering a range of communities and libraries with different roles within Sheffield's tiering strategy. Interviews were also conducted with information service users who made enquiries at the Central Library by telephone, with staff at academic and public libraries in the region, and with staff from a sample of book shops in Sheffield. Data recording book issues was tracked to assess the effect of the library closure on issue trends and to test the robustness of findings from the user survey. Comments and complaints submitted to the Sheffield Libraries and Information Services were reviewed to seek additional evidence of user attitudes to the stoppage. Data recording the return of books still on loan at the time of service closure was monitored to provide additional evidence of the strike's impact on future use of the library service and to permit an approximate calculation of the cost of the strike in terms of non-returned stock. Local newspapers were scanned for articles on issues related to the closure to assess the extent to which coverage either affected or reflected public attitudes towards the closure.

## Characteristics Of Users And Their Use Of Libraries

Core library opening hours are increasingly tending to correspond to office hours due to service reductions which place limits on library access for employed people. Findings from the user survey showed that 70% of the research sample were not in paid employment. In the case of one library (Southey) the figure was as high as 96%.

The user survey showed that 89% of community library users used the library for leisure purposes. It is ironic that a service that is so dependent, for its success, on leisure use should have its opening hours so heavily weighted towards 'office hours. The research raises questions about the extent to which employed people are effectively being 'disenfranchised' from public library use due to this weighting.

In two out of the four community libraries surveyed, more than half of the respondents visited the library at least once a week. For a third library, the figure was only slightly less (44%). Although 91% of respondents used their visit to borrow books, the frequency of visits cannot be attributed to the need to return them, since the loan period is three weeks. Although frequency of use was generally far higher than expected, there were significant differences between libraries which seemed to be dependent on the nature of the community. Evidence suggested that the library may be particularly significant as a community resource in communities where unemployment is high and access to other resources, including financial resources, limited.

## Changes In User Behaviour And Habits Resulting From Service Disruption

The user survey asked if anything had taken the place of library activities during the closure. The findings provide an indication of what other pursuits replaced the library and where the competition for users' attention and time comes from. 47% of respondents did not find a replacement; 44% found other means of reading or replacing specific library services, and only 9% found a replacement in the form of a non-library oriented 'leisure' activity.

The research suggests that, although libraries compete with other forms of leisure pursuit for the attention of non-library users, for regular library users there is no significant competition strong enough to persuade them to stop using the service.

The picture is similar when we look at the 44% of respondents who attempted to find replacements for the service. There were many comments about the unsatisfactory nature of alternatives, from the high cost of buying books (only 14% bought books from book shops or book clubs as an alternative) to the inconvenience of using other and less appropriate libraries. The 99.6% of respondents who said they would resume use of the service when it re-opened reinforces this conclusion. It is significant that nearly a third (31%) of all respondents said that they could find no suitable alternative at all for the missing service. The research indicated clearly that none of the alternatives to library use were satisfactory or acceptable on a long term basis.

Telephone users of reference services were more resourceful in finding replacement services than those who made enquiries in person, because it was easier to telephone alternative service points. It was easier to find replacements for business and technical related enquiries than for arts and social sciences, especially when enquiries were made by commercial organisations, as they tended to have more access to information services provided by professional institutions.

Three of the seven libraries surveyed in the region (Sheffield Hallam University, Manchester Commercial Library and Rotherham Central Library) provided anecdotal evidence of an increase in telephone reference enquiries made by Sheffield people. Sheffield Hallam University Library, situated within 10 minutes walk from the Central Lending Library experienced an increase in members of the public using the business and reference services. However there was no indication that any increase in use of other libraries would be sustained following resumption of Sheffield's services.

## How Important Are Libraries To Library Users?

The survey asked for which purposes users missed the library most. 79% said they had missed it for at least one purpose (78% missed the library for leisure purposes most, 58% for educational purposes). Fewer people than expected missed the library as an educational provider, possibly because the closure coincided with the end of the academic year. It was significant that educational purposes were missed most in those communities where people might have been expected to have less access to other sources of educational materials.

At Darnall, where only 1% of respondents had undertaken some form of higher education, it appeared that the Training and Education Council funded Open for Learning scheme operating there had contributed significantly to the high proportion of people using the library specifically for educational purposes. The evidence suggests that there may be a high potential for extended educational provision through the library service in communities with limited access to higher and further education.

The researchers had expected that most people would have missed the library for a specific reason related to lack of access to books. However, an unexpectedly high proportion of respondents said that they had missed the library for a reason related to its social value or because it had become an indispensable part of their lives. Well over half the respondents in three out of four of the community libraries surveyed said that they had missed the library for this reason. Clear evidence was provided of the very high value placed on the use of the library as a social resource, particularly in communities with a higher than average incidence of social and economic deprivation.

The contribution that the library can make to the quality of people's lives can also be judged from many of the responses to questions asking which specific library services users had missed the most, and whether they had succeeded in replacing them with an alternative during the strike. The research indicated that if people are deprived of a library, a high proportion will find reading material/information elsewhere or simply stop reading until the library reopens.

The research demonstrated the extent to which reading is an essential and critical factor in the lives of library users. For the majority (91%), it was not replaceable by any other activity.

## The Long-Term Impact Of The Strike On The Service

A reassuring finding of the research is that only two of the 518 people surveyed said they would not be using the library again. The analysis of book issue trends since the strike appears to confirm that the extended closure has not broken the library 'habit' of users.

The research suggests that normal borrowing and use patterns have been resumed because library services are not replaceable. Libraries are too important in users' lives for them, willingly, to transfer to another activity.

Users surveyed after the strike commented that the closure had brought home to them how much they depended on the service. Indeed, more people commented that they would use the service more now the service was back, than indicated they would stop using it.

## How Important Are Libraries To The Local Infrastructure?

Research on the economic impact of libraries has tended to concentrate on their impact on town centres ([Greenhalgh & Worpole, 1995](http://www.shef.ac.uk/uni/academic/I-M/is/publications/infres/paper18.html#gree95)). However, our findings suggested that for many people library use is a key factor in determining the frequency of their visits to all local centres, urban and suburban. Nearly a quarter (23%) of all respondents visited their local centre less often because of the library closure, indicating that the library's presence in a local community may have a significant impact on local retailers and other businesses. The Independent Bookshop, situated within 150 metres of the Central Lending Library experienced a loss of business during the closure as a result of the loss of passing trade from people who visit the book shop en route to or from the library.

## Conclusions

Conclusions reached were that the temporary closure had not broken the robustness of public library use because library services are not replaceable for the majority of users. The research showed that for the majority of users the public library is a service of inestimable value, enhancing quality of life, and, for many people, fulfilling an essential need that no other pursuit or activity satisfies. It also raises questions about the sort of benefits that people get from the public library service and how this determines the frequency and pattern of their library use. Particular questions in urgent need of further research are:

*   What has the impact of library closures over the last 5-10 years been on the lives of people and in particular on different groups in the community?
*   To what extent have recent changes in opening hours affected the characteristics of the people who use public libraries?
*   What is the most effective range and pattern of opening hours which will secure accessibility for the widest range of socio-economic groups?
*   What determines the value placed on the public library service and the frequency of its use?
*   What factors make a local library service indispensable rather than highly desirable?
*   What is the potential for the enhancement of educational support for independent learners from local libraries?

## References

*   <a name="asli95">Aslib.</a> (1995) Review of the Public Library Service in England and Wales. Final Report. London: Aslib.
*   <a name="back84">Backhouse, R.</a> (1984) Striking lessons. Library Association Record, 86 (8), 294.
*   <a name="gree95">Greenhalgh, L. & Worpole, K.</a> (1995) Libraries in a World of Cultural Change. London: U.C.L. Press.
*   <a name="come93">Comedia.</a> (1993) Borrowed Time? The Future of Public Libraries in the UK. Bournes Green: Comedia.
*   <a name="hold95">Holderness, M.</a> (1995) Back from the brink of disaster. Library Manager, 3, 16-17.
*   <a name="myer91">Myers, T.</a> (1991) There are no winners in a library strike. American Libraries, 22 (2), 170.
*   <a name="simp94">Simpson, E.</a> (1994) The Norwich Central Library fire. Paper Conservation News, 72, December, 10.
