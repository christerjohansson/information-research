17-4 notes

vol. 17 no. 4, December, 2012

paper534.html

Conceptualizing information need in context</h1>
<h4 id="reijo-savolainen"><a href="#author">Reijo Savolainen</a></h4>
<p>School of Information Sciences, University of Tampere, Tampere, Finland</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This paper examines the contextual features of information need. An attempt is made to demonstrate that information need can be conceptualized differently, depending on the context in which it appears.<br>
<strong>Method.</strong> Concept analysis of about

paper535.html

TO DO 

paper536.html

Analysing the effects of individual characteristics and self-efficacy on users' preferences for system features in relevance judgment</h1>
<h4 id="yin-leng-theng-and-sei-ching-joanna-sin"><a href="#authors">Yin-Leng Theng</a> and <a href="#authors">Sei-Ching Joanna Sin</a></h4>
<p>Division of Information Studies, Wee Kim Wee School of Communication and Information, Nanyang Technological University. 31 Nanyang Link, Singapore 637718</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> The design of information systems has traditionally focused on algorithmic relevance. While scholars have called for a user-centred perspective on relevance, less is known about what system features are important in supporting subjective relevance judgment of different individuals.<br>
<strong>Method.</strong> Drawing from Kuhlthau's information

paper537.html

The practical management of information in a task management meeting: taking 'practice' seriously</h1>
<h4 id="shinichiro-sakai"><a href="#authors">Shinichiro Sakai</a></h4>
<p>Kyoritsu Women's University, Tokyo, Japan<br>
<a href="#authors">Norihisa Awamura</a> and <a href="#authors">Nozomi Ikeya</a><br>
Keio University, Tokyo, Japan</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> Although researchers have turned their analytical focus to 'practices' in naturally occurring settings, reconsidering the ways in which we approach the information activities undertaken by practitioners is still needed. We argue that we must be able to capture the practical concern that participants experience rather than replace it with theoretical concern. This paper examines the concepts and language that participants (and we researchers) use.<br>
<strong>Method.</strong> We conducted fieldwork at the workplace of a group of Japanese information technology hardware engineers to understand their practices. Audio and video recordings, as well as field notes and photos, were used as part of the fieldwork.<br>
<strong>Anal

paper538.html

Managing collaborative information sharing: bridging research on information culture and collaborative information behaviour</h1>
<h4 id="gunilla-wid%C3%A9n"><a href="#authors">Gunilla Widén</a></h4>
<p>Information Studies, Åbo Akademi University, Åbo (Turku), Finland<br>
<a href="#authors">Preben Hansen</a><br>
Swedish Institute of Computer Science, SE-164 29 Kista, Sweden</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> The aim of this paper is to combine insights of collaborative information behaviour and information cultures in organizations to bridge different aspects of managing knowledge-creation and information sharing in order to highlight the importance of supporting collaborative handling within business environments.<br>
<strong>Method and Analysis.</strong> This paper is a literature review

paper539.html

Image seeking in multilingual environments: a study of the user experience</h1>
<h4 id="evgenia-vassilakaki"><a href="#authors">Evgenia Vassilakaki</a></h4>
<p>Department of Library Science &amp; Information Systems, Technological Educational Institute, Athens, Greece<br>
<a href="#authors">Frances Johnson</a> and <a href="#authors">R.J.Hartley</a><br>
Department of Languages, Information &amp; Communications, Manchester Metropolitan University, Manchester, UK</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction</strong>. There has been considerable activity in the development and testing of multilingual information retrieval systems, but little exploration of how users behave when using such systems. This paper provides a detailed understanding of how one group of users behaved when searching for images in a multilingual environment.<br>
<strong>Method.</strong> Observation, retrospective thinking aloud and interview were used to collect user data when searching on FlickLing, an experimental multilingual image retrieval system. Each of the twenty-four participants was required to conduct three searches for images which were described in a foreign language.<br>
<strong>Analy

paper540.html

Understanding information behaviour in palliative care: arguing for exploring diverse and multiple overlapping contexts</h1>
<h4 id="ina-fourie"><a href="#author">Ina Fourie</a></h4>
<p>Department of Information Science, University of Pretoria, Pretoria, South Africa</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> Palliative care is associated with life-threatening diseases and especially the last phase between life and death - end-of-life. Although noted, the emotion, trauma, despair and physical deterioration of this phase are not fully acknowledged in information behaviour and health care literature. Patients' and families' unmet information needs and frustrations remain of concern.<br>
<strong>Method.</strong> To deepen understanding of the complexity of

paper541.html

Report on a survey of readers of <em>Information Research</em></h1>
<h4 id="td-wilson"><a href="#author">T.D. Wilson</a></h4>
<p>Editor-in-chief, Information Research</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> A survey of readers of Information Research was carried out in October/November, 2012, using the SurveyMonkey online survey service.<br>
<strong>Method.</strong> A random sample of 300 'regular 

paper542.html

Coursework-related information horizons of first-generation college students</h1>
<h4 id="tien-i-tsai"><a href="#author">Tien-I Tsai</a></h4>
<p>School of Library and Information Studies, University of Wisconsin—Madison, USA</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This study examines first-generation college students' information behaviour through Sonnenwald's theoretical framework of information horizons, a mental map upon which people place the information sources they use.<br>
<strong>Method.</strong> A web survey with 450 first-generation college

paper543.html

Information access and use by legislators in the Ugandan parliament</h1>
<h4 id="ruth-nalumaga"><a href="#author">Ruth Nalumaga</a></h4>
<p>Makerere University, Makerere Hill Road, Kampala, Uganda</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This paper explores information practices at parliamentary level. The main objective is to highlight the influences of the context of legislative activities on the information behaviour of legislators. Female members of Parliament particularly considered because of their previous status as an under-represented social group in politics. Levels of access to and use of information were scrutinised across the sexes.<br>
<strong>Method.</strong> In-depth interviews were carried out 

paper544.html

Modelling historians' information-seeking behaviour with an interdisciplinary and comparative approach</h1>
<h4 id="hea-lim-rhee"><a href="#author">Hea Lim Rhee</a></h4>
<p>Department of R&amp;D System Development, NTIS Center, Korea Institute of Science and Technology Information, Daejeon, Republic of Korea.</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> The information-seeking behaviour of historians may be different from that of other user groups in libraries and archives and historical research may be unique.<br>
<strong>Method.</strong> This research combined interdisciplinary and comparative approaches.

paper545.html

Information behaviour among young women in vulnerable contexts and social inclusion: the role of social mediators.</h1>
<h4 id="martha-sabelli"><a href="mailto:marthasabelli@gmail.com">Martha Sabelli</a></h4>
<p>Escuela Universitaria de Bibliotecología y Ciencias Afines, Universidad de la República, Emilio Frugoni 1427, 11.200 Montevideo, Uruguay</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> The present paper concentrates on the study of institutional mediators (doctors, social workers, psychologists, professors, teachers, social educators) as information intermediaries.<br>
<strong>Methods.</strong> Three methodological tools were applied:

paper546.html

Relatives as supporters of elderly peoples' information behaviour</h1>
<h4 id="%C3%A1g%C3%BAsta-p%C3%A1lsd%C3%B3ttir"><a href="#author">Ágústa Pálsdóttir</a></h4>
<p>Department of Library and Information Science, School of Social Science, University of Iceland, Gimli v/Sæmundarg?tu, IS-101 Reykjavík, Iceland</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> The paper presents results from an exploratory study with the overall purpose of gaining knowledge of various aspects related to support at information behaviour that Icelandic elderly people, still living in their homes, receive from their relatives. The paper will focus on the viewpoints of the relatives and how they describe their own situation as informal information supporters of the elderly information needs.<br>
<strong>Method.</strong> A qualitative study was conducted. Open-ended

paper547.html

Task-based navigation of a taxonomy interface to a digital repository</h1>
<h4 id="christopher-sg-khoo"><a href="#authors">Christopher S.G. Khoo</a></h4>
<p>Wee Kim Wee School of Communication &amp; Information, Nanyang Technological University, Singapore<br>
<a href="#authors">Zhonghong Wang</a><br>
Cataloging Maintenance Center, Illinois Heartland Library System, USA<br>
<a href="#authors">Abdus Sattar Chaudhry</a><br>
Department of Library &amp; Information Science, College of Social Sciences, Kuwait University, Kuwait</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This is a study of hierarchical navigation; how users browse a taxonomy-based interface to an organizational repository to locate information resources. The study is part of a project to develop a taxonomy for an library and information science department to organize resources and support user browsing in a digital repository.<br>
<strong>Method.</strong> The data collection was carried out using task-based

paper548.html

Dancing around the edges: the use of postmodern approaches in information behaviour research as evident in the published proceedings of the biennial ISIC conferences, 1996—2010</h1>
<h4 id="lynne-ef-mckechnie-lucia-cedeira-serantes-and-cameron-hoffman"><a href="#authors">Lynne (E.F.) McKechnie</a>, <a href="#authors">Lucia Cedeira Serantes</a> and <a href="#authors">Cameron Hoffman</a></h4>
<p>The University of Western Ontario, London, Ontario, Canada, N6A 3K7</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This paper investigates the use of postmodern approaches in information behaviour research.<br>
<strong>Method.</strong> Content analysis of the published proceedings of the biennial ISIC conferences, 1996—20

paper549.html

An investigation of information-seeking behaviour of geography teachers for an information service intervention: the case of Lesotho
      </h1>
        <h4>
          <a href="#authors">Constance Bitso</a> and <a href="#authors">Ina Fourie</a><br>
          Department of Information Science, University of Pretoria, Pretoria, South Africa
        </h4>
      
          <h4>Abstract</h4>
          <blockquote>
            <strong>Introduction.</strong> This study investigates the information needs and information-seeking patterns of secondary level geography teachers in Lesotho to guide the design and implementation of an information service for these teachers. Leckie, Pettigrew and Sylvain's model of professionals' information-seeking served as a theoretical framework but was overlaid with other models such as that of Savolainen and Wilson.<br>
            <strong>Method.</strong> Data were collected from in-service teachers through focus group discussions, prospective teachers with a questionnaire, and key actors in Lesotho's secondary geography education context through individual interviews.<br>
            <strong>Analysis.</strong> The Statistical Package for Social Sciences was used to analyse the questionnaire responses. Content analysis was used to analyse the verbal responses as well as an open-ended question on the questionnaire. Triangulation was applied through using three sets of participants and different data collection techniques for each. It allowed comparative analysis of responses and data collection techniques used in the study.<br>
            <strong>Results.</strong> The work environment of in-service geography teachers is revealed including their work roles, associated tasks, information needs and information-seeking patterns. An information service model for Lesotho secondary level geography teachers (including possible interventions) is proposed.<br>
            <strong>Conclusions.</strong> Lesotho geography teachers need information to fulfil their work related roles and tasks that might be addressed through an information service. An implementation agency, information specialist(s) and collaborative approach to service delivery are crucial for the implementation and sustenance of the proposed information service.
          </blockquote>
       
        <h2>
          Background
        </h2>
        <p>
          Lesotho is a developing country in Southern Africa, totally enclosed by South Africa. Formal education in Lesotho starts with early childhood care and development, followed by seven years of basic primary education, then secondary education and tertiary education. Secondary education is divided into three years of junior secondary and two years of senior secondary. Learners start primary education at an age of six and write the Primary School Leaving Examination in their seventh year; this serves as an entry requirement into junior secondary education. There is Junior Certificate examination in third year of junior secondary, which determines entry into senior secondary that ends with Cambridge Overseas School Certificate examination which is a requirement for entry into tertiary education.
        </p>
        <p>
          In Lesotho, the na

paper550.html

TO BE DONE 

paper551.html

Understanding the information needs and search behaviour of mobile users</h1>
<h4 id="dima-kassab-and-xiaojun-yuan"><a href="#author">Dima Kassab</a> and <a href="#author">Xiaojun Yuan</a></h4>
<p>College of Computing and Information, University at Albany, State University of New York, Albany, NY, USA</p>
<h4 id="abstract">Abstract</h4>
<blockquote>
<p><strong>Introduction.</strong> This research is a pilot study that explores why and how users employ mobile phones or other small screen devices to access and acquire information, and to test and evaluate the adequacy of using our interview protocol as the main research instrument.<br>
<strong>Method.</strong> In this paper, we present the results

paper552.html

TO BE DONE

paper553.html

TO BE DONE

paper554.html

Comparative analysis of the development of multicultural library services in the Spanish public library network (2007-2010)</h1>
<blockquote></blockquote>
<h4 id="f-garcia-lopez-m-caridad-sebastian-and-am-morales-garcia"><a href="#authors">F. Garcia Lopez, M. Caridad Sebastian</a> and <a href="#authors">A.M. Morales Garcia</a></h4>
<p>Aguston Millares Institute for Information Management and Documentation, Carlos III University, Madrid, Spain</p>
<h2 id="introduction">Introduction</h2>
<p>Spain has evolved into a pluralistic societal space, shared by citizens of different nationalities who talk different languages and display a large variety of cultural characteristics. By taking into account descriptive data of the evolution of the foreign population in Spain, one can see that the percentage of citizens of foreign origin residing in Spain has increased from 3.8% in 2002 to 12.2% in 2010, placing it among the highest ratios in the European Union. By restricting the analysis to the years 2007 and 2010, aliens in the autonomous communities of Spain has increased in a way similar to the aforementioned development on the national level (Figure 1).</p>
<figure class="centre">![Percentage of alien populations in the Autonomous Communities of




