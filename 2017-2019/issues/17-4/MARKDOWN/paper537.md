#### vol. 17 no. 4, December, 2012

# The practical management of information in a task management meeting: taking 'practice' seriously

#### [Shinichiro Sakai](#authors)  
Kyoritsu Women's University, Tokyo, Japan  
[Norihisa Awamura](#authors) and [Nozomi Ikeya](#authors)  
Keio University, Tokyo, Japan

#### Abstract

> **Introduction.** Although researchers have turned their analytical focus to 'practices' in naturally occurring settings, reconsidering the ways in which we approach the information activities undertaken by practitioners is still needed. We argue that we must be able to capture the practical concern that participants experience rather than replace it with theoretical concern. This paper examines the concepts and language that participants (and we researchers) use.  
> **Method.** We conducted fieldwork at the workplace of a group of Japanese information technology hardware engineers to understand their practices. Audio and video recordings, as well as field notes and photos, were used as part of the fieldwork.  
> **Analysis.** Through explicating the organisation of practical management of information we attempt a detailed understanding of not only what participants do with information or what participants attribute meaning to as information but also how this information is organized in the course of social interaction.  
> **Conclusions.** Some distinctive features of practical management of information have emerged from the analysis. First, the information activities of various practical interests are mutually elaborative to one another. Secondly, these information activities are developed in a contingent and _ad hoc_ manner. Finally, they are in line with other work and everyday activities. All of these features point to the embedded nature of information activities within organizational work.

## Introduction

Research interest in practice in information behaviour has been growing since the 2000s (e.g., [Savolainen 2008](#sav08)), and scholars have generally recognised the importance of understanding information use as embedded in social practices (e.g., [Talja and Hansen 2006](#tal06)). Talja and McKenzie describe this understanding as a 'practice turn' of information behaviour research, and they criticise previous traditional studies from individualistic, cognitive and decontextualised viewpoints as considerably missing the social and collaborative aspects of information use ([Talja and McKenzie 2007](#tal07); [Veinot 2007](#vei07)). Research under the umbrella of 'practice turn' includes social constructionism-drawn studies, discourse-oriented studies (e.g., [Tuominen and Savolainen 1997](#tuo97); [Savolainen 2007](#sav07)) and collaborative information behaviour studies ([Talja and Hansen 2006](#tal06)).

However, this recognition of the necessity of practice turn has not significantly affected the actual scope of research in the field thus far. We believe reconsidering the ways in which we approach the information activities undertaken by practitioners is still needed. We argue in this paper that information activities of any kind are not independent and isolated from other work and everyday activities but are instead embedded within them. We will note in the next section that some researchers have indeed turned their analytical focus to practices in naturally occurring settings. However, we believe this embeddedness has not yet been fully explored.

We examine the practices that produce information activities through explicating the organisation of the practical management of information as participants experience it. For this purpose, we selected a meeting held by a group of Japanese information technology hardware engineers in which information is perspicuously made available and handled in interaction.

## Background

In recent studies, researchers have already turned their units of analysis to focus on social practices that produce information activities. For example, Reddy and colleagues presented ethnographic studies in a surgical intensive care unit and analysed the knowledge that practitioners used in their activities, especially in relation to information seeking. They examined work rhythms in the intensive care unit, such as the practitioners' various understandings of the temporal aspect of activities. Each medical practitioner has a personal understanding of various rhythms, which is called the _temporal sphere_ and each practitioner refers to this temporal sphere as a guide when seeking information ([Reddy and Dourish 2002](#red02); [Reddy _et al._ 2006](#red06)). In another study, Foster ([2009](#fos09)) focused on the content of conversations that took place during a lecture on information management and strategy. He developed a set of codes for conversational actions that he drew from existing discourse theory and used this set of codes to analyse the conversations. Basing from the use of the forms of talk in his data, he formulated a hypothesis on talk in terms of its educational value. As a third example, McKenzie ([2006](#mck06), [2009](#mck09)) studied the conversation between a female midwife and a patient and examined _informed choice_ as an instance of information practice. Based on the analysis, models of informed choice discussions were created, and these models were illustrated using relevant excerpts from McKenzie's data. In terms of document use, she created several maps that explained who used each document and how.

In our view, what remains missing in these studies is an approach that seriously considers the practitioners' points of view. If we hope to capture the aspects of social practitioners' practices and how information activities are embedded in their practices, digging into indigenous methods that the participants use and describing these methods from within are necessary. In other words, we must be able to capture the practical concern that participants experience rather than replace it with theoretical concern. Researchers are yet narrowing their analytical foci by making theoretical decisions. For instance, Reddy _et al._ ([2006](#red06)) focused exclusively on information seeking among other types of information-related behaviour embedded in the various activities that constitute practitioners' medical practice. Furthermore, Reddy _et al._ described much what participants did with information in their work, but an elucidation of how the participants treated this particular item as information for the practical purposes of performing their tasks remains limited. In McKenzie's analysis, the phenomenon or practice under study was segmented into various aspects, such as informed choice, structural aspect, and procedural aspect of interaction. This segmentation was based on her analytical framework, which she theoretically developed independently of the phenomenon. The situated practice, the phenomenon under study, in Foster's analysis was similarly understood in terms of the codes that he theoretically constructed but not in terms of the participants' point of view.

We believe that as long as researchers conduct analysis according to a theoretically derived framework, describing how actual activities are organised as the participants experience them is difficult ([Ikeya _et al._ 2010](#ike10)). This argument suggests that we should take an alternative approach concerning mundane understandings as the topic of research in its own right. To do so, we need to put our theoretical concern aside for the purpose of understanding the phenomenon under study and explicate the concepts and language that the participants (and we researchers) use. These concepts and language include common sense understandings of an ordinary conception of information, which is often taken for granted when researchers identify social phenomena as information activities.

In this respect, we believe that taking an ethnomethodological approach is an alternative method that leaves phenomena available for analysis without any theoretically derived frameworks that can limit or predetermine which phenomena should be examined. Before we consider our empirical data, taking into account the set of public criteria of the concept of information in ordinary language is worthwhile ([Ikeya 2001](#ike01)). Ikeya ([2001](#ike01)) attempted to explicate this ordinary conception based on logico-grammatical analysis, which was initially developed by language philosophers (e.g., Gilbert Ryle and Ludwig Wittgenstein) and later by ethnomethodologist Jeff Coulter ([1979](#cou79)). This set can be publicly found in our everyday practice in which people, whether practitioners or researchers, practically manage information in the course of situated activities. By _publicly found_ we mean they are inter-subjective and are therefore not cognitive processes found somewhere _inside_ an individual.

When we use the concept of information in everyday life, which includes scientific work as well, we are often referring to some kind of outcome or achievement, such as understanding. The related word, to inform, is an achievement verb, which is similar to _understand_ ([Ryle 1949](#ryl49)). Therefore, I informed my colleagues of a schedule change means that one has let the other party know about the change so that s/he can use such knowledge while doing his/her activities. Similarly, when we say we provided information, we found information or we received information, we are not conducting any specific action but are rather describing that something was achieved. Similarly, we can outline a set of public criteria for the practical use of information as follows ([Ikeya 2001](#ike01)):

1.  Factual: the item can be assumed to be either _factual_ or contains some facts.
2.  True: the item can be assumed to be _true_. This criterion is related to the criterion of factuality.
3.  Not known: the item can be assumed to be unknown to the other who is to be informed at this point.
4.  Something the other person wants to know: the other person can be assumed to want to know the item.
5.  Deliverable or accessible: the item can be assumed by the participants to be conveyable to the other party or accessible to him/her. However, this does not suggest that the item may be accessible to anybody, and certain items may allow only limited access.
6.  Situational: information is _situational_, which means it is an achievement word embedded in each situation, and this word is not used for the essential attributes of the item.

When one says she found some information for us, one is describing a situation in which she found some new knowledge item that she considered _not known_ (i.e., new) to us at the time she found it, something _true_, _factual_ and something she thought we would want to know at the time she found it.

These criteria are not to be treated as theoretical concepts but as an explication of commonsense understandings of an ordinary conception of information and more can certainly be listed ([Ikeya 2001](#ike01)). Ikeya ([1997](#ike97), [2001](#ike01)) presented these criteria as part of her attempt to examine people's practices in terms of how participants practically manage knowledge as part of their activities through understanding situated practical reasoning. In situated actions, _knowledge_ or what participants know or treat as known is sometimes treated as _information_. Therefore, the practical management of information is part of the _practical management of knowledge_. In terms of examining our empirical data, public criteria can serve as a good reminder whenever we analytically determine something as an information-related phenomenon as perceived by participants. By doing so, we not only remain close to the participants' understandings of information, but we can also analyse what exactly participants identify as information (and what not).

## Research methods

We studied the interactions of a group of Japanese hardware engineers by conducting fieldwork at their workplace. Audio and video recordings, as well as field notes and photos, were used as part of the fieldwork (see also [Ikeya _et al._ 2010](#ike10)). In an attempt to examine the practical management of information as part of activities, ethnomethodological analysis was conducted as described below.

Ethnomethodology ([Garfinkel 1967](#gar67)) is located within the much-debated trend of an empirical turn to practice. Whereas ethnomethodology originally emerged within sociology, its attitude is distinctive and incommensurably alternative ([Garfinkel and Wieder 1992](#gar92)). It has also extensively influenced beyond the domain of sociology ([Lynch and Sharrock 2010](#lyn10)), including studies on information use ([Ikeya _et al._ 2010](#ike10)). The notion of _practice_ in ethnomethodology is far from the dualistic construct of structure and agency, or the macro vs. micro dichotomy, which is often found in orthodox theoretical accounts. Practice in ethnomethodology is viewed as having a reflexive relation to activity. Practice is not only brought together through the activities of a competent member, but it is also a resource for members to bring their activities together ([Llewellyn and Spence 2009](#lle09)). Viewed in this way, the topic of ethnomethodological inquiry is members' ordered orientation to a situated practice that is accountable, i.e., describable and analysable, as members' phenomena. Ethnomethodological inquiry is available for analysis, and it is also available to members themselves, although analysts are not always as competent as members are. In this study, competency connotes understanding the ways in which practitioners organise their work in their organisational context, including the technical or professional terminologies the engineers used. We conducted ethnography to learn and thus be able to document what the engineers were actually doing, seeing and achieving over the course of their daily work.

## Analysis

### The setting: task management in the morning meeting

The analysis presented in this paper is drawn from a six-month fieldwork experience with a group of engineers in a hardware development team. As we conducted our fieldwork, we became interested in the ways in which the engineers conducted their work by managing their tasks on the daily basis. By task, we mean the piece of work that one is assigned to with responsibility. Because of the role of the engineers' group in the development of hardware (their assignment was to finalise the overall hardware design of a machine, including all of the details of its parts, and prepare it for the manufacturing phase), the engineers routinely faced a variety of simultaneous tasks. Any defect in a small part would cause them to check immediately with different organisations, including the machine's manufacturing plant, the quality assurance department, and the plant that produced the piece; any delay in manufacturing was to be avoided. Therefore, task management, which involved understanding of both potential and existing tasks in the group such as assessing the amount of work required, the timeframe, and the resource allocation, was regarded as an important part of the engineers' work ([Ikeya _et al._ 2010](#ike10)).

The engineers were not only designing new machines, but they also had to respond to requests and reports from a number of individuals and organisations, such as planners, quality assurance, factories that assemble parts and the end-user service department, to name a few. Furthermore, preparing responses and requests often involved coordinating with multiple organisations. Therefore, the group leader (a male employee) of the team usually struggled with his team's task management, especially in terms of information sharing.

At the time we conducted the fieldwork that we describe in this paper, the group was in the middle of implementing a new style of task management meeting. In this approach, the group leader walked up to every single engineer in his team and checked individually the tasks they had, the delivery dates, and how well (or not well) the member engineers were dealing with them. They had been doing this every morning so that the team would have a chance to organise its work and collaborate when necessary at the beginning of the day. The group called these meetings _morning meetings_. Except for these mornings, the engineers had very little chance to talk to one another during the day.

Morning meetings seemed a good setting for us to explore the practical management of information embedded in the engineers' work. Group members were typically engaged in a variety of information activities (e.g., sharing, seeking, asking for, finding, visualising and accumulating information) in their attempt to manage their tasks with other group members.

### Practical management of information in morning meetings

The engineers found morning meetings to be opportune avenues to discuss the problems they encountered. Problem finding and solving are significant elements of systematic development and productivity. We offer an analytical description of a conversation between the group leader and his subordinate engineer, Fujimoto (a female employee with several years of experience, henceforth). In this interaction, Fujimoto shares a bad news with the group leader.

In the following example, the participants attempt to develop mutual understanding of a problem through sharing and seeking information from each other, as well as from other sources, and finally agree to do something about it. We will describe how this incident gradually unfolds in detail, following several recognisable phases of conversation.

Prior to the conversation below, the group leader spent several minutes listening to how Fuimoto planned to spend her day, such as what her tasks were, when she planned to do them and whether she needed help. When the group leader was about to move on to another engineer, Fujimoto stops him (line 1) and opens the extract of a conversation presented below. Names were withheld to maintain privacy and confidentiality.

#### Sharing a problem with the group leader and asking for help.

> Extract 1: Morning of September 30.  
> GL: Group Leader, FU: Fujimoto  
> 1    FU: Well, hang on a minute. Excuse me.  
> 2    GL: Yes.  
> 3    FU: So? Um, when I'm done with today, the rest is apparatus B, and  
> 4            there arose a quite big problem. You know they have just  
> 5            assessed the cable for the apparatus A.  
> 6    GL: Yes, yes.  
> 7    FU: It had a defect.  
> 8    GL: The thing itself?  
> 9    FU: Yeah.  
> 10  GL: The thing itself had a defect.  
> 11  FU: What are we going to do?  
> 12  (0.8 seconds of silence)  
> 13  FU: Seems like the schedule...  
> 14  GL: The one they said they are getting a result today?  
> 15  FU: Yes.

Fujimoto starts by telling the group leader that '_quite a big problem_' has been found on Apparatus A, and it is on the cable that has undergone quality assurance test (lines 3 to 5). The group leader's response, '_Yes, yes_' (line 6), displays two actions: first, it shows that he also knows that the cable has undergone the test, and second, it requests the continuation of Fujimoto's talk and thus shows that he is unaware of or does not yet have much knowledge about this '_quite a big problem_' that Fujimoto is about to describe. In other words, Fujimoto gives a premonitory utterance that reveals she has something to inform the group leader about the possible presence of trouble on Apparatus A. The problem is '_big_' and worth being reported no later than now. The group leader in turn finds it new and worth listening to now.

Hearing the group leader's response, Fujimoto proceeds and announces the trouble. The cable has been evaluated as having '_a defect_' (line 7) which, in this context, means the cable did not pass the test and that they probably cannot use the cable as they had planned.

Hearing this announcement, the group leader first attempts to confirm his understanding by clarifying whether the problem is physical (line 8). In response, Fujimoto replies '_Yeah_' (line 9). The group leader then closes the sequence with a formulation of his understanding when he says, '_The thing itself has a defect_' (line 10). He has at least been informed about the presence of a problem, and he has some sense of its nature. Therefore, Fujimoto and the group leader come to share mutual information that the '_defect_' Fujimoto is reporting is the cable itself and nothing other than that, such as, say, dust.

Now that Fujimoto has announced the problem, she goes to ask the group leader, '_What are we going to do?_' (line 11), which is understandable as a request to the group leader to think together about the problem she is reporting. Furthermore, the mere fact that she raised this question to the group leader indicates the distinguishable nature of the problem as a major concern. Troubles of various kinds occur daily, and handling them is part of her everyday work. Fujimoto does not usually ask the group leader for a solution to every single problem she faces. Insofar as she knows how or is able to find a way to handle it, it is a normal problem to her. Thus, the fact that Fujimoto explicitly asks the group leader to brainstorm with her clearly implies that she has determined that the cable problem is a major concern, which she finds insurmountable without his help. Her request, however, remains unanswered (line 12). The group leader's silence, however, is not treated by Fujimoto as a declination because she continues her talk instead of responding directly to his silence. In fact, Fujimoto has only reached the beginning of her announcement that the cable has a defect, so he is expectedly not able to give a quick response to her request. As we can see below, the group leader later works to establish details in order to prepare his answer to her request, as we see in their further elaborations in Extract 3.

Therefore, the meeting between Fujimoto and the group leader, which was about to be finished, continued. Fujimoto has informed the group leader about the presence of a problem and has made a request to him to think with her about it. From the group leader's viewpoint, this event can be seen as 'information encountering' because he apparently did not expect the information.

#### Fujimoto now makes a further detailed exposition of the problem and asks the group leader to think with her again.

> Extract 2 (continued)  
> 15  FU: Yes. And he told me locally the progress yesterday that some pins  
> 16          are open with extremely high resistance. He hasn't analysed  
> 17          it yet, its cause is unknown, but the quality lab man says he has a feeling  
> 18          it's not the jig.  
> 19  GL: Wooow.

In Extract 2, Fujimoto continues telling the group leader what she knows about the trouble (lines 15 to 18). Notice that she is reporting in the form of indirect speech, i.e., reporting somebody's report of the problem. With the introductory component, '_he told me_', she is explicitly attributing the source of her report to a third party. The result was told '_locally_', which means the presence of the trouble has been shared only informally with a limited number of people and has not come to anybody else's attention. Furthermore, the status of the trouble is as of '_yesterday_.' The test is still in progress, so her report is tentative at this point in time. A closer examination of this problem, she indirectly reports, indicates that the problem lies on '_some pins_' that are '_open_' with '_extremely high resistance_' value (_open_ is a technical term for being shorted out). The cause of the problem remains unknown because it has not been analysed, although the quality lab man speculates that it is not in the jig.

The '_quality lab man_' (line 17), whom they later identify as Koyasu, informed Fujimoto about the problem. The authenticity and thus the credibility of the exposition at this moment are nevertheless unquestioned. Instead of asking further questions, the group leader responds with a confirmation that he has understood the problem and its complex nature. Her report is treated as credible by the group leader because both parties have confirmed it based on Koyasu's report (which Fujimoto received over the phone as we see in Extract 4).

#### Finding out how bad the situation is.

> Extract 3 (continued)  
> 20  FU: What are we going to do? I've had the blues since last night.  
> 21  GL: Uh...um... XXX (manufacturer) can't make it anymore?  
> 22  (2.8 seconds of silence)  
> 23  GL: Um... the previous factory can't make it anymore?  
> 24  FU: Can't.  
> 25  GL: Facilities are gone.  
> 26  FU: They have taken everything away.  
> 27  (7 seconds of silence)  
> 28  GL: Does the stock make it to November?  
> 29  FU: The end of October.  
> 30  GL: The end of October. Only a month left.

After informing the group leader of the details of the problem, Fujimoto revisits the question (line 20), as she did in Extract 1, line 11: she makes a request for them to think together. This time, she adds that this problem has been bothering her since the night before, which highlights her concern. The group leader's subsequent response (line 21) is again not recognisable as an answer to her request (line 20), although he now shifts from confirming what FU is talking about to establishing some conditions for a possible measure to address the manufacturing defect, not the emotional distress Fujimoto has been feeling (which could well be a potential trouble had the conversation been stated differently). The group leader asks about the manufacturability of the cable, first by referring to the company (line 21). While these are grammatically constructed as requiring more information about the problem, the group leader is pragmatically also seeking other options. However, a moment of silence ensues (line 22). Hearing Fujimoto's silence, the group leader changes his question slightly in line 23, '_the previous factory can't make it anymore?_', by replacing the manufacturer's name '_XXX_' with '_the previous factory_'. To this, Fujimoto answers '_Can't_', which implies that she found the group leader's first question in line 21 difficult to comprehend. The group leader formulates his understanding (line 25) of the reason the cable cannot be manufactured in the previous factory, and Fujimoto supports this (line 26). No more facilities for cable manufacturing exist at the previous factory. Subsequently, a long pause ensues (line 27). The group leader's new question about how long the stock is going to last (line 28) infers that the participants could not find a viable option here, as discussed in the next paragraph.

The upshot of what has unfolded so far in their conversation is that the cable is no good, and no alternative manufacturer can be found. This condition might result in a delay, or even worse, suspension of shipping, which would consequently result in stopping the manufacturing line. We have learned in our fieldwork that this was certainly the last thing they would want to happen, and their manager had repeatedly told the team that such scenario should be avoided at all costs. Therefore, their exchange regarding the insubstantial availability of a stock of cables indicates their sense of tension toward the deadline. The group leader goes on to inquire about the availability of the stock by testing his own estimation, which can be understood as another information seeking activity (line 28). However, the situation seems tighter than the groupleader initially estimated. The stock runs out at the end of October (line 29), '_only_' a month from now (line 30).

#### Seeking a way out.

> Extract 4 (continued)  
> 31  FU: So um... and so I'm checking once again, are they making it in a real  
> 32          line? If they're making it in a real line and if it has a defect, it is  
> 33          beyond saving.  
> 34  GL: Can't they take a way to produce in bulk and screen it out?  
> 35  FU: Well, it depends on the result.  
> 36  GL: Uh-huh.  
> 37  FU: If there's a dust or something attached to it in the first place and or  
> 38          that's causing ill, that's somewhat doable but if pin implant is loose  
> 39          or that kind of sort, it's hopeless.  
> 40  GL: Are you talking about the connector itself?  
> 41  FU: I can't tell. Connector... uh right now, I'm inquiring about it at the  
> 42          assembly.  
> 43  GL: Uh-huh.  
> 44  FU: Koyasu is out on business trip since yesterday and gave me  
> 45          a call yesterday and said I've got this information. And he says  
> 46          he'll be back tomorrow and start analysing.  
> 47  GL: OK.  
> 48  (2 seconds of silence)  
> 49  FU: That's the bottom line.

Fujimoto and the group leader are facing a major hassle with the cable, but they are still uncertain on how bad the cable is. FU and the GL are now aware that the cable has '_quite a big problem_'; however, Koyasu was the source of much of Fujimoto's report (Extract 2). Therefore, Fujimoto can only tell so much information to the group leader. Extract 4 (above) exhibits an intriguing exchange on this matter.

First, notice that Fujimoto begins to speak with an _if_ clause here (lines 32). Speaking in a hypothetical manner, Fujimoto talks about a reply she expects to an inquiry she made to the manufacturer. She presents two scenarios. If the cable in question was manufactured as a sample, that is, not in the real (assembly) line, ameliorating the problem is possible. However, if the cable was produced in the actual assembly line for commercial production, nothing can be done about it (lines 31 to 33). We learned in our fieldwork that this condition, although still presented as a hypothesis, might result in the worst-case scenario. This scenario also means that they might have to change their production plan, which would inevitably affect their work to a significant extent. Considering that Fujimoto and the group leader are in a position of having to respond to requests and reports from a number of individuals and organisations within their company, this change would certainly require a considerable amount of work. Furthermore, Fujimoto herself has already made an inquiry to the manufacturer by seeking information that she would need to prepare to solve the problem once the problem is confirmed ('_I'm checking once again_'). Fujimoto is informing the group leader that she herself has already taken action instead of just waiting for information from Koyasu.

Hearing this, the group leader asks a question, which is understandable again as a candidate solution to this problem (lines 34). To his question, Fujimoto does not respond with '_yes_' or '_no_', but says, '_Well it depends on the result_' (line 35) because the answer would depend on the result of Koyasu's analysis, which is yet to come. The source of her speech in lines 37 to 39 is uncertain, but notice that she is again speaking in a hypothetical manner, with an if-clause. The group leader (line 40) seems to have heard this as specifying a factor of the problem with the cable; however, Fujimoto answers that she '_cannot tell_'. She begins to utter '_connector_' but sharply cuts herself off in the middle of the word and gives the reason that she cannot tell because she is waiting for an answer from '_the assembly_' (lines 41 to 42). She is waiting for an answer, so the best she can offer the group leader at this moment is a hypothetical speculation.

She then continues, '_Koyasu is out on business trip yesterday and today and gave me a call yesterday and said I've got this information. He says he'll be back tomorrow and start analysing_' (lines 44 to 46). She now clarifies that the source of the information she has shared so far with the group leader is mostly from Koyasu (the '_quality lab man_' in line 17, Extract 2). Her clarification means the information is still second-hand and needs confirmation because Koyasu is out on a business trip, and the information cannot be easily updated at present.

The group leader now acknowledges the state and extent of the information they can share (line 47) and asks no further questions. After a moment of silence, FU makes an explicit formulation that she has told him all she knows and all she has been informed of at this point in time (line 49). Their conversation draws to a close.

#### Agreeing to think together again later.

> Extract 5 (continued)  
> 49  FU: That's the bottom line. What are we going to do?  
> 50  GL: Yes, let's deal with it a little later.  
> 51  FU: Yes. It's no good to deal with it here. That's it.  
> 52  GL: OK.  
> 53  FU: OK.

Having explicitly stated that she has given all information she can give at this point, Fujimoto requests that the group leader think together with her a third time. The group leader's reply, however, does not appear to be a direct solution to the problem (if we are to take her request as literally asking for a solution). It is a proposal to discuss it at a later time (line 50). He is not obviously giving any concrete measure to address the problem, but both parties are aware that finding a solution and deciding on what to do at that moment is not possible because they lack information, and additional information is expected to come. A solution the group leader provides her is a confirmation that they will think about the problem together, which turns out to be what she has been seeking throughout this entire conversation. Therefore, she agrees with him and further acknowledges the fact that they should not discuss the problem any further at that point in time (without further information) (line 51).

## Conclusion

In our attempt to demonstrate ways to examine information activities in action, we selected a meeting of a group of information technology hardware engineers in which information was clearly made available and handled in interaction. We decided to take an ethnomethodological approach to examine the organisation of situated practices in which the practical management of information can also be examined with its embedded nature. With this in mind, we attempted to stay close to the participants' understandings of the situation with mundane understandings of the ordinary conception of information as a reference.

As a result, some distinctive features of practical management of information emerged. First, the practical management of information such as seeking, providing and sharing information is found to be mutually elaborative. For example, the group leader's act of seeking information was followed by Fujimoto's attempt to share information with the group leader. These actions are interactionally and sequentially developed as their conversation unfolded.

Secondly, the practical management of information is contingently organised in an ad hoc manner as their talk unfolds rather than in a linear and planned manner. This approach contrasts with the traditional models offered in information behaviour research in which information behaviour of various kinds is portrayed as organising a linear process. Therefore, Fujimoto had to explain in so many words, for instance, in response to the questions asked by the group leader until the group leader finally understood the nature and status of the problem, which was about to emerge as real. In fact, she had to ask him for help three different times before he finally agreed. None of these were planned by both parties in advance, but these rather emerged through the interaction.

Finally, the practical management of information is made significant and relevant as part of, and in line with, the 'temporal rhythm' ([Reddy and; Dourish 2002](#red02)) of their work. Fujimoto was able to inform the group leader of the bad news just before the session with Fujimoto was about to be completed and just before the group leader was to move to talk to another person in the morning meeting. Moreover, informing the group leader of the news was located at the beginning of Fujimoto's work of the day, i.e., in the morning. Informing the group leader as soon as possible was important for her because the group leader usually leaves his desk to attend various kinds of meetings or even goes out, which is normal. Informing the group leader in the morning and making him ready to solve the problem with her was therefore important for Fujimoto, even if doing so was a little too early because she did not have enough information to act on the problem.

All of the above features point to the embedded nature of information activities within organisational work. These features were made possible by analysing the practices that produce information activities. Note that we attempted a detailed understanding of not only what participants do with information or what participants attribute meaning to as information but also how this information is organised in the course of social interaction. Insofar as we are concerned with practice, this cannot be overlooked.

## About the authors

**Shinichiro Sakai** is an adjunct lecturer in the Faculty of Arts and Letters, Kyoritsu Women's Unicersity, Tokyo, Japan. He can be contacted at: [ssakai@kyoritsu-wu.ac.jp](mailto:ssakai@kyoritsu-wu.ac.jp)  
**Norihisa Awamura** is an adjunct lecturer in the School of Library and Information Science, Keio University, Tokyo, Japan. He can be contacted at: [norihisa.awamura@gmail.com](mailto:norihisa.awamura@gmail.com)  
**Nozomi Ikeya** is a professor in the School of Library and Information Science, Keio University, Tokyo, Japan. She received her PhD from the University of Manchester. She can be contacted at: [nozomi.ikeya@a8.keio.jp](mailto:nozomi.ikeya@a8.keio.jp)

#### References

*   Coulter, J. (1979). _The social construction of mind: studies in ethnomethodology and linguistic philosophy._ Totowa, NJ: Rowman and Littlefield.
*   Foster, J. (2009). Understanding interaction in information seeking and use as a discourse: a dialogic approach. _The Journal of Documentation,_ **65**(1), 83-105.
*   Garfinkel, H. (1967). _Studies in ethnomethodology._ Englewood Cliffs, NJ: Prentice-Hall.
*   Garfinkel, H. & Wieder, D. L. (1992). Two incommensurable, asymmetrically alternate technologies of social analysis. In G. Watson & R. M. Seiler (Eds.), _Text in context: contributions to ethnomethodology_ (pp. 175-206). Newbury, CA: Sage Publications.
*   Ikeya, N. (1997). _The practical management of the social stock of knowledge: the case of an information giving service_. Unpublished doctoral dissertation, University of Manchester, Manchester, United Kingdom.
*   Ikeya, N. (2001). Seikatsusekai to jouhou [Life-world and information]. In S. Tamura (Ed.), _Jouhoutansaku to jouhouriyou_ [Information seeking and information use], (pp. 41-90). Tokyo: Keisoshobou.
*   Ikeya, N., Awamura, N. & Sakai, S. (2010). Why do we need to share information?: Analysis of a collaborative task management. In J. Foster (Eds.), _Collaborative information behavior: user engagement and communication sharing_ (pp. 89-108). Hershey, PA: Information Science Reference.
*   Llewellyn, N. & Spence, L. (2009). Practice as a members' phenomenon. _Organization Studies_, **30**(12), 1419-1439.
*   Lynch, M. & Sharrock, W. W. (2010). _Ethnomethodology_, 4 volumes. London: Sage Publications.
*   McKenzie, P. J. (2006). Mapping textually mediated information practice in clinical midwifery care. In A. Spink & C. Cole (Eds.), _New directions in human information behavior_ (pp. 73-92). Dordrecht, The Netherlands: Springer.
*   McKenzie, P. J. (2009). Informing choice: the organization of institutional interaction in clinical midwifery care. _Library & Information Science Research_, **31**(3), 163-173.
*   Reddy, M. & Dourish, P. (2002). [A finger on the pulse: temporal rhythms and information seeking in medical work.](http://www.webcitation.org/6Bp5yENF3) _Proceedings from CSCW '02: 2002 ACM Conference on Computer Supported Cooperative Work._ (pp. 344 - 353) New York, NY: ACM Press. Retrieved 30 October, 2012 from http://cleo.ics.uci.edu/teaching/Winter10/231/readings/6b-ReddyFingerOnPulse.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6Bp5yENF3)
*   Reddy, M., Dourish, P. & Pratt, W. (2006). [Temporality in medical work: time also matters](http://www.webcitation.org/6Bp6EhnMo). _Computer Supported Cooperative Work,_ **15**(1), 29-53\. Retrieved 30 October, 2012 from https://dourish.com/~dourishc/publications/2006/jcscw-temporality.pdf (Archived by WebCite<sup>®</sup> at http://www.webcitation.org/6Bp6EhnMo)
*   Ryle, G. (1949). _The concept of mind._ New York, NY: Barnes & Noble.
*   Savolainen, R. (2007). Information behavior and information practice. _Library Quarterly,_ **77**(2), 109-132.
*   Savolainen, R. (2008). _Everyday information practices: a social phenomenological perspective._ Lanham: Scarecrow Press.
*   Talja, S. & Hansen, P. (2006). Information sharing. In A. Spink & C. Cole (Eds.), _New directions in human information behavior_ (pp. 97-108). Dordrecht, The Netherlands: Springer.
*   Talja, S. & McKenzie, P. (2007). Editors' introduction: special issue on discursive approaches to information seeking in context. _Library Quarterly,_ **77**(2), 97-108.
*   Tuominen, K. & Savolainen, R. (1997). Social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen, & B. Dervin (Eds.), _Information seeking in context: proceedings of an International Conference on Research in Information Needs, Seeking and Use in Different Contexts_ (pp. 81-96). London: Taylor Graham.
*   Veinot, T. C. (2007). "The eyes of the power company": workplace information practices of a vault inspector. _Library Quarterly,_ **77**(2), 157-179.