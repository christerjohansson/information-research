#### vol. 17 no. 4, December, 2012

# An investigation of information-seeking behaviour of geography teachers for an information service intervention: the case of Lesotho

#### [Constance Bitso](#authors) and [Ina Fourie](#authors)  
Department of Information Science, University of Pretoria, Pretoria, South Africa

#### Abstract

> **Introduction.** This study investigates the information needs and information-seeking patterns of secondary level geography teachers in Lesotho to guide the design and implementation of an information service for these teachers. Leckie, Pettigrew and Sylvain's model of professionals' information-seeking served as a theoretical framework but was overlaid with other models such as that of Savolainen and Wilson.  
> **Method.** Data were collected from in-service teachers through focus group discussions, prospective teachers with a questionnaire, and key actors in Lesotho's secondary geography education context through individual interviews.  
> **Analysis.** The Statistical Package for Social Sciences was used to analyse the questionnaire responses. Content analysis was used to analyse the verbal responses as well as an open-ended question on the questionnaire. Triangulation was applied through using three sets of participants and different data collection techniques for each. It allowed comparative analysis of responses and data collection techniques used in the study.  
> **Results.** The work environment of in-service geography teachers is revealed including their work roles, associated tasks, information needs and information-seeking patterns. An information service model for Lesotho secondary level geography teachers (including possible interventions) is proposed.  
> **Conclusions.** Lesotho geography teachers need information to fulfil their work related roles and tasks that might be addressed through an information service. An implementation agency, information specialist(s) and collaborative approach to service delivery are crucial for the implementation and sustenance of the proposed information service.

## Background

Lesotho is a developing country in Southern Africa, totally enclosed by South Africa. Formal education in Lesotho starts with early childhood care and development, followed by seven years of basic primary education, then secondary education and tertiary education. Secondary education is divided into three years of junior secondary and two years of senior secondary. Learners start primary education at an age of six and write the Primary School Leaving Examination in their seventh year; this serves as an entry requirement into junior secondary education. There is Junior Certificate examination in third year of junior secondary, which determines entry into senior secondary that ends with Cambridge Overseas School Certificate examination which is a requirement for entry into tertiary education.

In Lesotho, the national Curriculum and Assessment Policy document has placed geography, sciences and technical subjects under the scientific and technological learning area; the schools, however, categorise geography in the social sciences given that geography, history and development studies teachers are in the department of social sciences. Geography is taught in Lesotho primary schools as part of social studies. It is then taught as a fully-fledged subject at secondary education even though it is not a compulsory subject; hence not all schools offer geography. While some schools offer geography at junior secondary, others offer it at senior secondary, and some at both levels. While the curriculum for junior secondary is developed in Lesotho, the curriculum for senior secondary is developed by the University of Cambridge. Both curricula have physical geography and human geography spheres. This study focused on secondary level geography teachers in schools that offer geography at both junior and senior secondary to investigate their information needs and information-seeking behaviour in order to guide the design and implementation of their information service.

The following factors and findings triggered the study on secondary level geography teachers in Lesotho:

*   Teaching and learning are associated with handling of information ([Karunarathna 2008](#Kar08)).
*   Teachers have been found to be active, experienced and critical users of information ([Taylor 1991: 219](#Tay91)).
*   Lesotho teachers have limited access to information and information communication and technology resources ([Moloi _et al._ 2008](#Mol08)).
*   Lesotho teachers have roles and responsibilities such as preparing learners for employment ([Boaduo 2005](#Boa05)) as well as counselling roles owing to an increasing number of orphans in schools because of HIV/AIDS ([Nyabanyaba 2009](#Nya09)). These all require pertinent information.
*   Most secondary level geography teachers in Lesotho graduate from the National University of Lesotho with a BSc.Ed., BAEd. or B.Ed. The BSc.Ed. teachers study mostly physical geography, while the rest study mostly human geography. This creates a disparity in knowledge content regarding these two major geographical spheres, which they have to teach in schools.
*   Little attention is given to geography teachers' in-service training compared to teachers of other compulsory subjects such as English, Mathematics and Science.
*   The researcher has experience and knowledge of teaching geography in Lesotho, hence it was easier to study secondary level geography teachers compared to teachers of other subjects.
*   In most of the newly opened government schools, geography is offered at Junior Certificate level. Previously, some of the schools were phasing out geography by offering subjects such as development studies, business education, etc. It was felt that geography as a subject could be threatened by the new subjects, including the introduction of environmental education in schools. It is envisaged that the provision of more information on geography may increase appreciation of this subject.
*   The need for an appropriate information service for geography teachers in Lesotho.
*   Limited research literature on information behaviour studies with regard to teachers ([Mundt _et al._ 2006](#Mun06)). A dearth of such studies is evident in Africa.

## Research problem

Effective information service depends largely on understanding fully the users' information needs and information-seeking behaviour ([Agosto & Hughes-Hassell 2005](#Ago05); [Hepworth 2007](#Hep07); [Wilson 2006](#Wil06)).

The research problem for the Lesotho geography teachers' context, assuming to require an appropriate information service was posed as a principal question:

> What are the information needs and information seeking patterns of secondary geography teachers in Lesotho with regards to their teaching and how can these guide the design and implementation of information service for these teachers?

This led to a number of sub-questions:

1.  What are the information needs of secondary level geography teachers in Lesotho?
2.  What are the information-seeking patterns of secondary level geography teachers in Lesotho?
3.  Which information sources do these teachers mostly use?
4.  Which information communication channels are preferred by these teachers to access and exchange information?
5.  Which information sources are available and accessible to these teachers at schools?
6.  What kind of an information service can be recommended for secondary level geography teachers in Lesotho?

## Clarification of key concepts

### Information need

Based on explanations from Belkin, Oddy and Brooks([1982](#Bel82)), Ikoja-Odongo and Mostert ([2006](#Iko06)), Ingwersen and Järvelin ([2005](#Ing05)), Krikelas ([1983](#Kri83)), Kuhlthau ([2004](#Kuh04)) and Wilson ([2000](#Wil00)), the study regarded information need as an absence or lack of information that creates deficiency in the knowledge of the geography teachers. Whilst the study acknowledges (from the authors above) that information needs may be **expressed** or **unexpressed**, or may be derived from the environment within which users operate, it considered only the information needs **expressed** by the geography teachers.

### Information-seeking

The following factors were considered in deciding on a definition of information seeking:

*   Information-seeking is a purposive acquisition of information ([Johnson 2003](#Joh03)).
*   Examines ways in which people find information they require ([Burke 2007](#Bur07)).
*   There is everyday life information-seeking ([Savolainen 1995](#Sav95)), that includes human sense-making ([Dervin 1998](#Der98)) behaviour of the environment and that it is considered more non-academic and less formal than professional or academic information behaviour ([Spink and Cole 2006](#Spi06)).
*   There is evidence of occasions when people make contact or interact with information sources through an agent, gatekeeper, intermediary ([McKenzie 2003](#Mck03)).
*   There is evidence of collaborative information-seeking whereby a person is not alone in the process, but rather works with others ([Foster 2006](#Fos06)).
*   Information-seeking may be passive or active ([Wilson 1999](#Wil99)).

The study considered information-seeking as the process engaged in by the geography teachers (individually or in groups) to find information, either with a specific purpose, through regular interaction with information sources or through agencies. This is because from the analysis of literature, an inference was made that there are four styles of information-seeking identified as: purposeful information-seeking ([Johnson 2003](#Joh03)), serendipitous information-seeking ([Foster and Ford 2003](#Fos03)), collaborative information-seeking ([Foster 2006](#Fos06))and information-seeking by proxy ([McKenzie 2003](#Mck03)).

### Information behaviour

Information behaviour is the 'human behaviour dealing with generation, communication and use of information and other activities concerned with information such as information-seeking and information retrieval' ([Ingwesen and Järvelin 2005](#Ing05)) and it encompasses information-seeking as well as the totality of other unintentional or passive behaviour (e.g., glimpses, encounters) and avoidance of information ([Case 2007](#Cas07); [Wilson 2000](#Wil00)). The study viewed information behaviour as any activities that teachers (individually/group) engage in relation to information including awareness of information needs, their active information-seeking, use of information in creation and presentation, communication, preferences and use of information sources.

## Information behaviour models

Wilson ([1999](#Wil99): 250-251) asserts that models can be presented theoretically or conceptually, aiming to provide a thinking framework about a problem; in this case, using a model enables specific research questions to be tested and researched. Some of the models reviewed for the reported study include:

*   Choo, Detlor and Turnball ([2000](#Cho00)) — information behaviour in digital environments.
*   Dervin ([1998](#Der98)) — sense-making.
*   Ellis ([1989](#Ell89), [1993](#Ell93)) — information-seeking.
*   Ingwersen ([1996](#Ing96)) — information retrieval.
*   Johnson ([1997](#Joh97)) — information-seeking in cancer context.
*   Krikelas ([1983](#Kri83)) — information-seeking behaviour.
*   Kuhlthau ([1993](#Kuh93)) — information search process.
*   Leckie, Pettigrew and Sylvain ([1996](#Lec96)) — information-seeking of professionals.
*   Savolainen ([1995](#Sav95)) — everyday life information-seeking.
*   Taylor ([1991](#Tay91)) — information use.
*   Wilson ([1981](#Wil81), [1999](#Wil99)) and Wilson and Walsh ([1996](#Wil96)) — general information behaviour.

## Theoretical framework

Information behaviour models do not test the same things, therefore researchers need to acquaint themselves with what each model has set out to test, then choose a model after determining whether the model will present the information required accurately, reliably and systematically, and whether it can explain or predict the desired phenomena, specifically enabling validation of representations and findings ([Wilson 1999](#Wil99)). This study chose the Leckie _et al._ ([1996](#Lec96)) professionals' information-seeking model to serve as its theoretical framework because it has attributes that could help to address the principal research question; it incorporates both information needs and information-seeking, which are the core aspects of the current study. In addition, the model was derived from studying professionals (lawyers, engineers and health professionals) in the workplace. According to Leckie _et al._ ([1996: 161](#Lec96)) it is applicable to all professionals.

<figure class="centre">![The information-seeking of professionals model ](p549fig1.png)  

<figcaption>Figure1: The information-seeking of professionals model ([Leckie _et al._ 1996](#Lec96): 180)</figcaption>

</figure>

Studies that used the Leckie _et al._ ([1996](#Lec96)) model include: Baker's ([2004](#Bak04)) study of undercover female police officers; Du Preez's research on consulting engineers ([2007](#Dup07), [2008](#Dup08)); Kerins, Madden and Fulton ([2004](#Ker04)) on engineering and law students; Kostiainen, Valtonen and Vakkari ([2003](#Kos03)) on police in pre-trial investigations; Landry's study of dentists ([2006](#Lan06)) and Mundt _et al_ ([2006](#Mun06)) on teachers.

Each model has its own strengths and weaknesses ([Ikoja-Odongo & Mostert 2006](#Iko06)); therefore the Leckie _et al._ ([1996](#Lec96)) model was overlaid by three other models. Everyday life information-seeking model ([Savolainen 1995](#Sav95)) featured in the recommendations to reveal Lesotho teachers' context (preparing learners for employment, assuming various roles, HIV/AIDS, bureaucracy, influx of learners). The nested model of information behaviour ([Wilson, 1999](#Wil99)) was used in data analysis to interpret geography teachers' information-seeking. Tarby and Hogan ([1997](#Tar97)) information service model guided the formulation of the information service model for Lesotho geography teachers.

## Data collection

Data were collected from the following groups considered to be representatives of the Lesotho geography education sector: in-service geography teachers through focus group discussions in schools that offer geography at both junior and senior secondary in January to March 2010; partial observations of school libraries in schools where in-service teachers work was done before or after the focus group discussions; prospective geography teachers (4th year students in the Faculty of Education, National University of Lesotho majoring in geography) with a questionnaire distributed in March 2010, which solicited these teachers' information needs and information-seeking during their teaching practice, and which were handed in to the geography educator lecturer in April 2010; and officials in institutions involved in geography education in Lesotho (i.e., teacher training, professional development, curriculum design and development, teaching and learning geography inspection, examinations, etc.) using individual interviews in April 2010.

### Sampling

A purposive sampling strategy (i.e., the selection of a sample on the basis of its contribution of information-rich cases for in-depth study ([Patton, 1990](#Pat90)), was used to select in-service teachers' schools to include in the study. Issues considered in the selection include: first, schools' proprietorship such that schools with different proprietors are included within one district. In Lesotho popular school proprietors are government, community and churches of denominations such as Anglican Church of Lesotho, Lesotho Evangelical Church and Roman Catholic Church. Secondly, distribution by district, that is, fair representation of all districts including rural and urban schools, and lowlands and highlands regions within one district. Rural and highlands regions have lower population density and less development when compared with urban and lowlands regions.

No sampling was done for prospective teachers and officials in institutions involved in secondary geography education, since the potential number of participants were considered few enough to deal with and did not warrant sampling.

### Sample size and response rate

The study's sample size and response rates were: prospective geography teachers, 74.2% (46/62); in-service geography teachers, 59.4 % (82/138); schools that participated in the study through their in-service geography teachers, 54.9% (28/51). (i.e., schools offering geography at both junior and senior secondary level); school libraries that were observed, 36.2% (17/47; 47/51 schools had libraries); and institutions (indicated under data collection above) involved in secondary geography education — 66.7% (8/12).

### Data analysis

A variety of methods was used for the data analysis such as the Statistical Package for Social Science for questionnaire responses. Content analysis was utilised to analyse focus group discussions, interviews and an open-ended question on the questionnaire which was included to solicit recommendations for information service. Using three sets of participants and different data collection techniques for each, allowed for triangulation of data as well as a comparative analysis of responses and data collection techniques used in the study. The latter will not be reflected in this paper. Although Bitso ([2011](#Bit11)) outlines the rationale for the data collection approaches used in this study, it is important to highlight that data analysis and interpretation was guided by the principal research question and the theoretical framework. According to Leckie _et al._ [(1996: 179](#Lec96)), in order to investigate the information-seeking behaviour of professionals, the broader working context in which professionals' practice is conducted must be closely examined and understood. Hence this study tried to understand the working context of teachers participating in this study to gain deeper understanding of their information needs, information-seeking and use.

## Findings

### Context of Lesotho secondary level geography teachers

In order to investigate the information-seeking behaviour of professionals, the broader working context in which professionals' practice is conducted must be closely examined and understood ([Leckie _et al._ 1996](#Lec96): 179; [Baker 2004;](#Bak04) [Johnson 2003](#Joh03); [Niedzwiedzka 2003](#Nie03)). With regard to the context in which Lesotho secondary level geography teachers practice, the following conditions were noted:

*   Lesotho as a country faces challenges such as a high prevalence of HIV and AIDS, a high unemployment rate, poverty and limited information services and a poor information infrastructure.
*   Bureaucracy in the school governance is affected by the fact that schools are owned by the government, churches and communities. Formal education started with missionaries who built schools. Later the government intervened by paying teachers' salaries without taking over the schools.
*   The work environment in schools in Lesotho is marked by constraints such as lack of finances and teaching materials, lack of facilities such as libraries and overcrowding in classrooms. Where libraries exist, they have limited books that are outdated and not related to geography.
*   '_Over-sized classes and scarcities of resources coupled with lack of trained teachers has been an outstanding problem in Lesotho for a long time_' ([Moloi _et al._ 2008](#Mol08): 613). This finding has been confirmed by the study being reported. Teachers experience a very high teaching load of 25-30 teaching periods a week. A period is a 40 minutes lesson. The average class size ranges from 40-70 pupils. There are many under the teachers' care on the school premises who need to use the limited facilities and resources of the schools.
*   A shortage of geography books for students and teachers particularly for senior secondary level has been noted. Although a government book rental scheme has been active at junior secondary level, it was only extended to the senior secondary level from the beginning of 2012.
*   Geography teachers have formed small geography teachers' associations that regularly hold meetings, workshops, plan teaching and learning including setting of common tests.
*   The HIV/AIDS pandemic impinges on the teachers since they are exposed to orphans and vulnerable children, with no learning support at home concerning school work and other life matters. Teachers thus become sources of information and support for such children.

### Information-seeking behaviour of Lesotho secondary level geography teachers

The findings of the study are presented in Figures 2, 3 and 4 in the subsequent sections. These are based on the research questions, the theoretical framework and the context of the Lesotho secondary level geography teachers. More findings can be found in Bitso's ([2011](#Bit11)) doctoral study, which is available electronically in the institutional repository of the University of Pretoria.

<figure class="centre">![Secondary level geography teachers' information needs and information-seeking patterns](p549fig2.png)  

<figcaption>Figure 2: Secondary level geography teachers' information needs and information-seeking patterns ([Bitso 2011](#Bit11): 210)</figcaption>

</figure>

<section>

While Figure 2 presents the findings based on the context of the geography teachers and the research questions, Figure 3 relates these findings to the information-seeking of professionals' model that was used as the theoretical framework in this study.

<figure class="centre">![Findings based on the Leckie <em>et al.</em> (1996) model](p549fig3.png)  

<figcaption>  
Figure 3: Findings based on the Leckie _et al._ (1996) model ([Bitso 2011](#Bit11): 208)</figcaption>

</figure>

Wilson's ([1999](#Wil99)) nested or onion model of information behaviour encompasses information-seeking behaviour, which also includes information search behaviour and then information retrieval in such a manner that the inner rings are subsets of the outermost ring (i.e., information behaviour). Information behaviour is the broader concept comprising information-seeking, information-searching and information retrieval. In Figure 4 below the idea of a nested model is borrowed to depict an onion model in an order to move from specific to general.

<figure class="centre">![Secondary level geography teachers' information-seeking using the nested model](p549fig4.png)  

<figcaption>  
Figure 4: Secondary level geography teachers' information-seeking using the nested model ([Bitso 2011](#Bit11): 191)</figcaption>

</figure>

In Figure 4 the onion model is used to depict information-seeking, which starts from the small inner component and expands to the next ring, depending on the complexity of the information need, until the information need is addressed. The model also reflects commitment to information-seeking among the teachers who participated in the survey to pursue various sources on different levels to find information.

## Factors guiding the design of an information service for Lesotho geography teachers

Underwood ([1990](#Und90)) reminds us that when designing information services, one must focus on the users of the information service and should think of operational issues by outlining specific products and the details of the service, including how it will be delivered and developed over time. Consequently the study outlined the factors to consider for the information service of geography teachers and these are presented in [Figure 5](#Fig5) below.

<figure class="centre">![Factors guiding the design of an information service for Lesotho geography teachers](p549fig5.png)  

<figcaption>Figure 5: Factors guiding the design of an information service for Lesotho geography teachers ([Bitso 2011](#Bit11): 226)</figcaption>

</figure>

The information specialists are part of the human resource; technology requirements include information and community technology; and the users of the service are secondary level geography teachers. The objectives of the services are outlined in the next section.

## Objectives of the proposed information service

Considering the findings from the data, it seems that an appropriate information service meeting the information needs and information seeking behaviour expressed by the participants, and also in line with the partial observations of the school library situation, need to meet with certain objectives. The following objectives were therefore formulated:

*   To collect and repackage relevant physical geography information to cover the scope of information needed by secondary level geography teachers to address the need for physical geography content expressed in Figure 2.
*   To provide a range of information sources in multimedia formats relevant to the Lesotho junior and senior secondary geography syllabi to address the needs expressed by teachers such as that of preference for information in print format and in audio-visual information material for use in class.
*   To create a reference service that serves as an inquiry and answer service for secondary level geography teachers in order to address the need for current and accurate content.
*   To strengthen geography education research through provision of research services to researchers and teachers in line with Cloutier's ([2005](#Clo05)) suggestion that information services may be document delivery services and research services, as well as in line with the researcher role for the teachers. Increasing research output is a contribution of knowledge that might address the need for current and accurate information expressed by the teachers.
*   To promote information resource-sharing among secondary level geography teachers and in Lesotho schools at large to address the shortage of information sources.
*   To provide alerting services on literature that is relevant to geography education to enable teachers to keep abreast with the latest developments and address their expressed need for current information.
*   To encourage the use of facilities for internet access to enable the teachers to access information that is available on the internet.
*   To evaluate the information service for secondary level geography teachers regularly.

## Proposed information service model

Based on the context of Lesotho geography teachers, their information needs, their information-seeking including their preferred information sources outlined in Figures 2, 3 and 4, as well as on the objectives of the information service above, the study proposed an information service model for Lesotho geography teachers. The model is presented as Figure 6 below. It includes secondary level geography teachers as the core users of the service, who use the schools' resources and consult various institutions, geography education specialists and the information specialists working at an implementing agency (e.g., a library or resource centre). The model involves institutions/individuals, teachers and geography education specialists who are perceived to have relevant information and interest in secondary level geography education in Lesotho, such that they may be referred to as stakeholders in the proposed model.

<figure class="centre">![Proposed information service model for secondary level geography teachers in Lesotho](p549fig6.png)  

<figcaption>Figure 6: Proposed information service model for secondary level geography teachers in Lesotho ([Bitso 2011](#Bit11): 240)</figcaption>

</figure>

In view of shortage of resources in Lesotho schools and the importance of information in teaching, the proposed information service model for geography teachers in Lesotho may serve as a prototype that may be adapted and implemented across the curriculum. This study only focused on geography teachers purely on the basis of the knowledge and experiences of teaching geography that the researcher (lead author of this paper) has, as a former geography teacher in Lesotho. The next section explains modes for the implementation of the proposed information service model.

## Implementation strategy for the proposed information service model

The information service model proposed in this study requires in-house services that will be provided by an implementing agency (i.e., library, resource centre or documentation centre) to plan, implement and manage the entire process of developing and deploying information services for these teachers. This section explains the ways of implementing the proposed information service, bearing in mind all parties concerned in the model and it is inclined towards the collaborative approach because it is set out to forge partnerships and include task teams.

<figure class="centre">![Ways of implementing the proposed information service](p549fig7.png)  

<figcaption>Figure 7: Ways of implementing the proposed information service ([Bitso 2011](#Bit11): 247)</figcaption>

</figure>

## Recommendations for information service interventions

There is a scarcity of resources in Lesotho and this may require resource-sharing and a collaborative approach involving various stakeholders, notwithstanding the important role of an information specialist and implementation agency. The following recommendations are put forward as further interventions meant to improve information service delivery for the geography teachers.

*   Implementation agency for the proposed information service.
*   Collaborative approach for service delivery considering the limited resources available in Lesotho.
*   Allowance for traditional print media and modern electronic formats.
*   Compiling and disseminating tailor-made information packages for teachers whose work load is heavy.
*   Availability of information resources such as computers, printers, fax lines, photocopiers, scanners, etc. in schools.
*   Building up personal files and information collections and documentation of these resources systematically for easy retrieval.
*   Introduction and maintenance of alerting services for teachers.
*   Documentation and dissemination of information for the activities of geography teachers' associations into reports that may serve as information sources. The geography teachers' association are operating in various districts at a small scale, and therefore it is recommended that they join to form a national geography teachers' association.
*   Sustenance of the information service.
*   Exploration of mobile devices for information access.
*   Introduction of inter-library lending through school libraries consortium.

## Recommendations for further research

In order to strengthen the proposed information service model and recommended interventions, further research on the following issues has to be considered for optimum benefits: assessment of information literacy skills among teachers is important bearing in mind the recommendation for internet access, and access to information in various formats; analysis of best internet access tools appropriate for the teachers and the e-readiness in Lesotho schools; a feasibility study on clustering of schools for information resource-sharing and a consortium; a study of teachers' information-seeking in non-work context particularly for their care-giving role using Savolainen's ([1995](#Sav95)) everyday life information-seeking model; finally, HIV and AIDS prevalence warrants the studying of information-seeking behaviour of care-giving professions to develop a new or adapted model for their information behaviour and their information service.

This study focused on geography teachers' information needs and information-seeking in order to support them with information through the design of an appropriate information service. It presumes that supporting geography teachers with appropriate information service indirectly support geography instruction. However, the study's inability to articulate how the information service model proposed in this study would directly support geography instruction is noted as limitation. This limitation needs to be considered as part of the impact assessment after the proposed information service has been implemented.

## Conclusion

The study revealed the information needs and information-seeking behaviour of secondary level geography teachers in Lesotho. Based on the data presented and interpreted in the figures above, as well as the context of Lesotho geography teachers, the study proposed an information service model for secondary level geography teachers in Lesotho. The model is viewed as an intervention to the problems of information and information resources and as an end product of an analysis of the information needs and information-seeking behaviour of secondary level geography teachers in their work environment and country. Considering the limited information services and resources in Lesotho schools, it is recommended that the model proposed in this study be adapted for possible implementation across all subjects.

The paper outlined the modalities for the implementation of the information service and suggested the crucial involvement of an information specialist, implementation agency and a collaborative approach to service delivery. In the midst of limited resources, information sharing and giving is important, therefore clustering of schools and formation of a school library consortium for resource sharing is encouraged. Recommendations serving as more interventions to the context of the geography teachers in Lesotho were outlined including recommendations for further study. Of utmost importance, an implementation agency, information specialist(s) and collaborative approach to service delivery is crucial for the implementation and sustenance of the proposed information service.

## Acknowledgements

With appreciation to Tina van der Vyver for converting the document to XHTML.  
We also want to acknowledge the authors for useful material from Mundt, M., Stockert, K. & Yellesetty, L. (2006). _The information behaviour of teachers_, retrieved May 28, 2008, from http://katestockert.com/LIS510_Information_Behaviour_of_Teachers.pdf This has since disappeared and no equivalent of this document could be found. [Editor's note: it appears that this was simply a piece of coursework for a course at the University of Washington]

## About the authors

**Constance Bitso** is is a lecturer in the Department of Information Science, University of Pretoria, South Africa. She received her Bachelor's degree in Science Education at the National University of Lesotho. She obtained Postgraduate Diploma, Honours' Degree and Masters Degree in Library and Information Science from the University of Cape Town (South Africa) and her D Phil (Information Science) from the University of Pretoria. She taught geography in Lesotho before changing career to Information Science. She can be contacted at [connie.bitso@up.ac.za](mailto:connie.bitso@up.ac.za)  
**Ina Fourie** is full Professor in the Department of Information Science, University of Pretoria, South Africa. She received her Bachelor's degree in Library and Information Science, her Honours' and Master's degree in Library and Information Science from the University of the Orange Free State, Bloemfontein (South Africa) her D.Litt et Phil from the Rand Afrikaans University, Johannesburg (South Africa), and a Post-graduate Diploma in Tertiary Education from the University of South Africa (Pretoria). She can be contacted at [ina.fourie@up.ac.za](mailto:ina.fourie@up.ac.za)

#### References

*   Agosto, D.E. & Hughes-Hasell, D. (2005). People, places, and questions: an investigation of the everyday life information-seeking behaviours of urban young adults. _Library and Information Science Research,_ **27**(2), 141-163.
*   Baker, L.M. (2004). [The information needs of female police officers involved in undercover prostitution work.](http://www.webcitation.org/6CH4T1vXz) _Information Research,_ **10**(1). Retrieved August 30, 2011 from ../ir/10-1/paper209.html (Archived by WebCite® at http://www.webcitation.org/6CH4T1vXz)
*   Belkin, N.J., Oddy, R.N. & Brooks, H.M. (1982). ASK for information retrieval: background and theory. Part I. _Journal of Documentation,_ **38**(2), 61-71.
*   Bitso, C.M.L. (2011). _[The information needs and information-seeking patterns of secondary level geography teachers in Lesotho: implications for information service](http://www.webcitation.org/6CQ1acw3y)_. Unpublished doctoral thesis, University of Pretoria, Pretoria, South Africa. Retrieved 24 November, 2012 from http://upetd.up.ac.za/thesis/available/etd-05132012-163028/unrestricted/00front.pdf (Archived by WebCite® at http://www.webcitation.org/6CQ1acw3y)
*   Boaduo, N.A. (2005). [An investigative study of innovation and reform in the education system of the Kingdom of Lesotho.](http://www.webcitation.org/6CH4q8ZO1) _The African Symposium: an Online Journal of African Educational Research_. Retrieved August 30, 2011, from www.ncsu.edu/aern/TAS5.1/TAS5.1.pdf (Archived by WebCite® http://www.webcitation.org/6CH4q8ZO1)
*   Burke, M.E. (2007). Cultural issues, organizational hierarchy and information fulfilment: an exploration of relationships. _Library Review,_ **56**(8), 678-693.
*   Case, D.O. (2007). _Looking for information: a survey of research on information seeking, needs, and behaviour_. 2nd ed. Amsterdam: Elsevier.
*   Choo, C.W., Detlor, B. & Turnball, D. (2000). [Information seeking on the web - an integrated model of browsing and searching.](http://www.webcitation.org/6CQ20T1hB) _First Monday_, **5**(2) Retrieved 24 November, 2012 from http://firstmonday.org/htbin/cgiwrap/bin/ojs/index.php/fm/article/view/729/638 (Archived by WebCite® at http://www.webcitation.org/6CQ20T1hB)
*   Cloutier, C. (2005). Setting up a fee-based information service in an academic library. _Journal of Academic Librarianship,_ **31**(4), 332-338.
*   Dervin, B. (1998). Sense-making theory and practice: an overview of user interests in knowledge seeking and use. _Journal of Knowledge Management,_ **2**(2), 36-46.
*   Du Preez, M. (2007). Information needs and information-seeking behaviour of engineers: a systematic review. _Mousaion,_ **25**(2), 72-94.
*   Du Preez, M. (2008). _[Information needs and information-seeking behaviour of consulting engineers: a qualitative investigation](http://www.webcitation.org/6CQ2L31qv)_. Unpublished master's thesis, University of South Africa, Pretoria, South Africa. Retrieved 24 November, 2012 from http://uir.unisa.ac.za/bitstream/handle/10500/1941/dissertation.pdf?sequence=1 (Archived by WebCite® at http://www.webcitation.org/6CQ2L31qv)
*   Ellis, D. (1989). A behavioural approach to information retrieval system design. _Journal of Documentation,_ **45**(3), 171-212.
*   Ellis, D. (1993). Modelling the information-seeking patterns of academic researchers: a grounded theory approach. _Library Quarterly,_ **63**(4), 469-486.
*   Foster, A. & Ford, N. (2003). Serendipity and information seeking: an empirical study. _Journal of Documentation_, **59**(3), 321 – 340.
*   Foster, J. (2006). Collaborative information seeking and retrieval. In: B. Cronin (ed.) _Annual Review of Information Science and Technology_. Vol. **40**, (pp. 329-356). Medford, NJ: Information Today.
*   Hepworth, M. (2007). Knowledge of information behaviour and its relevance to the design of people-centred information products and services. _Journal of Documentation,_ **63**(1), 33-56.
*   Ikoja-Odongo, R. & Mostert, J. (2006). Information seeking behaviour: a conceptual framework. _South African Journal of Libraries and Information Science,_ **72**(3), 145-158.
*   Ingwersen, P. (1996). Cognitive perspective of information retrieval interaction: elements of a cognitive IR theory. _Journal of Documentation,_ **52**(1), 3-50.
*   Ingwersen, P. & Järvelin, K. (2005). _The turn: integration of information seeking and retrieval in context_. Dordrecht: Springer.
*   Johnson, J.D. (1997). _Cancer-related information seeking_. Cresskill, NJ: Hampton Press.
*   Johnson, J.D. (2003). On contexts of information seeking. _Information Processing and Management,_ **39**(5), 735-760.
*   Karunarathna, A. (2008). [_Information-seeking behaviour of university teachers in Sri-Lanka in the field of management studies._](http://www.webcitation.org/6CH5LNQxb) Retrieved August 30, 2011, from http://hdl.handle.net/10760/12699 (Archived by WebCite® http://www.webcitation.org/6CH5LNQxb)
*   Kerins, G., Madden, R. & Fulton, C. (2004). [Information seeking and students studying for professional careers: the cases of engineering and law students in Ireland.](http://www.webcitation.org/6CH5WSOwn) _Information Research,_ **10**(1). Retrieved August, 30, 2011 from ../ir/10-1/paper208.html (Archived by WebCite® http://www.webcitation.org/6CH5WSOwn)
*   Kostiainen, E., Valtonen, M.R. & Vakkari, P. (2003). Information seeking in pre-trial investigation with particular reference to records management. _Archival Science,_ **3**(2), 157-176.
*   Krikelas, J. (1983). Information-seeking behavior: patterns and concepts. _Drexel Library Quarterly,_ **19**(2), 5-20.
*   Kuhlthau, C.C. (1993). A principle of uncertainty for information seeking. _Journal of Documentation,_ **49**(4), 339-355.
*   Kuhlthau, C.C. (2004). _Seeking meaning: a process approach to library and information services._ 2nd ed. Westport, Connecticut: Libraries Unlimited.
*   Landry, C.F. (2006). Work roles, tasks and the information behaviour of dentists. _Journal of the American Society for Information Science and Technology,_ **57**(14), 1896-1908.
*   Leckie, G.J., Pettigrew, K.E. & Sylvain, C. (1996). Modelling the information seeking of professionals: a general model derived from research on engineers, health care professionals and lawyers. _Library Quarterly,_ **66**(2), 161-193.
*   Mckenzie, P.J. (2003). A model of information practices in accounts of everyday-life information seeking. _Journal of Documentation,_ **59**(1), 19-40.
*   Moloi, F., Morobe, N. & Urwick, J. (2008). Free but inaccessible primary education: a critique of the pedagogy of English and mathematics in Lesotho. _International Journal of Educational Development,_ **28**(5), 612-621.
*   Niedzwiedzka, B. (2003). [A proposed general model of information behaviour.](http://www.webcitation.org/6CH7RXJih) _Information Research,_ **9**(1), paper 164\. Retrieved August 30, 2011, from ../ir/9-1/paper164.html (Archived by WebCite® http://www.webcitation.org/6CH7RXJih)
*   Nyabanyaba, T. (2009). [_Factors influencing access and retention in secondary schooling for orphaned and vulnerable children and young people: case studies from high HIV and AIDS prevalence contexts in Lesotho_](http://www.webcitation.org/6CH81Naqm). London: Institute of Education, University of London. Retrieved August 30, 2011, from http://sofie.ioe.ac.uk/publications/LesothoCaseStudyReport.pdf (Archived by WebCite® at http://www.webcitation.org/6CH81Naqm)
*   Patton, M.Q. (1990). _Qualitative evaluation and research methods_. London: Sage Publications.
*   Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in context of “way of life”. _Library and Information Science Research,_ **17**(3), 259-294.
*   Spink, A. & Cole, C. (2006). Human information behaviour: integrating diverse approaches and information use. _Journal of the American Society for Information Science and Technology,_ **57**(1), 25-35.
*   Tarby, W. & Hogan, K. (1997). Hospital-based patient information services: a model for collaboration. _Bulletin of the Medical Library Association,_ **85**(2), 158-166.
*   Taylor, R.S. (1991). Information use environments. _Progress in Communication Sciences,_ **10**, 217-255.
*   Underwood, P.G. (1990). _Managing change in libraries and information services: a systems approach._ London: Clive Bingley.
*   Wilson, T.D. (1981). [On user studies and information needs](http://www.webcitation.org/6CQ39jrVF). _Journal of Documentation,_ **37**(2), 3-15\. Retrieved 24 November, 2012 from http://informationr.net/tdw/publ/papers/1981infoneeds.html (Archived by WebCite® at http://www.webcitation.org/6CQ39jrVF)
*   Wilson, T.D. (1999). [Models in information behaviour research.](http://www.webcitation.org/6CQ3RVMlX) _Journal of Documentation,_ **55**(3), 249-270\. Retrieved 24 November, 2012 from http://informationr.net/tdw/publ/papers/1999JDoc.html (Archived by WebCite® at http://www.webcitation.org/6CQ3RVMlX)
*   Wilson, T.D. (2000). [Human information behavior.](http://www.webcitation.org/6CQ3ecu5a) _Informing Science,_ **3**(2), 49-56\. Retrieved 24 November, 2012 from http://www.inform.nu/Articles/Vol3/v3n2p49-56.pdf (Archived by WebCite® at http://www.webcitation.org/6CQ3ecu5a)
*   Wilson, T.D. (2006). [Revisiting user studies and information needs.](http://www.webcitation.org/6CQN5YDXJ) _Journal of Documentation,_ **62**(6), 680-684\. Retrieved 24 November, 2012 from http://informationr.net/tdw/publ/papers/2006JDoc.html (Archived by WebCite® at http://www.webcitation.org/6CQN5YDXJ)
*   Wilson, T.D. & Walsh, C. (1996). [_Information behaviour: an interdisciplinary perspective._](http://www.webcitation.org/6CH7kqBjv). Sheffield, UK: Department of Information Studies. (British Library Research and Innovation Report, 10) Retrieved August 30, 2011, from http://informationr.net/tdw/publ/infbehav/ (Archived by WebCite® http://www.webcitation.org/6CH7kqBjv)

</section>