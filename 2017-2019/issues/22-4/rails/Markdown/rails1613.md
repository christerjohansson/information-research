<header>

#### vol.22 no. 4, December, 2017

</header>

Proceedings of RAILS - Research Applications, Information and Library Studies, 2016: School of Information Management, Victoria University of Wellington, New Zealand, 6-8 December, 2016\.

<article>

# Just rol[l/e] with it: the sense-making practices of a tabletop roleplaying game community

## [Alex Wylie Atmore](#author)

> **Introduction.** A study was conducted into tabletop roleplaying games, using theoretical lenses from both information research and tabletop roleplaying game research, to observe the complexity of the relationship between the players and the official rulebooks.  
> **Method.** A series of qualitative interviews was conducted with two groups of tabletop roleplaying gamers (a large, established group and a smaller, group of less experienced players) based on how they interact with the source material (the rulebooks).  
> **Analysis.** Coded transcripts were analysed for trends and patterns of discourse, and the theoretical perspective of sense-making (from information research) was applied, to provide meaning to two common and related discursive frameworks from tabletop roleplaying research: the divide between ludologists (or role players); and how illicit knowledge is constructed through metagaming.  
> **Results.** The study found the discursive frameworks surrounding metagaming and the role/roll player divide to be less a dividing factor and more a concept all players grasped with in the project of co-creating a fantastic reality: a sense-making process.  
> **Conclusions.** From the point of information research, the study found that tabletop roleplaying games are an example of an information-rich leisure activity that allows and encourages its participants to ignore the official rules surrounding it. From the point of view of tabletop roleplaying game literature, the study casts doubt on the role/roll player dyad commonly adopted by theorists.

<section>

## Introduction

This paper details a study of the information behaviour of groups of people who play tabletop roleplaying games (TRPG), in particular observing the complex interaction between these groups and the official sources of information provided by a game’s creators. The purpose of the study was: to provide connections between the academic disciplines of information research and tabletop roleplaying research; to determine if information research theory could assist in the understanding of the phenomenon of tabletop roleplaying games; to observe an information-rich setting that is interacted with purely for pleasure; and to add to growing literature in a highly under-researched field.

</section>

<section>

### Tabletop roleplaying games: a description

Tabletop roleplaying games involve several people (usually a group of between three and five people) each pretending to be a character from a fictional alternate reality. The rest of the characters, and information about the world as the player characters perceive it, is provided by a facilitator-leader player, who is commonly referred to as the _GM_, short for gamesmaster or game master. The characters, the fictional reality, and the story of their exploits are a process of joint construction between the individual players, the gamesmaster, and the group as a whole as they interact with the rule set. This rule set is typically provided by an external source (the rulebooks), which provides mechanical rules (mathematical abstractions of actions, for example whether a character is capable of dodging a blow), and rules for how to understand the fictional reality (which are commonly prosaic, and referred to as either _story_ or _fluff_). When there is a gap between how the group perceives their shared reality, it will be a disconnection between how the mechanical reality is presented, how the fluff reality is presented, and/or how the group chooses to interpret these things. This tension is the source of the study. The interplay between mechanical rules, formulaic abstractions to be applied, and story or fluff, to be considered, discussed and interpreted, is explored in more detail below.

Tabletop roleplaying games are an information-rich leisure activity. Roleplaying games study as a focus of scholarly study (in particular the tabletop variant) is still in its infancy, hovering somewhere between arts and computer sciences ([Deterding and Zagal, 2013](#det13)), despite roleplaying games coming into existence more than three decades ago and having serious impacts on the modern world. In 2014, a joint Bond University-Interactive Games & Entertainment Service study found 65% of Australians play computer-based games; of this a full 15% is Internet-based role-playing games ([Brand, Lorentz and Mathew 2015](#bra15), p. 12). The study of tabletop roleplaying games, outside of its application to computer-based or digital roleplaying games, is even more under-researched, even according to its more prolific scholars ([Harviainen, 2009](#har09)).

There is no universally agreed-upon definition of _game_’ ([Ehrett and Worth, 2012](#ehr12)), so the first obstacle is determining what a tabletop roleplaying game is. Some view it as a vehicle for storytelling ([Bergström, 2012](#ber12); [Nicholas, 2014](#nic14)); some as a ritual (Lehrich, 2005, cited in [Evans 2012](#eva12)); others as a tactical battle-simulator ([Huvila, 2013](#huv13)) based on its roots ([Cogburn and Silcox, 2012](#cog12)). The question is largely settled by the semi-academic Forge group, which suggests that tabletop roleplaying groups are an exercise in shared play where a story is created between the players, rule books, and gamesmaster ([Kim, 2003](#kim03)). This is reframed by Gary Fine as three frames of game play (Fine, 1983, cited in [Montola, 2009](#mon09), p. 23) where the players and their characters interact simultaneously and separately.

The major scholarly focus in tabletop roleplaying game theory has been on the differences between ludology and narratology, that is, between people who play tabletop roleplaying games to enjoy it as a tactical challenge to be overcome and people who play the games to tell an engaging story (see for example [Brackin, 2012](#bra12), pp. 241-3; [Evans, 2012](#eva12), pp. 182-3; [Kim, 2003](#kim03); or [Rose, 2012](#ros12), pp. 270-4). The colloquial terms for the two positions are _role player_ for narratology (where the core game play is adopting a role) and _roll player_ for ludology (where the core interaction with the system is through rolling dice, which are used to add an element of random chance to many tabletop roleplaying games).

Another focus for discussion within the gaming community has been the tension between the elements that make up a tabletop roleplaying game: _mechanics_ and _fluff_ ([Woods, 2012](#woo12)). Even in games that are light on rules ([Montola,](#mon09) 2009, p. 29), the players’ _‘relationship with the world [is] defined by the ruleset’_ ([Woods, 2012](#woo12), p. 26). For the vast majority of tabletop roleplaying games, this includes the player character’s physical and mental attributes, in an attempt to quantify player character’s abilities. This is usually taken to be an aid to players in determining the physical and mental delimitations around how they should act ([Bateman, 2012](#bat12), p. 225; [White, 2014](#whi14), pp. 86-7). Deliberately stepping outside of the rules’ boundaries is often identified as _metagaming_, and is considered almost the only method of cheating in a tabletop roleplaying game ([Xu, Barba, Radu, Gandy and MacIntyre, 2011](#xuy11), p. 9). The precise definition of metagaming is defined by the gaming community in question, but in wider tabletop roleplaying gaming circles (and its research) it is seen as using _illicit knowledge_: information that a player has but a player’s character should not be aware of (anachronistic technology, for example, see [Rose, 2012](#ros12)).

The practice of metagaming; the related discursive framework around the supposed divide between ludology and narratology; how these discursive frameworks operate; and how they are understood by players are the focus of this study. There is a clear gap in the literature and one where a contribution for the field of information studies can make a contribution. At the heart of both conceptions of roleplaying is information: the rules and their place of authority within the game.

In this study, the application of information research theory has resulted in a focus on sense-making ([Dervin, 1991](#der91)), as an analytical tool and theoretical perspective ([Dervin, 1992](#der92); [Savolainen, 2007](#sav07)). The focus of the interviews was on how players interact with the official rules documents, and specifically on moments in-session when the rules and the story elements are out of alignment, and the players require clarification of the rules before the game can proceed. This has been referred to within this paper as a _rules-story gap_, a term used deliberately to aid the connection between Dervin’s notion of the gap in sense-making and tabletop roleplaying games research. In sense-making terms, it is an ongoing process of reshaping individuals’ understanding of their reality, requiring dialogue within a social framework. The rules-story gaps identified demonstrate the methods that players use in-game to negotiate the tension between _mechanics_ and _fluff_ whenever the information provided clashes, reducing the players’ immersion in the fictional reality being created. The common conception in tabletop roleplaying research, that these elements are the only two present, tends to marginalise the effect that the players will have on creating the fictional reality.

Even outside of the disciple of information research, it is widely acknowledged that roleplaying games in general, and tabletop ones specifically, involve a process of social construction through discourse. They have a code of conduct and social rules, central to the game but not identified in the official rules ([Bergström, 2012](#ber12); [Huvila, 2013](#huv13)). Roleplaying games have an alternate culture, where individual gaming groups and the wider gaming community adopt specialist practices and value specialist skills, where specialised abbreviations and spelling indicate fluency ([Adams, 2005](#ada05)). When it comes to finding information, “players are encouraged to ask questions and to depend on the players’ community for knowledge” (Snider, cited in [Adams, 2005](#ada05)). This trend continues in tabletop roleplaying games where rule books and online sources of information are present, but players will speak with each other first. In a relation to practice theory and developing a _‘taste’_, research has found that roleplaying game players use rules collectively and critically ([Adams, 2005](#ada05); [Beattie, 2007](#bea07)), and while official documents are shared among the community ([Beattie, 2007](#bea07)), groups are expected to develop their own interpretations. Furthermore, gaming groups gravitate toward these practices as they continue developing ([Nicholas, 2014](#nic14)). While not explicitly stated, some tabletop roleplaying game research is similarly engaged with communally-agreed practices, in particular Markus Montola’s 2009 paper, titled ‘_the_ invisible rules _of role-playing’ [emphasis_ added].

</section>

<section>

## Methodology

Sense-making was used as a conceptual framework for this study and provided an analytical tool to observe the complex relationship between the tabletop roleplaying game group, the (series of) text(s), i.e., the rulebooks, and the fictional reality that was co-created in the process of bringing these sometimes disparate elements together; to _make sense_ of the world as it was being shaped. This study also used sense-making as a method to identify when players felt they needed information in game sessions, where players would turn to in order to find information when it was required, and how the gaming group would agree on what was the best information and how to collectively interpret it. In other words: to identify how players make sense of a problem they encounter; how they bridge the gap between their current knowledge and what they believe to be the desired end point; and how that process is one of continuous or near-continuous social discourse around the nature of the fictional reality that is being co-created.

The study examined the information behaviour of a group of players specialising in a game system called _Pathfinder_. _Pathfinder_ is based around the more well-known _Dungeons & Dragons_ ([Gygax and Arneson, 1974](#gyg74)). This game system is very mechanics-heavy, which is a deliberate choice: firstly, as Bergström ([2012](#ber12)) identifies, systems that operate with fewer or more deliberately ambiguous rules will have different practices around rules-story gaps; secondly, this style of rule system is most common and most commonly known. Finally, the more complex systems will afford the largest amount of potential rules-story gaps, to best fit with the sense-making framework. It is, however, worth pointing out that even very _rules-light_ game systems make use of a _mechanic_ to have the players interact with the fictional reality ([Montola, 2009](#mon09), p. 29).

Data were collected via semi-structured interviews, ranging from a half hour to two hours in length, conducted over two week-long periods. This was to best accommodate the participants’ schedules, and to provide an expansion of the original participant sample, as discussed below. This method is common in tabletop roleplaying game research ([Bergström, 2012](#ber12); Montola, 2011, cited in [Lankoski and Järvelä, 2012](#lan12), p. 28). Data collection presents something of a challenge because of the specialised nature of the roleplaying and the intricacies of metagaming. However, the researcher is acknowledged as a respected member of this community of players, and is an active participant in one of the two groups interviewed. While this had disadvantages, especially making assumptions about information practices (_cf._ [Mussett, 2014](#mus14); [Nicholas, 2014](#nic14)), it brought an appropriate level of expertise to the study. For example, illustrative examples provided by participants could be grappled with immediately, rather than requiring extensive additional context. It is also important to recognise the significance of the _invisible rules_ of game playing arising within a group ([Montola, 2009](#mon09)). Any potential difficulties were overcome here by interviewing all members of a group who regularly play together.

Members of two groups were interviewed. One group consisted of experienced players, and the second of less experienced players overall but who had the same gamesmaster as the first group. Twelve participants were interviewed in total. The questions asked were about situations where information was required before the game(s) could proceed, where that information came from, and how it was interpreted. Where participants chose to explore additional topics, this was indulged. The interviews were audio-recorded and subsequently coded for analysis.

The study made use of _thematic_ or _coded_ analysis, as advised in Bryman ([2012](#bry12)). Thematic analysis created through coding is commonplace in studies throughout the social sciences, and is used in some tabletop roleplaying game research. Feedback from people with limited or no knowledge of tabletop roleplaying games ensured that the resulting themes were expressed in language understandable to a wider audience.

This approach does, of course, provide some potentially significant limitations. The researcher is entrenched in the culture: of tabletop roleplaying games generally, and the gaming community (or communities) specifically, including being an active participant in the first community of gamers. This causes the results to have a bias toward the interests and focus of that group’s sense-making practices. The inclusion of the second group, which the researcher was not involved in, was an attempt to provide more rigour. As this group was less experienced, there would also be less emphasis on the rules-heavy systems being used. The other limitation that is worth identifying is that the game system that is most commonly discussed by participants is mechanics-heavy, i.e., it involves a large quantity of mechanical rules, and as a result more potential points of rules-story gaps. As stated above, this is a deliberate choice on the researcher’s part, but may cause the study’s findings to be less applicable to groups who exclusively favour mechanics-light game systems.

</section>

<section>

## Results

Four key themes emerged from the analysis: authority to interpret rules, interrelation of fluff and rules, unreal realism, and social process. (Note that in the interviews, players often refer to the gamesmaster as GM.)

</section>

<section>

### Authority to interpret rules

> [the rule] Book, is a book, it’s just a text, and it has to be interpreted by someone, and that’s the GM although that said I prefer a GM who gains their authority by consulting with the party.

> Player B [a fairly experienced gamer]

> [choosing to ignore rules] has to be normal GM practice. It’s the times when that’s not happening that I don’t enjoy the game so much. When you can see the GM is rigidly adhering to a system when it’s time to break it. ... I always want the system to be moderated for fun. Player A [a highly experienced gamer and gamesmaster]

In asking the participants to describe how they interacted with rule book, there was agreement across the board that the final authority for what rules meant and how they should be applied did _not_ reside in the rule book. Authority to make an interpretation of the official rules lies with the gamesmaster, in relation to the group. As a result, participants are suggesting that the rule by itself, supposedly, the official source of all information required to play the game, does not contain all the information necessary to play the game. The interviews show that for both more experienced and less experienced players, interpreting the rules is a naturalised process; that conversations within a group that can result in the players choosing to reject some part of the official conception of how the fictional reality works happens, as Player A stated, _‘so often as to be ubiquitous’_, further pushing this gaming into the realms of a practice of socially-embedded discourse.

</section>

<section>

### Interrelation of fluff and rules

> You could have the GM, you could have the Rules, but where are the players? If you have the players and the GM, sure, you could do something, but then it’d be a lot of arguments and breakdown of communication because there are no rules to follow. Player D [a fairly experienced gamer, newer to tabletop roleplaying games]

> [when we] Find a gap, identify a short coming in the rules system and we, the people, decide how we wanna do it, I really enjoy that. Player E [experienced]

> I like the limitations of it [the rules]. I’d feel a bit nervous if I was given a bit too much freedom, just because I wouldn’t know where to stop, I wouldn’t know far to go, I wouldn’t know how much I was allowed to be in charge of the story. Player F [almost entirely inexperienced in tabletop roleplaying games]

All respondents agreed that mechanical rules and fluff rules must be balanced and working together for an enjoyable experience: _‘Mechanics and rules have to be working in tandem’_ (Player C). No particular preference was made between systems that made use of a large number of mechanical rules and those that use fewer, other than a tendency toward preferring rule systems in which an acknowledgement was made that fluff rules were just as important to the experience. This supports previous tabletop roleplaying game research: increasingly few significant differences are found in the interactions between players who engage with more-or less-rules-heavy systems, so long as they suit the gaming group’s intentions for the experience ([Bergstrӧm, 2012](#ber12); [Montola, 2009](#mon09), p. 29). Some respondents identified additional elements required to make the two rule sets work, specifically the community’s ability to interpret them, and the game systems allowing it.

</section>

<section>

### Unreal realism

> [there is] no reason to make something impossible unless it sounds unreasonable to what is happening in the story, and the setting, and what is essentially possible in that universe. Player G [highly experienced gamer and highly experienced gamesmaster]

> this is a physical world and these physical constants must apply: [i.e., when a player suggests an action outside the constraints of the ‘reality’] either say, (1) ‘well, right, with the greatest of intentions and the best will in the world, your obviously mentally ill character stands in the park and people laugh and him’ ... option 2(a) is ‘ok, your character blasts up into space [... etc], and then he notices the blood trickling down his head and he realises he has fallen out of a tree’, or option 2(b) is ‘your obviously mentally ill character is now hallucinating and your obviously mentally ill character believes this is happening, play it.’ Player E[experienced]

As respondents were attempting to describe why the combination of rule sets (mechanical and fluff) was important, a common theme that arose was that a fictional setting, despite having entirely unrealistic elements, nonetheless needed to have its own internal logical consistency, it had to feel real from the point of view of people within it. Rules-story gaps were often between what the mechanical rule(s) set out about how the fictional setting worked and what the fluff suggested was happening within that fictional reality.

</section>

<section>

### Social process

> It’s the responsibility of the group as a whole to help the new player learn the rules. Player I [experienced]

> this sort of gaming, in my limited experience, works best when it’s a collaborative experience. Player L [almost entirely new to tabletop roleplaying games]

> We’re using the narrative as a vehicle to learn the rules, and the decisions that we make are pushed by where we want to story to go. Player H [entirely new to tabletop roleplaying games]

Respondents saw the nature of tabletop roleplaying as being about players and a gamesmaster _‘interact[ing] socially with the rules’_ (Player D), even when they had had very little experience with tabletop roleplaying games. A common consideration for newer players was to ensure the rules were taught in a way was engaging, since the mechanical rules for complex systems can easily be rapidly overwhelming.

</section>

<section>

### Metagaming

> I can see why you’d leave metagaming out [of the interview questions], because you’d basically just have [twelve] recordings of people ranting and raving. Player K [a highly experienced player]

> It wouldn’t have added anything, following the rules completely [...]it would have broken the flow too much. Player J [highly experienced as player and gamesmaster]

> The rules are there to break them [but] some rules are needed so everyone knows what’s going on Player J

> [it is] annoying to be bogged down when it’s quite clear you don’t have to be [but] you don’t want to make an exception to the rules more often than you obey them. [...] The rules’ [i.e. the rule book] saying ‘the GM can roll secretly’ is the rules saying to the GMs you may ignore the rules when necessary. Player A [a highly experienced gamer and gamesmaster]

> bad things that happen to your character are really interesting for the [other players]. You have to be aware that the story is about the group as a whole, not about you: you have to be prepared that your character will die for the interest of the story. Player D [a fairly experienced gamer, newer to tabletop roleplaying games]

Metagaming and the use of illicit knowledge were mentioned by all participants. This was not part of the questions asked by the researcher. This is considered one of the few ways to truly break the rules, as it disrupts the realism of the gamers’ shared fictional reality, as discussed. At one level, the discourse of these groups is about how to effectively change one set of rules to make another fit better (usually the official ones, when there is a gap to fill). On a more complex level, the discussion around the rules is one of avoidance: participants speak about making changes to the official mechanical rules in the rules documents, but avoid using the phrase _breaking the rules_ in favour of _ignore_ (Player A), _bend_ (Player C), _adjust_ (Player G), _house-rule_ (Player G), _role-play a situation_ (i.e., instead of using the rules that apply; Players A, C, D, G and J), andso on. Players don’t feel they are truly breaking the rules of the experience, since the practice is to enforce the rule of realism over _both_ the fluff rules and the mechanical ones, in order to create the realism necessary to maintain the experience. Some participants were happy to bend rules but most suggested that rules shouldn’t be ignored entirely either. All players were aware that metagaming was illicit knowledge, and considered against the official and unofficial group rules, to the point where Player K commented on the researcher’s initial hesitation to broach the subject in this research at all. Despite this determination not to use illicit knowledge, several players (including all those who had experience at gamesmastering) felt that publishers were aware of and were actively encouraging players and gamesmasters to adopt a casual attitude to following the rules, if it meant that the story being created was enjoyable.

Similarly, there were occasions when players preferred a harder adherence to the rules to entirely dictate how the story played out, when this was considered the _best practice_. They also valued the rules when players were interacting with other players in a way that could cause one or all to be negatively impacted (up to and especially the deaths of any player’s character). In these cases the rules provided a _‘safer’_ way to agree upon the reality (and outcome) of the situation, even though this approach would extend playing time. When players were interacting with anything else in the fictional reality, the participants have no difficulty with a looser adherence to the rules. Even in cases where players’ characters had been negatively impacted, participants felt it added to the story: watching your friends’ characters suffer inconvenience can be as comical as much as watching them triumph (as Player D’s comment showed).

Simply put, in the eyes of the participants, it is common practice to allow leeway between the mechanics and the setting, as the community of players agree in the moment, and the participants believe that producers of these games acknowledge and agree that the story can inform the mechanics applied, and the application of rules can allow the story to progress in a more satisfying way. Rather than adopting one or the other, the experience of the game can use either or both approach where useful. This pattern informs and leads into the next, where the division between hard adherence to rules or fluff is further eroded.

</section>

<section>

### Role-player and roll-player

> Everything you’re doing that is taking away from actually playing the game is hurting your eventual experience of that game. Player F [almost entirely inexperienced in tabletop roleplaying games]

> [games are] attempting to approximate the action that is transpiring with a reality of the imagination. Player L [almost entirely new to tabletop roleplaying games]

> rules and setting [interpreted by] the GM create what you think you might expect in the game. Player A [a highly experienced gamer and gamesmaster]

Tellingly, the discourse of _role player_ vs. _roll player_ was touched on by every single participant, even players with little to no previous experience beyond a handful of game sessions. The key message that emerged was that the categorisation into role playing and roll playing creating a false dichotomy and that to contribute to an enjoyable and collaborative experience, it was necessary to adopt both approaches either individually or within the group as a whole.

</section>

<section>

## Discussion

</section>

<section>

### Roleplaying games literature

The common areas of research in tabletop roleplaying games involve how players interact with each other, the official rules documents, and how players inhabit the reality that is created. As mentioned above, the prevailing theory of how players approach tabletop roleplaying games is as one of two extremes, roll-players and role-players ([Brackin, 2012](#bra12); [Evans, 2012](#eva12); [Kim, 2003](#kim03); [Rose 2012](#ros12)). In this binary, those playing the game for the tactical-mathematical-logical challenge (roll players) will be tempted to metagame to use their considerable understanding of the rules for advantage their character should not possess. By contrast, those interested in tabletop roleplaying games as a means of improvisational acting (role players) will be tempted to metagame by avoiding mechanical rules entirely, content to allow their characters to perform actions that aren’t possible to those attempting to maintain the reality provided to the group through the mechanical rule set. Tabletop roleplaying game research has traditionally viewed these two approaches to engaging with a roleplaying game as being in opposition to each other. The only tabletop roleplaying game research to indicate that the two extremes might have defined middle ground is the Forge group ([Kim, 2003](#kim03)), which acknowledges the group as a third element. There is some tabletop roleplaying game research that acknowledge the presence of both elements ([Frasca, 2003](#fra03)), but only in the context of agreeing that either approach has value; or identifying that either approach provides the opportunities for metagaming.

However, the findings of this study have shown that the players interviewed have ongoing interest in ensuring that both their character and the group are all able to exist between these two poles. Players have to engage with both kinds of reality in order to have a meaningful experience. Indeed, the division is not a continuum that players place themselves on, but more like sets of tools that are used interchangeably in order to create the most realistic, enjoyable experience, whatever allows the group to collectively bridge rules-story gaps. Even players new to the experience have noticed rules-story gaps in tabletop roleplaying games, and part of their experience with finding enjoyment in the genre of game is to find _helps_ for these _gaps_, to use Dervin, Harpring and Foreman-Wernet’s ([1999](#der99)) terminology. This has led the researcher to question the validity of the dichotomy between narratology and ludology that is present in literature around tabletop roleplaying games.

The other noticeable difference between the current research and previous research in tabletop roleplaying games is the themes arising. While many pieces of research, such as Greifender and Ostrander’s [2008](#gre08) study, have found that players interact with information in unusual ways, it was typically in establishing a single accepted reality and ensuring it was static. Their study was organised by the themes _‘user-created content’_, _‘validity’_, and _‘authority’_ of information (pp. 514-5). Players interviewed in the current study identified the sense-making process of establishing a shared reality as an ongoing, socially-based experience, and that the shared fictional reality was only static until the next piece of information was introduced (that is, until the story continued). In short, their sense-making process was ongoing, and due to its discursive nature, being literally carried out through patterns of spoken language over time, is perforce socially experienced. However, the study did find that information from _official_ sources (the sourcebooks, the gamesmaster) took precedence over most other sources, so validity of information was certainly upheld.

Another factor commonly supported by tabletop roleplaying game literature and supported by these findings is that the fundamental goal of the tabletop roleplaying game is leisure. While there is contention around whether _fun_ can be considered the primary focus (Mäkelä, _et al._, 2005, cited in [Montola, 2009](#mon09), 26), there is consensus that tabletop roleplaying games are a leisure activity ([Greifender and Ostrander 2008](#gre08)). The findings of this study support this, and show that the sense-making process of co-creating reality is engaged with as long as it brings pleasure to the participants. Additionally, a breakdown of the reality established can cause conflict (i.e., a lack of pleasure). As Players D and E pointed out, it is important that enjoyment of the experience extends beyond any individual player. Both the rules and story work together to produce a more satisfying experience, which is the motive for engaging in sense-making dialogue and the ultimate goal of the experience.

</section>

<section>

### Sense-making

However, there is more to the sense-making process as observed in tabletop roleplaying game groups than the two common elements of mechanics and fluff. A multitude of additional elements require the attention of the players, individually and socially in the context of a group. These include: the players’ intentions for their characters (such as what they ought to be capable of or what they ought to know), and how well that can be translated into the mechanical and fluff reality (realities) provided; how the group and the gamesmaster agree to create the fantastic reality; and how the characters interact with each other. The process of aligning the various disparate elements that make up a tabletop roleplaying game is a sense-making process. The method used to bring the elements together is a discursive one: when players are re-creating their shared fantastic reality, the process is discursive. When players engage with that reality through their characters, it occurs through description. Even when there is a gap between any elements, it is expressed through dialogue, such as in the hypothetical example supplied by Player E.

Beyond observing sense-making in tabletop roleplaying games, sense-making occurs here in the context of leisure, and establishing reality. There is an increasing body of work in information research showing that leisure activities involve as much interaction with information as more formalised activities (for example, [Savolainen, 1995](#sav95) or [Prigoda and McKenzie, 2007](#pri07)). In this study, the activity observed has leisure as the agreed goal and has a highly complex relationship to information: formal, informal, illicit, individual, communal, from multiple different elements drawn together as a continual process of co-creation through sense-making. This study would support the concept that leisure activities can be more richly understood through the lens of information research, and vice versa.

</section>

<section>

## Conclusion

While by no means exhaustive, the results of the research show a clear connection between information research, in particular sense-making, and the practice of playing tabletop roleplaying games.

Gaming groups develop a highly complex and at times almost opposing viewpoint to the officially codified routines and practices of the official system (the rules documents) with the intent of improving on the official system. In analysing the discursive frameworks surrounding the community that the research participants belong to, the researcher has been able to determine that the information from the official rule books concerning the setting and context to tabletop roleplaying games is as useful to these groups as the mechanical rules structures in creating and maintaining realism in a fantasy setting; something to be borne in mind where further additions to the genre are made.

The results also show that the process of aligning different, often contradictory, elements contributing to a collective shared reality (a process that is one of the primary practices of the tabletop roleplaying game community) is a sense-making process. The discursive frameworks identified all revolve around how to make not just mechanical and fluff elements but also the players’ and the group’s understanding connect in a satisfactory way, literally making sense of often disparate elements. While sense-making is classically restrained to the world in which we live, the fact that the players are in the process of co-creating a fictional reality makes sense-making all the more applicable: it is _only_ through complex interaction with the rules books and each other that the gaps between their understandings can be resolved: tabletop roleplaying games _require_ sense-making to function.

Indeed, the commonly held assumption that tabletop roleplayers are one of two groups, roll-players or role-players, is less a question of which _side_ an individual leans toward and more a question of how an individual uses elements of both to create an enjoyable experience, and how this is further shaped by the context of the system and the gaming group. Whether this is attributable to this group’s context, i.e. gamers’ being sufficiently aware of the discourse that its framework has changed from being about one or other to how to enjoy both, or the contrast not being as prominent as previously suggested remains a question this study cannot answer, but can only be posed to the wider gaming community.

From the point of view of information research, this study can be added to the growing body of research that uses sense-making as a framework to explore leisure-related activities, in order to develop a richer understanding of the subject matter. In this case, that wider picture allows for the commonly held theory of the role-roll player divide to be brought into question.

</section>

<section>

## Acknowledgements

This article is dedicated to:  
All my gaming friends, without whom I would have no subject to research. Drs Michael Olsson, Bhuva Narayan, Maureen Henninger and Hilary Yerbury, without whom I would not have had the opportunity to conduct research. The invaluable aid of the anonymous reviewers, in particular in framing the breakdown of ludologist v narratologist dichotomy. The editorial prowess of Dr Amanda Cossham, without whom this paper would be riddled with embarrassing mistakes.

This research was originally conceived as part of the author’s undertaking a Master of Arts course in Information Knowledge Management at the University of Technology, Sydney, and developed for the 2016 RAILS conference.

</section>

<section>

## <a id="author"></a>About the author

**Alex Wylie Atmore**currently works at the University of Western Sydney, in the field of IT Assurance, following completion of a Master of Arts in Information Knowledge Management at the University of Technology, Sydney. In his spare time, he is either playing tabletop roleplaying games with friends, or having long conversations about tabletop roleplaying games with friends. Alex can be contacted on his personal e-mail account, [a.wylieatmore@gmail.com](mailto:a.wylieatmore@gmail.com).

</section>

<section>

## References

<ul>
<li id="ada05"> Adams, S. (2005). <em><a href="http://www.webcitation.org/6ulasc5Vu">Information behavior and the formation and maintenance of peer cultures in  massive  multiplayer  online  role-playing  games: a case  study  of  city  of  heroes.</a></em> Paper   presented   at   the   DiGRA   Conference.
Retrieved from https://pdfs.semanticscholar.org/c946/74e0dc23d3cbe4e6fe61b2b2017df3f83797.pdf.  
(Archived by WebCite&reg; at http://www.webcitation.org/6ulasc5Vu)</li>
<li id="bat12"> Bateman, C. (2012). The rules  of imagination. In  J.  Cogburn  &amp;  M.  Silcox  (Eds.),<em>Dungeons and Dragons and philosophy: raiding the Temple of Wisdom</em>  (pp. 225-238). Chicago, IL: Open Court Publishing.
</li><li id="bea07"> Beattie, S. (2007). Voicing the shadow: rule-playing and roleplaying in Wraith: the oblivion. <em>Law, Culture and the Humanities, 3</em>(3), 477-492. </li>				
<li id="ber12"> Bergström, K. (2012). <a href="http://www.webcitation.org/6uleNlVVb">Creativity rules: how rules impact player creativity in three tabletop role-playing games.</a> <em>International  Journal  of  Role-playing,  3,</em> 4-17.
Retrieved from http://www.ijrp.subcultures.nl/wp-content/issue3/IJRPissue3bergstrom.pdf. 
(Archived by WebCite&reg; at http://www.webcitation.org/6uleNlVVb)</li>
<li id="bra12"> Brackin, A. (2012). You got your gameplay in my role-play! In J. Cogburn &amp; M. Silcox (Eds.), <em>Dungeons and Dragons and philosophy: raiding the Temple of Wisdom</em> (pp. 239-253). Chicago, IL: Open Court Publishing. </li>
<li id="bra15"> Brand, J. E., Lorentz, P., &amp; Mathew, T. (2015). <em><a href="http://www.webcitation.org/6VsahDXsb">Digital  Australia  2014.</a></em>  
Retrieved from http://igea.wpengine.com/wp-content/uploads/2013/11/Digital-Australia-2014-DA14.pdf. 
(Archived by WebCite&reg; at http://www.webcitation.org/6VsahDXsb)</li>
<li id="bry12"> Bryman, A. (2012). <em>Social research methods.</em> Oxford: Oxford University Press. </li>
<li id="cog12"> Cogburn, J., &amp;  Silcox, M. (2012). <em>Dungeons and Dragons and philosophy: raiding the Temple of Wisdom</em> (Vol. 70). Chicago, IL: Open Court Publishing.</li>
<li id="der91"> Dervin, B. (1991). Information as non-sense; information as sense: the communication technology connection. <em>Tussen vrag end Aanbod,</em> 4-59.</li>
<li id="der92"> Dervin, B. (1992). From the mind's eye of the 'user': the sense-making qualitative-quantitative methodology. In J.D. Glazer &amp; R.R. Powell (Eds.), <em>Qualitative research in information management</em> (pp. 61-84). Englewood: CO: Libraries Unlimited. 
</li><li id="der99"> Dervin, B., Harpring, J. E., &amp; Foreman-Wernet, L. (1999). <a href="http://www.webcitation.org/6hlQzAVtz">In moments of concern: a sensemaking study of pregnant, drug-addicted women and their information needs.</a> <em> The Electronic Journal of Communication, 9</em>(2).
Retrieved from http://www.cios.org/EJCPUBLIC/009/2/00927.html
(Archived by WebCite&reg; at http://www.webcitation.org/6hlQzAVtz)	</li>			 
<li id="det13"> Deterding, S., &amp; Zagal, J. P. (2013). <em><a href="http://www.webcitation.org/6ulbvUD7x">Roleplaying game studies: a handbook.</a></em> Paper presented at theDiGRA Conference.
Retrieved from http://homes.lmc.gatech.edu/~cpearce3/DiGRA13/papers/paper_32.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6ulbvUD7x)	</li>			
<li id="ehr12"> Ehrett, C., &amp; Worth, S. (2012). What <em>Dungeons and Dragons</em> is and why we do it. In J. Cogburn &amp; M. Silcox (Eds.),<em> Dungeons and Dragons and philosophy: raiding the Temple of Wisdom</em> (pp. 195-205). Chicago, IL: Open Court Publishing.
</li><li id="eva12"> Evans, M. (2012). The secret lives of elven paladins. In J. Cogburn &amp; M. Silcox (Eds.), <em>Dungeons and Dragons and philosophy: raiding the Temple of Wisdom</em> (pp. 179-192). Chicago, IL: Open Court Publishing.</li> 	
<li id="fra03"> Frasca, G. (2003). <em><a href="http://www.webcitation.org/6ulc3TM35">Ludologists love stories, too: notes from a debate that never took place.</a></em> Paper presented at the DIGRA Conference 2003: 'Level Up'. 
Retrieved from http://www.ludology.org/articles/frasca_levelUP2003.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6ulc3TM35)	</li>			
<li id="gre08"> Greifender, E., &amp; Ostrander, M. (2008). Talking, looking, flying, searching: information seeking behaviour in Second Life. <em>Library Hi Tech, 26</em>(4), 512-524. </li>
<li id="gyg74"> Gygax, G., &amp; Arneson, D. (1974). <em>Dungeons &amp; Dragons.</em> Lake Geneva, Switzerland: TSR.
</li><li id="har09"> Harviainen, J. T. (2009). <a href="http://www.webcitation.org/6ule1JQBh">A hermeneutical approach to role-playing analysis.</a> <em>International Journal of Role-playing, 1</em>(1), 66-78.     
Retrieved from http://www.ijrp.subcultures.nl/wp-content/uploads/2009/01/harviainen_hermeneutical_approach_to_rp_analysis.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6ule1JQBh)	</li>				
<li id="huv13"> Huvila, I. (2013). <a href="http://www.webcitation.org/6uldvTQHg">Meta-games in information work.</a> <em>Information Research, 18</em>(3), paperC01. 
Retrieved from http://InformationR.net/ir/18-3/colis/paperC01.html
(Archived by WebCite&reg; at http://www.webcitation.org/6uldvTQHg)	</li>			
<li id="kim03"> Kim, J. H. (2003). <em><a href="http://www.webcitation.org/6PpFtEddB">Story and narrative paradigms in role-playing games.</a></em>
Retrieved from http://www.darkshire.net/jhkim/rpg/theory/narrative/paradigms.html
(Archived by WebCite&reg; at http://www.webcitation.org/6PpFtEddB)	</li>							
<li id="lan12"> Lankoski, P., &amp; Järvelä, S. (2012). <a href="http://www.webcitation.org/6uldahlmk">An embodied cognition approach for understanding role-playing.</a> <em>International Journal of Role-playing, 3,</em> 18-32.
Retrieved from http://www.ijrp.subcultures.nl/wp-content/issue3/IJRPissue3lankoskijarvella.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6uldahlmk)	</li>				
<li id="mon09"> Montola, M. (2009). <a href="http://www.webcitation.org/6uldRCNnO">The invisible rules of role-playing: the social framework of role-playing process.</a> <em>International Journal of Role-playing, 1</em>(1), 22-36.
Retrieved from http://www.ijrp.subcultures.nl/wp-content/uploads/2009/01/montola_the_invisible_rules_of_role_playing.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6uldRCNnO)	</li>	
<li id="mus14"> Mussett, S. M. (2014). Berserker in a skirt: sex and gender in Dungeons &amp; Dragons. In C. Robichaud (Ed.), <em>Dungeons and Dragons and philosophy: read and gain advantage on all wisdom checks</em> (pp. 189-201). Chichester: Wiley Blackwell.
</li><li id="nic14"> Nicholas, J. J. (2014). ‘Others play at dice’: friendship and Dungeons &amp; Dragons. In C. Robichaud (Ed.), <em>Dungeons and Dragons and philosophy: read and gain advantage on all wisdom checks.</em> Chichester: Wiley Blackwell.
</li><li id="pri07"> Prigoda, E., &amp; McKenzie, P. J. (2007). Purls of wisdom: a collectivist study of human information behaviour in a public library knitting group. <em>Journal of Documentation, 63</em>(1), 90-114. </li> 
<li id="ros12"> Rose, J. (2012). The gunpowder crisis. In J. Cogburn &amp; M. Silcox (Eds.), <em>Dungeons and Dragons and philosophy: raiding the Temple of Wisdom,</em> (pp. 265-278). Chicago, IL: Open Court Publishing. </li>
<li id="sav95"> Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of ‘way of life’. <em>Library &amp; Information Science Research, 17</em>(3), 259-294. </li>
<li id="sav07"> Savolainen, R. (2007). Information behavior and information practice: reviewing the ‘umbrella concepts’of information-seeking studies. <em>Library Quarterly, 77</em>(2),  109-132. </li> 
<li id="whi14"> White, W. J. (2014). Player-character is what you are in the dark: the phenomenology of immersion in Dungeons &amp; Dragons. In C. Robichaud (Ed.), <em>Dungeons and Dragons and philosophy: read and gain advantage on all wisdom checks</em> (pp. 82-92). Chichester: Wiley Blackwell. </li>					
<li id="woo12"> Woods, S. (2012). <em> Eurogames: the design, culture and play of modern European board games:</em> Jefferson, NC: McFarland.</li> 	
<li id="xuy11"> Xu, Y., Barba, E., Radu, I., Gandy, M., &amp; MacIntyre, B. (2011). <em><a href="http://www.webcitation.org/6uld31HU8">Chores are fun: understanding social play in board games for digital tabletop game design.</a></em> Paper presented at the Think design play: The fifth international conference of the digital research association (DIGRA).
Retrieved from http://www.digra.org/wp-content/uploads/digital-library/11307.16031.pdf
(Archived by WebCite&reg; at http://www.webcitation.org/6uld31HU8)	</li>					
</ul>

</section>

</article>