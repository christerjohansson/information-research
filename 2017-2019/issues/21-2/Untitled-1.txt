<article xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" locale="en_US" date_submitted="2010-09-01" stage="submission" date_published="2010-09-01" section_ref="ART" seq="1" access_status="0">
      <id type="internal" advice="ignore">554</id>
      <title>I just want more information about who I am: the search experience of sperm-donor offspring, searching for information about their donors and genetic heritage
</title>
      <abstract locale="en_US">This paper discusses the findings of a qualitative study of sperm-donor offspring conceived in the United States who have searched for information about their donors and genetic heritage. It explores how these individuals search for information and the characteristics of such searches.<br>
</abstract>

      <author include_in_browse="true" user_group_ref="Author">
          <firstname>Amber L. </firstname>
          <lastname> Cushing</lastname>
          <affiliation locale="en_US">School of Information and Library Science, University of North Carolina
</affiliation>
          <country>US</country>
          <email>info@hb.se</email>
        </author>
      </authors>


      <submission_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="submission" id="1834" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
      <revision number="1" genre="Article Text" filename="Paper428.html" viewable="false" date_uploaded="2010-09-01" date_modified="2019-09-11" filetype="text/html" uploader="christerjohansson">
          <name locale="en_US">I just want more information about who I am: the search experience of sperm-donor offspring, searching for information about their donors and genetic heritage
</name>
          <href src="https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/
          15-3/Paper428.html"> 
          </href>
        </revision>        
      </submission_file>

       <supplementary_file xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" stage="proof" id="1835" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
        <revision number="1" genre="Other" filename="p428fig1.png" viewable="false" date_uploaded="2010-09-01" date_modified="2019-09-11" filetype="image/gif" uploader="christerjohansson">
          <name locale="en_US">p428fig1.png</name>
          <href src="https://bitbucket.org/christerjohansson/information-research/raw/master/2017-2019/issues/15-3/p428fig1.png"></href>
        </revision>
        <publisher locale="en_US">Tom Wilson</publisher>
        <date_created>2019-06-01</date_created>
        <source locale="en_US">University of Bor&#xE5;s</source>
      </supplementary_file>

      GALLEY

      <article_galley xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" approved="true" xsi:schemaLocation="http://pkp.sfu.ca native.xsd">
        <id type="internal" advice="ignore">1828</id>
        <name locale="en_US">Paper428.html</name>
        <seq>0</seq>
        <submission_file_ref id="1834" revision="1"/>
      </article_galley>
    </article>
