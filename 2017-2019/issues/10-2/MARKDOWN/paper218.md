#### Vol. 10 No. 2, January 2005



# Speaking of users: on user discourses in the field of public libraries

#### [Åse Hedemark](mailto:ase.hedemark@abm.uu.se), [Jenny Hedman](mailto:jenny.hedman@hb.se), [Olof Sundin](mailto:olof.sundin@hb.se)
#### The Swedish School of Library and Information Science  
The University College of Borås & Göteborg University  
Högskolan i Borås, 501 90 Borås, Sweden

#### Abstract

> **Introduction**. The aim of the study reported is to examine user discourses identified in the Swedish public library field. The following questions are posed: What user discourses can be found and what characterises them? How are users categorised and what does this categorisation imply? The departure point in this paper is that the ways users are categorised may influence their information behaviour. Plausible consequences for the relation between the interest of the public library and the users are discussed.  
> **Method**. The empirical focus of the paper is a discourse analysis with a starting-point in Ernesto Laclaus and Chantal Mouffe's discourse theory.  
> **Analysis**. Sixty-two articles from three established Swedish library journals are analysed through a model in four phases. These phases include designations of users, user categories, themes within which users are described and user discourses.  
> **Results**. Four user discourses are revealed: a general education discourse, a pedagogical discourse, an information technology discourse and an information management discourse.  
> **Conclusion**. The discourses hold both levels of idealizing and experience-related rhetoric. The dominant general education discourse is based on a tradition of fostering and refining as well as educating the general public and thereby reproduces inequality between the user and the library.


## Introduction

The departure point in this paper is that the ways in which _the user_ is categorised affect information needs, seeking, and use. This categorisation may have consequences for users' information behaviour, for reference interviews, and in user education. One important arena for the production, reproduction and mediation of different views of the user is through professional journals. The empirical focus of the paper is on the categorisation mediated in Swedish professional library journals.

A discourse analysis was undertaken of articles from three established Swedish library journals between January 1999 and February 2002 that, in various ways, discuss users in public libraries. The aim of this paper is to examine the user discourses that can be identified in the Swedish public library field. The questions posed are:

*   How are users categorised within public library journals?
*   What does this user categorisation imply?
*   What user discourses can be found?
*   What characterises these discourses?

Laclau and Mouffe's ([1985](#Lac85)) approach to discourse analysis is used in order to answer these questions. A number of articles within librarianship and information science have focused on the methodological consequences of discourse analysis (e.g., [Sundin 2000](#Sun00); [Talja 1997](#Tal97); [Talja, _et al._ 1999](#Tal99)). Discourse analysis has also been used empirically to study how central concepts, such as _users_, _information_ and _intermediaries_ have been constructed within the discipline (e.g., [Day 2000](#Day00); [Tuominen 1997](#Tuo97a); [Tuominen & Savolainen 1997](#Tuo97b)). Furthermore, discourse analysis has been used to examine how information seeking and use can be studied as collective processes within various institutions (e.g., [Given 2002](#Giv02); [Talja 2001](#Tal01)), but the theoretical discourse of discourse analysis is still by far stronger than empirical investigations. We hope that the empirical investigation presented here will somewhat redress this situation. The paper begins with an introduction to discourse theory. Thereafter, the selection of texts from three Swedish library journals is described and followed by a presentation of how the analysis has been carried out. In the empirical findings four user discourses are revealed: a general education discourse, a pedagogical discourse, an information technology discourse and an information management discourse. Among the first three a discrepancy is noticed between the idealizing and experience-related aspects of the discourses. In the conclusion, the practical consequences of these aspects for public library users' information behaviour are discussed.

## Discourse theory

Discourse theory encompasses assumptions about the part of language in the social construction of the world, as well as theoretical and methodological models for an analytic procedure. A discourse can be seen as a certain way of speaking about and understanding the world and discourses manifest themselves primarily in spoken or written statements about a certain idea, theme or phenomenon ([Winther Jørgensen & Phillips 2002](#Win02): 1-3). The contents of texts are not regarded as information about the reality to which it refers, instead texts are analysed in order to reveal the underlying structures, assumptions or discourses. The prime object is not to judge whether the content of a text is true or not; the focus lies rather on identifying and understanding the discourse (or discourses) that constitute a foundation for the assumptions expressed in the text. No claim is raised to analyse the frequency of specific terms or discourses; this kind of analysis is essentially qualitative.

The discourse theory of Laclau and Mouffe ([1985](#Lac85)) advocates a wide definition of the discourse concept and claims that discursive practices comprise _all_ social practice. A discourse is, according to them, a fixation of meaning within a certain area and the purpose of discourse analysis is to map the processes whereby we struggle to fix the meaning of signs ([Winther Jørgensen & Phillips 2002](#Win02): 24-25). A _sign_ implies a combination of a word's expression and content. The sign _user_ for example, comprises both the written word and ideas about users. The aim of this analysis is to study these processes of creating meaning. Signs that are constantly the subject of a struggle over various meanings are called _elements_, exemplified in this study by different user categories such as children, youth and neglected groups. These elements all have various meanings that are not clearly defined. The opposite of elements is _moments_, signs whose meanings are fixed. The elements children, youth and neglected groups become moments when their meanings are determined within the field of the public library. This fixation is part of what we study in this analysis. A discourse means, thus, that ambiguity is reduced to unambiguity—the elements become moments ([Laclau & Mouffe 1985](#Lac85): 105). This reduction of possibilities is characterised by what it excludes. Laclau and Mouffe maintain '...to be something is always not to be something else ' ([Laclau & Mouffe 1985](#Lac85): 128). Some words are particularly open to different identities and these are called _floating signifiers_ ([Winther Jørgensen & Phillips 2002](#Win02): 28). In this paper _user_ is regarded as a floating signifier. The struggle for creating meaning is a recurrent theme in discourse theory and the concept for conflict is _antagonism_. Antagonism occurs when two possible identities mutually exclude each other, in other words, where discourses collide ([Winther Jørgensen & Phillips 2002](#Win02): 47-48). Laclau and Mouffe emphasise that the inner antagonism of discourses takes an active part in meaning creating processes ([Laclau & Mouffe 1985](#Lac85): 122-127).

## Data selection

The selected texts were published in _Bibliotek i Samhälle_ [Library in Society], _Biblioteksbladet_ [The Library Newspaper], or _Ikoner_ [Icons] – three leading Swedish professional library journals. All texts were published between January 1999 and February 2002 and were chosen because they all explicitly deal with users and public libraries. The delimitation in time was made with respect to balancing a demand for topicality with a reasonable number of texts. After a preliminary examination, sixty-two texts were selected that proved to enrich the existing body of information. The selection procedure was deemed sufficient to give a fair picture of the public libraries' user discourses as they are put into words in Swedish library journals.

## Analysis

Laclau and Mouffe primarily write _about_ discourse theory but they give no clear directions on how to empirically proceed with it. Therefore, their theory and its terminology are used in accordance with our own interpretation. In the analysis, chains of equivalence are identified; that is, associative relations connecting _elements_ and _floating signifiers_ in a text. This entails that signs that can be connected to each other by association are brought out and listed. We also analyse how different user discourses compete over how to divide the overall user group according to various criteria, and how these discourses struggle to give the floating signifier _user_ different meanings by relating it to different elements. In the texts users are almost always categorised into smaller groups, characterised by variables such as sex, age and ethnicity. Thus, the analysis is twofold; we examine what categories and themes in the texts can be linked to users and also what characteristics and phenomena within the chains of equivalence can be associated to these elements. A reading scheme was created to serve as an analysis tool (Figure 1).

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Figure 1\. The reading scheme**</caption>

<tbody>

<tr>

<td align="left" valign="top" width="50%">

<div style="text-align : center ; line-height : 2 em">**1**  
_Designations_  
</div>

<div style="text-align : left">User designations found in the articles</div>

</td>

<td align="left" valign="top" width="50%">

<div style="text-align : center ; line-height : 2 em">**2**  
_Categories_  
</div>

<div style="text-align : left">Meanings attributed to the various user designations</div>

</td>

</tr>

<tr>

<td align="left" valign="top" width="50%">

<div style="text-align : center ; line-height : 2 em">**3**  
_Themes_  
</div>

<div style="text-align : left">Themes and contexts within which users are being discussed and how it is done</div>

</td>

<td align="left" valign="top" width="50%">

<div style="text-align : center ; line-height : 2 em">**4**  
_Discourses_  
</div>

<div style="text-align : left">Conceptions and meanings as a whole</div>

</td>

</tr>

</tbody>

</table>

According to our interpretation of Laclau and Mouffe ([1985](#lac85): 112-113), it is important to identify the central elements of the discourse in an initial stage of the analysis. This corresponds to _square 1_ in Figure 1\. In the present study the elements consist of different designations of users. The next stage of the analysis is to examine how different signs can be associated with elements such as user categories, _square 2_. Chains of equivalence illustrate the meanings of elements. It is a matter of distinguishing the characteristics and words that are associated with different user designations.

In _square 3_, the context of concepts and themes is examined to see how users are reasoned and argued about. A text can hold contradictions and conflicting statements and it is within this struggle over meaning that antagonism can rise. The analysis both starts and ends in _square 4_; a first impression is gained of the discourses within the subject. This initiates analytical excursions into the other squares, where concepts and themes central to the discourse are examined. Finally, back in square 4 initial presumptions are partly confirmed and revised.

## Results

After reading the texts it was evident that the way those in the public library field regard their users reflects the way the institution looks at its own public role and duty. There should be _access for all_, the library should be _a meeting place for everyone_ and the librarian should _serve everybody_ [[1](#note1)]. These statements all originate from the traditional idea of the public library as a forum for general education in a democratic society. This can be seen as the _politically correct_ idea of the public library today. Analysing the texts more carefully, a complex and more problematic reasoning appears concerning these _everybody_ that the public libraries are meant to serve.

### Designations (Square 1)

One way of designating the users is to talk about them in terms of _the public_, _the local citizens_, _the subjects_ and _the people_, which usually refers to the whole target group of the library, that is _everybody_. However, the most frequent designations for users are _visitors_ and _borrowers_. Other common denominations are _users_ and _customers_. Besides these kinds of somewhat anonymous designations, more specific ones are found in the texts, such as different categories within the overall target group. For example, _distance students_, _little children_, _the new Swedes_, _seniors_, _lost Internet surfers_, _the disabled_, _the lower classes_, _the regulars_, _dyslectics_ and _those with an information disability_. It is noticeable that when individuals appear in the texts, it is often as illustrative examples. It may be everything from _a middle-aged, red-faced man_, _a skinhead_ and _a university professor_ to _little Tamara_. Other less spectacular ways of putting it are _a curious child_, _the old man_, _an information seeker_ and _the reader_.

Various institutions can also be found among the user designations in this study. The analysed texts hold formulations about the way in which _the market_, _organisations_, _private schools_ and _The Adult Education Programme_ [Kunskapslyftet] [[2](#note2)] make demands upon public libraries. Still another way of referring to users is in the form of metaphors. This occurs where the user is understood or implied in talk about _book lending_, _visits_ or where a certain _need_ or _will_ is supposed to be at hand. Sometimes users are mentioned in the form of statistical figures, and are thereby transformed into clearly measurable units.

### Categories (Square 2)

The user category _youth_ is often described as rather troublesome. The texts tell us that young people are indeed important and valuable, but nevertheless quite disturbing:

> Then there are other problems. Lads aged fifteen to thirty for example. Formerly, they used to be a problem because they didn't come to the libraries, now they're a problem because they're too many. The libraries Internet boxes are being annexed and used for chat and mail while the librarians keep sighing anxiously about information seeking.  
> [_[Bibliotek i samhälle](#SupRef1)_ (1)]

There are also a limited number of texts where young people are not related to negatively, but these more positive expressions about young users are, by far, in the minority. The following chain of equivalence can be built starting with the element _youth_:

>  - too many  
> - disturbing  
> - noisy, loud  
> - boys  
> - Internet  
> - information technology

Mostly boys or lads are mentioned as being noisy and disturbing and they also form associations with Internet surfing on inappropriate Websites.

Young children are explicitly considered to be an important user group. Sometimes the distinction between children and youth is slightly vague, but there is still a difference between the words linked to each category. The need for protection, education and control becomes salient in topics concerning very young users, especially in relation to the Internet.

> Children's Internet usage in the library calls for reflection. As adults we have—as in every situation where children are involved—a common responsibility to help, guide and superintend. There is no reason to allow chat and games to dominate children's Internet usage at the library.  
> [ [Bibliotek i samhälle](#SupRef2) (2)]

The following chain of equivalence is linked to the element _children_:

> - protection (from totally free information)  
> - learning  
> - supervision  
> - guidance

Another user category mentioned quite often is _mature students involved in The Adult Education Programme_. It recurs frequently and, as far as we can see, it is solely associated with problems and supposed lack of intellectual capacity:

> In almost every second town one has had to cut back other library activities to be able to provide service for mature students.  
> [[Bibliotek i samhälle](#SupRef3) (3)]  
>   
> Also, the level needs to be adapted to the borrower, you can't throw a doctor's thesis at someone on the Adult Education programme etc.  
> Also, the level needs to be adapted to the borrower, you can't throw a doctor's thesis at someone on the Adult Education Programme etc. [[Biblioteksbladet](#SupRef4) (4)]

The chain of equivalence linked to _mature students in The Adult Education Programme_ takes the following form:

> - resource demanding (at other users' expense)  
> - untrained/inexpert  
> - great need for help  
> - time-consuming

Still another category is perceived, called _neglected groups_. Even though this expression may seem somewhat out-of-date, we still think that it could work as a heading to this heterogeneous category. The different user groups assembled into this category have at least one thing in common; they are all attributed a certain victim status in the texts:

> Indignant librarians and dedicated cultural workers call attention to the fact that little children don't have computers and neither do poor homeless people, not to mention immigrants and mentally or physically handicapped.  
> [[Ikoner](#SupRef5) (5)]

Several notions are jointly associated with the element _neglected groups_, forming the subsequent chain of equivalence:

> - immigrants  
> - those tired of school  
> - elderly people  
> - handicapped  
> - no computers of their own  
> - don't lay any claims themselves  
> - reading and writing disability

### Themes (Square 3)

A number of themes are identified throughout the texts, reflecting different views of users. The first theme is focused on the functions, visions and potential of the public library: _The Mission_. The next theme, _Internet Usage_, has highly the character of a debate, especially as the question of whether there should be Internet filtering or not in public libraries has been discussed. Within the third theme the library's relation to its users is described as _The Pedagogical Role_ of the public library.

### Discourses (Square 4)

In this section the different discourses articulated in the texts are presented. In order to identify these discourses the texts have been read repeatedly and examined according to both their explicit and implicit contents. The overall discourses thus emerged in a hermeneutic process through deconstruction (designations, categories, themes) to reconstruction (discourses). The focus has been to encompass the ways users are being looked upon within the discourses. Discourses do not exist in a pure, isolated form, they are, rather, intertwined; both within the themes and within separate texts. Four discourses have been identified: the _General Education Discourse_, the _Pedagogical Discourse_, the _Information Technology Discourse_ and the _Information Management Discourse_.

#### The General Education Discourse

The public library, as an institution, is strongly characterized by its political and ideological origins. Torstensson ([1995](#Tor95)) writes that, at the same time as Swedish public library development gained momentum, it was advantageous to the working class movement to invest in education. This became a way of integrating into society and at the same time increasing influence on it. Understanding education as the foundation of the general education idea, it is still being reproduced within the general education discourse. The view of knowledge embedded in these ideas is based on the assumption that reason and scientific thinking lead to well being for individuals as well as for society (cf., [Vestheim 1997](#Ves97)).

A quotation from one of the articles illustrates a common way of describing the resources of the library as an important asset for under-privileged people. The information held by the library is described as crucial to people with limited economic and educational resources.

> Therefore, it is of the utmost importance, if one still cherishes the ideals of general education and the view of democracy of the old society, to also provide new information resources, evenly distributed within society, including groups of neglected citizens. [[Ikoner](#SupRef6) (6)]

In the texts, there is a common understanding that public libraries should serve _everybody_. The fact that users often are designated as citizens with certain rights can be seen as evidence for a political rhetoric that is deeply rooted in the idea of general education. Within this discourse users are not spoken of as individuals, but mentioned in more general terms, for example as _the public at large_ and _citizens_. When the actual library work is described there is a tendency to use categorisations such as those referred to in square 2\. The analysis also exposes contradictions, especially between the way politically-rooted visions are expressed and how users are referred to as categories. There seems to be a distinct discrepancy between what is spoken of in an accepted, politically correct way, and what is said out of one's own experience. The general education discourse therefore holds both idealizing and experience related aspects.

Talja ([2001](#Tal01)) points out that the democratic idea behind the library as an educational institution, in reality, is something of an illusion. She describes how an elite in society created an institution where the common people were offered education and refinement through taking part in the legitimate _high qualitative_ culture that the elite represents. The function of such an institution, therefore, is that of fostering and controlling rather than of bringing democracy to common people. The general education discourse contains a way of looking at the user as someone in need of education, fostering and upbringing. This fostering tone also reflects the library's pedagogical role and discourse, which is discussed further below.

#### The Pedagogical Discourse

The Pedagogical Discourse is present in many of the texts. The discourse is not expressed in a specific debate, rather it is evident in formulations about the role and task of the library. Through the introduction of new information technology the library's pedagogical role has been emphasised and _the pedagogical discourse_ often coexists with _the information technology discourse_. Both discourses share the way users are regarded, in that they define the user as someone in need of guidance, superintendence and education. In the chains of equivalence the pedagogical discourse is expressed by connecting many elements to a need of education (children) and a need of assistance (mature students on The Adult Education Programme). Specific to the pedagogical discourse is the tendency to assign the user unconscious information needs that only the librarian can diagnose (cf., [Tuominen 1997](#Tuo97)). This is expressed for example in discussions about selection policy in public libraries. It also becomes evident in formulations about the reference interview. The user is clearly presented as unaware of what is best for her.

> Further you should listen unconditionally, wait the question out, and also act as a 'midwife in a complicated labour' when the reference question is to emerge. You should always keep eye contact and you should not look as if you've lost interest, nod affirmatively, and also use the' vacuum extractor in the expulsion stage'. [[Biblioteksbladet](#SupRef4) (4)].

As the general education discourse and the information technology discourse, the pedagogical discourse is also divided. These discourses hold at least two different rhetorical aspects; one aspect related to librarians' expressions of their work experience and another aspect related to a more idealizing rhetoric about their professional role (Figure 3). If the pedagogical discourse-experience-related aspect is expressed in the discussion above, the more idealised aspect is shown in a user approach with an ambition to create _self-sufficient users_. These ideals are exemplified in the texts in talk about _efficiency_, _automation_ and _information_ literacy.

#### The Information Technology Discourse

What we have identified as _the information technology discourse_ is conspicuous within the present data. This discourse has become quite influential since the entry of digital techniques in Swedish public libraries during the 1990s. The new information technology has been accompanied by a certain vocabulary which has made an impact on the way users are being talked about in the library specialist press. In the selected texts this can be traced through terms such as _user friendliness_ and _usability_ and by talk of _interactivity_. The word _user_ is frequent in more technically oriented texts, usually mixed with _course members_ and/or _customers_. In our data a certain consciousness about these matters is recognised, which in one way is expressed by attributing responsibility and ability to act to the user. The user is spoken of as an individual with the right to raise demands for library services:

> Within the last four or five years the Internet and multimedia on CD ROM have become compulsory resources, that practically everybody expects to find at the local library. It has also become more common that 'borrowers' do their own literature search from their home computers and then bring their 'search lists' to the library to be able to find 'the real thing' behind the reference - that is the book, the article, the CD etc. [[Ikoner](#SupRef6) (6)]

Within the analysed texts such values are distinguishable, but they often collide with experiences and descriptions of library practice which is understood to be more _connected with reality_. A conflict is found between idealizing and experience-related rhetoric (Figure 3). According to librarians' own work experience users are supposed to need guidance towards what is considered correct Internet usage. Information seeking is to be encouraged and amusement usage is to be restrained.

#### The Information Management Discourse

What we identify as _the information management discourse_ has a strong connection with the information technology discourse. This discourse is concerned with more than new technology, however. The Swedish debate from earlier years about privatising library services seems partly to have come to a halt due to library legislation from 1996 that regulates free book loans. Nevertheless, some forms of educational activity have a certain business appeal to public libraries. In the texts we find statements from library representatives that have taken on a new consultancy role. The terminology in these cases is clearly influenced by a market-economy thinking concerning _demand_, _profit maximisation_ and users seen as _customers_ or _clients_. There is also a certain influence from management terms in talk about _environmental scanning_, _optimisation_ and _efficiency_. A distinction between the information management discourse and the general education discourse is that in the former it is primarily library activity as such that is supposed to be in need of development and rethinking; not the users. In the quotation below the political government of public libraries is explicitly criticised:

> Libraries are on their way, but I think that it takes more challenges to make the process powerful enough. These challenges don't come from the principal, that is the political government; they often hold a passive, nostalgic view of libraries. No, it's necessary to maximise the contacts with the suppliers and customers, and thereby create and carry the 'business projects' through, which promotes libraries forward and upwards... [ [Ikoner](#SupRef7) (7)]

Courses in information seeking (preferably of a technical nature) seem, apart from quick and efficient service, to be the help that users need. Statements that emphasise that a public library should be able to offer the users what they want are also expressions of the information management discourse. In comparison to the pedagogical discourse that to a certain point emphasise the libraries diagnostic function, here we can notice a stronger faith in users' ability to take responsibility for their information needs. Within the information management discourse we have not found the same discrepancy between the two different aspects as in the other discourses. This fact may of course be due to the data selection, but it can also possibly be explained by the nature of the discourse itself. It is, for instance, the least frequent discourse and plausibly one of the youngest. A general impression is that rhetoric and practice are more closely intertwined.

## Conclusions

The figure below shows different ways of designating and categorising users. It also presents themes within which users are constituted and the different discourses. The figure illustrates, in short, the results of the analysis.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Figure 2\. Summary of results**</caption>

<tbody>

<tr>

<td align="left" valign="top" width="50%">

**1**  
_Designations_

*   The whole target group
*   General designations
*   Groups
*   Individuals
*   Metaphors

</td>

<td align="left" valign="top" width="50%">

**2**  
_Categories_

*   Youth
*   Children
*   Mature students
*   Neglected groups

</td>

</tr>

<tr>

<td align="left" valign="top" width="50%">

**3**  
_Themes_

*   The mission
*   Internet usage
*   The pedagogical role

</td>

<td align="left" valign="top" width="50%">

**4**  
_Discourses_

*   General Education Discourse
*   Pedagogical Discourse
*   Information Technology Discourse
*   Information Management Discourse

</td>

</tr>

</tbody>

</table>

That certain discourses dominate a field has both ideological and practical consequences. With a departure point in the results of the analysis we would like to discuss a wider view of the discourses. The discussion is not only about what different discourses contain, but also about what they exclude. A discourse is always constituted in relation to what it leaves out. The constructed user categories contrast to all other possible categorisation.

_The general education discourse_ mediates, through the distinction between high and low culture, an image of the public library institution as being qualitatively superior to common people. As distinctions are made in the texts, between quality and entertainment literature, this can lead to a patronising attitude towards the user. Such a condescending approach brings the risk of underestimating and/or disregarding the user and her ability to initiate and take responsibility for her information needs. The preferential right of interpretation held by the official authority can make it difficult for the representatives of the public library to reflect on their role and maintain self-criticism. The user, on the other hand, risks standing out as incapacitated and culturally undeveloped in her use of the library. Not by lack of interest, but because her interests are not in accordance with the institution's opinion of high culture.

One consequence of _the pedagogical discourse_ could be an over-emphasis of the utilitarian function of the library. Although we share the idea that information seeking and learning are important aspects of the libraries' activities, the user's own experience, recreation and expression are not less important. If the libraries' pedagogical role dominates there is a risk that users associate the library with too much of a _would-be wise_ attitude. The relation between the librarian and the user can be described as one marked by one-sidedness and dependence.

The consequences of _the information technology discourse_ are closely linked to the Zeitgeist and new technology. Information technology holds a prominent position in what is called the _knowledge_ or _information society_. Nevertheless, technology's value as information carrier and a pedagogical tool can be questioned as well as the Internet's capacity to democratise. It is important to stress that other activities should not be overlooked in the priorities of the library. Through the texts there is a lot of debate about the possibilities and dangers of the Internet.

_The information management discourse_ may seem to hold as a consequence a respectful and authorising view of the user. For example, it is advocated that media purchase should be based on demand. This seems well enough but such a view takes for granted that the users are strong-willed, unafraid and focused. The social responsibility that is implied within the ideas of general education is not emphasised within the information management discourse; nobody speaks on behalf of the neglected groups. In the study we have found explicit criticism against the political government of the library, which can be interpreted as a criticism of the democratic function of public libraries.

While plausible consequences of these discourses could be apprehended as detrimental to library development this is not entirely the case. For instance, the idea of general education still holds considerable strength in its beliefs in equality and library access to all. The target group _all_ is indeed hard to meet, but we are highly aware of the great efforts made to address both the public at large and certain user groups. The value of the pedagogical discourse in turn lies in emphasising communication and mediating information and knowledge from within the institution. The public library as a democratic forum certainly has a potential to guide citizens towards desirable information literacy.

The information technology discourse, on the other hand, is characterised by its ambition to develop the library's activities and tools. As we see it, the most important issue raised by this discourse, is the awareness of user's interaction with information systems. The information management discourse, on its part, accentuates the value of taking both the user's own requests and the library's service functions seriously. It may be seen as pertinent that the users are afforded greater influence on media selection and the library's character at large. This kind of reasoning is opposed to the general education discourse and the pedagogical discourse where the library is supposed to mould the user's character.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Figure 3\. The four discourses with examples of user's designations;  
relating to our ideas about plausible consequences that may – directly or indirectly – affect the user**</caption>

<tbody>

<tr>

<th width="20%"></th>

<th valign="top" width="20%">The General Education Discourse</th>

<th valign="top" width="20%">The Pedagogical Discourse</th>

<th valign="top" width="20%">The Information Technology Discourse</th>

<th valign="top" width="20%">The Information Management Discourse</th>

</tr>

<tr>

<th valign="top" width="20%">Idealizing rhetoric</th>

<td valign="top" width="20%">Citizens  
Employers</td>

<td valign="top" width="20%">Self-propelled information seekers</td>

<td valign="top" width="20%">IT-oriented users</td>

<td rowspan="2" width="20%">Clients  
Customers</td>

</tr>

<tr>

<th valign="top" width="20%">Experience-related rhetoric</th>

<td valign="top" width="20%">'People'  
User categories associated with various problems</td>

<td valign="top" width="20%">User categories associated with varying need for help and guidance</td>

<td valign="top" width="20%">Disturbing users, primarily young people</td>

</tr>

<tr align="center">

<td width="20%"> </td>

<td width="20%">![arrow](p218arrow.gif)</td>

<td width="20%">![arrow](p218arrow.gif)</td>

<td width="20%">![arrow](p218arrow.gif)</td>

<td width="20%">![arrow](p218arrow.gif)</td>

</tr>

<tr>

<th rowspan="2" width="20%">Plausible consequences</th>

<td valign="top" width="20%">+  
Ideas of equality  
Social responsibility  
High quality collections</td>

<td valign="top" width="20%">+  
Encourages learning  
Increased access to the library</td>

<td valign="top" width="20%">+  
The library is up to date  
Democratic ideas  
'The user' in focus</td>

<td valign="top" width="20%">+  
Collections built on demand  
The user "declared capable"</td>

</tr>

<tr>

<td valign="top" width="20%">-  
The user "declared incapable"  
Lack of self-criticism  
Censorship</td>

<td valign="top" width="20%">-  
Over-emphasised utility point  
Users' integrity at risk</td>

<td valign="top" width="20%">-  
Less "hot" activities are not given priority</td>

<td valign="top" width="20%">-  
Lack of social responsibility  
Impoverished collections</td>

</tr>

</tbody>

</table>

No doubt, the results presented above may seem contradictory. This fact must be seen as an expression of both conspiring and conflicting discourses that rarely occur in isolation. Inter-discursive contradictions are to be found in the whole material as well as in separate texts and even within the statements of individuals. There is no aim in this study to recommend one discourse or the other, but rather to underline the importance for librarians and researchers to reflect upon their attitudes towards the users. Another important aspect of our study is that it touches on how the categorizations of _the user_ may have more practical implications. The close interactive relation between language and action is fundamental to discourse theory, and thus the user's information behaviour may be affected by different ways of speaking; for instance, directly in the reference interview, or indirectly by collection policies etc.

This study may also be seen as a contribution to the development of discourse analytic approaches within librarianship and information science research. To this date, concrete examples of discourse analytical research methods have been scarce in the field, in spite of an emerging interest. Therefore, perhaps this study may be one possible and suitable way of applying discourse theory empirically.

## Acknowledgements

The authors wishes to acknowledge the suggestions of the anonymous referees.

## Notes

<a name="note1" id="note1"></a>1\. All quotations throughout the paper have been translated by the authors.

<a name="note2" id="note2"></a>2\. _Kunskapslyftet_ was a major, Swedish government adult education initiative during 1997-2002.

## References

*   <a name="Day00" id="Day00"></a>Day, R.E. (2000). The _conduit metaphor_ and the nature and politics of information studies. _Journal of the American Society for Information Science_, **51** (9), 805-811.
*   <a name="Giv02" id="Giv02"></a>Given, L. (2002). Discursive constructions in the university context: social positioning theory & nature of undergraduates' information behaviour. _The New Review of Information Behaviour_, **3**, 127-141.
*   <a name="Kuh93" id="Kuh93"></a>Kuhlthau, C.C. (1993). _Seeking meaning: a process approach to library and information services_. Norwood, NJ: Ablex.
*   <a name="Lac85" id="Lac85"></a>Laclau, E. & Mouffe, C. (1985). _Hegemony & socialist strategy: towards a radical democratic politics_. London: Verso.
*   <a name="Sun00" id="Sun00"></a>Sundin, O. (2000). Qualitative research in health information user studies: a methodological comment. _Health Library Review_, **17**(4), 215- 218.
*   <a name="Tal97" id="Tal97"></a>Talja, S. (1997). Constituting _information_ and _user_ as research objects: a theory of knowledge formations as an alternative to the information man – theory. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information Seeking in Context: Proceedings of an International Conference on Research in Information, Needs, Seeking and Use in Different Contexts, 14-16 August, 1996, Tampere, Finland._ (pp. 67-80). London: Taylor Graham.
*   <a name="Tal01" id="Tal01"></a>Talja, S. (2001). _Music, culture, and the library: an analysis of discourses_. Lanham, MD: Scarecrow Press.
*   <a name="Tal99" id="Tal99"></a>Talja, S., Keso, H. & Pietiläinen, T. (1999). The production of 'context' in information seeking research: a metatheoretical view. _Information Processing and Management_, **35**(6), 751-763\.
*   <a name="Tor95" id="Tor95"></a>Torstensson, M. (1995). Expectations and a worthy, respectable position in society: means and aims of library work within the early labour movement in Sweden. _Swedish Library Research_. **1**, 16-26\.
*   <a name="Tuo97a" id="Tuo97a"></a>Tuominen, K. (1997). User-centered discourse: an analysis of the subject positions of the user and the librarian. _Library Quarterly_, **67**(4), 350-371\.
*   <a name="Tuo97b" id="Tuo97b"></a>Tuominen, K. & Savolainen, R. (1997) A social constructionist approach to the study of information use as discursive action. In P. Vakkari, R. Savolainen & B. Dervin (Eds.), _Information Seeking in Context: Proceedings of an International Conference on Research in Information, Needs, Seeking and Use in Different Contexts, 14-16 August, 1996, Tampere, Finland._ (pp. 81-96). London: Taylor Graham.
*   <a name="Tuo02" id="Tuo02"></a>Tuominen, K., Savolainen, R. & Talja, S. (2002). Discourse, cognition, and reality: towards a social constructionist metatheory for library and information science. In H. Bruce, R. Fidel, P. Ingwersen & P. Vakkari (Eds.), _Emerging Frameworks and Methods: Proceedings of the Fourth International Conference of Conceptions of Library and Information Science (CoLIS4)._ (pp. 271-283) Greenwood Village, CO: Libraries Unlimited.
*   <a name="Ves97" id="Ves97"></a>Vestheim, G. (1997) _Fornuft, Kultur og Velferd: Ein Historisk-Sosiologisk Studie av Norsk Folkebibliotekspolitikk. [Reason, culture and welfare. A historical-sociological study of Norwegian public library policy.]_ Oslo: Det Norske Samlaget.
*   <a name="Win02" id="Win02"></a>Winther Jørgensen, M. & Phillips, L. (2002). _Discourse analysis as theory and method._ London: Sage.

# Appendix: articles analysed

1.  <a id="SupRef1" name="SupRef1"></a>Gustafsson, O. (1999) Kan och ska folkbiblioteket vara till för  alla? _Bibliotek i Samhälle_, (4), 13.
2.  <a id="SupRef2" name="SupRef2"></a>Borg, B. (2001) Dags för biblioteken att inta en högre och tydligare profil. _Bibliotek i Samhälle_, (3), 11-12.
3.  <a id="SupRef3" name="SupRef3"></a>Hågård, S. (1999) Kan folkbiblioteket verkligen vara till för alla? _Bibliotek i Samhälle_, (4), 11-12.
4.  <a id="SupRef4" name="SupRef4"></a>Rabe, A. (1999) Technology information centres och hederlig inköpspolitik. _Biblioteksbladet_, (8), 11-12.
5.  <a id="SupRef5" name="SupRef5"></a>Hallgren, S. (1999) Kulturdagis eller bokmagasin? _Ikoner_, (3), 29-31.
6.  <a id="SupRef6" name="SupRef6"></a>Gustavsson, K. (1999) Det virtuella folkbiblioteket. En idéskiss för debatt. _Ikoner_, (1), 36-37, 43.
7.  <a id="SupRef7" name="SupRef7"></a>Nilsson, S. (1999) Med feedback mot framtiden. _Ikoner_, (4), 41-43.



