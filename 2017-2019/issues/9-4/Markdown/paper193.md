# Análisis bibliométrico de la literatura científica publicada en _"Ciencia. Revista hispano-americana de ciencias puras y aplicadas"_ (1940-1974)

#### [Antonio Pulgarín](mailto:pulgarin@unex.es), [Cristina Carapeto](mailto:ccarapet@alcazaba.unex.es), and [José M. Cobos](mailto:jcobos@unex.es)  
Facultad de Biblioteconomía y Documentación, Universidad de Extremadura  
Badajoz, España

#### **Resumen**

> El estudio es el avance de un proyecto, que tiene como objetivo el análisis de la producción científica de la revista _Ciencia_, desde su aparición (1940) hasta su desaparición (1974). La revista _Ciencia_ constituyó, como fuente de información, el canal formal de difusión científica entre los investigadores españoles exiliados en Hispanoamérica, debido a la Guerra Civil Española (1936-1939). Se analizan los trabajos originales publicados por la revista, contenidos en tres de las siete secciones en que esta se divide: Ciencia moderna (sección I), Comunicaciones originales (sección II) y Ciencia aplicada (sección IV). Igualmente se estudiaron las referencias bibliográficas contenidas en esos artículos. El total de artículos analizados fue de 972 y el de referencias bibliográficas de14.184.

#### [Abstract in English](#abstract)

## Introducción

La Guerra Civil Española (1936-1939) provocó uno de los éxodos más grandes que hayan tenido lugar en España. La comunidad científica, constituida en su mayor parte por republicanos, no fue ajena a este trágico evento. Y los esfuerzos realizados durante años, por aproximarse a la ciencia moderna, se frustraron.

La guerra sorprendió a muchos en pleno apogeo de sus investigaciones, algunos de ellos trabajando en otros países. El salir de España precipitadamente o el no volver, aquellos que se encontraban fuera, estaba motivado por las noticias que llegaban de los trágicos destinos de otros compañeros, fundamentalmente detenciones, torturas y asesinatos ([Fernández-Guardiola, 1997](#fer97)).

A México llegan filósofos como José Gaos, Joaquín Xirau o María Zambrano; poetas como José Bergamín o León Felipe; historiadores como Rafael Altamira o Pedro Boch Gimpera; etc. Y científicos como José Giral, Blas Cabrera, Pedro Carrasco, Augusto Pi Sunyer, Ignacio Bolívar, etc. Una aproximación al número de científicos españoles exiliados a México nos la ofrece Ordoñez-Aparicio ([2002](#ord02)), concluyendo que el desembarco de los científicos españoles va a dar un fuerte empuje a la ciencia mexicana, sobre todo a la medicina.

La inquietud cultural y científica de los exiliados se manifiesta rápidamente. Nada más llegar, fundan una revista, con el único fin de transmitir información, después crearán otras de carácter más científico. En 1940 aparece el primer número de la revista _Ciencia. Revista hispano-americana de ciencias puras y aplicadas_, bajo la dirección de Ignacio Bolívar (mencionado anteriormente), y el último número verá la luz en 1974\. La nómina de científicos españoles, así como de otras nacionalidades, que colaboraron con la revista fue extraordinaria ([Giral, 1994](#gir94)).

Para cumplir los fines de la revista _Ciencia_; que eran difundir el conocimiento de las ciencias físico-naturales y exactas y sus múltiples aplicaciones, esta se dividió en siete secciones: primera sección, la _ciencia moderna_, incluiría artículos procedentes de especialistas reconocidos que tratarían problemas científicos de actualidad; segunda sección, _comunicaciones originales_, incluiría notas o comunicaciones de carácter especializado; tercera sección, _noticias_, de carácter científico, universitario, etc. de los países hispano-americanos; cuarta sección, _ciencia aplicada_, con artículos referentes a problemas de ingeniería o arquitectura, procesos industriales o técnicos, etc.; quinta sección, _miscelánea_, con contenido referente a informaciones científicas, de enseñanza, de organización, etc.; sexta sección, _libros nuevos_, con reseñas de obras de reciente publicación; séptima sección, _revista de revistas_, en la que se publicaba una selección de notas de trabajos de gran interés ([Carapeto, _et al._, 2002](#car02)).

Por todo ello hemos creído interesante hacer un análisis de la producción científica de la revista Ciencia, como homenaje a estos científicos, ya que reflejaría una gran parte de la producción científica generada por estos investigadores españoles en el exilio, así como el tipo y alcance de colaboración científica con el mundo, etc., en contraposición con la generada en la España de Franco.

El presente estudio fue llevado a cabo bajo un enfoque bibliométrico, ya que otros acercamientos de tipo filosóficos y humanísticos (sociológico, histórico social, etc.), aunque interesantes, no eran el objetivo. Se ha estudiado, empíricamente, la actividad científica generada por un grupo de científicos, a través del producto final de la investigación, como es el artículo científico. El enfoque bibliométrico conlleva el uso y la aplicación de métodos cuantitativos (indicadores y modelos matemáticos) a esa literatura científica, con objeto de obtener datos que puedan aportar conocimiento acerca de la evolución de una determinada producción científica, conocer su calidad y obtener elementos de juicio para posibles intervenciones o interpretaciones de hechos o fenómenos sociales, como es el caso.

Entre los indicadores de la actividad científica destacan: los implícitos (tipo económico, demográfico, etc.) y los explícitos; extraídos de repertorios de datos estadísticos (libros, artículos, etc.), o creados a partir de un programa determinado y con una finalidad (índice de Lotka, dispersión de Bradford, etc.)

La aplicación de este tipo de indicadores no puede limitarse a la aportación de una serie de datos estadísticos, sin más y por separado, sino que deben estar integrados para que puedan aportar una explicación sólida sobre la actividad científica que se esté considerando. La actividad científica presenta una serie de características que tienen que ver, en primer lugar, con la publicación científica, que es el producto final de esa actividad ( [López-Piñero y Terrada, 1992a](#lop92a)).

Por tanto, resulta adecuado ocuparse aquí de las funciones que en el proceso de la comunicación científica realizan las fuentes de información (libros, revista científica, patentes, tesis doctorales, etc.), así como otros patrones cuantitativos, como son las citas y las referencias bibliográficas, por su importancia para los indicadores bibliométricos. En los estudios bibliométricos cabe distinguir entre cita, que es el reconocimiento que un documento recibe de otro, y referencia bibliográfica, que es el reconocimiento que un documento da a otro ( [Price, 1970](#pri70)).

El objetivo tanto del estudio de las citas como de las referencias es el consumo de información científica (por parte de un autor, grupo, etc.) y conocer la repercusión o impacto que su producción ha tenido en comunidades científicas determinadas ( [López Piñero y Terrada, 1992b](#lop92b)). El apoyo de los indicadores en las citas y en las referencias indica que los trabajos importantes son usualmente citados. Además, los autores, deben mantener unas obligaciones éticas, ya que la investigación y la publicación de sus resultados son actividades intrínsecamente éticas. En cuanto a las referencias bibliográficas, el comportamiento ético se deriva del compromiso adquirido con los lectores para que estos puedan reproducir situaciones, analizar los hechos y efectuar interpretaciones a partir de las fuentes de información aparecida en las listas de documentos que componen la bibliografía del artículo. La credibilidad del autor no solo se determina por la solvencia científica que tenga, sino que también va a influir la pertinencia de la información seleccionada ([Pulido, 1997](#pul97)).

Así que, por lo expuesto, el comportamiento de la información científica es competencia de la bibliometría. Los indicadores bibliométricos serán utilizados para clasificar las revistas científicas, para saber cual es su producción, su difusión (nacional y/o internacional), la repercusión alcanzada en la comunidad científica, etc. Se puede decir que los indicadores bibliométricos orientan sobre el valor científico de una revista, permiten saber cuales son los artículos, los autores y los grupos más leídos y, que por lo tanto, tienen trabajos de mayor peso científico.

Los indicadores bibliométricos son de cuatro tipos: de producción, de circulación y dispersión, de consumo y de repercusión ([López-Piñero y Terrada, 1992c](#lop92c)).

Los indicadores de producción analizan la cantidad de publicaciones científicas producidas por un autor, un grupo de autores, una revista, etc. Entre estos se encuentran:

1.  El índice de productividad de Lotka, que es el logaritmo decimal del número de publicaciones, pudiendo agrupar a los autores por su nivel, generalmente en tres niveles de productividad: pequeños productores, con un solo trabajo publicado y un índice igual a cero; medianos productores (de 2 a 9 trabajos publicados), con índice de Lotka entre cero y uno; y grandes productores (diez o más trabajos publicados), con un índice de productividad igual o mayor que uno.
2.  El índice de cooperación o más conocido como número de firmas/trabajo o media de firmantes por cada trabajo. Existe una correlación positiva entre este índice y el de productividad de Lotka.
3.  El índice de referencias por artículo. Su cálculo se efectúa mediante el cociente entre el número de referencias y el número de artículos publicados. Se suele hacer para períodos de tiempo, por ejemplo un año, un volumen, etc.

Los indicadores de circulación y dispersión, como son: el índice de productividad circulante (logaritmo decimal del número de trabajos circulantes en una base de datos), índice de circulación (cociente entre el número de trabajos circulantes y número de trabajos publicados), índice de difusión internacional (cociente entre el número de trabajos circulantes en _n_ bases de datos internacionales y el número de trabajos publicados, multiplicado por _n_) o la dispersión de Bradford (ordenación de revistas según su productividad en zonas concéntricas de productividad decreciente), no los vamos a aplicar en este caso, ya que se trata de una sola revista y además con cobertura anterior a las base de datos (1940-1974).

Los indicadores de consumo se refieren al análisis de las referencias bibliográficas, contenidas en los artículos publicados por las revistas científicas, e informan de la obsolescencia y del aislamiento de la producción científica. Entre estos se encuentran:

1.  Vida media o semiperíodo de las referencias. Es el tiempo o número de años en que la utilidad de una bibliografía se reduce al 50 % ([Burton y Kléber, 1960](#bur60)).
2.  Índice de Price. Es el porcentaje de referencias con una antigüedad menor a 5 años ([Price, 1965](#pri65)). De aquí se deduce que las revistas que publican artículos referidos a campos muy dinámicos suelen tener una vida media baja y un índice de Price alto.
3.  Índice de aislamiento. Es el porcentaje de referencias que corresponden al mismo país que la publicación citadora, reflejando el grado de aislamiento o de apertura al exterior.
4.  Distribución de las referencias según el país de origen, idioma cuales son las revistas de esas referencias, tipo de documento, etc., ayuda a valorar las influencias de los autores en una revista.

Los indicadores de repercusión o impacto se elaboran con la citas y, por tanto, tampoco se han aplicado en este trabajo (no aparece seleccionada la revista Ciencia en ISI). Son:

1.  Í;ndice de visibilidad. Es el logaritmo decimal de las citas recibidas.
2.  Í;ndice de influencia. Cociente entre el número de citas recibidas y las referencias emitidas.
3.  Vida media de las citas. Es la mediana de la distribución de las citas por año de emisión.
4.  Índice de impacto. Es el cociente entre el número de citas recibidas y el número de trabajos publicados. El índice de impacto relativo es el cociente entre el índice de impacto de un autor, revista o grupo y el índice de impacto máximo del campo al que pertenecen. El factor de impacto, como variante del índice e impacto, fue popularizado por el Institute for Scientific Information (Philadelphia) y está teniendo, en los últimos años, una gran repercusión. Se calcula como el cociente entre las citas recibidas en un año por los artículos publicados por una revista los dos años anteriores, dividido por el total de artículos publicados por dicha revista en los dos años previos.

## Metodología

Financiado por el proyecto 2PR02A055, de la Junta de Extremadura, se ha adquirido copia de los volúmenes, en forma impresa, de la revista Ciencia. Con este material se construyó una base de datos (Microsoft Access). Cada registro contiene los datos bibliográficos de cada artículo, además de las referencias bibliográficas pertenecientes a ese mismo artículo.

Los datos que recoge la base de datos creada pertenecen a las tres secciones siguientes de la revista Ciencia: sección I (Ciencia moderna), sección II (Comunicaciones originales) y sección IV (Ciencia aplicada). En total la base de datos recoge 972 artículos y 14,184 referencias bibliográficas, contenidas en esos artículos.

Una vez construida la base de datos, se procede a un análisis bibliométrico descriptivo de los datos, aplicándoseles una serie de indicadores bibliométricos. Este análisis está referido al volumen de información (número de trabajos científicos publicados), su distribución anual por secciones, productividad de los autores, índice de productividad de Lotka, temática de investigación de los trabajos científicos, colaboración científica, índice de cooperación, etc.

En cuanto al estudio de la bibliografía (referencias bibliográficas de cada artículo), se estudia la media del número de estas por artículo, estudio de la obsolescencia (índice de Price), tipología documental e idioma en que se publicaron, estas referencias.

## Resultados

### De la producción científica

En la Tabla 1 se muestra el volumen de información total que se ha considerado para este estudio, como son los artículos correspondientes a la sección I (Ciencia moderna), sección II (Comunicaciones originales) y sección IV (Ciencia aplicada). Igualmente, junto al número de artículos, en la tabla se observa el número de referencias bibliográficas contenidas en estos. En total el número de artículos asciende a 972 y el de referencias a 14.184\. Tanto artículos como referencias bibliográficas se distribuyen en 29 volúmenes, publicados por la revista Ciencia, que abarca el período comprendido entre 1940, inicio de la publicación de la revista Ciencia y 1974, desaparición de la misma. Aunque no sea muy significativo, en este caso, dada la irregularidad presentada por la revista Ciencia, en cuanto a la correspondencia de volúmenes y años, también se ha calculado y expuesto en la Tabla 1 el índice de referencias por artículos (indicador de producción), siendo la media de 14,6 referencias por artículo. A pesar de la irregularidad de los volúmenes, en cuanto a fecha de salida, el número de artículos se ha mantenido bastante constante, a excepción del último año, creemos que por estar cerca el fin de la revista.

De estos artículos, 325 (33,44 %) corresponden a Biología, 291 (29,94 %) a Química, 216 (22,22 %) a Medicina, 64 (6,6 %) a Geología, 35 (3,6 %) a Física, 29 (3 %) a Matemáticas y 12 (1,2 %) al grupo denominado como Otros. Para realizar esta primera aproximación a una clasificación por materias, se ha tenido en cuenta tanto la procedencia del investigador como el contenido del artículo.

<table><caption>

**Tabla 1: Distribución anual de artículos, correspondiente a las secciones I (Ciencia moderna), II (Comunicaciones originales) y IV (Ciencia aplicada), referencias bibliográficas e índices de referencias por artículo, de la revista Ciencia.**</caption>

<tbody>

<tr>

<th>Años</th>

<th>Número artículos</th>

<th>Sección I</th>

<th>Sección II</th>

<th>Sección IV</th>

<th>Referencias</th>

<th>Índice de referencias</th>

</tr>

<tr>

<td>1940</td>

<td>65</td>

<td>19</td>

<td>35</td>

<td>11</td>

<td>690</td>

<td>10,6</td>

</tr>

<tr>

<td>1941</td>

<td>43</td>

<td>11</td>

<td>22</td>

<td>10</td>

<td>583</td>

<td>13,5</td>

</tr>

<tr>

<td>1942</td>

<td>37</td>

<td>10</td>

<td>19</td>

<td>8</td>

<td>319</td>

<td>8,6</td>

</tr>

<tr>

<td>1943</td>

<td>39</td>

<td>10</td>

<td>25</td>

<td>4</td>

<td>510</td>

<td>13,0</td>

</tr>

<tr>

<td>1944</td>

<td>29</td>

<td>5</td>

<td>19</td>

<td>5</td>

<td>193</td>

<td>6,6</td>

</tr>

<tr>

<td>1945</td>

<td>36</td>

<td>7</td>

<td>22</td>

<td>7</td>

<td>406</td>

<td>11,3</td>

</tr>

<tr>

<td>1946-47</td>

<td>36</td>

<td>5</td>

<td>26</td>

<td>5</td>

<td>332</td>

<td>9,2</td>

</tr>

<tr>

<td>1947-48</td>

<td>41</td>

<td>5</td>

<td>31</td>

<td>5</td>

<td>648</td>

<td>15,8</td>

</tr>

<tr>

<td>1948-49</td>

<td>40</td>

<td>6</td>

<td>29</td>

<td>5</td>

<td>640</td>

<td>16,0</td>

</tr>

<tr>

<td>1950-51</td>

<td>31</td>

<td>5</td>

<td>20</td>

<td>6</td>

<td>505</td>

<td>16,3</td>

</tr>

<tr>

<td>1951-52</td>

<td>39</td>

<td>4</td>

<td>31</td>

<td>4</td>

<td>571</td>

<td>14,6</td>

</tr>

<tr>

<td>1952-53</td>

<td>40</td>

<td>2</td>

<td>34</td>

<td>4</td>

<td>371</td>

<td>9,3</td>

</tr>

<tr>

<td>1953-54</td>

<td>37</td>

<td>5</td>

<td>28</td>

<td>4</td>

<td>646</td>

<td>17,4</td>

</tr>

<tr>

<td>1954-55</td>

<td>33</td>

<td>3</td>

<td>25</td>

<td>5</td>

<td>495</td>

<td>15,0</td>

</tr>

<tr>

<td>1955-56</td>

<td>34</td>

<td>4</td>

<td>22</td>

<td>8</td>

<td>541</td>

<td>15,9</td>

</tr>

<tr>

<td>1956</td>

<td>46</td>

<td>0</td>

<td>42</td>

<td>4</td>

<td>359</td>

<td>7,8</td>

</tr>

<tr>

<td>1957-58</td>

<td>31</td>

<td>4</td>

<td>23</td>

<td>4</td>

<td>510</td>

<td>16,4</td>

</tr>

<tr>

<td>1958-59</td>

<td>25</td>

<td>3</td>

<td>19</td>

<td>3</td>

<td>350</td>

<td>14,0</td>

</tr>

<tr>

<td>1959-60</td>

<td>32</td>

<td>4</td>

<td>25</td>

<td>3</td>

<td>561</td>

<td>17,5</td>

</tr>

<tr>

<td>1960-61</td>

<td>29</td>

<td>5</td>

<td>20</td>

<td>4</td>

<td>763</td>

<td>26,3</td>

</tr>

<tr>

<td>1961-62</td>

<td>28</td>

<td>5</td>

<td>20</td>

<td>3</td>

<td>472</td>

<td>16,8</td>

</tr>

<tr>

<td>1962-63</td>

<td>27</td>

<td>1</td>

<td>24</td>

<td>2</td>

<td>252</td>

<td>9,3</td>

</tr>

<tr>

<td>1964-65</td>

<td>29</td>

<td>4</td>

<td>24</td>

<td>1</td>

<td>445</td>

<td>15,3</td>

</tr>

<tr>

<td>1965-66</td>

<td>36</td>

<td>1</td>

<td>35</td>

<td>0</td>

<td>652</td>

<td>18,1</td>

</tr>

<tr>

<td>1966-67</td>

<td>30</td>

<td>5</td>

<td>25</td>

<td>0</td>

<td>570</td>

<td>19,0</td>

</tr>

<tr>

<td>1968</td>

<td>21</td>

<td>5</td>

<td>16</td>

<td>0</td>

<td>409</td>

<td>19,4</td>

</tr>

<tr>

<td>1969-72</td>

<td>28</td>

<td>2</td>

<td>26</td>

<td>0</td>

<td>362</td>

<td>12.9</td>

</tr>

<tr>

<td>1973</td>

<td>22</td>

<td>1</td>

<td>21</td>

<td>0</td>

<td>584</td>

<td>26,5</td>

</tr>

<tr>

<td>1974</td>

<td>8</td>

<td>0</td>

<td>8</td>

<td>0</td>

<td>445</td>

<td>55,6</td>

</tr>

<tr>

<th>Total</th>

<th>972</th>

<th>141</th>

<th>716</th>

<th>115</th>

<th>14.184</th>

<th>14,6</th>

</tr>

</tbody>

</table>

Respecto a la productividad científica de los autores (Tabla 2), se puede observar que el número de autores con un solo artículo publicado (índice de productividad igual a cero) fue de 436, el número de autores con dos artículos fue de 85, etc. (columna 2). Estos mismos datos, en forma porcentual, aparecen en la columna 3\. En la columna 4 se muestra el producto de la columna 1 por la columna 2, que es el número de trabajos aparentes, y en la última columna se muestra el logaritmo decimal del número de artículos (columna 1). Del cociente entre el total del número de trabajos aparentes (columna 4) y el total del número de autores (columna 2) resulta el número de trabajos por autor, que es de 2,28\. De los datos de la última columna de la Tabla 2 se deduce los índices de productividad de Lotka (log<sub>10</sub> n), distribuyéndose estos en tres niveles de productividad.

<table><caption>

**Tabla 2: Productividad científica de los autores e índice de productividad de Lotka.**</caption>

<tbody>

<tr>

<th>(n) Número artículos</th>

<th>Número Autores</th>

<th>% Número Autores</th>

<th>Trabajos aparentes</th>

<th>Índice de productividad de Lotka (lg10 n)</th>

</tr>

<tr>

<td>1</td>

<td>436</td>

<td>66,56  
</td>

<td>436</td>

<td>0,00000</td>

</tr>

<tr>

<td>2</td>

<td>85</td>

<td>12,97  
</td>

<td>170</td>

<td>0,30103</td>

</tr>

<tr>

<td>3</td>

<td>43</td>

<td>6,56  
</td>

<td>129</td>

<td>0,47712</td>

</tr>

<tr>

<td>4</td>

<td>26</td>

<td>3,96  
</td>

<td>104</td>

<td>0,60205</td>

</tr>

<tr>

<td>5</td>

<td>19</td>

<td>2,90  
</td>

<td>95</td>

<td>0,69897</td>

</tr>

<tr>

<td>6</td>

<td>9</td>

<td>1,37  
</td>

<td>54</td>

<td>0,77815</td>

</tr>

<tr>

<td>7</td>

<td>9</td>

<td>1,37  
</td>

<td>63</td>

<td>0,84509</td>

</tr>

<tr>

<td>8</td>

<td>2</td>

<td>0,30  
</td>

<td>16</td>

<td>0,90308</td>

</tr>

<tr>

<td>9</td>

<td>4</td>

<td>0,61  
</td>

<td>36</td>

<td>0,95424</td>

</tr>

<tr>

<td>10</td>

<td>2</td>

<td>0,30  
</td>

<td>20</td>

<td>1,00000</td>

</tr>

<tr>

<td>11</td>

<td>2</td>

<td>0,30  
</td>

<td>22</td>

<td>1,04139</td>

</tr>

<tr>

<td>12</td>

<td>5</td>

<td>0,76  
</td>

<td>60</td>

<td>1,07918</td>

</tr>

<tr>

<td>13</td>

<td>2</td>

<td>0,30  
</td>

<td>26</td>

<td>1,11394</td>

</tr>

<tr>

<td>14</td>

<td>2</td>

<td>0,30  
</td>

<td>28</td>

<td>1,14612</td>

</tr>

<tr>

<td>17</td>

<td>1</td>

<td>0,15  
</td>

<td>17</td>

<td>1,23044</td>

</tr>

<tr>

<td>19</td>

<td>1</td>

<td>0,15  
</td>

<td>19</td>

<td>1,27875</td>

</tr>

<tr>

<td>20</td>

<td>2</td>

<td>0,30  
</td>

<td>40</td>

<td>1,30103</td>

</tr>

<tr>

<td>22</td>

<td>1</td>

<td>0,15  
</td>

<td>22</td>

<td>1,34242</td>

</tr>

<tr>

<td>26</td>

<td>1</td>

<td>0,15  
</td>

<td>26</td>

<td>1,41497</td>

</tr>

<tr>

<td>33</td>

<td>1</td>

<td>0,15  
</td>

<td>33</td>

<td>1,51851</td>

</tr>

<tr>

<td>34</td>

<td>1</td>

<td>0,15  
</td>

<td>34</td>

<td>1,53147</td>

</tr>

<tr>

<td>45</td>

<td>1</td>

<td>0,15  
</td>

<td>45</td>

<td>1,65321</td>

</tr>

<tr>

<th> </th>

<th>655</th>

<td>  
</td>

<th>1.495</th>

<th> </th>

</tr>

</tbody>

</table>

Los niveles de productividad o índices de productividad (NP) se pueden observar con mayor claridad en la Fig. 1, donde aparecen 436 (66,6%) autores con NP = 0 (productividad baja), mientras que hubo 197 (30%) autores con 0 < NP < 1 (productividad media) y tan solo 22 (3,4%) autores con NP ≥ 1 (altos productores). El total de autores contabilizados en los 972 artículos publicados ha sido de 655.

<figure>

![Fig. 1](../p193fig1.jpg)

<figcaption>

**Fig. 1: Niveles de productividad de los autores de la revista Ciencia.**</figcaption>

</figure>

Dentro de la productividad científica es interesante saber el número de autores que trabajan en colaboración (Tabla 3). Si hay muchos autores o no que trabajan en solitario o en equipo, etc. Este indicador denominado índice de cooperación o más conocido como número de firmas/trabajo, ha resultado ser de 1,51 firmas o autores/trabajo. El cálculo procede de dividir el número de firmas aparentes (1.475) entre el número de artículos (972). En la tabla 3 se observa que el número de trabajos en colaboración es de 360 (37 %). Destaca un solo trabajo firmado por diez autores, 4 por 6, etc.

<table><caption>

**Tabla 3: Índice de cooperación o número de firmas/trabajo.**</caption>

<tbody>

<tr>

<th>Número de Firmas (a)</th>

<th>Número de Trabajos (b)</th>

<th>% de (b)</th>

<th>a×b</th>

</tr>

<tr>

<td>10</td>

<td>1</td>

<td>0,10  
</td>

<td>10</td>

</tr>

<tr>

<td>6</td>

<td>4</td>

<td>0,41  
</td>

<td>24</td>

</tr>

<tr>

<td>5</td>

<td>5</td>

<td>0,51  
</td>

<td>25</td>

</tr>

<tr>

<td>4</td>

<td>21</td>

<td>2,16  
</td>

<td>64</td>

</tr>

<tr>

<td>3</td>

<td>82</td>

<td>8,43  
</td>

<td>246</td>

</tr>

<tr>

<td>2</td>

<td>247</td>

<td>25,41  
</td>

<td>494</td>

</tr>

<tr>

<td>1</td>

<td>612</td>

<td>62,96  
</td>

<td>612</td>

</tr>

<tr>

<th>Total</th>

<th>972</th>

<td>  
</td>

<th>1.475</th>

</tr>

</tbody>

</table>

Respecto a la colaboración científica de los investigadores españoles exiliados en Hispanoamérica, se ha podido constatar la existencia de varios grupos de investigación, posiblemente colegios invisibles. Uno de estos grupos estaría formado por Giral, F. que colabora con un número de investigadores próximo a cuarenta, con varios de ellos publica en repetidas ocasiones en temas de Bioquímica – Química Orgánica. Otro grupo detectado sería el encabezado por Giral, J. (padre del anterior), grupo más pequeño que el anterior, formado por una veintena de investigadores que trabajan en temas de Bioquímica – Biología. Gravioto, R. O. es cabeza de un gran grupo de investigación de unos 20 investigadores que publicaron sobre Bioquímica Nutricional. El resto de grupos de investigación están encabezados por Massieu, H. G., con unos 30 investigadores, con publicaciones sobre Bioquímica – Biología, Erdos, J. formado por unos quince investigadores, Guzmán, G.J. y otros grupos formados por un número menor de investigadores.

Como dato final sobre la producción, se muestran los autores con mayor productividad en la revista Ciencia (Tabla 4) a lo largo de los más de treinta años de vida de la revista. Se ha confeccionado la tabla con aquellos autores que resultaron tener un NP > 1\. Francisco Giral, con 45 artículos, es el de mayor producción científica (NP = 1,65).

<table><caption>

**Tabla 4: Autores más productivos (NP > 1).**</caption>

<tbody>

<tr>

<th>Autores</th>

<th>Artículos</th>

</tr>

<tr>

<td>GIRAL, F.</td>

<td>45</td>

</tr>

<tr>

<td>CRAVIOTO, R.O.</td>

<td>34</td>

</tr>

<tr>

<td>ERDOS, J.</td>

<td>33</td>

</tr>

<tr>

<td>MASSIEU, H.G.</td>

<td>26</td>

</tr>

<tr>

<td>SANCHEZ MARROQUIN, A.</td>

<td>22</td>

</tr>

<tr>

<td>BOLIVAR PIELTAIN, C.</td>

<td>20</td>

</tr>

<tr>

<td>GUZMAN, G.J.</td>

<td>20</td>

</tr>

<tr>

<td>CASTRO, H. de</td>

<td>19</td>

</tr>

<tr>

<td>BARGALLO, M.</td>

<td>18</td>

</tr>

<tr>

<td>GIRAL, J.</td>

<td>17</td>

</tr>

<tr>

<td>MULLERRIED, F.K.G.</td>

<td>14</td>

</tr>

<tr>

<td>PELAEZ, D.</td>

<td>14</td>

</tr>

<tr>

<td>BARRERA, A.</td>

<td>13</td>

</tr>

<tr>

<td>MALOWAN, L.S.</td>

<td>13</td>

</tr>

<tr>

<td>CUATRECASAS, J.</td>

<td>12</td>

</tr>

<tr>

<td>HAHN, F.L.</td>

<td>12</td>

</tr>

<tr>

<td>MALDONADO-KOERDELL, M.</td>

<td>12</td>

</tr>

<tr>

<td>OSORIO TAFALL, B.F.</td>

<td>12</td>

</tr>

<tr>

<td>SANCHEZ-VIESCA, F.</td>

<td>12</td>

</tr>

<tr>

<td>ALVAREZ, J.</td>

<td>11</td>

</tr>

<tr>

<th>Total</th>

<th>379</th>

</tr>

</tbody>

</table>

### Del análisis de las referencias bibliográficas

En la Tabla 5 se muestra el índice Price obtenido para cada año, según el número de referencias bibliográficas pertenecientes a los artículos publicados en esos años. En la tabla se observa el número de artículos publicados cada año, en número de referencias pertenecientes a esos artículos, el número de referencias con antigüedad menor a 5 años respecto al de publicación del artículo y el índice de Price. El resultado es muy variable, por lo general un índice de Price bajo, a excepción de los 5 primeros años que es bastante alto. El índice de Price de la totalidad de las referencias es de 27,8 %.

<table><caption>

**Tabla 5: Índice Price (% de referencias con una antigüedad <5 años).**</caption>

<tbody>

<tr>

<th>Año</th>

<th>Número de artículos</th>

<th>Número de Referencias</th>

<th>Referencias < 5 años</th>

<th>Índice de Price (%)</th>

</tr>

<tr>

<td>1940</td>

<td>65</td>

<td>690</td>

<td>(1936-40) 355</td>

<td>51,4</td>

</tr>

<tr>

<td>1941</td>

<td>43</td>

<td>583</td>

<td>(1937-41) 339</td>

<td>58,1</td>

</tr>

<tr>

<td>1942</td>

<td>37</td>

<td>319</td>

<td>(1938-42) 142</td>

<td>44,5</td>

</tr>

<tr>

<td>1943</td>

<td>39</td>

<td>510</td>

<td>(1939-43) 183</td>

<td>35,8</td>

</tr>

<tr>

<td>1944</td>

<td>29</td>

<td>193</td>

<td>(1940-44) 100</td>

<td>51,8</td>

</tr>

<tr>

<td>1945</td>

<td>36</td>

<td>406</td>

<td>(1941-45) 120</td>

<td>29,5</td>

</tr>

<tr>

<td>1946-47</td>

<td>36</td>

<td>332</td>

<td>(1943-47) 72</td>

<td>21,6</td>

</tr>

<tr>

<td>1947-48</td>

<td>41</td>

<td>648</td>

<td>(1944-48) 197</td>

<td>30,4</td>

</tr>

<tr>

<td>1948-49</td>

<td>40</td>

<td>640</td>

<td>(1945-49) 147</td>

<td>22,9</td>

</tr>

<tr>

<td>1950-51</td>

<td>31</td>

<td>505</td>

<td>(1947-51) 68</td>

<td>13,4</td>

</tr>

<tr>

<td>1951-52</td>

<td>39</td>

<td>571</td>

<td>(1948-52) 155</td>

<td>27,1</td>

</tr>

<tr>

<td>1952-53</td>

<td>40</td>

<td>371</td>

<td>(1949-53) 75</td>

<td>20,2</td>

</tr>

<tr>

<td>1953-54</td>

<td>37</td>

<td>646</td>

<td>(1950-54) 127</td>

<td>19,6</td>

</tr>

<tr>

<td>1954-55</td>

<td>33</td>

<td>495</td>

<td>(1951-55) 177</td>

<td>35,7</td>

</tr>

<tr>

<td>1955-56</td>

<td>34</td>

<td>541</td>

<td>(1952-56) 116</td>

<td>21,4</td>

</tr>

<tr>

<td>1956</td>

<td>46</td>

<td>359</td>

<td>(1952-56) 98</td>

<td>27,2</td>

</tr>

<tr>

<td>1957-58</td>

<td>31</td>

<td>510</td>

<td>(1954-58) 148</td>

<td>29,0</td>

</tr>

<tr>

<td>1958-59</td>

<td>25</td>

<td>350</td>

<td>(1955-59) 96</td>

<td>27,4</td>

</tr>

<tr>

<td>1959-60</td>

<td>32</td>

<td>561</td>

<td>(1956-60) 95</td>

<td>16,9</td>

</tr>

<tr>

<td>1960-61</td>

<td>29</td>

<td>763</td>

<td>(1957-61) 152</td>

<td>19,9</td>

</tr>

<tr>

<td>1961-62</td>

<td>28</td>

<td>472</td>

<td>(1958-62) 113</td>

<td>23,9</td>

</tr>

<tr>

<td>1962-63</td>

<td>27</td>

<td>252</td>

<td>(1959-63) 79</td>

<td>31,3</td>

</tr>

<tr>

<td>1964-65</td>

<td>29</td>

<td>445</td>

<td>(1961-65) 93</td>

<td>20,8</td>

</tr>

<tr>

<td>1965-66</td>

<td>36</td>

<td>652</td>

<td>(1962-66) 114</td>

<td>17,4</td>

</tr>

<tr>

<td>1966-67</td>

<td>30</td>

<td>570</td>

<td>(1963-67) 109</td>

<td>19,1</td>

</tr>

<tr>

<td>1968</td>

<td>21</td>

<td>409</td>

<td>(1964-68) 99</td>

<td>24,2</td>

</tr>

<tr>

<td>1969-72</td>

<td>28</td>

<td>362</td>

<td>(1968-72) 47</td>

<td>12,9</td>

</tr>

<tr>

<td>1973</td>

<td>22</td>

<td>584</td>

<td>(1969-73) 232</td>

<td>39,7</td>

</tr>

<tr>

<td>1974</td>

<td>8</td>

<td>445</td>

<td>(1970-74) 96</td>

<td>21,5</td>

</tr>

<tr>

<th>Total</th>

<th>972</th>

<th>14.184</th>

<th>3.944</th>

<th>27,8</th>

</tr>

</tbody>

</table>

La Tabla 6 muestra la distribución de frecuencias de las referencias bibliográficas según el idioma de publicación y por tipo documental. El inglés, como lengua oficial de la ciencia, es el idioma más frecuentemente utilizado, con un total de 9.423 referencias, segundo del español con 2.206, le siguen alemán, francés y un grupo de idiomas con unas frecuencias mucho más bajas.

En lo que se refiere al tipo documental, el artículo de revista es el de mayor frecuencia como fuente de difusión de los resultados de la actividad científica, con 11.075 (78,1%) referencias, a continuación le siguen las monografías con 2.319 (16,3%), patentes en número de 227 (1,6%), comunicaciones a congresos 212 (1,5%), tesis doctorales 151 (1,1%) y un grupo de 200 (1,4%) referencias de muy variada tipología documental (Tabla 6).

<table><caption>

**Tabla 6: Distribución frecuencias de las referencias bibliográficas según el idioma de la publicación y por tipología documental**</caption>

<tbody>

<tr>

<th>Idioma</th>

<th>Artíc. revista</th>

<th>Monogr.</th>

<th>Patentes</th>

<th>Comunic. congresos</th>

<th>Tesis Doct.</th>

<th>Otros</th>

<th>Total</th>

<th>%</th>

</tr>

<tr>

<td>Inglés</td>

<td>7.871</td>

<td>1.332</td>

<td>42</td>

<td>79</td>

<td>16</td>

<td>63</td>

<td>9.423</td>

<td>66,4  
</td>

</tr>

<tr>

<td>Español</td>

<td>1.418</td>

<td>461</td>

<td>1</td>

<td>78</td>

<td>121</td>

<td>127</td>

<td>2.206</td>

<td>15,6  
</td>

</tr>

<tr>

<td>Alemán</td>

<td>1.053</td>

<td>224</td>

<td>147</td>

<td>7</td>

<td>3</td>

<td>3</td>

<td>1.437</td>

<td>10,1  
</td>

</tr>

<tr>

<td>Francés</td>

<td>405</td>

<td>237</td>

<td>9</td>

<td>27</td>

<td>6</td>

<td>4</td>

<td>688</td>

<td>4,9  
</td>

</tr>

<tr>

<td>Otros</td>

<td>328</td>

<td>45</td>

<td>28</td>

<td>21</td>

<td>5</td>

<td>3</td>

<td>430</td>

<td>3  
</td>

</tr>

<tr>

<th>Total</th>

<th>11.075</th>

<th>2.319</th>

<th>227</th>

<th>212</th>

<th>151</th>

<th>200</th>

<th>14.184</th>

<td>  
</td>

</tr>

</tbody>

</table>

## Discusión

### Limitaciones

Aunque la revista Ciencia, como se comentó al principio, reflejaría una gran parte de la producción científica generada por los investigadores españoles exiliados en Hispanoamérica, es lógico pensar que no recoja toda la producción científica. La aportación de este trabajo permitiría tener conocimiento de la situación de la producción científica de forma aproximada. Pues hemos de suponer que muchos autores publicarían en otras revistas (en inglés, en otros idiomas e incluso en otras revistas en español), también publicarían en otro tipo de fuentes distintas a las revistas científicas (libros, actas de congresos, tesis doctorales, etc.).

Otras limitaciones se han debido a la imposibilidad de aplicar algunos indicadores, bien por el tipo de fuente utilizada (una sola revista) o por los años de cobertura de la misma. Así no se han podido aplicar los indicadores de circulación y dispersión por no estar recogida en bases de datos (índice de productividad circulante, índice de circulación, índice de difusión internacional o la dispersión de Bradford). Por las mismas razones no se pudo aplicar los indicadores de repercusión o impacto (índice de visibilidad, índice de influencia, vida media de las citas o factor de impacto).

### A los resultados

Se ha podido constatar que la producción científica se ha mantenido constante a lo largo de los años de vida de la revista, siendo la Biología la que más trabajos aportó (325), seguida de Química (291) y Medicina (216). De todas formas hay que tener en cuenta que se trata de la producción científica de una serie de áreas de conocimiento a través de una única revista.

Respecto a la clasificación por materia, no era prioritario hacer en estos momentos un análisis riguroso, ya que su estudio requiere una atención aparte y en mayor profundidad. Sin embargo se abre, con ello, un campo de investigación muy interesante, que será abordado en breve por los autores, con el que se pretende dar respuesta a preguntas como: ¿Qué científicos españoles en el exilio continuaron con la línea de investigación iniciada en España? ¿Qué razones se podrán hallar que expliquen por qué algunos de estos investigadores cambiaron de línea de investigación? ¿Se adaptaron a la diversidad científica iniciada en la mitad del siglo XX? ¿Por qué aparecen pocos artículos de Física y de Matemáticas? Y a muchas otras preguntas.

La producción total fue de 972 artículos, lo que supone una media de 2,28 trabajos por autor. Una producción baja, ya que el índice de transitoriedad o de autores ocasionales fue del 66,56 % (Fig. 1). Hay que tener presente que el número de autores ocasionales (autores con una sola publicación en el estudio) es un índice que da idea de la consolidación de la actividad científica en un país, área o disciplina ([Shubert y Glänzel, 1991](#shu91)). Por consiguiente, un % alto de autores ocasionales sería preocupante y deseable su disminución. Los resultados del presente estudio han proporcionado un índice de transitoriedad relativamente alto, si se compara con el de otros estudios más recientes ([Pulgarín _et al._, 2003](#pul03), [2004](#pul04)), aunque hay excepciones en las que el % de autores con un solo trabajo es superior al 80 % ([Álvarez-Solar, _et al._, 1998](#alv98)).

En cuanto a la colaboración científica tan solo 360 de los trabajos (37%) se publicaron en colaboración. Mientras que el índice de cooperación o número de firmas/trabajo fue de 1,51. Un porcentaje y un índice bajos si se tiene en cuenta que la materia es científica, si bien queda justificado por la época inicial y media de la revista (décadas 40 a 60), donde todavía, podríamos decir, que no tiene efecto la ley 80/20 de Price, que viene a decir que en ciencia el 80% de los trabajos se realizan en colaboración, quedando reflejado a la hora de contar las firmas/trabajo ([López-Piñeiro y Terrada, 1992c](#lop92c)).

Se ha podido observar la existencia de varios grupos de investigación, que podrían muy bien constituir colegios invisibles (aunque la confirmación quedaría pendiente de la realización de investigaciones posteriores en mayor profundidad). Este hecho es muy importante, desde un punto de vista de la sociología y la historia de la ciencia, ya que entraña un estudio del comportamiento de los científicos agrupados para investigar. Verificar este fenómeno, dado con bastante frecuencia en la literatura científica, conlleva un análisis más profundo de las causas, circunstancias políticas y otras no menos importantes dentro de la ciencia de la información. De todas formas es muy interesante que en este pequeño estudio bibliométrico se haya podido detectar la posibilidad de su existencia.

El índice de Price ([1965](#pri65)), obtenido como medida de la obsolescencia de las referencias bibliográficas, ha sido por lo general bajo, a excepción de los 5 primeros años que es bastante alto. El índice para la totalidad de las referencias fue de 27,8%. Decimos que el índice de Price es bajo, por lo general, si lo comparamos con el obtenido en otros estudios, cuyos índices de Price superan el 35 % de media ([Terrada _et al._, 1979](#ter79); [López-Piñero y Terrada, 1994](#lop94); [Álvarez-Solar _et al._, 1998](#alv98); [Acea-Nebril _et al._, 2000](#ace00)). Este comportamiento puede tener su justificación en que, los científicos españoles exiliados, dispusieran de una literatura abundante y actualizada, en los primeros años, y a medida que fue pasando el tiempo, tal vez debido a penurias económicas u otras causas, no tuvieran a su alcance toda la literatura necesaria y, sobre todo, actualizada.

El número de referencias por artículo (14,6), considerando los años estudiados y las circunstancias que rodearon a este grupo de científicos, no se puede considerar bajo. Trabajos sobre las incidencias bibliográficas en las revistas científicas indican un patrón abstracto de media igual a 15 referencias por artículo ([Price, 1965](#pri65)). En estudios posteriores, sobre áreas médicas, se han obtenido medias de 21 referencias por artículo ( [Lópe-Piñero y Terrada, 1994](#lop94)) y de 24 referencias por artículo ( [Álvarez-Solar _et al._, 1998](#alv98)).

De todas formas dispusieron de bastante bibliografía publicada en inglés, ya que como lengua oficial de la ciencia, fue el que con más frecuencia utilizaron (9.423 referencias), seguido del español con 2.206 referencias, alemán, francés y un grupo de idiomas con frecuencias mucho más bajas. Esto quiere decir que las fuentes principales de donde bebieron, este grupo de científicos españoles, para desarrollar sus investigaciones, era literatura publicada en inglés, fundamentalmente. Este bajo número de referencias en español, respecto a las utilizadas en inglés, indica un índice de aislamiento bajo (15%). Un índice de aislamiento bajo es conveniente para aquellos países con producción científica de segundo orden, ya que implica que los científicos están utilizando un % de literatura internacional por encima del 85% ( [López-Piñero y Terrada, 1994](#lop94)).

Y en lo referente al tipo documental, estuvieron en sintonía con las formas de la ciencia, ya que el artículo de revista científica es el que con mayor frecuencia utilizaron (11.075 referencias), seguido de monografías (2.319); siendo bastante menos frecuente la utilización de otros tipos de documentos como referencia bibliográficas.

## Conclusiones

En los 29 volúmenes que publicó la revista Ciencia, entre 1940 y 1974, aparecieron publicados un total de 972 artículos. Estos contenían 14.184 referencias bibliográficas.

El mayor número de artículos corresponde a Biología (325), seguido de Química (291), Medicina (216), Geología (64), Física (35), Matemáticas (29) y 12 artículos a otras materias.

La media del número de referencias por artículo fue de 14,6, si bien fue bastante variable a lo largo de los años.

El número de autores con un solo trabajo fue de 436 y el resto (219) publicaron dos o más artículos. La media del número de trabajos por autor resultó ser de 2.28\. El autor más productivo fue Giral, F., con 45 artículos.

El número de firmas/trabajo resultante fue de 1,51\. El número de trabajos en colaboración fue de 360 (37%), destacando un solo trabajo firmado por diez autores.

Se ha detectado la existencia de grupos de investigación encabezados por los autores más productivos, sobre todo en materias que tienen que ver con la Química, Bioquímica y Biología, en sus distintas especialidades.

El índice de Price (medida de la obsolescencia de las referencias bibliográficas), de la totalidad de las referencias fue de 27,8%, si bien el resultado anual fue muy variable. Los 5 primeros años tuvieron un índice muy alto y el resto bajo.

El inglés, como lengua oficial de la ciencia, fue el más utilizado, con un total de 9.423 referencias, seguido del español con 2.206 (índice de aislamiento del 15,6). Otro grupo de idiomas, como alemán o francés, tuvieron frecuencias más bajas.

Entre la tipología documental destacan los artículos de revistas (11.075 referencias), siguiendo a gran distancia las monografías (2.319 referencias) y en menor número otros tipos de de documentos.

## Referencias

*   <a id="ace00"></a>Acea-Nebril, B., Figueira-Moure, A. y Gómez-Freijoso, C. (2000). Artículos originales publicados en la _Revista Española de Enfermedades Digestivas_ (1993-1998): autoría, demora en la publicación y referencias bibliográficas. _Revista Española de Enfermedades Digestivas_, **92**, 573-579.
*   <a id="alv98"></a>Álvarez-Solar, M., López-González, M. L. y Cueto-Espinar, A. (1998). Indicadores bibliométricos, análisis temático y metodológico de la investigación publica en España sobre epidemiología y salud pública. _Medicina Clínica (Barcelona)_, **111**(14), 529-535.
*   <a id="bur60"></a>Burton, R.E. y Kleber, R.W. (1960). The half-life of some scientific and technical literature. _American Documentation_, **11**(1), 18-22.
*   <a id="car02"></a>Carapeto, C., Pulgarín, A. y Cobos, J.M. (2002). Ciencia. Revista hispano-americana de ciencias puras y aplicadas (1940-1975). _LLULL_, **25**, 329-368.
*   <a id="fer97"></a>Fernández-Guardiola, A. (1997). Las Neurociencias en el exilio español en México. Colección “_Encuentros Iberoamericanos_”. México D.F.: Impresora y Encuadernación.
*   <a id="gir94"></a>Giral, F. (1994). _Ciencia española en el exilio (1939-1989). El exilio de los científicos españoles._ Barcelona: Anthropos.
*   <a id="ord02"></a>Ordóñez-Aparicio, Mª. M. (2002). _[Los científicos del exilio español en México: un perfil](http://clio.rediris.es/clionet/articulos/cientificos.htm)_. Retrieved 15 February, 2004 from the Clio Website at http://clio.rediris.es/clionet/articulos/cientificos.htm
*   <a id="lop92a"></a>López-Piñero, J.M. y Terrada, M.L. (1992a). Los indicadores bibliométricos y la evaluación de la actividad médico científica (I). Usos y abusos de la bibliometría. _Medicina Clínica (Barcelona)_, **98**(2), 64-68.
*   <a id="lop92b"></a>López-Piñero, J.M. y Terrada, M.L. (1992b). Los indicadores bibliométricos y la evaluación de la actividad médico científica (II). La comunicación científica en las distintas áreas de la ciencia médica. _Medicina Clínica (Barcelona)_, **98**(3), 101-106.
*   <a id="lop92c"></a>LÓpez-Piñero, J.M. y Terrada, M.L. (1992c). Los indicadores bibliométricos y la evaluación de la actividad médico científica (III). Los indicadores de producción, circulación, dispersión, consumo de la información y repercusión. _Medicina Clínica (Barcelona)_, **98**(4), 142-148.
*   <a id="lop94"></a>López-Piñero, J. M.y Terrada, M. L. (1994). El consumo de información científica nacional y extranjera en las revistas médicas españolas: un nuevo repertorio destinado a su estudio. _Medicina Clínica (Barcelona)_, **102**(3), 104-112.
*   <a id="pri65"></a>Price, D.J. de S. (1965). Networks of scientific papers. _Science_, **149**(3683), 510-515.
*   <a id="pri70"></a>Price, D.J. de S. (1970). Citation measures of hard science, soft science, technology, and nonscience. En C.E. Nelson and D.K. Pollock (Eds.) _Communication among scientists and engineers_. (pp. 3-22). Lexington, MA: Heath.
*   <a id="pul03"></a>Pulgarín-Guerrero, A., González-Calatraba, I.; Escalona-Fernández, M.I. y Pérez-Pulido, M. (2003). _Estudio bibliométrico de la producción científica y tecnológica de la Universidad de Extremadura. Análisis de la difusión alcanzada en bases de datos internacionales: período 1991-2000._ Cáceres: Universidad de Extremadura.
*   <a id="pul04"></a>Pulgarín-Guerrero, A., González-Calatraba, I.; Escalona-Fernández, M. I. y Pérez-Pulido, M. (2004). _Estudio bibliométrico de la producción científica de la Universidad de Extremadura. Análisis de la difusión alcanzada en bases de datos nacionales: período 1974-2000._ Cáceres: Universidad de Extremadura
*   <a id="pul97"></a>Pulido, M. (1997). Obligaciones éticas de los autores: referencias bibliográficas, criterios de originalidad y publicación redundante y derechos de la propiedad intelectual. _Medicina Clínica (Barcelona)_, **109**(17), 673-676.
*   <a id="shu91"></a>Shubert, A. y Glänzel, W. (1991). Publication dynamics: models and indicators. _Scientometrics_, **20**(1), 317-331.
*   <a id="ter79"></a>Terrada, M. L., de la Cueva, A. y Añon R. (1979). La obsolescencia de la información científica en las publicaciones médicas españolas. _Revista Española de Documentación Científica_, **2**(1), 9-23.

#### <a id="abstract"></a>Bibliometric analysis of the scientific literature published in  
_"Ciencia. Revista hispano-americana de ciencias puras y aplicadas"_ (1940-1974)

#### **Abstract**

> This paper reports the pilot stage of a Project whose objective is to analyse the scientific output of the journal “Ciencia” from its appearance (1940) until its closure (1974). The journal constituted the formal channel for the dissemination of science among Spanish researchers in exile in Hispano-America due to the Spanish civil War (1936-1939). The original articles published in three of the seven sections into which the journal was divided – Modern science (section I), Original communications (section II), and Applied science (section IV) – are studied, together with the bibliographical references contained in those articles. The number of articles analysed was 972, and of bibliographical references 14,184.