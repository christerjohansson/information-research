<header>

#### vol. 23 no. 2, June, 2018

</header>

<article>

# Selecting digital children's books: an interview study

## [Kirsten Schlebbe](#author)

> **Introduction.** The market for digital children's books is growing steadily. But the options vary in terms of quality and parents looking for suitable apps for young children can get overwhelmed quickly. This study seeks to answer how families search for suitable applications and what aspects are important to them while selecting.  
> **Method.** Semi-structured interviews were conducted with eight families who have already gained experience in the use of digital children's books. The investigation focused on children aged one to seven years. Families were asked about specific approaches during the search for and the selection of appropriate media offerings.  
> **Analysis** Different coding methods were applied for data analysis and analytical memos were used throughout the research process.  
> **Results.** Search strategies and various selection criteria for selecting digital children's books could be identified. The analysis indicates that these criteria relate more to external characteristics and safety or privacy issues than to content-related features.  
> **Conclusions.** While the statements of the parents interviewed for selecting printed children's books confirm the results of previous studies, their approaches for selecting digital children's books are significantly different. The interviews suggest that families are still insecure about the applications and their selection.

<section>

## Introduction

In 2016, 41% of the families in Germany with a child at the age of three to thirteen years owned a tablet computer. If parents use such a device, it may pass quickly into the hands of their children: 19% of the three to five year olds are allowed to use a tablet computer regularly ([Guth and Schäffner, 2016](#gut16)). In the UK, the numbers are even higher: 55% of the three to four year olds and 67% of the five to seven year olds use a tablet computer on a regular basis ([Ofcom, 2016](#ofc16)). Colourful pictures, sounds and interactive app elements fascinate the children and the touchscreen operation seems to be very intuitive, even for the youngest.

By now, there are numerous applications for playing, learning and reading that have been developed specifically for children. This also includes digital children's books that combine stories and illustrations with interactive elements. Therefore, more and more parents are using e-books and book apps in the literacy education of their children in addition to the reading practice with traditional print media ([Aufenanger, 2014](#auf14); [Stiftung Lesen, 2012](#sti12); [Vaala and Takeuchi, 2012](#vaa12)). But the growing range of digital children's books varies in terms of quality and parents looking for suitable products for young children can get overwhelmed quickly. So how do families find suitable applications and what aspects are particularly important to them while selecting?

The search and selection process for children's e-books and book apps is a relevant topic not only for developers and manufacturers but also for online stores and public libraries ([McKay _et al._, 2012](#mck12)). Therefore, the purpose of this study is to investigate the behaviour of families with young children during the search for and the selection of digital children's books. Search strategies used and selection criteria are identified, and the behaviour while searching for and selecting printed books for children is also examined. Semi-structured interviews with parents of children aged one to seven years were used to gather information about the families' behaviour and opinions. Shortly after conducting the interviews, the results were published in German on the institutional repository of Humboldt-Universität zu Berlin ([Schlebbe, 2015](#sch15)).

The paper is structured as follows. In the next section, previous research on the selection of children's books in the digital as well as in the analogue context is presented. This is followed by information on the methods of the study, the results, discussion and conclusions.

## Previous research on the selection of children's books

Numerous studies in the field of library and information science investigate the search for and the selection of printed literature by adult readers, particularly in public libraries (for a literature review see, for example, [Ooi and Li Liew, 2011](#ooi11)). The scientific study of children's reading interests also has a long research tradition ([Reutzel and Gali, 1998](#reu98)). But the majority of these studies examine the interests and preferences of school-aged children (_e.g._, [Kragler, 2000](#kra00); [Mohr, 2006](#moh06); [Raqi and Zainab, 2008](#raq08); [Reutzel and Gali, 1998](#reu98)).

Robinson, Larsen, Haupt and Mohlmann ([1997](#rob97)) presume that the lack of studies in the field of younger children, _inter alia_, is because of the results of previous studies in the 1970s ([Chiu, 1973](#chi73); [Beta Upsilon Chapter of Pi Lambda Theta, 1974](#bet74)). These implied that children under the age of eight years show little consistency in their reading preferences. This assumption is contradicted by the results by Robinson _et al._ ([1997](#rob97)). For their study, 102 children between four and six years were observed while selecting printed books from a given set of items over a period of seven weeks. It turned out that the children showed a tendency to pick books from the fantasy genre and to already known content. In addition, the difficulty of the text contained served as a criterion for their decision. Robinson _et al._ therefore assumed that genre preferences and complex strategies for selecting books already exist for young children ([1997](#rob97)). This hypothesis was supported by the results of an online survey on the reading habits of young people and children in England ([Maynard, Mackay and Smyth, 2008](#may08)). For this study, 502 children at the age of four to seven years were interviewed about the reasons for their book selection. The main deciding factors were the book cover (48.7% approval) and illustrations contained (25.1%). Other reasons for selection were an interesting sounding title (37.7%), recommendations from the personal environment (27.9%) as well as the popularity through film and television (29.9%) ([Maynard _et al._, 2008](#may08)).

In an extensive qualitative study Cunningham ([2013](#cun13)) explored the search and selection behaviour of children and their parents in public libraries and bookstores. This study also demonstrates that even young kids develop certain preferences regarding the selection of books. The study states that the selection behaviour of younger children often depends on serendipity principles. The observations showed that the children were moving rather aimlessly between the shelves: _'Few of these youngest readers browsed shelf-by-shelf or even row-by-row; instead, they caromed from one spot to another'_ ([Cunningham, 2013](#cun13), p. 4). The likelihood for selecting a book from the shelves thereby increased greatly when the cover was visible for the children. So the most important and often exclusive decision-making factor proved to be the design of the book cover ([Cunningham, 2013](#cun13)). This is not surprising when considering that the visual characteristics of the book often represent the only useful evaluation criterion for children that are not capable of reading. While examining the selection of books for preschool children it should be kept in mind that not only the children themselves but in most cases, parents, siblings and/or friends are involved in the search and selection of an appropriate title: _'for children, the practice of searching for a book is often far more social than individual. The process of search, interaction and decision making is often completed in collaboration with, or, along-side a parent or sibling.'_ ([Vanderschantz, Timpany, Hinze and Cunningham, 2014b](#van14b), p. 182). Cunningham also highlights the important role of family and friends in the book selection: _'For a child, finding a good book is rarely a solitary activity. Children visit bookstores and libraries accompanied by parents, siblings, and friends'_ ([Cunningham, 2013](#cun13), pp. 4-5). Often, children are not able to decode textual information themselves and therefore the parents offer recommendations and help to their children to find their way in the library or in the book shop. Additionally, in most cases they decided which pre-selected books were ultimately borrowed or bought ([Cunningham, 2013](#cun13)). In the study of Vanderschantz _et al._ ([2014b](#van14b)) social aspects of the selection of children's books are also central. The investigation accompanied seventeen families with children aged two to eleven years during their visits in a library and in a book shop. During the observations it became clear that the search for suitable books for the studied children aged between two and five years was carried out in close contact with the accompanying parents: _'collaborative search was the prominent method of identifying books'_ ([Vanderschantz _et al._, 2014b](#van14b), p. 188). In the selection of books the children also showed a strong focus on the criterion _book cover_. Also the back cover, the spine and illustrations contained were considered ([Vanderschantz _et al._, 2014b](#van14b)).

Two other investigations show a stronger focus on the perspective of parents selecting appropriate children's books. The German Publishers and Booksellers Association [Börsenverein des Deutschen Buchhandels] asked purchasers of children's books (n = 1.978) about their selection criteria. The three criteria most frequently mentioned were the child's will, an exciting narrative and the transmission of knowledge ([Börsenverein des Deutschen Buchhandels, 2013](#boe13)). Gönen, Özen Altinkaynak, Sanli and Kent Kükürtcü ([2015](#goe15)) surveyed 121 Turkish parents with children aged between three and five years for their selection of children's books specifically for reading out loud. The most important criteria mentioned here were the age appropriateness of the content, the design of text and illustrations, the subject matter, the child's will and recommendations ([Gönen _et al._, 2015](#goe15)). One problem of both studies may be the given pre-selection of possible criteria: _'pre-established categories of answers … rarely capture the complexities of readers' experiences nor provide explanatory detail for readers' actions'_ ([Ooi and Li Liew, 2011](#ooi11), p. 749).

In summary, the selection of printed books for young children is a process in which both the children themselves and their families are involved. The decision on the selection of a book can be taken relatively independently by the children or in cooperation with their parents, but also exclusively by the parents. Children base their selection mainly on visual evaluation criteria because of a lack of reading ability while parents pay more attention to content-related criteria like the age appropriateness or the subject matter. However, they also take into account the preferences of the child during their decision.

Few studies have been undertaken on the search for and the selection of digital book content in the fiction genre ([McKay, Smith and Chang, 2014](#mck14)). In addition, the studies deal almost exclusively with the search for and the selection of digital books by adults for their own use (_e.g._, [Hinze, McKay, Vanderschantz and Timpany, 2012](#hin12); [McKay _et al._, 2012](#mck12); [McKay _et al._ 2014](#mck14); [Vanderschantz, Hinze, Cunningham, Timpany and McKay, 2011](#van11)). Therefore, the results cannot be transferred directly to the procedures of families for searching for and selecting digital children's books. Studies on the selection of children's books in the digital domain deal mainly with the search behaviour of children in online public access catalogues (OPACs) (_e.g._, [Borgman, Hirsh, Walter and Gallagher, 1995](#bor95)) or digital libraries (_e.g._, [Druin, 2005](#dru05); [Reuter, 2007](#reu07); [Reuter and Druin, 2004](#reu04); [Theng, Mohd-Nasir, Thimbleby, Buchanan and Jones, 2000](#the00); [Vanderschantz, Hinze and Cunningham, 2014a](#van14a)). However, these studies apply only to school-aged children. For younger children no independent use of search interfaces of virtual or digital libraries is assumed.

A more recent qualitative study ([Kühn, Lampert, Christof, Jarmula and Maaß, 2015](#kue15)) investigated the tablet computer use of twelve families with children aged between two and five years. Regarding the selection of software applications for children in general, it became clear that the children do not select them on their own. Some parents reported that they looked at the content descriptions of the online stores and tested the applications for suitability for children and advertising contained in advance. Broekman, Piotrowski, Beentjes and Valkenburg ([2016](#bro16)) also focused on the parental perspective on apps for pre-school children. Their study demonstrates that parents show five overarching needs when selecting apps for their children: _'the need for independent entertainment, co-education, familiarity, a tailored challenge, and to pass time'_ ([Broekman _et al._, 2016](#bro16), p.149). Furthermore, their study indicates that these needs vary by different styles of parenting.

However, none of these studies examined the specific strategies used by families during the search for and the selection of digital books for toddlers or pre-schoolers. Hence, there is a clear need for research in this area and it is the purpose of the present study to address this need.

## Method

Earlier studies have shown that the selection of digital content for children seems to differ significantly from the selection of printed materials for children ([Kühn _et al._, 2015](#kue15); [Broekman _et al._, 2016](#bro16)). Therefore, an open and explorative approach to data collection was chosen: this study aims to gather information about the behaviour and opinions of families regarding the selection of digital children's books by using semi-structured interviews with parents of toddlers and pre-school children.

### Participant recruitment and sample

The required criteria for the selection of interview participants were:

*   parent or caregiver of at least one child aged one to seven years
*   experience with the use of e-books and/or book apps for children
*   knowledge of the search for and the selection of e-books and/or book apps for children

Physical and digital requests were used for the recruitment of participants. These included a notice posted in paediatric practices, day-care centres and family-friendly cafés in various districts of Berlin. Furthermore, the mailing list of the Humboldt-Universität zu Berlin and e-mail inquiries in the author's personal environment were used. Finally, a total of nine parents were interviewed (see table 1; anonymised data). It must be viewed critically that data saturation could not be reached because of the small number of cases.

<table><caption>Table 1: Sample</caption>

<tbody>

<tr>

<th>Family</th>

<th>Children (age in years)</th>

<th>Interviewee</th>

</tr>

<tr>

<td>A</td>

<td>Anton (3) & Adam (2)</td>

<td>Mother</td>

</tr>

<tr>

<td>B</td>

<td>Benjamin (2)</td>

<td>Mother</td>

</tr>

<tr>

<td>C</td>

<td>Christian (4)</td>

<td>Mother</td>

</tr>

<tr>

<td>D</td>

<td>Dina (3) & David (3 months)</td>

<td>Father</td>

</tr>

<tr>

<td>E</td>

<td>Emil (3) & Erik (1)</td>

<td>Mother</td>

</tr>

<tr>

<td>F</td>

<td>Frieda (7) & Felix (5)</td>

<td>Mother</td>

</tr>

<tr>

<td>G</td>

<td>Greta (7), Georg (5) & Gregor (5)</td>

<td>Father & Mother</td>

</tr>

<tr>

<td>H</td>

<td>Hannah (7), Hannes (5) & Hendrik (3)</td>

<td>Mother</td>

</tr>

</tbody>

</table>

In one case both parents could take part in the interview. In six cases only the mother and in one case only the father of the family was questioned. A larger proportion of male parents would have been desirable for contrasting. Also, the distribution of the children's sexes in the families interviewed was uneven with four female and ten male children. The distribution of other social characteristics, however, is quite balanced. The examined ages are represented relatively evenly. Three families live in more urban areas and five families in more rural ones. Overall, the families live in three different federal states of Germany: Baden-Württemberg (one family), North Rhine-Westphalia (six families) and Thuringia (one family). Except for one mother (family E), all parents are working full- or part-time. All parents have a high educational background and all surveyed families have a regular practice of reading to the children with traditional print media.

### Data collection and analysis

Because of the nationwide distribution of the subjects, three interviews were led face-to-face and five by phone. The length of the interviews varied between thirty and sixty minutes. All conversations have been structured on the basis of the field manual, which can be found in Appendix 1\. The interviews were recorded digitally and transcribed completely.

For the systematic analysis of data, different coding methods were used and all coding was done by a single researcher. In a first round of initial coding, in vivo codes were used to organize the data. These in vivo codes were then used to systematically create various descriptive codes regarding the research question. In a third step, focused coding was used to organize these descriptive codes into different categories. The developed coding system can be found in Appendix 2\. In addition, analytical memos were used throughout the research process

For data processing, storage and analysis the CAQDA-software [MAXQDA](https://www.maxqda.com/) was used. The cited families' statements were translated from German into English. The original quotations can be found in Appendix 3.

## Results

The structure of the presentation of the results follows the developed coding system. First, the context of use of digital children's books in the families interviewed is described. Then, results regarding the search process for new products are presented. After this, the selection criteria used by the parents are specified. Finally, the families' behaviour while searching for and selecting printed books for children is described and compared.

### The context of use

In the families interviewed the tablet PC is used by almost all children. Excluded here are only David, the son of family D, who is only three months old, and the one-year-old son, Erik, of family E. In general, the children have used the mobile device from a very young age with the beginning age of use varying between one and three years. If parents use a tablet PC, their children seem to quickly become active users, too:

> 'Well, they discover pretty quickly how to handle that [laughter].' (Mother E)

All children use the mobile device relatively regularly. The frequency of use varies from nearly daily to once a month. The frequency also depends on the context of use: While most families offer the tablet PC as a gaming and television alternative in everyday life, family C uses it almost exclusively on long trips and while travelling.

The parents regulate the duration and frequency of use, and want to avoid overuse:

> 'Of course we try to keep it within a limit, that she, she should not play with it too much.' (Father D)

The degree of independence of use varies between the families. While some children use the equipment solely with their parents, most children use the tablet PC relatively autonomously.

Apart from family G, all families use exclusively book apps, software applications that can be downloaded from an online store. Only family G uses interactive e-books for children, which are electronic books in a document format. The families interviewed own three to ten applications that have been downloaded specifically for the children. While the families A, B and C use almost exclusively apps from the reading area, only about half of the applications of the families D, G and H are attributable to that sphere. The families E and F use only one book app for children. However, this low number has to be seen in context with the rather low number of children's applications on the tablet PC as a whole in these families (three to four apps). Apps that do not originate from the reading area are either applications from the learning domain, which should playfully teach first literacy and numeracy skills, or games. Furthermore, most families use the devices to watch children's TV series through different online video channel apps.

Popular content for the children's book apps are called _Wimmelbücher_ (hidden object books), which are often used for younger children. Additionally, various digital versions of printed books are available.

### Searching for digital children's books

#### The general search procedure

As mentioned above, all families exclusively use the online sales portals of the tablet manufacturers for the search for and the acquisition of digital children's books. Although almost all families use the local public library, they are not or hardly familiar with their digital offer and do not use it:

> 'So we do not use that. I think it might even be that there is, I do not know exactly. But we do not use it, no.' (Mother A)

The search for digital children's books is performed solely by the parents. The children are generally not present at that time or only in the background. In this context, it is noticeable that in three of the eight families the responsibility for the (technical) use of the tablet PC is placed on the father:

> 'my husband is always doing this, I simply cannot do this [laughter].' (Mother A)

The search for new apps takes place at intervals of several weeks or even months. Some parents also report that they looked more often for new applications at the beginning of the use of the tablet PC. Their information seeking behaviour can be defined as browsing, in most cases. In addition, classical search queries are used during the search for known content or when following recommendations of other persons or institutions.

#### Information sources

From professional review services to Websites offering user-generated recommendations (_e.g._, [Digital Storytime](http://digital-storytime.com) or [Common Sense Media](https://www.commonsensemedia.org/)), numerous guidelines for the selection of digital children's books exist. But do families actually use such services?

While looking for new apps, various sources of information are used by the parents. Thereby, recommendations from other persons or institutions represent an important information source in the search and selection process. One can distinguish between recommendations within the online stores, from the personal environment and expert recommendations.

_Recommendations within the online stores._ The online stores of the manufacturers generally offer various recommendation services for their customers. Six of the eight families stated that they have relied on recommendations of this type. Most often the criterion of customer ratings was cited. Half of the families stated that they look at the reviews and comments from other customers and include them in their selection decision:

> 'I went to the customer reviews and looked up what were the most popular ones [apps].' (Mother F)

In addition to customer reviews, the recommendation services of online stores are used. Two of the eight families reported that they use system recommendations for search and selection:

> 'he [the husband] looks, uh, for recommendations. If you have a game then you can check what is recommended based on that.' (Mother A)

Parents also mentioned that some apps directly refer to other applications:

> 'then advertising appeared again and thus we arrived at this app My Animals.' (Mother B)

Finally, providers promote their products in the online stores with special offers or free trial versions.

_Recommendations from the personal environment._ Four of the eight families considered recommendations from the personal environment, for example, from friends or coworkers:

> 'mostly it was that we have discovered it … from friends who said: "we have discovered an app for our little ones, which is really nice."' (Mother C)

Parents use the recommended titles in online portals as search queries. Parents' use of terms like _'mostly'_ or _'most'_ makes clear that this criterion is of central importance for some of them.

_Expert recommendations._ Half of the families interviewed have used professional recommendations from media education before. A particularly popular medium in this context is a print magazine called _Eltern_ [_Parents_] published by _Gruner + Jahr_, which is named as a source of children's app recommendations by all of these parents. The only other source of professional recommendation information, named by just one participant, is a print magazine called _Baby und Familie_ [_Baby and Family_]. The reviews in these magazines serve as a source of information for parents while searching for appropriate digital children's book apps and other applications. The recommended titles are used as search queries in the online stores.

### Selecting digital children's books

#### The general procedure for the selection

When selecting a children's book app, a typical scheme can be found in almost all families interviewed. First, one or both parents look for appropriate applications. If a potentially useful app is found, the parents look at it and evaluate the content. Only if they are satisfied with it are the children given access to the application. If the parents do not like the content, the app will be removed from the device:

> 'he [the husband] always checks the apps first and tests whether it is good or not and sometimes he deletes it afterwards.' (Mother A)

In this, it becomes clear again that children are not involved in the selection process in general. As mentioned above, they are usually not present in the search and thus also in the selection of digital children's books; only the older son in family E, Emil, was active in the selection process. His mother reported:

> 'Sometimes the older one sits beside. But he just can tell because of the illustrations: "I want that with the tractor" or "I want that". But in principle, we decide what is going to be downloaded.' (Mother E)

#### Selection criteria

The interviews with the parents revealed that the families use different criteria during the selection of digital books for children. During the coding process these were broken down into two categories: external criteria and content-related criteria.

_External criteria._ When interviewing the parents for the present study it became apparent that the amount of advertising and in-app-purchases is a sensitive issue for all eight families. Indeed, three of the parents explicitly ensure that the applications contain little or no advertising. One reason given for this is that the child should not be distracted from the content of the app by the sudden emergence of advertising:

> 'that he does not necessarily see any crap and any advertising, yes, that we already know that there is no advertising in it.' (Mother A)

The parents also fear that the children could make unintended purchases by clicking the advertising:

> 'It is important of course that there is no, I would say, that no advertising appears, on which he then clicks accidentally, uh, and orders thirty other apps.' (Mother B)

The other five families interviewed are also aware of the risk of an unwanted purchase. But they accept the occurrence of promotions and in-app-purchases to a certain extent and refer to the blocking of the payment account for the children, who therefore cannot make unwanted purchases. A further criterion for the selection is the price of the application. Seven family representatives named the amount of the price as an aspect that they consider when choosing a digital children's book. Three of the eight families report that they only use free apps, which means they are not willing to spend money on digital children's books. It appears that at least to some extent these parents are aware of possible quality differences between free and commercial applications:

> 'There are some free ones, which I look at first. Although I see easily that the apps which I like more are not for free.' (Mother G)

The other five interviewees have bought commercial applications. However, it can be seen that these parents either show only a slight willingness to pay or the parents act very carefully and are only willing to pay if they can test the applications in advance:

> 'And the other ones I bought for a few Euros. But I have set value on it that it is something reasonable.' (Mother F)

_Content-related criteria._ In addition to external criteria, various content-related aspects play a role for the choice of digital books for children. These include a total of seven different aspects. The most important criterion related to the content seems to be the age appropriateness of the apps. Seven of eight parents reported that they consider the age recommendation of the online stores when choosing:

> 'I also pay attention to, uh, the age recommendation. I find it very important, that this is age appropriate.' (Mother F)

Several of the interviewed parents emphasise the special importance that this criterion has for them through the use of terms like _'the most significant'_, _'very important'_, _'in the first instance'_, _'definitive … clearly'_. One of the respondents indicates that he does not fully trust the suggestions of the online stores in this context:

> 'There is usually an age recommendation where you can conform to. But you still have to check the content.' (Father D)

The statements of other parents indicate that they do not trust the recommendations of the providers either, and therefore check independently whether the content seems appropriate for the age of their children. Another important selection criterion seems to be the familiarity with the content from the print media. Six of eight families reported that they have resorted to characters and stories from the printed book area while selecting:

> 'So he knows The Very Hungry Caterpillar from the kindergarten and then he [the husband] saw it and thought "Oh, he already knows this".' (Mother A)

A further aspect that the majority (five) of the parents cite as a criterion for the selection refers to the image, text and sound design of the applications. It is important to them that the app does not overwhelm the children. This statement refers to contained audio elements

> 'I try to make sure, uh, that it is not too much at once and this firefighter app sometimes crosses the border because you can click on a lot of things and so the background noise can become extremely high.' (Mother B),

but also to the design of text and image and to the information contained:

> 'So that they will not be bombarded with too much information.' (Mother F)

Because of the interactivity involved and the numerous image and sound elements, a greater proportion of parents obviously fears that they will overextend the children with the use of the applications. But a quieter design of the content seems to be in the interest of the parents themselves as well. One mother states:

> 'so if they [the apps] are accompanied heavily by music that is repeated all the time, well, then that is just very annoying [laughter].' (Mother E)

Half of the families reported that the topic of the digital story is important for the selection of the applications. Therefore, the parents set value on content that the children know from their everyday lives and their environment:

> 'Topics of their everyday life, which are police, firefighter, the airport … about farming, the forest and so on. So things from their environment.' (Mother A)

Furthermore they pay attention to topics that match the current individual interests of their children:

> 'First, we are looking for topics he is dealing with right now.' (Mother C)

Three parents report that they check possible language settings of the applications. In two cases, it is explicitly important to the parents that the applications are in German:

> 'that they are also in German. Because that is sometimes a bit difficult.' (Mother E)

Given that all children of the families interviewed grow up speaking German exclusively, it can be assumed that the other parents generally prefer applications for children in German, too. That this criterion is explicitly mentioned from two of the parents surveyed seems to point to negative experiences in this regard. However, another interviewed mother preferred applications that can be used in both German and English, as she uses the apps for simple first foreign language exercises with her son:

> 'So we have quite a lot of fairy tales, which are bilingual. Which means they are read out aloud in English. But it is not the case that we are practicing English massively. But sometimes he shows some interest and says "I want to listen to it" or "That is a funny language, what is that?" So he is quite curious about it.' (Mother C)

Two parents indicate specifically that they think that the learning effect of the applications is important:

> 'Of course we see that a little bit as an early reading and spelling skills training, and that is important for our selection, if it makes sense, uh, for the skills they have to develop for school.' (Mother A)

The quality of the illustrations is also named by two parents as a concrete selection criterion:

> 'this time I will look at the drawings very exactly. Because for the second [app], which I downloaded, I did not like the drawings.' (Mother B)

Table 2 shows an overview of the selection criteria mentioned by the families.

<table><caption>Table 2: Parents' selection criteria</caption>

<tbody>

<tr>

<th>Selection criteria</th>

<th>mentioned by … of 8 families</th>

</tr>

<tr>

<td colspan="2">

**External criteria**</td>

</tr>

<tr>

<td>Amount of advertising & in-app-purchases</td>

<td>8</td>

</tr>

<tr>

<td>Price</td>

<td>7</td>

</tr>

<tr>

<td colspan="2">

**Content-related criteria**</td>

</tr>

<tr>

<td>Age appropriateness</td>

<td>7</td>

</tr>

<tr>

<td>Content / format known from the print media</td>

<td>6</td>

</tr>

<tr>

<td>Avoid an excessive demand of the child through the app design</td>

<td>5</td>

</tr>

<tr>

<td>Topic</td>

<td>4</td>

</tr>

<tr>

<td>Available language settings</td>

<td>3</td>

</tr>

<tr>

<td>Educational value</td>

<td>2</td>

</tr>

<tr>

<td>Quality of illustrations</td>

<td>2</td>

</tr>

</tbody>

</table>

### Satisfaction with the selection made

To assess the suitability of these parental selection criteria and the possible need for professional supporting services in this area, parents were also asked about their satisfaction with the current selection of digital children's books. Four of eight surveyed families reported that they had deleted existing apps after installation. In one case this was done because of technical problems. In the other cases, the parents were not satisfied with the content, or the apps were not used by the children. Even parents who have not yet deleted apps see some of their previous choices in a rather critical light:

> 'Since I have downloaded more of this free stuff, I would say that I was disappointed by many things. That at first sight, I had hoped to get more from it. Where I looked at it and said: "Hm, well, I have actually expected something else".' (Mother G)

### In comparison: the search for and the selection of printed books for children

When searching for printed children's books, the families interviewed use primarily local book stores. In addition, almost all families visit the local public library regularly. During the search for and the selection of printed books the children are usually present and actively involved in the decision. This is especially true for the book selection in libraries:

> 'so actually the big difference in the selection is, that for conventional books we let the children decide, so they are allowed to simply browse through the library.' (Mother A)

While visiting the bookstore or library, the families show a classic browsing behaviour:

> 'and in the bookstore he is just, he first browses and looks at the illustrations.' (Mother C)

When selecting a printed book, the current interests of the child stand in the foreground. In addition to the topic, the parents pay particular attention to the illustrations contained and the relationship between text and images. Searching for a concrete title is done if the parents are looking for known content from their own childhood or if families have recommendations from the kindergarten or the personal environment:

> 'where he simply brings an idea from the kindergarten and says that he would like to read a certain book again … and then we go to the library and, um, yes, get this book for him.' (Mother H)

Another option for selection are book series, which include several titles.

In summary, it can be stated that the children of the families interviewed have a great impact on the selection of printed children's books. They are usually present during the search and are actively involved in the decision of which books should be bought or lent. Accordingly, it is primarily their individual interests and needs that are considered when selecting printed books.

## Discussion

For the families interviewed, the children are usually not involved in the selection of digital children's books. They are not present during the search or during the selection process. This confirms the results of Kühn _et al._ ([2015](#kue15)).

Searching for suitable digital content for children can be defined as browsing in most cases. In addition, specific search queries are used by the parents for finding known content or to follow recommendations by other persons or institutions. In these cases, title or keywords are entered as search queries in the online stores.

Recommendations play an important role in the search for appropriate apps for the parents interviewed. Firstly, recommendations of the providers are used. The reviews of other customers, recommendations from the systems and traditional advertising serve the parents for guidance. Equally important are recommendations from the personal environment, for example, from friends and colleagues. Professional recommendation services are also used as a source of information. However, it must be stressed that only half of the parents interviewed are familiar with only a few tools in this context, in form of one or two print magazines from the field of education. Public libraries or other professional institutions are not mentioned as a source of information by the parents interviewed.

The families use different criteria during the selection of digital books for children. An important selection criterion when choosing appropriate apps for children was the amount of advertising and in-app-purchases contained in the applications. So all parents are conscious about the risk of hidden costs. For this reason, parents usually block the purchase function. Three of the eight families reject applications with advertising and in-app-purchases in general. Another criterion that is important to the parents is the price of the applications. Three of the respondents only use free apps. Other parents show either a low willingness to pay or act very carefully when choosing an app and are only willing to pay for it if they can test the applications in advance. Overall, this behaviour suggests a certain mistrust of the quality of the applications. Looking at the content-related criteria, the age appropriateness is the most important criterion for most of the parents. Furthermore, they often rely on already known content and formats from print media. In addition to that, some of the families try to avoid a possible excessive demand of the children through the design of image and sound elements. Other aspects that play a role in the selection but are mentioned less frequently are the topic of the book app, the available language settings, the educational value and the quality of the illustrations. It should be noted that especially the aspects _topic_, _educational value_ and _quality of illustrations_ are not exclusive to the digital sphere, but also relate more generally to the field of children's books. The low number of responses, especially for the last two criteria, could be caused by the fact that parents perceive these aspects as self-evident selection criteria and have therefore not mentioned them explicitly.

However, it appears that the parents' criteria for selecting digital children's books often relate more to external characteristics and usability features than to content-related issues. In addition, the age appropriateness is critically reviewed and almost all parents interviewed sought to avoid possibly overloading their child with too many multimodal and interactive elements. Furthermore, the families often rely on secure reference points such as familiar content and characters from the printed book area. The frequent recourse to external recommendations could also be understood as an expression of uncertainty. This is assisted by the generally very cautious approach in choosing apps for children when candidate applications are checked precisely before being made available to the children.

The behaviour during the search for and selection of printed books for children seems to be significantly different for the families interviewed. As Cunningham ([2013](#cun13)) and Vanderschantz _et al._ ([2014b](#van14b)) observed in their studies, the children are present and actively involved in finding and selecting appropriate books. Parents and children report on joint visits of bookstores and libraries and describe a classic browsing behaviour, as observed by Cunningham ([2013](#cun13)). However, unlike for the digital children's books, during the selection of printed books the current interests of the child as well as content elements such as illustrations and their relationship with the text stand in the foreground. These findings coincide in large parts with the results of the survey conducted by the German Publishers and Booksellers Association ([Börsenverein des Deutschen Buchhandels, 2013](#boe13)).

Reference may be made at this point to a research project of the German Youth Institute (Deutsches Jugendinstitut), a representative survey that investigates what support parents of zero to five-year-olds expect and need when dealing with digital devices ([Feil, 2014](#fei14)). Results show that even parents of very young children show a strong information need regarding the digital behaviour of their children. For example, information about child protection settings of the device are very important for 66% of mothers and 55% of fathers, as are information about risks on the Internet (59% and 50%). These protection interests come first. They are followed by interest in information about child-friendly Websites and apps (55% and 44%) and age appropriate Internet use (52% and 40%) ([Grobbin and Feil, 2014](#gro14)).

## Conclusions

The aim of the present study was to investigate the behaviour of families with young children during the search for and the selection of digital children's books. Search strategies and specific selection criteria were identified and the behaviour while searching for and selecting printed books for children was also examined.

While the statements of the parents interviewed for selecting printed books confirm the results of previous studies, the approach for choosing digital children's books seems to be significantly different. The results suggest that the parents are insecure about the applications and their selection. Overall, a strong consideration of external factors such as the amount of advertising or the price of the application was found. Content-related criteria were mentioned less frequently.

Because of the difference in quality of digital children's books, a cautious approach while choosing is certainly appropriate. Nevertheless, besides privacy and safety aspects an additional focus on content-related quality criteria seems recommendable. Time will tell whether the attitude of parents will change and if they will develop a more confident handling of digital applications for their children. Maybe this will also lead to a greater involvement of the children in the search for and selection of children's book apps, as already observed in the field of printed children's books for decades.

When looking at the results, the methodical weaknesses of this study should be mentioned. For reasons of availability, theoretical sampling was done only partly during the selection of appropriate cases. As a consequence of this, the sample represents just one special user group: highly educated parents with a regular practice of reading to the child. It can be assumed that families without a regular practice or parents with a low educational background show a different behaviour. In addition, because of the small number of cases, data saturation could not be reached. Hence, a higher number of interviewees would have been desirable. In addition, further studies using different methods of data collection like observation or device tracking could complement this research.

The research in the field of the search for and the selection of digital books for children is currently still in its infancy. More detailed studies are needed to gain a deeper insight into the search and selection practices of families with young children. But it is sure that the relevance of this area of research will continue to grow. Digitisation has long since arrived in the everyday lives of families, as well as during story time in the nursery.

## Acknowledgements

The author would like to thank all the participants who volunteered for this study. The author is also grateful to the anonymous reviewers and the editor for their helpful comments.

## <a id="author"></a>About the author

**Kirsten Schlebbe** is a doctoral student and lecturer at the Berlin School of Library and Information Science, Humboldt-Universität zu Berlin, Unter den Linden 6, 10099 Berlin, Germany. She can be contacted at [schlebbe@ibi.hu-berlin.de](mailto:schlebbe@ibi.hu-berlin.de).

</section>

<section>

## References

<ul>

<li id="auf14">Aufenanger, S. (2014). Digitale Medien im Leben von Kindern und Herausforderungen f&uuml;r Bildung und Erziehung. <em>Fr&uuml;he Kindheit. Die ersten sechs Jahre, 17</em>(6), 8-18.</li>

<li id="bet74">Beta Upsilon Chapter of Pi Lambda Theta. (1974). Children's reading interests classified by age level. <em>The Reading Teacher, 27</em>(7), 694-700.</li>

<li id="bor95">Borgman, C.L., Hirsh, S.G., Walter, V.A. &amp; Gallagher, A.L. (1995). Children's searching behavior on browsing and keyword online catalogs: the Science Library Catalog project. <em>Journal of the American Society for Information Science, 46</em>(9), 663-684.</li>

<li id="boe13">B&ouml;rsenverein des Deutschen Buchhandels. (2013). <a href="http://www.webcitation.org/6kJgNO8kI"> <em>Kinder- und Jugendb&uuml;cher III. Marktentwicklung, Kaufverhalten, Konsumentenstrukturen und -einstellungen.</em></a> Frankfurt am Main, Germany: Börsenverein des Deutschen Buchhandels e.V. Retrieved from http://www.boersenverein.de/sixcms/media.php/976/KiJuBu_Presseversion_2013.pdf. (Archived by WebCite&reg; at http://www.webcitation.org/6kJgNO8kI))</li>

<li id="bro16">Broekman, F.L., Piotrowski, J.T., Beentjes, H.W.J. &amp; Valkenburg, P.M. (2016). A parental perspective on apps for young children. <em>Computers in Human Behavior, 63</em>, 142-151.</li>

<li id="chi73">Chiu, L.H. (1973). Reading preferences of fourth grade children related to sex and reading ability. <em>The Journal of Educational Research, 66</em>(8), 369-373.</li>

<li id="cun13">Cunningham, S.J. (2013). Children in the physical collection: implications for the digital library. In Trond Aalberg, Christos Papatheodorou, Milena Dobreva, Giannis Tsakonas &amp; Charles J. Farrugia, (Eds.), <em>Research and Advanced Technology for Digital Libraries - International Conference on Theory and Practice of Digital Libraries</em>, TPDL 2013, Valletta, Malta, September 22-26, 2013. Proceedings. (pp. 1-10). Cham, Switzerland: Springer International Publishing. (Lecture Notes in Computer Science, 8092).</li>

<li id="dru05">Druin, A. (2005). What children can teach us: developing digital libraries for children with children. <em>Library Quarterly, 75</em>(1), 20-41.</li>

<li id="fei14">Feil, C. (2014). Digitale Medien in der Lebenswelt von Klein- und Vorschulkindern. [Digital media in the lives of small and preschool children.] <em>Fr&uuml;he Bildung, 3</em>(2), 116-118.</li>

<li id="goe15">G&ouml;nen, M., &Ouml;zen Altinkaynak, S., Sanli, Z.S. &amp; Kent K&uuml;k&uuml;rtc&uuml;, S. (2015). The consideration of issues regarding families' selection on reading materials for their children and activities during presentation of these materials. <em>Hacettepe University Journal of Education, 30</em>(2), 30-41.</li>

<li id="gro14">Grobbin, A. & Feil, C. (2014).<a href="http://www.webcitation.org/6kJgUaMYk"> <em>Digitale Medien: Beratungs-, Handlungs- und Regulierungsbedarf aus Elternperspektive. Kurzbericht zur Teilstudie - Eltern mit 1- bis 8-j&auml;hrigen Kindern.</em></a> [Digital media: need for advice, action and regulation from the parents' perspective. Brief report on the sub-study - Parents with 1- to 8-year-old children.] Munich, Germany: Deutsches Jugendinstitut e.V. Retrieved from http://www.dji.de/fileadmin/user_upload/www-kinderseiten/1176/Kurzbericht_Internet-Elternperspektive-06-07-2014.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6kJgUaMYk)</li>

<li id="gut16">Guth, B. & Sch&auml;ffner, R. (2016).<a href="http://www.webcitation.org/6vnwNNnIy"> <em>Von gro&szlig;en und kleinen Screens: Basisdaten kindlicher Mediennutzung.</em></a> [From large and small screens: basic data on children's media use] [Presentation]. Retrieved from http://www.ip.de/fakten_und_trends/zielgruppen/kinderwelten/von_grossen_und_kleinen_screen.cfm (Archived by WebCite&reg; at http://www.webcitation.org/6vnwNNnIy)</li>

<li id="hin12">Hinze, A., McKay, D., Vanderschantz, N., Timpany, C. &amp; Cunningham, S.J. (2012). Book selection behavior in the physical library. In <em>Proceedings of the 12th ACM/IEEE-CS Joint Conference on Digital Libraries</em> JCDL '12 (pp. 305-314). New York, NY: ACM.</li>

<li id="kra00">Kragler, S. (2000). Choosing books for reading: an analysis of three types of readers. <em>Journal of Research in Childhood Education, 14</em>(2), 41-133.</li>

<li id="kue15">K&uuml;hn, J., Lampert, C., Christof, L., Jarmula, A.-M. &amp; Maa&szlig;, S. (2015).<a href="http://www.webcitation.org/6kJgaQuay"> <em>Mobile Internetnutzung von Kindern und Jugendlichen. Eine qualitative Studie zur Smartphone- und Tablet-Nutzung von Zwei- bis 14-J&auml;hrigen.</em></a> [Mobile Internet use of children and adolescents. A qualitative study on the smartphone and tablet usage of 2-14 year olds.] Hamburg, Germany: Verlag Hans-Bredow-Institut (Arbeitspapiere des Hans-Bredow-Instituts; 35). Retrieved from https://www.hans-bredow-institut.de/webfm_send/1108 (Archived by WebCite&reg; at http://www.webcitation.org/6kJgaQuay) </li>

<li id="may08">Maynard, S., Mackay, S. &amp; Smyth, F. (2008). A survey of young people's reading in England: borrowing and choosing books. <em>Journal of Librarianship and Information Science, 40</em>(4), 239-253.</li>

<li id="mck12">McKay, D., Buchanan, G., Vanderschantz, N., Timpany, C., Cunningham, S.J. &amp; Hinze, A. (2012). Judging a book by its cover: interface elements that affect reader selection of ebooks. In Vivienne Farrell, Graham Farrell, Caslon Chua, Weidong Huang, Raj Vasa &amp; Clinton Woodward, (Eds.), <em>Proceedings of the 24th Australian Computer-Human Interaction Conference</em> OzCHI '12. (pp. 381-390). New York, NY: ACM.</li>

<li id="mck14">McKay, D., Smith, W. &amp; Chang, S. (2014). Lend me some sugar: borrowing rates of neighbouring books as evidence for browsing. In <em>Proceedings of the 14th ACM/IEEE-CS Joint Conference on Digital Libraries</em> JCDL (pp. 145-154). Piscataway, NJ: IEEE.</li>

<li id="moh06">Mohr, K.A. (2006). Children's choices for recreational reading: a three-part investigation of selection preferences, rationales, and processes. <em>Journal of Literacy Research, 38</em>(1), 81-104.</li>

<li id="ofc16">Ofcom. (2016).<a href="http://www.webcitation.org/6vnwzxF6B"> <em>Children and parents: media use and attitudes report.</em></a> London: Ofcom. Retrieved from https://www.ofcom.org.uk/__data/assets/pdf_file/0034/93976/Children-Parents-Media-Use-Attitudes-Report-2016.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6vnwzxF6B)</li>

<li id="ooi11">Ooi, K. &amp; Li Liew, C. (2011). Selecting fiction as part of everyday life information seeking. <em>Journal of Documentation, 67</em>(5), 748-772.</li>

<li id="raq08">Raqi, S.A. &amp; Zainab, A.N. (2008). Observing strategies used by children when selecting books to browse, read or borrow. <em>Journal of Educational Media &amp; Library Sciences, 45</em>(4), 483-503.</li>

<li id="reu04">Reuter, K. &amp; Druin, A. (2004). Bringing together children and books: an initial descriptive study of children's book searching and selection behavior in a digital library. In <em>Proceedings of the 67th American Society for Information science and Technology (ASIST) annual meeting</em>. (pp. 339-348). Medford, NJ: Information Today.</li>

<li id="reu07">Reuter, K. (2007). Assessing aesthetic relevance: children's book selection in a digital library. <em>Journal of the American Society for Information Science, 58</em>(12), 1745-1763.</li>

<li id="reu98">Reutzel, D.R. &amp; Gali, K. (1998). The art of children's book selection: a labyrinth unexplored. <em>Reading Psychology, 19</em>(1), 3-50.</li>

<li id="rob97">Robinson, C.C., Larsen, J.M., Haupt, J.H. &amp; Mohlman, J. (1997). Picture book selection behaviors of emergent readers: influence of genre, familiarity, and book attributes. <em>Reading Research and Instruction, 36</em>(4), 287-304. </li>

<li id="sch15">Schlebbe, K. (2015).<a href="http://www.webcitation.org/6va7dP29u"> Vorlesen mit dem Tablet: Auswahlkriterien f&uuml;r digitale Kinderb&uuml;cher.</a> [Reading aloud with the tablet: Selection criteria for digital children's books.] Unpublished master's thesis, Humboldt-Universit&auml;t zu Berlin, Berlin, Germany. Retrieved from https://edoc.hu-berlin.de/handle/18452/14904 (Archived by WebCite&reg; at http://www.webcitation.org/6va7dP29u)</li>

<li id="sti12">Stiftung Lesen (2012).<a href="http://www.webcitation.org/6kJgtcnm9"> <em>Vorlesestudie 2012: Digitale Angebote - neue Anreize f&uuml;r das Vorlesen? Repr&auml;sentative Befragung von Eltern mit Kindern im Alter von 2 bis 8 Jahren.</em></a> [Lecture study 2012: digital offers - new incentives for reading aloud? Representative interviews of parents with children aged 2 to 8 years.] Mainz, Germany: Stiftung Lesen. Retrieved from https://www.stiftunglesen.de/download.php?type=documentpdf&id=752 (Archived by WebCite&reg; at http://www.webcitation.org/6kJgtcnm9)</li>

<li id="the00">Theng, Y.L., Mohd-Nasir, N., Thimbleby, H., Buchanan, G. &amp; Jones, M. (2000). Designing a children's digital library with and for children. In <em>Proceedings of the Fifth ACM Conference on Digital Libraries</em> DL '00 (pp. 266-267). New York, NY: ACM.</li>

<li id="vaa12">Vaala, S. &amp; Takeuchi, L. (2012).<a href="http://www.webcitation.org/6kJh2LFxs"> <em>Parent co-reading survey. Co-reading with children on iPads: parents' perceptions and practices.</em></a> New York, NY: The Joan Ganz Cooney Center. Retrieved from http://www.joanganzcooneycenter.org/wp-content/uploads/2012/11/jgcc_ereader_parentsurvey_quickreport.pdf (Archived by WebCite&reg; at http://www.webcitation.org/6kJh2LFxs)</li>

<li id="van11">Vanderschantz, N., Hinze, A., Cunningham, S.J., Timpany, C. &amp; McKay, D. (2011).<a href="http://www.webcitation.org/6kJh8hahv"> How to take a book off the shelf: learning about ebooks from using a physical library.</a> In <em>Proceedings of the 23rd Australian Computer-Human Interaction Conference OzCHI 2011.  Workshops Programme, Nov 28 &amp; 29, Canberra, Australia. CHISIG.</em>.  New York, NY: ACM. Retrieved from http://researchcommons.waikato.ac.nz/handle/10289/6266 (Archived by WebCite&reg; at http://www.webcitation.org/6kJh8hahv)</li>

<li id="van14a">Vanderschantz, N., Hinze, A. &amp; Cunningham, S.J. (2014a). "Sometimes the internet reads the question wrong": children's search strategies &amp; difficulties. In <em>Proceedings of the 77th American Society for Information Science and Technology ASIS&amp;T Annual Meeting Seattle, WA</em> (pp. 1-10).  Silver Spring, MD: ASIST.</li>

<li id="van14b">Vanderschantz, N., Timpany, C., Hinze, A. &amp; Cunningham, S.J. (2014b). Family visits to libraries and bookshops: observations and implications for digital libraries. In Kulthida Tuamsuk, Adam Jatowt &amp; Edie Rasmussen, (Eds.), <em>The emergence of digital libraries - Research and practices. 16th International Conference on Asia-Pacific Digital Libraries</em> ICADL (pp. 182-195). Cham, Switzerland: Springer International Publishing. (Lecture Notes in Computer Science, 8839).</li>

</ul>

</section>

<section>

## Appendix 1: Interview field manual (translated, original in German)

_[Introduction; consent form; installation of recording technology]_

**Introducing question and personal data**

Would you please first introduce your family and describe your current family life?

**Open entry to the subject**

*   What does the reading practice in your family look like?
*   How do you use the tablet as a family in general? (Beginning of use, frequency of use, context of use, etc.)
*   How do you use the tablet as a family for reading and/or reading out loud?

**Answering the research question**

Would you please describe your procedure if you want to purchase or borrow a digital book for your child (e.g. on the basis of your last purchase or loan).

**In-depth questions (if necessary)**

*   Where do you look for e-books or book apps for children? (Purchase or rental; app-stores; online bookstores, etc.)
*   How often do you buy or borrow digital children's books?
*   Who is searching for and selecting the e-books or apps? (Parents; child; both)
*   How do you search concretely?
*   Which aspects are important to you while selecting digital books for children?
*   Do you know and/or use recommendation services for digital children's books?
*   What experiences have you had with your previous selection?

Please describe your standard approach for searching for and selecting printed books for your child or children.

**Conclusion**

Do you have any questions or additional comments?

_[Thanks for participating]_

## Appendix 2: Coding system

<figure>

![Figure 1: Coding system (translated, original in German)](../p794fig1.jpg)

<figcaption>Figure 1: Coding system (translated, original in German)</figcaption>

</figure>

## Appendix 3: Original quotations and translations

<table><caption>Table 3: Original quotations and translations</caption>

<tbody>

<tr>

<th>Translation</th>

<th>Original</th>

</tr>

<tr>

<td>'Well, they discover pretty quickly how to handle that [laughter].' (Mother Family E, 2015, para. 45)</td>

<td>"Naja, also die kriegen das ja ziemlich schnell raus, wie man das so bedient. [Lachen]" (Mutter Familie E, 2015, A. 45)</td>

</tr>

<tr>

<td>'Of course we try to keep it within a limit, that she, she should not play with it too much.' (Father Family D, 2015, para. 22)</td>

<td>"Wir versuchen natürlich, es in Grenzen zu halten, dass sie, sie soll ja auch nicht zu viel damit rumspielen." (Vater Familie D, 2015, A.22)</td>

</tr>

<tr>

<td>'So we do not use that. I think it might even be that there is, I do not know exactly. But we do not use it, no.' (Mother Family A, 2015, para. 23)</td>

<td>"Also das nutzen wir nicht. Also ich glaube, es könnte sogar sein, dass es das gibt, das weiß ich jetzt nicht genau. Aber wir nutzen es nicht, nein." (Mutter Familie A, 2015, A.23)</td>

</tr>

<tr>

<td>'my husband is always doing this, I simply cannot do this [laughter].' (Mother Family A, 2015, para. 15-17)</td>

<td>"weil das immer mein Mann macht, ich kann das gar nicht [Lachen]" (Mutter Familie A, 2015, A.15 ff.)</td>

</tr>

<tr>

<td>'I went to the customer reviews and looked up what were the most popular ones [apps].' (Mother Family F, 2015, para. 47)</td>

<td>"also nach den Bewertungen bin ich gegangen. Hab so geguckt, was die beliebtesten Spiele dann waren und bin nach Bewertungen gegangen." (Mutter Familie F, 2015, A.47)</td>

</tr>

<tr>

<td>'he [the husband] always checks the apps first and tests whether it is good or not and sometimes he deletes it afterwards.' (Mother Family A, 2015, para. 49)</td>

<td>"dass er da drauf guckt, ah, was empfehlen die noch, wenn man dieses Spiel hat, dass man dann guckt, was wird empfohlen." (Mutter Familie A, 2015, A.41)</td>

</tr>

<tr>

<td>'then advertising appeared again and thus we arrived at this app My Animals.' (Mother Family B, 2015, para. 33)</td>

<td>"dann kommt ja da natürlich wieder Werbung und dadurch kamen wir dann auf dieses Meine Tiere." (Mutter Familie B, 2015, A.33)</td>

</tr>

<tr>

<td>'mostly it was that we have discovered it … from friends who said: "we have discovered an app for our little ones, which is really nice."' (Mother Family C, 2015, para. 19)</td>

<td>"also meistens war es so, dass wir entweder von Bekannten erfahren haben, die gesagt haben: "Mensch, wir haben da für unseren Kleinen 'nen Spiel entdeckt, das ist ganz schön."" (Mutter Familie C, 2015, A.19)</td>

</tr>

<tr>

<td>'He [the husband] always checks the apps first and tests whether it is good or not, and sometimes he deletes it afterwards.' (Mother Family A, 2015, para. 49)</td>

<td>"Er guckt immer erst mal selber an und schaut, ob das irgendwie gut ist oder ob's blöd ist und dann löscht er's auch wieder." (Mutter Familie A, 2015, A.49)</td>

</tr>

<tr>

<td>'Sometimes the older one sits beside. But he just can tell because of the illustrations: "I want that with the tractor" or "I want that". But in principle, we decide what is going to be downloaded.' (Mother Family E, 2015, para. 61)</td>

<td>"der Große sitzt schon mal daneben. Aber der kann halt nur aufgrund der Bilder sagen: "Ich möchte das mit dem Trecker" oder "ich möchte das". Aber im Prinzip entscheiden wir das schon, was da drauf geladen wird." (Mutter Familie E, 2015, A.61)</td>

</tr>

<tr>

<td>'that he does not necessarily see any crap and any advertising, yes, that we already know that there is no advertising in it.' (Mother Family A, 2015, para. 39)</td>

<td>"dass er nicht unbedingt jeden Mist und jeden, jede Werbung auch sieht, ja, das ist uns schon wichtig, dass da keine Werbung dabei ist." (Mutter Familie A, 2015, A.39)</td>

</tr>

<tr>

<td>'It is important of course that there is no, I would say, that no advertising appears, on which he then clicks accidentally, uh, and orders thirty other apps.' (Mother Family B, 2015, para. 71)</td>

<td>"Wichtig ist natürlich auch, dass da nicht, ich sag mal, dauernd Werbung erscheint, auf die er dann aus Versehen drauf klickt, ähm, und mir noch dreißig andere Sachen bestellt." (Mutter Familie B, 2015, A.71)</td>

</tr>

<tr>

<td>'There are some free ones, which I look at first. Although I see easily that the apps which I like more are not for free.' (Mother Family G, 2015, para. 65)</td>

<td>"Da gibt's halt eben Kostenlose, die guck ich mir dann schon mal schneller an. Wobei man aber auch sieht, die die einem eher zusagen, die kosten dann auch was." (Mutter Familie G, 2015, A.65)</td>

</tr>

<tr>

<td>'And the other ones I bought for a few Euros. But I have set value on it that it is something reasonable.' (Mother Family F, 2015, para. 49)</td>

<td>"Und die anderen hab ich auch für ein paar Euro dann gekauft. Also da leg ich dann schon Wert drauf, dass das auch was Vernünftiges ist." (Mutter Familie F, 2015, A.49)</td>

</tr>

<tr>

<td>'I also pay attention to, uh, the age recommendation. I find it very important that this is age appropriate.' (Mother Family F, 2015, para. 100-102)</td>

<td>"Wo ich natürlich auch drauf achte ist natürlich, ähm, die Altersangabe. Finde ich immer ganz wichtig. Also da achte ich also auch schon drauf, dass das wirklich altersgerecht ist." (Mutter Familie F, 2015, A.100 ff.)</td>

</tr>

<tr>

<td>'There is usually an age recommendation where you can conform to. But you still have to check the content.' (Father Family D, 2015, para. 124)</td>

<td>"Dann steht ja meistens eine Altersempfehlung auch dabei. Daran kann man sich dann schon mal ein bisschen danach ausrichten. Man muss halt auch noch selber gucken, was denn dann so für ein Inhalt drin ist." (Vater Familie D, 2015, A.124)</td>

</tr>

<tr>

<td>'So he knows The Very Hungry Caterpillar from the kindergarten and then he [the husband] saw it and thought "Oh, he already knows this".' (Mother Family A, 2015, para. 17)</td>

<td>"Also Die kleine Raupe Nimmersatt war auch so, das hatte er, das hatte unser Großer im Kindergarten gemacht und dann hatte er einfach geschaut "Oh, das kennt er jetzt"" (Mutter Familie A, 2015, A.17)</td>

</tr>

<tr>

<td>'I try to make sure, uh, that it is not too much at once and this firefighter app sometimes crosses the border because you can click on a lot of things and so the background noise can become extremely high.' (Mother Family B, 2015, para. 69)</td>

<td>"Ich achte darauf, ähm, dass es nicht zu viel auf einmal ist und das ist bei dieser Feuerwehr-App schon manchmal grenzwertig, weil man eben viele Dinge auf einmal anklicken kann und die Geräuschkulisse dann enorm hoch ist." (Mutter Familie B, 2015, A.69)</td>

</tr>

<tr>

<td>'So that they will not be bombarded with too much information.' (Mother Family F, 2015, para. 109)</td>

<td>"Dass die nicht mit so vielen Informationen einfach bombardiert werden dann" (Mutter Familie F, 2015, A.109)</td>

</tr>

<tr>

<td>'so if they [the apps] are accompanied heavily by music that is repeated all the time, well, then that is just very annoying [laughter].' (Mother Family E, 2015, para. 69)</td>

<td>"also wenn die so sehr stark mit Musik untermalt sind und das die ganze Zeit immer wiederholt wird, dann ist das halt sehr nervig [Lachen]." (Mutter Familie E, 2015, A.69)</td>

</tr>

<tr>

<td>'Topics of their everyday life, which are police, firefighter, the airport … about farming, the forest and so on. So things from their environment.' (Mother Family A, 2015, para. 31)</td>

<td>"Sicher auch eben ob's Bereiche aus ihrem Alltag sind, das heißt, Polizei, Feuerwehr, Flughafen, so diese Wimmelbilder sind auch in diesem Bereich so auch im Bauernhof, das Thema Wald und so. Also das es Dinge aus ihrer Lebenswelt sind." (Mutter Familie A, 2015, A.31)</td>

</tr>

<tr>

<td>'First, we are looking for topics he is dealing with right now.' (Mother Family C, 2015, para. 35)</td>

<td>"Also es ist so, dass wir zuerst, ähm, geguckt haben, mit was beschäftigt er sich gerade viel." (Mutter Familie C, 2015, A.35)</td>

</tr>

<tr>

<td>'that they are also in German. Because that is sometimes a bit difficult.' (Mother Family E, 2015, para. 53)</td>

<td>"dass sie auf jeden Fall auch deutschsprachig sind. Weil das ist ja manchmal so ein bisschen schwierig." (Mutter Familie E, 2015, A.53)</td>

</tr>

<tr>

<td>'So we have quite a lot of fairy tales, which are bilingual. Which means they are read out aloud in English. But it is not the case that we are practicing English massively. But sometimes he shows some interest and says "I want to listen to it" or "That is a funny language, what is that?" So he is quite curious about it.' (Mother Family C, 2015, para. 55)</td>

<td>"Also wir haben relativ viele Märchen, die zweisprachig sind. Das heißt, die lesen Englisch vor. Es ist aber auch nicht so, dass wir dann da jetzt ganz massiv Englisch üben, sondern das ist halt wirklich wenn er irgendwie Interesse zeigt und sagt "Mensch, ich möchte das mal mir anhören" und "Das ist ja 'ne komische Sprache, was ist das?", dass er dann da auch schon ziemlich neugierig ist." (Mutter Familie C, 2015, A.55)</td>

</tr>

<tr>

<td>'Of course we see that a little bit as an early reading and spelling skills training, and that is important for our selection, if it makes sense, uh, for the skills they have to develop for school.' (Mother Family A, 2015, para. 33-35)</td>

<td>"sehen wir das natürlich auch alles so ein bisschen, also auch als Vorläuferfertigkeiten vom Lese-Rechtschreib-Prozess, und danach wählen wir es ganz sicher auch aus, ob's einfach auch Sinn macht, ähm, mit den Fertigkeiten, die sie lernen sollen für später hinsichtlich der Schule." (Mutter Familie A, 2015, A.33 ff.)</td>

</tr>

<tr>

<td>'this time I will look at the drawings very exactly. Because for the second [app], which I downloaded, I did not like the drawings.' (Mother Family B, 2015, para. 99)</td>

<td>"ich werde mir diesmal die Zeichnungen sehr genau anschauen. Weil eben bei der Zweiten, die ich runtergeladen hab, mir die Zeichnungen nicht gut gefielen." (Mutter Familie B, 2015, A.99)</td>

</tr>

<tr>

<td>'Since I have downloaded more of this free stuff, I would say that I was disappointed by many things. That at first sight, I had hoped to get more from it. Where I looked at it and said: "Hm, well, I have actually expected something else".' (Mother Family G, 2015, para. 165-167)</td>

<td>"Da ich jetzt immer so eher diese kostenlosen Sachen runtergeladen hab, würd ich sagen, dass ich von vielen Sachen enttäuscht war. Dass das auf den ersten Blick, dass ich mir da mehr von versprochen habe. Wo ich's mir dann angeguckt habe und so: "Hm, naja, hab ich mir jetzt eigentlich was anderes erhofft"." (Mutter Familie G, 2015, A.165 ff.)</td>

</tr>

<tr>

<td>'so actually the big difference in the selection is, that for conventional books we let the children decide, so they are allowed to simply browse through the library.' (Mother Family A, 2015, para. 55)</td>

<td>"also eigentlich der große Unterschied ist bei der Auswahl, dass bei den normalen Büchern wir vor allem die Kinder entscheiden lassen, dass sie sich die in der Bücherei auch einfach aussuchen dürfen oder halt einfach nehmen können" (Mutter Familie A, 2015, A.55)</td>

</tr>

<tr>

<td>'and in the bookstore he is just, he first browses and looks at the illustrations.' (Mother Family C, 2015, para. 79)</td>

<td>"und im Buchladen ist er halt dann, da stöbert er halt erst mal und guckt sich die Bilder an." (Mutter Familie C, 2015, A.79)</td>

</tr>

<tr>

<td>'where he simply brings an idea from the kindergarten and says that he would like to read a certain book again … and then we go to the library and, um, yes, get this book for him.' (Mother Family H, 2015, para. 95)</td>

<td>"wo er einfach Anregungen aus dem Kindergarten mitbringt und sagt, das würde er gerne noch mal lesen oder er viel von erzählt und wir dann in die Bücherei gehen und, ähm, ja, das Buch dazu holen." (Mutter Familie H, 2015, A.95)</td>

</tr>

</tbody>

</table>

</section>

</article>