<header>

#### vol. 20 no. 1, March, 2015

</header>

<article>

# Civil society organizations: providing and exchanging information about environmental health risk

#### [Shelagh K. Genuis PhD](#author) and [Cindy G. Jardine PhD](#author)  
School of Public Health University of Alberta, Alberta, Canada

#### Abstract

> **Introduction.** Potential risks to human health from environmental factors are a source of significant public concern. Moreover, the internet has transformed traditional health communication and increased public awareness of and concern for exiting, emerging and speculative environmental health (EH) issues. This paper is a work in progress. Its objective is to scan and evaluate the everyday information contexts that influence and incubate public views related to environmental health risk in Canada.  
> **Method.** A two part environmental scan is being conducted; oral presentation will focus on phase one. (1) Telephone interviews were conducted with representatives of environmental civil society organizations to explore their role in the public discourse related to environmental health. A modified convergent approach is being used. And (2) environmental health as represented on government and civil society organization websites will be evaluated.  
> **Analysis.** Facilitated by NVivo 8TM, results are being inductively analyzed using directed content analysis and grounded theory's constant comparative approach. NVivo 8TM facilitated analysis.  
> **Results.** Preliminary results suggest civil society organizations are involved in synthesizing and communicating environmental health information gathered from a wide range of formal and informal sources. These organizations occupy important _between_ spaces, providing information to and collaborating with the pubic, other groups and decision/policy-makers in government.  
> **Conclusions.** Phase one demonstrates that civil society organizations make an important contribution to public environmental health discourse as information providers and collaborators. Interviews will provide a strong foundation for further investigation.

<section>

## Introduction

Potential risks to human health from environmental factors and conditions are a source of significant public concern ([WHO 2013](#Who13); [Lemyre _et al_. 2006](#Lem06)). This concern is supported by increasing scholarly evidence for the adverse influence of environmental factors on human health ([E Risk Sciences 2010](#Eri10)). At the same time, rapid communication via the internet and popular press has transformed traditional communication. This has resulted in increased public awareness of and concern for existing, emerging and speculative environmental health hazards. These concerns may range from concerns about specific consumer products to concerns about the sequelae of climate change. The response of the public to local and international environmental concerns has brought attention to the information that informs public perspectives and to the effective communication of environmental health risk.

Communication, particularly risk communication, is a core goal of efficacious public health practice ([Bernhardt 2004](#Ber04)). To meet this goal, however, health agencies and decision/policy-makers must understand the rich information worlds that incubate and frame public understanding. This includes information provided by government agencies, as well as consideration of the increasing role of civil society organizations as information providers in the area of environmental health ([Cordner _et al_. 2013](#Cor13)). In addition, the media and internet are leading sources of scientific and health information, which have a powerful influence on public attitudes (Bubela 2009) and perceptions of personal health risks ([Gana _et al_. 2010](#Gan10)).

This paper is a work in progress. We will discuss a study exploring (1) the role of Canadian civil society organizations in the public discourse related to environmental health (data collection complete, analysis initiated), and (2) environmental health as represented on government and organization websites (data collection initiated). The conference presentation will focus on stage one, with discussion of data from stage two anticipated.

### Brief background: risk communication and public risk perspectives

Risk communication has evolved significantly in the last decades. It was previously conceived as information delivery from experts to the unknowledgeable public (a deficit model) and was considered to be the final stage of risk management ([Skarlatidou _et al_. 2012](#Ska12)). More recently risk communication has come to be viewed as an integrated process entailing two-way information exchange between experts and the public, with the primary purpose of empowering responsible risk decisions ([Jardine & Driedger In press](#Jar)). Furthermore, a primary tenet of informed and effective risk communication is the right of people and communities to participate in discussion of the risk factors that affect their lives ([Infanti _et al_. 2013](#Inf13)). Efficacious communication about environmental health risk can, therefore, be conceived as empowering the public through the provision of balanced information that facilitates consideration of risks, decisions about risks under their control and effective participation in risk management discussions. This egalitarian approach to risk communication coincides with the increasing influence of environmental organizations ([Cordner _et al_. 2013](#Cor13)). Because these groups frequently bridge a gap between governments and the public, it is valuable to gain deeper understanding of the perspectives of these organizations and to explore their contribution to public discourse.

### Objectives

In this study we explore and evaluate the everyday information contexts that influence and incubate public views of environmental health risk in Canada by interviewing representatives from Canadian environmental civil society organizations and by analyzing online representation of issues by government agencies and environmental organizations. Our objective is to examine how environmental groups position themselves within the information landscape and how the information they provide contrasts with information from government sources. Findings will contribute to a larger study exploring public perspectives on environmental health issues, with the goal of improving environmental risk information and communication.

## Method

This study takes an environmental scan approach ([Choo 2001](#Cho01); [Graham _et al_. 2008](#Gra08)). Findings will be descriptive and will illuminate the external trends and everyday information contexts which influence public understanding of environmental health risk. Environmental health is defined as any factor that operates through the environment to affect human health ([Integrated Assessment of Health Risks of Environmental Stressors in Europe 2013](#Int13)).

### Theory

This environmental scan is part of a larger study, which is informed by a modified _mental models_ approach ([Chowdhury _et al_. 2011](#Cho11)). This approach is premised on the notion that people seek and understand information from the perspective of their own knowledge, beliefs and experiences. _Risk_ is thus an inherently subjective concept. Social Positioning Theory ([Davies & Harré 1990](#Dav90)), which recognizes both the subjectivity and positionality of perspectives, will inform and guide analysis.

### Interviews with civil society organization representatives

Based on the United Nations' ([2010](#Uni10)) definition for civil society organizations, we conducted an extensive web-based search using keywords and online directories. Selected organizations were required to have a web presence that explicitly indicated concern for both the environment and human health. To ensure data completeness and saturation, we purposely recruited national and regional Canadian organizations with a range of foci. Participants were chosen by their organizations. We enriched the sample by recruiting 8 individuals who, although collaborating with civil society organizations, were involved in unique ways with environmental health (for example, environmental health education in Indigenous communities). Domains of inquiry included the purpose and focus on the organization, the role of organizations as information users and providers, responsibility for environmental health risk, public understanding of environmental health risk, and perspectives related to concerns and priorities. A modified convergent approach ([Driedger 2008](#Dri08)) was used. A list of possible items/themes was iteratively generated through this process, thus obtaining convergence of responses and theme saturation. Interviews were audio-recorded and transcribed verbatim; NVivo 10TM software is being used to facilitate data organization and analysis. Findings are currently being inductively analyzed using directed content analysis and grounded theory's constant comparative approach.

### Scan of Websites

An extensive Web-based search of online environmental health information available to the public via government websites (federal and provincial) and civil society organizations will be conducted. Domains of investigation will range from website content (e.g. target audience, currency, inclusion of mission statement) and purpose (e.g. inform, advocate), to examination of information style (e.g. editorial, report), 'voice'/tone (e.g. personal, factual) and authority (e.g. credentials of group officials/members). Source attribution and information referral will be documented. Data from this phase of the environmental scan will allow analysis of how information providers position themselves and exploration of similarity/contrast between governments and organizations. Although consumers access a wide range of online resources including social media, patient forums and blogs, this selection of sources is in keeping with the goals of an environmental scan - it will provide an overarching understanding of information framing by different groups.

## Preliminary findings

Semi-structured telephone interviews (n=30 to date) lasted 40 to 82 minutes (mean, 64 minutes). Preliminary analysis indicates that the majority of participants are involved in public education and awareness - synthesizing and communicating information from a wide range of formal (for example, government grey literature, peer reviewed literature, literature from research institutes) and informal (for example, other civil society organizations, individual citizen scientists, blogs) sources. Findings suggest that civil society organizations occupy important 'between' spaces, providing information to and collaborating with the public, other groups, and policy and decision-makers at various levels of government. Participants positioned these environmental organizations as filling an information void ('_I think that's our responsibility: to get that information out there for people_'), creating balanced and informed public discourse ('_there's a general sense amongst the public that government information is tainted with industry information_'), and facilitating knowledge translation ('_making [environmental information] understandable and providing outreach tools that are educational, informative, and helpful for the public_'). When asked about the most pressing issues facing Canadians, participants raised concern about specific issues (e.g. consumer products, air quality, climate change); however, a majority pointed to underlying socio-political concerns such as transparency and trust, a lack of confidence in decision-makers, perceptions of differing values or priorities, and concerns about information availability and credibility.

## Conclusion

Preliminary findings suggest that civil society organizations play a role as information providers in the area of environmental health. They collaborate extensively with both governments and other organizations. This study will contribute to improved understanding of the ways in which these organizations contribute to public discourse. Interviews provide a strong foundation for potential next steps: investigation from the perspective of the public.

## Acknowledgements

Sincere thanks to the participating environmental civil society organizations and to the interview participants who so generously shared their thoughts, perspectives and experiences. We are indebted to Dr. Michelle Driedger (University of Manitoba, Canada) for funding through her Tier II Canada Research Chair in Environmental and Health Risk Communication; and to Alberta Innovates - Health Solutions for support through a Postdoctoral Research Fellowship (Shelagh K. Genuis).

</section>

#### References

*   Bernhardt, J.M. Communication at the core of effective public health. (2004). _American Journal of Public Health, 94_(12), 2051-2053.
*   Bubela, T., Hyde-Lay, R., Lane, S., Ogbogu, U., Ouellette, C., Nisbet, M. C., . . . Caulfield, T. (2009). _Science communication reconsidered. Nature Biotechnology, 27_, 514-518.
*   Choo C.W. (2001). Environmental scanning as information seeking and organizational learning. _Information Research, 7_(1), paper 112\. Retrieved from http://informationr.net/ir/7-1/paper112
*   Chowdhury, P.D., Haque, C.E., & Driedger, S.M. (2011). Public versus expert knowledge and perception of climate change induced heat wave risk: a modified mental model approach. _Journal of Risk Research, 15_, 149-168.
*   Cordner, A., Mulcahy, M., & Brown, P. (2013). Chemical regulation on fire: rapid policy advances on flame retardant. _Environmental Science and Technology, 47_(13), 7067-7076.
*   Davies, B. & Harré, T. (1990). Positioning: the discursive production of selves. _Journal for the Theory of Social Behaviour, 20_, 43-63.
*   Driedger, S.M. 2008\. Convergent interviewing. In L. Given, (Ed.), _SAGE Encyclopedia of Qualitative Research Methods_ (pp. 125-127). Sage Publications, Inc.: Thousand Oaks, CA.
*   E Risk Sciences. (2010). _Systematic review of environmental burden of disease in Canada: final report_. National Collaborating Centre for Environmental Health. Retrieved from http://www.ncceh.ca/sites/default/files/Env_Burden_Disease_Oct_2010.pdf
*   Gana, K., Lourel, M., Trouillet, R., Fort, I., Mezred, D., Blaison, C., . & Ledrich, J. (2010). Judgment of riskiness: impact of personality, naive theories and heuristic thinking among female students. _Psychology and Health 25_:131-147.
*   Graham, P., Evitts, T. & Thomas-MacLean, R. (2008). Environmental scans: how useful are they for primary care research. _Canadian Family Physician, 54_, 1022-1023.
*   Infanti, J., Sixsmith, J., Barry, M.M., Núñez-Córdoba, J., Oroviogoicoechea-Ortega, C. & Guillén-Grima, F. (2013). _A literature review on effective risk communication for the prevention and control of communicable diseases in Europe_. Stockholm: European Centre for Disease Prevention and Control. Retrieved from http://www.ecdc.europa.eu/en/publications/publications/risk-communication-literary-review-jan-2013.pdf
*   Integrated Assessment of Health Risks of Environmental Stressors in Europe. (2013). _About_. Retrieved from http://www.intarese.org/
*   Jardine, C.G. & Driedger, S.M. (in press). Risk communication for empowerment: an ultimate or elusive goal? In J. Arvai & L. Rivers, _Effective risk communication_. Earthscan Publication Ltd.: London.
*   Lemyre L., Lee, J.E.C., Mercier, P., Bouchard, L. & Krewski, D. (2006). The structure of Canadians' health risk perceptions: environmental, therapeutic, and social health risks. _Health, Risk & Society, 8_, 185-195.
*   Skarlatidou, A., Cheng, T. & Haklay, M. (2012). What do lay people want to know about the disposal of nuclear waste? A Mental Model approach to the design and development of an online risk communication. _Risk Analysis, 32_, 1496-1511.
*   United Nations Global Compact. (2010). Civil Society. Retrieved from http://www.unglobalcompact.org/HowToParticipate/civil_society/
*   WHO. (2013). _Health and environment linkages initiative_. Retrieved from http://www.who.int/heli/en/

</article>