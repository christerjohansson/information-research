# The Use of the Internet by English Academics

### Wendy Shaw  
Department of Information and Library Studies  
University of Wales,  
Aberystwyth, UK  
_wws94@aber.ac.uk_

## Introduction

The original title for the PhD research was broad and read as: _The Use of the Internet by Humanities Academics_. This was narrowed slightly by the selection of the English and History disciplines for comparisons amongst the humanities overall. In the time available for the research (three years funded), it would not have been feasible to consider investigating more than two groups of humanities academics. Generally it is thought that the humanities can be viewed as either traditional or modern in interpretation. The traditional subject areas include language, literature, linguistics, philosophy, religion, music, history, archaeology and the arts. The more modern areas would be performing, creative and visual arts, media studies and communication studies ([Kirkham, 1989](#ref2)). It is notoriously difficult to say what those disciplines have in common, but they share at least the task of connecting the past and present as they are manifested in the works and acts of men ([Roush, 1969](#ref2)). Many authors prefer to be general in their usage of the term humanities, others specific, referring to historians alone. The phrase "humanistic scholar" is often used instead of "humanities scholar". Likewise, the humanities has been described as a group of study as opposed to fields of study. It can only be concluded that there is a lack of consensus being reached within the humanities as to what the term humanities means.

Since January 1998, there has been a review and modification of the title towards English Literature, which is part of the traditional humanities. It now reads as: _The Use of the Internet by English Academics_. According to Gould ([1993](#ref1)) literary scholarship is increasingly interdisciplinary with more than ever, its sources overlapping with those of other disciplines. Mullings _et al._ ([1996](#ref2)) advocates that the computer has already become a comfortable object for almost all literary scholars in areas of communication and production of typescript, and it is rapidly becoming accepted as a tool of literary enquiry also. Ciolek ([1998](#ref1)) concludes in a Scholarly _Internet_ Survey that the three most popular professional uses of the _Internet_ revolve around sending and receiving electronic mail (individual and list-mediated), and reading on-line news. This is open to debate as this survey was distributed to subscribers of English language electronic mailing lists specialising in Asian and Pacific studies.

## Context

During this decade we have witnessed a proliferation of information technologies which are transporting us into the new millennium and beyond. One example is the Internet and the World Wide Web. The Internet could not have been made possible without the advent of the personal computer in the 1970s. It is evident that the library is no longer the only building where access may be gained to a wealth of stored information. Increasingly, it is the Internet, which is operational 24 hours a day, seven days a week (subject to connections and time delays), that is the preferred option, being available via the personal computer which is more often than not situated in the academic's office, or increasingly at home.

What does the Internet have to offer? Not only are there web sites which contain hypertext-links all over the world, but there are also discussion groups and mailing lists, including electronic mail that can enable the academic to converse electronically within minutes about a query. File Transfer Protocol (ftp) allows the rapid transfer of documents from one file to another almost instantaneously. The first electronic project dates back to 1948 when Father Busa started to prepare electronic texts of St Thomas Aquinas for his monumental Index Thomisticus ([Hockey, 1997](#ref1)). It was 1973 before the first volume was made available ([Deegan, 1995](#ref1)). As a result of information becoming available electronically, academics are having to become computer literate to enable them to handle this tool effectively for use in their research. Electronic texts on-line are a valuable resource for the academic who can scan pages rapidly and download chapters of books for analysis. This is especially true for the English academic who can now access a whole range of primary sources, such as novels, poetry and literary essays that have exceeded their copyright date, back runs of critical journals and current scholarly journals in electronic form. The Oxford Text Archive, which is one of the oldest and best-known electronic centres in the world, works closely with members of the Arts and Humanities academic community to collect, catalogue, and preserve high-quality texts for research and teaching.

## The proposed work plan

Key areas of focus are :

*   Information Needs
*   Information Seeking Behaviour
*   Information Uses of English Academics

These areas of focus will be investigated from two angles: the teaching and research aspects of the literary scholar's daily working life. Whilst previous studies will be a guide to this area, one major aspect to be covered here will be how information on the Internet is interacted with by the English Academics themselves. The emphasis will also be particularly on the _USE_ of the Internet and the electronic environment.

The above areas of focus raise the following (sample) questions :

*   How do they carry out their literature searches for their research?
*   Is their information need satisfied by the use of the Internet?
*   Are English academics aware of the types of information that are available to them electronically?
*   Do they use them for research/teaching?
*   How or why do they use these sources?
*   What problems do they face in using these sources?
*   What are the problems associated with using these sources, e.g. copyright, access, knowledge about what is on offer, technological problems?
*   Have English academics grasped the necessary information-handling skills in order to use the electronic sources that are available on the Internet?

In the first instance it was essential that a literature survey (approximately six months) was carried out. This helped to determine which methodologies would be most suitable for use with this target group, as well as identifying previously recorded studies of information use in the area of the humanities. The choice of methodologies (e.g. focus groups, questionnaires, observation and interviews) will only reveal themselves once personal contact with academics has taken place, although the approach will be qualitative as opposed to quantitative. Time management must feature highly throughout the duration of this research to gain the maximum usage of resources and results. The intention is to carry out a pilot study in the University of Wales, Aberystwyth (UWA) first to gauge the response and eliminate those academics who are unable to assist in this research from the outset, before extending the study to other colleges of the University of Wales.

After analysis of these results it may be possible to make suggestions as to how English Academics can be more productive in their work (teaching and research) through the _USE_ of the Internet. Two-way communication is vital in this research, otherwise the outcome will be a biased point of view. The nineties scholar after all, has varying information needs, uses and seeking behaviour now, especially since electronic environments became accessible in their own workplace.

A number of studies have taken place in the 1980s and early 1990s, but these relate to the humanities discipline, and not necessarily to those in the English discipline or to the Internet, for example; ([Ellis _et al._ 1993](#ref1); [Fulton, 1991](#ref1); [Gould, 1988](#ref1); [Katzen, 1986](#ref2); [Stieg, 1981](#ref2); [Stone, 1982](#ref2); [Watson-Boone, 1994](#ref3); [Wilson & Walsh, 1996](#ref3)). According to most estimates, more than one million people will ‘Net’work each day from over two million host computers. In 1995, universities in more than 40 countries provided full Internet participation. At that time, many had access only to the email function, but if the rate of growth in that year continued, it was projected that there could be 300 million users by 1999, 750 million by 2000, and 1.5 billion by 2001 ([Bane & Milheim, 1995](#ref1)).

## Progress to date

### Year One

The initial PhD research began in October 1997 with a literature survey covering a variety of formats; for example; _Computers & Humanities; Computers & Texts_ printed journals, electronic journals, the world wide web, and the Library and Information Science Abstracts on CD-ROM, dealing with my chosen discipline. The literature search looked at the information seeking habits and information needs of the humanities scholar. Before the New Year a general overview of the humanities was produced, focusing on the areas of History and English (including Uniform Resource Locators references). This overview revealed that English academics are the lesser known academics in terms of their research and teaching habits in an electronic context (except for those situated in America where technology has moved on at a faster pace).

Scholars in the humanities, like researchers in other subjects, make use of computers for word processing, for cataloguing the books in their departmental libraries, and also for desk-top publishing ([Kenny, 1991](#ref2)). The tried and tested print formats in the physical library are often the preferred option to the electronic scene. More often than not, the necessary information handling skills have not been acquired by the academic of the English discipline to use the hardware and software to access the Internet. The Internet does require a different approach for obtaining information, articles, documents and carrying out a discussion in electronic format. For the past three years, Netskills Workshops have taken place across the country, for example, _Using the Internet for teaching and learning_ (Cardiff, July 7<sup>th</sup> 1998) introduced and explored some of the tools and techniques for teaching using the Internet.

During the spring months a second overview was compiled which concentrated solely on the varying aspects of English Literature. Unlike the previous overview, this one endeavoured to find out how the English Academic works in the working environment. Although not comprehensive in nature, a list of approximately 100 web sites was constructed to highlight the availability of sites dedicated to English Literature for the academic. Many academics are concerned with typology i.e. they study genre, period or a theme or specific named authors in depth; occasionally they might interact with more than one of these typologies. The use of the web can enhance the ability of research that an academic is required to undertake during their working life. Dedicated sites to authors such as the Thomas Hardy, Jane Austen and Shakespeare for example, offer the English academic with the opportunity of locating hypertext editions of novels, works and biographical, bibliographic and critical material. Academics can also be directly involved with inputting information into an online database such as _The Reading Experience Database 1450-1914_ (RED) which is co-ordinated by the Open University.

Similarly, teaching can be carried out in multi-media format with a large class of students quite effectively. Wack ([1993](#ref3)) from the English Department of the Washington State University has already taught a course on Chaucer in 2001, nicknamed 'Electronic Chaucer'. From the remarks made in the symposium, it would appear that electronic sources used solved her pedagogical problems. Similarly, Skilton ([1998](#ref2)) from Cardiff University devised a comprehensive electronic module entitled 'Literature and the City: London in the Nineteenth Century' for his English students.

It is invaluable to know how these academics study their subject within the English discipline, to what depth, and which sources are of most value to them. In this way it can be assumed that a certain academic uses a specific type of electronic resource to suit their information use, thereby building up a picture of the English Department as a whole. However, three quarters of Internet hosts and the majority of its users are in the US ([Barrett, 1996](#ref1)), so this can apply a limiting factor to British counterparts.

## Summary

Today's Internet is a result of the Department of Defense's ARPAnet which began in 1969 to enable researchers to share major hardware and software resources located at remote computer centers. As with all technologies, there are the inevitable advantages and disadvantages. However, the speed at which information can be accessed and downloaded or printed (subject to connections and time zones) is phenomenal. A wealth of information awaits the academic in the English discipline on the continually evolving web which was developed at CERN in Geneva in 1994 ([Rockwell, 1997](#ref2)). English Academics need to be given the opportunity to embrace the electronic sources that are available for use on the Internet, and receive training if information handling skills are not present.

The research to date has been hampered by the requirement of funding bodies for first year PhD students to undertake a Postgraduate Training Course. This has taken up a considerable proportion of time during the first year, and it is to be hoped that the effort expended will eventually pay off.

## <a id="ref1"></a>References

1.  Jane Austen web site (Republic of Pemberley) Available from: [http://www.pemberley.com](http://www.pemberley.com)
2.  Bane, A.F., & Milheim, W.D. (1995). Internet insights: How academics are using the Internet. _Computers in Libraries_, **15** (2), 32-36.
3.  Barrett, N. (1996). _The state of the cybernation: cultural, political and economic implications of the internet_. London: Kogan Page.
4.  Ciolek, T.M. (1998, March) _The scholarly uses of the Internet: An on-line survey_. Available from: [http://www.ciolek.com/PAPERS/InternetSurvey-98.html](http://www.ciolek.com/PAPERS/InternetSurvey-98.html), p5.
5.  Deegan, M. (1995). IT and the humanities. _Information UK Outlooks_, **14**, 1-19\. London: South Bank University and The British Library.
6.  E-Journals, Listservs, Reference (Rhodes English Resources). (1998, July) Available from: [http://blair.library.rhodes.edu/englhtmls/engljrnls.html](http://blair.library.rhodes.edu/englhtmls/engljrnls.html)
7.  Ellis, D., Cox, D. & Hall, K. (1993). A comparison of the information-seeking patterns of researchers in the physical and social sciences. _Journal of Documentation_ **49**, 4, 356-369.
8.  Fulton, C. (1991). Humanists as information users: a review of the literature, _AARL_, **22**, 188-196.
9.  Garrod, P. (1997). New skills for information professionals. _Information UK Outlooks_, **22** (7), 3-17\. London: South Bank University and The British Library.
10.  Gould, C.C. (1988). _Information needs in the humanities: an assessment_. Stanford, California: Research Libraries Group.
11.  Hockey, S. (1997). Electronic texts: The promise and the reality. _American Council of Learned Societies Newsletter_, **4**, 4\. Available from: [http://www.acls.org/n44hock.htm](http://www.acls.org/n44hock.htm), p1
12.  <a id="ref2"></a>Katzen, M. (1986). The application of computers in the humanities. Information Processing & Management **22**, 264.
13.  Kenny, A. (1992). _Computers and the Humanities_. British Library annual research lecture; 9\. London: British Library
14.  Kirkham, S. (1989). _How to find information in the humanities_. London: Library Association Ltd.
15.  Mullings, C., Deegan, M., Ross, S. & Kenna, S. (eds.) (1996). _New technologies for the humanities_. (pp.280-298) London: Bowker-Saur.
16.  Netskills Workshop: Using the Internet for teaching and learning. (1998, April 20). Available from: [http://www.netskills.ac.uk/events/workshops/mcc-apr98-1](http://www.netskills.ac.uk/events/workshops/mcc-apr98-1)
17.  Rockwell, R.C. (1997) The World Wide Web as a resource for scholars and students. _American Council of Learned Societies Newsletter_, **4**, 4\. Available from: [http://www.acls.org/n44hock.htm](http://www.acls.org/n44hock.htm), p1
18.  Roush, J.G. (1969). What will become of the past? _Daedalus 98, 64_. Cited in: Tibbo, H.R. (1993). _Abstracting, information retrieval and the humanities: Providing access to historical literature_. American Library Association.
19.  William Shakespeare web site Available from: [http://www.intergo.com/Library/lit/shakespr/colworks.htm](http://www.intergo.com/Library/lit/shakespr/colworks.htm)
20.  Skilton, D. (1998, July 7). [http://www.cf.ac.uk/uwcc/secap/skilton/](http://www.cf.ac.uk/uwcc/secap/skilton/)
21.  Stieg, M.F. (1981).The information needs of historians, _College & Research Libraries_, **42**, 6, 549-560.
22.  Stone, S. (1982). Humanities scholars: Information needs and uses, _Journal of Documentation_ **38**, 292-313.
23.  The Oxford Text Archive. (1998, July) Available from: [http://firth.natcorp.ox.ac.uk/ota/public/index.shtml](http://firth.natcorp.ox.ac.uk/ota/public/index.shtml)
24.  The Reading Experience Database 1450-1914 (RED). (1998, March) Available from: [http://www.open.ac.uk/OU/Academic/Arts/RED/redform.htm](http://www.open.ac.uk/OU/Academic/Arts/RED/redform.htm)
25.  <a id="ref3"></a>The Thomas Hardy Society of North America (THSNA). (1998) Available from: [http://wolf.its.ilstu.edu/hardysoc/TEACHING.HTM](http://wolf.its.ilstu.edu/hardysoc/TEACHING.HTM)
26.  Wack, M. (1994). Chaucer in 2001 _in_ Okerson, A. and Mogge, D. (eds.): _Gateways, gatekeepers , and roles in the information omniverse: Proceedings from the third symposium_ November 13<sup>th</sup>-15<sup>th</sup>, Association of Research Libraries Office of Scientific and Academic Publishing, Washington DC. (pp.45-57).
27.  Watson-Boone, R. (1994). The information needs and habits of humanity scholars, _Information Needs_, Winter, 203-215.
28.  Wilson, T.D. & Walsh, C. (1996). Information behaviour: an inter-disciplinary perspective: a review of the literature. London: British Library Research and Innovation Centre. British Library research and innovation report, 10.

    **[Information Research](http://InformationR.net/ir/), Volume 4 No. 2 October 1998**  
    _The use of the Internet by English academics_, by [Wendy Shaw](MAILTO: wws94@aber.ac.uk) Location: http://InformationR.net/ir/4-2/isic/shaw.html    © the author, 1998\. Last updated: 9th September 1998