#### vol. 14 no. 1, March, 2009

* * *

# Term based comparison metrics for controlled and uncontrolled indexing languages

#### [B.M. Good](#authors)  
Michael Smith Foundation for Health Research/Canadian Institute for Health Research Bioinformatics Training Programme, University of British Columbia, Heart + Lung Research Institute at St. Paul's Hospital Vancouver, BC, Canada  
[J.T. Tennis](#authors)  
The Information School, University of Washington, Box 352840, Mary Gates Hall, Seattle, WA 98195-2840, USA

#### Abstract

> **Introduction.** We define a collection of metrics for describing and comparing sets of terms in controlled and uncontrolled indexing languages and then show how these metrics can be used to characterize a set of languages spanning folksonomies, ontologies and thesauri.  
> **Method.** Metrics for term set characterization and comparison were identified and programs for their computation implemented. These programs were then used to identify descriptive features of term sets from twenty-two different indexing languages and to measure the direct overlap between the terms.  
> **Analysis.** The computed data were analysed using manual and automated techniques including visualization, clustering and factor analysis. Distinct subsets of the metrics were sought that could be used to distinguish between the uncontrolled languages produced by social tagging systems (folksonomies) and the controlled languages produced using professional labour.  
> **Results.** The metrics proved sufficient to differentiate between instances of different languages and to enable the identification of term-set patterns associated with indexing languages produced by different kinds of information system. In particular, distinct groups of term-set features appear to distinguish folksonomies from the other languages.  
> **Conclusions.** The metrics organized here and embodied in freely available programs provide an empirical lens useful in beginning to understand the relationships that hold between different, controlled and uncontrolled indexing languages.

## Introduction

We are in an era of a rapidly expanding number and diversity of systems for organizing information. Wikis, collaborative tagging systems and semantic Web applications represent broad categories of just a few emerging frameworks for storing, creating, and accessing information. As each new kind of information system appears, it is important to understand how it relates to other kinds of system. This understanding allows us to answer a variety of important questions, which will shape the way future systems are designed. Does the new system represent a less expensive way to achieve the same functionality as another? Might it be fruitfully combined with another approach? How similar is it to an exemplar in its domain?

In addition to deriving theoretical answers to questions at the level of the kind of system, such as, How does social tagging relate to professional indexing? ([Feinberg 2006](#fei06); [Tennis 2006](#ten06)) or, How do the ontologies of computer science relate to the classifications of library and information science?, as addressed by Soergel ([1999](#soe99)), it is also now of practical importance to find answers to specific instance-level questions. For example, Al-Khalifa and Davis ([2007](#alk07)) attempt to answer the question, How do the tags provided by [Delicious](http://delicious.com) users relate to the terms extracted by the Yahoo indexing algorithm over the same documents? and Morrison ([2008](#mor08)) asks, How do the results of searches performed on social tagging systems compare to those performed on full Web search engines? Answers to such questions provide vital knowledge to system designers, because, in the age of the Web, information systems do not operate in isolation from one another. It is both possible and beneficial to integrate components of different systems to create symbiotic aggregates that meet the needs of specific user groups better than any single system could, and doing so depends upon the knowledge of how the different systems relate. Would Yahoo automatic indexing be improved through incorporation of indexes provided by Delicious users? Comparative analyses of the components of the two systems can help tell us.

Both comparative studies of information systems in the abstract and efforts to design specific instances of new integrative systems can benefit from mechanisms that help to identify the specific similarities and differences that obtain between different systems. One facet of this is empirical, reproducible, quantitative methods of investigation. To inform both kinds of enquiry, empirical protocols that allow for reproducible, quantitative comparison would be beneficial. However, the term _information system_ covers a vast and ill-defined set of things, each of which is composed of many complex components operating together in many different contexts to achieve a variety of different purposes. To conduct useful empirical comparisons of such systems, 1) hypotheses must be evaluated in light of the many contributing qualitative factors, and 2) reproducible metrics must be devised that can be used to test assumptions. While qualitative interpretations of the differences that hold between different kinds of information system continue to advance, there are few practical, reproducible metrics defined for use in empirical comparisons of system components.

Our broad goal in this work is to define a set of measurements, which can be taken of information systems, that are meaningful, widely applicable, and reproducible. The specific set of metrics introduced here do not intend nor pretend to be exhaustive nor definitive, in fact, we suggest that is not an attainable goal given the complexity of the systems under scrutiny. Rather, we advance them as an early set of candidates in what we expect will be a broad pool of metrics, which will continue to expand and be refined indefinitely. In light of these goals, the metrics defined here are meant for the characterization of one key component common to the vast majority of information systems in current operation, the language used to index the resources of interest within the system.

Zhang ([2006:121](#zha06)) defines an indexing language as ‘_the set of terms used in an index to represent topics or features of documents and the rules for combining or using those terms_’. As the emphasis here is on empirical observation and many of the information systems under consideration offer little or no rules for the application nor of the construction of the terms, we will operate under the broader definition of indexing languages as _sets of terms used in an index to represent topics or features_. Notice that this definition spans both controlled languages, such as institutionally maintained thesauri, and uncontrolled languages, such as the sets of keywords generated by social tagging systems. Examples of indexing languages, as defined here, thus include the [Medical Subject Headings (MeSH) thesaurus](http://www.nlm.nih.gov/bsd/disted/mesh/index.html), the Gene Ontology as described by Ashburner _et al_. ([2000](#ash00)), and the [Connotea](http://www.connotea.org/) folksonomy as described by Lund _et al_. ([2005](#lun05)). Each of these languages, though varying in relational structure, purpose, and application, is composed of a set of terms that represent aspects of the resources within the information systems that utilize them. Through comparisons of the features of these indexing languages, we hope to start work that will eventually allow us insight that will enable us to gain a better understanding not just of the relations between the languages, but, through them, of the relations between the systems that generate and use them.

In this work, we advance an approach to the automated, quantitative characterization of indexing languages through metrics based simply on the sets of terms used to represent their concepts. These metrics are divided into two groups, intra-set and inter-set. The intra-set metrics provide views on the shape of the sets of terms in aggregate. The inter-set metrics provide a coherent approach to the direct comparison of the overlaps between different term sets. The paper is divided into two main sections. The first section describes each of the metrics in detail and the second presents the results from a quantitative comparison of twenty-two different indexing languages. Results are provided for each language individually, using the intra-set metrics, and for each language pair, using the inter-set metrics. In addition to the broad all-against-all comparison, we present a more detailed exploration of the similarities and differences, revealed using the proposed metrics, that hold between controlled and uncontrolled indexing languages.

## Metrics for comparing term sets

In this work we focus on the set of terms used to represent the concepts that compose indexing languages. Relationships between the terms or the concepts that they represent are not analysed at this stage because some languages, such as many folksonomies, do not display the explicitly defined relationship structures present in other forms, such as thesauri and ontologies. This view allows us to produce metrics that are applicable to a broad array of different indexing languages and can serve as the foundation for future efforts that expand the comparative methodology. In the following section, we identify a group of specific, measurable characteristics of term sets. From these we can measure similarities and differences between indexing languages based on quantifiable characteristics that they all share.

### Intra-term set measures

Measurements taken at the level of the set define what might be termed the shape of the term set. Such features of a term set include its size, descriptive statistics regarding the lengths of its terms, and the degree of apparent modularity present in the set. Measures of modularity expose the structure of the term set based on the proportions of multi-word terms and the degrees of sub-term re-use. These measures of modularity include two main categories, Observed Linguistic Precoordination and Compositionality.

Observed Linguistic Precoordination indicates whether a term appears to be a union of multiple terms based on syntactic separators. For example, the MeSH term **_Fibroblast Growth Factor_** would be observed to be a linguistic precoordination of the terms **_Fibroblast_**, **_Growth_** and **_Factor_** based on the presence of spaces between the terms. As explained in Tables [1](#tab1) and [2](#tab2), we categorize terms as uniterms (one term), duplets (combinations of two terms), triplets (combinations of three terms) or quadruplets or higher (combinations of four or more terms). Using these categorizations, we also record the flexibility of a term set as the fraction of sub-terms (the terms that are used to compose duplets, triplets, and quadplus terms) that also appear as uniterms.

<table id="tab1"><caption>

**Table 1: Examples of Observed Linguistic Precoordination term classifications**</caption>

<tbody>

<tr>

<th>Terms</th>

<th>Observed Linguistic Precoordination  
Sub-term Number</th>

<th>Naming convention</th>

</tr>

<tr>

<td>ontology</td>

<td>1</td>

<td>uniterm</td>

</tr>

<tr>

<td>ontology evaluation</td>

<td>2</td>

<td>duplet</td>

</tr>

<tr>

<td>Fibroblast Growth Factor</td>

<td>3</td>

<td>triplet</td>

</tr>

<tr>

<td>United States of America</td>

<td>4</td>

<td>quadruplet or higher</td>

</tr>

<tr>

<td>Type 5 Fibroblast Growth Factor</td>

<td>5</td>

<td>quadruplet or higher</td>

</tr>

</tbody>

</table>

<table id="tab2"><caption>

**Table 2: Explanation of the Observed Linguistic Precoordination flexibility measure**  
(The flexibility for the term set listed in the first columns is equal to 0.17 (1 divided by 6) because there is one sub-term _Web_, which is also a uniterm out of a total of six sub-terms.)</caption>

<tbody>

<tr>

<th>Terms</th>

<th>Uniterms</th>

<th>Sub-terms</th>

<th>Consolidated  
sub-terms</th>

<th>Both consolidated  
and uniterms</th>

</tr>

<tr>

<td>Semantic Web</td>

<td> </td>

<td>Semantic</td>

<td>Semantic</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Web</td>

<td>Web</td>

<td>Web</td>

</tr>

<tr>

<td>Web</td>

<td>Web</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Social Web</td>

<td> </td>

<td>Social</td>

<td>Social</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Web</td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Planet</td>

<td>Planet</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td>Do Re Mi</td>

<td> </td>

<td>Do</td>

<td>Do</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Re</td>

<td>Re</td>

<td> </td>

</tr>

<tr>

<td> </td>

<td> </td>

<td>Mi</td>

<td>Mi</td>

<td> </td>

</tr>

<tr>

<td>Star</td>

<td>Star</td>

<td> </td>

<td> </td>

<td> </td>

</tr>

<tr>

<td colspan="3"> </td>

<td>6</td>

<td>1</td>

</tr>

</tbody>

</table>

The Observed Linguistic Precoordination measurements described here were adapted from term-set characteristics, originally identified by Van Slype ([1976](#van76)), for gauging the quantifiable features of a thesaurus. Van Slype developed and used these metrics in the process of suggesting revisions to the ISO standard based on comparisons of the attributes of a sample of thesauri to the prescriptions of the standard. Our intent in using these, and related, metrics is to make it possible to explore the consequences of adding a similar, empirical aspect to studies of modern indexing languages.

The Observed Linguistic Precoordination measures were extended with related measures of compositionality as introduced by Ogren _et al_. ([2004](#ogr04)). Compositionality measures include a) the number of terms that contain another complete term as a proper substring, b) the number of terms that are contained by another term as a proper substring, c) the number of different complements used in these compositions, and d) the number of different compositions created with each contained term. A complement is a sub-term that is not itself an independent member of the set of terms. For example, the term set containing the two terms {_macrophage_, _derived from macrophage_} contains one complement, _derived from_. A composition is a combination of one term from the term set with another set of terms (forming the suffix and/or the prefix to this term) to form another term in the set. For example, in the Academic Computing Machinery subject listing, the term _software program verification_ contains three sub-terms that are also independent terms (_software_, _program_, and _verification_). According to our definition, this term would be counted as three compositions: _software_+suffix, prefix+_program_+suffix, prefix+_verification_. As another example, the term _denotational semantics_ would only result in one composition because _semantics_ is an independent term while _denotational_ is not (and thus is a complement as defined above).

Modularity is indicative of the _factors_ that go into the semantics of a term set, and shape its use. Here we are guided by Soergel's rubric from concept description and semantic factoring. He tells us,

> ...we may note that often conceptual structure is reflected in linguistic structure; often multi-word terms do designate a compound concept, and the single terms designate or very nearly designate the semantic factors. Example: Steel pipes = steel:pipes [demonstrating the factoring] ([Soergel 1974:75](#soe74)).

The relative presence or absence of modular structure within a term set thus provides some weak indication of its conceptual structure. For example, even though an indexing language may not explicitly declare relationships between its terms, semantic relationships may sometimes be inferred between terms that share, for example, common sub-terms ([Ogren _et al_. 2004](#ogr04)). The potential to detect re-usable semantic factors that may be indicators of semantic structure within a term set makes modularity metrics important axes for the comparison of different term sets.

Together, these measurements combine to begin to form a descriptive picture of the shape of the many diverse term sets used in indexing languages. Table [3](#tab3) lists and provides brief definitions for all of the term set measurements taken.

<table id="tab3"><caption>

**Table 3: Parameters of term sets**</caption>

<tbody>

<tr>

<th>Measure</th>

<th>Definition</th>

</tr>

<tr>

<th>Number distinct terms</th>

<td>The number of syntactically unique terms in the set.</td>

</tr>

<tr>

<th>Term length</th>

<td>The length of the terms in the set. We report the mean, minimum, maximum, median, standard deviation, skewness, and coefficient of variation for the term lengths in a term set.</td>

</tr>

<tr>

<th>Observed Linguistic Precoordination uniterms, duplets, triplets, quadplus</th>

<td>We report both the total number and the fraction of each of these categories in the whole term set.</td>

</tr>

<tr>

<th>Observed Linguistic Precoordination flexibility</th>

<td>The fraction of Observed Linguistic Precoordination sub-terms (the independent terms that are used to compose precoordinated terms) that also appear as uniterms.</td>

</tr>

<tr>

<th>Observed Linguistic Precoordination number sub-terms per term</th>

<td>The number of sub-terms per term is zero for a uniterm (_gene_), two for a duplet (_gene ontology_), three for a triplet (_cell biology class_), and so on. We report the mean, maximum, minimum, and median number of sub-terms per term in a term set.</td>

</tr>

<tr>

<th>Contains another</th>

<td>The terms that contain another term from the same set. Both the total and the proportion of terms that contain another are reported.</td>

</tr>

<tr>

<th>Contained by another</th>

<td>The terms that are contained by another term from the same set. Both the total and the proportion of terms that are contained by another are reported</td>

</tr>

<tr>

<th>Complements</th>

<td>A complement is a sub-term that is not itself an independent member of the set of terms. The total number of distinct complements is reported.</td>

</tr>

<tr>

<th>Compositions</th>

<td>A composition is a combination of one term from the term set with another set of terms (forming the suffix and/or the prefix to this term) to form another term in the set. The total number of compositions is reported.</td>

</tr>

</tbody>

</table>

### Inter-term set measures

The descriptions of term set shape described above are useful in that they can be applied to any set of terms independently and because they provide detailed descriptions of the term sets, but, from the perspective of comparison, more direct methods are also applicable. To provide a more exact comparison of the compositions of sets of terms used in different languages, we suggest several simple measures of set similarity. Each of the measures is a view on the relation between the size of the intersection of the two term sets and the relative sizes of each set. The members of the intersection are determined through exact string matches applied to the term sets (after a series of syntactic normalization operations). As depicted in Figure [1](#fig1) and explained below, these intersections are used to produce measures of precision, Recall, and Overlap (the F-measure).

#### Context considerations in inter-set comparisons

The equivalence function used when conducting direct set comparisons of the components of different indexing languages is important. In this preliminary work, we rely on the simplistic notion that a term in one indexing language is equivalent to a term in another language if and only if, after syntactic normalization, the terms are identical. Synonymy, hyponymy and polysemy are not considered and, thus, the measured overlaps are purely syntactic. When considering indexing languages used in similar contexts; for example, as might be indicated when two different languages are used to index the same set of documents by similar groups of people, this function provides useful information because the same words are likely to be used for similar purposes. However, the greater the difference in context of application between the indexing languages being compared, the greater the danger that this simple function will not yield relevant data. Logical extensions of this work would thus be to make use of semantic relations, for example of synonymy, present within the indexing languages, as well as natural language processing techniques, to develop additional equivalence functions that operate on a more semantic level. That being said, with or without such extensions, any empirical comparison should always be interpreted in light of the contexts within which the different indexing languages operate.

#### Quantifying set similarity

Once methods for assessing the equivalence relationship are established (here post-normalization string matching), it is possible to quantify the relations between the resultant sets in several different ways. For example, Al-Khalifa and Davis ([2007](#alk07)) find what they term _percentage overlap_ by dividing the size of the intersection of the two sets by the size of the union of the sets and multiplying by 100\. They use this measure to quantify the similarity of the sets of terms used to index the same documents produced by different indexing systems. For example, to find the percentage overlap between the set F {A,B,C} and the set K{A,G,K,L} , the size of the intersection {A} is 1, the size of their union {A,B,C,G,K,L} is 6 and thus the percentage overlap is 100(1/6)= 17%.

While a useful measurement, this equation misses key information regarding the relative sizes of the two sets. To capture the size discrepancies and the asymmetry of the relationship, we employ additional metrics typically used to evaluate class prediction algorithms.

Binary class prediction algorithms are often evaluated on the basis of relations between sets of true and false positive and negative predictions ([Witten & Frank 2000](#wit00)). These relations are quantified with measures of Accuracy, precision and Recall. Accuracy is the number of correct predictions divided by the number of false predictions. precision is the number of true positives divided by the number of predicted positives. Recall is the number of true positives divided by the number of both true and false positives. precision and Recall are often summarized with the F-measure, which equates to their harmonic mean.

Hripcsak and Rothschild ([2005](#hri05)) showed that, by arbitrarily assigning one set as the _true positives_ and the other as the _predicted positives_, the F-measure can be used to measure the degree of agreement between any two sets. Because it is commutative, the choice of which set to assign as _true_ makes no difference to the outcome. Figure [1](#fig1) illustrates the idea of using precision, Recall, and the F-measure as generic set comparison operators. The logic goes as follows, if set A is conceptualized as an attempt to predict set B, the number of items in both sets (the intersection) corresponds to the number of true positives for the predictor that produced A; the number of items in A corresponds to the number of true positives plus the number of false positives; and the number of items in B corresponds to the number of true positives plus the number of false negatives. From this perspective, accuracy thus equates to percentage overlap as described by Al-Khalifa and Davis ([2007](#alk07)). In addition, precision and Recall can be used for the asymmetric quantification of the similarity of the two sets and the F-measure can be used to provide a symmetric view of the overlap between the sets that takes into account their relative sizes.

<figure>

![figure 1](../p395fig1.png)

<figcaption>

**Figure 1: Set comparison operations**</figcaption>

</figure>

## Demonstration and evaluation of proposed metrics

The metrics described above are intended to be useful in scientific enquiries regarding the relationships that hold between different indexing languages. This information, in turn, should then be useful in informing assumptions about the relationships between the information systems that generate and use these languages. As such, it should be possible to use the metrics to answer specific questions. We chose the following questions as demonstrative examples:

1.  Are the intra-set characteristics of the folksonomies emerging from collaborative tagging systems sufficient to distinguish them from term sets associated with indexing languages created using professional labour? (We assume that the difference in kind between these groups will be expressed in a difference in shape as expressed in the intra-set measures.)
2.  How much direct overlap exists between terms from the _Connotea_, Bibsonomy, and _CiteULike_ folksonomies, and terms from MeSH? These folksonomies are used to describe tens of thousands of the same resources as MeSH, hence we expect some overlap in representation, but how much is there in reality?

To answer these questions and thus demonstrate example applications of the proposed set of metrics, we implemented programs that calculate each intra- and inter-set metric described above. In the text that follows, we describe the application of these programs to the automated characterization and comparison of twenty-two different indexing languages.

### Sample

We gathered a sample of twenty-two different term sets. The terms were extracted from folksonomies, thesauri, and ontologies, all of which are currently in active use. Our domains span biology, medicine, agriculture, and computer science; however, the sample set is biased towards biology and medicine. Ontologies constitute the most common type of structure in the sample simply because more of them were accessible than the other forms, Table [4](#tab4) lists the subjects of the study (note that there are more than twenty-two listed because multiple versions for some of the larger term sets were considered separately).

<table id="tab4"><caption>

**Table 4: Term sets**  
(Each of the term sets evaluated in this study is listed here. See [Appendix 1](#app1) for additional information about these languages.)</caption>

<tbody>

<tr>

<th>Name</th>

<th>Abbreviation</th>

<th>Source syntax</th>

<th>Type</th>

<th>Domain</th>

</tr>

<tr>

<td>Academic Computing Machinery subject listing</td>

<td>ACM 1997</td>

<td>OWL</td>

<td>thesaurus</td>

<td>computer science</td>

</tr>

<tr>

<td>Agriculture Information and Standards ontology</td>

<td>AG</td>

<td>OWL</td>

<td>ontology</td>

<td>agriculture</td>

</tr>

<tr>

<td>Bibsonomy</td>

<td>Bibsonomy</td>

<td>text</td>

<td>folksonomy</td>

<td>general/academic</td>

</tr>

<tr>

<td>BioLinks</td>

<td>BioLinks</td>

<td>OWL</td>

<td>thesaurus</td>

<td>bioinformatics</td>

</tr>

<tr>

<td>Biological Process branch of the Gene Ontology</td>

<td>GO_BP</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Cell Type Ontology</td>

<td>CL</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Cellular Component branch of the Gene Ontology</td>

<td>GO_CC</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Chemical Entities of Biological Interest</td>

<td>CHEBI</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>CiteULike</td>

<td>CiteULike</td>

<td>text</td>

<td>folksonomy</td>

<td>general/academic</td>

</tr>

<tr>

<td>Common Anatomy Reference Ontology</td>

<td>CARO</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Connotea</td>

<td>Connotea</td>

<td>text</td>

<td>folksonomy</td>

<td>general/academic</td>

</tr>

<tr>

<td>Environment Ontology</td>

<td>ENVO</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Foundational Model of Anatomy (preferred labels + synonyms)</td>

<td>FMA + synonyms</td>

<td>OWL</td>

<td>ontology</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>Foundational Model of Anatomy (preferred labels)</td>

<td>FMA PrefLabels</td>

<td>OWL</td>

<td>ontology</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>Medical Subject Headings (descriptors + entry terms)</td>

<td>MeSH With All Labels</td>

<td>XML</td>

<td>thesaurus</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>Medical Subject Headings (descriptors)</td>

<td>MeSH PrefLabels</td>

<td>XML</td>

<td>thesaurus</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>Molecular Function branch of the Gene Ontology</td>

<td>GO_MF</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>National Cancer Institute Thesaurus (preferred labels + synonyms)</td>

<td>NCI Thesaurus + synonyms</td>

<td>OWL</td>

<td>thesaurus</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>National Cancer Institute Thesaurus (preferred labels)</td>

<td>NCI Thesaurus PrefLabels</td>

<td>OWL</td>

<td>thesaurus</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>Ontology for Biomedical Investigation</td>

<td>OBI</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology/medicine</td>

</tr>

<tr>

<td>Phenotype Ontology</td>

<td>PATO</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Protein Ontology</td>

<td>PRO</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Sequence Ontology</td>

<td>SO</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

<tr>

<td>Thesaurus of EIONET, the European, Environment, Information, and Observation Network</td>

<td>GEMET</td>

<td>SKOS/RDF</td>

<td>thesaurus</td>

<td>environment</td>

</tr>

<tr>

<td>Zebrafish Anatomy</td>

<td>ZFA</td>

<td>OBO/OWL</td>

<td>ontology</td>

<td>biology</td>

</tr>

</tbody>

</table>

The indexing languages considered here were chosen for three general reasons: (1) they were freely available on the Web, (2) most of the terms associated with the indexing languages had representations in English and (3) we sought popular examples spanning both controlled and uncontrolled indexing languages. Availability on the Web not only made data collection for the present study easier, it increases the likelihood that the study could be repeated by others in the future. By constraining the natural language of origin for the indexing languages under study, the likelihood that the measured differences between term sets were the results of factors aside from differences in, for example, typical grammatical structure of the source languages, was increased. Finally, by sampling from a broad range of the known types of indexing language, as suggested, for example, in the typology of Tudhope ([2006](#tud06)), we hoped to show the generic nature of the metrics introduced here and to offer some basic exploratory comparisons of the broad groups of controlled and uncontrolled languages.

Although we provide results for all of the inter-term set comparisons, the emphasis of the set comparisons is on the relationship between MeSH and the uncontrolled indexing languages. To partially decrease the problems, noted above, associated with conducting syntactic comparisons of indexing languages operating in different contexts, uncontrolled languages were sought that were used to index many of the same documents as MeSH. Folksonomies emanating from social tagging services targeted towards academic audiences thus compose the set of uncontrolled languages in the sample.

### Data analysis

Once each of the term sets was collected (see [Appendix 1](#app1)), two levels of term normalization were applied corresponding to the intra-set analysis (phase 1) and the inter-set analysis (phase 2). Both phases were designed based on the premise that most of the terms in the structures were English words. Though there were certainly some non-English terms present in the folksonomy data, notably German and Spanish, these terms constituted a relatively small minority of the terms in the set and, as such, we do not believe they had any significant effect on the results.

#### Phase 1 term normalization

Phase 1 normalization was designed primarily to help consistently delineate the boundaries of compound words, especially in the case of the folksonomies. The operations were:

1.  All non-word characters (for example, comma, semi-colon, underline and hyphen) were mapped to spaces using a regular expression. So the term _automatic-ontology_evaluation_ would become _automatic ontology evaluation_.
2.  CamelCase compound words were mapped to space separated words: _camelCase_ becomes _camel case_.
3.  All words were made all lower case (_case-folded_).
4.  Any redundant terms were removed such that, after operations 1-3, each term in a set composed a string of characters that was unique within that set.

All of the intra-set measurements (Size and Flexibility for example) were taken after Phase 1 normalization. Phase 2 normalization was applied before the set-intersection computations (for the inter-set measurements).

#### Phase 2 term normalization

Phase 2 normalization was intended to reduce the effects of uninformative inconsistencies such as _dolphins_ not matching _dolphin_ when estimating the intersections of the term sets.

1.  Phase 1 normalization was applied.
2.  Porter stemming was applied to all terms and sub-terms ([Porter 1980](#por80)).
3.  All sub-terms were sorted alphabetically.
4.  All terms and sub-terms with less than two characters were removed.
5.  All terms and sub-terms matching words from a popular English stop-word list were removed ([Default... 2008](#ran08))

These additional steps resulted in an average reduction in the total number of distinct terms per term set of 13% with the most substantial difference seen for the _MeSH all_ term set, which included both the preferred labels for each descriptor and all of the alternate labels, at 52%. The set of just the preferred labels for the MeSH descriptors was only reduced by 3%. This demonstrates that the normalization step was successful in reducing redundancy within the term sets because the _MeSH all_ set intentionally includes many variations of the same term while the preferred labels are intended to be distinct. Figure [2](#fig2) plots the reduction in the (non-redundant) term set size between phase 1 and phase 2 normalization for all the term sets.

<figure id="fig2">

![figure 2](../p395fig2.png)

<figcaption>

**Figure 2: The effect of phase 2 normalization on the size of the term set**  
(For each term set, the chart displays the ratio of its size after phase 2 normalization versus its size after phase 1 normalization.)</figcaption>

</figure>

After normalization, the shape of each of the term sets was first assessed individually using the intra-set measures. Then each of the term sets was compared directly to all others using the inter-set metrics.

## Intra-set findings

The intra-set measures displayed a broad range of diversity across all of the samples and provided some preliminary evidence of the presence of distinct shapes associated with term sets originating from controlled versus uncontrolled information organization structures. The collected measurements are provided in Tables 5 to 7 and discussed below.

<table id="tab5"><caption>

**Table 5: Size and composition of term sets**  
(The term sets are grouped into three types: folksonomies are indicated in green, thesauri in yellow, and ontologies in blue. The maximum and minimum values for each column are indicated by the uncolored cells.)</caption>

<tbody>

<tr>

<th>

Term set

</th>

<th>Number distinct terms</th>

<th>OLP mean number sub-terms per term</th>

<th>OLP max number sub-terms per term</th>

<th>OLP median number sub-terms per term</th>

<th>Complements</th>

<th>Compositions</th>

</tr>

<tr>

<td>Bibsonomy</td>

<td>48120</td>

<td>0.63</td>

<td>21</td>

<td>0</td>

<td>16448</td>

<td>25881</td>

</tr>

<tr>

<td>CiteULike</td>

<td>234223</td>

<td>0.56</td>

<td>14</td>

<td>0</td>

<td>62364</td>

<td>127118</td>

</tr>

<tr>

<td>Connotea</td>

<td>133455</td>

<td>1.49</td>

<td>33</td>

<td>2</td>

<td>119486</td>

<td>183980</td>

</tr>

<tr>

<td>ACM 1997 (OWL version)</td>

<td>1194</td>

<td>2.47</td>

<td>15</td>

<td>2</td>

<td>583</td>

<td>654</td>

</tr>

<tr>

<td>AG (English terms)</td>

<td>28432</td>

<td>1.34</td>

<td>7</td>

<td>2</td>

<td>7146</td>

<td>10018</td>

</tr>

<tr>

<td>BioLinks</td>

<td>90</td>

<td>1.87</td>

<td>6</td>

<td>2</td>

<td>9</td>

<td>9</td>

</tr>

<tr>

<td>GEMET</td>

<td>5207</td>

<td>1.68</td>

<td>7</td>

<td>2</td>

<td>2201</td>

<td>3809</td>

</tr>

<tr>

<td>MeSH PrefLabels</td>

<td>24766</td>

<td>1.67</td>

<td>20</td>

<td>2</td>

<td>8333</td>

<td>11162</td>

</tr>

<tr>

<td>MeSH With All Labels</td>

<td>167081</td>

<td>2.35</td>

<td>27</td>

<td>2</td>

<td>90032</td>

<td>163010</td>

</tr>

<tr>

<td>CARO</td>

<td>50</td>

<td>2.38</td>

<td>4</td>

<td>2</td>

<td>21</td>

<td>22</td>

</tr>

<tr>

<td>CHEBI</td>

<td>73465</td>

<td>8.88</td>

<td>241</td>

<td>3</td>

<td>255506</td>

<td>289469</td>

</tr>

<tr>

<td>CL</td>

<td>1268</td>

<td>2.57</td>

<td>9</td>

<td>2</td>

<td>1171</td>

<td>1529</td>

</tr>

<tr>

<td>ENVO</td>

<td>2001</td>

<td>1.49</td>

<td>10</td>

<td>2</td>

<td>925</td>

<td>1452</td>

</tr>

<tr>

<td>FMA plus synonyms</td>

<td>120243</td>

<td>5.81</td>

<td>18</td>

<td>6</td>

<td>255632</td>

<td>545648</td>

</tr>

<tr>

<td>FMA Preflabels</td>

<td>75147</td>

<td>6.14</td>

<td>18</td>

<td>6</td>

<td>169042</td>

<td>352541</td>

</tr>

<tr>

<td>GO_BP</td>

<td>42482</td>

<td>5.00</td>

<td>33</td>

<td>5</td>

<td>33667</td>

<td>79062</td>

</tr>

<tr>

<td>GO_CC</td>

<td>3539</td>

<td>3.45</td>

<td>14</td>

<td>3</td>

<td>2493</td>

<td>3821</td>

</tr>

<tr>

<td>GO_MF</td>

<td>30843</td>

<td>4.83</td>

<td>62</td>

<td>4</td>

<td>18941</td>

<td>26138</td>

</tr>

<tr>

<td>NCI hesaurus ‚ preflabels</td>

<td>60980</td>

<td>3.38</td>

<td>31</td>

<td>3</td>

<td>107413</td>

<td>148151</td>

</tr>

<tr>

<td>NCI Thesaurus + synonyms</td>

<td>146770</td>

<td>3.81</td>

<td>73</td>

<td>3</td>

<td>391297</td>

<td>592554</td>

</tr>

<tr>

<td>OBI</td>

<td>764</td>

<td>2.20</td>

<td>8</td>

<td>2</td>

<td>288</td>

<td>315</td>

</tr>

<tr>

<td>PATO</td>

<td>2162</td>

<td>1.57</td>

<td>7</td>

<td>2</td>

<td>1162</td>

<td>2780</td>

</tr>

<tr>

<td>PRO</td>

<td>837</td>

<td>4.28</td>

<td>32</td>

<td>5</td>

<td>552</td>

<td>767</td>

</tr>

<tr>

<td>SO</td>

<td>2104</td>

<td>2.86</td>

<td>18</td>

<td>3</td>

<td>2342</td>

<td>3183</td>

</tr>

<tr>

<td>ZFA</td>

<td>3250</td>

<td>2.40</td>

<td>8</td>

<td>2</td>

<td>2255</td>

<td>3616</td>

</tr>

</tbody>

</table>

<table id="tab6"><caption>

**Table 6: Modularity measurement ratios**  
(The term sets are grouped into three types: folksonomies are indicated in green, thesauri in yellow, and ontologies in blue. The maximum and minimum values for each column are indicated by the uncolored cells.)</caption>

<tbody>

<tr>

<th>Term set</th>

<th>OLP uniterms</th>

<th>OLP duplets</th>

<th>OLP triplets</th>

<th>OLP quadplus</th>

<th>OLP flexibility</th>

<th>Contains another</th>

<th>Contained by another</th>

</tr>

<tr>

<td>Bibsonomy</td>

<td>72.7%</td>

<td>21.7%</td>

<td>4.2%</td>

<td>1.5%</td>

<td>56.6%</td>

<td>25.8%</td>

<td>13.7%</td>

</tr>

<tr>

<td>CiteULike</td>

<td>75.8%</td>

<td>18.8%</td>

<td>4.3%</td>

<td>1.2%</td>

<td>68.2%</td>

<td>23.8%</td>

<td>9.6%</td>

</tr>

<tr>

<td>Connotea</td>

<td>44.8%</td>

<td>35.1%</td>

<td>12.4%</td>

<td>7.7%</td>

<td>43.8%</td>

<td>51.7%</td>

<td>18.2%</td>

</tr>

<tr>

<td>ACM 1997 (OWL version)</td>

<td>18.5%</td>

<td>40.9%</td>

<td>18.4%</td>

<td>22.2%</td>

<td>9.2%</td>

<td>40.3%</td>

<td>12.8%</td>

</tr>

<tr>

<td>AG (English terms)</td>

<td>34.3%</td>

<td>63.1%</td>

<td>2.2%</td>

<td>0.4%</td>

<td>15.6%</td>

<td>32.5%</td>

<td>11.0%</td>

</tr>

<tr>

<td>BioLinks</td>

<td>35.6%</td>

<td>31.1%</td>

<td>14.4%</td>

<td>18.9%</td>

<td>6.5%</td>

<td>8.9%</td>

<td>8.9%</td>

</tr>

<tr>

<td>GEMET</td>

<td>27.5%</td>

<td>54.4%</td>

<td>13.9%</td>

<td>4.1%</td>

<td>26.6%</td>

<td>51.7%</td>

<td>16.0%</td>

</tr>

<tr>

<td>MeSH PrefLabels</td>

<td>37.3%</td>

<td>37.1%</td>

<td>15.7%</td>

<td>9.8%</td>

<td>15.8%</td>

<td>35.1%</td>

<td>10.5%</td>

</tr>

<tr>

<td>MeSH With All Labels</td>

<td>16.4%</td>

<td>40.6%</td>

<td>28.1%</td>

<td>14.9%</td>

<td>23.1%</td>

<td>62.0%</td>

<td>10.4%</td>

</tr>

<tr>

<td>CARO</td>

<td>4.0%</td>

<td>54.0%</td>

<td>38.0%</td>

<td>4.0%</td>

<td>3.7%</td>

<td>44.0%</td>

<td>12.0%</td>

</tr>

<tr>

<td>CHEBI</td>

<td>22.1%</td>

<td>18.8%</td>

<td>11.2%</td>

<td>47.9%</td>

<td>33.2%</td>

<td>73.7%</td>

<td>20.9%</td>

</tr>

<tr>

<td>CL</td>

<td>15.3%</td>

<td>35.3%</td>

<td>28.0%</td>

<td>21.4%</td>

<td>6.3%</td>

<td>80.6%</td>

<td>13.4%</td>

</tr>

<tr>

<td>ENVO</td>

<td>37.3%</td>

<td>47.6%</td>

<td>10.4%</td>

<td>4.6%</td>

<td>26.6%</td>

<td>51.6%</td>

<td>17.3%</td>

</tr>

<tr>

<td>FMA plus synonyms</td>

<td>1.2%</td>

<td>6.9%</td>

<td>11.7%</td>

<td>80.2%</td>

<td>15.0%</td>

<td>95.1%</td>

<td>24.6%</td>

</tr>

<tr>

<td>FMA Preflabels</td>

<td>1.4%</td>

<td>5.2%</td>

<td>8.8%</td>

<td>84.6%</td>

<td>16.6%</td>

<td>95.5%</td>

<td>25.3%</td>

</tr>

<tr>

<td>GO_BP</td>

<td>0.8%</td>

<td>14.4%</td>

<td>18.1%</td>

<td>66.7%</td>

<td>3.7%</td>

<td>87.1%</td>

<td>20.7%</td>

</tr>

<tr>

<td>GO_CC</td>

<td>9.1%</td>

<td>26.7%</td>

<td>22.3%</td>

<td>41.9%</td>

<td>7.0%</td>

<td>57.0%</td>

<td>19.5%</td>

</tr>

<tr>

<td>GO_MF</td>

<td>4.0%</td>

<td>8.2%</td>

<td>20.4%</td>

<td>67.5%</td>

<td>2.3%</td>

<td>58.2%</td>

<td>11.0%</td>

</tr>

<tr>

<td>NCI Thesaurus - preflabels</td>

<td>14.8%</td>

<td>25.8%</td>

<td>22.5%</td>

<td>36.9%</td>

<td>22.3%</td>

<td>77.9%</td>

<td>17.2%</td>

</tr>

<tr>

<td>NCI Thesaurus + synonyms</td>

<td>16.8%</td>

<td>20.1%</td>

<td>17.5%</td>

<td>45.6%</td>

<td>37.5%</td>

<td>81.3%</td>

<td>24.8%</td>

</tr>

<tr>

<td>OBI</td>

<td>19.5%</td>

<td>42.1%</td>

<td>25.1%</td>

<td>13.2%</td>

<td>7.7%</td>

<td>32.2%</td>

<td>13.6%</td>

</tr>

<tr>

<td>PATO</td>

<td>36.6%</td>

<td>40.0%</td>

<td>17.7%</td>

<td>5.7%</td>

<td>42.8%</td>

<td>57.6%</td>

<td>32.2%</td>

</tr>

<tr>

<td>PRO</td>

<td>11.1%</td>

<td>6.0%</td>

<td>11.8%</td>

<td>71.1%</td>

<td>12.1%</td>

<td>69.7%</td>

<td>17.6%</td>

</tr>

<tr>

<td>SO</td>

<td>12.8%</td>

<td>29.8%</td>

<td>27.5%</td>

<td>30.0%</td>

<td>17.2%</td>

<td>76.4%</td>

<td>22.6%</td>

</tr>

<tr>

<td>ZFA</td>

<td>17.4%</td>

<td>36.4%</td>

<td>26.0%</td>

<td>20.3%</td>

<td>13.7%</td>

<td>60.9%</td>

<td>14.7%</td>

</tr>

</tbody>

</table>

<table id="tab7"><caption>

**Table 7: Measurements of term length**  
(The term sets are grouped into three types: folksonomies are indicated in green, thesauri in yellow, and ontologies in blue. The maximum and minimum values for each column are indicated by the uncolored cells.)</caption>

<tbody>

<tr>

<th>Term set</th>

<th>Mean</th>

<th>Max</th>

<th>Median</th>

<th>Standard Deviation</th>

<th>Skewness</th>

<th>Coefficient of variation</th>

</tr>

<tr>

<td>Bibsonomy</td>

<td>10.19</td>

<td>196.00</td>

<td>9.00</td>

<td>6.59</td>

<td>5.17</td>

<td>0.65</td>

</tr>

<tr>

<td>CiteULike</td>

<td>12.38</td>

<td>80.00</td>

<td>11.00</td>

<td>7.35</td>

<td>1.83</td>

<td>0.59</td>

</tr>

<tr>

<td>Connotea</td>

<td>15.29</td>

<td>268.00</td>

<td>13.00</td>

<td>14.14</td>

<td>7.56</td>

<td>0.92</td>

</tr>

<tr>

<td>ACM 1997 (OWL version)</td>

<td>21.70</td>

<td>94.00</td>

<td>20.00</td>

<td>10.96</td>

<td>1.48</td>

<td>0.51</td>

</tr>

<tr>

<td>AG (English terms)</td>

<td>15.29</td>

<td>48.00</td>

<td>15.00</td>

<td>5.67</td>

<td>0.12</td>

<td>0.37</td>

</tr>

<tr>

<td>BioLinks</td>

<td>16.30</td>

<td>45.00</td>

<td>15.00</td>

<td>9.12</td>

<td>0.74</td>

<td>0.56</td>

</tr>

<tr>

<td>GEMET</td>

<td>15.48</td>

<td>54.00</td>

<td>15.00</td>

<td>6.73</td>

<td>0.67</td>

<td>0.43</td>

</tr>

<tr>

<td>MeSH PrefLabels</td>

<td>17.46</td>

<td>98.00</td>

<td>16.00</td>

<td>8.71</td>

<td>1.29</td>

<td>0.50</td>

</tr>

<tr>

<td>MeSH With All Labels</td>

<td>20.36</td>

<td>112.00</td>

<td>19.00</td>

<td>9.29</td>

<td>0.93</td>

<td>0.46</td>

</tr>

<tr>

<td>CARO</td>

<td>20.96</td>

<td>35.00</td>

<td>20.00</td>

<td>7.24</td>

<td>0.30</td>

<td>0.35</td>

</tr>

<tr>

<td>CHEBI</td>

<td>36.12</td>

<td>831.00</td>

<td>21.00</td>

<td>45.44</td>

<td>4.01</td>

<td>1.26</td>

</tr>

<tr>

<td>CL</td>

<td>19.35</td>

<td>72.00</td>

<td>18.00</td>

<td>9.59</td>

<td>1.07</td>

<td>0.50</td>

</tr>

<tr>

<td>ENVO</td>

<td>12.43</td>

<td>73.00</td>

<td>11.00</td>

<td>7.80</td>

<td>2.32</td>

<td>0.63</td>

</tr>

<tr>

<td>FMA plus synonyms</td>

<td>38.26</td>

<td>125.00</td>

<td>36.00</td>

<td>16.45</td>

<td>0.64</td>

<td>0.43</td>

</tr>

<tr>

<td>FMA Preflabels</td>

<td>40.35</td>

<td>125.00</td>

<td>38.00</td>

<td>17.03</td>

<td>0.59</td>

<td>0.42</td>

</tr>

<tr>

<td>GO_BP</td>

<td>39.71</td>

<td>160.00</td>

<td>37.00</td>

<td>18.63</td>

<td>1.38</td>

<td>0.47</td>

</tr>

<tr>

<td>GO_CC</td>

<td>26.50</td>

<td>96.00</td>

<td>23.00</td>

<td>15.29</td>

<td>1.00</td>

<td>0.58</td>

</tr>

<tr>

<td>GO_MF</td>

<td>39.82</td>

<td>322.00</td>

<td>38.00</td>

<td>19.61</td>

<td>1.45</td>

<td>0.49</td>

</tr>

<tr>

<td>NCI Thesaurus - preflabels</td>

<td>25.87</td>

<td>208.00</td>

<td>22.00</td>

<td>17.36</td>

<td>1.86</td>

<td>0.67</td>

</tr>

<tr>

<td>NCI Thesaurus + synonyms</td>

<td>26.67</td>

<td>342.00</td>

<td>23.00</td>

<td>19.96</td>

<td>2.25</td>

<td>0.75</td>

</tr>

<tr>

<td>OBI</td>

<td>18.69</td>

<td>62.00</td>

<td>17.00</td>

<td>9.46</td>

<td>0.99</td>

<td>0.51</td>

</tr>

<tr>

<td>PATO</td>

<td>14.96</td>

<td>46.00</td>

<td>14.00</td>

<td>7.33</td>

<td>0.67</td>

<td>0.49</td>

</tr>

<tr>

<td>PRO</td>

<td>26.38</td>

<td>162.00</td>

<td>27.00</td>

<td>13.82</td>

<td>1.40</td>

<td>0.52</td>

</tr>

<tr>

<td>SO</td>

<td>19.87</td>

<td>142.00</td>

<td>18.00</td>

<td>11.86</td>

<td>1.58</td>

<td>0.60</td>

</tr>

<tr>

<td>ZFA</td>

<td>18.45</td>

<td>72.00</td>

<td>18.00</td>

<td>8.72</td>

<td>0.54</td>

<td>0.47</td>

</tr>

</tbody>

</table>

Table [5](#tab5) contains the non-ratio measurements of the size and the composition of the term sets. From it, we can see that there is a wide range in the size and degrees of modularity of the term sets under study. The largest term set was the [CiteULike](http://www.citeulike.org/) folksonomy at 234,223 terms and the smallest was the Common Anatomy Reference ontology ([Haendel _et al_. 2008](#hae08)) at just fifty terms. There was also substantial variation in the total number of Observed Linguistic Precoordination sub-terms per term, with the CHEBI ontology ([Degtyarenko _et al_. 2008](#deg08)) averaging 8.88 while the [Bibsonomy](http://www.bibsonomy.org/) folksonomy averaged just 0.63. This is suggestive of differences in the relative compositionality of the different term sets, with the ontologies being much more modular in general than the folksonomies.

The sub-terms per term measurement highlights the uniqueness of the CHEBI ontology within the context of our sample; its terms include both normal language constructs like _tetracenomycin F1 methyl ester_ and chemical codes like _methyl 3,8,10,12-tetrahydroxy-1-methyl-11-oxo-6,11-dihydrotetracene-2-carboxylate_. Although both term structures are highly modular in this large ontology, the latter are clearly driving the very high observed mean number of sub-terms per term.

Table [6](#tab6) focuses specifically on illustrating the amount of modularity apparent in these term sets. It displays the percentages of uniterms, duplets, triplets, and quadplus terms; the flexibility, and the percentages of terms that contain other terms or are contained by other terms. The _CiteULike_ folksonomy has the highest percentage of uniterms at 75.8%, followed closely by the _Bibsonomy_ folksonomy at 72.7%, while the two lowest percentages are observed for the Foundational Model of Anatomy (FMA) (including synonyms), described by Rosse and Mejino ([2003](#ros03)), at 1.2% and the Biological Process (BP) branch of the Gene Ontology, described by Ashburner _et al._ ([2000](#ash00)) at 0.8%. This tendency towards increased compositionality in these ontologies and decreased compositionality in these folksonomies is also apparent in the percentage of their terms that contain other complete terms from the structure, with more than 95% of the FMA terms containing other FMA terms and only 23.8% of the Bibsonomy terms containing another _Bibsonomy_ term. As might be expected, larger average term lengths, as presented in Table [7](#tab7), appear to correlate to some extent with some of the measures indicating increased compositionality. The highest correlation for a compositionality measure with average term length was observed for Observed Linguistic Precoordination Quad Plus (r-squared 0.86) while the lowest was for containedByAnother (r-squared 0.13). The highest mean term length observed was 40.35 characters for the preferred labels for the FMA and the lowest was 10.19 for the _Bibsonomy_ terms.

### Factor analysis

Following the collection of the individual parameters described above, exploratory factor analysis was applied to the data to deduce the major dimensions. Before executing the factor analysis, the data were pruned manually to reduce the degree of correlation between the variables. The features utilized in the factor analysis were thus limited to _percent of uniterms_, _percent of duplets_, _percent of quadplus_, _flexibility_, _percent of containsAnother_, _percent of containedByAnother,_ _mean number of sub-terms per term_, _mean term length_, and the _coefficient of variation for term length_. Maximum likelihood factor analysis, as implemented in the R statistical programming environment by the R Development Core Team ([2008](#rde08)), was applied using these variables for all of the sampled term sets. Three tests were conducted with 1, 2, and 3 factors to be fitted respectively. In each of these tests, the dominant factor, which might be labelled _term complexity_, was associated with the variables: _percent of quadplus_, _mean term length_, and _mean sub-terms per term_. In the 2-factor test, the secondary factor was most associated with the _percent of uniterms_ and the _flexibility_. Finally, in the 3 factor analysis, the third factor was associated with percent of _containsAnother_ and percent _containedByAnother_. Table [8](#tab8) provides the factor loadings for the 3-factor test.

<table id="tab8"><caption>

**Table 8: Factor loadings from maximum likelihood factor analysis using three factors**  
(The loadings for the dominant variables are indicated in bold.)</caption>

<tbody>

<tr>

<th>Variable</th>

<th>Factor1</th>

<th>Factor2</th>

<th>Factor3</th>

</tr>

<tr>

<td>pct.OLP.uniterms</td>

<td>-0.321</td>

<td>-0.537</td>

<td>

**0.774**</td>

</tr>

<tr>

<td>pct.OLP.duplets</td>

<td>-0.907</td>

<td>-0.131</td>

<td>-0.275</td>

</tr>

<tr>

<td>pct.OLP.quadplus</td>

<td>

**0.876**</td>

<td>0.409</td>

<td>-0.24</td>

</tr>

<tr>

<td>OLP.flexibility</td>

<td>-0.199</td>

<td></td>

<td>

**0.94**</td>

</tr>

<tr>

<td>pct.containsAnother</td>

<td>0.421</td>

<td>

**0.814**</td>

<td>-0.171</td>

</tr>

<tr>

<td>pct.containedByAnother</td>

<td>0.206</td>

<td>

**0.756**</td>

<td>0.173</td>

</tr>

<tr>

<td>Mean.Term Length</td>

<td>

**0.769**</td>

<td>0.438</td>

<td>-0.321</td>

</tr>

<tr>

<td>Coefficient.of.variation. Term.Length</td>

<td> </td>

<td> </td>

<td>0.54</td>

</tr>

<tr>

<td>OLP.mean.number.sub.terms.per.term</td>

<td>

**0.665**</td>

<td>0.518</td>

<td>-0.216</td>

</tr>

</tbody>

</table>

### Controlled versus uncontrolled term sets

The data presented in Tables 5 to 7 provide evidence that the metrics captured here are sufficient to distinguish between term sets representing different indexing languages. To assess their utility in quantifying differences between indexing languages emanating from different kinds of information system, we tested to see if they could be used to differentiate between the languages produced by professional labour (the thesauri and the ontologies) and languages generated by the masses (the folksonomies).

This examination was conducted using manual inspection of the data, multi-dimensional visualization, and cluster analysis. At each step, we tested to see if the data suggested the presence of a distinct constellation of intra-set parameters associated with the term sets drawn from the folksonomies. For some subsets of variables, the difference was obvious. For example, as Figure [3](#fig3) illustrates, both the _percent of uniterms_ and the _Observed Linguistic Precoordination flexibility_ measurements were sufficient to separate the folksonomies from the other term sets independently. For other subsets of variables, the differences were less clear and, in some cases, the folksonomies did not group together.

Figures 4 to 10 use radar-charts to illustrate the shapes associated with the three folksonomies in the sample as well as representative ontologies and thesauri. Radar charts were chosen because they make it possible to visualize large numbers of dimensions simultaneously. Though it would be possible to reduce the number of features in the charts, for example using the results from the factor analysis presented above, we chose to present all of the measurements taken. These figures, which capture all of the features measured for a given term set in a single image, suggest fairly distinct patterns in the term sets associated with the different kinds of information system present in our sample. However, when utilizing all of the variables, the borders of the various categories are not entirely clear. For example, the _Bibsonomy_ and _CiteULike_ folksonomies appear to be nearly identical in these charts but, while similar, the _Connotea_ folksonomy shows substantial variations.

In various iterations of cluster analysis we repeatedly found that the _Bibsonomy_ and the _CiteULike_ term sets grouped tightly together and that _Connotea_ was generally similar to them but that this similarity was strongly influenced by the specific subset of the metrics used. In one specific analysis presented in Good & Tennis ([2008](#goo08)), Ward’s method identified a distinct cluster containing just the folksonomies using the following parameters: _percent of uniterms_, _percent of duplets_, _flexibility_, _percent contained by another_, _standard deviation of term length_, _skewness of term length_, and _number of complements_.

<figure id="fig3">

![fig3](../p395fig3.png)

<figcaption>

**Figure 3: %Uniterms verse Observed Linguistic Precoordination flexibility**  
(Either metric is sufficient to form a linear separator between the term sets originating from folksonomies (the three in the upper right corner) and the controlled terms from the other information organization structures in the sample. The Pearson correlation coefficient for _percent of uniterms_ and _Observed Linguistic Precoordination flexibility_ is 0.79\. )</figcaption>

</figure>

These results indicate that the answer to the first question, namely: Are the terms from folksonomies shaped differently than the terms from controlled vocabularies? is, generally, yes. Subsets of these metrics can be used to separate folksonomies from the controlled vocabularies using a variety of methods. However, _Bibsonomy_ and _CiteULike_ are clearly much more similar to each other than either is to _Connotea_. Without advancing a definitive answer as to why this is the case, we offer several possible explanations. First, one clear technical difference between _Connotea_ and the other two systems is that it allows spaces in its tags. For example, it is possible to use the tag _semantic Web_ in _Connotea_, but, in Bibsonomy or _CiteULike_, one would have to use a construct like _semanticWeb_, _semantic-Web_, or _semanticWeb_ to express the same term. Though the syntactic normalization we utilized will equate _semantic-Web_ with _semantic Web_ (and detect the two-term composition), the term semanticWeb would not match and would be classified by the system as a uniterm. This difference suggests that there may be more compound terms in _Bibsonomy_ and _CiteULike_ than our metrics indicate; however, this aspect of the tagging system may also act to discourage the use of complex tags by the _Bibsonomy_ and _CiteULike_ users. Aside from differences in the allowed syntax of these uncontrolled indexing languages, this may also be an effect of the differing communities that use these systems. While _Connotea_ is clearly dominated by biomedical researchers, _Bibsonomy_ is much more influenced by computer scientists and _CiteULike_ seems to have the broadest mixture. Perhaps the biomedical tags are simply longer and more complex than in other fields. A final possibility, one that we will return to in the discussion of the direct measures of term-set overlap, is that _Connotea_ may be disproportionately affected by the automatic import of terms from controlled vocabularies, in particular MeSH, as tags within the system.

<figure>

![Figure 4](../p395fig4.png)

<figcaption>

**Figure 4: Radar graph of the MeSH thesaurus**</figcaption>

</figure>

<figure>

![Figure 5](../p395fig5.png)

<figcaption>

**Figure 5: Radar graph of the Association for Computing Machinery (ACM) thesaurus**</figcaption>

</figure>

<figure>

![Figure 6](../p395fig6.png)

<figcaption>

**Figure 6: Radar graph of the _Connotea_ folksonomy**</figcaption>

</figure>

<figure>

![Figure 7](../p395fig7.png)

<figcaption>

**Figure 7: Radar graph of the _Bibsonomy_ folksonomy**</figcaption>

</figure>

<figure>

![Figure 8](../p395fig8.png)

<figcaption>

**Figure 8: Radar graph of the _CiteULike_ folksonomy**</figcaption>

</figure>

<figure>

![Figure 9](../p395fig9.png)

<figcaption>

**Figure 9: Radar graph of term set from Gene Ontology Biological Process (GO_BP)**</figcaption>

</figure>

<figure>

![Figure 10](../p395fig10.png)

<figcaption>

**Figure 10: Radar graph of term set from the Foundational Model of Anatomy (FMA)**</figcaption>

</figure>

### Inter-set findings

Following the intra-set results, the inter-set comparisons indicate high diversity in the term sets present in the sample while also highlighting interesting relationships between them. Figures [11](#fig11) and [12](#fig12) provide an overview of the all-against-all comparison of each of the term sets using the F-measure and the measures of precision and recall respectively. They show that, in general, there was a very low amount of overlap between most of the pairs that were examined. This is likely a direct result of the wide variance of contexts associated with the diverse indexing languages represented in the sample. Though the sample was biased towards ontologies in the biomedical domain, biomedical is an extremely broad term. For example, the domains of items intended to be indexed with the different languages ranged from amino acid sequences, to biomedical citations, to tissue samples. That there was not much direct overlap in general is unsurprising.

Aside from overlaps between different term sets drawn from the same structure (e.g., between a version of MeSH with only the preferred labels and a version that included all of the alternate labels), the greatest overlap, as indicated by the F-measure, was found between the Zebrafish Anatomy (ZFA) ontology ([Sprague _et al_. 2008](#spr08)) and the Cell ontology (CL) ([Bard _et al_. 2005](#bar05)) at (f = 0.28). This overlap results because the ZFA ontology contains a large proportion of cell-related terms that are non-specific to the Zebrafish, such as _mesothelial cell_ and _osteoblast_.

<figure id="fig11">

![Figure 11](../p395fig11.jpg)

<figcaption>

**Figure 11: All against all comparison using the F-measure**  
(The white intensity of each cell (or anti-redness) is determined by the overlap (F-measure) of the term set indicated on horizontal and vertical axes. As the relationship is commutative, either side of the diagonal is identical.)</figcaption>

</figure>

<figure id="fig12">

![Figure 12](../p395fig12.jpg)

<figcaption>

**Figure 12: All against all comparison using Precision/Recall**  
(The white intensity of each cell (or anti-redness) is determined by the Precision of the term set indicated on the horizontal axis in its coverage of the term set indicated on the vertical axis. The chart may also be read as the Recall of the term set indicated on the vertical axis in its coverage (or prediction) of the term set on the horizontal axis.)</figcaption>

</figure>

Table [9](#tab9) lists the F-measure, precision and recall estimates for the term-set pairs with the highest F-measures. Aside from the ZFA/CL comparison, the greatest overlaps were observed for the inter-folksonomy pairs, MeSH and the [Agricultural Information Management Standards thesaurus](http://www.fao.org/) (Ag), MeSH and the National Cancer Institute thesaurus (NCI) ([Sioutos _et al_. 2007](#sio07)), and MeSH and _Connotea_ ([Lund _et al._ 2005](#lun05)).

<table id="tab9"><caption>

**Table 9: Term set pairs with the highest F-measures**</caption>

<tbody>

<tr>

<th>Comparison pair</th>

<th>F(x,y)</th>

<th>P(x,y) = r(y,x)</th>

<th>R(x,y) = P(y,x)</th>

</tr>

<tr>

<td>cl vs. zfa</td>

<td>0.28</td>

<td>0.46</td>

<td>0.20</td>

</tr>

<tr>

<td>

citeulike vs. _Connotea_</td>

<td>0.22</td>

<td>0.17</td>

<td>0.30</td>

</tr>

<tr>

<td>

bibsonomy vs. _Connotea_</td>

<td>0.19</td>

<td>0.37</td>

<td>0.13</td>

</tr>

<tr>

<td>

bibsonomy vs. _CiteULike_</td>

<td>0.16</td>

<td>0.47</td>

<td>0.09</td>

</tr>

<tr>

<td>ag_EN vs. mesh_prefLabel</td>

<td>0.15</td>

<td>0.14</td>

<td>0.17</td>

</tr>

<tr>

<td>ncithesaurus_prefLabel vs. mesh_prefLabel</td>

<td>0.14</td>

<td>0.10</td>

<td>0.24</td>

</tr>

<tr>

<td>

mesh_prefLabel vs. _Connotea_</td>

<td>0.12</td>

<td>0.36</td>

<td>0.07</td>

</tr>

</tbody>

</table>

#### MeSH versus the folksonomies

The second specific, demonstrative question put forward above, and one of the early motivators for this project, was the question of how the terms from MeSH compare to the terms from academic folksonomies. To answer this question, Tables [10](#tab10) and [11](#tab11) delineate the overlaps in terms of precision, recall, and the F-measure that were observed between the three folksonomies in our sample and the MeSH thesaurus (including one version with just the preferred labels and another that included alternate terms). Of the three folksonomies, _Connotea_ displayed the greatest degree of overlap with MeSH in terms of the F-measure, precision, and recall for both the preferred labels and the complete MeSH term set. The precision of the _Connotea_ terms with respect to the MeSH preferred labels was 0.073, the recall 0.363, and the F-measure was 0.122\.

<table id="tab10"><caption>

**Table 10: Precision/Recall estimates of the similarity between MeSH and three folksonomies.**  
(Each cell in the table may be read as either the precision of the term set identified for the row with respect to the term set identified by the column or the recall of the column with respect to the row. For example, the first cell indicates that the precision of _CiteULike_ with respect to mesh_all (including alternate term labels) and the recall of mesh_all with respect to _CiteULike_ is 0.047.)</caption>

<tbody>

<tr>

<th>term set</th>

<th>mesh_all</th>

<th>mesh_prefLabel</th>

<th>Bibsonomy</th>

<th>CiteULike</th>

<th>Connotea</th>

</tr>

<tr>

<td>CiteULike</td>

<td>0.047</td>

<td>0.030</td>

<td>0.094</td>

<td>1.000</td>

<td>0.170</td>

</tr>

<tr>

<td>Connotea</td>

<td>0.104</td>

<td>0.073</td>

<td>0.129</td>

<td>0.297</td>

<td>1.000</td>

</tr>

<tr>

<td>Bibsonomy</td>

<td>0.075</td>

<td>0.047</td>

<td>1.000</td>

<td>0.470</td>

<td>0.370</td>

</tr>

<tr>

<td>mesh_all</td>

<td>1.000</td>

<td>0.301</td>

<td>0.039</td>

<td>0.122</td>

<td>0.155</td>

</tr>

<tr>

<td>mesh_prefLabel</td>

<td>1.000</td>

<td>1.000</td>

<td>0.081</td>

<td>0.263</td>

<td>0.363</td>

</tr>

</tbody>

</table>

<table id="tab11"><caption>

**Table 11: F measures of the similarity between MeSH and three folksonomies**</caption>

<tbody>

<tr>

<td> </td>

<th>Bibsonomy</th>

<th>CiteULike</th>

<th>Connotea</th>

<th>mesh_all</th>

<th>mesh_prefLabel</th>

</tr>

<tr>

<th>Bibsonomy</th>

<td>1.000</td>

<td>0.157</td>

<td>0.191</td>

<td>0.051</td>

<td>0.059</td>

</tr>

<tr>

<th>CiteULike</th>

<td> </td>

<td>1.000</td>

<td>0.217</td>

<td>0.068</td>

<td>0.054</td>

</tr>

<tr>

<th>Connotea</th>

<td> </td>

<td> </td>

<td>1.000</td>

<td>0.124</td>

<td>0.122</td>

</tr>

<tr>

<th>mesh_all</th>

<td> </td>

<td> </td>

<td> </td>

<td>1.000</td>

<td>0.462</td>

</tr>

<tr>

<th>mesh_prefLabel</th>

<td> </td>

<td> </td>

<td> </td>

<td> </td>

<td>1.000</td>

</tr>

</tbody>

</table>

The fact that the _Connotea_ term set contains nearly 9000 MeSH terms (36% of the entire set of preferred labels) suggests a) that there are a number of biomedical researchers using _Connotea_ and b) that they have chosen, one way or another, to utilize MeSH terminology in the organization of their publicly accessible resource collections. How these terms came to be used in this manner is a more difficult question. In some cases, the _Connotea_ users likely recreated the MeSH terms when going about their normal tagging practices; however, the relatively high level of overlap is suggestive of other underlying factors.

#### Batch import in folksonomies

_Connotea_, as well as the other social tagging systems in the study, offers a way to import data from other sources automatically. For example, it is possible to export bibliographic information from applications such as Endnote and then import these records as bookmarks within the _Connotea_ system. This opens up the possibility that tags generated outside of the _Connotea_ system, such as MeSH indexing by MEDLINE, can wind up in the mix of the tags contained within the _Connotea_ folksonomy.

To help assess the impact of imported tags on the contents of the _Connotea_ folksonomy, we identified and removed a subset of the _Connotea_ tags that were highly likely to have been imported through the use of additional information about the context of the creation of the tags, and then recomputed all of the metrics defined above. In a social bookmarking system like _Connotea_, tags are added to the system as descriptive annotations of Web resources. When a bookmark is posted to the system by a particular user, the tags associated with it, as well as a timestamp, are associated with the entry. Sets of bookmarks posted via batch import, for example from the contents of an Endnote library, will all have nearly identical timestamps associated with them. Thus, by pruning out tags originating only in posts submitted by the same user during the same minute, we constructed a new _Connotea_ term set that should be more representative of the terms actually typed in directly by the users.

Figure [13](#fig13) shows the differences between the pruned _Connotea_ term set (Connotea_no_batch) and the original dataset on both intra-set measures and measures of direct overlap with the MeSH preferredLabel term set. In every metric except for the skewness of the lengths of the terms, the pruned _Connotea_ term set more closely resembled the other folksonomies. For example, in the pruned set, the % uniterms increased by about 10%, the % quadplus decreased by more than 30% and the flexibility increased by about 10%. The overlap with the MeSH prefLabels decreased from 0.12 to 0.11 with respect to the F measure, the precision decreased from 0.363 to 0.266, and the recall decreased from 0.073 to 0.069\.

<figure id="fig13">

![Figure 13](../p395fig13.png)

<figcaption>

**Figure 13: Differences between the full _Connotea_ term set and _Connotea_ term set with batch uploaded bookmark posts removed**</figcaption>

</figure>

It appears the process of batch uploading bookmarks in _Connotea_, in cooperation with other personal information management practices such as the use of Endnote, has influenced the contents of the _Connotea_ folksonomy. In particular, many MeSH terms appear to have been incorporated into it. Since most other folksonomies, including the others evaluated here, also have automated upload capabilities, it is highly likely that similar results may be observed within them. While this phenomenon makes the interpretation of folksonomy datasets more complex by obscuring the origins of the data, its illumination should provide new opportunities for investigation. For example, perhaps it would be possible to track the migration of terms across the boundaries of different systems through the addition of a temporal attribute to the inter-set metrics suggested here. Such data might help to explain the origins of the terms utilized in different indexing languages. One would assume for example, that many of the terms that now overlap between MeSH and the folksonomies appeared first in MeSH and then migrated over somehow; however, in the future, perhaps this process might be reversed as folksonomies are mined for candidate extensions to controlled vocabularies.

## Discussion

Robust, reproducible methods for comparing different information systems are vital tools for scientists and system developers faced with what has been called ‘_an unprecedented increase in the number and variety of formal and informal systems for knowledge representation and organization_’ ([Tennis & Jacob 2008](#ten08)). Indeed, we are in a Cambrian Age of Web-based indexing languages. Metrics and tools, such as the system for indexing language characterization described here, can be used to provide information about how the many emerging kinds of information systems relate to one another. It can also be used in the design of new systems that incorporate ideas inspired by such comparisons, as suggested by the University of California’s Bibliographic Services Task Force ([2005](#bib05)), or, as demonstrated by Good _et al._ ([2006](#goo06)) and Willighagen _et al._ ([2007](#wil07)), explicitly combine multiple extant systems to form novel hybrids.

In the research presented above, we introduced metrics for the automatic characterization and set-theoretic comparison of sets of terms from indexing languages. Using these metrics, we provided a broad-spectrum analysis of twenty-two different languages. Within the data gathered in this exploratory analysis, we identified suggestive patterns associated with the terms that compose folksonomies versus the terms from controlled vocabularies as well as directly quantifying the degree of overlap present across each of the sets in the sample. Of particular interest is the apparent migration of terms across the boundaries of the different systems, in particular from MeSH into the folksonomies. Though the results presented here are informative, the main contribution of this work is the enumeration and implementation of the comparative protocol.

Future term set analyses, particularly if they can be integrated with rich qualitative dimensions, might be put to any number of novel uses. Given the definition of these metrics and the provision of tools for their calculation, it would now be straightforward to test whether any of the term-based measurements are related to other attributes of information systems. For example, it might be interesting to test to see if any of these factors were predictive of system performance; e.g., is the percentage of uniterms in the tags that compose a folksonomy correlated with the performance of that folksonomy in the context of a retrieval task? If that answer turned out to be yes, then it would offer an easy way to estimate the retrieval performance of different systems and might suggest ways to improve performance, for example by adapting the tagging interface to encourage the contribution of more complex tags. Other potential applications include: comparative quality evaluation, term set warrant and the identification of relationships between term-set shape and theoretical types of indexing language.

### Comparative quality evaluation

From the perspective of systems evaluation, one particular use of the methods defined here might be in gold-standard based quality assessments similar to those described by Dellschaft and Staab ([2006](#del06)) for the automated, comparative evaluation of ontologies. If a particular indexing language is judged to be of high quality for some particular context, other structures might be evaluated for their quality in that or a very similar context based on their similarity to this gold-standard. For example, for the purpose of indexing biomedical documents for an institutional information retrieval system like MEDLINE, many would consider MeSH as a gold standard. The similarity of another indexing language, such as a folksonomy, to this standard might thus be used as a measure of its quality for indexing biomedical documents for retrieval. The principle advantage of such an approach is that it can be completely automatic, potentially helping to avoid the intensive manual labour and possible subjectivity associated with manual evaluations. The disadvantages are that, for any real, new application, (a) a gold standard is unlikely to exist and (b) any acceptable evaluation would still have to be informed by extensive qualitative alignment of the contextual attributes of the intended application in comparison with the gold standard.

### Term-set warrant

The creators and maintainers of indexing languages often require justifications for the inclusion or exclusion of classes within their structures ([Beghtol 1986: 110-111](#beg86)). These justifications, referred to as warrants, may come in many forms, though the most commonly discussed is probably _literary warrant_. Essentially a particular kind of warrant bases the justification for the contents of an indexing language on a particular kind of empirical evidence (e.g., user requests) or argument (e.g., philosophical or scientific warrant). The inter-set metrics may provide data useful in the development of a new kind of warrant based upon the overlap between different structures. Essentially, such a _term-set warrant_ might be invoked to justify the inclusion of terms or the concepts they represent based on the presence or absence of those terms in other structures.

### Relationship of term-set shape to theoretical type

It is tempting to think that this approach, or some extension of it, could be used to describe meaningful types of indexing languages, not from design requirements, but from the actualization of those design requirements manifest in and observable to us in the shape of term sets. This could provide a weak empirical corroboration for types of indexing languages in use, not only according to standard or theory, but based on empirical evidence of term corpus. Defending and making use of such inferences would require a solid understanding of the meaning of the different shapes. The work presented here is exploratory and future work will have to substantiate any claim at deriving type from these empirical factors. However, we can see that, in this sample, there were clear distinctions between the shapes of controlled and uncontrolled vocabularies, demonstrating at this stage that we can hypothesize that folksonomies have a particular shape in relation to both thesauri and ontologies. Future studies may take advantage of the increasing number of different indexing languages to, for example, attempt to define the relationship of term-set shape to the breakdown of theoretical type within the controlled vocabularies.

## Future work

The metrics derived and applied here operate at what amounts to a syntactic level and no specific attempt, other than rudimentary term normalization, was made to identify the concepts present in the different indexing languages. A natural extension of this work would be to apply natural language processing technology to make this attempt. The rough indications of semantic similarity provided by the inter-term set comparisons could be made much more robust if the comparisons were made at the level of concepts rather than terms, for example making it possible to equate synonymous terms from different languages.

Aside from the incorporation of natural language processing technology for concept identification, it would be useful to consider the analysis of predicate relationships between the terms (e.g., the hierarchical structure) and the analysis of the relationships between terms and the items they may be used to index. Metrics that captured these additional facets of information systems, characteristic of their form and application, would provide the opportunity for much more detailed comparisons, thus forming the raw materials for the derivation and testing of many new hypotheses.

There remain many indexing languages, both controlled and uncontrolled, that are available online that have not been characterized with the methods and from the naturalistic perspective adopted here. In addition to improving and expanding methods, the majority of future work will be the application of these tools to the analysis of other languages.

## Conclusion

We are at the very beginning of a rapid expansion in the number and the diversity of different frameworks for the organization of information. As more and more information systems come into the world, the application of expository, reproducible protocols for their comparative analysis, such as the one described in this article, will lead to ever increasing abilities to illuminate and thus build upon this expanding diversity of form and content.

## Note

**Availability**. All materials, including the programs generated to conduct the term set analysis and the term sets analysed, are freely available at [http://biordf.net/~bgood/tsa/](http://biordf.net/~bgood/tsa/).

## Acknowledgements

B.M. Good is supported by a University of British Columbia Graduate Student Fellowship.

## <a id="authors"></a>About the authors

Joseph T. Tennis is an Assistant Professor at the Information School of the University of Washington, Associate Member of the Peter Wall Institute for Advanced Study at The University of British Columbia, and Reviews Editor for Knowledge Organization. He is also a member of the Dublin Core Usage Board. He received his M.L.S. from Indiana University and the Ph.D. in Information Science from the University of Washington. He works in classification theory, the versioning of classification schemes and thesauri (subject ontogeny), and the comparative discursive analysis of metadata creation and evaluation, both contemporary and historical. He can be contacted at [jtennis@u.washington.edu](mailto:jtennis@u.washington.edu)

Benjamin M. Good is a Ph.D. candidate in Bioinformatics at the University of British Columbia. He received his M.Sc. in Evolutionary and Adaptive Systems from the University of Sussex and his B.Sc. in Cognitive Science from the University of California at San Diego. His current research focuses on the design of mass-collaborative strategies for building and automated methods for characterizing the content of the semantic Web. He can be contacted at: [goodb@interchange.ubc.ca](mailto:goodb@interchange.ubc.ca)

## References

*   <a id="alk07"></a>Al-Khalifa, H. & Davis, H. (2007). Exploring the value of folksonomies for creating semantic metadata. _International Journal on Semantic Web and Information Systems_, **3**(1), 13-39\.
*   <a id="ash00"></a>Ashburner, M., Ball, C. A., Blake, J.A., Botstein, D., Butler, H., Cherry, J. M., _et al_. (2000). Gene ontology: tool for the unification of biology. _Nature Genetics_, **25**(1), 25-29.
*   <a id="bar05"></a>Bard, J., Rhee, S. Y. & Ashburner, M. (2005). An ontology for cell types. _Genome Biology_, **6**(2), R21.
*   <a id="beg86"></a>Beghtol C. (1986). Semantic validity: concepts of warrant in bibliographic classification systems. _Library Resources & Technical Services_, **30**(2), 109-125.
*   <a id="ran08"></a>[Default English stopwords](http://www.ranks.nl/tools/stopwords.html). (2008). Amsterdam: 1ste Kreuze BV. Retrieved 26 February 2009 from http://www.ranks.nl/tools/stopwords.html
*   <a id="deg08"></a>Degtyarenko, K., de Matos, P., Ennis, M., Hastings, J., Zbinden, M., McNaught, A., _et al_.. (2008). ChEBI: a database and ontology for chemical entities of biological interest. _Nucleic Acids Research_, **36**(Database issue), D344-350.
*   <a id="del06"></a>Dellschaft, K. & Staab, S. (2006). On how to perform a gold standard based evaluation of ontology learning. In Isabel Cruz, Stefan Decker, Dean Allemang, Chris Preist, Daniel Schwabe, Peter Mika, _et al._ _Proceedings of the 2006 International Semantic Web Conference_, (pp. 228-241). Berlin, Heidelberg:
*   <a id="fei06"></a>Feinberg, M. (2006). [An examination of authority in social classification systems](http://dlist.sir.arizona.edu/1783/01/feinberg.pdf). In J. Furner & J.T. Tennis, (Eds.), _Proceedings of the 17th Workshop of the American Society for Information Science and Technology Special Interest Group in Classification Research, Austin, Texas._ Retrieved 26 February, 2009 from http://dlist.sir.arizona.edu/1783/01/feinberg.pdf (Archived by WebCite® at http://www.webcitation.org/5esiE7yRX)
*   <a id="goo06"></a>Good, B., Kawas, E., Kuo, B. & Wilkinson, M. (2006). iHOPerator: user-scripting a personalized bioinformatics Web, starting with the iHOP Website. _BMC Bioinformatics_, **7**(1), 534.
*   <a id="goo08"></a>Good, B.M. & Tennis, J.T. (2008). [Evidence of term-structure differences among folksonomies and controlled indexing languages](http://www.asis.org/Conferences/AM08/posters/78.html). Poster presentation at the Annual Meeting of the American Society for Information Science and Technology, Columbus, OH, USA. Retrieved 19 January 2009 from http://www.asis.org/Conferences/AM08/posters/78.html (Archived by WebCite® at http://www.webcitation.org/5et58YQBG)
*   <a id="hae08"></a>Haendel, M.A., Neuhaus, F., Osumi-Sutherland, D., Mabee, P.M., Mejino Jr., J.L.V., Mungall, C.J., _et al_. (2008). CARO: the Common Anatomy Reference Ontology. In A. Burger, D. Davidson and R. Baldock (Eds.), _Anatomy ontologies for bioinformatics: principles and practice_ (pp. 327-349). London: Springer.
*   <a id="hri05"></a>Hripcsak, G. & Rothschild, A.S. (2005). Agreement, the F-measure, and reliability in information retrieval. _Journal of the American Medical Association_, **12**(3), 296-298.
*   <a id="lun05"></a>Lund, B., Hammond, T., Flack, M. & Hannay, T. (2005). [Social bookmarking tools (II): a case study - Connotea.](http://www.dlib.org/dlib/april05/lund/04lund.html) _D-Lib Magazine_, **11**(4). Retrieved 26 February, 2009 from http://www.dlib.org/dlib/april05/lund/04lund.html (Archived by WebCite® at http://www.webcitation.org/5et5FPQXR)
*   <a id="mor08"></a>Morrison, P.J. (2008). Tagging and searching: search retrieval effectiveness of folksonomies on the World Wide Web. _Information Processing and Management_, **44**(4), 1562-1579.
*   <a id="ogr04"></a>Ogren, P.V., Cohen, K.B., Acquaah-Mensah, G.K., Eberlein, J. & Hunter, L. (2004). The compositional structure of gene ontology terms. _Proceedings of the Pacific Symposium on Biocomputing_, 2004, 215-225.
*   <a id="por80"></a>Porter, M. F. (1980). An algorithm for suffix stripping. _Program_, **14**(3), 130-137.
*   <a id="rde08"></a>R Foundation for Statistical Computing. _R Development Core Team_ (2008). [R: a language and environment for statistical computing](http://cran.r-project.org/doc/manuals/refman.pdf). Retrieved 26 February 2009 from http://cran.r-project.org/doc/manuals/refman.pdf
*   <a id="ros03"></a>Rosse, C. & Mejino, J.J.L.V. (2003). A reference ontology for biomedical informatics: the foundational model of anatomy. _Journal of Biomedical Informatics_, **36**(6), 478-500.
*   <a id="sio07"></a>Sioutos, N., de Coronado, S., Haber, M.W., Hartel, F.W., Shaiu, W.L. & Wright, L.W. (2007). NCI thesaurus: a semantic model integrating cancer-related clinical and molecular information. _Journal of Biomedical Informatics_, **40**(1), 30-43.
*   <a id="soe74"></a>Soergel, D. (1974). _Indexing languages and thesauri: construction and maintenance_. Los Angeles, CA: Melville Publishing Company
*   <a id="soe99"></a>Soergel, D. (1999). The rise of ontologies or the reinvention of classification. _Journal of the American Society for Information Science_, **50**(12), 1119-1120.
*   <a id="spr08"></a>Sprague, J., Bayraktaroglu, L., Bradford, Y., Conlin, T., Dunn, N., Fashena, D., _et al_. (2008). The Zebrafish Information Network: the zebrafish model organism database provides expanded support for genotypes and phenotypes. _Nucleic Acids Research_, **36**(Database issue), D768-772.
*   <a id="ten06"></a>Tennis, J. T. (2006). [Social tagging and the next steps for indexing](http://dlist.sir.arizona.edu/2091/). In J. Furner & J.T. Tennis, (Eds.), _Proceedings of the 17th Workshop of the American Society for Information Science and Technology Special Interest Group in Classification Research,_ Austin, Texas_._ Available at [http://dlist.sir.arizona.edu/2091/]
*   <a id="ten08"></a>Tennis, J.T. & Jacob, E.K. (2008). Toward a theory of structure in information organization frameworks. In Clément. Arsenault & Joseph T. Tennis, (Eds.). _Culture and identity in knowledge organization: proceedings of the Tenth International ISKO Conference, 5-8 August 2008, Montreal, Canada_, (pp. 262-268) Würzburg, Germany: Ergon.
*   <a id="tud06"></a>Tudhope, D. (2006). _A tentative typology of KOS: towards a KOS of KOS?_ Paper presented at the _5th European NKOS Workshop_, Alicante, Spain. [[PowerPoint presentation](http://bit.ly/12QbDC) retrieved 26 February, 2009 from http://bit.ly/12QbDC]
*   <a id="bib05"></a>University of California Libraries. _Bibliographic Services Task Force_. (2005). _[Rethinking how we provide bibliographic services for the University of California.](http://libraries.universityofcalifornia.edu/sopag/BSTF/Final.pdf)_ Retrieved 8 July, 2008 from http://libraries.universityofcalifornia.edu/sopag/BSTF/Final.pdf (Archived by WebCite® at http://www.webcitation.org/5eshFEjEt)
*   <a id="van76"></a>Van Slype, G. (1976). _Definition of the essential characteristics of thesauri_. Brussels: Bureau Marcel van Dijk.
*   <a id="wil07"></a>Willighagen, E., O'Boyle, N., Gopalakrishnan, H., Jiao, D., Guha, R., Steinbeck, C., _et al_. (2007). Userscripts for the life sciences. _BMC Bioinformatics_, **8**(1), 487.
*   <a id="wit00"></a>Witten, I.H. & Frank, W. (2000). _Data mining: practical machine learning tools with Java implementations_, San Francisco, CA: Morgan Kaufmann.
*   <a id="zha06"></a>Zhang, X. (2006). Concept integration of document databases using different indexing languages. _Information Processing and Management_, **42**(1), 121-135\.

## <a id="app1"></a>Appendix 1\. Data collection: assembly of term sets

### MeSH

1.  Files representing the 2008 release of MeSH were downloaded from [http://www.nlm.nih.gov/mesh/filelist.html](http://www.nlm.nih.gov/mesh/filelist.html) on 11 February 2008.
2.  The preferred labels for the terms were taken from the downloaded file 'mshd2008.txt'.
3.  The union of the preferred labels and the synonyms (mesh all) was extracted from the downloaded MeSH XML file 'desc2008' using a Java program.
4.  The MeSH terms with comma separated adjectives, like 'Cells, Immobilized', were programmatically re-ordered to reflect a more natural English language usage of adjective-noun, such as 'Immobilized Cells'. This step was taken to facilitate comparison with the other indexing languages that tended much more towards this form.

### OWL/RDF formatted thesauri and ontologies

Unless otherwise, noted, all the labels for the concepts and their synonyms were extracted from the files using custom Java code built with the [Jena OWL/RDF API](http://jena.sourceforge.net/).

#### ACM – Association for Computing Machinery

An OWL-XML version of the 1998 ACM thesaurus was acquired from Miguel Ferreira of the Department of Information Systems at the University of Minho.

#### AG – AGROVOC thesaurus from the Agricultural Information Management Standards initiative

An OWL-XML file containing the thesaurus ('ag_2007020219.owl') was downloaded from [http://www.fao.org/aims/ag_download.htm](http://www.fao.org/aims/ag_download.htm)

#### BioLinks

BioLinks is a subject listing used to organize the [bioinformatics links directory](http://bioinformatics.ca/links_directory/). An OWL version of these subject headings was composed by one of the authors in August of 2007, and is available at [http://biordf.net/~bgood/ont/BioLinks.owl](http://biordf.net/~bgood/ont/BioLinks.owl).

#### Open Biomedical Ontologies (OBO)

The daily OWL versions of the following ontologies from the [OBO foundry](http://obofoundry.org/) were downloaded from [http://www.berkeleybop.org/ontologies/](http://www.berkeleybop.org/ontologies/) on 11 February 2008.

*   Gene Ontology (biological process, molecular function, cellular component)
*   CARO – common anatomy reference ontology
*   CHEBI – chemical entities of biological interest
*   CL – cell ontology
*   ENVO – environment ontology
*   FMA – an OWL version of the Foundational Model of Anatomy
*   NCI Thesaurus – National Cancer Institute thesaurus
*   OBI – Ontology for Biomedical Investigations
*   PATO – Phenotypic Quality ontology
*   PRO – Protein Ontology
*   SO – Sequence Ontology
*   ZFA – Zebrafish Anatomy and Development Ontology

#### GEMET

GEMET - the thesaurus used by the European Environment Information and Observation Network - was downloaded from [http://www.eionet.europa.eu/gemet/rdf?langcode=en](http://www.eionet.europa.eu/gemet/rdf?langcode=en) on 15 February 2008\. The English terms were extracted from the provided HTML table.

#### Folksonomies (collections of tags created in social bookmarking systems)

*   Connotea  
    The _Connotea_ folksonomy was extracted from 377885 posts to _Connotea_ collected prior to 12 December 2007\. The [Connotea Web API](http://www.connotea.org/wiki/WebAPI) and the [Connotea Java library](http://code.google.com/p/connotea-java/) were used to gather and process the data.
*   Bibsonomy  
    The _Bibsonomy_ tag set was extracted from a 1 January 2008 export of the Bibsonomy database. It is available for research upon request from [Webmaster@bibsonomy.org](mailto:Webmaster@bibsonomy.org).
*   CiteULike  
    The _CiteULike_ tag set was extracted from a 31 December 2007 export of the _CiteULike_ database. Daily versions of this database are available for research purposes from [http://www.citeulike.org/faq/data.adp](http://www.citeulike.org/faq/data.adp).