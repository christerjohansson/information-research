#### vol. 15 no. 4, December, 2010

## Proceedings of the Seventh International Conference on Conceptions of Library and Information Science - "Unity in diversity" - Part 2

# Ecological dimensions of information literacy

#### [Jela Steinerová](#author)  
Comenius University Bratislava, Slovakia  
Department of Library and Information Science

#### Abstract

> **Introduction**. We examine relationships between information literacy and information ecology with regard to conceptual innovation in information science. We aim to expand our understanding of human information behaviour and relevance assessment in the electronic environment.  
> **Method**. Conceptual analysis and conceptual mapping is used and relationships of main concepts are determined in a concept map. Earlier empirical research on information behaviour styles and relevance assessment using surveys and interviews are synthesized.  
> **Analysis**. Two information styles (pragmatic and analytic) are characterized. Relevance assessment in the electronic environment is analysed. Relevance is considered as part of information literacy. Examples of students' information horizons and a concept map of relevance are given.  
> **Results**. Ecological dimensions of information literacy are determined at semantic (relevance), visual (information horizon, concept maps) and behavioural and social levels (information styles). A model of ecological dimensions of information literacy is outlined based on triangle of values, tools and communities. Recommendations for development of students' information literacy are presented at levels of information seeking and relevance assessment.  
> **Conclusions** Implications for information literacy development in the electronic environments and for information and educational strategies are derived. Relevance assessment should be actively trained as part of information literacy and guided inquiry. Ecological dimensions of information literacy can be used in new models of e-learning and digital libraries.

## Introduction

Conceptual innovations in information science are important sources of introducing new theoretical frameworks. In this paper we examine information ecology as part of emerging science 2.0 and reflect on reconsideration of information literacy in information science. Science 2.0 combines technological innovations with knowledge of social systems ([Shneiderman 2008](#ref52)). In science 2.0 collaboration-centred, socio-technical systems dominate. As examples we can mention Web 2.0, social networking, blogging and citizen journalism and patient-centred medical information systems. Science 2.0 will be oriented to human relationships in social (electronic) networks including trust, empathy, responsibility, privacy. This represents a challenge for information science concentrated on information processing and use.

Information ecology examines the position of human beings in information places and spaces. These complex relationships are manifested by sense-making in the information environment. Information literacy can be considered as part of these relationships at two levels. The first level includes relationships between individuals and information systems in information seeking and uses (Web services, tools and products). The second level is represented by interactions in social networks and social media.

This paper attempts to review if and how information ecology contributes to the information literacy concept. It is not aimed at analysis of guidelines and standards, but draws on our earlier research based on empirical data on information use. We suppose that ecological dimensions of information literacy are closely tied to knowledge of information behaviour styles and relevance assessment. The first section determines background, research questions and methodology. Next sections review several concepts of information ecology and selected models of information literacy. Information styles and information horizons are described and recommendations for students' information literacy presented. Results of two research projects on users' behaviour and relevance assessment are synthesized. In conclusion a model of ecological dimensions of information literacy is presented.

## Problem statement and methodology

Potential benefits of the information ecology perspective for information literacy can be seen in an holistic view of activities of people in the information environment. The emphasis is put not on information skills, but on interconnections of people and information and on ways of integrating information use into the natural human information environment. Most important ecological dimensions of information use include adaptations, monitoring of the information environment, filtering of information and social co-operation.

In this paper we consider the following questions: _How should we reconsider information literacy in the context of information ecology? Can we prove the shift in interpretations of information literacy from text-oriented towards socio-cultural perspective by examination of its ecological dimensions? Is information ecology a productive framework for the improvement of information literacy practice (teaching and learning)? How can we apply results of previous empirical research on information styles and relevance assessment to development of students' information literacy?_

Earlier empirical data based on quantitative and qualitative methods (mainly surveys, interviews and concept mapping) are further analysed and synthesized in characterization of information styles and relevance assessment. Further interpretations of previous research projects are applied to derive ecological dimensions of information literacy. The term dimension is used here as appropriate for description of holistic perspective on facets or aspects of information literacy.

The focus of information ecology is on human information behaviour and on information use. We suppose that empirical data on users' behaviour can help disclose new aspects of information literacy and build new models. At the empirical level practical recommendations for information use should be introduced. Another assumption is that the perspective of information ecology could help improve the teaching of information literacy and its integration into guided learning at various levels of education. As examples of new ecological tools we present building of students' information horizons and a concept map resulting from research on relevance.

For a theoretical methodological perspective we chose the following: cognitive construction of meanings for information literacy and relevance assessment, sense-making for information behaviour styles and activity theory for modelling ecological dimensions of information literacy. Relationships between main concepts in this paper are depicted on Figure 1\. Information ecology comprises interconnections between humans and information environment. It subsumes information behaviour as activities in the information environment Information behaviour relies on information horizon and is manifested by information styles and relevance assessment.

<div align="center">![Conceptual map - information ecology and information literacy](colis719fig1.jpg)</div>

<div align="center">  
**Figure 1: Conceptual map of information ecology and information literacy**</div>

## Information ecology

The concept of information ecology emerged from information management and information behaviour studies. It is based on complex relationships between humans and technologies while using information in communities and organizations. Information ecology was determined by Davenport and Prusak ([1997](#ref08)) as making information meaningful. Information ecology as a metaphor should help in managing the information environment and information behaviour styles.

Another concept of information ecologies ([Nardi and O'Day 1999](#ref36)) is based on relationships between information technologies and people in transforming information to knowledge. Information ecologies represent the procedures, goals, values of communities, which are supported by technologies. Information ecologies are places where people use tools and help each other in information activities through social relationships.

An ecological model of information seeking and use ([Williamson 2005](#ref66)) depicts a social actor involved in such settings as information needs, specific personal, physical, working and social contexts. The features of adapting and monitoring of information environment are here emphasized.

Based on environmental psychology, an ecological constructionist model of user information behaviour ([Nahl 2007](#ref33)) was developed. It integrates the affective, cognitive and sensorimotor parts of information activities.

Information ecology is also connected with studies of affective information behaviour ([Nahl and Bilal 2007](#ref34)). Emotions influence perception and use of information as well as relationships to information technologies. Nahl explained the affective revolution in information science with emphasis on affective filters in information seeking and the evaluation of information. A model of the affective information behaviour ecology ([Given 2007](#ref15)) identifies macro- and micro-emotional contexts that shape students' information behaviour. The author calls for more attention to affective dimensions of services and products in the academic information environment.

Our concept of information ecology is based on meaningful information activities in the information environment with regard to information behaviour styles of different social actors. Information literacy and relevance judgments are often intuitively involved in information activities and can be reconsidered in a new context of information ecology.

## Information literacy

Information literacy is here regarded as knowledge, skills and efficient information practices in human information behaviour. Educators all over the world developed learning strategies for students with respect to recognition of information need, development of information strategies, use of information sources and building of pathways for gaining knowledge.

Most important standards of information literacy include standards for students' learning for schools ([AASL and AECT 1998](#aas98)), the Association of College and Research Libraries information literacy competency standards for higher education ([2004](#acrl)) and the Special Libraries Association competencies of information literacy ([Abels _et al._ 2003](#abe03)). Information literacy has also been supported by international organizations (especially UNESCO and IFLA). In library and information science three models dominated in modeling information literacy: Eisenberg and Berkowitz's Big6skills, Doyle's attributes of the information literate person ([Doyle 1992](#ref68)) and Bruce's seven faces of information literacy ([Bruce 1997](#ref69)).

Information literacy in Eisenberg and Berkowitz's ([2009](#ref11)) model of Big6 steps include task definition, creating information seeking strategies, locating and access to information, use of information, synthesis of information and evaluating information. The model was developed in the 1980s. While Doyle specifies attributes of an information literate person (e.g., recognition of information need, formulating questions, accessing sources, evaluating information, organizing information, integrating information), Bruce's model is more ecological in that it sees information literacy as experiencing information use, information practices and reflection. Her seven facets of information literacy experience include: information technologies for retrieval and communication, information sources, information process, information control, knowledge construction, knowledge extension and wisdom ([Bruce 2002](#ref05)).

Known patterns of students' information behaviour have revealed common characteristics of students' learning styles in different countries. For example, Limberg ([Limberg 2000](#ref27)) determined three styles of information seeking and use in Swedish schools (fact finding, balancing information or forming a personal standpoint, scrutinizing and analysing). She also discovered that students who approached information seeking and use in more sophisticated ways also achieved better learning outcomes. This depended especially on understanding, interpretation and reproduction. Limberg's methodological approach based on phenomenography comprises ecological dimensions when studying complex phenomena of information needs and information literacy in natural environments and ways of experiencing information use ([Limberg 2000](#ref27)).

Research in library and information science (e.g. [Kuhlthau _et al._ 2007](#ref23); [Limberg 2007](#ref28)) confirms that information literacy develops from basic competencies (reading, writing, numeracy, basic cognitive information processing) through information and communication technology skills, to cognitive processes of problem-solving, communication and decision making. Another perspective integrates information literacy into a set of other literacies or multiliteracies (social, scientific, economic, civilization, cultural, etc.) Information literacy is regarded as a sociocultural practice (e.g., [Lloyd 2007](#ref29)). A situated perspective puts information literacy into various social contexts (work, education, leisure, everyday information practices). From the perspective of metacognition information literacy includes self-appraisal and self-management in information use. Guided inquiry concept ([Kuhlthau _et al._ 2007](#ref23)) then introduces the support of students' own natural ways and styles of information literacy development in learning. In the electronic environment users can form their own information grounds and systems of personal information management.

Common features of information ecology models and information literacy models are interconnections between macro- and micro-environments, human information behaviour and sense-making through cognition, communication, collaboration, evaluation and use of information in information activities. However, little attention has been paid to information styles and relevance assessment as parts of information literacy. That is why we summarize these aspects in next sections based on results of our research at the Department of Library and Information Science, Comenius University Bratislava.

## Information styles and information literacy

We regard information style as generalizations of typical ways and preferences of people in information processing and use which depend on personality and tasks ([Solomon 1997](#ref54); [Heinström 2003](#ref17)). Human information behaviour in the electronic environment is marked by easy access, quick online reading and social networking. Changes in users' information behaviour are influenced by new tools and services of digital libraries. Young students are digitally literate, prefer visual information processing, inductive discovery, fast response time ([Oblinger and Oblinger 2005](#ref41)). The Net Generation exhibits tendency to socialize and participate in networks. Although many surveys generalize their patterns of information use, the information strategies of individuals may be unique. From the viewpoint of information literacy the problem is that students behave like digital consumers in digital spaces ([Nicholas _et al._ 2005](#nic05)) and that social networking and consumer Websites change their behaviour in the forms of education, learning and scholarship. Other research ([Head & Eisenberg 2009](#ref16)) confirms that students conceptualized information seeking as a fast, pragmatic task and not as an opportunity to learn and construct meaning. Important contexts of information use were identified as big picture, language, situation and information gathering.

When modelling information literacy it is important to recognize the information style and find out an efficient type of its natural support. If we integrate information styles into information literacy, then we can develop ecological information literacy, i.e., the natural development of affective, cognitive and sensorimotor components of information use ([Nahl 2001](#ref71)).

An information environment can be determined as information, human, technological and organizational resources aimed at the support of information activities. Our studies into users' information behaviour of academic libraries in the project on the _Interaction of Man and Information Environment_ confirmed two information styles of students' behaviour, i.e., the pragmatic and analytic styles. We ran a series of surveys in academic libraries based on an original methodology which integrated knowledge from social and cognitive sciences into information behaviour. Information seeking preferences and perceptions, use of electronic resources and electronic publishing were analysed. The conception and methodology were reported elsewhere ([Steinerová and Šušol 2005](#ref60); [Steinerová _et al._ 2004](#ref59)).

The pragmatic style prefers simple access to information, simple organization of knowledge, low cost and fast access to electronic resources (quantity and time). The analytic style is marked by deeper intellectual information processing and puts emphasis on reliability and verification of information and reviewing process (quality and relevance). In our surveys we found out that pragmatic style of information processing dominates.

Typical differences between these two styles are summarized in table 1.

<table width="80%" border="1" cellspacing="0" cellpadding="3" align="center" style="border-right: #99f5fb solid; border-top: #99f5fb solid; font-size: smaller; border-left: #99f5fb solid; border-bottom: #99f5fb solid; font-style: normal; font-family: verdana, geneva, arial, helvetica, sans-serif; background-color: #fdffdd"><caption align="bottom">  
**Table 1 Differences between pragmatic and analytic styles**</caption>

<tbody>

<tr>

<th> </th>

<th>Pragmatic</th>

<th>Analytic</th>

</tr>

<tr>

<td>seeking</td>

<td>horizontal</td>

<td>explorative</td>

</tr>

<tr>

<td>terminology</td>

<td>clear, simple</td>

<td>multidisciplinary</td>

</tr>

<tr>

<td>assessment</td>

<td>surface, serendipity</td>

<td>experience in relevance judgments</td>

</tr>

<tr>

<td>organization</td>

<td>surface, field dependence</td>

<td>integrative, based on expert knowledge and experience</td>

</tr>

<tr>

<td>planning</td>

<td>intuitive, simple queries</td>

<td>complex queries</td>

</tr>

<tr>

<td>purpose</td>

<td>orientation</td>

<td>intellectual processing</td>

</tr>

<tr>

<td>emotions</td>

<td>trust, optimism</td>

<td>doubts</td>

</tr>

<tr>

<td>motivation</td>

<td>fast solution</td>

<td>understand contexts</td>

</tr>

<tr>

<td>Access to information</td>

<td>navigation</td>

<td>interpretation</td>

</tr>

</tbody>

</table>

For integration of information styles into information literacy development it is important to understand what is important to students and how they learn best and work with information. Within the pragmatic style they would not read extensive texts, they are experiential learners. Critical reading is used especially at higher levels of education and in the social sciences and humanities. The challenge is how to accommodate dominant pragmatic information and learning styles to the needs of building knowledge in the analytic style.

The ecological dimensions of information literacy can be determined by changing, interactive information activities of social actors (e.g., students' learning, citizens' information use, education and research of scientists and educators). From different viewpoint information literacy is embedded in ecological characteristics of information environment determined as mixture of sources, systems and services managed by a social actor.

## Information horizons and information literacy

Two stages of information seeking were determined in our research, i.e. the orientation and the analytic stages. In the orientation stage it can be productive to build an information horizon as part of information literacy development ([Savolainen 2008](#ref49); [Sonnenwald _et al._ 2001](#ref53)). An information horizon is a map of information sources including experts, criteria of source preferences, issues of interest and information pathways. It also uses criteria for relevance assessment, especially credibility and cognitive authority and contexts and situations which make an impact on the added value to information.

By depicting an information horizon we develop information strategies as a special approach to solving an information problem. Information activities then involve orientation (perception of problem, setting the goal, analysis), strategy (choice of procedures, practices) and synthesis and verification (feedback). While building information horizons people often form a hierarchy of information and information sources (most relevant, sources of secondary importance, peripheral). In our memory we have recorded successful strategies and errors which we use as examples of building a new information horizon.

The information horizon concept was used as a case study in the course on Information Retrieval and Seeking. Graphs of our students proved that they relied on their previous experience and could develop their information literacy when they see information sources, order of their use and their representations placed into the information horizon. Examples of information horizons of students of library and information science in Bratislava are shown in Figure 2.

<div align="center">![Information horizons examples of library and information science students](colis719fig3.jpg)</div>

<div align="center">![Information horizons examples of LIS students](colis719fig2.jpg)</div>

<div align="center">  
**Figure 2: Information horizon examples of library and information science students as part of information literacy development**</div>

Based on results of our surveys and on case studies of students' information horizons we developed a set of recommendations for students' information literacy:

## Relevance assessment and information literacy

Another key part of ecological approach to information literacy is the concept of relevance. In our second project on Information use and information behaviour we examined relevance judgments of doctoral students ([Steinerová _et al._ 2007](#ref58)). Based on our research we articulated a set of recommendations for relevance assessment as part of information literacy ([Steinerová _et al._ 2010](#ste10)).The study of relevance as part of information use by twenty-one doctoral students was designed within a phenomenographic approach ([Steinerová 2008](#ref57)). Semi-structured interviews as a main method of gathering data were applied. Students were selected from social sciences at the Faculty of Philosophy, Comenius University Bratislava, Slovakia. Research questions concentrated on perception of relevance, manifestations of relevance in the electronic environment, ways of categorization and typology of relevant information. Results were visualized in concept maps.

An example of a concept map of relevance in the electronic environment is on the Figure 3.

<div align="center">![An example of concept map Relevance in the electronic environment](colis719fig4.jpg)</div>

<div align="center">  
**Figure 3: An example of concept map '_Relevance in the electronic environment_'**</div>

Respondents confirmed relevance as value, utility and importance. Main findings suggest that the same criteria are used through different contexts and are related to development of information needs. Relevance judgment was confirmed as multidimensional process, based on multi-criteria cognitive processing. Relevance is experienced and integrated by emotions, especially delight, discovery and anger.

From the perspective of information literacy we found out that users need support for discovering, decision-making and participation. Policy makers should consider relevance behaviour in information strategies of education and information literacy.

We suggest that relevance assessment should be deliberately developed as part of information literacy development. Concept maps can support relevance and information literacy development. Concept maps are representations of spatial information and help develop cognitive processing, filtering and adaptations. Students can build their understanding of a topic with the use of such tools as C-maps ([Novak and Canas 2008](#ref39)). They can construct both meaning and relevance in the electronic environment. Resulting models of relevance emphasize interaction, linking, visualize concepts and relations, as well as community and collaboration ([Steinerová 2008](#ref57)).

Results also determined emerging patterns of relevance in the electronic environment. Hierarchy and associations of knowledge organization support relevance judgments Collaborative use and flexibility help students build their relevance assessment. Relevance in the electronic environment is marked by non-linearity, flexibility of navigation, high-level visualization. Quest for the origin of the source is facilitated by such characteristics of electronic sources as scattered information, serendipity of information discovery, context and non-linearity. Information literacy requires digital skills and additional ethical awareness, as information processing in the electronic environment integrates creation, seeking and use.

Frequent use of electronic sources is conditioned by contexts: topics, disciplines, tasks. In the electronic environment students appreciate topicality, speed and technological features of linking. Printed sources were approached in a more emotional way, putting stress on readability and reliability, quality, intellectual depth, stability, predictability. Several students admitted that manifold mediation in electronic texts make the construction of meanings and relevance assessment more complicated.

Based on our research we articulated a set of recommendations for relevance assessment as part of information literacy. Relevance connects information literacy with critical evaluation of information sources.

_gestalt_

## Ecological dimensions of information literacy

Based on previous analyses ecological dimensions of information literacy include the semantic dimension (relevance), the visual dimension (information horizon, concept maps), the behavioural dimension (information style) and the social dimension (community, values).

Ecological dimensions of information literacy are derived from the following assumptions:

Information literacy is interconnected with digital skills and ethical awareness (fair use). The tools for satisficing (completion of information seeking) and optimizing in information systems ([Nahl 2007](#ref33)) can help assess relevance and make sense of information. The main ecological features of information literacy are determined as adaptations and filtering. They support transition from information to knowledge.

As examples of tools for support of information literacy we can mention concepts maps, topic maps, intelligent thesauri or complex ontologies. We regard these tools as crucial in information ecology research as cleaning filters that support sense-making. Information literacy development should support natural information activities and user pathways in electronic environments. This support is represented by navigation, filtering and help with cognitive and affective overload.

## A model of ecological dimensions of information literacy

Information literacy from the ecological perspective draws also on the activity theory ([Wilson 2006](#ref67)). The information activity can be modelled by the Engeström's model triangle composed of three components: man, information objects and tools. This can be combined into such activities as production, consumption and distribution of information. Besides communicative tools an important role is played by community, values, division of labor and collaboration. Cognition and communication, adaptation and interaction, relevance, satisficing and optimizing are crucial principles for modelling information literacy.

A model of ecological dimensions of information literacy (Figure 4) connects micro-level of information literacy (affective, cognitive, sensomotoric, social strata of social actors) with macro-level of information environment. Social actor builds his information literacy as a pathway through the information environment. On this pathway he uses a set of values (for relevance assessment), special tools (language, knowledge organization tools) and acts as part of communities (operated by group standards, values, social roles and communication patterns). The left side of the model integrates intellectual processes of information management with their transformation to wisdom as knowledgeable decisions. The right side of the model depicts interconnections of information sources and information processes. Links between external and internal environments are manifested by use of information sources, information technologies, information styles, relevance assessment and by construction of meanings. Both sides frame information literacy as transition of information to knowledge. The model generalizes our empirical data on information styles and relevance behaviour. Ecological dimensions of information literacy represent development of information literacy by existence, activities and movements in the information environment.

<div align="center">![A model of ecological dimensions of information literacy](colis719fig5.jpg)</div>

<div align="center">  
**Figure 4: A model of ecological dimensions of information literacy**</div>

Thus, information ecology of information literacy can be determined as a way of information existence in the information environment. It is not the volume of information or immediacy of delivery that matter but tools for support of making sense and transformation of information to knowledge.

Ecological dimensions of information literacy can be further elaborated and the following questions can be asked: _How to provide contexts for sense making? How to develop information literacy by learning to know and learning to do? Which digital tools can make information use more efficient?_ and _What are the connections between information literacy and learning in communities (learning to live together and learning to be)?_

Information ecology helps us identify those factors that make an impact on the information environment and tools for eliminating information overload and risks of information use. Ecological dimensions are determined at micro-level (individual cognitive, affective, sensomotoric skills in building information horizons and pathways) and at macro-level (information sources, systems, environments). In interactions between these levels and factors we can follow continuity and discontinuity of information literacy development. It covers formal and informal communications, collaboration, quality control, information overload, creativity and knowledge production Ethical, legal and security awareness and standards are other ecological dimensions of information literacy which are out of the scope of this paper.

## Conclusions

Information ecology concept contributes to information literacy by holistic view on information processing and use. Ecological theories of human information behaviour place human beings in the centre of information environments. Information ecology helps reconsider information literacy in contexts of science 2.0 aimed at integration of technological innovations and social information processes. While in technological concepts people had to adapt to technologies, in ecological concepts technologies are adapted to information needs and become parts of information literacy.

Several models of information ecology and information literacy have been developed so far. These models depict cognitive processes and skills required for information use. From our perspective we integrated information behaviour styles and relevance assessment into ecological dimensions of information literacy which include especially semantic, visual, behavioural and social dimensions. As examples we mentioned students' information horizons and concept maps. We derived recommendations for support of students' information literacy and for relevance assessments. This approach fills the gap in integration of relevance research into information literacy concepts.

Information literacy in the electronic environment develops towards new ways of communicating in short-term groups (e.g., classroom) or longer term groups (family, friends).

Information habits of students in the electronic environment indicate that simplicity, attractiveness, images and immediacy dominate the information use. That is the reason why ecological models of information literacy emphasize navigation, personalization and visualization. Guided inquiry promotes analytical information behaviour style and practical information processing skills. Guidance is provided in development and adaptations of users' information horizons and strategies.

Concept mapping and other visual perception tools support information literacy by pointing to landmarks, connectedness, visual hierarchy, spatial proximity, colors, shapes, etc. The _networked social library_ requires information literacy based on interactive features of collaboration, creativity, production and community building. Ecological dimensions of information literacy can be mirrored in digital libraries in new rules of information processing and use. The presented model of ecological dimensions of information literacy interconnects tools, values and communities that shape information activities. This can be applied to practical information literacy development of students integrated in electronic environments.

Benefits of ecological dimensions of information literacy are in mastering relations between external and internal information environments. The model could support design of digital libraries using resource-based learning, problem-oriented and project-oriented learning, multimedia learning, e-learning, etc. Understanding ecological dimensions of information literacy and more empirical data can be beneficial for development of information literacy strategies at various levels and situations of education and learning.

### Acknowledgement

This paper was developed as part of the research project VEGA 1/0429/10, supported by the Ministry of Education of the Slovak Republic.

## About the author

Jela Steinerová is Professor in the Department of Library and information Science, Comenius University Bratislava, Faculty of Arts, Bratislava, Slovakia (professor, Comenius University Bratislava, 2010). She can be contacted at [jela.steinerova@fphil.uniba.sk](mailto:jela.steinerova@fphil.uniba.sk)

#### References

*   Abels, E., Jones, R., Lathan, R,. Magnoni, D. & Marshall, J.G. (2003). [_Competencies for information professionals of the 21st century._](http://www.webcitation.org/5umAsULIS) Alexandria, VA: Special Libraries Association. Retrieved December 6th 2010 from http://www.sla.org/content/learn/members/competencies/index.cfm (Archived by WebCite® at http://www.webcitation.org/5umAsULIS)
*   American School Library Association _and_ Association for Educational Communications and Technology. (1998). Information power: building partnerships for learning. Chicago, IL: ASLA and AECT.
*   Association of College and Research Libraries. (2004). _[Information literacy competency standards for higher education.](http://www.webcitation.org/5um9QiC5f)_ Chicago, IL: ACRL. Retrieved December 5, 2010 from http://www.ala.org/ala/mgrps/divs/acrl/standards/standards.pdf (Archived by WebCite® http://www.webcitation.org/5um9QiC5f)
*   Bruce, C.S. (1997). _Seven faces of information literacy_. Adelaide, South Australia: AUSLIB Press.
*   Bruce, C.S. (2002). _[Information literacy as a catalyst for educational change. A background paper.](http://www.Webcitation.org/5RjPtrRgo)_ (White Paper prepared for UNESCO, the U.S. National Commission on Libraries and Information Science and the National Forum on Information Literacy for use at the Information Literacy Meeting of Experts, Prague, The Czech Republic.for use at the Information Literacy Meeting of Experts, Prague, The Czech Republic.) Retrieved 30 July, 2010 from: http://www.nclis.gov/libinter/infolitconf&meet/papers/bruce-fullpaper.pdf (Archived by WebCite® at http://www.Webcitation.org/5RjPtrRgo)
*   Davenport, T.H. & Prusak, L. (1997). _Information ecology: mastering the information and knowledge environment._ New York, NY: Oxford University Press.
*   Doyle, C. 1992\. _[Outcome measure for information literacy within the National Educational Goals of 1990\. Final Report to the National Forum on Information Literacy](http://www.webcitation.org/5v0MnOkf2)_. Flagstaff, AZ: National Forum on Information Literacy. Retrieved 15 December, 2010 from http://www.eric.ed.gov/PDFS/ED351033.pdf (Archived by WebCite® at http://www.webcitation.org/5v0MnOkf2)
*   Eisenberg, M. & Berkowitz, B. (2009). [The Big6<sup>™</sup> skills](http://www.Webcitation.org/5mSWNlstU). Retrieved 30 November, 2010 from http://www.big6.com/ (Archived by WebCite® at http://www.Webcitation.org/5mSWNlstU)
*   Given, L.M. (2007). Emotional entanglements on the university campus: the role of affect in undergraduates' information behaviors. In _Information and emotion: the emergent affective paradigm in information behavior research and theory_. (pp. 161-175). Medford, NJ: Information Today.
*   Head A.J. & Eisenberg, M.B. (2009). _[How college students seek information in digital age.](http://www.Webcitation.org/5um3Dw2al)_ Seattle, WA: University of Washington, The Information School. (Project Information Literacy. Progress Report) Retrieved 21 January, 2010 from http://projectinfolit.org/pdfs/PIL_Fall2009_Year1Report_12_2009.pdf (Archived by WebCite® at http://www.Webcitation.org/5um3Dw2al)
*   Heintström, J. (2003). [Five personality dimensions and their influence on information behaviour](http://www.Webcitation.org/5KLUg1rqR). _Information Research_, **9**(1), paper 165\. Retrieved 30 November, 2010 from: http://InformationR.net/ir/9-1/paper165.html (Archived by WebCite® at http://www.Webcitation.org/5KLUg1rqR)
*   Kuhlthau, C.C., Maniotes, L.K. & Caspari, A.K. (2007). _Guided inquiry. Learning in the 21st century_. Westport, CT: Libraries Unlimited
*   Limberg, L. (2000). Phenomenography: a relational approach to research on information needs, seeking and use. _The New Review of Information Behaviour Research_, **1**, 51-68.
*   Limberg, L. (2007). [Learning assignment as task in information seeking research.](http://www.Webcitation.org/5um3pH8fw) _Information Research_, **12**(1) paper colis28\. Retrieved 30 November, 2010 from http://InformationR.net/ir/12-1/colis/colis28.html (Archived by WebCite® at http://www.Webcitation.org/5um3pH8fw)
*   Lloyd, A.M. (2007). [Recasting information literacy as sociocultural practice: implications for library and information science researchers.](http://www.Webcitation.org/5tzifGKIB) _Information Research_ 12(4), paper colis34\. Retrieved 30 November, 2010 from http://InformationR.net/ir/12-4/colis34.html (Archived by WebCite® at http://www.Webcitation.org/5tzifGKIB)
*   Nahl, D. (2001). [A conceptual framework for explaining information behavior](http://utpjournals.metapress.com/content/f695043tk142qh56/fulltext.pdf). _Studies in Media and Information Literacy_, **1**(2), 1-15\. Retrieved 15 December, 2010 from http://utpjournals.metapress.com/content/f695043tk142qh56/fulltext.pdf [Unable to archive to WebCite]
*   Nahl, D. (2007). A discourse analysis technique for charting the flow of micro-information behaviour. _Journal of Documentation_, **63**(3), 323-339.
*   Nahl, D. & Bilal, D., (Eds.) (2007). _Information and emotion: the emergent affective paradigm in information behavior research and theory._ Medford,NJ: Information Today.
*   Nardi, B.A. & O'Day, V.L. (1999). _Information ecologies: using technology with heart._ Cambridge, MA: MIT Press.
*   Nicholas, D., Huntington, P. & Dobrowolski, T. (2005). The digital information consumer. In Amanda Spink & Charles Cole, (Eds.). _New directions in human information behavior,_ (pp. 203-228). Dordrecht, the Netherlands: Springer.
*   Novak, J.D. &. Cañas, A.J. (2008). _[The theory underlying concept maps and how to construct them.](http://www.webcitation.org/5cyS52XTv)_ Pensacola, FL: Florida Institute for Human and Machine Cognition. (Technical Report IHMC CmapTools 2006-01 Rev 01-2008). Retrieved 30 November, 2010 from: http://cmap.ihmc.us/Publications/ResearchPapers/TheoryUnderlyingConceptMaps.pdf. (Archived by WebCite® at http://www.webcitation.org/5cyS52XTv)
*   Oblinger, D.G. & Oblinger, J.L. (Eds.). (2005). _[Educating the Net Generation](http://www.webcitation.org/5uPsjqRRl)_. Washington, DC: EDUCAUSE. Retrieved 30 November, 2010 from: http://www.educause.edu/ir/library/pdf/pub7101.pdf (Archived by WebCite® at http://www.webcitation.org/5uPsjqRRl)
*   Savolainen, R. (2008). _Everyday information practices: a social phenomenological perspective._ Lanham, MD: The Scarecrow Press.
*   Shneiderman, B. (2008). Science 2.0\. _Science_, **319**(5868), 1349-1350.
*   Sonnenwald, D.H., Wildemuth, B.M. & Harmon, G.L. (2001). A research method to investigate information seeking using the concept of information horizons: an example from a study of lower socio-economic students' information seeking behaviour. _The New Review of Information Behaviour Research_, **2**, 65-86.
*   Solomon, P. (1997). Discovering information behavior in sense making. I. Time and timing. II. The social. III. The person. _Journal of the American Society for Information Science_, **48**(12), 1097-1108 1109-1126 1127-1138.
*   Steinerová, J. (2008). [Seeking relevance in the academic information use](http://www.webcitation.org/5v1GrKjEw). _Information research_. **13,**(4), paper 380 Retrieved 16 December, 2010 from http://informationr.net/ir/13-4/paper380.html Archived by WebCite® at http://www.webcitation.org/5v1GrKjEw)
*   Steinerová, J., Grešková, M. & Ilavska, J. (2010). _Informacne strategie v elektronickom prostredi._ [Information strategies in the electronic environment]. Bratislava: Vydavatelstvo UK.
*   Steinerová, J., Grešková, M. & Šušol, J. (2007). _Prieskum relevancie informácií: Výsledky rozhovorov s doktorandmi FiFUK._ [Survey of relevance. Research report.] Bratislava: Slovak Centre of Scientific and Technical Information.
*   Steinerová, J., Šušol, J., Makulová, S., Matthaeidesová, M., Verčeková, J. & Rankov, P. (2004). _Správa o empirickom prieskume používateľov knižníc ako súčasť grantovej úlohy VEGA 1/9236/02 Interakcia človeka s informačným prostredím v informačnej spoločnosti._ [Report on empirical survey of library users as part of the research project VEGA 1/9236/02 Interaction of man and the information environment in the information society]. Bratislava: Comenius University, Faculty of Philosophy, Department of Information Science.
*   Steinerová, J. & Šušol, J. (2005). Library users in human information behavior. _Online Information Review_, **29**(2), 39-156.
*   Williamson, K. (2005). Ecological theory of human information behavior. In Karen E. Fisher, Sanda Erdelez and Lynne (E.F.) McKechnie, (Eds.). _Theories of information behavior_. (pp. 128-132). Medford, NJ: Information Today.
*   Wilson, T.D. (2006). [A re-examination of information seeking behaviour in the context of activity theory.](http://www.webcitation.org/5IWYdH5fH)) _Information Research_, **11**(4), paper 260\. Retrieved 30 November, 2010 from: http://InformationR.net/ir/11-4/paper260.html (Archived by WebCite® at http://www.webcitation.org/5IWYdH5fH)