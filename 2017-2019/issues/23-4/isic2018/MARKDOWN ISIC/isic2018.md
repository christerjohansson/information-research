<header>

#### vol 23 no.4, December 2018

##### Proceedings of ISIC: the Information Behaviour Conference, Krakow, Poland, 9-11 October, 2018: Part 1.

</header>

T.D. Wilson, [The diffusion of information behaviour research across disciplines](isic1801.html)  

Ina Fourie, Tumelo Maungwa and Theresa Dirndorfer Anderson, [Subject domain expertise of ISIC2018 reviewer community: a scoping review](isic1802.html)  

Tim Gorichanaz, [Perspective in information behaviour research](isic1803.html)  

Heidi Julien, Lynne McKechnie, Sarah Polkinghorne and Roger Chabot, [The "user turn" in practice: information behaviour researchers’ constructions of information users](isic1804.html)  

Elena Macevičiūtė and Zinaida Manžuch, [Conceptualising the role of digital reading in social and digital inclusion](isic1805.html)  

Ola Pilerot, [The practice of public library-work for newly arrived immigrants](isic1806.html)  

Amy VanScoy, Deborah Hicks and Mary Cavanagh, [What motivates twitter users to engage with libraries?](isic1807.html)  

Fiona Brown and Kirsty Williamson, [The development of legal capability through information use: empirical findings, along with methodological and practical challenges in a mixed methods study](isic1808.html)  

Trine Schreiber, [Information stabilisation and destabilisation as potential usable concepts in practice theoretical approaches](isic1809.html)  

Hue Thi Pham and Kirsty Williamson, [A two-way street: collaboration and information sharing in academia. A theoretically-based, comparative Australian/Vietnamese study](isic1810.html)  

Marek Deja and Maria Prochnicka, [Metadata as a normalising mechanism for information-transfer behaviour in higher education institutions: the information culture perspective](isic1811.html)  

Jela Steinerova, [Perceptions of the information environment by researchers: a qualitative study](isic1812.html)  

Tumelo Maungwa and Ina Fourie, [Exploring and understanding the causes of competitive intelligence failures: an information behaviour lens](isic1813.html)  

Anika Meyer, Preben Hansen, and Ina Fourie, [Assessing the potential of third space to design a creative virtual academic space based on findings from information behaviour](isic1814.html)  

Naailah Parbhoo-Ebrahim and Ina Fourie, [Which lens for a study of information retrieval systems for cold case investigations - activity theory, systems or ecological approach?](isic1815.html)  

Waseem Afzal, [Weaving an affective framework for information behaviour research: a consideration of ‘trilogy of mind’ and ‘flow’](isic1816.html)  

Farhan Ahmad and Gunilla Widén, [Information literacy at workplace: the organizational leadership perspective](isic1817.html)



> Thanks to Marek Deja and colleagues, of the Jagiellonian University, Krakow, for organizing the conversion of the papers to html. The papers were double-blind, peer-reviewed for the Conference but have not been through the journal's copy-editing and final proof-reading and, in general, may not fully conform to the journal's style requirements and standards. The remaining papers will be published with Volume 24, No. 1, March, 2019



