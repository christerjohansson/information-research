#### vol. 14 no. 2, June, 2009

* * *

# Coordenadas paradigmáticas, históricas y epistemológicas de la Ciencia de la Información: una sistematización

#### [Rosa Lidia Vega-Almeida](mailto:vega.rosa@infomed.sld.cu)  
**Unidad de Análisis y Tendencias en Salud,  
Ministerio de Salud Péblica,  
Ciudad de La Habana,  
Cuba.**

#### [J. Carlos Fernández-Molina](mailto:jcfernan@ugr.es)  
**Facultad de Comunicación y Documentación,  
Universidad de Granada,  
España.**

#### [Radamés Linares](mailto:rlinares@infomed.sld.cu)  
**Facultad de Comunicación.  
Departamento de Bibliotecología y Ciencia de la Información,  
Universidad de la Habana,  
Cuba.**

#### Resumen

> **Introducción.** Este trabajo pretende analizar y establecer las coordenadas históricas y epistemológicas de la Ciencia de la Información, que, en la bésqueda de una solución definitiva a los problemas que la singularizan, ha evolucionado desde la explícita validación de la objetividad del conocimiento, en una primera etapa, a la aceptación del sujeto como ente social que construye el conocimiento en estrecha relación con su contexto y a través de procesos de reflexión e interpretación.  
> **Metodología y Análisis.** Se utilizó como punto de partida una división cronológica en tres etapas, dominadas sucesivamente por los paradigmas físico, cognitivo y social, y cada una de ellas fue analizada a través de una serie de variables cualitativas. El análisis se llevó a cabo mediante una revisión bibliográfica.  
> **Resultados.** El acercamiento desde perspectivas históricas y epistemológicas permitió precisar las sustanciales diferencias que indican una reconstrucción cíclica de los pilares teóricos-conceptuales disciplinarios. Estas diferencias permitieron identificar aquellos aspectos distintivos centrados en la relación sujeto-objeto-contexto, y vinculados con el paradigma social y su emergencia.  
> **Conclusiones.** Si el paradigma físico es el principal responsable de la configuración de la Ciencia de la Información como disciplina, al establecer los cimientos teórico-metodológicos fundacionales, el paradigma cognitivo, al centrarse en el sujeto como ente individual, es reflejo de un cambio social e intelectual más radical. Por su parte, el paradigma social trasciende el estrecho marco utilitario y metodológico que sustentó el paradigma cognitivo, al hacer énfasis en la historicidad de todos los fenómenos sociales, y en el cuestionamiento persistente de todos los elementos subjetivos presentes en los modelos teóricos.

#### **[Abstract in English](#abs)**

## Introducción

La utilización de propuestas teórico-metodológicas extra e intradisciplinarias para el estudio de las transformaciones que se manifiestan en un área del conocimiento, constituye un aspecto esencial para una mejor interpretación de sus peculiaridades, así como de su comportamiento en relación con su contexto. Particularmente, la Ciencia de la Información no ha estado ajena a este fenómeno, por cuanto su posible cientificidad ha sido objeto de fuertes polémicas desde su nacimiento, y entre los objetivos cardinales de su comunidad ha estado siempre el desarrollo de sólidos fundamentos teóricos que permitan la reafirmación de su identidad como disciplina científica.

La evolución de la Ciencia de la Información puede ser analizada a través de dos propuestas que persiguen la vinculación recíproca de los fenómenos intra y extradisciplinarios: la teoría sobre la estructura de las revoluciones científicas de Thomas Kuhn ([1971](#kuh71)), presente regularmente en la base intelectual de la disciplina desde su etapa fundacional, y el Análisis de Dominio de Hjørland y Albrechtsen ([1995](#hjo95)), de gran influencia en la éltima década ([Åström 2007](#ast07); [Budd 1995](#bud95); [Pettigrew & McKechnie 2001](#pet01); [White & McCain 1998](#whi98)). Ambas propuestas teórico-metodológicas se caracterizan por incluir las nociones de paradigma y comunidad (científica o discursiva) en su aparato conceptual, además de advertir la pertinencia de combinar los enfoques histórico y epistemológico para el estudio del proceso evolutivo en una disciplina concreta. De igual forma, las dos parten de la atención particular a las contradicciones internas de índole intelectual y social que, como reflejo de la realidad histórica, condicionan el progreso de la disciplina y la emergencia de paradigmas que permiten delimitar sus diferentes etapas cronológicas

Este artículo pretende analizar y establecer las coordenadas histórica y epistemológica de la Ciencia de la Información, la cual, en la bésqueda de una solución definitiva a los problemas que la singularizan, ha evolucionado desde la explícita validación de la objetividad del conocimiento en una primera etapa, a la aceptación del sujeto como ente social que construye el conocimiento en estrecha relación con su contexto y a través de procesos de reflexión e interpretación. Por consiguiente, la sistematización de criterios en torno a la evolución paradigmática de la Ciencia de la Información, desde una visión integradora, se convierte en imperativo para contribuir al conocimiento de la base intelectual de la disciplina, al permitir a los investigadores y profesionales una comprensión más profunda de su propia identidad, y de las consecuencias relevantes de los presupuestos epistemológicos tanto en la praxis profesional como en la investigación científica.

## Metodología

La interpretación, contextualización y mutua complementación de los enfoques histórico y epistemológico se basó en el análisis de un conjunto de fuentes de información, tales como libros, artículos científicos, ponencias y tesis doctorales especializadas en Ciencia de la Información, con una amplia cobertura geográfica y temporal, para el estudio en profundidad de la comunidad disciplinaria. La asunción de la existencia de tres paradigmas profundos en la Ciencia de la Información permite delimitar cognoscitiva, social y cronológicamente el desarrollo disciplinario, por lo que es posible establecer igual némero de etapas en su decurso histórico ([Capurro 2003](#cap03); [Ørom 2000](#oro00)).

Por tanto, como punto de partida se asumió la siguiente división cronológica: una primera etapa que se corresponde con el dominio del paradigma físico y se enmarca entre 1945-197?, una segunda etapa que se caracteriza por la hegemonía del paradigma cognitivo, entre 1980-199? y una tercera etapa que comienza a configurarse bajo la influencia de un emergente paradigma social a partir de la segunda mitad de los noventa.

El análisis de cada una de estas etapas desde las coordenadas histórica y epistemológica se apoyó en las siguientes variables:

*   Factores sociales e intelectuales
*   Definición de la disciplina
*   Macroespacio paradigmático
*   Clasificación de la ciencia
*   Basamento filosófico
*   Concepto de Información
*   Basamento teórico-empírico
*   Enfoque
*   Premisas

A través de estas variables, así como de la precisión del conjunto de condicionantes políticos, económicos, tecnológicos, científicos y profesionales en las tres etapas estudiadas, se abordó el propio proceso y desarrollo disciplinario. Este desarrollo, a su vez, está dominado en cada etapa por un paradigma más profundo (Modernidad, Posmodernidad) que genera, organiza e impone su discurso. Por otro lado, se tuvieron en cuenta las concepciones filosóficas que inciden en la formación de conceptos y teorías. Estas concepciones constituyen sistemas fuertemente interrelacionados de proposiciones y conceptos abstractos que describen, predicen o explican grandes categorías de fenómenos. De igual forma, se valoró la orientación hacia determinado concepto (objeto-sujeto), así como el cémulo de asunciones consecuentes con el discurso imperante en un espacio concreto. El conjunto de rasgos que tipifica tanto a la disciplina como a su objeto de estudio, y su inscripción dentro del esquema de las ciencias, se constató a través de las mutaciones de sus correspondientes definiciones.

## Génesis y evolución disciplinaria

La identificación de una etapa regida por un paradigma social en la Ciencia de la Información durante los éltimos años, conduce inevitablemente hacia el estudio de su proceso fundacional, y a la concatenación de factores influyentes de índole social e intelectual, pues, tal y como plantea Capurro ([2003](#cap03)), lo que aparentemente surgió al final ya estaba presente en sus inicios, no como paradigma de la nueva disciplina, pero sí de sus predecesoras, y en particular de la Biblioteconomía y la Documentación. Esta idea resulta sumamente importante, no sólo para comprender la disciplina desde su dimensión paradigmática, sino también para reconocer y aceptar la independencia disciplinaria de la Ciencia de la Información con respecto al resto de las disciplinas informativas

El proceso constructivo de un espacio diferenciado del conjunto de la ciencia y de su propia identidad, estuvo dominado en su etapa fundacional por un paradigma físico. De esta manera, el paradigma físico como iniciador en la Ciencia de la Información define y establece los límites de la comunidad que lo ha creado, y controla a su vez los discursos compartidos en un ciclo concreto de desarrollo -coincidente en tiempo con el paradigma social de las otras disciplinas informativas-, que es reemplazado décadas después por un paradigma cognitivo que niega sus presupuestos y reconstruye los cimientos teórico-conceptuales de la nueva disciplina.

El planteamiento de Capurro, visto desde la perspectiva kuhniana, conlleva entonces a la hipótesis de que fue precisamente una situación revolucionaria, determinada por factores sociales e intelectuales, la que afectó a la Biblioteconomía y la Documentación -marcada por profundos y frecuentes debates sobre conceptos, métodos, instrumentos, problemas y soluciones para enfrentar la realidad informativa, lo cual no se reflejó en un aumento en profundidad de las disciplinas informativas existentes, sino en un crecimiento en anchura- y propició el nacimiento de una nueva disciplina científica informativa en la segunda mitad del siglo XX: la Ciencia de la Información.

Esta acotación es vital para establecer el marco temporal y espacial en el que surge la Ciencia de la Información, ante el debate en torno a su condición disciplinaria, en el cual se defienden tres posiciones: su independencia como disciplina en relación con la Biblioteconomía, con la que sostiene fuertes vínculos interdisciplinarios ([Saracevic 1999](#sar99)); su dependencia, al ser considerada un continuum, expansión y metamorfosis de la Documentación ([Harmon 1971](#har71)) y de la Biblioteconomía; y su superioridad con respecto a la Biblioteconomía, que es aceptada como una actividad investigadora o de desarrollo dentro de la Ciencia de la Información ([Ingwersen 1992a](#ina92)). En este trabajo se asume la hipótesis de que la Ciencia de la Información surge como resultado de una situación revolucionaria, de manera que se comparte la posición de Saracevic y se coincide con él en situar la génesis disciplinaria en la segunda mitad del siglo XX, y concretamente en los Estados Unidos.

## El paradigma físico: 1945-197?

Aunque la Ciencia de la Información comienza a desarrollarse inmediatamente después de la Segunda Guerra Mundial, su proceso de institucionalización como disciplina no se produce hasta principios de los sesenta, en concreto hasta la celebración de dos conferencias en el Georgia Institute of Technology ([1962](#geo62)), donde se rechazó de forma explícita el uso del término _Documentación_ y se definió por primera vez la Ciencia de la Información como una 'ciencia que investiga las propiedades y comportamiento de la información, las fuerzas que rigen su flujo y los medios de procesarla para su óptima accesibilidad y aprovechamiento' ([Borko 1968](#bor68)), y se estableció su doble carácter: 'un componente de ciencia pura que investiga su objeto sin considerar sus aplicaciones", y un "componente de ciencia aplicada que desarrolla productos y servicios' ([Taylor 1966](#tay66)).

Desde un primer momento, la Ciencia de la Información ha tenido tres características básicas ([Saracevic 1999](#sar99)): a) interdisciplinariedad, b) conexión con las tecnologías, c) participación activa en el desarrollo de la sociedad de la información. Su carácter interdisciplinar ha sido reconocido por sus fundadores y está presente en cada una de sus etapas de desarrollo. Por su parte, su indisoluble unión con las tecnologías ha contribuido al cambio de perspectiva en el acceso, transferencia y recepción de la información, y se expresa además en su aparato-conceptual de manera directa e indirecta ([Hjørland 2000](#hjo00)), con un marcado carácter paradigmático ([Vega-Almeida 2007](#veg07)). Este imperativo tecnológico no sólo la impulsa, sino que también limita su evolución dado que la hace depender del desarrollo de un némero importante de disciplinas y de la sociedad de la información como un todo. Finalmente, al abordar el problema informativo y sus diversas manifestaciones e intentos de solución, la Ciencia de la Información ha encontrado un nicho que se amplía y engrandece en el contexto actual, incrementándose su importancia para la sociedad.

Estas características fueron condicionadas por un conjunto de factores sociales e intelectuales relacionados directamente con los profundos cambios políticos, económicos, científicos e informativos que manifiestan la conexión cíclica y estratégica entre investigación científica, información y desarrollo ([Farkas-Conn 1990](#far90); [Linares 2003](#lin03); [Saracevic 1999](#sar99)), y que acontecieron desde la segunda mitad de la década de 1940\. De esta manera, en el contexto que trasciende el marco informativo es posible identificar los siguientes factores políticos y económicos:

*   La conversión de Estados Unidos en el poder económico dominante mundial, con su infraestructura económica y su superestructura académica intactas. Estados Unidos alcanzó durante este período un extraordinario índice de crecimiento económico, logrando el ritmo más rápido de su historia. El país se benefició de su alejamiento del escenario bélico, de su condición de principal arsenal de sus aliados y de la capacidad de su economía para organizar la expansión de la producción más eficazmente que ninguna otra nación ([Hobsbawm 1998](#hob98)). De igual forma, después de la Segunda Guerra Mundial, Estados Unidos se convirtió en el mayor productor de literatura científica, al generar de 1948 a 1950 más del cincuenta por ciento de las publicaciones científicas del orbe ([Hayes 1998](#hay98)). La contienda bélica devastó y desplazó a Europa de su posición protagónica internacional, y puso de manifiesto la relación bipolar Estados Unidos-Unión Soviética, ambas superpotencias por su capacidad económica, territorial, demográfica y militar ([Hobsbawm 1998](#hob98); [Linares 2003](#lin03)).
*   La promoción y financiamiento de la investigación fundamental y militar, y la determinación de prioridades; al considerarse que las aplicaciones resultantes de la ciencia pura contribuirían en el futuro al desarrollo social. Este principio es consecuente con la idea de progreso, que tipifica a la ciencia moderna en su estrecho vínculo con la práctica de la producción material ([Iovchuk _et al._ 1979](#iov79)). A su vez, la impregna de un objeto social y misión benéficos: garantizar la seguridad y la prosperidad de las naciones ([Waast & Boukhari 1999](#waa99)).

En el contexto circunscrito a la realidad informativa se distinguen como factores:

*   El problema de la explosión de información y la consecuente crisis provocada por la incapacidad para facilitar su acceso y recuperación, enunciado por Vannevar Bush ([1945](#bus45)), principal responsable de la política científica durante la Segunda Guerra Mundial en los Estados Unidos ([Bowles 1998](#bow98)).
*   El desarrollo de las tecnologías de la información y la comunicación, y su conversión en una necesidad e interés prioritarios, sustentado en la interacción complementaria del gobierno, la industria y las universidades. De este modo, fueron los servicios militares las fuentes de financiación más prominentes en la década de 1950 ([Hughes 1998](#hug98)), y fundamentalmente después del lanzamiento del Sputnik por los soviéticos en 1957\. Este hecho fue considerado por los norteamericanos como un acontecimiento sísmico que vino a alterar la estabilidad militar, industrial y científica del país, y que determinó directamente la conformación de la disciplina, al encargársele a la National Science Foundation la elaboración de un programa para la creación y perfeccionamiento de métodos, incluyendo sistemas mecanizados, para hacer disponible la información científica ([Shera & Cleveland 1977](#she77)).
*   El reconocimiento gubernamental de la importancia estratégica de la ciencia y la tecnología y, por consiguiente, de la transferencia de información como parte inseparable de la investigación y el desarrollo. Esto propició la convocatoria de científicos e ingenieros de todo tipo de instituciones, péiblicas y privadas, dedicadas a la investigación, que debían asumir la responsabilidad de la diseminación e intercambio de información científica y tecnológica ([Saracevic 1999](#sar99)).

En el plano profesional y científico, los frecuentes debates que preceden al radical cambio de paradigma y a la constitución disciplinaria se vislumbran también a partir de 1945, manifestándose en:

*   La crítica a la Biblioteconomía como profesión informativa que no es capaz de responder a la nueva y crítica situación -en especial a las necesidades de los científicos-, dadas sus limitaciones para hacer frente a al crecimiento e incremento de la complejidad de los registros del conocimiento. Los ataques provienen de figuras importantes de la política como Vannevar Bush, y en especial de los considerados como pioneros de la Ciencia de la Información, como Calvin Mooers ([1951](#moo51)) o Mortimer Taube ([1951](#tau51)).
*   El conflicto manifiesto ante la emergencia de un grupo rival que se denomina a sí mismo _científicos de la información_, término con el que pretenden evitar el uso del término _bibliotecario_. Este conflicto evidencia la ya documentada existencia en la sociedad de dos grupos polares: científicos y humanistas, que expresan la división de los modos de conocer y de aprehender la realidad ([Bowles 1998](#bow98); [Wallerstein 1999](#wal99)), y que conforman las 'dos culturas separadas por un golfo de mutuas incomprensiones, manifiestas en hostilidad y disgusto', presentadas por el novelista británico C.P. Snow ([1961](#sno61)). Tal situación, que puede calificarse no sólo como cisma intelectual sino también como cisma profesional, se muestra en el deseo de Bush de una reforma fundamental de la biblioteca, conforme a los conceptos de los nuevos científicos e ingenieros como solución inmediata al problema de la información.
*   La bésqueda de la solución a estos problemas a través de disciplinas académicas, en particular las de carácter ingenieril ([Bowles 1998](#bow98)), se puede observar en la constitución de una comunidad científica y profesional integrada por matemáticos, físicos, químicos, ingenieros, y una reducida participación de bibliotecarios dedicados a la ciencia y la tecnología, y científicos sociales ([Kochen 1983](#koc83); [Linares 2003](#lin03)). Estos especialistas determinan con sus perfiles profesionales la propia construcción interdisciplinaria del basamento teórico de la Ciencia de la Información, soportado en la validez de la objetividad del conocimiento (racional, determinista, sistemático, exacto, cuantificable y verificable), la especialización del saber, la asunción del modelo explicativo, y la irrelevancia del sujeto cognoscente.

Consecuentemente, no sólo surge sino que se consolida un paradigma físico, reflejo de su contexto histórico y congruente con las necesidades sociales, a la vez que regula la praxis comunitaria como se aprecia en la tabla 1.

<table><caption>

**Tabla 1\. Paradigma físico en la Ciencia de la Información (1945-197?)**</caption>

<tbody>

<tr>

<th>Paradigma Físico</th>

<th>Características</th>

</tr>

<tr>

<td>Macroespacio paradigmático</td>

<td>

Modernidad (Siglo XVI-finales del Siglo XX), Sociedad Industrial. 1\. El progreso tecnológico aporta el progreso social. 2\. El objetivo de la ciencia moderna se centra en el poder y la razón del ser humano sobre la naturaleza y los procesos sociales ([Blanco 1998](#bla98); [Iovchuk _et al._ 1979](#iov79)). 2.1\. Se constituye en dos premisas: el modelo newtoniano y el dualismo cartesiano. 2.2\. Se manifiesta en el concepto de orden, ley natural, matematización, la distinción fundamental entre la naturaleza-materia-mundo físico, y los humanos-mente-mundo social/espiritual, y la relevancia del objeto sobre el sujeto ([Wallerstein, 1999](#wal99))</td>

</tr>

<tr>

<td>Clasificación de la Ciencia</td>

<td>

Ciencia empírica ([Zunde & Gehl, 1977](#zun77)): focalizó la investigación en la naturaleza de la información con el objetivo de descubrir las leyes empíricas que gobiernan el fenómeno de la información, lo que incluía además los estudios del crecimiento de la información y obsolescencia, la difusión y propagación de la información y los efectos de la estructura de los textos sobre el contenido de la información, y el establecimiento de conceptos y teorías. Ciencia nomotética ([Ørom 2000](#oro00)).</td>

</tr>

<tr>

<td>Basamento filosófico</td>

<td>

Empirismo, Racionalismo ([Hjørland 1998](#hjo98), [2002](#hjo02), [2005](#hjo05); [Hjørland & Albrechtsen 1995](#hjo95)), Positivismo ([Budd 1995](#bud95); [Dick 1999](#dic99); [Fernández-Molina & Moya-Anegón 2002](#fer02)). Las tres corrientes filosóficas sostienen que la ciencia se construye de elementos de absoluta verdad derivados de lo sensorial (Empirismo y Positivismo) y del razonamiento y la teorización a priori (Racionalismo).</td>

</tr>

<tr>

<td>Concepto de Información</td>

<td>

La información es considerada en términos de señales o mensajes, expresada en algoritmos y probabilidades ([Saracevic 1999](#sar99)). Algo externo, objetivo, tangible y mensurable ([Fernández-Molina 1994](#fer94)).</td>

</tr>

<tr>

<td>Basamento teórico-empírico</td>

<td>

- Los experimentos de Cranfield, desarrollados por el Cranfield Institute of Technology en 1957 para medir los resultados de un sistema de recuperación de información computarizado, marcan el inicio del paradigma en la disciplina y en la subdisciplina Recuperación de Información ([Capurro 2003](#cap03); [Ellis 1992](#ell92); [Ørom 2000](#oro00)).
- Teoría Matemática de la Comunicación de Shannon y Weaver, y la cibernética de Norbert Wiener ([Capurro 2003](#cap03)).
- Refinamiento de técnicas de recuperación de información y desarrollo de métodos de representación de textos ([Ørom 2000](#oro00)).
- Métodos bibliométricos ([Schneider & Borlund 2004](#sch04)). El desarrollo del basamento teórico-empírico es apreciable en las dos subdisciplinas de la Ciencia de la Información: la recuperación de información y las disciplinas métricas.</td>

</tr>

<tr>

<td>Enfoque</td>

<td>

Sistema y tecno-centrista ([Capurro 2003](#cap03); [Fernández-Molina & Moya-Anegón 2002](#fer02); [Ørom 2000](#oro00); [White & McCain 1998](#whi98)).</td>

</tr>

<tr>

<td>Premisas</td>

<td>

- La conceptualización de la información se basa en modelos matemáticos.
- Los sistemas de recuperación de información se sustentan en la equiparación entre las representaciones de los textos del sistema y la de las demandas de los usuarios.
- Las necesidades de información se consideran estables e invariables.
- El proceso de bésqueda de información es determinista, no dinámico e iterativo, sin intervención de elementos psicológicos y físicos ([Fernández-Molina & Moya-Anegón 2002](#fer02)).
- La relevancia es objetiva y puede ser medida ([Ørom 2000](#oro00)).
- Metodología cuantitativa.</td>

</tr>

</tbody>

</table>

## El paradigma cognitivo: 1980-199?

Después de tres décadas de relativa estabilidad, la Ciencia de la Información percibió su limitada capacidad para responder a las nuevas necesidades sociales generadas de la situación crítica, aspecto observable claramente a partir de la década de 1970\. La crisis disciplinaria constituyó el reflejo de la más significativa y universal transformación ocurrida en cuatro siglos, el nacimiento de un modelo que se define a partir de la asunción de una actitud antagónica y de rechazo a los principios emanados de la Modernidad y la Sociedad Industrial.

En esta etapa, se destaca como el más importante acontecimiento político de acentuada repercusión social y económica, la desintegración del sistema socialista europeo y la desaparición de la Unión Soviética. Esta situación determinó el fin de la Guerra Fría y la ruptura en la concepción y materialización de la economía industrial moderna en función del capitalismo y el socialismo, alternativas mutuamente excluyentes y representadas por Estados Unidos y la Unión Soviética ([Hayes 1998](#hay98); [Hobsbawm 1998](#hob98)). Se asistió entonces al proceso destructivo de los mecanismos sociales reguladores de la conducta individual y colectiva, en un presente permanente sin vínculo orgánico con el pasado, que es asumido desde una perspectiva eminentemente individualista en una primera etapa, manifiesta también en la esfera intelectual. Por consiguiente, el proceso evolutivo y renovador de la Ciencia de la Información a partir de la década de 1970 está condic ionado directamente en la dimensión social por:

*   La transición de una Sociedad Industrial a una Sociedad de la Información en los Estados Unidos y otros países industrializados ([Hayes 1998](#hay98)). Esta nueva sociedad se sustenta en una economía de la información o el conocimiento, lo que supone que la riqueza creada se mide cada vez menos por el trabajo en su forma inmediata, mensurable y cuantificable, dependiendo cada vez más del nivel general de la ciencia y del desarrollo tecnológico ([UNESCO 2005](#une05)).
*   El continuo desarrollo revolucionario de las tecnologías de la información y la comunicación, soporte infraestructural de la Sociedad de la Información, materializado en el surgimiento de la microcomputadora en los años 80, de rápida y extensivo empleo, y el interés en la década de 1990 en la creación y sostenimiento de una infraestructura de información nacional e internacional basada en el progreso acelerado de Internet, la telefonía digital y móvil, y el crecimiento vertiginoso de la World Wide Web ([Hayes 1998](#hay98)).

En la dimensión intelectual, la emergencia del paradigma cognitivo es consecuente con la nueva concepción de una ciencia, resultante del agotamiento del modelo de racionalidad moderno y la negación de su propuesta por parte de las Ciencias Exactas y Naturales. Éstas reconstruyen los pilares teórico-conceptuales y establecen nuevos principios: a) la relatividad, al demostrar que la simultaneidad de los acontecimientos distantes no puede ser verificada empíricamente sino sólo definida; b) el cuestionamiento de la objetividad de la ciencia y el rigor de las mediciones con la mecánica cuántica; c) la impugnación del vehículo formal en que se expresa la medición, al demostrar que el rigor de la Matemática se basa en un criterio de selectividad; y d) la introducción de la incertidumbre y el principio del orden a través de las fluctuaciones ([Santos 1988](#san88)), de influencia reformadora en las Ciencias Sociales.

En tal sentido, bajo la hegemonía de un paradigma cognitivo, la Ciencia de la Información se concentró en el sujeto individual, activo transformador de la información en conocimiento, y en la subjetividad. De esta forma, como advierte Barreto ([2001](#bar01)), su esencia disciplinaria radica en la generación de conocimiento en el individuo y en su espacio de convivencia, y su propósito es conocer y hacer que ocurra el sutil fenómeno de percepción de la información por la conciencia. Este fenómeno se inserta en la soledad básica de cada individuo, en la esfera más privada de la individualidad, trasladable a la esfera péblica mediante la información que es dirigida por el propio individuo al flujo de transferencia, y que al llegar al péblico destinado debe provocar la modificación y desarrollo individual. Por tanto, la disciplina, considerada como una Ciencia Social, se re-edifica entonces sobre la base de una epistemología individualista ([Dick 19 95](#dic95)) que le aporta la estabilidad y coherencia interna para la solución efectiva de los problemas generados en la práctica social, como se sintetiza en la tabla 2.

<table><caption>

**Tabla 2\. Paradigma cognitivo en la Ciencia de la Información (1989-199?)**</caption>

<tbody>

<tr>

<th>Paradigma Cognitivo</th>

<th>Características</th>

</tr>

<tr>

<td>Macroespacio paradigmático</td>

<td>

Post-Modernidad (postrimerías del Siglo XX). Sociedad de la Información (información como recurso clave para el desarrollo). Se acepta una racionalidad de índole intersubjetiva; se reconoce del rol activo del sujeto cognoscente; se cuestiona la noción de objetividad de la ciencia y el rigor de las mediciones; se opone a la fragmentación del conocimiento; se evidencia en los conceptos de sistema, estructura, modelo o proceso, y la investigación cualitativa.</td>

</tr>

<tr>

<td>Clasificación de la Ciencia</td>

<td>

Ciencia Social (epistemología individualista): focalizada en el sujeto (usuario) y sus necesidades, y la intermediación entre productores y usuarios, con énfasis en la comprensión psicológica ([Hjørland & Albrechtsen 1995](#hjo95)).</td>

</tr>

<tr>

<td>Basamento filosófico</td>

<td>

Frohmann ([1992](#fro92)) refiere el Cognitivismo y el Mentalismo. Cognitivismo que parte de la analogía de que el cerebro es una computadora digital y la mente, un programa computacional. Ingwersen ([1996](#ing96)) sostiene que la visión cognitivista en CI se enfatiza en el aspecto humano de la investigación cognitiva. Se focaliza en el usuario individual para estimular, pero no para duplicar el procesamiento cognitivo humano. El Mentalismo en la CI implica que el sistema de información debe reflejar la percepción subjetiva del conocimiento y la información del usuario ([Hjørland & Albrechtsen 1995](#hjo95)). Hjørland ([1992](#hjo92)) circunscribe el paradigma al idealismo subjetivista, caracterizado por la concepción de que la conciencia es primaria y que la realidad del mundo material es, en consecuencia, un constructo mental. Esta posición implica que la realidad no está determinada externamente sino que es generada internamente por los procesos mentales de cada individuo ([Jacob & Shaw 1998](#jac98)). Talja lo enmarca en el Contructivismo cognitivo, que al igual que sus predecesores en la Psicología y la Ciencia Cognitiva, en la CI parte del supuesto de que la mente individual genera conocimiento a través de la creación de estructuras y modelos mentales, los cuales representan el mundo y median la información. Se asume que la mente de un individuo es el elemento más importante en la creación de conocimiento ([Talja _et al._ 2005](#tal05)).</td>

</tr>

<tr>

<td>Concepto de Información</td>

<td>

La información es vista como un concepto directamente involucrado con la comprensión y el procesamiento cognitivo. Es el resultado de la interacción de dos estructuras cognitivas, una mente y un texto. La información es aquello que afecta o cambia el estado de la mente ([Saracevic 1999](#sar99)); algo subjetivo ([Fernández-Molina 1994](#fer94)). El significado de un mensaje es producido por el receptor a través de la mediación de sus estructuras cognitivas ([Ørom 2000](#oro00)).</td>

</tr>

<tr>

<td>Basamento teórico-empírico</td>

<td>

- El problema de la información relacionada con el conocimiento considerado por Brookes y Belkin ([Zunde & Gehl 1977](#zun77)).
- La ecuación fundamental de la Ciencia de la Información o ecuación cognitiva de Brookes (paradigma cognitivo mentalista) ([Capurro 2003](#cap03)).
- La teoría del Estado Anómalo del Conocimiento (ASK) de Belkin.
- La teoría del Sense-Making de Dervin.
- El Modelo de Bésqueda de Información de Kuhlthau.
- El concepto de información de Ingwersen a partir de las propuestas de Brookes y Belkin.
- El desarrollo teórico-empírico es observable en la subdisciplina Recuperación de Información.</td>

</tr>

<tr>

<td>Enfoque</td>

<td>

Usuario centrista (individualista) ([Capurro 2003](#cap03); [Fernández-Molina & Moya-Anegón 2002](#fer02); [Ørom 2000](#oro00); [White & McCain 1998](#whi98)).</td>

</tr>

<tr>

<td>Premisas</td>

<td>

- Se parte del presupuesto planteado por De Mey ([1977](#dem77)) de que cualquier procesamiento de información, sea perceptivo o simbólico, es mediado por un sistema de categorías o conceptos que constituyen un modelo de su mundo para el dispositivo procesador de información.
- Se concentra en los aspectos cualitativos de la interacción durante el proceso de recuperación de la información.
- Se basa en un modelo de conocimiento relativista: el conocimiento es relativo en tanto es alterado por procesos cognitivos ([Ingwersen 1992b](#inb92)).
- Los modelos cognitivos de interacción en el proceso de recuperación son estructurales, en el sentido que median las relaciones entre elementos involucrados en la creación de significado y en la transformación de las estructuras de conocimiento ([Ingwersen 1992b](#inb92)).
- La relevancia se define a nivel individual, e incluye el comportamiento humano en la recupe ración de información en general, y en relación con la recuperación de información y los sistemas de recuperación como: a) Relevancia pragmática a nivel individual: el juicio para la relevancia es determinado por el espacio de problemas del usuario individual y el estado de conocimiento ([Ingwersen 1992b](#inb92)). b) Relevancia intermediaria: el juicio de relevancia se basa en la evaluación subjetiva de un intermediario en torno a la relación entre una solicitud y la representación del documento ([Ørom 2000](#oro00)).
- El conocimiento de los usuarios y sus necesidades son estudiadas desde la perspectiva individualista. - Se estudia cómo las personas piensan, y se mimetizan estas regularidades del pensamiento.
- Subjetividad inherente, y metodología caracterizada por un enfoque individualista ([Hjørland & Albrechtsen 1995](#hjo95).
- Se enfoca el contexto disciplinario como una parte de la estructura cognitiva de un i ndividuo ([Hjørland & Albrechtsen 1995](#hjo95)), y se considera un nivel intermedio entre el paradigma cognitivo mentalista y el social ([Capurro 2003](#cap03)).</td>

</tr>

</tbody>

</table>

## El paradigma social: 199?-

La década finisecular y la primera mitad del siglo XXI también se presenta como un período crítico de incertidumbre e inestabilidad, que afecta todas las esferas sociales. Estas características son consecuencia inevitable del orden económico y político mundial, responsable de la falta de equidad en la distribución de los recursos, la globalización económica neoliberal y la tecnológica, los conflictos bélicos, la destrucción acelerada del entorno ecológico, y una cultura intelectual y ética marcada por el individualismo. Ante tales contradicciones, persistentes y agudizadas durante los éltimos años, resulta visible la revitalización de los movimientos sociales en la bésqueda de un proyecto humanista alternativo, y particularmente en la esfera científica, en el reconocimiento del ser humano como sujeto histórico en mutua interrelación con la naturaleza y la sociedad.

Si el rechazo al paradigma dominante de la Modernidad condujo inicialmente a la instauración del sujeto como ente individual, y por consiguiente a la excesiva valoración de la subjetividad, en la práctica, se corrobora la necesaria construcción intersubjetiva, reflexiva e interpretativa de un conocimiento, resultante de la relación dialógica sujeto-objeto-contexto. El debate epistemológico que recorre transversalmente los dos polos opuestos: Ciencias Naturales-Ciencias Sociales, asume las influencias recíprocas, así como la pertinente integración y valoración del contexto socio-cultural, que da lugar a un paradigma emergente que, segén Santos ([1988](#san88)), "no puede ser sólo un paradigma científico, sino también un paradigma social" y que se sustenta en cuatro tesis: a) todo conocimiento científico-natural es científico-social, b) todo conocimiento es local y total, c) todo conocimiento es autoconocimiento, y d) todo el conocimiento científico ha de constituirse en un nuevo sentido comén.

Los nuevos modos de conceptualizar y aprehender la realidad constituyen, en el contexto de la Ciencia de la Información, una manifestación del debilitamiento e ineficacia del paradigma cognitivo para afrontar los problemas presentes. En tal sentido, a finales de la década de 1990, la Ciencia de la Información no sólo se distingue por su indisoluble relación con las tecnologías de la información y la comunicación y la Sociedad de la Información, sino de manera particular, por su fuerte dimensión social y humana, y la gran aceptación en torno a la orientación social de sus fundamentos científicos, lo que ratifica su definitivo posicionamiento dentro de las Ciencias Sociales ([Araéjo 2003](#ara03); [Dick 1999](#dic99); [Saracevic 1999](#sar99)).

De esta manera, como campo de práctica profesional e investigación científica, la disciplina enfoca los problemas de comunicación efectiva de los registros del conocimiento entre humanos en el contexto de las organizaciones sociales, así como las necesidades y usos de la información por los individuos. Por tanto, es clave el problema de las necesidades de información y el uso de la información en su relación con el contexto ([Saracevic 1999](#sar99)). Dichos aspectos, de carácter social, constituyen puntos focales del nuevo enfoque epistemológico ([Ørom 2000](#oro00)). Esta perspectiva es la que permite encontrar los argumentos para desarrollar la Ciencia de la Información en la línea de las Ciencias Sociales. Tales argumentos, reflejo de los radicales cambios que reconstruyen su metodología, tienen como rasgos fundamentales ([Dick 1993](#dic93)):

1.  El rechazo a la creencia de que las ciencias sociales deben imitar a las ciencias naturales.
2.  Los enfoques que plantean que la énica forma de conocimiento es la resultante de la aplicación de los métodos científicos.
3.  Existe la percepción de que las concepciones de la metodología científica y otros términos investigativos clave, poseen un carácter histórico distintivo. Se reconoce que no existe el método científico definitivo e inmutable.
4.  Se reconoce la unidad entre la metodología y la epistemología, debido a que las formas de conocer la realidad están guiadas por supuestos y asunciones. Se reconsidera que el objetivo de la metodología se focaliza en acentuar la comprensión de la práctica.
5.  Una nueva preocupación por el papel del poder y la ideología.

La comunión armónica de estos rasgos, unida a la inclusión del contexto -elemento que diferencia a la Ciencia de la Información actual de la considerada una Ciencia Social con basamento epistemológico individualista-, reconfigura la disciplina sobre los pilares de un paradigma social. Por consiguiente, el contexto se convierte en elemento resonante y es aceptado en el propio seno del paradigma cognitivo de la Ciencia de la Información en los éltimos años. Tal fenómeno evidencia la existencia de una posición intermedia del tránsito hacia un paradigma social ([Capurro 2003](#cap03); [Talja _et al._ 2005](#tal05)), pues aén cuando reconocen el contexto, éste se considera como una parte de la estructura cognitiva de un individuo ([Hjørland & Albrechtsen 1995](#hjo95)), que siempre es vista en un primer plano ([Capurro 2003](#cap03)).

Precisamente en la subdisciplina Recuperación de Información, donde se generó el basamento teórico cognitivo, comenzó a enfatizarse a finales de los noventa la naturaleza social e histórica del objeto de investigación de la disciplina, y se asumió que una variedad de consideraciones contextuales devienen los marcos de referencia del comportamiento informativo de los individuos ([Dervin 1997](#der97); [Johnson 2003](#joh03); [Talja _et al._ 1999](#tal99), [2005](#tal05)). Por tanto, desde méltiples propuestas epistemológicas que reconocen las bases sociales del conocimiento y su historicidad, se consolida un nuevo paradigma en la Ciencia de la Información como se resume en la tabla 3.

<table><caption>

**Tabla 3\. Paradigma social en la Ciencia de la Información (199?- )**</caption>

<tbody>

<tr>

<th>Paradigma Social</th>

<th>Características</th>

</tr>

<tr>

<td>Macroespacio paradigmático</td>

<td>

Post-Modernidad (postrimerías del Siglo XX). Sociedad de la Información (información como recurso clave para el desarrollo). Se acepta una racionalidad de índole intersubjetiva; se reconoce del rol activo del sujeto cognoscente; se cuestiona la noción de objetividad de la ciencia y el rigor de las mediciones; se opone a la fragmentación del conocimiento; se evidencia en los conceptos de sistema, estructura, modelo o proceso, y la investigación cualitativa.</td>

</tr>

<tr>

<td>Clasificación de la Ciencia</td>

<td>

Ciencia Social: reconoce las bases sociales del conocimiento, y se enfoca al estudio de su objeto a partir de la historicidad de los sujetos cognoscentes y los objetos cognoscibles en su relación socialmente determinada, la totalidad de los fenómenos, y la tensión presente en la sociedad ([Araéjo 2003](#ara03)). El contexto constituye una condición necesaria para la comprensión del fenómeno relativo a las necesidades de información, y a la investigación en el área de bésqueda y recuperación de Información ([Dervin 1997](#der97)); lo cual es extrapolable al área de la gestión y a la comunicación científica.</td>

</tr>

<tr>

<td>Basamento filosófico</td>

<td>

Historicismo: enfatiza que la percepción y el pensamiento siempre están bajo la influencia del lenguaje, cultura, pre-comprensión y horizonte; reconoce que el conocimiento está determinado por factores sociales. Se incluyen teorías o escuelas como: hermenéutica, pragmatismo, constructivismo social, semiótica ([Hjørland 1998](#hjo98)). Contextualismo en las Ciencias Sociales, centrado en el estudio del individuo en su entorno, cultura y tiempo histórico ([Fernández-Molina & Moya-Anegón 2002](#fer02)). Se enmarca en el Colectivismo y el Construccionismo con aplicaciones en la Organización y Recuperación de la Información ([Talja _et al._ 2005](#tal05)). Colectivismo basado en el Constructivismo social: orientado a la comprensión profunda de las prácticas de los grupos y dominios científicos y el conocimiento tácito relativo a esas prácticas; sostiene que los procesos informativos pueden ser vistos en relación con sus contextos sociales, organizacionales y profesionales. Construccionismo centrado en los procesos lingüísticos: hace énfasis en el contexto, y desde la perspectiva de la naturaleza dependiente y argumentativa del uso del lenguaje. se centra en el discurso como vehículo a través del cual el sujeto y el mundo son articulados. En la CI asume que la información, los sistemas de información y las necesidades, son entidades que se producen dentro de un discurso (Análisis de Discurso).</td>

</tr>

<tr>

<td>Concepto de Información</td>

<td>

La información es tratada desde una perspectiva amplia que involucra, además de los mensajes (paradigma físico), que son procesados a un nivel cognitivo (paradigma cognitivo), un contexto -situación, tarea, problema-, motivaciones e intencionalidad ([Saracevic 1999](#sar99)).</td>

</tr>

<tr>

<td>Basamento teórico-empírico</td>

<td>

- La crítica de Winograd y Flores a los modelos de la informática, que supone para la CI una nueva visión de usuarios como seres sociales y culturales, así como una visión sociológico-epistemológica de la bésqueda de información ([Capurro 2003](#cap03); [Hjørland 1998](#hjo98)).
- Análisis de Dominio de Hjørland y Albrechtsen ([1995](#hjo95)).
- Hermenéutica de Capurro ([1992](#cap92)).
- Fenomenología hermenéutica de Budd ([1995](#bud95)).
- Cibersemiótica de Brier ([1996](#bri96)).
- Teoría sobre los entornos de los usuarios de información de Taylor ([Talja _et al._ 2005](#tal05)).
- Teoría de los entornos de los usuarios de información y su estructuración ([Talja _et al._ 2005](#tal05)).
- Análisis del Discurso ([Frohmann 1992](#fro92)). El desarrollo teórico-empírico desde este paradigma es transversal a toda la disciplina ([Ørom 2000](#oro00)). Segén Ingwersen ([1992a](#ina92)) se reconoce fundamentalmente dentro de la informetría y la gestión de información.</td>

</tr>

<tr>

<td>Enfoque</td>

<td>

Social (usuario-sistema-contexto) ([Capurro 2003](#cap03); [Fernández-Molina & Moya-Anegón 2002](#fer02); [Ørom 2000](#oro00)).</td>

</tr>

<tr>

<td>Premisas</td>

<td>

- Se estudian, analizan y conceptualizan los procesos informativos y la comunicación del conocimiento a un nivel macro (contexto socio-cultural).
- Se concibe el área temática de la CI desde la sociología de la ciencia, la hermenéutica, la semiótica y el análisis del discurso ([Ørom 2000](#oro00)). Se considera que los métodos estadísticos cuantitativos pueden ser usados sólo en aquellas áreas en las cuales la percepción humana de una situación no es un factor ([Benediktsson 1989](#ben89)).
- La relevancia se define como contextual: a) El juicio para la relevancia está basado en el conocimiento del dominio ([Hjørland & Albrechtsen 1995](#hjo95)). b) Factores contextuales determinan la relevancia, que se considera un acto de interpretación ([Ørom 2000](#oro00)).</td>

</tr>

</tbody>

</table>

## Conclusiones

La revolución científica acaecida en la segunda mitad del siglo XX afectó de manera particular a la realidad informativa y sus diversos dominios. El surgimiento de la Ciencia de la Información fue resultado de la proliferación disciplinaria y la instauración de un paradigma, en concreto el denominado físico. Este paradigma resultó determinante en la creación y consolidación de la integridad y singularidad del nuevo campo del conocimiento, en un período histórico concreto, y definió su independencia y autonomía.

La superación cualitativa que lleva consigo cada paradigma de la Ciencia de la Información lo hace mejor que su predecesor, por su capacidad adaptativa y resolutiva ante las nuevas necesidades sociales. Un paradigma es reflejo de su época, y eso le atribuye una marcada significación histórica en el estudio de la disciplina, más allá de su derogación, pues cada paradigma ha sido portador de la solución para su tiempo y ha convertido a la comunidad científica -durante las diversas etapas de la Ciencia de la Información- en generadora de basamento teórico-conceptual y guía de la investigación, lo que evidencia su condición de unidad productora y validadora de conocimiento.

En este sentido, la importancia del paradigma físico radica en su contribución a la configuración de la disciplina, al establecer los cimientos teórico-metodológicos fundacionales. Por su parte, el paradigma cognitivo destaca por expresar y reflejar un cambio social e intelectual más radical, y centrarse en el sujeto como ente individual. Por éltimo, el paradigma social trasciende el estrecho marco utilitario y metodológico en el que se recluyó el paradigma cognitivo, al poner su énfasis en la historicidad de todos los fenómenos sociales, y en el cuestionamiento persistente en torno a los elementos subjetivos de los modelos teóricos, lo que aumenta la probabilidad de que esos modelos sean relevantes, y contribuye así al desarrollo orgánico de la disciplina.

El acercamiento desde la integración histórica y epistemológica permitió precisar las sustanciales diferencias que indican una reconstrucción cíclica de los pilares teórico-conceptuales disciplinarios, y que facilitan la identificación de aquellos aspectos distintivos centrados en la relación sujeto-objeto-contexto, y vinculados con el paradigma social y su emergencia. De esta manera, de la síntesis histórico-epistemológica, considerada como marco referencial disciplinario, se asumen tres supuestos básicos para la identificación del paradigma social en el período comprendido desde 1995 hasta el presente:

1.  La existencia de un nivel intermedio entre el paradigma cognitivo mentalista y el paradigma social, integrado por las propuestas teóricas que reconocen el contexto o situación como un elemento que condiciona la bésqueda y recuperación de información, constituye una clara expresión del agotamiento del paradigma cognitivo.
2.  El paradigma social se visibiliza en todas las áreas de la disciplina y se reconoce fundamentalmente dentro de la informetría y la gestión de información.
3.  La investigación cuantitativa, bajo la égida del paradigma social, sólo puede ser utilizada en aquellas áreas donde la percepción humana de una situación no es un factor.

## Agradecimientos

A los investigadores cubanos Ricardo Arencibia, Yohannis Martí y Gloria Ponjuán, además de Fredrik Åström (Suecia), Don Fallis (USA) y Félix de Moya (España), por sus correcciones, observaciones y apoyo en el desarrollo de esta investigación.

## Referencias

*   <a id="ast07"></a>Åström, F. (2007). Changes in the LIS research front: time-sliced cocitation analyses of LIS journal articles, 1990-2004\. _Journal of the American Society for Information Science and Technology_, **58**(7), 947-957.
*   <a id="ara03"></a>Araéjo, C.A.A. (2003). A ciência da informação como ciência social. _Ciência da Informação_, **32**(3), 21-27.
*   <a id="bar01"></a>Barreto, A.D.A. (2001). Cambio estructural en el flujo de conocimiento: la comunicación electrónica. _Acimed_, **9**(4), 23-28.
*   <a id="ben89"></a>Benediktsson, D. (1989). Hermeneutics: dimensions toward LIS thinking. _Library and Information Science Research_, **11**(3), 210-234.
*   <a id="bla98"></a>Blanco, J.A. (1998). _Tercer Milenio: una visión alternativa de la posmodernidad._ La Habana: Centro Félix Varela.
*   <a id="bor68"></a>Borko, H. (1968). Information science: what is it? _American Documentation_, **19**(1), 3-5.
*   <a id="bow98"></a>Bowles, M.D. (1998). [The information wars: two cultures and the conflict in information retrieval, 1945-1999](http://www.webcitation.org/5ge8aNM18). In: Mary Ellen Bowden, Trudi Bellardo Hahn & Robert V. Williams (Eds.), _Proceedings of the 1998 Conference on the History and Heritage of Science Information Systems_ (pp. 156-166). Medford, NJ: Information Today, Inc. for the American Society for Information Science and the Chemical Heritage Foundation. Retrieved 26 February, 2008 from http://www.chemheritage.org/explore/ASIS_documents/ASIS98_Bowles.pdf (Archived by WebCite® at http://www.webcitation.org/5ge8aNM18)
*   <a id="bri96"></a>Brier, S. (1996). Cybersemiotics: a new interdisciplinar development applied to the problems of knowledge organisation and document retrieval in information science. _Journal of Documentation_, **52**(3), 296-344.
*   <a id="buc98"></a>Buckland, M.K. (1998). [Overview of the history of science information systems](http://www.webcitation.org/5geEL6lvS). In: Mary Ellen Bowden, Trudi Bellardo Hahn & Robert V. Williams (Eds.), _Proceedings of the 1998 Conference on the History and Heritage of Science Information Systems_ (pp. 3-7). Medford, NJ: Information Today, Inc. for the American Society for Information Science and the Chemical Heritage Foundation. Retrieved 26 February, 2008 from http://www.chemheritage.org/explore/ASIS_documents/ASIS98_Buckland.pdf (Archived by WebCite® at http://www.webcitation.org/5geEL6lvS)
*   <a id="bud95"></a>Budd, J. (1995). An epistemological foundation for library and information science. _The Library Quarterly_, **65**(3), 295-318.
*   <a id="bud01"></a>Budd, J. (2001). _Knowledge and knowing in library and information science: a philosophical framework._ Lanham, MD: Scarecrow.
*   <a id="bus45"></a>Bush, V. (1945). As we may think? _The Atlantic Monthly_, **176**(1), 101-108.
*   <a id="cap92"></a>Capurro, R. (1992). What is information science for? a philosophical reflection. In Vakkari, P. & Cronin, B. (Eds.). _Conceptions of library and information science: historical, empirical and theoretical perspectives._ (pp. 82-96) London: Taylor Graham.
*   <a id="cap03"></a>Capurro, R. (2003). [_Epistemología y Ciencia de la Información_](http://www.webcitation.org/5ge92nXr9). Retrieved 26 February, 2008 from http://www.capurro.de/enancib.htm (Archived by WebCite® at http://www.webcitation.org/5ge92nXr9)
*   <a id="dem77"></a>De Mey, M. (1977). The cognitive viewpoint: its development and its scope. In: De Mey, M. (Ed.). _International Workshop on the Cognitive Viewpoint._ (pp. xvi-xxxii). Ghent: University of Ghent.
*   <a id="der77"></a>Dervin, B. (1997). Given a context by any other name: methodological tools for taming the unruly beast. In P. Vakkari, R. Savolainen & B. Dervin (Eds.). _Information seeking in context: Proceedings of an International Conference on Research in Information Needs, Seeking and use in Different Contexts._ (pp. 13-38). London: Taylor Graham.
*   <a id="dic93"></a>Dick, A.L. (1993). Three paths to inquiry in library and information science: positivist, constructivist and critical theory approaches. _South African Journal of Information Science_, **61**(2), 53-60.
*   <a id="dic95"></a>Dick, A.L. (1995). Library and information science as a social science: neutral and normative conceptions. _The Library Quarterly_, **65**(2), 216-235.
*   <a id="dic99"></a>Dick, A.L. (1999). Epistemological positions and library and information science. _The Library Quarterly_, **69**(3), 305-323.
*   <a id="ell92"></a>Ellis, D. (1992). The physical and cognitive paradigms in information retrieval research. _Journal of Documentation_, **48**(1), 45-64.
*   <a id="far90"></a>Farkas-Conn, I.S. (1990). _From documentation to information science. The beginnings and early development of the American Documentation Institute-American Society for Information Science._ New York, Greenwood Press.
*   <a id="fer94"></a>Fernández-Molina, J.C. (1994). Enfoque objetivo y subjetivo del concepto de información. _Revista Española de Documentación Científica_, **17**(3), 320-331.
*   <a id="fer02"></a>Fernández-Molina, J.C. & Moya-Anegón, F. (2002). Perspectivas epistemológicas "humanas" en la Documentación. _Revista Española de Documentación Científica_, **25**(3), 241-253.
*   <a id="fro92"></a>Frohmann, B. (1992). The power of images: a discourse analysis of the cognitive viewpoint. _Journal of Documentation_, **48**(4), 365-386.
*   <a id="har71"></a>Harmon, G. (1971). On the evolution of information science. _Journal of American Society for Information Science_, **22**(4), 235-241.
*   <a id="hay98"></a>Hayes, R.M. (1998). [History review: the development of information science in the United States](http://www.webcitation.org/5geFs7CGe). In: Mary Ellen Bowden, Trudi Bellardo Hahn & Robert V. Williams (Eds.), _Proceedings of the 1998 Conference on the History and Heritage of Science Information Systems_ (pp. 223-248). Medford, NJ: Information Today, Inc. for the American Society for Information Science and the Chemical Heritage Foundation. Retrieved 26 February, 2008 from http://www.chemheritage.org/explore/ASIS_documents/ASIS98_Hayes.pdf (Archived by WebCite® at http://www.webcitation.org/5geFs7CGe)
*   <a id="hjo92"></a>Hjørland, B. (1992). The concept of "subject" in information science. _Journal of Documentation_, **48**(2), 172-200.
*   <a id="hjo98"></a>Hjørland, B. (1998). Theory and metatheory of information science: a new interpretation. _Journal of Documentation_, **54**(5), 606-621.
*   <a id="hjo00"></a>Hjørland, B. (2000). Documents, memory institutions and information science. _Journal of Documentation_, **56**(1), 27-41.
*   <a id="hjo02"></a>Hjørland, B. (2002). Epistemology and the socio-cognitive perspective in information science. _Journal of the American Society for Information Science and Technology_, **53**(4), 257-270.
*   <a id="hjo05"></a>Hjørland, B. (2005). Empiricism, rationalism and positivism in library and information science. _Journal of Documentation_, **61**(1), 130-155.
*   <a id="hjo95"></a>Hjørland, B. & Albrechtsen, H. (1995). Toward a new horizon in information science: domain analysis._Journal of the American Society for Information Science_, **46**(6), 400-425.
*   <a id="hob98"></a>Hobsbawm, E. (1998). _Historia del siglo XX._ Barcelona: Grijalbo.
*   <a id="hug98"></a>Hughes, T.P. (1998). [_Funding a revolution_](http://www.webcitation.org/5geGf1dKE). In: Mary Ellen Bowden, Trudi Bellardo Hahn & Robert V. Williams (Eds.), _Proceedings of the 1998 Conference on the History and Heritage of Science Information Systems_ (pp. 8-13). Medford, NJ: Information Today, Inc. for the American Society for Information Science and the Chemical Heritage Foundation. Retrieved 26 February, 2008 from http://www.chemheritage.org/explore/ASIS_documents/ASIS98_Hughes.pdf (Archived by WebCite® at http://www.webcitation.org/5geGf1dKE)
*   <a id="ina92"></a>Ingwersen, P. (1992a). Information and information science in context. _Libri_, **42**(2), 99-135.
*   <a id="inb92"></a>Ingwersen, P. (1992b). _Information retrieval interaction._London: Taylor Graham.
*   <a id="ing96"></a>Ingwersen, P. (1996). Cognitive perspectives of information retrieval interaction: elements of a cognitive IR theory. _Journal of Documentation_, **52**(1), 3-50.
*   <a id="iov79"></a>Iovchuk, M.T., Oizerman, T.I. & Shchipanov, I. (1979). _Compendio de historia de la Filosofía. 2ª ed._La Habana: Pueblo y Educación.
*   <a id="jac98"></a>Jacob, E.K. & Shaw, D. (1998). Sociocognitive perspectives on representation. _Annual Review of Information Science and Technology_, **33**, 131-185.
*   <a id="joh03"></a>Johnson, J.D. (2003). On contexts of information seeking. _Information Processing & Management_, **39**(5), 735-760.
*   <a id="koc83"></a>Kochen, M. (1983). Library science and information science: broad or narrow? In F. Machlup & U. Mansfield (Eds.). _The study of information: interdisciplinary messages._(pp. 371-388). New York: John Wiley & Sons.
*   <a id="kuh71"></a>Kuhn, T.S. (1971). _La estructura de las revoluciones científicas._México D.F.: Fondo de Cultura Económica.
*   <a id="kuh91"></a>Kuhlthau, C. (1991). Inside the search process: information seeking from the user's perspective. _Journal of the American Society for Information Science_, **42**(5), 361-371.
*   <a id="lin03"></a>Linares, R. (2003). _La Ciencia de la Información y sus matrices teóricas: contribución a su historia._ Tesis para optar por el título de Doctor en Ciencias de la Información, Universidad de La Habana, Cuba.
*   <a id="moo51"></a>Mooers, C. (1951). Information retrieval viewed as temporal signalling. In: _International Congress of Mathematicians. Cambridge, Mass., 1950\. Proceedings._ (pp. 572-573). Providence, RI: American Mathematical Society.
*   <a id="geo62"></a>National Science Foundation _and_ Georgia Institute of Technology (1962). _Proceedings of the Conferences on Training Science Information Specialists, 12-13 October 1961, 12-13 April 1962._ Atlanta: Georgia Institute of Technology.
*   <a id="oro00"></a>Ørom, A. (2000). Information Science, historical changes and social aspects: a Nordic outlook. _Journal of Documentation_, **56**(1), 12-26.
*   <a id="pet01"></a>Pettigrew, K.E. & McKechnie, L. (2001). The use of theory in information science research. _Journal of the American Society for Information Science and Technology_, **52**(1), 62-73.
*   <a id="san88"></a>Santos, B.S. (1988). Um discurso sobre as ciências na transição para uma ciência pós-moderna. _Estudos Avançados_, **2**, 56-71.
*   <a id="sar99"></a>Saracevic, T. (1999). Information science. _Journal of American Society for Information Science and Technology_, **50**(12), 1051-1063.
*   <a id="sch04"></a>Schneider, J.W. & Borlund, P. (2004). Introduction to bibliometrics for construction and maintenance of thesauri: methodical considerations. _Journal of Documentation_, **60**(5), 524-549.
*   <a id="sha48"></a>Shannon, C.E. (1948). A mathematical theory of communication. _Bell System Technical Journal_, **27**, 379-423, 623-656.
*   <a id="she77"></a>Shera, J.H. & Cleveland, D.B. (1977). History of foundations of information science. _Annual Review of Information Science and Technology_, **12**, 249-275.
*   <a id="sno61"></a>Snow, C.P. (1961). _The two cultures and the scientific revolution._New York, NY: Cambridge University Press.
*   <a id="tal99"></a>Talja, S., Keso, H. & Pietilaïnen, T. (1999). The production of 'context' in information seeking research: a metatheoretical view. _Information Processing and Management_, **35**(6), 751-763.
*   <a id="tal05"></a>Talja, S., Tuominen, K. & Savolainen, R. (2005). "Isms" in information science: constructivism, collectivism and constructionism. _Journal of Documentation_, **61**(1), 79-101.
*   <a id="tau51"></a>Taube, M. (1951). Functional approach to bibliographic organization: a critique and a proposal. In Shera, J.H. & Egan, M.E. (Eds.). _Bibliographic organization._ (pp. 57-71). Chicago, IL: University of Chicago Press.
*   <a id="tay66"></a>Taylor, R.S. (1966). Professional aspects of information science and technology. _Annual Review of Information Science and Technology_, **1**, 15-40.
*   <a id="une05"></a>UNESCO (2005). _Hacia las sociedades del conocimiento._ Paris: UNESCO Publishing.
*   <a id="veg07"></a>Vega-Almeida, R.L. (2007). [Influencia del paradigma tecnológico en la organización de la información](http://www.webcitation.org/5geGqdBQ5). _Acimed_, **15**(2). Retrieved 26 February, 2008 from http://scielo.sld.cu/pdf/aci/v15n2/aci06207.pdf (Archived by WebCite® at http://www.webcitation.org/5geGqdBQ5)
*   <a id="waa99"></a>Waast, R. & Boukhari, S. (1999). ¿Quién posee la ciencia? _El Correo de la UNESCO_, **LII**, 17-20.
*   <a id="wal99"></a>Wallerstein, I. (1999). _Abrir las ciencias sociales._ 4ª ed. Distrito Federal, Mexico: Siglo Veintiuno Editores.
*   <a id="whi98"></a>White, H.D. & McCain, K. (1998). Visualizing a discipline: an author co-citation analysis of information science, 1972-1995\. _Journal of the American Society for Information Science_, **49**(4), 327-355.
*   <a id="zun77"></a>Zunde, P. & Gehl, J. (1977). Empirical foundation of information science. _Annual Review of Information Science and Technology_, **14**, 67-83.

## <a id="abs">Abstract</a>

> **Introduction.** This paper intends to analyse and establish the historical and epistemological coordinates of Information Science, which in search of a definitive solution to the problems that defune it, has evolved from the explicit validation of objective knowledge, in the first period, to the acceptance of the subject as social actor that constructs knowledge in strong relationship to its context, through a reflexive and interpretative processes.  
> **Methods and analysis** A chronological division into three periods was used, which were successively under the domain of physical, cognitive and social paradigms. Each of these periods was analysed through a set of qualitative variables. A literature review was employed for the analysis.  
> **Results.** The approach from historical and epistemological perspectives allows one to identify the substantial differences that indicate a cyclic reconstruction of the theoretical and conceptual disciplinary supports. These differences allow one to identify those distinctive aspects centred in the subject-object-context relationship, and related to the social paradigm and its emergence.  
> **Conclusions.** If the physical paradigm has the main responsibility for the configuration of Information Science, establishing the foundational theoretic-methodological basis, the cognitive paradigm expresses and is a reflex of a more radical, social and intellectual change and it is centred in the subject as individual. However, the social paradigm transcends the narrow utilitarian and methodological framework that supported the cognitive paradigm, emphasising the historicity of all the social phenomena, and in the persistent discussion of all the subjective elements present in theoretical models.