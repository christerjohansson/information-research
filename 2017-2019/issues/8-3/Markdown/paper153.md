#### Information Research, Vol. 8 No. 3, April 2003

# Competency in network use as a resource for citizenship: implications for the digital divide

#### [Pirkko Jääskeläinen](mailto:pirkko.jaaskelainen@saunalahti.fi)  
The Central Pension Security Institute, Finland  

[Reijo Savolainen](mailto:liresa@uta.fi)  
Department of Information Studies  
FIN-33104 University of Tampere, Finland

#### **Abstract**

> Conceptual and empirical issues of citizens' network competency are explored, defined as people's capabilities to use the Internet in order to communicate and seek information and to utilize electronic public services. First, the concept of network competency is discussed. Second, based on an empirical case study conducted in Finland, perceived network competency is explored as a resource for autonomous and participatory citizenship. Perceived network competency refers to the self-rated assessment made by the informants. A high degree of perceived network competency correlated positively with success in work life. Network competent people participated more frequently in the activities of civic organizations and contacted decision-makers. Compared to less competent people, they appeared to be informed consumers. Those with high perceived competency believed that people's opportunities to influence social issues will increase in the future. The findings are explored with regard to the digital divide vs. digital inclusion, discussing the prospects for future research.

## Introduction

Since the late 1990s, surveys focusing on the "digital divide" have produced a huge number of facts about the demographic differences in access to the Internet or the differences in Internet use among various segments of society (see, for example, [Bonfadelli, 2002](#bon2002); [Norris, 2001](#nor2001); [Katz & Rice 2002](#kat2002)). In addition to general surveys monitoring the gaps in access and use, there is a lot of programmatic talk about the necessity to empower citizens as knowledgeable actors in the information society and to enhance their capabilities in the use of information and communication technologies (ICT) (see, for example, [Mansell, 2002](#man2002)). Undeniably, contributions such as these address highly important issues of equality but, due to the general approach, they are unable to specify the characteristics of citizen's new media competencies in everyday contexts. Thus, there is a need to dig deeper and to approach these competencies in more detail both conceptually and empirically.

Currently, there are high expectations towards the growing significance of the Internet in various fields of action. For example, the European Commission's programme "eEurope. An Information Society For All" aims at bringing every school in Europe into the digital age and online, to create a digitally literate Europe and to ensure that the whole process is socially inclusive and strengthens social cohesion ([European Commission 2000](#eur2000)). However, these goals are not easy to attain because all people are not equally interested in the utilization of networked services.

We hypothesize that the ways in which people subjectively assess their capabilities to use the Internet is a significant predictor of whether people become active users of networked services. In the present study, we use the concept of _perceived network competency_ when referring to the self-rated assessments people make with regard to their abilities to use the Internet (the concept will be discussed later in more detail). The question of network competency is also important in regard to the attainment of the goals of citizenship in the information society, because participation in everyday activities is increasingly dependent on the mastery of ICTs. Generally, the Internet is expected to increase citizens' participation in local, global and virtual societies but all people may not be equal in this sense even in countries which are most advanced technologically. We will discuss Finland as an example.

The study addresses two major research questions:

1.  In which ways is perceived network competency correlated with the attainment of the goals of citizenship among various groups of people using computers and the Internet?
2.  What are the major implications of low network competency for the development of the digital divide?

To answer these questions, we discuss first the concept of network competency. Second, an empirical case study is made to specify how perceived network competency is related to the ways people assess their resources as citizens and how this competency is related to the attainment of the significant goals of citizenship.

## Citizenship and its resources

In general, citizenship means membership in a socio-political community, involving many rights and some responsibilities. Most of the developers of the theories of citizenship explicitly regard _participation_ in society and personal _autonomy_ as the major goals. For example, [Barbalet (1988: 68-69)](#bar1988) proposes that citizenship is fully practiced when individuals are participating on an equal basis in community life. Ultimately, participation of this kind may result in better and more responsible citizenship ([Mansbridge, 1999](#man1999)). Further, autonomy or independence belong to basic needs common to all human beings. The most important feature of autonomy is initiative, which may be triggered particularly when one faces social evils ([Doyal & Gough, 1991](#doy1991)). Also [Roche (1992: 93-94)](#roc1992) emphasizes the importance of personal autonomy, good mutual relations and participation in politics. In the empirical part of our paper, the goals of citizenship will be discussed in more concrete terms.

Thinking of developments in the future information society, the basic goals of good citizenship may not undergo radical changes because autonomy and participation seem to be indispensable to all forms of a democratic society. The difference is that, more strongly than before, the attainment of the goals of citizenship, both autonomous and participatory, are dependent on the access to information needed in everyday life. For example, [Twist (2000)](#twi2000) emphasizes the importance of practical information serving the needs of civil society, concerning e.g., local jobs and housing, neighbourhood events and local schools. Naturally, the list of practical information can be continued by adding other types of information concerning, for instance, social benefits such as pension, access to education and availability of leisure time activities.

People's interest in accessing and using societal information varies among social groups and classes. We may exemplify this issue by reviewing the findings of a nationwide survey conducted in 1998 in the United Kingdom (see [Marcella & Baxter, 2000: 247-249](#mar2000)). They found significant differences between social classes, as far as awareness of various citizenship topics is concerned. For example, 76% of those working in professional occupations felt that they are well or adequately informed of equal rights in society, while only 41% of unskilled workers agreed with this assumption. When asked about the potential use of the Internet to participate in democratic activities, for example, voting in elections and providing feedback to the Government, the differences between the respondents were explained by their social class positions. Those working in professional and managerial occupations appeared to be more interested in the utilization of the Internet to participate in the political discussion.

In sum: autonomy and participation can be defined as the most central goals of (good) citizenship. The attainment of these goals presupposes the existence of various material and immaterial resources. The most important are income, health, education, accommodation, family and social environment. Increasingly, knowledge is a basic resource of citizenship because it lays the foundation of an understanding of society and its processes. Hence, more strongly than before, competency to use various tools such as computers and the Internet belong to the basic resources of attaining the goals of citizenship.

## Information society competencies and network competency

Concepts such as competency, skill and ability are elusive and can be defined from various angles (see, for example, [Attewell, 1990](#att1990); [Sandberg, 2000](#san2000)). In general, competency denotes one's faculty of doing things appropriately, based on one's (inherent) abilities, capabilities and capacities. In other words, competency means an appropriate way of combining the knowledge of what to do and how to do it. Thus, competency refers to the application of an art in a social context where knowledge received through studying and experience is taken into use ([Raivola & Vuorensyrjä, 1998: 23-24](#rai1998)). In short; competency has two dimensions: knowledge and skills. Generally, knowledge may be seen as our understanding of how our everyday world is constituted and how it works. _Skills_ involve the ability to pragmatically apply, consciously or even unconsciously, our knowledge in practical settings (cf. [Spitzberg & Cupach, 1984: 118](#spi1984)).

A number of competencies are required by citizens in the information society. Some emphasize the effective use of ICT tools, while others focus on abilities to interpret the content of information ([Savolainen, 2002](#sav2002)). In addition to _reading skills_ (traditional literacy), people should acquire _information literacy_. The concept implies that an information literate individual should be an ideal "information consumer" in a rapidly and continually changing world of information ([Bruce, 1997: 3](#bru1997)). In order to become "information literate", people should acquire a new set of skills, which include the efficient and effective location and utilization of information for problem-solving and decision-making ([Behrens, 1994: 310](#beh1994)). These demands crystallize the basic idea of information literacy: skilful access to information and its effective use in ever changing information environments.

In addition to information literacy, citizens are expected to have _computer skills_. In general, computer skills deal with the ability to operate and communicate with technological devices, to utilize applications software; to understand the basic jargon or terminology of information technology, to solve problems through the use of technology and to identify and use alternative sources of information ([Bruce, 1997: 21-22](#bru1997)). Further, there is the requirement of _communicative competence_ emphasizing the abilities to adapt messages appropriately to the interaction context ([Spitzberg & Cupach, 1984: 63](#spi1984)). A communicatively competent person knows what to speak and how to do it in order to optimize the conditions of dialogue.

The advent of the Internet challenged the sufficiency of traditional literacy, communicative competence and computer skills. Hence, the specific issues of _network competency_ became topical in the 1990s at the latest. In principle, network competency can be associated with all kinds of activities taking place in electronic networks, ranging from tele-voting and online shopping to designing one's own home pages.

The conceptual setting is complicated because there are a number of related concepts such as 'digital literacy' and 'Internet literacy' ([Bawden 2001](#baw2001); cf. [Gilster 1997](#gil1997)). We prefer the concept network competency because it is not confined to the Internet but is valid also in the context of other electronic networks. Further, network literacy may be preferred as a concept because, unlike "digital literacy", it is not associated with a specific mode of conveying information, that is, "digital" as distinct from "analog".

[Barry (1997: 225-227](#bar1997)) provides a useful approach to network competency, discussing the specific knowledge and skills required in information seeking in networked information environments. For example, in the formulation of information needs when searching the Web, one has to define exact queries in order to avoid excessive information overload. In the location of likely sources, specific skills are needed to find the most relevant Web pages or other electronic sources. In this respect, searchers will need sufficiently broad domain knowledge to be sufficiently aware of materials available on the Internet and to know the ways in which they are organized for use. Similarly, in the examination and selection of sources, one has to be able to specify and refine searches by narrowing or broadening concepts. Evaluation of search results increasingly requires filtering skills in the networked environments.

We may specify the above ideas by adding two major requirements relevant to network competency. First, knowledge and skills in using tools of computer-mediated communication such as e-mail and the software of synchronous and asynchronous discussion groups (for example, IRC and Usenet newsgroups,). E-mail is needed in contacting, for example, busy political decision makers who are not available face-to-face or by telephone. Posting lists and discussion groups provide forums for asking advice from participants having faced, for example, consumer problems (cf. [Savolainen, 2001](#sav2001)). Second, network competency may refer to the knowledge and skills for using transaction services online; for example, filling in electronic forms when applying for social benefits.

To sum up: network competency stands for the knowledge of networked information sources and services plus their skilled utilization (cf. [Savolainen, 2002: 218](#sav2002)). Based on the above ideas, we suggest that network competency stands for the mastery of the following five major requirements:

*   awareness of information (knowing what is available on the Internet and how information resources are organized)
*   the skilled use of ICT tools such as e-mail and Web search engines needed to access information
*   assessment of the relevance of the information found; that is, the evaluation of its quality, filtering out irrelevant information and focusing on specific needs
*   communication (creating and transferring messages by using the tools of computer-mediated communication), and
*   using electronic transaction services (for example, home-banking, ordering and paying services online).

## The empirical study

In order to specify how network competency is related to the ways people assess their resources as citizens, an empirical study was conducted ([Jääskeläinen, 2000](#jaa2000)). As noted above, network competency was investigated by using the self-rated assessments made by the informants. Hence, the study focuses on _perceived_ network competency.

The empirical data of the study were gathered in May-June 1997 by distributing questionnaires to customers who contacted the Central Pension Security Institute, Helsinki, Finland (the questionnaire translated from Finnish is available in the [Appendix](#app), containing items which are directly relevant for the present study). The questionnaires were handed out to customers who personally contacted the Institute to receive information on pension issues. Altogether 900 questionnaires were distributed and 313 were returned (response rate 34.7 %). In addition, the same questionnaire was available on the Web site of the Institute in June - September 1997, resulting in 99 responses. Thus, the study is based on the analysis of 412 responses.

Originally, the study aimed at gathering a statistically representative sample of Internet users and non-users who utilise the services of the Institute. However, the idea of a nationwide survey was given up because the customer population of the Institute is not exactly known. Thus, an empirical case study was preferred, with the aim of obtaining indicative results on the connections between two major variables; that is, the realization of the goals of citizenship and network competency. In contrast to individual variables measuring the rapidly changing use of individual Internet services such as e-mail, the relationships between variables such as realization of the goals of citizenship and network competency tend to change quite slowly. Thus, findings based on the case study material gathered in 1997 can still be considered relevant.

Of the informants, 58% were females and 42% males. One third of the respondents lived in the Helsinki Region, one third in Southern Finland and the rest in other parts of Finland. On average, the informants were 45 years old and about 40% of the participants had secondary or third level education. About 40% of the informants had no vocational education. All the informants were Finnish and Finnish-speaking. We did not ask about ethnicity and religion because almost 100% of the Finns are white and 85% are members of the Evangelical-Lutheran Church.

The analysis is based on the comparison of informants classified into four groups. For the comparison, a scale measuring "citizen competency" was developed: the scale will be discussed in the next chapter. The statistical significance of the differences between the groups was tested by means of the Chi Squared Test, where the frequencies observed were compared with the estimates for the expected frequencies. Differences where the randomized probability, the value p, is less than 0.05 were considered to be statistically significant. In addition, two regression analyses were made; citizen competency and socio-economic factors were taken as explanatory variables, while participatory and autonomous citizenship were the variables to be explained.

## Network competency and the dimensions of citizenship

The issues of competency were investigated by assessing the level of societal knowledge and perceived network competency. Combining these components we may speak of a kind of 'citizen competency'.

The level of societal knowledge was measured by presenting questions about pension issues. The informants were asked to assess whether twelve individual statements describing the Finnish statutory, earnings-related, pension scheme were true or not (or whether they felt undecided). Each correct answer was given one point; those being able to identify at least five correct alternatives were classified into the group of "informed" and others to "uninformed", respectively.

Network competency was measured by asking about the frequency of computer and Internet use. The alternatives available in the questionnaire were as follows: never, sometimes, fairly often and almost daily. The informants choosing the alternatives 'fairly often' and 'almost daily' were classified as 'network competent'; the rest were named 'network incompetent'. Thus, it was assumed that the frequency of use is correlated with the level of competency: the regular users of networked services have necessarily acquired more knowledge and skills in this field as compared to less frequent users or non-users. Even though measurements based on dichotomies such as these may seem quite unspecific, they provide the opportunity to obtain an indicative picture of people's perceived network competency as a component of general 'citizen competency'.

On the basis of the above criteria, four groups of citizen competency were formed:

1.  Those with poor competency: uninformed on societal issues, no network competency ( - - )
2.  Knowledge-centred competency: having societal knowledge but no network competency ( + - )
3.  Those having network competency but uninformed on societal issues ( - + )
4.  Those with good competency: informed on societal issues and having network competency ( + + )

The socio-economic characteristics of the informants in various competence groups are presented in Table 1.

<table>

<tbody>

<tr>

<th> </th>

<th>

1\. ( - - )  
Poor competency: uninformed on societal issues, no network competency (n=95)</th>

<th>

2\. ( + - )  
Knowledge-  
centred competency: having societal knowledge but no network competency  
(n=134)</th>

<th>

3\. ( - + )  
Emphasis on network competency, uninformed of societal issues (n=69)</th>

<th>

4\. ( + + )  
Good competency: informed on societal issues and having network competency (n=114)</th>

<th>Total  
(n=412)</th>

</tr>

<tr>

<td>Females</td>

<td>64%</td>

<td>62%</td>

<td>48%</td>

<td>56%</td>

<td>58%</td>

</tr>

<tr>

<td>Age (average)</td>

<td>46 years</td>

<td>47 years</td>

<td>37 years</td>

<td>44 years</td>

<td>45 years</td>

</tr>

<tr>

<td>Share of full-time job<sup>1)</sup></td>

<td>32%</td>

<td>45%</td>

<td>59%</td>

<td>79%</td>

<td>54%</td>

</tr>

<tr>

<td>Unemployed<sup>1)</sup></td>

<td>48%</td>

<td>34%</td>

<td>7%</td>

<td>2%</td>

<td>24%</td>

</tr>

<tr>

<td>Average income level (FIM/month)</td>

<td>8,500</td>

<td>9,500</td>

<td>12,000</td>

<td>14,000</td>

<td>11,000</td>

</tr>

<tr>

<td>Having college or university education<sup>1)</sup></td>

<td>14%</td>

<td>27%</td>

<td>64%</td>

<td>63%</td>

<td>40%</td>

</tr>

<tr>

<td>Works as superior or expert<sup>1)</sup></td>

<td>5%</td>

<td>9%</td>

<td>30%</td>

<td>39%</td>

<td>20%</td>

</tr>

<tr>

<td colspan="6"><sup>1)</sup> Chi Squared Test; the differences between the groups are statistically highly significant p. = < 0.001</td>

</tr>

</tbody>

<caption>

**Table 1: The groups of citizen competency**</caption></table>

Table 1 shows that there are clear differences between the competency categories. Those located in Group 3 with the emphasis on network competency are younger than the others and there are more men in this group. The share of persons working full-time grows from Group 1 to Group 4\. Even more obvious is the decrease of the proportion of the unemployed. Whereas almost every second person in the low-competence group was unemployed, there was almost no unemployment in Groups 3 and 4\. Also, the educational level rises with competency, and so does success in working life, which is indicated by earnings level and position as superior or expert.

Roughly estimated, network competency seems to be related to the possibilities to succeed in working life, which is most clearly seen when comparing the groups representing poor and good competency. When comparing the middle groups (2 and 3), it seems that network competency is more important than the possession of societal knowledge. Network competency in itself (Group 3) is quite a good indicator of success, but when network competency and knowledge about societal issues combine (Group 4), the indicators for success are even clearer. Correspondingly, a general conclusion can be drawn that insufficient network competency can also reduce the prospects to succeed in the labour market, and it can also be connected to the risk of exclusion.

## Participatory citizenship and network competence

Participatory citizenship was operationalised by asking whether the person has any positions of trust or works for civic organizations and whether he or she, during the past twelve months, had contacted (spoken, telephoned, written, sent e-mail) to any authorities, influential persons, etc., to further a cause. The additional question concerned the effects of the contact. In addition, the participants were also asked about their opinion on the development of ordinary citizens' possibilities to influence.

<table>

<tbody>

<tr>

<th> </th>

<th>

1\. ( - - )  
Poor competency: uninformed on societal issues, no network competency (n=95)</th>

<th>

2\. ( + - )  
Knowledge-  
centred competency: having societal knowledge but no network competency  
(n=134)</th>

<th>

3\. ( - + )  
Emphasis on network competency, uninformed of societal issues (n=69)</th>

<th>

4\. ( + + )  
Good competency: informed on societal issues and having network competency (n=114)</th>

<th>Total  
(n=412)</th>

</tr>

<tr>

<td>Holds positions of trust or works for a civic organization<sup>1)</sup></td>

<td>6%</td>

<td>8%</td>

<td>22%</td>

<td>29%</td>

<td>15%</td>

</tr>

<tr>

<td>Has contacted influential persons</td>

<td>21%</td>

<td>34%</td>

<td>32%</td>

<td>34%</td>

<td>32%</td>

</tr>

<tr>

<td>The matter has been furthered<sup>1)</sup></td>

<td>5%</td>

<td>15%</td>

<td>22%</td>

<td>20%</td>

<td>15%</td>

</tr>

<tr>

<td>Development of possibilities to influence<sup>2)</sup></td>

<td colspan="5"> </td>

</tr>

<tr>

<td>*   have diminished</td>

<td>52%</td>

<td>44%</td>

<td>34%</td>

<td>32%</td>

<td>41%</td>

</tr>

<tr>

<td>*   remained constant</td>

<td>39%</td>

<td>43%</td>

<td>57%</td>

<td>34%</td>

<td>47%</td>

</tr>

<tr>

<td>*   are increasing</td>

<td>9%</td>

<td>13%</td>

<td>9%</td>

<td>14%</td>

<td>12%</td>

</tr>

<tr>

<td colspan="6"><sup>1)</sup> Chi Squared Test; the differences between the groups are statistically highly significant p=<0.001</td>

</tr>

<tr>

<td colspan="6"><sup>2)</sup> Chi Squared Test; the differences between the groups are statistically nearly significant p=<0.05</td>

</tr>

</tbody>

<caption>

**Table 2: Attaining the goals of participatory citizenship in the different categories of citizen competency.**</caption></table>

The comparisons between the different competency groups show that citizens' activity grew with competency. Whereas only 6% of the informants belonging to Group 1 were active in civic organizations, the figure was nearly fivefold in the group with good competency (Group 4). There were no major differences in their contacts with influential persons; however, the differences are significant when assessing the results of the keeping in contact. In Group 1, contacting influential people was scarce and it relatively rarely resulted in any furthering of the matter. Having this kind of experience, it is no wonder that those classified in Group 1 were more pessimistic about their possibilities to influence.

## Autonomous citizenship and network competency

Autonomy was operationalised in line with the method of [Doyal & Gough (1991)](#doy1991) as reacting when noticing faults or unsatisfactory conditions. Other indicators of autonomy were opinions reflecting independence, capability and mastery of life such as opinions about the sufficiency of knowledge in order to be able to influence, whether the respondents thought of themselves as informed consumers, a feeling of having achieved one's personal goals in life and whether life was conceived more as an opportunity than as loss or fear of loss. Table 3 summarises the main findings.

<table>

<tbody>

<tr>

<th> </th>

<th>

1\. ( - - )  
Poor competency: uninformed on societal issues, no network competency (n=95)</th>

<th>

2\. ( + - )  
Knowledge-  
centred competency: having societal knowledge but no network competency  
(n=134)</th>

<th>

3\. ( - + )  
Emphasis on network competency, uninformed of societal issues (n=69)</th>

<th>

4\. ( + + )  
Good competency: informed on societal issues and having network competency (n=114)</th>

<th>Total  
(n=412)</th>

</tr>

<tr>

<td>Has made a complaint about a product or service during the past 12 months</td>

<td>55%</td>

<td>58%</td>

<td>67%</td>

<td>67%</td>

<td>61%</td>

</tr>

<tr>

<td>Is personally able to make a complaint in writing if necessary<sup>1)</sup></td>

<td>59%</td>

<td>71%</td>

<td>78%</td>

<td>78%</td>

<td>71%</td>

</tr>

<tr>

<td>Thinks he or she has sufficient necessary knowledge, or that sufficiency depends on the case in question</td>

<td>65%</td>

<td>63%</td>

<td>73%</td>

<td>72%</td>

<td>71%</td>

</tr>

<tr>

<td>Thinks of him-/herself as a conscious consumer<sup>1)</sup></td>

<td>43%</td>

<td>60%</td>

<td>64%</td>

<td>73%</td>

<td>60%</td>

</tr>

<tr>

<td>Thinks he/she has reached many important goals in life<sup>1)</sup></td>

<td>28%</td>

<td>42%</td>

<td>40%</td>

<td>45%</td>

<td>40%</td>

</tr>

<tr>

<td>Thinks that during the past few years there have been more opportunities than losses or threat of loss in life<sup>1)</sup></td>

<td>41%</td>

<td>52%</td>

<td>67%</td>

<td>77%</td>

<td>59%</td>

</tr>

<tr>

<td colspan="6"><sup>1)</sup> Chi Squared Test; the differences between the groups are statistically highly significant p=<0.001</td>

</tr>

</tbody>

<caption>

**Table 3: Attaining the goals of autonomous citizenship in the different categories of citizen competency.**</caption></table>

Table 3 indicates that the autonomy of the respondents was on a satisfactory level on average. However, the indicators for autonomy regularly increase with network competency. The competent respondents also react more frequently than others when they feel they have been treated wrongly and they are able to make a complaint in writing themselves. They are confident that they have sufficient knowledge to be able to influence and consider themselves as informed consumers. Moreover, their opinions about mastering their own lives and reaching personal goals were better than those with poor competency. The differences are very obvious when comparing the upper and lower competency groups, but not as clear as in the case of participatory citizenship.

## Explanatory models for participatory and autonomous citizenship

In order to elaborate the empirical analysis, direct sum variables were formed from participatory and autonomous citizenship. The variable for participatory citizenship was given the values 0 - 3 in that organisational activity, contacts to influential persons and neutral or positive opinions about possibilities to influence were given one point each. The sum variable for autonomous citizenship was formed by giving one point for each positive reply to the questions in Table 3 above. The scale was given values within the range 0-6.

Since factors other than competency may explain both participatory and autonomous citizenship, the analysis was continued by regression analysis. Explanatory factors adopted were gender, age, educational level, income level, labour market position and the level of knowledge about pensions and experience in using computers. Based on the original explanatory variables, dummy variables were first formed. They were given the values 0 or 1, and thus, they could be applied in the regression analysis. One of the dummy variables of the original variables was left out, and made a constant to which the regression coefficients of the other variables were compared. The constant of the model is an estimate of participatory or autonomous citizenship of a person who comes under the categories left out in the regression analysis.

In the regression analyses presented in Table 4 below, the standard coefficient is a male person with low income (earnings below FIM 5,000 a month), no vocational education, over 60 years of age, working part-time, entrepreneur or pensioner, male with no experience in using computers, poor knowledge about pensions. The statistical significance of the coefficients is expressed in the normal way (***p<0.001, **p<0.01, *p<0.05).

<table>

<tbody>

<tr>

<td> </td>

<th>Participatory citizenship</th>

<th>Autonomous citizenship</th>

</tr>

<tr>

<th>Explanatory variables</th>

<th>Regression coefficients</th>

<th>Regression coefficients</th>

</tr>

<tr>

<td>Constant</td>

<td>0.90 ***</td>

<td>3.32 ****</td>

</tr>

<tr>

<td>Male</td>

<td>- 0.19 *</td>

<td>- 0.09</td>

</tr>

<tr>

<td>under 40</td>

<td>0.08</td>

<td>0.26</td>

</tr>

<tr>

<td>aged 40-49</td>

<td>0.08</td>

<td>0.19</td>

</tr>

<tr>

<td>aged 50-59</td>

<td>0.07</td>

<td>0.07</td>

</tr>

<tr>

<td>Earnings FIM 5,000 - 9,999/month</td>

<td>0.21</td>

<td>0.29</td>

</tr>

<tr>

<td>Earnings FIM 10,000 - 14,999/month</td>

<td>0.25</td>

<td>0.51 *</td>

</tr>

<tr>

<td>Earnings over FIM 15,000/month</td>

<td>-0.43 **</td>

<td>0.53 *</td>

</tr>

<tr>

<td>Full-time job</td>

<td>-0.03</td>

<td>-0.17</td>

</tr>

<tr>

<td>Unemployed</td>

<td>-0.07</td>

<td>-0.18</td>

</tr>

<tr>

<td>Vocational school or technical school</td>

<td>-0.20 *</td>

<td>-0.40 *</td>

</tr>

<tr>

<td>College</td>

<td>-0.11</td>

<td>0.52 **</td>

</tr>

<tr>

<td>University</td>

<td>0.23 *</td>

<td>0.61 **</td>

</tr>

<tr>

<td>Knowledge about pensions average (5-9 correct answers out of 12)</td>

<td>0.17 *</td>

<td>0.47 **</td>

</tr>

<tr>

<td>Good knowledge about pensions (10-12 correct answers out of 12)</td>

<td>0.33 **</td>

<td>0.37 *</td>

</tr>

<tr>

<td>Does not use computers on a regular basis</td>

<td>0.32 **</td>

<td>0.40 **</td>

</tr>

<tr>

<td>Uses computers daily</td>

<td>0.40 ***</td>

<td>0.65 ***</td>

</tr>

<tr>

<td>

**Explanatory rate of the model R<sup>2</sup> (%)**</td>

<td>

**17.05**</td>

<td>

**20.75 \*\*\***</td>

</tr>

</tbody>

<caption>

**Table 4: Regression analysis of autonomous and participatory citizenship.**</caption></table>

The explanatory degrees of the models, 17% and 21% of the variations of the explanatory variables, are satisfactory. Another significant observation is that skills and knowledge are evident explanatory factors for both participatory and autonomous citizenship. This is in line with the presumption, but the great significance of network competency was surprising. Judging from the regression coefficients and their significance it can be concluded that network competency is somewhat more important than societal knowledge.

In the explanatory model for participatory citizenship the impact of other explanatory variables is insignificant or unclear with the exception of gender. Men participated less than women. Age and labour market position had no explanatory value. Instead, it is surprising that high-income persons were participating less than other groups. Perhaps the high-income people considered themselves as being in a position where they did not have enough time to participate in the activities of civil society. Also, as regards education, the results are somewhat unclear. Those with the highest level of education were more involved as compared with other citizens, whereas the participatory level of those with a degree from a vocational or technical school was lower than those having no vocational education whatsoever. More frequent participation among women also indicates the role of income, as there were very few women in the highest income bracket. Although the findings suggesting that the issues of participatory citizenship are more important for women are not conclusive, the results of the case are highly interesting and would deserve further study based on a wider survey.

The explanatory model for autonomous citizenship is more obvious than that of participatory citizenship, which is shown by the explanatory rate of 20.75%. Thus, the educational level, societal knowledge and network competency significantly explain autonomous citizenship. In general terms it can be stated that they form a significant immaterial resource for the information society and that they, as such, are the most significant result of this study. Autonomous citizenship also increased with higher income level. Gender, age and labour market position were not significant in this respect.

## Conclusion

Our empirical study suggests that the importance of network competency as a resource for autonomous and participatory citizenship is increasing. The study indicated that those having the highest level of network competency had a relatively high level of income, and they worked in information intensive tasks, including management tasks. Civic activities appeared to be positively correlated with perceived network competency. Those having less competency were more pessimistic about their potentials to influence social issues and political decision-making. Interestingly, those classified as most competent were more active to seek compensation when facing, for example, low level services. In addition, these people saw themselves as informed consumers, making complaints if needed and believed that they were sufficiently able to seek information in order to influence decision-makers. They also felt that they had reached important goals in their life; in general, they were also looking forward to seeing positive challenges in the future.

As the findings suggest, good computer skills and network competency alone do not suffice for survival in the information society, but attention should also be paid to the substance of societal knowledge needed in problematic situations. Even though the Internet offers a growing repertoire of information sources, they may not be used optimally if people lack societal knowledge, for example, knowing who is entitled to apply for various kinds of social benefits and what kind of benefits are available. On the other hand, if a person is satisfied to consult traditional sources and channels in order to seek information or to use services, devoting no attention to the Internet, he or she is at risk of remaining on the margins because, increasingly, information needed in everyday life will be made accessible through networked media.

Talking about the risks of remaining on the margins does not mean that the extensive utilization of the Internet _per se_ would overturn fundamental inequalities of social stratification. Rather, the risks are associated with less dramatic issues such as being excluded from the access to online employment vacancies, educational resources and new systems of transaction services. The significance of exclusion and the digital divide should not be downplayed, even though some recent surveys (see, for example, [National Telecommunications and Information Administration, 2000](#nat2000); cf. [Norris, 2001: 77-86](#nor2001)) indicate some positive developments towards social inclusion. It seems that the significance of age and gender as the explaining factors of Internet use seem to be decreasing in the United States and in the European Union countries. However, other factors, particularly level of education, occupational position and level of income seem to become central in this respect.

Obviously, the simultaneous developments towards digital inclusion (as to factors such as age and gender) and the digital divide (level of education and level of income) will elicit new tasks for comparative and longitudinal research at national and international levels. In addition to the macro level surveys there is a need to explore the developments towards the divide vs. inclusion within social strata and groups as exemplified by our empirical case study. To sharpen the picture, the quantitative surveys should be complemented by qualitative analyses. Here we find a largely unexplored area: in which ways do people talk about the competencies required in information society and how do they assess their network competence as a resource for the attainment of the goals of citizenship? Apparently, the qualitative issues of network competency become more fruitful if the competency is not conceived as a static disposition but rather as a combination of knowledge and skills which may be developed life long in order to survive in the information society.

## Notes

<a id="note1"></a>1\. An earlier version of this manuscript was presented at 'Internet Research 2.0: Interconnections.' The Second International Conference of the Association of Internet Researchers, October 10-14, 2001, University of Minnesota, Minneapolis, Minnesota, USA.

<a id="note2"></a>2\. Correspondence on this paper should be addressed to [Reijo Savolainen](mailto:liresa@uta.fi).

## References

*   <a id="att1990"></a>Attewell, P. (1990) What is skill? _Work and Occupations_, **17**(4), 422-448.
*   <a id="bar1988"></a>Barbalet, J.M. (1988) _Citizenship, rights, struggle and class inequality_. Milton Keynes: Open University Press.
*   <a id="bar1997"></a>Barry, C.A. (1997) Information skills for electronic world: training doctoral research students. _Journal of Information Science_, **23**(3), 225-238.
*   <a id="baw2001"></a>Bawden, D. (2001) Digital literacies: a review of concepts. _Journal of Documentation_, **57**(2), 218-259.
*   <a id="beh1994"></a>Behrens, S. (1994) A conceptual analysis and historical overview of information literacy. _College & Research Libraries_, **55**(July), 309-322.
*   Blomberg, G., Petersson, O., & Westholm, A. (1989) _Medborgarundersökningen. Råtabeller_ [A survey on citizenship. Statistical tables]. Stockholm, Sweden: Regeringskansliet.
*   <a id="bon2002"></a>Bonfadelli, H. (2002) The Internet and knowledge gaps. A theoretical and empirical investigation. _European Journal of Communication_, **17**(1), 65-84.
*   <a id="bru1997"></a>Bruce, C. (1997) _The seven faces of information literacy_. Adelaide: Auslib Press.
*   <a id="doy1991"></a>Doyal, L., & Gough, I. (1991) A _theory of human need_. London: Macmillan.
*   <a id="eur2000"></a>European Commission (2000) _[eEurope. An information society for all. Communication on a Commission initiative for the Special European Council of Lisbon, 23-24 March 2000](http://dbs.cordis.lu/cordis-cgi/autoftp?FTP=/documents_r5/natdir0000001/s_1690005_20010928_134059_5FPC991512en.pdf&ORFN=5FPC991512en.pdf)_. http://dbs.cordis.lu/cordis-cgi/autoftp?FTP=/documents_r5/natdir0000001/s_1690005_20010928_134059_5FPC991512en.pdf&ORFN=5FPC991512en.pdf (20 March 2003)
*   <a id="gil1997"></a>Gilster, P. (1997) _Digital literacy_. New York, NY: John Wiley.
*   <a id="jaa2000"></a>Jääskeläinen, P. (2000) _Tiedolla ja taidolla kansalaisten tietoyhteiskuntaan: tutkimuksia tiedoista ja taidoista osallistuvan ja autonomisen kansalaisuuden resursseina - esimerkkeinä eläketieto ja tietotekninen osaaminen_ [Through knowledge and skills towards a citizens' information society. Studies in knowledge and skills as resources for participatory and autonomous citizenship - pension knowledge and information technology as examples]. Helsinki: Eläketurvakeskus [The Central Pension Security Institute, Finland].
*   <a id="kat2002"></a>Katz, James & Rice, Ronald E. (2002) _Social consequences of Internet use. Access, involvement and interaction_. Cambridge, MA: The MIT Press.
*   <a id="man1999"></a>Mansbridge, J. (1999) On the idea that participation makes better citizens. In _Citizen competence and democratic institutions_, eds. S. Elkin & K. Soltan. State College, PA: The Pennsylvania State University.
*   <a id="man2002"></a>Mansell, R. (2002) From digital divides to digital entitlements in knowledge societies. _Current Sociology_, **50**(3). 407-426.
*   <a id="mar2000"></a>Marcella, R. & Baxter, G. (2000) The impact of social class and status on citizenship information need: the results of two national surveys in the UK. _Journal of Information Science_, **26**(4), 239-254.
*   <a id="nat2000"></a>National Telecommunications and Information Administration (NTIA) 2000 _[Falling through the net. Toward digital inclusion](http://www.ntia.doc.gov/ntiahome/fttn00/contents00.html)_. Washington, DC: NTIA. http://www.ntia.doc.gov/ntiahome/fttn00/contents00.html (17 January 2003)
*   <a id="nor2001"></a>Norris, P. (2001) _Digital divide. Civic engagement, information poverty, and the Internet worldwide_. Cambridge: Cambridge University Press.
*   <a id="rai1998"></a>Raivola, R. & Vuorensyrjä, M. (1998) _Osaaminen tietoyhteiskunnassa_ [Know-how in the information society]. Helsinki: Sitra.
*   <a id="roc1992"></a>Roche, M. (1992) _Rethinking citizenhip. Welfare, ideology and change in modern society_. Cambridge: Polity Press.
*   <a id="san2000"></a>Sandberg, J. (2000) Understanding human competence at work: an interpretative approach. _Academy of Management Journal_ **43**(1), 9-25.
*   <a id="sav2001"></a>Savolainen, R. (2001) Living encyclopedia or idle talk? Seeking and providing consumer information in an Internet newsgroup. _Library and Information Science Research_, **23**(1), 67-90.
*   <a id="sav2002"></a>Savolainen, R. (2002), Network competence and information seeking on the Internet: from definitions towards a social cognitive model. _Journal of Documentation_, **58**(2). 211-226.
*   <a id="spi1984"></a>Spitzberg, B. & Cupach, W.R. (1984) _Interpersonal communication competence_. Beverly Hills, CA: Sage.
*   <a id="twi2000"></a>Twist, K. (2000) _[Content and the digital divide: what do people want?](http://members.tripod.com/vstevens/papyrus/2000/pn00811b.htm)_ Washington, DC: Digital Divide Network, c/o Benton Foundation. http://members.tripod.com/vstevens/papyrus/2000/pn00811b.htm (17 January 2003)

* * *

<a id="app"></a>

## Appendix

### The questionnaire used in the study.

**Dear client of the Central Pension Security Institute (Eläketurvakeskus),**

On this form you are asked to give your views on certain questions regarding information on earnings-related pensions, finding this information and the ordinary citizen's possibilities of influencing matters. Your opinions are valuable, because they affect the direction of further development. The aim is that the Finnish Centre for Pensions could, in the future, provide services to the citizens even better than now. Therefore, I ask you to take the trouble to fill in this form carefully and return it to the Finnish Centre for Pensions in the enclosed envelope. Postage is paid by the recipient.

On the form, your name and address are also requested. This information is needed for a possible personal follow-up interview, which will be agreed upon separately. Otherwise, the forms will be kept only for research purposes, they will be used only for statistics and the respondents' names will not figure in the results. Further information about this study can be obtained from me.

Thank you for your help!

Central Pension Security Institute

Pirkko Jääskeläinen  
Head of Communications

Phone: 09 - 151 2177

Respondent's name:  
Address:

<u>I. THE QUESTIONS START WITH PENSION ISSUES</u>

Answer the questions by circling the number of the answer alternative that most closely corresponds to your viewpoint or by writing your answer in the space reserved for this purpose on the right.

<table>

<tbody>

<tr>

<td>QUESTION</td>

<td>ANSWER (circle or write)</td>

</tr>

<tr>

<td>

1\. You have contacted the Finnish Centre for Pensions. In which way did you do it?</td>

<td>

1.  Personally, by visiting the customer service or one of the pension information happenings.
2.  By phone.
3.  By letter.
4.  Over the Internet.

</td>

</tr>

<tr>

<td>

<table>

<tbody>

<tr>

<td>

10\. The following is a list of some pensions and benefits. Which of them can be awarded on the basis of the Employees' Pensions Act (TEL)?  

</td>

</tr>

<tr>

<td>1) Individual early retirement pension</td>

</tr>

<tr>

<td>2) Housing allowance</td>

</tr>

<tr>

<td>3) Early old-age pension</td>

</tr>

<tr>

<td>4) Unemployment pension</td>

</tr>

<tr>

<td>5) Care allowance</td>

</tr>

<tr>

<td>6) Injury grant</td>

</tr>

<tr>

<td>7) Part-time pension</td>

</tr>

<tr>

<td>8) Industrial injuries pension</td>

</tr>

<tr>

<td>9) Survivors' pension</td>

</tr>

<tr>

<td>10) Redundancy pay</td>

</tr>

<tr>

<td>11) Front-veteran's pension</td>

</tr>

<tr>

<td>12) Disability pension</td>

</tr>

</tbody>

</table>

</td>

<td>

<table>

<tbody>

<tr>

<td>

Alternatives:
1\. = No  
2\. = Yes  
3\. = I don't know  

</td>

</tr>

<tr>

<td>1) 1., 2., 3.</td>

</tr>

<tr>

<td>2) 1., 2., 3.</td>

</tr>

<tr>

<td>3) 1., 2., 3.</td>

</tr>

<tr>

<td>4) 1., 2., 3.</td>

</tr>

<tr>

<td>5) 1., 2., 3.</td>

</tr>

<tr>

<td>6) 1., 2., 3.</td>

</tr>

<tr>

<td>7) 1., 2., 3.</td>

</tr>

<tr>

<td>8) 1., 2., 3.</td>

</tr>

<tr>

<td>9) 1., 2., 3.</td>

</tr>

<tr>

<td>10) 1., 2., 3.</td>

</tr>

<tr>

<td>11) 1., 2., 3.</td>

</tr>

<tr>

<td>12) 1., 2., 3.</td>

</tr>

</tbody>

</table>

</td>

</tr>

<tr>

<td>

14\. What is your opinion about the Internet (information networks) as a source for pension information for everyone?</td>

<td>

1.  The Internet is too difficult for ordinary people.
2.  Hard to say.
3.  The Internet is also a source for pension information for everyone.

</td>

</tr>

<tr>

<td>

15\. Would you in the future want to be able to check your own pension data over the Internet (information networks), provided that the data protection is one hundred per cent secured?</td>

<td>

1.  No.
2.  Hard to say.
3.  Yes, I would like to.

</td>

</tr>

<tr>

<td>

16\. Would you in the future consider filing your pension application over the Internet, provided that the data protection is one hundred per cent secured?</td>

<td>

1.  No.
2.  Hard to say.
3.  Yes, I would like to.

</td>

</tr>

<tr>

<td>

17\. Do you yourself use computers and the Internet?</td>

<td>

1.  Not at all.
2.  From time to time.
3.  Fairly often (e.g. once a week).
4.  Almost daily.

</td>

</tr>

</tbody>

</table>

<u>II. NEXT WE ASK YOUR OPINION ABOUT ORDINARY PEOPLE'S POSSIBILITIES OF INFLUENCING MATTERS</u>

<table>

<tbody>

<tr>

<td>QUESTION</td>

<td>ANSWER (circle or write)</td>

</tr>

<tr>

<td>

2\. Have you yourself during the past 12 months contacted (talked to, sent a letter or an e-mail to) someone with influence in order to further a certain issue?</td>

<td>

1.  No, I have not contacted anyone.
2.  I contacted someone once.
3.  I have contacted such persons/institutions several times.

</td>

</tr>

<tr>

<td>

4\. How do you think that ordinary people's possibilities of influencing matters have developed in recent times?</td>

<td>

1.  Decreased further.
2.  More or less the same as before.
3.  Increasing.

</td>

</tr>

<tr>

<td>

6\. If needed, do you yourself write a written complaint if you think you have been treated wrongly?</td>

<td>

1.  Yes, I do it myself.
2.  No, not myself, but I know where I can get help.
3.  No, not myself, and I don't know anyone who could help me.

</td>

</tr>

<tr>

<td>

7\. Are you yourself currently active in a position of trust or in a civic organisation? If you are active, in what positions and in which organisations?</td>

<td>

1.  No, I don't
2.  I have the following positions:  

</td>

</tr>

<tr>

<td>

8\. Next follow consumer issues. As what type of consumer do you consider yourself?</td>

<td>

1.  I am not interested in consumer issues.
2.  I keep myself informed about consumer issues to some extent.
3.  I consider myself as a fairly informed consumer.
4.  I consider myself as an excellently informed consumer.

</td>

</tr>

<tr>

<td>

9\. Have you in the past 12 months complained about a product that you have bought or a service that you have received?</td>

<td>

1.  No, I have not complained.
2.  I have complained, but nothing has happened.
3.  I have complained and received compensation, an acceptable explanation or an apology.

</td>

</tr>

</tbody>

</table>

<u>III. FINALLY SOME QUESTIONS ON BACKGROUND INFORMATION</u>

<table>

<tbody>

<tr>

<td>QUESTION</td>

<td>ANSWER (circle or write)</td>

</tr>

<tr>

<td>

1\. Your year of birth</td>

<td>19</td>

</tr>

<tr>

<td>

2\. Sex</td>

<td>

1.  Male
2.  Female

</td>

</tr>

<tr>

<td>

3\. Place of residence</td>

<td>

1.  Helsinki area
2.  Other locality in the Province of Southern Finland
3.  Province of Western Finland
4.  Province of Eastern Finland
5.  Province of Oulu
6.  Province of Lapland
7.  Outside Finland, where 

</td>

</tr>

<tr>

<td>

4\. Your vocational training</td>

<td>

1.  No vocational training
2.  Vocational course, short vocational training, training alongside work
3.  Vocational school, technical school or similar training
4.  Polytechnic, or college/institute on the same level
5.  University-level degree
6.  Other

</td>

</tr>

<tr>

<td>

5\. What is your current labour market status?</td>

<td>

1.  Full-time work, permanent employment
2.  Part-time work, temporary employment
3.  Self-employed person, entrepreneur
4.  Student
5.  Unemployed
6.  Other, what

</td>

</tr>

<tr>

<td>

6\. Your profession  

</td>

<td> </td>

</tr>

<tr>

<td>

7\. If you are working, which do you think is your category?</td>

<td>

1.  Management or leading position
2.  Expert
3.  Professional
4.  Official
5.  Employee
6.  Self-employed person or entrepreneur
7.  Other, what

</td>

</tr>

<tr>

<td>

8\. Your current monthly earnings (gross), FIM/month</td>

<td>

1.  20,000 or more
2.  15,000 - 19,999
3.  10,000 - 14,999
4.  5,000 - 9,999
5.  Less than 5,000
6.  No earnings

</td>

</tr>

</tbody>

</table>

<u>

_CHECK YOUR ANSWERS, MAKE SURE YOUR NAME AND ADDRESS IS MENTIONED._</u>

SEND YOUR ANSWER IN THE ENCLOSED ENVELOPE, POSTAGE IS PAID BY THE RECIPIENT.

<u>

_THANK YOU FOR YOUR TIME AND TROUBLE._</u>