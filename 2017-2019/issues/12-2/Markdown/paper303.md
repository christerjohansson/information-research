#### Vol. 12 No. 2, January 2007

* * *

# A framework for understanding culture and its relationship to information behaviour: Taiwanese aborigines' information behaviour

#### [Nei-Ching Yeh](mailto:ncyeh@cc.shu.edu.tw)  
Department of Information and Communications,
Shih Hsin University,
Taiwan

#### Abstract

> **Introduction.** This article proposes a model of culture and its relationship to information behaviour based on two empirical studies of Taiwanese aborigines' information behaviour.  
> **Method.** The research approach is ethnographic and the material was collected through observations, conversations, questionnaires, interviews and relevant documents. In 2003-2004, the author lived with two Taiwan aboriginal tribes, the Yami tribe and the Tsau tribe and conducted forty-two theme-based interviews.  
> **Analysis.** Data were analysed with the help of software for qualitative analysis (NVivo), where all sentences from both interviews and field notes were coded. The conceptual framework used is the sociology of knowledge.  
> **Results.** The model of culture and its relationship to information behaviour can show us how to think about the relationship between culture and human information behaviour. This model also identifies elements of the model, which are habitus, tradition and prejudice and suggests how we can apply the concepts of information _fullness_ and _emptiness_ to view the relationship between culture and human information behaviour.  
> **Conclusions.** Theoretically, this research puts forward a new model of information behaviour and focuses on the role and the importance of culture when thinking about and studying human information behaviour. Methodologically, this study demonstrates how an ethnographic research method can contribute to exploring the influence that culture has on human life and the details of the human life world and information behaviour.

## Introduction

Culture has been addressed as an influential factor of human information behaviour. For example, Chatman ([2000](#cha00)) found that cultural and social norms do affect ways in which people choose to respond, are passive, or ignore information. Hjørland ([2000](#hjo00)) addressed an information seeking behaviour theory should define the essential characteristics in human information seeking, including a description of its cultural and social determinants. It is generally agreed that information seeking and use are not processed objectively but rather interpreted through expectations about life experience.

Culture shapes the meaning of people's information behaviour and rules determine who is to be considered an information agent. The way we handle, exchange and use information is determined to a large extent by the culture of our environment. Through living and working, people discover, shape, or create information during the process of interacting with the environment or other people. The interaction specifies or shapes what the information is, what makes sense and what information is sought out or neglected. As Solomon said:

> information is something that is embedded in the fabric of people's lives and work. Information is constructed through involvement in life's activities, problems, tasks and social and technological structures, as opposed to being independent and context free. ([Solomon 2002](#sol02): 229)

However, the role of culture or its influence on human information behaviour is not stated explicitly. How should the relationship between culture and information behaviour be considered? Culture is usually seen as a situation which triggers people's information need. This paper argues that we not only can view culture as a situation but also as a big, boundless and invisible feeling of community, in which people's information behaviour is embedded. Culture as a situation not only stimulates people's information need, but also influences people's way of seeking and using information. Also, people filter information through the various components of culture.

The author argues that previous models of information behaviour do not consider the role of culture and its relationship with information behaviour. On the other hand, limited studies have examined the interplay between culture and human information behaviour. In order to bring about a more in-depth understanding of the above questions, together with culture, Berger and Luckmann's ([1966](#ber66)) viewpoint of the sociology of knowledge is the theoretical framework of this study. Within this framework, this paper outlines a theoretical framework for examining culture and its relationship to information behaviour, based on a case study of Taiwanese aborigines' information behaviour.

## The construction of social reality

Berger and Luckmann ([1966](#ber66)) discussed the social construction process of reality and pointed out that a society is both a subjective and objective reality and can be understood through reviewing the sustained procedure of the internalizing and externalizing that people go through after they are born. In this way, people form their subjective views and establish an inter-subjective world, which becomes a foundation of knowledge in everyday life.

Since the society exists before the birth of each individual, the social norms and tradition normalize people's behaviour imperceptibly and form a society called a symbolic universe. The symbolic universe gives people a direction and lets individuals judge whether or not their behaviour is legal. The symbolic universe is a reference to people's actions and provides the chance for people to share with others the collective memories. Each individual accepts and follows the standards of the symbolic universes and identities himself as belonging to one symbolic universe. This is the externalization process ([Berger and Luckmann 1966](#ber66)).

Concerning the internalization process of personal knowledge, the authors describe the process of each person's entering into school and having contact with others. It is the process of socialization: people learn from the environment and those whom they encounter, especially the significant others and establish their personal stock of knowledge. People continue correcting and re-establishing the meanings during the process ([Berger and Luckmann 1966](#ber66)).

Berger and Luckmannn ([1966](#ber66))pointed out that everyday life is controlled by practical motivation. Therefore, practical knowledge, for example, the knowledge of solving problems, occupies an important position in the personal stock of knowledge. Another part of the personal stock of knowledge is determined by the person's social situation. Because people live in the society, they must play roles according to the social norms. From the objective and habitual traditions, people extract and absorb rules which become part of their personal knowledge, thus causing them to behave according to social norms.

Other factors such as personal interests, restrictions (such as being poor), affection (influenced by the system) and habit also influence the knowledge types that people recognize and accept. Once the individual has habitualized actions, his behaviour becomes a typification, which then produces institutionalization. After the personal stock of knowledge is built up, it will influence the individual's behaviour and cognition; this is the so-called 'intentionality', which influences each individual's attempt to reach different goals. Maybe we can not control the individual's consciousness and inference, but we can know well another person's intentionality and comprehend the meaning that things or objects represent to the individual ([Berger and Luckmann 1966](#ber66)).

## Method

In order to explore culture and its relationship to information behaviour, this study illustrates three research questions as follows:

1.  What are the characteristics of Taiwanese aborigines' daily life?
2.  How do they construct the meanings of information and daily life activities?
3.  What is the relationship between culture and Taiwanese aborigines' information behaviour?

This study employed a qualitative research design to explore the above questions and applied an ethnographic research method to explore how Taiwanese aborigines experience, understand, or think about the phenomena that they encounter in the world ([Marton and Booth 1997](#mar97)) and how they think about information during the process of interacting with the phenomena.

In 2003-2004 the author lived in Tsau (twenty-one days) and Yami (nine days) tribes, observing their daily practice and activities and talking with them to gather data. Also, I attended their daily life activities or festival ceremonies to seek an empathetic understanding of the characteristics of their daily life experience and what these experiences meant to them, as well as to understand how their specific cultural features influence them. The observations were written down immediately after they were made. In total, twenty-four Tsau people and eighteen Yami people were interviewed. Interviews were tape-recorded and were transcribed for analysis. Because most Yami people only speak the Yami language, there was a Yami interpreter available to aid in translating the Yami language into Mandarin. The analyses were carried out with the help of software for qualitative analyses (NVivo).

I used certain methods of establishing reliable and valid results in ethnographic research. For example, consistently taking notes, immersing myself in the tribes and exposure to multiple situations were used to increase reliability. As regards validity, I used field notes and an interview guide to establish criterion validity. I also employed construct validity in my research. For example, the results indicated convergence with Berger and Luckmann's concepts, thereby supporting construct validity for this inquiry.

## Results

### The backgrounds of Tsau and Yami people

Even though Taiwanese aborigines are deeply influenced by Han culture, they can still remain culturally distinct. Regarding their brilliant culture, the tribal head decides many of the public affairs, even thought there is an administrative system in each tribe. In addition, the shaman has a high position similar to that of the tribal head for the Tsau and Yami people. Hunting is the major feature of Tsau culture, whereas the technique of carving boats is the most famous characteristic of Yami culture.

The Tsau people live in a mountainous region (Taiwan Central Mountain) twenty kilometers away from the nearest community with poor transport facilities, while the Yami tribe is the only oceanic ethnic group in Taiwan. In order to collect rich data, I focused on two tribes with different lifestyles. They have a slow pace of life and like to chat with each other, so messages are transmitted quickly.

### The characteristics of the Taiwanese aborigines' daily life

#### The Kuba—the meeting place

The Kuba is a common meeting place of the Tsau people, in which only the males can enter. When the tribal head or the elder has important affairs to declare, he shouts from the Kuba and every family sends one man to hear the news. This symbolizes and strengthens people's solidarity and functions as a social network.

#### The ancestral soul—causing them illness

Two tribes believe their ancestor's souls can lead to disease, so they ask the shaman to drive the souls away, in order to regain health. In other words, they do not make appointments with a doctor and are not cured by medicine. Although the younger generation has a more modern idea of health, they still rely on the shaman to help them feel at ease.

#### Faith in the Shaman's—helping them solve problems

They also believe that the shaman can help them resolve problems, relating to being unpropitious, catching an illness, being away from home, having a nightmare and buying a house etc. They rely on the shaman's advice to give them a direction or to tell them what to do. Because of the invasion of one culture by another, we can see the effects of combining religious beliefs and traditional cultures everywhere. For example, some people go to a health centre to get medicine to cure a disease, they also go to a church to pray for recovery, or ask shamans to drive the soul away.

#### Hunting—maintaining life

Even though Taiwan public authorities in Taiwan prohibit people from hunting, hunting is part of the Tsau people's traditional way of life. They do not want to give up and hope to hand down their hunting knowledge from generation to generation. Children learn hunting skills through observing and simulating their fathers' actions. Children are taught how to hunt, how to distinguish the footprints or calls of specific animals and how to protect the forest and respect other people's resources and transmit such an agreement through the marking of territory.

#### Ceremony—praying for a good harvest

Each Taiwanese aborigine tribe has its own ceremony. For example, the Tsaus people's most famous ceremonies are Mayasvi and Homeyaya and the Yami people's is _the memorial ceremony for the flying fish_. They hold the ceremony to pray for a good harvest every year.

### Meanings of information and daily activities

#### Traditional—tradition is culture

People's behaviour is directed toward tradition. As Gadamer ([1989](#gad89)) said, tradition is the power of history over finite human consciousness. We should become completely aware of effective history which has affected consciousness and has also been an element in the act of understanding. This is what Berger and Luckmann said,

> The social norms and tradition normalize people's behaviors imperceptibly and form a society called a symbolic universe which gives people a direction and lets individuals judge whether or not their behaviours are legal. The symbolic universe is a reference to people's actions, each individual accepts and follows the standards of the symbolic universes and identities himself as belonging to one symbolic universe. This is tradition and it makes sense for each individual. ([Berger and Luckmann 1966](#ber66): 94)

Just like the Hollywood film 'The Fiddler on the Roof', the poor peasant Tevye under pressure from Jewish rules, survived by playing the fiddle on the roof. But he still hoped to follow the Jewish traditional etiquette and customs and was happy to lead a simple and virtuous life. Jewish tradition made sense to him, so he could maintain balance on the roof. Obviously, how culture influences people is far reaching.

In this study, aborigines believe and rely on the shaman to cure their illness and make them safe and do not need additional information. We might feel the tribe's life is not modern, especially coming from an information society, but it is their real life world and full of meaning for the aborigines.

#### From generation to generation

Based on Berger and Luckmann's theory, through the externalization and internalization processes, these cultures, learn from significant others or other group members. The Tsau people's hunting culture is an example. The ancestor's daily life wisdom tells them how to exist and transmits the hunting knowledge, they get the through learning by doing, not through books or other printed publications. As an interviewee said,

> We live in the forest when we are children and follow our parents up the hill, whether they are working or hunting. The adult teaches us how to set a trap to catch animals and how to use different tools. We can say that the wooded mountain is our classroom. (Respondent V)

#### Conversation in daily life

Taiwanese aborigines can be regarded as a society based on oral communication and by this way, they get rich information. They have their own oral language but no written language, so oral communication is also an important way to preserve their culture. They use spoken language to deliver the information. Besides, they like to talk with others, because the population of the tribe is small and almost all people know one another; chatting with others is considered polite. Here is one example:

> The pace of life is slow and people have much time and are used to chatting with others, about everything, even subjects we may think are trifling such as newly born pigs. (Respondent U)

#### The relationship between culture and Taiwanese aborigines' information behaviour

People's behaviour, includes information behaviour, embedded in cultural and social practice. The relationship between culture and Taiwanese aborigines' information behaviour is multiple, not only association but also interaction. Based on the review of literature and empirical study results, the model offered in Figure 1 could be used to think about and understand the interaction of culture and human information behaviour.

<figure>

![Figure 1](../p303fig1.jpg)

<figcaption>

**Figure 1\. The model of culture and human information behaviour**</figcaption>

</figure>

This figure illustrates this relationship between culture and human information behaviour. Through the internalization and externalization processes, the tradition shapes people's stock of knowledge and forms their prejudices, horizons, values, habitus, common sense and specific knowledge. When people encounter a situation and need information, they will react to it with their own knowledge firstly, to judge if they need to seek information or not. If their own knowledge (or prejudice) can bridge the gap, they do not need to seek other information, on the contrary, they seek other information to help them. Through using the information and internalization, they extend their stock of knowledge. Again, the stock of knowledge determines if people will seek information or not. This circular and interactive relationship between culture and information behaviour, is denoted as _information fullness and emptiness_.

_Information fullness and emptiness_ refers to someone who encounters a context or situation in which information is needed: when s/he first applies personal knowledge to react to the context, this is a condition of information fullness. In other words, people do not need to seek other information to help them. If his/her personal knowledge cannot bridge the gap, the condition is one of information emptiness. In this case, his/her information need is formed and s/he seeks information from an information sources.

The elements of the model are briefly described below:

**Tradition**. People are born and grow up in the symbolic community. How to behave to fit the social norm is taught and transmitted from generation to generation. It is tradition and tradition is culture. Tradition shapes parts of the individual's experiences and individual build their knowledge through their experience. In other words, tradition forms parts of the individual's stock of knowledge, which is the first resource for bridging the gap when a problem is encountered.

**Prejudice**. Prejudice is a concept derived from Gadamer's ([1989](#gad89)) philosophy. According to him, prejudice refers to a judgment that is rendered before all the elements that determine a situation have been fully examined. People's experience, common sense learned from the lifeworld and education, construct their prejudice. In other words, through learning by doing, observing and listening, people form their prejudice unconsciously. Prejudice determines one's horizons and ideas about the world and with respect to information behaviour, it influences people's thinking about what information is, if they need information or not and why they ignore information.

**Habitus**. According to Bourdieu ([1990](#bou90)), habitus is a system of durable, transposable dispositions, that is, as principles which generate and organize practices and representations that can be objectively adapted to their outcomes without presupposing a conscious aiming at ends or an express mastery of the operations necessary in order to attain them. Habitus affects actions and was constructed and cultivated by tradition during the processes of maturation.

### Conclusion

The aim of this article is to conceptualize culture and its relationship to information behaviour. The author offers a model to show how to think about the relationship between culture and human information behaviour, identifies elements of the model and suggests how we can apply the concept of _information fullness and emptiness_ to view the relationship between culture and human information behaviour.

Theoretically, this research puts forward a new model of information behaviour and focus on the role and the importance of culture when thinking about and studying human information behaviour. This model is helpful for understanding some unclear questions, for example, why people do not seek or ignore information. It also aids us in reflecting on some ingrained thinking, for example, information is important.

ethodologically, this study demonstrates a way in which an ethnographic research method can contribute to explore the influence that culture has on human life and the details of the human life world and information behaviour. In the future, we still need much more critical suggestions and empirical studies to improve and perfect the model and the _information fullness and emptiness_ concept explored in this study.

## Acknowledgements

The paper benefited from the thoughtful feedback of its anonymous reviewers.

## References

*   <a id="ber66"></a>Berger, P.L. & Luckmannn, T. (1966). _The social construction of reality: a treatise in the sociology of knowledge_. New York, NY: Anchor Books.
*   <a id="bou90"></a>Bourdieu, P. (1990). _The logic of practice_. Cambridge: Polity Press.
*   <a id="cha96"></a>Chatman, E.A. (1996). The impoverished life-world of outsiders. _Journal of the American Society for Information Science_, **47**(3), 193-206.
*   <a id="cha98"></a>Chatman, E.A. (1998). Small world lives: implications for the public library. _Library Trend_, **46**(4), 732-752.
*   <a id="cha99"></a>Chatman, E.A. (1999). A theory of life in the round. _Journal of the American Society for Information Science_ **50**(3), 207-217.
*   <a id="cha00"></a>Chatman, E.A. (2000). Framing social life in theory and research. _The New Review of Information Behaviour Research_, **1**, 3-17.
*   <a id="gad89"></a>Gadamer, H-G. (1989). _Truth and method - I_. (2nd rev. ed.). London: Continuum. (Original German edition published in 1960).
*   <a id="hjo00"></a>Hjørland, B. (2000). Information seeking behaviour: what should a general theory look like? _The new review of information behaviour research_, **1**, 19-33.
*   <a id="mar91"></a>Markus, H.R. & Kitayama, S. (1991). Culture and the self: implications for cognition, emotion and motivation. _Psychological Review_, **98**(2), 224-253.
*   <a id="mar93"></a>Marsella, A.J. (1993).Counseling and psychotherapy with Japanese Americans: cross-cultural considerations. _American Journal of Orthopsychiatry_, **63**(2), 200-208.
*   <a id="mar97"></a>Marton, F. & Booth, S. (1997). _Learning and awareness_. Mahwah, NJ: Lawrence Erlbaum Associates.
*   <a id="sav95"></a>Savolainen, R. (1995). Everyday life information seeking: approaching information seeking in the context of way of life._Library and Information Science Research_, **17**(3), 259-294.
*   <a id="sol02"></a>Solomon, P. (2002). Discovering information in context. _Annual review of information science and technology_, **36**, 229-264.
*   <a id="tuo97"></a>Tuominen, K. and Savolainen, R. (1997). A social constructionist approach to the study of information use as discourse action. In Pertti Vakkari, Reijo Savolainen and Brenda Dervin, ed._Information seeking in context: proceedings of an international conference on research in information needs, seeking and use in different context 14-16 August, 1996, Tampere, Finland_. (pp.81-96). London: Taylor Graham.
*   <a id="wil97"></a>Wilson, T.D. (1997). Information behaviour: an interdisciplinary perspective. _Information Processing and Management_, **33**(4), 551-572.