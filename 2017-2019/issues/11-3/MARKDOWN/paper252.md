####Information Research, Vol. 11 No. 3, April 2006



# Towards an integrated analytical framework of information and communications technology literacy: from intended to implemented and achieved dimensions

#### [Lina Markauskaite](mailto:l.markauskaite@edfac.usyd.edu.au)  
Centre for Research on Computer Supported Learning and Cognition (CoCo),  
School of Development and Learning, Faculty of Education & Social Work (A35),  
The University of Sydney, Australia


#### Abstract

> **Introduction.** Theoretical approaches and frameworks that help us to understand the contemporary notion of information and communication technology literacy (ICT literacy) in the formal education sector are reviewed and examined.  
> **Method.** The analysis is conducted from a technology (i.e., computer science) conceptual perspective. The focus is on those aspects of new literacies that are directly related to the use of information and communication technologies. Structured literature review and documentary research techniques are applied.  
> **Analysis.** Relationships between ICT literacy, information literacy, media literacy and other new literacies are clarified. Important terms - 'ICT', 'literacy' and 'ICT literacy' - are discussed. An analytical framework for the investigation of contemporary understandings of information and communication technologies literacy is presented. Three analytical dimensions of ICT literacy - (1) intended, (2) implemented and (3) achieved - are employed in this framework. The main perspectives and structural approaches that can be applied for the examination of ICT literacy in each of these three dimensions are discussed.  
> **Results.** The proposed analytical framework reveals links between (1) the conceptual approaches and initial aims of ICT literacy policies, proposed at the top-level of policymaking; (2) teaching and learning practices, implemented at the middle-level of educational system and (3) ICT literacy learning experiences and students' outcomes, expected at the base-level of educational system.  
> **Conclusions.** It is argued that this analytical framework can be applied for an integrated analysis of ICT literacy. The framework provides a conceptual structure for discovering inconsistencies in the understanding of ICT literacy at various levels of educational systems.



## Introduction

Various technological and social developments have been reshaping almost all aspects of human life. Some of the knowledge, skills, abilities, competencies and personal characteristics that were necessary for life in previous centuries have now become irrelevant, while others have become critical. The majority of these changes are associated with the proliferation of new technologies, particularly information and communication technologies (ICT). The capacity to apply ICT in various areas of human life (usually defined as _ICT literacy_) has become an important contributor to human wellbeing and the prosperity of society. Enhancement of ICT literacy is the top priority of the social, economic, and educational policies of many international organizations and individual countries. However, the need for ICT-related capacities has emerged quite suddenly and the concept of ICT literacy is still very new. The notion of this term is poorly understood, particularly in the sector of formal education.

Many different terms are used to describe various sets of ICT-related capacities. For instance, a myriad of terms have been used in national and international policy documents. These include _ICT literacy_ ([ETS 2002](#ETS02)), _digital literacy_ ([EC 2003](#EC03)), _ICT fluency_ ([NRC 1999](#NRC99)), _computer literacy_ ([Williams 2003](#Williams03)), _ICT skills_ ([QCA 2005](#QCA05)) and _technological literacy_ ([ISTE 1998](#ISTE98)). Other terms that describe capabilities indirectly related to ICT - such as _media literacy_ ([AMLA 2005](#AMLA05)), _information literacy_ ([ACRL 2000](#ACRL00); [Bundy 2004](#Bundy04)) and _eliteracy_ ([Martin 2000](#Martin00)) - have sometimes been used interchangeably in similar contexts. A number of other terms that include ICT-related capabilities, but in general describe broader sets of capacities - such as _generic skills_ ([Kearns 2001](#Kearns01)), _21st century skills_ ([NCREL 2003](#NCREL03); [Partnership for 21st Century Skills 2002](#Partnership02)) _multiliteracies_ ([Cope & Kalantzis 2000](#Cope00)) and _new literacies_ ([Leu, _et al._ 2004](#Leu04)) - have been used in recent literacy reports and reviews. Hundreds of other terms with similar meanings can be found in the scholarly literature as well (_e.g._, see [Bawden 2001](#Bawden01); [Cesarini 2004](#Cesarini04); [Virkus 2003](#Virkus03)).

In distinctive contexts, various combinations of these terms are sometimes applied synonymously and have the same semantic. However, this is not always the case: the meanings of different terms often are similar, but not exactly the same, while sometimes they are completely different ([Armstrong, _et al._ 2005](#Armstrong05); [Bawden 2001](#Bawden01); [Virkus 2003](#Virkus03)). Moreover, with the evolution of technology and society, the need for ICT-related capacities continues to change. For instance, in the 1980's the ICT- related knowledge and skills taught in secondary schools and colleges included mainly fundamental ICT concepts and programming ([Selfe & Hawisher 2002](#Selfe02)). In the 1990's, courses shifted to the teaching of how to use general-purpose computer applications ([Langhorne, _et al._ 1989](#Langhorne89)). Currently the focus is on the teaching and learning of a broad range of ICT-related cognitive capabilities and general understandings ([ISTE 1998](#ISTE98); [NRC 1999](#NRC99)).

ICT-related capacities are tightly connected with other traditional and new human capabilities, such as literacy and numeracy, scientific, economic, technological, visual and information handling abilities, thinking and productivity capacities and communication abilities ([Armstrong & Warlick 2004](#Armstrong04); [NCREL 2003](#NCREL03)). Therefore, changes in ICT cause changes not only in ICT literacy, but also in many other human capabilities. For this reason, it is not always easy to separate ICT-related capacities from other cognitive and non-cognitive human attributes. Because of this complexity, there is no consensus even about the minimal set of abilities required to be ICT literate in the knowledge society. For instance, European Computer Driving Licence Foundation _Equalskills_ program, concerned with universal ICT literacy, covers only the basic skills to use a computer and networks ([ECDL 2004b](#ECDL04b)). While the European Union action plan _eEurope 2005_ includes into the definition of digital literacy various higher-order and communication capacities, such as teamwork, problem solving and project management ([EC 2002](#EC02)).

An agreement on the definitions and descriptions of key terms is important. As researchers argue, a common vocabulary is necessary for effective policymaking ([Lonsdale & McCurry 2004](#Lonsdale04); [Virkus 2003](#Virkus03)). It helps to reconcile the aims and activities at different levels of the educational system and achieve synergy in policy implementation ([Bundy 2004](#Bundy04)). However, in a number of countries the understanding of ICT literacy is not the same in different domains and levels of decision-making and implementation ([Cuttance & Stokes 2000](#Cuttance00); [Kearns & Grant 2002](#Kearns02a); [Markauskaite & Dagiene 2004](#Markauskaite04); [Markauskaite 2005a](#Markauskaite05a)). The goals of ICT literacy enhancement proposed at one level of the educational system are sometimes at odds with the actions implemented at another level or the results achieved at a third level. Discovering inconsistencies in the understanding of ICT literacy in various domains and levels of educational systems is an important research task.

This paper aims to contribute to the discussion about the notion of ICT literacy in formal education. It presents and summarises analytical frameworks that have been used to examine ICT literacy policies, implemented curricula and learning outputs in several theoretical and empirical studies ([Markauskaite & Dagiene 2004](#Markauskaite04); [Markauskaite 2005a](#Markauskaite05a), [2005b](#Markauskaite05b), [2005c](#Markauskaite05c)). The paper highlights relationships between different analytical dimensions and proposes an integrated framework that can be used for the investigation of contemporary understandings of ICT literacy at different levels of policy making and in different contexts. This paper investigates the phenomenon of new literacies primarily a technology perspective. Thus, the focus of the study is on those capacities that are directly related to the use of ICT. The primary concern in this paper is the sector of formal education. The applied research methodology is based on structured literature review and documentary research techniques ([May 1997](#May97)). It combines secondary analysis of research literature with a primary analysis of international policy and educational documents (such as international reports, ICT strategies, curricula and standards).

The paper is structured into six sections. In the first section, the term _ICT literacy_ is discussed. Initially, several related terms that have different meaning, but are often used in similar contexts, are reviewed. Contemporary understandings of the words _ICT_ and _literacy_ and their synonyms are investigated. It is argued that _ICT literacy_ is a semantically correct and appropriate term for the definition of ICT-related capacities. In the second section, a three-dimensional framework for the investigation of ICT literacy is presented. It includes three analytical dimensions: (1) intended or external policy; (2) implemented or teaching and learning and (3) achieved or internal structural. In the third section, different rationales for the introduction of ICT into education are summarized and their implications on the understanding of ICT literacy are investigated. The main models of general literacy are presented and their impact on the notion of ICT literacy is discussed. In the fourth section, fundamental aspects of the teaching and learning of ICT literacy are investigated. In the fifth section, the key perspectives and the main elements of ICT literacy are discussed. In the last section, the relationships between all three dimensions are discussed.

## ICT literacy: definitions of the main terms

### Defining territory: ICT literacy and other new literacies

New technologies and other changes in society generate a need for new literacies. As scholars from the literacy domain argue, new capabilities are required to exploit the potential of reading, writing and numeracy in a new environment, and consequently the notions of traditional literacies need to be redefined ([Kress 2000](#Kress00); [Unsworth 2002](#Unsworth02)). In addition, scholars from other disciplinary domains have begun to recognize that new kinds of capabilities, related to their subject areas, are necessary to effectively harness various cultural, social and economical transformations ([Cope & Kalantzis 2000](#Cope00); [Snyder 2002](#Snyder02)). A number of new literacies, such as _environmental literacy_ ([Disinger & Roth 1992](#Disinger92)), _media literacy_ ([Potter 2001](#Potter01)), _multicultural literacy_ ([Courts 1998](#Courts98); [Weil 1998](#Weil98)), and _multiliteracy_ ([New London Group 2000](#New00)) have been conceptualized.

Not all newly identified literacies have gained a broader recognition. However, at least three literacies - ICT ([ISTE 1998](#ISTE98); [Martin 2000](#Martin00)), information ([Bundy 2004](#Bundy04); [Herring 1996](#Herring96)) and media literacy ([Potter 2001](#Potter01); [Schwarrz & Brown 2005](#Schwarrz05)) - which on different levels are related to ICT, have achieved some status in the academy and found a place in secondary and higher school curricula. In general, scholars from different domains agree that each of these three literacies include different sets of capabilities (_e.g._, [Bawden 2001](#Bawden01); [Candy 2003](#Candy03); [Christ & Potter 2004](#Christ04)). Nonetheless there are no clearly agreed definitions and the confusion between ICT capacities, media literacy and information literacy is still big ([Armstrong, _et al._ 2005](#Armstrong05); [Virkus 2003](#Virkus03)).

For example, one of the most established American Library Association definitions describes _information literacy_ as: 'a set of abilities requiring individuals to recognize when information is needed and have the ability to locate, evaluate, and use effectively the needed information.' ([ACRL 2000](#ACRL00): 2). In the same US, the most agreed among scholars _media literacy_ definition describes media literate person as one who: 'can access, analyze, evaluate, and produce both print and electronic media.' ([Aufderheide 1992](#Aufderheide92)). Recently proposed by the US Educational Testing Service _ICT proficiency_ definition is also similar. It describes ICT proficiency as: 'the ability to use digital technology, communication tools, and/or networks appropriately to solve information problems in order to function in an information society. This includes the ability to use technology as a tool to research, organize, evaluate, and communicate information and the possession of a fundamental understanding of the ethical/legal issues surrounding the access and use of information.' ([ETS 2003](#ETS03): 11).

The definitions of all three literacies have some similarities and semantic differences between the terms are not self-evident. Leu, _et al_. ([2003](#Leu04)) identify ten central principles that are common to all new literacies. These principles, _inter alia_, state that two aspects - _ICT tools_ and _strategic knowledge_ - are central for any new literacy. Indeed, ICT proficiency, information literacy and media literacy are related to ICT. In addition, strategic knowledge, such as the ability to identify, locate and evaluate, are included in all three definitions. This strategic knowledge corresponds to different levels of general cognitive capabilities in the Bloom's educational taxonomy ([Bloom & Krathwohl 1956](#Bloom56); [Krathwohl, _et al_. 1964](#Krathwohl64)). As Lonsdale and McCurry ([2004](#Lonsdale04)) argues, cognitive capabilities are not a separate literacy, nor a part of one particular literacy, rather they are a core part of all literacies.

However, the teaching and learning of ICT, information and media literacies have their roots in different disciplinary areas and place an emphasis on different shared components of new literacies. ICT literacy has its foundation in computer sciences and informatics, and consequently emphasises that 'the ability to use particular digital devices, software, and infrastructure is important' ([ETS 2003](#ETS03): 12). Information literacy emerged from library and information sciences. Therefore it accentuates the capabilities to interact with information ([ACRL 2000](#ACRL00)). Meanwhile, media literacy emerged from media and civic studies and primarily concerns the capability to critically analyze media messages ([Aufderheide 1992](#Aufderheide92)). The main difference between ICT literacy and the other two literacies is that technological 'know-how' has central role in the notion of ICT literacy and the learning of ICT literacy is impossible without ICT tools. Meanwhile ICT has an important, but complementary role in the concepts and learning processes of information and media literacies.

While recognizing close links and interconnections between all three literacies, this paper focuses on capacities directly related to ICT, _i.e._, ICT literacy. This choice was made for several reasons. Because of its technological origination, conceptual aspects of the teaching and learning of ICT literacy have not been extensively researched. Moreover, despite the fact that the teaching and learning of computer and other ICT-related capabilities have a long history and prevalence in formal education, there is paradoxically little agreement on how to define the nature and scope of ICT-related capacities that should be of concern for formal education ([McMillan 1996](#McMillan96)).

As indicated above, different terms are used for the description of capabilities that relate directly to ICT. The term _ICT literacy_ has been chosen as the main umbrella term in this paper. The following characteristics of the term _ICT literacy_ are regarded as being the most important for its selection: (1) the term incorporates all information processing and transmission related technologies, but is not too general and excludes field-specific advanced technologies (_e.g._, gene engineering); (2) it includes the full range of knowledge, skills, abilities, attitudes and other capacities associated with the use of both information processing and transmission technologies; (3) the semantic of the term is open to include other cognitive and non-cognitive human attributes that could become important with the future development of technologies and society and (4) the term is not new, but already in use by decision-making, educational and research communities. In addition, consideration was given to other main principles of new literacies ([Leu, _et al._ 2003](#Leu04); [Lonsdale & McCurry 2004](#Lonsdale04)). A detailed examination of the semantic appropriateness of _ICT literacy_ and other rival terms is outside the objectives of this paper. Nevertheless, a discussion about the features of 'ICT', 'literacy', 'ICT literacy' and various other terms that are used for the definition of ICT-related capacities introduces the complexity of the topic and provides the conceptual ground for the understanding of ICT literacy issues in formal education.

### Why _ICT_ literacy?

The development of new technologies has resulted in a demand for the development of new human capacities. The majority of these changes in education are associated with considerably enhanced opportunities to store, manipulate, and diffuse information. That is, three features of new technologies are the most important for education - the capability to digitalize, process and transmit information. Words that are commonly used for the definition of these technologies in research and policy documents include such terms as _21st century tools_, _computer_, _information technologies_, _communication technologies_, _multimedia_, _information and communication technologies_, _digital technologies_, _network technologies_, _learning technologies_ or just _technologies_. However, the scope and semantic of these terms are not the same. For instance, the terms _multimedia_, _digital_ ([EC 2003](#EC03)), _computer_ and _information technology_ ([ETS 2002](#ETS02), [2003](#ETS03); [Lennon, _et al_. 2003](#Lennon03)) imply only the digitalisation and processing of information. _Communication_ ([ETS 2002](#ETS02), [2003](#ETS03); [Lennon, _et al._ 2003](#Lennon03)), _network_ ([Bawden 2001](#Bawden01)) or similar terms entail only digitalisation and transmission. The term _information and communication technologies_ ([Anderson & Weert 2002](#Anderson02); [ETS 2002](#ETS02), [2003](#ETS03)) covers all three features. More general terms - _21st century tools_ ([Partnership for 21st Century Skills 2002](#Partnership02)), _technology_ ([ISTE 1998](#ISTE98)) or _learning technology_ ([ACTG 2004](#ACTG04)) - are usually used in a broader sense than ICT and involve other (_i.e._, non-ICT) technologies that could be applied for teaching and learning.

However, there is a lack of consensus concerning definitions of these terms. For instance, in the Education Testing Service's reports ([ETS 2002](#ETS02), [2003](#ETS03)), the term _digital technology_ covers only information technology and explicitly excludes communication tools.[<sup>1</sup>](#note1)<a name="note1back"></a> While in the _Learning Technologies Plan_ of the Australian Capital Territory ([ACTG 2004](#ACTG04)), _digital technology_ is as a synonym for _information and communication technology_. Moreover, the definition of both these terms includes other technologies that are not directly related to computer-based information processing and/or transmission.[<sup>2</sup>](#note2)<a name="note2back"></a>

The most exact and sustainable term, which has been used broadly in the educational domain, is _information and communication technology_ ([Anderson & Weert 2002](#Anderson02); [ETS 2002](#ETS02), [2003](#ETS03); [Lennon, _et al._ 2003](#Lennon03)). The most comprehensive description of this term was proposed by UNESCO as far back as 1994 ([Weert & Tinsley 1994](#Weert94)); and it is still used ([Anderson & Weert 2002](#Anderson02)).[<sup>3</sup>](#note3)<a name="note3back"></a> This definition of ICT explicitly includes not only hardware and software, but also 'organizational and human aspects' and 'industrial, commercial, governmental and political implications' ([Anderson & Weert 2002](#Anderson02): 12). Other recently proposed definitions of ICT have similar semantic and include all three aspects of modern technologies - digitalisation, processing, and transmission ([Cuttance & Stokes 2000](#Cuttance00); [ETS 2002](#ETS02); [Lennon, _et al._ 2003](#Lennon03)). They particularly emphasize ICT's ability to enable communication.

In summary, _information and communication technology_ (ICT) is one of the most appropriate terms for use in an educational context. Generalising the UNESCO ([Anderson & Weert 2002](#Anderson02)) and other ([Cuttance & Stokes 2000](#Cuttance00); [ETS 2002](#ETS02), [2003](#ETS03)) commonly employed definitions, ICT can be described as the application of information processing and transmission systems in society, including hardware, software, communication tools and networks, organizational and human aspects, and the industrial, commercial, governmental, political, social and cultural implications of these.

### Why ICT _literacy_?

Various words are used to define the range of cognitive and non-cognitive human attributes (_e.g._, knowledge, skills, abilities, capabilities, beliefs, and attitudes) that could be associated with ICT. The most common terms that have been used in extensive research reviews ([Cesarini 2004](#Cesarini04); [Kearns 2001](#Kearns01); [Lonsdale & McCurry 2004](#Lonsdale04); [Virkus 2003](#Virkus03)) and international reports ([ETS 2002](#ETS02); [NRC 1999](#NRC99); [OECD 2001](#OECD01)) include such terms as _ability_, _capability_, _capacity_, _competence_, _competency_, _expertise_, _fluency_, _empowerment_, _know-how_, _knowledge_, _literacy_, _mastery_, _proficiency_ and _skills_. Similarities, differences and relationships between these terms are not easy to identify.

The descriptions of terms given in a contemporary language dictionary ([Oxford English Dictionary 1989](#Oxford89)) indicate that the scope of these terms is not the same.[<sup>4</sup>](#note4)<a name="note4back"></a> For instance, the words _capability_ and _capacity_ clearly include not only cognitive attributes, but also non-cognitive characteristics, such as character and awareness. All other terms - _i.e._, _ability_, _competence_, _competency_, _expertise_, _fluency_, _empowerment_, _know-how_, _knowledge_, _literacy_, _mastery_, _proficiency_ and _skills_ - have narrower meaning. They express different aspects (_e.g._, ability, knowledge and skill) or levels of cognitive capacities (_e.g.,_ expert knowledge, great knowledge) and do not include non-cognitive attributes. The dictionary's definitions do not reveal the actual differences, interrelationships and/or hierarchy among the terms in the latter group. Almost all definitions are based on different combinations of four keywords: _knowledge_, _skills_, _ability_ and _understanding_. However, the explanations of these keywords are recursive - _i.e._, the _ability_ is a 'level of skill'; _skills_ are 'the ability to do'; _knowledge_ is 'understanding and skills'; to _understand_ is 'to know'.

The research literature employs different terms. Some scholars examine various concepts in detail ([Lonsdale & McCurry 2004](#Lonsdale04); [NRC 1999](#NRC99); [Virkus 2003](#Virkus03); [Williams 2003](#Williams03)), others provide only brief definitions ([Kearns 2001](#Kearns01)), with many providing no descriptions at all. However, even the most exhaustive research in this domain usually focuses on the difficulties that are encountered in understanding various concepts, rather than providing exact definitions of terms. For instance, Lonsdale & McCurry ([2004](#Lonsdale04)) investigate current understandings of literacy. They describe various aspects of literacy, but finally conclude that: 'there is no universal definition of literacy' ([Lonsdale & McCurry 2004](#Lonsdale04): 12). Virkus ([2003](#Virkus03)) examines semantic differences between definitions of various terms used in European research papers and policy documents. She concludes that many publications do not adequately define the exact nature of the concepts; different words are used interchangeably; and that it is difficult to find out the conceptual hierarchy between various terms.

Nevertheless, some research and policy papers provide quite exact and extensive definitions of terms. However, given descriptions sometimes depart from the standard meanings of the terms given in language dictionaries. For example, the Australian Commonwealth proposal for the development of an _ICT Competency Framework for Teachers_ ([CDEST 2003](#CDEST03)) uses the term _competence_, which they define as: 'the ability to combine and apply relevant attributes to particular tasks in particular contexts. These attributes include high levels of knowledge, values, skills, personal dispositions, sensitivities and capabilities, and the ability to put these into practice in an appropriate way' ([CDEST 2003](#CDEST03): 3). This definition is far more than simply 'the ability' or 'a skill need in a particular job', as it is defined in the Oxford English Dictionary ([1989](#Oxford89)). It includes non-cognitive culturally-situated aspects of human behaviour, such as values, personal dispositions and even sensitivities.

Research justifies this divergence of the meanings of terms from their established definitions in the dictionaries ([Lonsdale & McCurry 2004](#Lonsdale04); [Williams 2003](#Williams03)). It has been revealed that together with technological and social developments, the notions of terms that are used for the description of human attributes change as well. As Leu, _et al._ ([2004](#Leu04)) states, 'Literacy may be thought as a moving target, continually changing its meaning depending on what society expects literate individuals to do. As societal expectations for literacy change, so too must definitions of literacy change to reflect this moving target.' ([Leu, _et al._ 2004: 1580](#Leu04)). Therefore, the definitions and conceptions of different words are not only the rational outcome of language science, but also natural social phenomenon. Thus, the same terms that in the past included only knowledge and technical skills now may include a complex range of cognitive and other human attributes.

In the recent international and national ICT policy papers three terms - _skills_, _competence_ and _literacy_ - clearly dominate. _Skills_ and _competence_ are particularly common in the industry, adult education and professional training sectors ([CDEST 2003](#CDEST03); [ECDL 2004a](#ECDL04a); [Kearns 2001](#Kearns01); [QCA 2005](#QCA05)). However, both these terms have been heavily criticised by the proponents of the constructivist metacognitive approach in ICT education ([Lambrecht 1999](#Lambrecht99); [Phelps & Ellis 2002](#Phelps02)) on the basis that skills or competency-based training, which has been broadly implemented throughout the world in the area of ICT (_e.g._, [ECDL 2004a](#ECDL04a)), is based on step-by-step systematic model of instruction. It emphasizes the achievement of clearly pre-specified and measurable skill levels rather than a broad conceptual understanding of ICT features and its potential in various domains.

In contrast, the term _literacy_ has become increasingly more accepted, particularly in the sector of formal education (_e.g._, [ETS 2002](#ETS02), [2003](#ETS03); [Lennon, _et al._ 2003](#Lennon03); [Martin 2000](#Martin00)). Some scholars also criticise this term as being too narrow ([McMillan 1996](#McMillan96); [NRC 1999](#NRC99)), but other scholars disagree with this view citing evidence that technical and functional skills are only a part of contemporary notions of literacy ([Kellner 2000](#Kellner00); [Lonsdale & McCurry 2004](#Lonsdale04); [Williams 2003](#Williams03)). Williams ([2003](#Williams03)) in particular advocates the relevance of the term literacy for the definition of the key ICT-related capacities. She argues that the present concept of literacy is broad and open enough to include a full range of cognitive and non-cognitive human attributes. In addition, contemporary notion of literacy maintains its established semantic of basic human right, universality and public accessibility (_e.g._, see [Horton 2003](#Horton03)). These features are essential attributes of formal education. This democratic nature of _literacy_ provides an additional rationale for the acceptance of the term _ICT literacy_ in research and policy discussions.

### Definition of ICT literacy

Several scholars have researched key principles for the conceptualization and definition of new literacies ([Leu, _et al._ 2004](#Leu04); [Lonsdale & McCurry 2004](#Lonsdale04)). Lonsdale & McCurry's ([2004](#Lonsdale04)) recommendations for defining literacy can be used as a starting point for the description of _ICT literacy_. They state that: 'Any definition of literacy adopted must be broad enough to encompass the multiple literacies that exist without being either so broad as to be meaningless, or indistinguishable from educational outcomes in general' ([Lonsdale & McCurry 2004](#Lonsdale04): 39). They also suggest that it is beneficial to see literacy as a set of tools facilitating an individual's participation in society. They propose that literacy must be understood not as a goal of education, but rather as means to enable learning and participation in society.

Using Lonsdale and McCurry's ([2004](#Lonsdale04)) approach, ICT literacy can be defined as a _transferable set of capacities related to ICT use_. In this way, the meaning of _literacy_ is broad enough to include all necessary cognitive and non-cognitive qualities, nevertheless it also implies that these are only basic human qualities. _ICT literacy_ is essential, but only one of many other human qualities and educational targets. In this definition, the word 'ICT' distinguishes ICT-related capacities from a broader set of capacities - such as _generic skills_ ([Kearns 2001](#Kearns01)) or _21st century literacies_ ([Partnership for 21st Century Skills 2002](#Partnership02)) - that describe the full array of human attributes necessary for current and future life in society. The word 'transferable' points out that ICT literacy is not a goal _per se_ and is not limited to specific context or content. Rather it is a universal or generic tool, which could be applied for a variety of purposes and in a diverse array of situations. The word 'set' indicates that ICT literacy itself is not homogenous. It is made up of other component capacities. The term 'capacity' includes not only knowledge and instrumental abilities, but also personal and interpersonal attributes, as well as capabilities to apply them in specific contexts. The words 'related to ICT use' again stress that the capacities, which are included into the notion of ICT literacy, are connected to ICT. It distinguishes ICT literacy from other literacies and capacities that could be enhanced with ICT, but also could be mastered without it.

In this definition, a certain hierarchy between terms is admitted. _Literacy_ is the most general term, which covers a rage of interrelated qualities. _Capacity_ is a more specific term, which describes a distinct aspect of literacy (_e.g._, ICT-related capacity and general cognitive capacity). _Capability_ is a more narrow term than capacity. It describes the qualities (_e.g._, knowledge and skill) necessary to do something specific (_e.g._, plan, access, manage and create). Whereas, _knowledge_ and _skills_, related to rote or mechanical learning, are regarded as the most narrow terms.

## Theoretical framework: dimensions and perspectives of ICT literacy analysis

Theoretical models, applied in general educational studies ([Robitaille & Maxwell 1996](#Robitaille96)) and in studies on ICT use in education ([Law, _et al._ 2000](#Law00)), traditionally distinguish three dimensions of analysis: (1) intended; (2) implemented and (3) achieved. The intended dimension refers to the learning goals or objectives of education or ICT use in education. It is generally described in terms of the achievement targets and strategic directions defined in policy documents at a national, regional or school system level. The implemented dimension refers to the educational processes happening at the school and/or classroom level. It is usually described in terms of the learning opportunities offered to students and depends on curricula, educational standards, assessment and other implemented structural arrangements of schooling. The achieved dimension refers to the learning outcomes achieved by students as a result of their learning experiences. It is usually described in terms of the main capabilities or other more specific qualities that students are expected to demonstrate as a result of successful learning

In order to get a comprehensive understanding of ICT literacy policies and practices in specific contexts, all three dimensions should be investigated (Figure 1). In the intended dimension, ICT literacy is both a part of policy for ICT introduction into education and a part of general literacy policy. These two perspectives can be used as a starting point for the analysis of ICT literacy policies. In the implemented dimension, ICT literacy is a part of ICT development at a school. Models of ICT development reflect approaches to the teaching and learning of ICT literacy. Thus, they can be adapted and used for the analysis of curricula, standards, assessment and other practical arrangements for ICT literacy enhancement. In the achieved dimension, expected and observable students' ICT literacy capabilities and learning experiences reveal learning outcomes. Key perspectives regarding ICT literacy key elements and outcomes can be employed for the analysis of students' ICT literacy learning results. Taken together, these three dimensions reveal the links between (1) the initial goals of ICT literacy development; (2) teaching and learning practices at a school and (3) students' expected learning outcomes.

<div align="center">![figure 1](p252fig1.gif)</div>

<div align="center">  
**Figure 1: The main analytical dimensions and perspectives of ICT literacy**</div>

## Intended ICT literacy

### ICT literacy as a part of ICT in education policy

The emergence of the phenomenon of ICT literacy is tightly linked with general rationales for the inclusion of ICT into education. Research literature and policy documents state a variety of reasons for the introduction of ICT into education. Motives can be grouped into three broad categories: (1) economic (or vocational, professional); (2) social (or public, cultural, personal) and (3) educational (or pedagogical, cognitional) ([Kearns 2002](#Kearns02b); [Markauskaite 2005a](#Markauskaite05a); [NRC 1999](#NRC99); [OECD 2001](#OECD01)).

The economic rationale recognises the increasingly common and dominant place of ICT in the current and future economy ([NRC 1999](#NRC99); [OECD 2001](#OECD01)). It focuses on the needs to have personnel with appropriate ICT skills. This rationale includes various categories of the workforce in all areas of employment: specialists in the ICT industry; professionals with a high level of ICT competence in non-ICT areas of the economy that rely heavily on ICT; and the rest of the workforce. According to this rationale, current and future employees in each of these vocational categories should develop a certain level of ICT literacy, which is necessary to perform effectively their job tasks.

The social rationale recognises the increasing importance of ICT in our everyday social, political, and cultural life ([OECD 2001](#OECD01); [Tiffin & Rajasingham 1995](#Tiffin95)). ICT applications (_e.g._, e-mail, access to on-line information) help to organize and improve the quality of private life. Commercial (e-banking, e-commerce, _etc_.) and social (e-government, e-health, _etc_.) e-services enhance the way we live. Public information, policy debates and democratic activities have been moving away from print, one-way broadcasting media and face-to-face participation to activities in an interactive on-line environment. Traditional cultural life and emerging new cultural activities (_e.g._, e-communities) have become increasingly associated with the new tools and activities in an on-line world. Thus, ICT literacy has become an essential life skill and a human right. The social rationale focuses on the need to provide all individuals with the capacities to use ICT, which would enable full participation in society. For this reason, ICT literacy includes not only familiarity with ICT tools, processes and services, but also an understanding of ICT-related public issues that allow informed judgements to be made (_e.g._, copyright, electronic media, security, data privacy), and also many non-cognitive capacities (_e.g._, willingness to use ICT).

The educational rationale recognises the present and future roles of ICT in learning and teaching ([Davis & Carlsen 2004](#Davis04); [NRC 1999](#NRC99); [OECD 2001](#OECD01)). It covers various educational imperatives, such as to increase educational attainment, to promote educational renewal and to increase access to education ([Davis & Carlsen 2004](#Davis04)). The educational rationale is related to both traditional behaviourist ([Burton, _et al._ 2004](#Burton04)) and modern constructivist pedagogical approaches ([Cobb 1996](#Cobb96)). Research reviews evidence that various ICT-based delivery options offer different educational opportunities for formal education ([Davis & Carlsen 2004](#Davis04); [Plomp & Ely 1996](#Plomp96); [Wallace 2003](#Wallace03)). For instance, the introduction of electronic textbooks and other digital tools could improve the efficiency of traditional teaching and learning.[<sup>5</sup>](#note5)<a name="note5back"></a> Access to new ICT tools and resources, such as CD-ROMs and on-line databases, modelling environments and other tools of computer-mediated learning, could change and even totally reshape present instruction from behaviourist teacher-centred teaching to constructivist student-centred learning. The provision of ICT tools which are necessary for the development of information capabilities, higher-order thinking, communication, collaboration, and other capabilities could change learning goals and provide possibilities to develop new essential capabilities. New communication tools, methods and services, such as virtual learning environments, personalized learning and other formal and informal forms of e-learning, could also enhance opportunities to access education and to move the learning and teaching processes from classrooms into a real world ([Davis & Carlsen 2004](#Davis04); [Wallace 2003](#Wallace03)). The educational rationale focuses on the need for each citizen to become a powerful lifelong learner and e-learner. From this point of view, ICT literacy is a set of capacities that enables ICT to be used for various learning activities. This ranges from the simple abilities to apply ICT as a tool or medium for learning to ICT-related metacognitive capacities and dispositions about lifelong learning.

There is some overlap between all three rationales. With development and convergence of technologies, the convergence between various approaches to ICT in education has been also increasing. For instance, the OECD ([2001](#OECD01)) report notices that education is no longer limited to a period of formal schooling, but rather is a lifelong experience. Lifelong learning capacities and other capabilities of ICT literacy acquired through the educational use of ICT are now needed in the workplace, so they become a part of the economic rationale. Nonetheless, majority of policies for ICT implementation into education are underpinned by one or two dominant rationales ([Kearns 2002](#Kearns02b); [Markauskaite & Dagiene 2004](#Markauskaite04); [Markauskaite 2005a](#Markauskaite05a)). Different rationales imply different emphases and consequently influence the way ICT literacy is understood and enhanced. Table 1 summarises how various rationales for the introduction of ICT into education predetermine the aims of ICT literacy enhancement.

<a name="Table1"></a>

<table border="1" cellspacing="0" cellpadding="3" style="background-color: #FDFFDD; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: smaller; font-style: normal; border: solid;" align="center"><caption align="bottom">  
**Table 1: The main rationales for ICT inclusion and their implications for the aims of ICT literacy ([Markauskaite & Dagiene 2004](#Markauskaite04); [Markauskaite 2005a](#Markauskaite05a))**</caption>

<tbody>

<tr>

<th rowspan="1"></th>

<th colspan="3">Rationales for ICT inclusion into education</th>

</tr>

<tr>

<th></th>

<th>Economic</th>

<th>Social</th>

<th>Educational</th>

</tr>

<tr>

<td align="left">**The main aims of ICT literacy**</td>

<td align="left">

*   Development of a skilled workforce for ICT industry
*   Development of professionals with extensive ICT proficiency for ICT-intensive domains in the economy
*   Development of an ICT literate general workforce for all other domains in the economy

</td>

<td align="left">

*   Learning to apply ICT in everyday life
*   Learning to use commercial e-services
*   Learning to use social and public e-services
*   Development of capacities to be involved in on-line public policy and democracy processes
*   Development of capacities to participate in cultural and community life

</td>

<td>

*   Learning to apply ICT tools for traditional learning
*   Learning to use ICT for the achievement of new educational goals
*   Enhancement of new personal and interpersonal capacities (critical thinking, collaboration skills, _etc_.)
*   Development of abilities to access and use new education services
*   Enhancement of lifelong learning capacities

</td>

</tr>

</tbody>

</table>

### ICT literacy as a part of literacy policy

ICT literacy is also 'a literacy'. As Williams ([2003](#Williams03)) suggests, the phenomenon of ICT literacy can be studied in the context of various schools of thought concerning general literacy. A lot of theoretical and empirical researches on various aspects of literacies have been conducted already in the past ([Collins 1995](#Collins95)). Recently, a number of new literacies have been identified and conceptualised and new categorizations of literacies have been proposed (_e.g._, see [Leu, _et al._ 2004](#Leu04); [Lonsdale & McCurry 2004](#Lonsdale04); [Williams 2003](#Williams03)). However, as Lonsdale and McCurry ([2004](#Lonsdale04)) argue, not all new theories have added much value to existing conceptualizations of literacies. Their research review suggests that the most explicit and comprehensive theoretical framework for the investigation of literacies includes two partly interrelated binary taxonomies. The first taxonomy differentiates the purpose of the literacy according to who benefits from literacy: the individual or society. The second taxonomy, which is based on Street's ([1984](#Street84)) work, sets apart various conceptual and practical aspects of literacy enhancement into two models: autonomous and ideological.

In the first taxonomy, the benefits of the literacy to an individual or society can be associated with various economic, social and educational aspects. For instance, the economic benefits for an individual could include improved employment opportunities, increased personal income, and other private economic advantages; the social benefits - greater confidence and capabilities to improve personal life; educational benefits - possession of skills required to access further education. The economic benefits for society could would result from the increased competitiveness and productivity of a community and other public economic achievements; social benefits - improved social and cultural cohesion and increased capability of communities for the transformation of society; educational benefits - improved capacity for lifelong learning and fundamental the redesign of education.

The parallelism between general literacies and ICT literacy is obvious. The gains from ICT literacy could be also associated with benefits either for an individual or for society. In addition, these gains could be related to similar economic, social and educational objectives (Table 2).

<a name="Table2"></a>

<table border="1" cellspacing="0" cellpadding="3" style="background-color: #FDFFDD; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: smaller; font-style: normal; border: solid;" align="center"><caption align="bottom">  
**Table 2: Relationships between rationales for the use of ICT education and purposes of ICT literacy**</caption>

<tbody>

<tr>

<th rowspan="2">Rationales of ICT use</th>

<th colspan="2">Purposes of ICT literacy</th>

</tr>

<tr>

<th>Benefits to individual</th>

<th>Benefits to society</th>

</tr>

<tr>

<td align="left">**Economic**  
</td>

<td align="left">ICT literacy improves employment opportunities, personal income, _etc_.  
</td>

<td align="left">ICT literacy increases the competitiveness and productivity of a community, _etc_.  
</td>

</tr>

<tr>

<td align="left">**Social**  
</td>

<td align="left">ICT literacy allows individuals to benefit from new economic and public services, improves confidence, enhances one's capacity to question social norms, _etc_.  
</td>

<td align="left">ICT literacy builds capacity, which enables transformations of society, improves social cohesion, _etc_.  
</td>

</tr>

<tr>

<td align="left">**Educational**  
</td>

<td align="left">ICT literacy improves individual outcomes, enables access to further education, _etc_.  
</td>

<td align="left">ICT literacy transforms education, builds learning communities, integrates learning into social context, promotes diverse pathways, lifelong learning, _etc_.  
</td>

</tr>

</tbody>

</table>

Different purposes of ICT literacy are related to different teaching and learning practices. Autonomous and ideological models provide comprehensive classifications of the main conceptual and practical features of literacy ([Street 1984](#Street84)).

The autonomous model conceptualises literacy from a behaviourist perspective. According to this model, literacy is primarily an individual attribute and intellectual ability. It includes a standard uniform set of abilities and it can be measured by standard psychometric tests. The learning of literacy is underpinned by dominant ideologies and separated from the context in which it will be applied. The main purpose of literacy teaching and learning is the development of human capital, which will enhance the economic productivity of the nation. In the autonomous model, ICT literacy is an important component of professional development. As technologies rapidly change, workers must undergo continual re-skilling in order to maintain a high level of professional knowledge in ICT.

The ideological model conceptualises literacy from a socio-cultural cognitive learning perspective. Literacy is seen as a social practice, which is multifaceted and learner-centred. Literacy includes a diverse range of knowledge, skills and understandings, which are specific for each individual. There is no universal definition of literacy, thus its assessment is based on ethnographic approaches. Literacy is underpinned by critical thinking and the ability to challenge dominant ideologies. All literacy practices are integrated with a social context. Thus, what it means to be literate depends on each individual and her or his specific context. The objectives of literacy are holistic. They are not limited to individual and/or vocational outcomes, but also include capacity building for communities. In the ideological model, ICT is an integral part of all literacy practices. Thus, ICT literacy is an aspect of multiliteracy.

For the classification of ICT literacy concepts a majority of scholars use more detailed taxonomies ([Anderson & Weert 2002](#Anderson02); [Martin 2000](#Martin00); [Williams 2003](#Williams03)). For instance, Martin ([2000](#Martin00)) applies a three stage model and identifies the following approaches: (1) mastery, which puts an emphasis on learning how computers work and how to program them; (2) application, which focuses on the practical knowledge needed to operate ICT and (3) reflective, which emphasizes integrated use of ICT and reflective aspects of learning. Nevertheless, the parallelism between various taxonomies specifically tailored to ICT literacy and Street's ([1984](#Street84)) general models of literacy is evident. For example, the mastery and application approaches in Martin's ([2000](#Martin00)) taxonomy correspond to the autonomous model, while the reflective stage corresponds to the ideological model. Thus the same autonomous vs. ideological thinking could be adapted for the classification of conceptual approaches to ICT literacy. Table 3 summarizes the main features of these two ICT literacy models.

<a name="Table3"></a>

<table border="1" cellspacing="0" cellpadding="3" style="background-color: #FDFFDD; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: smaller; font-style: normal; border: solid;" align="center"><caption align="bottom">  
**Table 3: The main aspects of the autonomous and ideological models of ICT literacy ([Markauskaite 2005a](#Markauskaite05a))**</caption>

<tbody>

<tr>

<th>Attributes of ICT literacy</th>

<th>Autonomous model</th>

<th>Ideological model</th>

</tr>

<tr>

<td align="left">**1\. Objective of ICT literacy**</td>

<td align="left">ICT-skilled workforce</td>

<td align="left">Cohesive and inclusive knowledge society</td>

</tr>

<tr>

<td align="left">**2\. Structure of ICT capacities**</td>

<td align="left">ICT-related technical knowledge and skills</td>

<td align="left">Integrated problem-solving and ICT capabilities, metacognitive capacities and non-cognitive attributes (_e.g._, self-management, effectiveness in teamwork)</td>

</tr>

<tr>

<td align="left">**3\. Standards of ICT literacy**</td>

<td align="left">Uniform set of knowledge and skills</td>

<td align="left">Tailored to individual needs and contexts (_e.g._, different for various groups of society)</td>

</tr>

<tr>

<td align="left">**4\. Structure of ICT curriculum**</td>

<td align="left">The same for all</td>

<td align="left">Flexible, structured into stand-alone identifiable units, easily customized to individual needs</td>

</tr>

<tr>

<td align="left">**5\. Learning methods for learning ICT literacy**</td>

<td align="left">Separate key learning area</td>

<td align="left">Integrated with other literacies (cross-curricular learning)</td>

</tr>

<tr>

<td align="left">**6\. Role of teachers and students**</td>

<td align="left">Teacher-lead and teacher-centred learning</td>

<td align="left">Self-managed student-centred ICT literacy enhancement (_e.g._, supported by Managed Learning Environments, other technical and human self-managed learning scaffolders)</td>

</tr>

<tr>

<td align="left">**7\. Learning setting**</td>

<td align="left">Computer lab-based learning</td>

<td align="left">Multiple learning settings and environments</td>

</tr>

<tr>

<td align="left">**8\. Assessment of ICT literacy**</td>

<td align="left">Based on psychometric approaches (_e.g._, multiple-choice tests)</td>

<td align="left">Based on ethnographic approaches (_e.g._, e-portfolio, progress tracking using managed learning environments)</td>

</tr>

</tbody>

</table>

In the autonomous model of ICT literacy, ICT-related knowledge and skills are regarded as a new kind of intellectual thinking and the main precondition for human productivity. The primary emphasis is placed on gaining fundamental knowledge and functional ICT skills. Competence-based ICT literacy training is provided during separate ICT courses. The curriculum mainly covers isolated ICT topics, such as theoretical principles of ICT, word processing, the Internet, the social and economic effects of computers. The training is based on teacher-centred laboratory-based activities. Universal computer literacy standards are applied for all individuals. ICT skills certification, based on multiple-choice tests, flourishes at this stage.

In the ideological model, the emphasis is on a holistic understanding of ICT literacy and integrated learning practices. The notion of ICT literacy extends beyond the mastery and application of ICT. Functional ICT capabilities are primarily merged with cognitive problem-solving capacities (_e.g._, information literacy). Different reflective capacities (_e.g._, self-managed, lifelong learning) and non-cognitive attributes (_e.g._, effectiveness in teamwork, awareness of social and ethical issues) are integral components of ICT literacy. The enhancement of ICT literacy is integrated into various subjects and the overall teaching and learning process. ICT is primarily a medium for learner-centred personalized learning and individual mentoring and assessment. The process of learning is supported by virtual and managed learning environments. Thus the learning and teaching of ICT literacy are naturally integrated with other literacies and infused into broader learning and social contexts.

## Implemented ICT literacy

The infusion of ICT into education is a long-term and complex process. Research reveals that the evolution of ICT in society and at a school has certain clearly distinguishable regularities ([Apple Computer Inc 1995](#Apple95); [Weert 1995](#Weert95)). For example, one of the classical models proposed by Weert ([1995](#Weert95)) identifies three main stages of ICT development in society and education: (1) automatisation, when ICT improves the effectiveness of existing processes; (2) information, when ICT allows people to accomplish more complex tasks, changes human roles and expands their productivity and (3) communication, when ICT changes both processes and the human role in these processes and consequently creates completely new more dynamic organizational structures. Many other models of ICT introduction have been proposed (_e.g._, [Anderson & Weert 2002](#Anderson02); [Langhorne, _et al._ 1989](#Langhorne89); [Ridgeway & Passey 1995](#Ridgeway95); [Weert 1995](#Weert95)). They identify different stages of progress and indicate different key factors that characterize each stage. However, many of the differences between various models are minor. Typically, ICT is initially introduced as a separate curriculum area, later ICT is applied for other curricula areas and new learning purposes, while finally it becomes invisibly infused into all aspects of education.

The understanding of ICT literacy and approaches to its teaching and learning progress together with the evolution of ICT at a school. Anderson's and Weert's ([2002](#Anderson02)) model of ICT development clearly reflects this reciprocal relationship. The model consists of two parallel axes: (1) approaches to teaching and learning _with_ and _through_ ICT and (2) stages of ICT development in a school (Figure 2).

<div align="center">![](p252fig2.gif)</div>

<div align="center">  
**Figure 2: Relationship between ICT development at a school and approaches to ICT-related teaching and learning. Based on Anderson and Weert ([2002](#Anderson02))**</div>

Axis one describes four approaches to ICT-related teaching and learning: (1) discovering ICT tools - ICT skills are taught as a separate subject; (2) learning how to use ICT tools - the development of ICT skills is integrated into separate subjects; (3) understanding how and when to use ICT tools to achieve particular purposes - ICT is embedded across curricula; and (4) specializing in the use of ICT tools - ICT is learned in specialized subjects or professional courses. These approaches are parallel to another axis, which describes the four stages of ICT development in a school: (1) emerging; (2) applying; (3) infusing and (4) transforming. Each stage is characterized by eight indicators: (1) vision; (2) learning pedagogy; (3) development plans and policies; (4) facilities and resources; (5) understanding of curriculum; (6) professional development for school staff; (7) community and (8) assessment. Some of these indicators - pedagogy, curriculum and assessment - directly characterize approaches to teaching and learning of ICT literacy. Whereas others - vision, facilities and resources, _etc_. - describe background aspects of ICT literacy enhancement. Therefore, the general model of ICT development at a school can be adapted and applied for the analysis of the notion of ICT literacy from the implemented perspective. Table 4 summarizes the main features of implemented ICT literacy at various stages of development.

<a name="Table4"></a>

<table border="1" cellspacing="0" cellpadding="3" style="background-color: #FDFFDD; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: smaller; font-style: normal; border: solid;" align="center"><caption align="bottom">  
**Table 4: Progress in implementing ICT at a school and indicators that describe ICT literacy. Based on Anderson and Weert ([2002](#Anderson02))**</caption>

<tbody>

<tr>

<th rowspan="2">Stages of ICT development and approaches to ICT-related teaching and learning</th>

<th colspan="3">Indicators of ICT development in a school</th>

</tr>

<tr>

<th>Learning and teaching pedagogy</th>

<th>Understanding of curriculum</th>

<th>Assessment</th>

</tr>

<tr>

<td align="left">**Emerging**  
Discovering ICT tools</td>

<td align="left">Teacher-centred pedagogy. Focus on knowledge and skills of ICT.</td>

<td align="left">Students' ICT literacy is developed during special ICT lessons. Target: to teach students to understand and use hardware and software.</td>

<td align="left">ICT capabilities are assessed separately from students' capacities in other domains. Assessment is a responsibility of the ICT subject teacher. Mainly knowledge and technical skills are assessed. Multiple-choice questions and other standard test techniques are used.</td>

</tr>

<tr>

<td align="left">**Applying**  
Learning how to use ICT</td>

<td align="left">Teacher-centred pedagogy. Focus on fundamental knowledge and skills to apply ICT in discrete areas.</td>

<td align="left">ICT is applied within discrete subjects in artificial isolated subject contexts.</td>

<td align="left">Assessment focuses on skills and abilities to perform tasks. ICT literacy is assessed separately and as a part of other subjects. Assessment is a responsibility of isolated teachers.</td>

</tr>

<tr>

<td align="left">**Infusing**  
Understanding how and when to use ICT to achieve particular purposes</td>

<td align="left">Student-centred pedagogy. Focus on collaboration and communication, use of different information sources and application of ICT for various standard purposes.</td>

<td align="left">Curriculum is organized on a problem-based authentic basis. ICT and different subjects are integrated. ICT (and ICT literacy) is a tool used for accomplishment of various authentic tasks. Projects and other resource-based learning methods are dominant.</td>

<td align="left">Evaluation is integrated and moderated across subject areas. Assessment focuses on attainments in subject domains. Portfolios and multiple media are used to demonstrate attainments. ICT literacy includes technical, cognitive, social and ethical aspects. Evaluation is the responsibility of the student.</td>

</tr>

<tr>

<td align="left">**Transforming**  
Specializing in the use of ICT tools</td>

<td align="left">Student-centred pedagogy. Focus is on active experimental learning, critical thinking and decision-making capabilities. ICT is applied for individualization of learning and a range of other purposes.</td>

<td align="left">Curriculum is tailored to each student's individual needs. Blended learning environments and learning management systems are used in the teaching and learning process. ICT literacy is enhanced and applied while accomplishing various learning tasks.</td>

<td align="left">Continuous holistic evaluation. Learner oriented, open-ended, project-based, peer-mediated evaluation approaches are used. ICT literacy is a part of multiliteracy. Various communities are involved in the assessment.</td>

</tr>

</tbody>

</table>

## Achieved ICT literacy

There are several generic taxonomies that can be applied for the categorization of ICT literacy in the achieved dimension ([Ba, _et al_. 2002](#Ba02); [Bigum & Green 1992](#Bigum92); [Corbel & Gruba 2004](#Corbel04); [Lankshear & Knobel 1997](#Lankshear97)). Various taxonomies classify theoretical and practical perspectives according to different criteria. For example, Ba, _et al._ ([2002](#Ba02)) classify the definitions of _digital literacy_ according to their conceptual and operational features. They identify four major groups of definitions: _technical_; _generic_; _generic with information technology_ and _problem-based_. Corbel and Gruba ([2004](#Corbel04)) classify perspectives of _computer literacy_ according to their conceptual origin. They identify four categories of perspectives: _skills_; _textual practices_; _socio-political_ and _information_. However, the categorization of present approaches to ICT literacy is a more complicated issue. As Lankshear and Knobel ([1997](#Lankshear97)) point out: 'The question of what counts as "technological literacies" is complex.' ([Lankshear & Knobel 1997](#Lankshear97): 139). Bigum and Green ([1992](#Bigum92)) argue that _technology_ and _literacy_ can be related to each other in four different ways: technology _for_ literacy; literacy _for_ technology; literacy _as_ technology; and technology _as_ literacy. Lankshear and Knobel ([1997](#Lankshear97)) go deeper arguing that each of these four technology-related literacies have at least three qualitative dimensions: _functional literacy_; _cultural literacy_ and _critical literacy_. The complication is even more profound as _functional_, _cultural_ and _critical_ literacies are themselves not homogenous and have many conceptual and practical variants. Nevertheless, Lankshear and Knobel ([1997](#Lankshear97)) have shown that the complexity of _technological literacy_ operationalization can be reduced by selecting a concrete analytical approach (_e.g._, traditional literacies). Having established conceptual restraints, it is possible to identify common categories and construct relevant taxonomies.

In recent years, proposed operational definitions of ICT literacy (_e.g._, [ETS 2002](#ETS02), [2003](#ETS03); [Lennon, _et al._ 2002](#Lennon03)) and other ICT-related generic literacies (_e.g._, [NCREL 2003](#NCREL03)) reflect the complexity of technological literacy taxonomisation. The new definitions of ICT literacy integrate more than one conceptual perspective and describe expected learning outcomes as sets of many interrelated capabilities. For the purposes of this paper, the notion of ICT literacy can be constrained to the definition proposed in the first section, _i.e._: 'ICT literacy is a transferable set of capacities related to ICT use'. From this technological viewpoint, six broad perspectives regarding the key elements and outcomes of ICT literacy can be identified: (1) fundamental ICT knowledge perspective; (2) basic ICT skills perspective; (3) cognitive capabilities perspective; (4) inter-literacy perspective; (5) situated literacy perspective and (6) metacognitive perspective (Table 5). Some of these perspectives have a strong theoretical base, whereas others have a wide-ranging practical presence.

<a name="Table5"></a>

<table border="1" cellspacing="0" cellpadding="3" style="background-color: #FDFFDD; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: smaller; font-style: normal; border: solid;" align="center"><caption align="bottom">  
**Table 5: The main theoretical perspectives of the key elements and outputs of ICT literacy**</caption>

<tbody>

<tr>

<th>Theoretical perspective</th>

<th>Examples of the main aspects and output categories</th>

</tr>

<tr>

<td align="left">**1\. Fundamental ICT knowledge perspective**</td>

<td align="left">_Knowledge of ICT concepts and understanding of underlying theoretical principles._  
_E.g._: computers; information systems; networks; digital representation of information; information organization; modelling and abstraction; algorithmic thinking and programming; universality, limitations of information technology; societal impact ([NRC 1999](#NRC99)).</td>

</tr>

<tr>

<td align="left">**2\. Basic ICT skills perspective**</td>

<td align="left">_Understanding and ability to use ICT tools._  
_E.g._: concepts of ICT; computer; word processing; spreadsheets; database; presentations; information and communication ([ECDL 2002](#ECDL02)).</td>

</tr>

<tr>

<td align="left">**3\. Cognitive capabilities perspective**</td>

<td align="left">_1) Cognitive capabilities to use ICT in a problem-solving process._  
_E.g._: capability to access; manage; integrate; evaluate; create; communicate ([ETS 2002](#ETS02)).  

_2) Generic understanding and capabilities to use ICT._  
_E.g._: basic operations and concepts; social, ethical and human issues; productivity tools; communication tools; research tools; problem-solving and decision making tools ([ISTE 1998](#ISTE98)).</td>

</tr>

<tr>

<td align="left">**4\. Inter-literacy perspective**</td>

<td align="left">_Capabilities to use ICT as an integral part of basic literacies, core curricula subjects and new literacies._  
_E.g._, _Digital-Age Literacies_: basic literacy; scientific literacy; economic literacy; technological literacy; visual literacy; information literacy; multicultural literacy and global awareness ([NCREL 2003](#NCREL03)).</td>

</tr>

<tr>

<td align="left">**5\. Situated literacy perspective**</td>

<td align="left">_Critical capabilities to use ICT in various contexts._  
_E.g._: citizen and community; economic activity; domestic and everyday life; leisure; education and training ([QCA 2005](#QCA05)).</td>

</tr>

<tr>

<td align="left">**6\. Metacognitive capabilities perspective**</td>

<td align="left">_1) Cognitive self-appraisal during ICT-related activities._  
_E.g._, _Inventive Thinking_: adaptability, managing complexity and self-direction; curiosity, creativity and risk taking; higher-order thinking and sound reasoning ([NCREL 2003](#NCREL03)).  

_2) Cognitive self-management during ICT use._  
_E.g._, _High Productivity_: prioritizing, planning and managing for results; effective use of real world tools; ability to produce relevant high-quality products ([NCREL 2003](#NCREL03)).</td>

</tr>

</tbody>

</table>

The fundamental ICT knowledge and ICT skills perspectives primarily focus on isolated core ICT capabilities. According to these two perspectives: 'computers add several new grammars to the list of things that individuals must to learn before they become successfully literate in a computer-supported environment.' ([Cesarini 2004](#Cesarini04)). In contrast, the other four perspectives interpret ICT literacy in an integrated way.

The fundamental ICT knowledge perspective has its roots in computer scientists' theoretical views of ICT literacy ([Ershov 1988](#Ershov88); [NRC 1999](#NRC99)). This perspective maintains that students should learn key ICT concepts and theoretical principles, such as the basic principles of ICT, logic, data structures, programming and ICT-related social knowledge. It is argued that: 'Concepts are raw material for understanding new information technology as it evolves.' ([NRC 1999](#NRC99)). However, this theoretical approach is not common in current ICT literacy learning and assessment practices. As Martin ([2000](#Martin00)) argues: 'Whilst thinking skills can be seen as a valuable complement to familiarity with applications, the need for an elementary grounding in computer science may be difficult to understand.' ([Martin 2000](#Martin00): 7).

The basic ICT skills perspective has a more empirical, rather than philosophical, conceptual base ([Martin 2000](#Martin00); [NRC 1999](#NRC99)) and a strong wide-ranging practical presence in present day ICT literacy teaching and assessment practices (_e.g.,_ see [ECDL 2004a](#ECDL04a)). This perspective focuses on the basic practical skills needed to use computer hardware, software and networks. Basic skills are seen as the requisite foundation from which new more sophisticated capabilities can be developed ([Corbel & Gruba 2004](#Corbel04); [NRC 1999](#NRC99)). The ICT skills are also essential for employment and social readiness ([ECDL 2004a](#ECDL04a)). The main learning outcomes for ICT literacy are related to students' abilities to operate a computer, manage files, use word processors, internet browsers, e-mail and other applications.

The cognitive perspective looks to ICT literacy outcomes from a generic skills angle. This view integrates ICT knowledge and skills with problem-solving and information handling capabilities. For this reason, theoretical works and practical implementations appear in various disciplinary domains, such as ICT (_e.g.,_ [ETS 2002](#ETS02); [ISTE 2002a](#ISTE02a), [2002b](#ISTE02b); [NRC 1999](#NRC99); [QCA 2005](#QCA05)), information literacy (_e.g.,_ [Eisenberg & Johnson 2002](#Eisenberg02)) and interdisciplinary domains (_e.g.,_ [Candy 2003](#Candy03), [2004](#Candy04)). Two structural approaches, described by Ba, _et al._ ([2002](#Ba02)), can be found in this perspective: problem-based (_e.g.,_ [ETS 2002](#ETS02)) and generic with information technology (_e.g.,_ [ISTE 1998](#ISTE98); [NRC 1999](#NRC99)). The key difference is that problem-based approaches structure ICT outcomes according to the key steps of the problem-solving process (_e.g._, ability to access, manage and integrate), whereas the generic approach does not link knowledge or skills to specific problem-solving activity (_e.g._, basic operations and concepts, productivity tools, communication tools, research tools). The cognitive perspective has become common in new ICT literacy assessment frameworks ([Honey, et al. 2005](#Honey05); [Ridgeway, McCusker & Pead 2004](#Ridgeway04)).

The inter-literacy perspective looks to ICT knowledge and skills from the viewpoint of various subject domains. It covers ICT capabilities that are related to three broad groups of other literacies: traditional core literacies (_i.e._, reading, writing and numeracy); other core subject areas (_e.g._, science, arts, economic) and new literacies (_e.g._, global literacy, civic literacy). On a theoretical level, the impact of new texts and visual media on textual practices and understandings is broadly conceptualized in various language literacy studies (_e.g._, [Kress 2000](#Kress00); [Unsworth 2002](#Unsworth02); [Warschauer 1999](#Warschauer99)). Similar theoretical works are available for other traditional subject domains (_e.g._, [Keitel 1994](#Keitel94); [Voogt 1996](#Voogt96)) and newly conceptualized literacies (_e.g._, [Potter 2001](#Potter01)). In teaching and learning practices, the enhancement of subject-related ICT capacities is scattered across the curricula (_e.g._, see [DETE 2001](#DETE01); [ISTE 2002a](#ISTE02a), [2002b](#ISTE02b); [Partnership for 21st Century Skills 2004](#Partnership04)). In assessment practices, students' ICT-related outcomes are consolidated and assessed together from an ICT literacy perspective (_e.g._, see [DETE 2001](#DETE01); [DEYFS 2003](#DEYFS03)).

The situated literacy perspective conceptualizes ICT literacy from a multiliteracies theoretical viewpoint ([Gee 1992](#Gee92); [New London Group 2000](#New00); [Street 2003](#Street03)). Literacy is seen as a social practice that should be acquired in real everyday situations. Thus, the outcomes of literacy learning should enhance students' capacities to use new technologies to meet current and future needs. In the domain of ICT literacy, this perspective has a conceptual foundation in sociocultural theories of ICT use for situated cognition and learning ([Henning 2004](#Henning04); [Lankshear & Knobel 1997](#Lankshear97)). In practice implemented assessments simplify this broad theoretical perspective by linking students' ICT literacy outputs to their capabilities to apply ICT in various relevant real-life contexts, such as education and training, occupation, public life and leisure ([Lennon, _et al_. 2003](#Lennon03); [QCA 2005](#QCA05)).

The metacognitive perspective conceptualizes ICT literacy from a higher-order reflective thinking and self-regulated learning viewpoint ([Biggs 1985](#Biggs85); [Flavell, Miller & Miller,1993](#Flavell93)). According to Zimmerman, _et al._ ([1996](#Zimmerman96)), metacognition is a cognition concerning one's own thinking processes. It has two dimensions: (1) cognitive self-appraisal, which is the capability to reflect on the state of your own knowledge and abilities; and (2) cognitive self-management, which is the capability to plan and implement cognitive strategies and to monitor, adjust and improve your performance. Self-regulated autonomous learning of ICT skills ([Candy 2003](#Candy03), [2004](#Candy04); [Phelps, _et al._ 2001;](#Phelps01) [Phelps & Ellis 2002](#Phelps02)), computer self-efficacy ([Compeau & Higgins 1995](#Compeau95)) and independent coping with ICT problems ([Ropp 1999](#Ropp99)) are critical features of ICT literacy. According to the metacognitive perspective of ICT literacy, these higher-order capacities allow for the transfer of ICT knowledge and skills across different tasks and domains. The metacognitive perspective, however, does not have a strong practical counterpart. In current educational policy papers, metacognitive capacities, such as _inventive thinking_ (_i.e._, self-appraisal) and _high productivity_ (_i.e._, self-management) are usually considered as more generic learning outcomes that are not related just to ICT or any other specific literacy (_e.g._, [Honey, et al. 2005](#Honey05); [NCREL 2003](#NCREL03)).

These six perspectives are not mutually exclusive. Indeed, the majority of recently proposed ICT literacy models incorporate more than one perspective. For example, the American ICT fluency framework covers fundamental ICT knowledge, ICT skills and cognitive capabilities ([NRC 1999](#NRC99)). The Educational Testing Service's ([2002](#ETS03)) ICT proficiency assessment model covers ICT skills and cognitive capabilities and some elements of inter-literacy and situated literacy perspectives. The British Qualifications and Curriculum Authority's ([2005](#QCA05)) ICT skills standards framework combines ICT skills, cognitive capabilities and some elements of the situated learning perspective. Therefore, different combinations of ICT literacy perspectives reveal what ICT-related capabilities students are expected to learn and in which contexts students are expected to demonstrate them.

## Summary, discussion and conclusions

Various terms have been used for the definition of ICT-related capacities and other attributes that are necessary in modern society. This discussion has shown that semantic differences between various terms are not always clear, however different words entail different conceptual understandings of the scope and nature of ICT-related capacities. The term ICT literacy can be used as an umbrella term. This paper defined ICT literacy as a transferable set of capacities related to ICT use. This definition conforms with the essential specifications of literacy definitions - it is broad, resistant to constant technological and social transformations, relevant to various contexts and at the same time distinguishable from other learning outputs.

To have a well-rounded picture of the notion of ICT literacy in a specific context, three dimensions of ICT literacy - intended, implemented and achieved - must be investigated. Each dimension is related to different levels and aspects of an educational system: intended to top-level policy aims and strategic attitudes; implemented to middle-level teaching and learning approaches; achieved to base-level empirical learning activities and to students' experiences and outcomes. Different analytical perspectives and models are relevant for the analysis of ICT literacy within each dimension. Nevertheless, the links and interactions between different analytical perspectives and frameworks that are applied in each dimension are apparent. Figure 3 illustrates some main parallels.

<div align="center">![figure 3](p252fig3.gif)</div>

<div align="center">  
**Figure 3\. The main links between different analytical dimensions of ICT literacy**</div>

First, different economic, social and educational rationales of ICT implementation emphasize different aspects of ICT-related capabilities. There is an obvious connection between various rationales of ICT introduction into education (_i.e._, economic, social and educational) in the intended level and respective cognitive contexts (_i.e._, work, personal and public life, and learning) in the achieved level. The kind of context in which the learning of ICT literacy takes place indirectly determines the range of situations in which students are able to apply ICT and what kind of ICT capacities they learn (_i.e._, to apply ICT for future work, social life or educational purposes). Therefore, the intended aims of ICT literacy enhancement influence the nature of the cognitive contexts in which ICT literacy will be taught and learned.

Second, similarities exist between indicators (_e.g._, learning methods and assessment) that describe intended and implemented ICT literacy (see [Table 3](#Table3) and [Table 4](#Table4)). This indicates that intended and implemented dimensions are underpinned by the same ideological assumptions and are interrelated. For this reason, the transition in the implementation of ICT literacy from discovering ICT tools to other approaches is inseparable from the conceptual shift in thinking about literacy from the autonomous to the ideological model.

Third, the primary beneficiary of ICT literacy can be either an individual or society. Like all literacies, ICT literacy can be conceptualised from two theoretical perspectives: a narrow competence-focussed autonomous perspective or a broader social practice-based ideological perspective. The literacy perspective selected at the intended level, in essence, determines the internal structure of ICT literacy at the achieved level. For instance, a focus on the benefits for an individual mainly leads to disintegrated teaching and learning of core ICT knowledge and skills, whereas a focus on the benefits to society leads to the integration of ICT knowledge and skills into a broader cognitive context, reflective teaching and learning practices.

Finally, there is a strong connection between implemented and achieved dimensions of ICT literacy. Each new stage of the evolution of ICT at a school is differently associated with various perspectives of ICT literacy (see [Table 3](#Table3) and [Table 4](#Table4)). For instance, in the emerging stage, the enhancement of ICT literacy focuses on separate knowledge and tool-centred learning of ICT capabilities. Thus, teaching and learning mainly concerns technical ICT knowledge and skills. In the applying stage, tool-centred learning of ICT capabilities is integrated with other subjects. Therefore, teaching and learning practices integrate ICT technical capacities into basic literacies and core school subjects. In the infusing stage, the ICT literacy centres on the integrated learning of ICT and cognitive capabilities within the context of various other subjects and new literacies. While finally, in the transforming stage, the integrated learning of ICT, cognitive and other capacities are embedded into the authentic real world context and bring in higher-order metacognitive outputs.

Discussed analytical perspectives and models were originally adapted from the research literature and employed for the analysis of ICT literacy in separate theoretical and empirical studies. These studies analysed the notion of ICT literacy in the national and federal policies in the intended and implemented dimensions ([Markauskaite & Dagiene 2004](#Markauskaite04); [Markauskaite 2005a](#Markauskaite05a)) or university students' ICT literacy learning experiences in the achieved dimension ([Markauskaite 2005b](#Markauskaite05b), [2005c](#Markauskaite05c)). Nevertheless, these theoretical models are interrelated. As this review has shown, the shift in students' learning experiences from disintegrated separate components of ICT literacy to all-inclusive integrated teaching and learning of ICT literacy is associated with progress from the emerging stage to the transforming stage in the implemented dimension and from the autonomous to ideological model in the intended one. All analytical perspectives and models together provide an explicit comprehensive analytical framework for the analysis of ICT literacy policies, practices and experiences. This analytical framework provides a conceptual structure for discovering inconsistencies in the understanding of ICT literacy at various levels of educational systems.

## Acknowledgements

This study was conducted as a part of a Sesqui post-doctoral fellowship at the University of Sydney, Center for Research on Computer Supported Learning and Cognition (CoCo). The author wishes to acknowledge the contribution of Ms Molly Jay Nicholson for her assistance in preparing this paper for the publication. She also thanks anonymous referees for their constructive comments and suggestions to an earlier draft of the paper.

## Notes

1.  <a name="note1"></a>The Educational Testing Service's report gives the following definition: 'Digital technology reflects hardware and software products, communication tools reflect those products and services used to transmit information, and networks themselves are the pathways for this transmission' ([ETS, 2002](#ETS02): 16-17). [[Back]](#note1back)

3.  <a name="note2"></a>The Australian Capital Territory's plan provides the following explanation: 'ICT refers to the whole array of digital technologies including computers, video, digital cameras, handheld computers, assistive technologies, mobile phones, and other devices still in development' ([ACTG, 2004](#ACTG04): 26). [[Back]](#note2back)

5.  <a name="note3"></a>The UNESCO definition of ICT is based on two other complementary terms _informatics (computing science)_ and _informatics technology_ ([Anderson & Weert, 2002](#Anderson02)).
    *   _Informatics (computing science)_ is 'the science dealing with the design, realization, evaluation, use, and maintenance of information processing systems, including hardware, software, organizational and human aspects, and the industrial, commercial, governmental and political implications of these' ([2002](#Anderson02): 12).
    *   _Informatics technology_ is 'the technological applications (artefacts) of informatics in society' ([2002](#Anderson02): 13).
    *   _Information and communication technology (ICT)_ is 'the combination of informatics technology with other, related technologies, specifically communication technology' ([2002](#Anderson02): 13).[[Back]](#note3back)

7.  <a name="note4"></a>Oxford English Dictionary ([1989](#Oxford89)) gives the following descriptions:
    *   _Ability_ - the fact that somebody/something is able to do something; a level of skill or intelligence. Intelligence - the ability to learn, understand and think in a logical way about things; the ability to do this well.
    *   _Capability_ - the ability or qualities necessary to do something. Quality is a thing that is part of a person's character, especially something good.
    *   _Capacity_ - the ability to understand or to do something. Understand - to know or realize how or why something happens, how it works or why it is important, realize - to understand or become aware of a particular fact or situation.
    *   _Competence or competency_ - the ability to do something well; a skill that you need in a particular job or for a particular task.
    *   _Expertise_ - expert knowledge or skill in a particular subject, activity or job.
    *   _Fluency_ - the quality of being able to speak or write a language easily and well.
    *   _Empowerment_ - the state of being empowered (_i.e._, enabled, permitted).
    *   _Knowledge_ - the information, understanding and skills that you gain through education or experience; the state of knowing about a particular fact or situation.
    *   _Know-how_ - knowledge of how to do something and experience in doing it.
    *   _Literacy_ - the ability to read and write.
    *   _Mastery_ - great knowledge about or understanding of a particular thing.
    *   _Proficiency_ - ability to do something well because of training and practice.
    *   _Skills_ - the ability to do something well; a particular ability or type of ability.[[Back]](#note4back)
8.  <a name="note5"></a>E.g., see section 'Delivery options' in _International encyclopaedia of educational technology_ ([Plomp and Ely, 1996](#Plomp96)). [[Back]](#note4back)

## References

*   <a name="AMLA05"></a>Alliance for a Media Literate America. (2005). [_What is media literacy?_](http://www.amlainfo.org/home/media-literacy) Denver, CO: Alliance for a Media Literate America. Retrieved 23 September, 2005 from http://www.amlainfo.org/home/media-literacy
*   <a name="Anderson02"></a>Anderson, J. & Weert, T. (2002). [_Information and communication technology in education. A curriculum for schools and programme of teacher development_](http://unesdoc.unesco.org/images/0012/001295/129538e.pdf). Paris: UNESCO. Retrieved 2 October, 2005 from unesdoc.unesco.org/images/0012/001295/129538e.pdf
*   <a name="Armstrong05"></a>Armstrong, C. (2005). [Defining information literacy for the UK](http://www.cilip.org.uk/publications/updatemagazine/archive/archive2005/janfeb/armstrong.htm). _Library and Information Update_, **4**(1). Retrieved 23 September, 2005 from http://www.cilip.org.uk/publications/updatemagazine/archive/archive2005/janfeb/armstrong.htm
*   <a name="Armstrong04"></a>Armstrong, S. & Warlick, D. (2004, September). [The new literacy](http://www.techlearning.com/content/about/archives/volume25/sep.html). _TechLearning_, **25**(2). Retrieved 2 October, 2005 from http://www.techlearning.com/content/about/archives/volume25/sep.html
*   <a name="ACRL00"></a>Association of College and Research Libraries. (2000). [_Information literacy competency standards for higher education_](http://www.ala.org/ala/acrl/acrlstandards/standards.pdf) Chicago, IL: Association of College and Research Libraries. Retrieved 23 September, 2005 from http://www.ala.org/ala/acrl/acrlstandards/standards.pdf
*   <a name="Aufderheide92"></a>Aufderheide, P. (1992). [_A report of the national leadership conference on media literacy_](http://interact.uoregon.edu/MediaLit/mlr/readings/articles/aspen.html). Washington, DC: The Aspen Institute. Retrieved 24 September, 2005 from http://interact.uoregon.edu/MediaLit/mlr/readings/articles/aspen.html
*   <a name="CDEST03"></a>Australia. _Department of Education, Science and Training._ (2003). _[Raising the standards. A proposal for the development of an ICT competency framework for teachers](http://www.dest.gov.au/sectors/school_education/publications_resources/other_publications/raising_the_standards.htm)_. Canberra: Australian Government, Department of Education, Science and Training. Retrieved 2 October, 2005 from http://www.dest.gov.au/sectors/school_education/publications_resources/other_publications/raising_the_standards.htm
*   <a name="DEYFS03"></a>Australian Capital Territory. _Department of Education Youth and Family Services._ (2003). [_Information and communication technology competences_](http://www.decs.act.gov.au/publicat/ict_competencies.htm). Canberra: Australian Capital Territory, Department of Education Youth and Family Services. Retrieved 26 September, 2005 from http://www.decs.act.gov.au/publicat/ict_competencies.htm
*   <a name="ACTG04"></a>Australian Capital Territory Government. (2004). [_Learning technologies plan for ACT government schools and preschools 2004 -2006\. Transforming the way we teach and learn_](http://activated.det.act.gov.au/admin/ltplan/pdf/LTPlan.pdf). Canberra: Australian Capital Territory Government. Retrieved 2 October, 2005 from http://activated.det.act.gov.au/admin/ltplan/pdf/LTPlan.pdf
*   <a name="Bawden01"></a>Bawden, D. (2001). Progress in documentation - information and digital literacies: a review of concepts. _Journal of Documentation_, **57**(2), 218-259.
*   <a name="Bigum92"></a>Bigum, C., & Green, B. (1992). Technologizing literacy: the dark side of the dream. _Discourse: the Australian Journal of Educational Studies_, **12**(2), 4-28.
*   <a name="Bloom56"></a>Bloom, B. S. & Krathwohl, D. R. (1956). _Taxonomy of educational objectives: the classification of educational goals. Handbook I: cognitive domain_. New York: Longmans.
*   <a name="Bundy04"></a>Bundy, A. (Ed.). (2004). _Australian and New Zealand information literacy framework. Principles, standards and practice_. (2nd ed.). Adelaide: Australian and New Zealand Institute for Information Literacy.
*   <a name="Burton04"></a>Burton, J.K., Moore, D.M. & Magliaro, S.G. (2004). Behaviourism and instructional technology. In D.H. Jonassen (Ed.), _Handbook of research for educational communications and technology_. (2nd ed.). (pp. 3-36). Mahwah, NJ; London: Lawrence Erlbaum Associates.
*   <a name="Candy04"></a>Candy, P.C. (2004). [_Linking Thinking. Self-directed learning in the digital age_](http://www.dest.gov.au/sectors/training_skills/publications_resources/other_publications/linking_thinking.htm). Canberra: Australian Government, Department of Education, Science and Training. Retrieved 27 September, 2005 from http://www.dest.gov.au/sectors/training_skills/publications_resources/other_publications/linking_thinking.htm
*   <a name="Cesarini04"></a>Cesarini, P. (2004). [Computers, technology, and literacies](http://www.literacyandtechnology.org/v4/cesarini.htm). _The Journal of Literacy and Technology_, **4**(1). Retrieved 2 October, 2005 from http://www.literacyandtechnology.org/v4/cesarini.htm
*   <a name="Christ04"></a>Christ, W.G., & Potter, W.G. (2004). [Media literacy, media education and the academy](http://joc.oxfordjournals.org/cgi/reprint/48/1/5). _Journal of Communication_, **48**(1), 5-15\. Retrieved 30 September, 2005 from http://joc.oxfordjournals.org/cgi/reprint/48/1/5
*   <a name="Cobb96"></a>Cobb, P. (1996). Constructivism and learning. In T. Plomp & A.D. Ely (Eds.), _International encyclopaedia of educational technology_. (2 ed.). (pp. 56-59). Oxford: Pergamon.
*   <a name="Collins95"></a>Collins, J. (1995). Literacy and literacies. _Annual Review of Anthropology_, **24**, 75-93.
*   <a name="Compeau95"></a>Compeau, D.R., & Higgins, C.A. (1995). Application of social cognitive theory to training of computer skills. _Information Systems Research_, **6**(2), 118-143.
*   <a name="Cope00"></a>Cope, B. & Kalantzis, M. (2000). _Multiliteracies: literacy learning and the design of social futures_. London: Routledge.
*   <a name="Corbel04"></a>Corbel, C. & Gruba, P. (2004). _Teaching computer literacy_. Sydney: Macquarie University, AMEP Research Centre.
*   <a name="Courts98"></a>Courts, P.L. (1998). Multicultural literacies: dialect, discourses, and diversity. New York, NY: Peter Lang.
*   <a name="Cuttance00"></a>Cuttance, P. & Stokes, S. (2000). _[Monitoring progress towards the national goals for schooling: information and communication technology (ICT) skills and knowledge](http://www.mceetya.edu.au/pdf/reportnepmt_ict.pdf)_. Report to the National Education Performance Monitoring Taskforce of the Ministerial Council on Education, Employment, Training and Youth Affairs. Retrieved 2 October, 2005 from http://www.mceetya.edu.au/pdf/reportnepmt_ict.pdf
*   <a name="Disinger92"></a>Disinger, J.F. & Roth, C.E. (1992). _[Environmental literacy. ERIC/CSMEE Digest](http://www.ericdigests.org/1992-1/literacy.htm)_. . Columbus, OH: ERIC/CSMEE. (ED351201). Retrieved 27 February, 2006 from http://www.ericdigests.org/1992-1/literacy.htm
*   <a name="EC02"></a>European Commission. (2002). _eEurope 2005: an information society for all. An action plan to be presented in view of the Sevilla European Council, 21/22 June 2002_. Brussels: Commission of the European Communities.
*   <a name="EC03"></a>European Commission. _Directorate-General for Education and Culture._ (2003). [_Elearning: better elearning for Europe_](http://europa.eu.int/comm/dgs/education_culture/publ/pdf/elearning/en.pdf). European Commission. Directorate-General for Education and Culture. Luxembourg, Office for Official Publications of the European Communities. Retrieved 2 October, 2005 from http://europa.eu.int/comm/dgs/education_culture/publ/pdf/elearning/en.pdf
*   <a name="ECDL02"></a>ECDL Foundation Ltd. (2002). _[European computer driving licence syllabus version 4.0](http://www.ecdl.com/main/download/syl4feb06.pdf)_. Dublin: ECDL Foundation Limited. Retrieved 27 February, 2006 from http://www.ecdl.com/main/download/syl4feb06.pdf
*   <a name="ECDL04a"></a>ECDL Foundation Ltd. (2004a). _[ECDL-F products](http://www.ecdl.com/main/products.php)_.Dublin: ECDL Foundation Limited. Retrieved 2 October, 2005 from http://www.ecdl.com/main/products.php
*   <a name="ECDL04b"></a>ECDL Foundation Ltd. (2004b). _[Equal skills](http://www.ecdl.com/main/eqskills.php)_. Dublin: ECDL Foundation Limited. Retrieved 2 October, 2005 from http://www.ecdl.com/main/eqskills.php
*   <a name="Eisenberg02"></a>Eisenberg, M.B. & Johnson, D. (2002). _Learning and teaching information technology - computer skills in context. ERIC Digest_. Syracuse, NY: Syracuse University, ERIC Clearinghouse on Information &Technology. (ED465377)
*   <a name="Ershov88"></a>Ershov, A.P. (1988). Basic concepts of algorithms and programming to be taught in a school course in informatics. _BIT Numerical Mathematics_, **28**(3), 397-405.
*   <a name="ETS02"></a>Educational Testing Service. (2002). _[Digital transformation: a framework for ICT literacy. A report of international information and communication literacy panel](http://www.ets.org/Media/Tests/Information_and_Communication_Technology_Literacy/ictreport.pdf)_. Princeton, NJ: Educational Testing Service. Retrieved 2 October, 2005 from http://www.ets.org/Media/Tests/Information_and_Communication_Technology_Literacy/ictreport.pdf
*   <a name="ETS03"></a>Educational Testing Service. (2003). _[Succeeding in the 21st century. What higher education must do to address the gap in information and communication technology proficiencies. Assessing literacy for today and tomorrow](http://www.ets.org/Media/Tests/Information_and_Communication_Technology_Literacy/ICTwhitepaperfinal.pdf)_. Princeton, NJ: Educational Testing Service. Retrieved 2 October, 2005 from http://www.ets.org/Media/Tests/Information_and_Communication_Technology_Literacy/ICTwhitepaperfinal.pdf
*   <a name="Flavell93"></a>Flavell, J.H., Miller, P.H. & Miller, S.A. (1993). _Cognitive development_. (3rd ed.). Englewood Cliffs, NJ: Prentice Hall.
*   <a name="Gee92"></a>Gee, J.P. (1992). _The social mind: language, ideology, and social practice_. New York, NY: Bergin & Garvey.
*   <a name="Henning04"></a>Henning, P.H. (2004). Everyday cognition and situated learning. In D.H. Jonassen (Ed.), _Handbook of research for educational communications and technology_ (2nd ed.). (pp. 143-268). Mahwah, NJ; London: Lawrence Erlbaum Associates.
*   <a name="Herring96"></a>Herring, J. E. (1996). _Teaching information skills in schools_. London: Library Association Publishing.
*   <a name="Honey05"></a>Honey, M., Fasca, C., Gersick, A. Mandinach, E. & Sinha, S. (2005). [_Assessment of 21st century skills: the current landscape. Pre-publication draft._](http://www.21stcenturyskills.org/images/stories/otherdocs/Assessment_Landscape.pdf) New York, NY: Education Development Center's Center for Children & Technology. Retrieved 1 October, 2005 from http://www.21stcenturyskills.org/images/stories/otherdocs/Assessment_Landscape.pdf
*   <a name="Horton03"></a>Horton, W. (2003). [Information literacy: towards an information literate society](http://unesdoc.unesco.org/images/0013/001361/136151e.pdf). _UNISIST Newsletter_, **31**(2), 17-18\. Retrieved 23 September, 2005 from http://unesdoc.unesco.org/images/0013/001361/136151e.pdf
*   <a name="ISTE98"></a>International Society for Technologies in Education. (1998). _[Technology standards. The national educational technology standards (NETS) project. The national educational technology standards for students](http://www.iste.org/standards/)_. Washington D.C.: International Society for Technologies in Education. Retrieved 2 October, 2005 from http://www.iste.org/standards/
*   <a name="ISTE02a"></a>International Society for Technologies in Education. (2002a). [_National educational technology standards for students. Connecting curriculum and technology_](http://cnets.iste.org/students/s_book.html). Washington D.C.: International Society for Technologies in Education. Retrieved 27 September, 2005 from http://cnets.iste.org/students/s_book.html
*   <a name="ISTE02b"></a>International Society for Technologies in Education. (2002b). [_National educational technology standards for teachers. Preparing teachers to use technology_](http://cnets.iste.org/teachers/t_book.html). Washington D.C.: International Society for Technologies in Education. Retrieved 27 September, 2005 from http://cnets.iste.org/teachers/t_book.html
*   <a name="Kearns01"></a>Kearns, P. (2001). _[Review of research: generic skills for the new economy](http://www.ncver.edu.au/research/proj/nr0024.pdf)_. Kensington Park, South Australia: Australian National Training Authority, National Centre for Vocational Education Research. Retrieved 27 February, 2006 from http://www.ncver.edu.au/research/proj/nr0024.pdf

*   <a name="Kearns02a"></a>Kearns, P. & Grant, J. (2002). _[The enabling pillars: learning, technology, community, partnership. A report on Australian policies for information and communication technologies in education and training](http://www.dest.gov.au/NR/rdonlyres/D4C7D055-D510-40DA-9B15-32170EB2227C/911/aust_ict_report.pdf)_. Kambah, Australian Capital Territory: Global Learning Services. Retrieved 27 February, 2006 from http://www.dest.gov.au/NR/rdonlyres/D4C7D055-D510-40DA-9B15-32170EB2227C/911/aust_ict_report.pdf
*   <a name="Kearns02b"></a>Kearns, P. (2002). _Towards the connected learning society. An international overview of trends in policy for information and communication technology in education_. Commonwealth Australia: Global Learning Services.
*   <a name="Keitel94"></a>Keitel, C., Kotzmann, E. & Skovsmose, O. (1994). Beyond the tunnel vision: analysing the relationship between mathematics, society and technology. In P. Ernest (Ed.), _Constructing mathematical knowledge: epistemology and mathematics education_. London, Washington, D.C.: Falmer Press.
*   <a name="Kellner00"></a>Kellner, D. (2000). New technologies/new literacies: reconstructing education for the new millenium. _Teaching Education_, **11**(3), 245-265.
*   <a name="Krathwohl64"></a>Krathwohl, D.R., Bloom, B.S. & Masia, B.B. (1964). _Taxonomy of educational objectives: the classification of educational goals. Handbook II: affective domain_. New York, NY: David McKay Co.
*   <a name="Kress00"></a>Kress, G. (2000). _Literacy in the new media age._ London, New York, NY: Routledge.
*   <a name="Lambrecht99"></a>Lambrecht, J.J. (1999). Teaching technology-related skills. _Journal of Education for Business_, **74**(3), 144-151.
*   <a name="Langhorne89"></a>Langhorne, M.J., Donham, J.O., Gross, J.F. & Rehmke, D. (1989). _Teaching with computers: a new menu for the '90s_. USA: The Oryx Press.
*   <a name="Lankshear97"></a>Lankshear, C. & Knobel, M. (1997). Literacies, text and difference in the electronic age. In C. Lankshear, J.P. Gee, M. Knobel, & M. Searle, _Changing literacies_. (pp. 133-163). Buckingham, Philadelphia: Open University Press.
*   <a name="Law00"></a>Law, N., Yuen, H.K., Ki, W.W., Li, S.C., Lee, Y. & Chow, Y. (2000). _[Changing classrooms & changing schools: a study of good practices in using ICT in Hong Kong schools](http://sites.cite.hku.hk/Changing_blue_book.htm)_. Hong Kong: Centre for Information Technology in School and Teacher Education, The University of Hong Kong. Retrieved 2 October, 2005 from http://sites.cite.hku.hk/Changing_blue_book.htm
*   <a name="Lennon03"></a>Lennon, M., Kirsch, I., Von Davier, M., Wagner, M. & Yamamoto, K. (2003). _[Feasibility study for the PISA ICT literacy assessment. Report to network A](http://www.pisa.oecd.org/dataoecd/35/13/33699866.pdf)_. ACER, ETS, NIER. Retrieved 19 August, 2004 from http://www.pisa.oecd.org/dataoecd/35/13/33699866.pdf
*   <a name="Leu04"></a>Leu, D.J., Kinzer, C.K., Coiro, J.L. & Cammack, D.W. (2004). [Toward a theory of new literacies emerging from the internet and other information and communication technologies](http://www.readingonline.org/newliteracies/leu/). In R.B. Ruddell & N. Unrau. (Eds.), _Theoretical models and processes of reading_. Newark, DE: International Reading Association. Retrieved 23 September, 2005 from http://www.readingonline.org/newliteracies/leu/
*   <a name="Lonsdale04"></a>Lonsdale, M. & McCurry, D. (2004). [_Literacy in the new millennium_](http://www.ncver.edu.au/publications/1490.html). Adelaide: Australian Council for Educational Research, NCVER. Retrieved 2 October, 2005 from http://www.ncver.edu.au/publications/1490.html
*   <a name="Markauskaite04"></a>Markauskaite, L., & Dagiene, V. (2004). Lietuvos bendrojo lavinimo mokyklos kompiuterinio rastingumo samprata siuolaikiniose rastingumo ir IKT diegimo diskursuose (in Lithuanian, Computer literacy in Lithuanian secondary schools: contemporary discourses of literacy and ICT implementation). _Informacijos Mokslai_, **31**, 55-72.
*   <a name="Markauskaite05a"></a>Markauskaite, L. (2005a). [Notions of ICT literacy in Australian school education](http://www.vtex.lt/informatics_in_education/htm/INFE057.htm). _Informatics in Education_, **4**(2), 253-280\. Retrieved 26 September, 2005 from http://www.vtex.lt/informatics_in_education/htm/INFE057.htm
*   <a name="Markauskaite05b"></a>Markauskaite, L. (2005b). From a static to dynamic concept: a model of ICT literacy and an instrument for self-assessment. In P. Goodyear, D.G. Sampson, D.J-T. Yang, Kinshunk, T. Okamoto, R. Hartley, & N-S. Chen (Eds.), _The 5th IEEE International Conference on Advanced Learning Technologies. ICALT 2005\. Kaohsiung, Taiwan, 5-8 July 2005._ (pp. 464-466). Los Alamitos, California: IEEE Computer Society.
*   <a name="Markauskaite05c"></a>Markauskaite, L. (2005c). Markauskaite, L. (2005). Exploring differences in trainee teachers' ICT literacy: does gender matter? In H. Goss (Ed.), _Balance, Fidelity, Mobility: Maintaining the Momentum? Proceedings of the 22nd Annual Conference of the Australasian Society for Computers in Learning in Tertiary Education. Brisbane, Australia 4-7 December 2005._ (pp. 445-455). Brisbane: Teaching and Learning Support Services, Queensland University of Technology.
*   <a name="Martin00"></a>Martin, A. (2000). _[Concepts of ICT literacy in higher education](http://www.citscapes.ac.uk/citscapes/products/backgroundreports/files/concepts_ict_HE.pdf)_. CITSCAPES project. Report. Glasgow: University of Glasgow. Retrieved 2 October, 2005 from http://www.citscapes.ac.uk/citscapes/products/backgroundreports/files/concepts_ict_HE.pdf
*   <a name="May97"></a>May, T. (1997). _Social research: issues, methods and process_. Buckingham, Philadelphia: Open University Press.
*   <a name="McMillan96"></a>McMillan, S. (1996). Literacy and computer literacy: definitions and comparisons. _Computers and Education_, **27**(3-4), 161-170.
*   <a name="NCREL03"></a>NCREL. (2003). _[21st century skills: literacy in the digital age](http://www.ncrel.org/engauge/skills/skills.htm)_. North Central Regional Educational Laboratory (NCREL). Retrieved 2 October, 2005 from http://www.ncrel.org/engauge/skills/skills.htm
*   <a name="New00"></a>New London Group. (2000). A pedagogy of Multiliteracies designing social future. In B. Cope & M. Kalantzis (Eds.), _Multiliteracies: literacy learning and the design of social futures_. (pp. 9-37). London: Routledge.
*   <a name="NRC99"></a>NRC. (1999). _Being fluent with information technology_. Washington: National Research Council, National Academy Press.
*   <a name="OECD01"></a>OECD. (2001). _Schooling for tomorrow. Learning to change: ICT in schools. Education and skills_. Paris: OECD, Center for Educational Research and Innovation.
*   <a name="Oxford89"></a>_Oxford English Dictionary_. (1989). 2nd edition. Oxford University Press.
*   <a name="Partnership02"></a>Partnership for 21st Century Skills. (2002). _[A report and mile guide for 21st century skills](http://www.21stcenturyskills.org/)_. Partnership for 21st Century Skills. Retrieved 2 October, 2005 from http://www.21stcenturyskills.org/
*   <a name="Partnership04"></a>Partnership for 21st Century Skills. (2004). _[ICT literacy maps](http://www.21stcenturyskills.org/index.php?option=com_content&task=view&id=31&Itemid=33)_. Partnership for 21st Century Skills. Retrieved 1 October, 2005 from http://www.21stcenturyskills.org/index.php?option=com_content&task=view&id=31&Itemid=33
*   <a name="Phelps01"></a>Phelps, R., Ellis, A., & Hase, S. (2001). [The role of metacognitive and reflective learning processes in developing capable computer users](https://secure.ascilite.org.au/conferences/melbourne01/pdf/papers/phelpsr.pdf). In G. Kennedy, M. Keppell, C. McNaught & T. Petrovic (Eds.), _Meeting at the crossroads. Proceedings of the 18th Annual Conference of the Australian Society for Computers in Learning in Tertiary Education_. (pp. 481-490). Melbourne: Biomedical Multimedia Unit, The University of Melbourne. Retrieved 26 September, 2005 from https://secure.ascilite.org.au/conferences/melbourne01/pdf/papers/phelpsr.pdf
*   <a name="Phelps02"></a>Phelps, R. & Ellis, A. (2002). _A metacognitive approach to computer education for teachers: Combining theory and practice for computer capability_. Paper presented at the Linking learners: Australian Computers in Education Conference (ACEC2002), Hobart, Tasmania.
*   <a name="Plomp96"></a>Plomp, T. & Ely, D. (Eds.). (1996). _International encyclopaedia of educational technology_. (2 ed.). Great Britain: Pergamon.
*   <a name="Potter01"></a>Potter, J.W. (2001). _Media literacy_. (2nd ed.). Thousand Oaks: Sage Publications.
*   <a name="QCA05"></a>QCA. (2005). [_Information and communication technology. Standards for adult ICT skills_](http://www.qca.org.uk/2791.html). London: Qualifications and Curriculum Authority. Retrieved 27 September, 2005 from http://www.qca.org.uk/2791.html
*   <a name="Ridgeway95"></a>Ridgeway, J. & Passey, D. (1995). Using evidence about teacher development to plan systematic revolution. In D. Watson D. & D. Tinsley (Eds.), _Integrating Information Technology into Education_ (pp. 59-72). London: Chapman & Hall, IFIP.
*   <a name="Ridgeway04"></a>Ridgeway, J., McCusker, S., & Pead, D. (2004). [_Literature review on e-assessment._](http://www.nestafuturelab.org/images/downloads/futurelab_review_10.pdf) Nesta Futurelab Series. Report no 10\. United Kingdom: Nesta Futurelab. Retrieved 1 October, 2005 from http://www.nestafuturelab.org/images/downloads/futurelab_review_10.pdf
*   <a name="Robitaille96"></a>Robitaille, D.F. & Maxwell, B. (1996). The conceptual framework and research questions. In D.F. Robitaille & R.A. Garden (Eds.), _TIMSS Monograph No 2: Research questions and study design_. Vancouver, BC: Pacific Educational Press.
*   <a name="Ropp99"></a>Ropp, M.M. (1999). Exploring individual characteristics associated with learning to use computers in preservice teacher preparation. _Journal of Research on Computing in Education_, **31**(4), 402-424.
*   <a name="Schwarrz05"></a>Schwarrz, G. & Brown, P.U. (2005). _Media literacy: transforming curriculum and teaching. 104 Yearbook of the National Society for the Study of Education_. Malden: Blackwell Publishing.
*   <a name="Selfe02"></a>Selfe, C.L., & Hawisher, G.E. (2002). A historical look at electronic literacy. _Journal of Business and Technical Communication_, **16**(3), 231-276.
*   <a name="Snyder02"></a>Snyder, I. (Ed.). (2002). _Silicon literacies: communication, innovation and education in the electronic age_. London: Routledge.
*   <a name="Street84"></a>Street, B. (1984). _Literacy in theory and practice_. Cambridge: Cambridge University Press.
*   <a name="Street03"></a>Street, B. (2003). What's 'new' in new literacy studies? Critical approaches to literacy in theory and practice. _Current Issues in Comparative Education_, **5**(2), 1-14.
*   <a name="Tiffin95"></a>Tiffin, J. & Rajasingham, L. (1995). _In search of the virtual class. Education in an information society_. London: Routledge.
*   <a name="Unsworth02"></a>Unsworth, L. (2002). Changing dimensions of school literacies. _Australian Journal of Language and Literacy_, **25**(1), 62-77.
*   <a name="Virkus03"></a>Virkus, S. (2003). [Information literacy in Europe: a literature review](http://informationr.net/ir/8-4/paper159.html). _Information Research_, **8**(4). Paper No. 159\. Retrieved 2 October, 2005 from http://informationr.net/ir/8-4/paper159.html
*   <a name="Voogt96"></a>Voogt, J.M. (1996). New information technology in science education. In T. Plomp & A.D. Ely (Eds.), _International encyclopaedia of educational technology_. (2 ed.). (pp. 551-556). Great Britain: Pergamon.
*   <a name="Wallace03"></a>Wallace, R.M. (2003). On-line learning in higher education: a review of research on interactions among students and teachers. _Education, Communication and Information_, **3**(2), 241-280.
*   <a name="Warschauer99"></a>Warschauer, M. (1999). _Electronic literacies: language, culture, and power in online education_. Mahwah, N.J.: Lawrence Erlbaum Associates.
*   <a name="Weert94"></a>Weert, T. & Tinsley, D. (Eds.). (1994). _Informatics for secondary education. A curriculum for schools_. Paris: UNESCO.
*   <a name="Weert95"></a>Weert, T. (1995). Integration of informatics into education. In D. Watson, & D. Tinsley. (Eds.), _Integrating information technology into education_ (pp. 127-137). London: Chapman & Hall, IFIP.
*   <a name="Weil98"></a>Weil, D.K. (1998). Toward a critical multicultural literacy. New York, NY: Peter Lang.
*   <a name="Williams03"></a>Williams, K. (2003). [Literacy and computer literacy: analyzing the NRC's being fluent with information technology](http://www.literacyandtechnology.org/v3n1/williams.htm). _The Journal of Literacy and Technology_, **3**(1). Retrieved 2 October, 2005 from http://www.literacyandtechnology.org/v3n1/williams.htm
*   <a name="Zimmerman96"></a>Zimmerman, B.J., Bonner, S. & Kovach, R. (1996). _Developing self-regulated learners: beyond achievement of self-efficacy_. Washington, DC: American Psychological Association.





