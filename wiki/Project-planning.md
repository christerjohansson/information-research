# Project Plan 2020

## 1 Migration

Första steget är att färdigställa import av samtligt material enling den ursprungliga projektplanen. Till stöd för detta finns XML-filer som innehåller all meta-data för varje utgåva och artikel. Material ligger samlat i mapp för var utgåva.

### 1.1 Kvalitetskontroll före import till Open Journal System

Innan import till OJS, måste varje XML-fil valideras och kvalitetskontrolleras för att säkerställa att varje utgåva och artikel får rätt metadata och att bildmaterial, samt länkat material, importeras korrekt. Till stöd för detta finns både schema och tidigare mallar för referens.

## 2
